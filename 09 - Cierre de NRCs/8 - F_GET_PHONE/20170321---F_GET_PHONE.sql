/*
SET SERVEROUTPUT ON
DECLARE   P_RESP                 VARCHAR2(100);
BEGIN
    P_RESP := F_GET_PHONE(473906,1);
    DBMS_OUTPUT.PUT_LINE( '--' || P_RESP);
END;
*/

CREATE OR REPLACE FUNCTION  F_GET_PHONE (
          P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_N_ORDER         NUMBER
) RETURN VARCHAR2        
AS
/* ===================================================================================================================
  NOMBRE    : F_GET_PHONE
  FECHA     : 21/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Segun el PIDM obtiene el TELEFONO que se encuentra en el orden "P_N_ORDER" indicado.
              
              P_N_ORDER
                1º       --> numero preferido
                2º a mas --> numero segun el orden indicado

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
          V_PHONE           VARCHAR2(49);
          V_RESP_PHONE      VARCHAR2(49);
          
          -- GET PHONE
          CURSOR C_SPRTELE_PRIM IS
          SELECT PRONE FROM (
              SELECT TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PRONE
              FROM SPRTELE LEFT JOIN STVTELE ON SPRTELE_TELE_CODE = STVTELE_CODE
              WHERE SPRTELE_PIDM = P_PIDM AND SPRTELE_PRIMARY_IND = 'Y' AND SPRTELE_STATUS_IND IS NULL
              ORDER BY SPRTELE_SEQNO
          )WHERE ROWNUM = 1;
          
          CURSOR C_SPRTELE IS
          SELECT PRONE FROM (
              SELECT TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PRONE
              FROM SPRTELE LEFT JOIN STVTELE ON SPRTELE_TELE_CODE = STVTELE_CODE
              WHERE SPRTELE_PIDM = P_PIDM AND SPRTELE_PRIMARY_IND IS NULL AND SPRTELE_STATUS_IND IS NULL
              ORDER BY SPRTELE_SEQNO
          )WHERE ROWNUM = P_N_ORDER;
BEGIN
     V_RESP_PHONE := '-';
     -- GET telefono
     IF (P_N_ORDER = 1) THEN
          OPEN C_SPRTELE_PRIM;
          LOOP
            FETCH C_SPRTELE_PRIM INTO V_PHONE;
            IF C_SPRTELE_PRIM%FOUND THEN
                V_RESP_PHONE := CASE WHEN V_PHONE LIKE '%<%' OR V_PHONE LIKE '%>%' THEN '-' ELSE V_PHONE END ;
            ELSE EXIT;
          END IF;
          END LOOP;
          CLOSE C_SPRTELE_PRIM;
      ELSE  
          OPEN C_SPRTELE;
          LOOP
            FETCH C_SPRTELE INTO V_PHONE;
            IF C_SPRTELE%FOUND THEN
                V_RESP_PHONE := CASE WHEN V_PHONE LIKE '%<%' OR V_PHONE LIKE '%>%' THEN '-' ELSE V_PHONE END ;
            ELSE EXIT;
          END IF;
          END LOOP;
          CLOSE C_SPRTELE;
      END IF;
      
      RETURN V_RESP_PHONE;
END F_GET_PHONE;