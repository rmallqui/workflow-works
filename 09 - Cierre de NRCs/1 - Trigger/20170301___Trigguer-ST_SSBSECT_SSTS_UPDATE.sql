/*
-- buscar objecto
select dbms_metadata.get_ddl('TRIGGER','ST_SSBSECT_SSTS_UPDATE') from dual

-- Desabilitar - disable trigger
ALTER TRIGGER ST_SSBSECT_SSTS_UPDATE ENABLE;
DROP TRIGGER ST_SSBSECT_SSTS_UPDATE;

select * from spriden where spriden_id = '70073244'; --325180
*/


/*
-- buscar objecto
select dbms_metadata.get_ddl('TRIGGER','ST_SSBSECT_SSTS_UPDATE') from dual

-- Desabilitar - disable trigger
ALTER TRIGGER ST_SSBSECT_SSTS_UPDATE ENABLE;
DROP TRIGGER ST_SSBSECT_SSTS_UPDATE;

select * from spriden where spriden_id = '70073244'; --325180
*/



CREATE OR REPLACE TRIGGER ST_SSBSECT_SSTS_UPDATE
BEFORE UPDATE
   ON "SATURN"."SSBSECT"
   FOR EACH ROW
/* ===================================================================================================================
  NOMBRE    : ST_SSBSECT_SSTS_UPDATE
  FECHA     : 02/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Generar un evento al cambiar el estado del NRC --> "P" POR CERRAR

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION                               
  =================================================================================================================== */
DECLARE
      V_PERIODO                 SSBSECT.SSBSECT_TERM_CODE%TYPE;
      V_FECHA_SOLICITUD         SSBSECT.SSBSECT_ACTIVITY_DATE%TYPE;
      V_SSTS_CODE               SSBSECT.SSBSECT_SSTS_CODE%TYPE;     -- ** Estado 'P' por cerrar
      V_MAX_ENRL                SSBSECT.SSBSECT_MAX_ENRL%TYPE;      -- ** Maximo de vacante
      V_NRC                     SSBSECT.SSBSECT_CRN%TYPE;

      V_LIGA                    NUMBER;
      V_LIGA_DESCRIPCION        VARCHAR2(50);
      V_SEQ_NUMB                SSBSECT.SSBSECT_SEQ_NUMB%TYPE;      -- Seccion
      V_LIGA_IDENT              SSBSECT.SSBSECT_LINK_IDENT%TYPE;    -- IDEN_LIGA -- IDENTIFICADOR LIGA
      V_LIGA_CONEC              SSRLINK.SSRLINK_LINK_CONN%TYPE;     -- CONE_LIGA
      V_INDICADOR               NUMBER;
      V_MESSAGE                 VARCHAR2(150);

      V_PARAMS          GOKPARM.T_PARAMETERLIST;
      EVENT_CODE        GTVEQNM.GTVEQNM_CODE%TYPE;
BEGIN
-- 
      -- -- -- -- GET CODIGO SERVICIO -- -- -- --
      V_NRC                 :=      :NEW.SSBSECT_CRN;
      V_PERIODO             :=      :NEW.SSBSECT_TERM_CODE;
      V_FECHA_SOLICITUD     :=      SYSDATE;
      V_SSTS_CODE           :=      :NEW.SSBSECT_SSTS_CODE;
      V_MAX_ENRL            :=      :NEW.SSBSECT_MAX_ENRL;

      IF V_SSTS_CODE = 'P' THEN  -- 'P' --> "POR CERRAR"

          -- Check for the event definition and set the event code.
          event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFUPD01');

          -- Calcular si el NRC es: SIMPLE, PADRE, HIJO. 
          SELECT  
                CASE  WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSRLINK_LINK_CONN   THEN 1 -- Ident <-> Conector (NRC HIJO)
                      WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSBSECT_LINK_IDENT  THEN 2 -- Seccion <-> Conector (NRC PADRE)
                      WHEN NVL(TRIM(SSRLINK_LINK_CONN),'0') = '0'             THEN 3 -- (NRC SIMPLE)
                      ELSE 0 END,
                SSBSECT_SEQ_NUMB,   -- Seccion
                SSBSECT_LINK_IDENT, -- IDEN_LIGA -- IDENTIFICADOR LIGA
                SSRLINK_LINK_CONN   -- CONE_LIGA
          INTO  V_LIGA,
                V_SEQ_NUMB,
                V_LIGA_IDENT,
                V_LIGA_CONEC
          FROM SSBSECT -- NRCs
          LEFT JOIN SSRLINK -- CONECTOR LIGAS
            ON SSRLINK_TERM_CODE    = SSBSECT_TERM_CODE
            AND SSRLINK_CRN         = SSBSECT_CRN
          WHERE  SSBSECT_TERM_CODE  = V_PERIODO AND SSBSECT_CRN = V_NRC;

          -- SET PARAMETERS
          IF (EVENT_CODE <> 'NULL') THEN
              --------- Set the parameter values
              v_Params(1).param_value := event_code;
              v_Params(2).param_value := 'Banner';
              v_Params(3).param_value := 'WF Solicitud Cierre NRC '|| V_NRC || ' Periodo ' || V_PERIODO;
              v_Params(4).param_value := V_PERIODO;
              v_Params(5).param_value := V_NRC;
              v_Params(6).param_value := V_MAX_ENRL;    
              v_Params(7).param_value := V_LIGA;
              v_Params(8).param_value := V_SEQ_NUMB;
              v_Params(9).param_value := V_LIGA_IDENT;
              v_Params(10).param_value := V_LIGA_CONEC;
              --------- Create the event
              Gokparm.Send_Param_List(event_code, v_Params);
          END IF;

      END IF;
            
END;

