/*
-- buscar objecto
select dbms_metadata.get_ddl('TRIGGER','ST_SSBSECT_SSTS_UPDATE') from dual

-- Desabilitar - disable trigger
ALTER TRIGGER ST_SSBSECT_SSTS_UPDATE ENABLE;
DROP TRIGGER ST_SSBSECT_SSTS_UPDATE;

select * from spriden where spriden_id = '70073244'; --325180
*********************************************************************/
-- PERMISOS REQUERIDOS: WFEVENT User sobre la tabla para el trigger
-- *****************************************************************




CREATE OR REPLACE TRIGGER ST_SSBSECT_SSTS_UPDATE
BEFORE UPDATE
   ON "SATURN"."SSBSECT"
   FOR EACH ROW
/* ===================================================================================================================
  NOMBRE    : ST_SSBSECT_SSTS_UPDATE
  FECHA     : 16/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Generar un evento al cambiar el estado del NRC --> "P" POR CERRAR

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION                               
  =================================================================================================================== */
DECLARE
      V_PERIODO                 SSBSECT.SSBSECT_TERM_CODE%TYPE;
      V_FECHA_SOLICITUD         SSBSECT.SSBSECT_ACTIVITY_DATE%TYPE;
      V_SSTS_CODE               SSBSECT.SSBSECT_SSTS_CODE%TYPE;     -- ** Estado 'P' por cerrar
      V_NRC_MAXENROL            SSBSECT.SSBSECT_MAX_ENRL%TYPE;      -- ** Maximo de vacante
      V_NRC_ENROL               SSBSECT.SSBSECT_ENRL%TYPE;          -- ** N ENROLADOS
      V_NRC                     SSBSECT.SSBSECT_CRN%TYPE;

      V_INDICADOR               NUMBER;
      V_MESSAGE                 VARCHAR2(150);

      V_PARAMS          GOKPARM.T_PARAMETERLIST;
      EVENT_CODE        GTVEQNM.GTVEQNM_CODE%TYPE;
    
BEGIN
-- 
      -- -- -- -- GET CODIGO SERVICIO -- -- -- --
      V_NRC                 :=      :NEW.SSBSECT_CRN;
      V_PERIODO             :=      :NEW.SSBSECT_TERM_CODE;
      V_FECHA_SOLICITUD     :=      SYSDATE;
      V_NRC_ENROL           :=      :NEW.SSBSECT_ENRL;

      IF :NEW.SSBSECT_SSTS_CODE = 'P' AND :OLD.SSBSECT_SSTS_CODE <> 'P' THEN  -- 'P' --> "POR CERRAR"
          
          V_SSTS_CODE               :=  :OLD.SSBSECT_SSTS_CODE;
          V_NRC_MAXENROL            :=  :OLD.SSBSECT_MAX_ENRL;
          :NEW.SSBSECT_MAX_ENRL     :=  0;
          :NEW.SSBSECT_SEATS_AVAIL  :=  0 - V_NRC_ENROL; -- # vacantes restantes

          -- Check for the event definition and set the event code.
          event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFUPD01');

          -- SET PARAMETERS
          IF (EVENT_CODE <> 'NULL') THEN
              --------- Set the parameter values
              v_Params(1).param_value := event_code;
              v_Params(2).param_value := 'Banner';
              v_Params(3).param_value := 'Solicitud de Cierre de NRC: '|| V_NRC || ' - Periodo: ' || V_PERIODO;
              v_Params(4).param_value := V_PERIODO;
              v_Params(5).param_value := V_NRC;
              v_Params(6).param_value := V_NRC_MAXENROL;    -- OLD # de vacantes
              v_Params(7).param_value := V_SSTS_CODE;       -- OLD estado del NRC
              --------- Create the event
              Gokparm.Send_Param_List(event_code, v_Params);
          END IF;

      END IF;
            
END;

