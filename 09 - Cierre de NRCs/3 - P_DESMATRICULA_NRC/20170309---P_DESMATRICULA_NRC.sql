/*
SET SERVEROUTPUT ON
declare 
      P_NRC_IN          SSBSECT.SSBSECT_CRN%TYPE := '1185';
      P_PERIODO         STVTERM.STVTERM_CODE%TYPE := '201710';
      P_LIGA            NUMBER;
      P_LIGA_IDENT      SSBSECT.SSBSECT_LINK_IDENT%TYPE;
      P_LIGA_CONEC      SSRLINK.SSRLINK_LINK_CONN%TYPE;
      P_ERROR           VARCHAR2(500);
begin
    P_GET_VALIDACION(P_PERIODO,P_NRC_IN,P_LIGA,P_LIGA_IDENT,P_LIGA_CONEC,P_ERROR); -- 381 380
    DBMS_OUTPUT.PUT_LINE(P_LIGA||' - '||P_LIGA_IDENT||' - '||P_LIGA_CONEC||' - '||P_ERROR);
    
    
    --- PRUEBA DEL SP
    P_DESMATRICULA_NRC(P_PERIODO, P_NRC_IN, P_LIGA, P_LIGA_IDENT, P_LIGA_CONEC, P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_ERROR);
end;
*/

CREATE OR REPLACE PROCEDURE P_DESMATRICULA_NRC (
      P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC_IN          IN SSBSECT.SSBSECT_CRN%TYPE,
      P_LIGA            IN NUMBER,
      P_LIGA_IDENT      IN SSBSECT.SSBSECT_LINK_IDENT%TYPE,
      P_LIGA_CONEC      IN SSRLINK.SSRLINK_LINK_CONN%TYPE,
      P_ERROR           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_DESMATRICULA_NRC
  FECHA     : 08/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Lista y prepara los NRCs para desmaticular alumnos.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
      
      V_NRC_P           SSBSECT.SSBSECT_CRN%TYPE;
      E_INVALID         EXCEPTION;
      E_INVALIDNRC      EXCEPTION;
      
      V_SSBSECT_REC     SSBSECT%ROWTYPE;
      -- ADD SSBSECT -
      CURSOR C_SSBSECT IS
        SELECT SSBSECT.* FROM SSBSECT 
        INNER JOIN SSRLINK
           ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
          AND SSRLINK_CRN = SSBSECT_CRN
        WHERE SSBSECT_LINK_IDENT = P_LIGA_CONEC
        AND SSRLINK_LINK_CONN = P_LIGA_IDENT
        AND SSBSECT_TERM_CODE = P_PERIODO;
      
BEGIN
    
    IF P_LIGA = 0 THEN
        RAISE E_INVALID;
    /*******************************************************************
    -- Para NRCs --> HIJO (1)
    ********************************************************************/
    ELSIF (P_LIGA = 1) THEN

        -- get NRC PADRE
        SELECT SSBSECT_CRN INTO V_NRC_P
        FROM SSBSECT 
        INNER JOIN SSRLINK
           ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
          AND SSRLINK_CRN = SSBSECT_CRN
        WHERE SSBSECT_LINK_IDENT = P_LIGA_CONEC
        AND SSRLINK_LINK_CONN = P_LIGA_IDENT
        AND SSBSECT_TERM_CODE = P_PERIODO;

        -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
        P_ALUMDEL_NRC(P_PERIODO, P_NRC_IN, V_NRC_P);
        
        
    /*******************************************************************
    -- Para NRCs --> PADRE (2)  Ó  NRC SIMPLE --> (3)
    ********************************************************************/
    ELSIF (P_LIGA = 2 OR P_LIGA = 3) THEN
        
        OPEN C_SSBSECT;
        LOOP
            FETCH C_SSBSECT INTO V_SSBSECT_REC;
            IF C_SSBSECT%FOUND THEN
            
                -- UPDATE NRC Hijos CUMPLA CON LO NECESARIO  CON REQUISITOS (**)
                UPDATE SSBSECT
                SET SSBSECT_SSTS_CODE = 'P',
                    SSBSECT_ACTIVITY_DATE = SYSDATE, 
                    SSBSECT_DATA_ORIGIN = 'WorkFlow',
                    SSBSECT_USER_ID = USER
                WHERE SSBSECT_TERM_CODE = V_SSBSECT_REC.SSBSECT_TERM_CODE 
                AND   SSBSECT_CRN = V_SSBSECT_REC.SSBSECT_CRN
                AND   SSBSECT_SSTS_CODE <> 'P'; -- ** Estado 'P' por cerrar
                
                -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
                P_ALUMDEL_NRC(V_SSBSECT_REC.SSBSECT_TERM_CODE, V_SSBSECT_REC.SSBSECT_CRN);
                
            ELSE  
                -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
                P_ALUMDEL_NRC(P_PERIODO, P_NRC_IN);
                EXIT;
            END IF;
        END LOOP;
        CLOSE C_SSBSECT;
          
    END IF;
    
    COMMIT;
    
EXCEPTION
  WHEN E_INVALID THEN
      P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '|| 'No se detecto el NRC o no cumple con algun requisito para cerrarse.';
  WHEN OTHERS THEN
      P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_DESMATRICULA_NRC;