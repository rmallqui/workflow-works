create or replace PACKAGE WFK_CONTISCNRC AS
/*******************************************************************************
 WFK_CONTISESM:
       Conti Package SOLICITUD DE CIERRER DE NRC
*******************************************************************************/
-- FILE NAME..: WFK_CONTISCNRC.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISCNRC
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/


PROCEDURE P_GET_VALIDACION (
          P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
          P_NRC             IN SSBSECT.SSBSECT_CRN%TYPE,
          P_LIGA            OUT NUMBER,
          P_LIGA_IDENT      OUT SSBSECT.SSBSECT_LINK_IDENT%TYPE,
          P_LIGA_CONEC      OUT SSRLINK.SSRLINK_LINK_CONN%TYPE,
          P_NUM_CURSO       OUT SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
          P_TITULO          OUT SCRSYLN.SCRSYLN_LONG_COURSE_TITLE%TYPE,
          P_SECCION         OUT SSBSECT.SSBSECT_SEQ_NUMB%TYPE,
          P_ENROLADOS       OUT SSBSECT.SSBSECT_ENRL%TYPE,
          P_CAMP_DESC       OUT STVCAMP.STVCAMP_DESC%TYPE,
          P_PTRM_CODE       OUT SSBSECT.SSBSECT_PTRM_CODE%TYPE,
          P_GTVINSM_DESC    OUT GTVINSM.GTVINSM_DESC%TYPE,
          P_INSTRUC_ID      OUT SPRIDEN.SPRIDEN_ID%TYPE,
          P_INSTRUC_NAME    OUT VARCHAR2,
          P_ERROR           OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_DESMATRICULA_NRC (
          P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
          P_NRC_IN          IN SSBSECT.SSBSECT_CRN%TYPE,
          P_LIGA            IN NUMBER,
          P_LIGA_IDENT      IN SSBSECT.SSBSECT_LINK_IDENT%TYPE,
          P_LIGA_CONEC      IN SSRLINK.SSRLINK_LINK_CONN%TYPE,
          P_NUM_CURSO       IN SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
          P_HTML1           OUT VARCHAR2,
          P_HTML2           OUT VARCHAR2,
          P_HTML3           OUT VARCHAR2,
          P_HTML4           OUT VARCHAR2,
          P_HTML5           OUT VARCHAR2,
          P_MAIL_STDN       OUT VARCHAR2,
          P_ERROR           OUT VARCHAR2
    );
              
--------------------------------------------------------------------------------

PROCEDURE P_NRC_CLOSE (
          P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
          P_NRC_IN          IN SSBSECT.SSBSECT_CRN%TYPE,
          P_LIGA            IN NUMBER,
          P_LIGA_IDENT      IN SSBSECT.SSBSECT_LINK_IDENT%TYPE,
          P_LIGA_CONEC      IN SSRLINK.SSRLINK_LINK_CONN%TYPE,
          P_NUM_CURSO       IN SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
          P_ERROR           OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_ALUMDEL_NRC (
          P_PERIODO      IN STVTERM.STVTERM_CODE%TYPE,
          P_NRC          IN SSBSECT.SSBSECT_CRN%TYPE,
          P_NRCSUB       IN SSBSECT.SSBSECT_CRN%TYPE DEFAULT NULL,
          P_HTML1        OUT VARCHAR2,
          P_HTML2        OUT VARCHAR2,
          P_HTML3        OUT VARCHAR2,
          P_HTML4        OUT VARCHAR2,
          P_HTML5        OUT VARCHAR2,
          P_MAIL_STDN    OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_NRC_CANCEL (
        P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
        P_NRC             IN SSBSECT.SSBSECT_CRN%TYPE,
        P_OLD_SSTS_CODE   IN SSBSECT.SSBSECT_SSTS_CODE%TYPE,
        P_MAXENROL        IN SSBSECT.SSBSECT_MAX_ENRL%TYPE
    );

--------------------------------------------------------------------------------

FUNCTION  F_GET_MAIL (
            P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_MAILCORP        BOOLEAN          
      )RETURN VARCHAR2;

--------------------------------------------------------------------------------

FUNCTION  F_GET_PHONE (
          P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_N_ORDER         NUMBER
      ) RETURN VARCHAR2;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTISCNRC;