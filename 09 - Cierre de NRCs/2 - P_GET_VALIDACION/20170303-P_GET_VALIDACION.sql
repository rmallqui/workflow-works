/*
SET SERVEROUTPUT ON
declare P_LIGA            NUMBER;
      P_LIGA_IDENT      SSBSECT.SSBSECT_LINK_IDENT%TYPE;
      P_LIGA_CONEC      SSRLINK.SSRLINK_LINK_CONN%TYPE;
      P_ERROR           VARCHAR2(500);
begin
    P_GET_VALIDACION('201730','1175',P_LIGA,P_LIGA_IDENT,P_LIGA_CONEC,P_ERROR); -- 381 380
    DBMS_OUTPUT.PUT_LINE(P_LIGA||' - '||P_LIGA_IDENT||' - '||P_LIGA_CONEC||' - '||P_ERROR);
end;
*/

CREATE or replace PROCEDURE P_GET_VALIDACION (
      P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC             IN SSBSECT.SSBSECT_CRN%TYPE,
      P_LIGA            OUT NUMBER,
      P_LIGA_IDENT      OUT SSBSECT.SSBSECT_LINK_IDENT%TYPE,
      P_LIGA_CONEC      OUT SSRLINK.SSRLINK_LINK_CONN%TYPE,
      P_ERROR           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_VALIDACION
  FECHA     : 03/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida y obtiene los datos y calcula si NRC es Hijo,Padre o un NRC sin ligados.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
      V_INDICADOR       NUMBER;
      E_INVALID         EXCEPTION;
      V_SEQ_NUMB        SSBSECT.SSBSECT_SEQ_NUMB%TYPE;
BEGIN
    
    -- VALIDAR NRC CUMPLA CON LO NECESARIO (**)
    SELECT COUNT(*) INTO V_INDICADOR 
    FROM SSBSECT 
    WHERE SSBSECT_TERM_CODE = P_PERIODO 
    AND   SSBSECT_CRN = P_NRC
    AND   SSBSECT_MAX_ENRL = '0'   -- ** Maximo de vacante
    AND   SSBSECT_SSTS_CODE = 'P'; -- ** Estado 'P' por cerrar
    
    -- Get si NRC es: SIMPLE, PADRE, HIJO.
    SELECT COUNT(*) INTO P_LIGA FROM SSBSECT -- NRCs
    LEFT JOIN SSRLINK ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE AND SSRLINK_CRN = SSBSECT_CRN
    WHERE  SSBSECT_TERM_CODE = P_PERIODO AND SSBSECT_CRN = P_NRC;
    IF (P_LIGA > 0) THEN
        SELECT  
              CASE  WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSRLINK_LINK_CONN   THEN 1 -- Indent <-> Conector (NRC HIJO)
                    WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSBSECT_LINK_IDENT  THEN 2 -- Seccion <-> Conector (NRC PADRE)
                    WHEN NVL(TRIM(SSRLINK_LINK_CONN),'0') = '0'             THEN 3 -- (NRC SIMPLE)
                    ELSE 0 END,
              SSBSECT_SEQ_NUMB,   -- Seccion
              SSBSECT_LINK_IDENT, -- IDEN_LIGA -- IDENTIFICADOR LIGA
              SSRLINK_LINK_CONN   -- CONE_LIGA
        INTO  P_LIGA,
              V_SEQ_NUMB,
              P_LIGA_IDENT,
              P_LIGA_CONEC
        FROM SSBSECT -- NRCs
        LEFT JOIN SSRLINK -- CONECTOR LIGAS
          ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
          AND SSRLINK_CRN = SSBSECT_CRN
        WHERE  SSBSECT_TERM_CODE = P_PERIODO AND SSBSECT_CRN = P_NRC;
    END IF;
    
    /*******************************************************************
    -- VALIDACION 
    ********************************************************************/
    IF (V_INDICADOR = 0 OR P_LIGA = 0 ) THEN
        P_LIGA      := 0;
        RAISE E_INVALID;
    END IF;
    
EXCEPTION
    WHEN E_INVALID THEN
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '|| 'No se detecto el NRC o no cumple con algun requisito para cerrarse.';
    WHEN OTHERS THEN
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_GET_VALIDACION;