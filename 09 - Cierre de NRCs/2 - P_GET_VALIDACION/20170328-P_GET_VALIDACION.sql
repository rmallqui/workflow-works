/*
SET SERVEROUTPUT ON
declare 
      P_LIGA            NUMBER;
      P_LIGA_IDENT      SSBSECT.SSBSECT_LINK_IDENT%TYPE;
      P_LIGA_CONEC      SSRLINK.SSRLINK_LINK_CONN%TYPE;
      P_NUM_CURSO       SSBSECT.SSBSECT_CRSE_NUMB%TYPE;
      P_TITULO          SCRSYLN.SCRSYLN_LONG_COURSE_TITLE%TYPE;
      P_SECCION         SSBSECT.SSBSECT_SEQ_NUMB%TYPE;
      P_ENROLADOS       SSBSECT.SSBSECT_ENRL%TYPE;
      P_CAMP_DESC       STVCAMP.STVCAMP_DESC%TYPE;
      P_PTRM_CODE       SSBSECT.SSBSECT_PTRM_CODE%TYPE;
      P_ERROR           VARCHAR2(1000);
begin
    P_GET_VALIDACION('201710','1175',P_LIGA,P_LIGA_IDENT,P_LIGA_CONEC,P_NUM_CURSO,P_TITULO,P_SECCION,P_ENROLADOS,P_CAMP_DESC,P_PTRM_CODE,P_ERROR); -- 381 380
    DBMS_OUTPUT.PUT_LINE(P_LIGA||' - '||P_LIGA_IDENT||' - '||P_LIGA_CONEC||' - '||P_NUM_CURSO||' - '||P_TITULO||' - '||P_SECCION||' - '||P_ENROLADOS||' - '|| P_CAMP_DESC ||' - '|| P_PTRM_CODE ||' - '||P_ERROR);
end;
*/

CREATE or replace PROCEDURE P_GET_VALIDACION (
      P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC             IN SSBSECT.SSBSECT_CRN%TYPE,
      P_LIGA            OUT NUMBER,
      P_LIGA_IDENT      OUT SSBSECT.SSBSECT_LINK_IDENT%TYPE,
      P_LIGA_CONEC      OUT SSRLINK.SSRLINK_LINK_CONN%TYPE,
      P_NUM_CURSO       OUT SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
      P_TITULO          OUT SCRSYLN.SCRSYLN_LONG_COURSE_TITLE%TYPE,
      P_SECCION         OUT SSBSECT.SSBSECT_SEQ_NUMB%TYPE,
      P_ENROLADOS       OUT SSBSECT.SSBSECT_ENRL%TYPE,
      P_CAMP_DESC       OUT STVCAMP.STVCAMP_DESC%TYPE,
      P_PTRM_CODE       OUT SSBSECT.SSBSECT_PTRM_CODE%TYPE,
      P_ERROR           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_VALIDACION
  FECHA     : 03/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida y obtiene los datos y calcula si NRC es Hijo,Padre o un NRC sin ligados.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
      V_INDICADOR       NUMBER;
      E_INVALID         EXCEPTION;
      V_SUBJ_CODE       SSBSECT.SSBSECT_SUBJ_CODE%TYPE;
      V_CAMP_CODE       SSBSECT.SSBSECT_CAMP_CODE%TYPE;
      
      V_SCRSYLN_REC     SCRSYLN%ROWTYPE;

      -- GET SCRSYLN -
      CURSOR C_SCRSYLN IS
      SELECT SCRSYLN_LONG_COURSE_TITLE 
      FROM (
        SELECT SCRSYLN_LONG_COURSE_TITLE FROM SCRSYLN 
        WHERE SCRSYLN_SUBJ_CODE = V_SUBJ_CODE 
        AND SCRSYLN_CRSE_NUMB = P_NUM_CURSO 
        AND SCRSYLN_TERM_CODE_END IS NULL 
        ORDER BY SCRSYLN_TERM_CODE_EFF DESC
      ) WHERE ROWNUM = 1;
BEGIN
    
      -- VALIDAR NRC CUMPLA CON LO NECESARIO (**)
      SELECT COUNT(*) INTO V_INDICADOR 
      FROM SSBSECT 
      WHERE SSBSECT_TERM_CODE = P_PERIODO 
      AND   SSBSECT_CRN = P_NRC
      AND   SSBSECT_MAX_ENRL = '0'   -- ** Maximo de vacante
      AND   SSBSECT_SSTS_CODE = 'P'; -- ** Estado 'P' por cerrar
      
      -- Get si NRC es: SIMPLE, PADRE, HIJO.
      SELECT COUNT(*) INTO P_LIGA FROM SSBSECT -- NRCs
      LEFT JOIN SSRLINK ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE AND SSRLINK_CRN = SSBSECT_CRN
      WHERE  SSBSECT_TERM_CODE = P_PERIODO AND SSBSECT_CRN = P_NRC;
      IF (P_LIGA > 0) THEN
          SELECT  
                CASE  WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSRLINK_LINK_CONN AND NVL(TRIM(SSBSECT_LINK_IDENT),'FALSE') <> 'FALSE' THEN 1 -- Indent <-> Conector (NRC HIJO)
                      WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSBSECT_LINK_IDENT AND NVL(TRIM(SSRLINK_LINK_CONN),'FALSE') <> 'FALSE' THEN 2 -- Seccion <-> Conector (NRC PADRE)
                      WHEN NVL(TRIM(SSRLINK_LINK_CONN),'0') = '0' AND NVL(TRIM(SSBSECT_LINK_IDENT),'0') = '0'             THEN 3 -- (NRC SIMPLE)
                      ELSE 0 END,
                SSBSECT_SUBJ_CODE,  -- Materia -->  Catalogo
                SSBSECT_CRSE_NUMB,  -- Numero Curso
                SSBSECT_SEQ_NUMB,   -- Seccion
                SSBSECT_LINK_IDENT, -- IDEN_LIGA -- IDENTIFICADOR LIGA
                SSRLINK_LINK_CONN,  -- CONE_LIGA
                SSBSECT_ENRL,       -- N Enrolados (Matriculados)
                SSBSECT_CAMP_CODE,  -- Campus
                SSBSECT_PTRM_CODE   -- Parte Periodo
          INTO  P_LIGA,
                V_SUBJ_CODE,
                P_NUM_CURSO,
                P_SECCION,
                P_LIGA_IDENT,
                P_LIGA_CONEC,
                P_ENROLADOS,
                V_CAMP_CODE,
                P_PTRM_CODE
          FROM SSBSECT -- NRCs
          LEFT JOIN SSRLINK -- CONECTOR LIGAS
            ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
            AND SSRLINK_CRN = SSBSECT_CRN
          WHERE  SSBSECT_TERM_CODE = P_PERIODO AND SSBSECT_CRN = P_NRC;
          
          -- GET nombre campus 
          SELECT STVCAMP_DESC INTO P_CAMP_DESC FROM STVCAMP WHERE STVCAMP_CODE = V_CAMP_CODE;
                                                      
          -- GET TITULO: Forma SCACRSE -> Opciones/Syllabus (SCASYLB) 
          OPEN C_SCRSYLN;
          LOOP
            FETCH C_SCRSYLN INTO P_TITULO;
            EXIT WHEN C_SCRSYLN%NOTFOUND;
            END LOOP;
          CLOSE C_SCRSYLN;
          P_TITULO := NVL(TRIM(P_TITULO),'');

      END IF;
      
      /*******************************************************************
      -- VALIDACION 
      ********************************************************************/
      IF (V_INDICADOR = 0 OR P_LIGA = 0 ) THEN
          P_LIGA      := 0;
          RAISE E_INVALID;
      END IF;
    
EXCEPTION
    WHEN E_INVALID THEN
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '|| 'No se logra identificar si el NRC tiene o no otros NRCs LIGADOS, favor de verificar la configuración del NRC.';
    WHEN OTHERS THEN
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_GET_VALIDACION;