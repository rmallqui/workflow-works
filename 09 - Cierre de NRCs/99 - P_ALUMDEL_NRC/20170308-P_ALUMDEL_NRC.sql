
CREATE OR REPLACE PROCEDURE P_ALUMDEL_NRC (
      P_PERIODO      IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC          IN SSBSECT.SSBSECT_CRN%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_ALUMDEL_NRC
  FECHA     : 08/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cambiar los estados de los alumnos inscritos al NRC. Estado DD(Curso Eliminado)
              el cual se puede visualizar en SFAREGS

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
BEGIN

    -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
    UPDATE SFRSTCR 
    SET SFRSTCR_RSTS_CODE = 'DD',           SFRSTCR_RSTS_DATE = SYSDATE,
        SFRSTCR_ERROR_FLAG = 'D',           SFRSTCR_BILL_HR = 0,
        SFRSTCR_WAIV_HR = 0,                SFRSTCR_CREDIT_HR = 0,
        SFRSTCR_ACTIVITY_DATE = SYSDATE,    SFRSTCR_USER = 'WorkFlow',
        SFRSTCR_ASSESS_ACTIVITY_DATE = SYSDATE      
    WHERE SFRSTCR_TERM_CODE = P_PERIODO  AND SFRSTCR_CRN = P_NRC
    AND (NVL(TRIM(SFRSTCR_RSTS_CODE),'NULL') <> 'DD' 
        OR NVL(TRIM(SFRSTCR_ERROR_FLAG),'NULL') <> 'D');
    
    -- UPDATE NRC Hijos CUMPLA CON LO NECESARIO CON REQUISITOS (**) VACANTE REAL = 0
    UPDATE SSBSECT
    SET SSBSECT_ENRL = 0
    WHERE SSBSECT_TERM_CODE = P_PERIODO
    AND   SSBSECT_CRN = P_NRC; 

END P_ALUMDEL_NRC;