
/*
SET SERVEROUTPUT ON
DECLARE   
     P_HTML1                 VARCHAR2(4000);
     P_HTML2                 VARCHAR2(4000);
     P_HTML3                 VARCHAR2(4000);
     P_HTML4                 VARCHAR2(4000);
     P_HTML5                 VARCHAR2(4000);
BEGIN
    P_ALUMDEL_NRC('201710','1199',NULL,P_HTML1,P_HTML2,P_HTML3,P_HTML4,P_HTML5);
    -- P_ALUMDEL_NRC('201710','1199'); no funciona...!!
    DBMS_OUTPUT.PUT_LINE(P_HTML1|| '--' ||P_HTML2|| '--' ||P_HTML3|| '--' ||P_HTML4|| '--' ||P_HTML5);
END;
*/


CREATE OR REPLACE PROCEDURE P_ALUMDEL_NRC (
      P_PERIODO      IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC          IN SSBSECT.SSBSECT_CRN%TYPE,
      P_NRCSUB       IN SSBSECT.SSBSECT_CRN%TYPE DEFAULT NULL,
      P_HTML1        OUT VARCHAR2,
      P_HTML2        OUT VARCHAR2,
      P_HTML3        OUT VARCHAR2,
      P_HTML4        OUT VARCHAR2,
      P_HTML5        OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_ALUMDEL_NRC
  FECHA     : 08/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cambiar los estados de los alumnos inscritos al NRC. Estado DD(Curso Eliminado)
              el cual se puede visualizar en SFAREGS

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
    V_NALUMNOS              NUMBER;
    V_ROWAFFECT             NUMBER;
    V_ID_ALUMNO             SPRIDEN.SPRIDEN_ID%TYPE;
    V_NAMES                 VARCHAR2(150);
    SAVE_ACT_DATE_OUT       VARCHAR2(100);
    RETURN_STATUS_IN_OUT    NUMBER;
    V_HTML_COUNT            NUMBER := 0;
    V_HTML                  VARCHAR2(4000);

    V_EMAIL                 GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
    V_EMAILCORP             GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;

    V_SFRSTCR_REC           SFRSTCR%ROWTYPE;

    -- ADD SSBSECT -
    CURSOR C_SFRSTCR IS
      SELECT * FROM SFRSTCR 
      WHERE SFRSTCR_TERM_CODE = P_PERIODO  
      AND SFRSTCR_CRN = P_NRC
      AND SFRSTCR_RSTS_CODE IN  ('RE','RW');
BEGIN
    
    P_HTML1 := '<tr></tr>';
    P_HTML2 := '<tr></tr>';
    P_HTML3 := '<tr></tr>';
    P_HTML4 := '<tr></tr>';
    P_HTML5 := '<tr></tr>';

    -- P_HTML1 := '<table class="GeneratedTable"><thead><tr><th>Periodo</th><th>NRC</th><th>ID</th><th>NOMBRES</th><th>EmailCorp</th><th>Email</th></tr></thead><tbody>';
    DBMS_OUTPUT.PUT_LINE( ' PASO 1 ');
    -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
    OPEN C_SFRSTCR;
    LOOP
        FETCH C_SFRSTCR INTO V_SFRSTCR_REC;
        IF C_SFRSTCR%FOUND THEN
            
            -- GET nombres , ID
            SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_ID INTO V_NAMES, V_ID_ALUMNO  FROM SPRIDEN 
            WHERE SPRIDEN_PIDM = V_SFRSTCR_REC.SFRSTCR_PIDM AND SPRIDEN_CHANGE_IND IS NULL;

            V_EMAIL     := F_GET_MAIL(V_SFRSTCR_REC.SFRSTCR_PIDM,FALSE);
            V_EMAILCORP := F_GET_MAIL(V_SFRSTCR_REC.SFRSTCR_PIDM,TRUE);
            
            DBMS_OUTPUT.PUT_LINE( V_EMAIL || ' --- ' || V_EMAILCORP);

            /***********************************************************************************************************
            -- Exportar reporte de alumnos desmaticulados y retornar los correos para la notificacion a los alumnos
            ***********************************************************************************************************/
            V_HTML := '<tr><td>' || P_PERIODO || '</td><td>' || P_NRC || '</td><td>' || V_ID_ALUMNO || '</td><td>' || V_NAMES || '</td><td>' || V_EMAILCORP || '</td><td>' || V_EMAIL || '</td></tr>';
            
            DBMS_OUTPUT.PUT_LINE(LENGTH(NVL(TRIM(P_HTML1),'-')) + LENGTH(V_HTML));
            
            IF    LENGTH(NVL(TRIM(P_HTML1),'-')) + LENGTH(V_HTML) < 4000 THEN
                      P_HTML1 := P_HTML1 || V_HTML;
            ELSIF LENGTH(NVL(TRIM(P_HTML2),'-')) + LENGTH(V_HTML) < 4000 THEN 
                      P_HTML2 := P_HTML2 || V_HTML;
            ELSIF LENGTH(NVL(TRIM(P_HTML3),'-')) + LENGTH(V_HTML) < 4000 THEN 
                      P_HTML3 := P_HTML3 || V_HTML;
            ELSIF LENGTH(NVL(TRIM(P_HTML4),'-')) + LENGTH(V_HTML) < 4000 THEN 
                      P_HTML4 := P_HTML4 || V_HTML;
            ELSIF LENGTH(NVL(TRIM(P_HTML5),'-')) + LENGTH(V_HTML) < 4000 THEN 
                      P_HTML5 := P_HTML5 || V_HTML;
            END IF;

            /******************************************************************************
            -- Desmatricular y Estimar deuda (BANNER)
            *******************************************************************************/
            UPDATE SFRSTCR 
            SET SFRSTCR_RSTS_CODE = 'DD',           SFRSTCR_RSTS_DATE = SYSDATE,
                SFRSTCR_ERROR_FLAG = 'D',           SFRSTCR_BILL_HR = 0,
                SFRSTCR_WAIV_HR = 0,                SFRSTCR_CREDIT_HR = 0,
                SFRSTCR_ACTIVITY_DATE = SYSDATE,    SFRSTCR_USER = 'WorkFlow',
                SFRSTCR_ASSESS_ACTIVITY_DATE = SYSDATE      
            WHERE SFRSTCR_TERM_CODE = V_SFRSTCR_REC.SFRSTCR_TERM_CODE
            AND SFRSTCR_CRN IN (V_SFRSTCR_REC.SFRSTCR_CRN , P_NRCSUB)
            AND SFRSTCR_PIDM = V_SFRSTCR_REC.SFRSTCR_PIDM
            AND SFRSTCR_RSTS_CODE IN  ('RE','RW'); -- Estados válidos (SFQRSTS)
            V_ROWAFFECT := SQL%ROWCOUNT;

            IF (V_ROWAFFECT > 0) THEN
                  -- Get ID del alumno
                  SELECT SPRIDEN_ID INTO V_ID_ALUMNO 
                  FROM SPRIDEN WHERE SPRIDEN_PIDM = V_SFRSTCR_REC.SFRSTCR_PIDM 
                  AND SPRIDEN_CHANGE_IND IS NULL;

                   -- #######################################################################
                  -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
                  SFKFEES.p_processfeeassessment (  V_SFRSTCR_REC.SFRSTCR_TERM_CODE,
                                                    V_SFRSTCR_REC.SFRSTCR_PIDM,
                                                    SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                                    SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                                    'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                                    'Y',          -- create TBRACCD records
                                                    'SFAREGS',    -- where assessment originated from
                                                    'Y',          -- commit changes
                                                    SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                                    'N',          -- do not ignore SFRFMAX rules
                                                    RETURN_STATUS_IN_OUT );   -- OUT -- return_status
                  ------------------------------------------------------------------------------  
                  -- -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
                  -- TZJAPOL.p_run_proc_tvrappl(V_ID_ALUMNO);
                  ------------------------------------------------------------------------------  
            END IF;
            
        ELSE  
            
            /******************************************************************************************/
            -- CASO LIGA 1 (NRC HIJO): Actualizar los matriculados y vacantes restasnter del NRC PADRE
            /******************************************************************************************/

            -- CALCULAR Numero de matriculados válidos (SSBSECT_ENRL) en el NRC PADRE 
            SELECT COUNT(*) INTO V_NALUMNOS 
            FROM SFRSTCR
            WHERE SFRSTCR_CRN = P_NRCSUB
            AND SFRSTCR_RSTS_CODE IN  ('RE','RW');

            -- UPDATE (SSBSECT_ENRL) actualiza los datos del numero matriculados en el NRC PADRE 
            UPDATE SSBSECT
            SET   SSBSECT_ENRL        = V_NALUMNOS, -- # matriculados válidos en el nrc
                  SSBSECT_SEATS_AVAIL = SSBSECT_MAX_ENRL - V_NALUMNOS, -- # vacantes restantes
                  SSBSECT_CENSUS_ENRL = CASE WHEN SYSDATE < SSBSECT_CENSUS_ENRL_DATE THEN V_NALUMNOS ELSE SSBSECT_CENSUS_ENRL END, -- # de matriculados antes de una fecha SSBSECT_CENSUS_ENRL_DATE
                  SSBSECT_ACTIVITY_DATE = SYSDATE,
                  SSBSECT_DATA_ORIGIN   = 'WorkFlow',
                  SSBSECT_USER_ID       = USER
            WHERE SSBSECT_TERM_CODE = P_PERIODO
            AND   SSBSECT_CRN = P_NRCSUB; 
            
            EXIT;

        END IF;
    END LOOP;
    CLOSE C_SFRSTCR;
    
    -- UPDATE NRC Hijos CUMPLA CON LO NECESARIO CON REQUISITOS (**) VACANTE REAL = 0
    UPDATE SSBSECT
    SET   SSBSECT_MAX_ENRL    = 0, -- # vacantes
          SSBSECT_ENRL        = 0, -- # matriculados validos en el nrc
          SSBSECT_SEATS_AVAIL = 0, -- # vacantes restantes
          SSBSECT_CENSUS_ENRL = CASE WHEN SYSDATE < SSBSECT_CENSUS_ENRL_DATE THEN 0 ELSE SSBSECT_CENSUS_ENRL END, -- # de matriculados antes de una fecha SSBSECT_CENSUS_ENRL_DATE
          SSBSECT_ACTIVITY_DATE = SYSDATE,
          SSBSECT_DATA_ORIGIN   = 'WorkFlow',
          SSBSECT_USER_ID       = USER
    WHERE SSBSECT_TERM_CODE = P_PERIODO
    AND   SSBSECT_CRN = P_NRC; 
    
    COMMIT;
    
END P_ALUMDEL_NRC;