/**********************************************************************************************/
/* BWZKCNRC.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso Cierre de  */
/*                    NRC.                                                                    */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        RML             21/MAR/2017 */
/*    --------------------                                                                    */
/*    Creación del paquete de Cierre de NCR.                                                  */
/*    Procedure P_GET_VALIDACION: Valida y obtiene los datos y calcula si NRC es Hijo,Padre o */
/*                                un NRC sin ligados.                                         */
/*    Procedure P_DESMATRICULA_NRC: Lista y prepara los NRCs para desmaticular alumnos.       */
/*    Procedure P_NRC_CLOSE: Retira los horarios y docentes asignados a el(los) NRC(s) para   */
/*                           posteriormente CERRARLOS.                                        */
/*    Procedure P_ALUMDEL_NRC: Cambiar los estados de los alumnos inscritos al NRC. Estado    */
/*                             DD(Curso Eliminado)el cual se puede visualizar en SFAREGS.     */
/*    Procedure P_NRC_CANCEL: Restaura el ESTADO y # de VACANTES del NRC al a uno anterior de */
/*                            solicitar su cierre.                                            */
/*    Function F_GET_MAIL: Obtiene el correo corporativo o alternativo del alumno por PIDM.   */
/*    Function F_GET_PHONE: Segun el PIDM obtiene el TELEFONO que se encuentra en el orden    */
/*                          "P_N_ORDER" indicado.                                             */
/*    P_VERIFICAR_STATUS_NRC                                      LAM             20/NOV/2018 */
/*    Verifica si el el estado del NRC sigue como "POR CERRAR", para continuar el proceso.    */
/*                                                                                            */
/* 2. Actuslización WFK_CONTISCNRC                                LAM             22/ENE/2018 */
/*    Se renombra el paquete de WFK_CONTISCNRC.sql y WFK_CONTISCNRC BODY.sql a BWZKCNRC.sql y */
/*    BWZKCNRC BODY.sql, de acuerdo a la nomenclatura compartida.                             */
/* 3. Actualización del P_GET_VALIDACION                          LAM             20/NOV/2018 */
/*    Se agrega los parámetros P_USERID, P_USER_EMAIL, P_DATE, P_TERM_STATUS y P_ERROR para   */
/*    identificar el usuario de cierre de NRC y validar si el periodo académico del NRC es    */
/*    apto para el cierre.                                                                    */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BWZKCNRC AS

PROCEDURE P_GET_VALIDACION (
          P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
          P_NRC             IN SSBSECT.SSBSECT_CRN%TYPE,
          P_LIGA            OUT NUMBER,
          P_LIGA_IDENT      OUT SSBSECT.SSBSECT_LINK_IDENT%TYPE,
          P_LIGA_CONEC      OUT SSRLINK.SSRLINK_LINK_CONN%TYPE,
          P_NUM_CURSO       OUT SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
          P_COD_ASIGN       OUT VARCHAR2,
          P_TITULO          OUT SCRSYLN.SCRSYLN_LONG_COURSE_TITLE%TYPE,
          P_SECCION         OUT SSBSECT.SSBSECT_SEQ_NUMB%TYPE,
          P_ENROLADOS       OUT SSBSECT.SSBSECT_ENRL%TYPE,
          P_CAMP_DESC       OUT STVCAMP.STVCAMP_DESC%TYPE,
          P_PTRM_CODE       OUT SSBSECT.SSBSECT_PTRM_CODE%TYPE,
          P_GTVINSM_DESC    OUT GTVINSM.GTVINSM_DESC%TYPE,
          P_INSTRUC_ID      OUT SPRIDEN.SPRIDEN_ID%TYPE,
          P_INSTRUC_NAME    OUT VARCHAR2,
          P_USERID          OUT VARCHAR2,
          P_USER_EMAIL      OUT VARCHAR2,
          P_DATE            OUT VARCHAR2,
          P_TERM_STATUS     OUT VARCHAR2,
          P_ERROR           OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_DESMATRICULA_NRC (
          P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
          P_NRC_IN          IN SSBSECT.SSBSECT_CRN%TYPE,
          P_LIGA            IN NUMBER,
          P_LIGA_IDENT      IN SSBSECT.SSBSECT_LINK_IDENT%TYPE,
          P_LIGA_CONEC      IN SSRLINK.SSRLINK_LINK_CONN%TYPE,
          P_NUM_CURSO       IN SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
          P_HTML1           OUT VARCHAR2,
          P_HTML2           OUT VARCHAR2,
          P_HTML3           OUT VARCHAR2,
          P_HTML4           OUT VARCHAR2,
          P_HTML5           OUT VARCHAR2,
          P_MAIL_STDN       OUT VARCHAR2,
          P_ERROR           OUT VARCHAR2
    );
              
--------------------------------------------------------------------------------

PROCEDURE P_NRC_CLOSE (
          P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
          P_NRC_IN          IN SSBSECT.SSBSECT_CRN%TYPE,
          P_LIGA            IN NUMBER,
          P_LIGA_IDENT      IN SSBSECT.SSBSECT_LINK_IDENT%TYPE,
          P_LIGA_CONEC      IN SSRLINK.SSRLINK_LINK_CONN%TYPE,
          P_NUM_CURSO       IN SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
          P_ERROR           OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_ALUMDEL_NRC (
          P_PERIODO      IN STVTERM.STVTERM_CODE%TYPE,
          P_NRC          IN SSBSECT.SSBSECT_CRN%TYPE,
          P_NRCSUB       IN SSBSECT.SSBSECT_CRN%TYPE DEFAULT NULL,
          P_HTML1        OUT VARCHAR2,
          P_HTML2        OUT VARCHAR2,
          P_HTML3        OUT VARCHAR2,
          P_HTML4        OUT VARCHAR2,
          P_HTML5        OUT VARCHAR2,
          P_MAIL_STDN    OUT VARCHAR2
    );
--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_STATUS_NRC (
          P_PERIODO          IN STVTERM.STVTERM_CODE%TYPE,
          P_NRC              IN SSBSECT.SSBSECT_CRN%TYPE,
          P_STATUS_NRC_DESC  OUT VARCHAR2,
          P_STATUS           OUT VARCHAR2,
          P_USERCHANGE       OUT VARCHAR2,
          P_DATECHANGE       OUT VARCHAR2
);
--------------------------------------------------------------------------------

PROCEDURE P_NRC_CANCEL (
        P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
        P_NRC             IN SSBSECT.SSBSECT_CRN%TYPE,
        P_OLD_SSTS_CODE   IN SSBSECT.SSBSECT_SSTS_CODE%TYPE,
        P_MAXENROL        IN SSBSECT.SSBSECT_MAX_ENRL%TYPE
    );

--------------------------------------------------------------------------------

FUNCTION  F_GET_MAIL (
            P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_MAILCORP        BOOLEAN          
      )RETURN VARCHAR2;

--------------------------------------------------------------------------------

FUNCTION  F_GET_PHONE (
          P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_N_ORDER         NUMBER
      ) RETURN VARCHAR2;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKCNRC;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKCNRC;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKCNRC FOR BANINST1.BWZKCNRC;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKCNRC
--  START gurgrth BWZKCNRC
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/
