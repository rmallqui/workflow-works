/**********************************************************************************************/
/* SZVSMCI.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripci�n corta: Script para generar la vista SZVSMCI                                    */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creaci�n del C�digo.                                        LAM           20/FEB/2018   */
/*    --------------------                                                                    */
/*    Se crea la vista SZVSMCI para elegir los cr�ditos aptos a inscripci�n para el ciclo     */
/*    verano entre el rango de 1 a 3 cr�ditos para el UREG, 1 a 5 para UPGT/UVIR.             */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/ 

  CREATE OR REPLACE VIEW "BANINST1"."SZVSMCI" (CONFIRM_CRED_ID, CONFIRM_CRED_DESC) AS 
  WITH cteSZVSMCI AS (		
			SELECT 'UREG' SZVSMCI_DEPT_CODE, 1 SZVSMCI_CRED_ID, '1 cr�dito' SZVSMCI_CRED_DESC FROM DUAL UNION
            SELECT 'UREG', 2, '2 cr�ditos' FROM DUAL UNION
            SELECT 'UREG', 3, '3 cr�ditos' FROM DUAL UNION
            SELECT 'UPGT', 1, '1 cr�dito' FROM DUAL UNION
            SELECT 'UPGT', 2, '2 cr�ditos' FROM DUAL UNION
            SELECT 'UPGT', 3, '3 cr�ditos' FROM DUAL UNION
            SELECT 'UPGT', 4, '4 cr�ditos' FROM DUAL UNION
            SELECT 'UPGT', 5, '5 cr�ditos' FROM DUAL UNION
			SELECT 'UVIR', 1, '1 cr�dito' FROM DUAL UNION
            SELECT 'UVIR', 2, '2 cr�ditos' FROM DUAL UNION
            SELECT 'UVIR', 3, '3 cr�ditos' FROM DUAL UNION
            SELECT 'UVIR', 4, '4 cr�ditos' FROM DUAL UNION
            SELECT 'UVIR', 5, '5 cr�ditos' FROM DUAL)
		SELECT SZVSMCI_CRED_ID, SZVSMCI_CRED_DESC
			FROM cteSZVSMCI
			WHERE SZVSMCI_DEPT_CODE = (SELECT SORLFOS_DEPT_CODE
											FROM (SELECT SORLFOS_DEPT_CODE
													FROM SORLCUR        
									            INNER JOIN SORLFOS
									                ON SORLCUR_PIDM = SORLFOS_PIDM 
									                    AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
									            WHERE SORLCUR_PIDM = BVSKOSAJ.F_GetParamValue('SOL007',999)
									                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
									                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
									                  AND SORLCUR_CURRENT_CDE =   'Y'
									            ORDER BY SORLCUR_TERM_CODE DESC,SORLCUR_SEQNO DESC)
											WHERE ROWNUM <= 1);