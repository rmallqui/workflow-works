NOTAS
=============================================================
=============================================================
-- buscar objecto
select dbms_metadata.get_ddl('TRIGGER','TG_SPRIDEN_DESPUES_INSERT') from dual;

    -- tipos de servicios definidos por el usuario
    select SVVSRCA_CODE,SVVSRCA.* from SVVSRCA;

    -- SVVSRCA ::::::::  VALIDACION CODIGOS SERVICIO - 
    select SVVSRVC_CODE,SVVSRVC_SRCA_CODE,SVVSRVC.* from SVVSRVC;

    -- SVVSRVS :::::::::: los estados que un servicio puede adquirir 
    SELECT SVVSRVS_CODE,SVVSRVS.* FROM SVVSRVS;
      
    -- SVASVPR : :::::::: tabla SVRSVPR ; VISTA  SVRSVPR1
    SELECT SVRSVPR_PROTOCOL_SEQ_NO,SVRSVPR_SRVC_CODE,SVRSVPR_SRVS_CODE,SVRSVPR_TERM_CODE,
    SVRSVPR_WSSO_CODE,SVRSVPR_ORIG_CODE,SVRSVPR_CHNL_CODE,SVRSVPR_RQST_CODE,
    SVRSVPR_CAMP_CODE,SVRSVPR.* FROM SVRSVPR;

    -- SVVCHNL :::::::::: permite definir los canales de asistencia
    select SVVCHNL_CODE,* from SVVCHNL;

    -- SVVRQST :::::::::::::::: permite definir los tipos de solicitud
    SELECT SVVRQST_CODE,SVVRQST.* FROM SVVRQST;

    -- STVWSSO :::::::::: configurar los métodos de entrega 
        -- no se usa "CARGO" ni "EMITIDO A" , STVWSSO_CHRG,STVWSSO_ISSUE_TO_COMMENT
    SELECT STVWSSO_CODE,STVWSSO.* FROM STVWSSO;

    -- SVVSRCT :::::::: configurar los valores predefinidos para los distintos 
    --                  atributos de la solicitud de servicios
    SELECT SVVSRCT_CODE,SVVSRCT.* FROM SVVSRCT;


    -- SVARSRV ::::::::::. configurar las reglas de negocio de cada uno de los servicios definidos
    --                      ; COMO REGLAS PARA SOL003
    SELECT * FROM SVRRSRV;  -- tiene multiples tablas enlazadas, pero esta contiene la configuraciòn total.;
    SELECT * FROM SVRRSST;  -- subformulario de reglas -- estados.


    -- SVARSCM ::::::::::: permite asociar un servicio con un campus (STVCAMP) y un centro de costos


    -- SVASVPR ::::::::::: despues de generarse un solcitud puede visualizarse con la forma “Administración de
    --                      Solicitud de Servicios” (SVASVPR).


=============================================================
=============================================================

SELECT * FROM SVRSRAD;

--===============================
select * from SPRIDEN
 where SPRIDEN_PIDM = '30539';
 
 
 
 
 
-----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
--====================================================================================
--====================================================================================
--====================================================================================
--====================================================================================
--====================================================================================
-- tipos de servicios definidos por el usuario
select SVVSRCA_CODE,SVVSRCA.* from SVVSRCA;
-- SVVSRCA ::::::::  VALIDACION CODIGOS SERVICIO - 
select SVVSRVC_CODE,SVVSRVC_SRCA_CODE,SVVSRVC.* from SVVSRVC;
-- SVASVPR : :::::::: tabla SVRSVPR ; VISTA  SVRSVPR1
SELECT SVRSVPR_PROTOCOL_SEQ_NO,SVRSVPR_SRVC_CODE,SVRSVPR_SRVS_CODE,SVRSVPR_TERM_CODE,
SVRSVPR_WSSO_CODE,SVRSVPR_ORIG_CODE,SVRSVPR_CHNL_CODE,SVRSVPR_RQST_CODE,
SVRSVPR_CAMP_CODE,SVRSVPR.* FROM SVRSVPR;

SELECT 
    SVVSRVC_CODE,SVVSRCA_CODE
FROM  SVVSRVC
INNER JOIN SVVSRCA
ON SVVSRVC_SRCA_CODE = SVVSRCA_CODE
WHERE SVVSRCA_CODE = 'SOLICITUDES'
AND SVVSRVC_CODE = 'SOL003' ;



-- Estados de la solicitud disponibles segun las reglas y configuracion
  SELECT SVRRSRV_SEQ_NO, 
        SVRRSRV_SRVC_CODE       TEMP_SRVC_CODE, 
        SVRRSRV_INACTIVE_IND,
        SVRRSST_RSRV_SEQ_NO , 
        SVRRSST_SRVC_CODE , 
        SVRRSST_SRVS_CODE       TEMP_SRVS_CODE,
        SVRRSRV_START_DATE      TEMP_START_DATE,--- fecha finalizacion
        SVRRSRV_END_DATE        TEMP_END_DATE,----- fecha finalizacion
        SVRRSRV_DELIVERY_DATE , -- fecha estimada
        SVVSRVS_LOCKED
 FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
 INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
 ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
 AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
 INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
 ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
 WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
 AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
 AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 




SELECT 
--    SVRSVPR_PROTOCOL_SEQ_NO,SVRSVPR_SRVC_CODE,SVRSVPR_SRVS_CODE,SVRSVPR_TERM_CODE,
--    SVRSVPR_WSSO_CODE,SVRSVPR_ORIG_CODE,SVRSVPR_CHNL_CODE,SVRSVPR_RQST_CODE,
--    SVRSVPR_CAMP_CODE
     SVRSVPR.*
FROM SVRSVPR
INNER JOIN 
  (
      SELECT 
          SVVSRVC_CODE TEMP_SVVSRVC_CODE,
          SVVSRCA_CODE TEMP_SVVSRCA_CODE
      FROM  SVVSRVC
      INNER JOIN SVVSRCA
      ON SVVSRVC_SRCA_CODE = SVVSRCA_CODE
      WHERE SVVSRCA_CODE = 'SOLICITUDES'
      AND SVVSRVC_CODE = 'SOL003' 
   ) TEMP
ON SVRSVPR_SRVC_CODE = TEMP_SVVSRVC_CODE
WHERE SVRSVPR_SRVS_CODE IN (
      SELECT SVVSRVS_CODE FROM SVVSRVS
      WHERE SVVSRVS_LOCKED <> 'l'
      AND SVVSRVS_CODE IN ('AC','PE') 
      AND SVRSVPR_ESTIMATED_DATE >= SYSDATE
  );

SELECT SYSDATE FROM DUAL;








SET SERVEROUTPUT ON
DECLARE P_ROW_FOUND varchar2(5);
BEGIN
      
      SELECT DECODE(COUNT(*),0,'FALSE','TRUE')          
      --INTO P_ROW_FOUND
      FROM SVRSVPR
      INNER JOIN 
        (
            SELECT 
                SVVSRVC_CODE TEMP_SVVSRVC_CODE,
                SVVSRCA_CODE TEMP_SVVSRCA_CODE
            FROM  SVVSRVC
            INNER JOIN SVVSRCA
            ON SVVSRVC_SRCA_CODE = SVVSRCA_CODE
            WHERE SVVSRCA_CODE = 'SOLICITUDES'
            AND SVVSRVC_CODE = 'SOL003' 
         ) TEMP
      ON SVRSVPR_SRVC_CODE = TEMP_SVVSRVC_CODE
      WHERE SVRSVPR_SRVS_CODE IN (
            SELECT SVVSRVS_CODE FROM SVVSRVS
            WHERE SVVSRVS_LOCKED <> 'L'
            AND SVVSRVS_CODE IN ('AC','PE') -- Acitvo , En Proceso
        );
END;