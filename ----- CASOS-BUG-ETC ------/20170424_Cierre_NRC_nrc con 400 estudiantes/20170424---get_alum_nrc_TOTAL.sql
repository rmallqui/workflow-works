
CREATE GLOBAL TEMPORARY TABLE my_temp_table (
  INDICE         NUMBER(8,0),
  PIDM           NUMBER(8,0),
  ID_ALUM  VARCHAR2(150),
  NOMBRES VARCHAR2(250),
  EMAIL_CORP VARCHAR2(150),
  PHOEN1 VARCHAR2(150),
  PHOEN2 VARCHAR2(150),
  PHOEN3 VARCHAR2(150),
  RSTS_CODE VARCHAR2(2),
  ACTIVITY_DATE DATE
)
ON COMMIT DELETE ROWS;

SET SERVEROUTPUT ON;
DECLARE 
      P_PERIODO      STVTERM.STVTERM_CODE%TYPE :='201710';
      P_NRC          SSBSECT.SSBSECT_CRN%TYPE := 5401;
      P_NRCSUB       SSBSECT.SSBSECT_CRN%TYPE DEFAULT NULL;
      P_MAIL_STDN    VARCHAR2(4000);

    V_NALUMNOS              NUMBER;
    V_ROWAFFECT             NUMBER;
    V_ID_ALUMNO             SPRIDEN.SPRIDEN_ID%TYPE;
    V_NAMES                 VARCHAR2(250);
    SAVE_ACT_DATE_OUT       VARCHAR2(100);
    RETURN_STATUS_IN_OUT    NUMBER;
    V_HTML_COUNT            NUMBER := 0;
    V_HTML                  VARCHAR2(4000);
    V_INDICE                NUMBER := 0;
    V_PHONE1                VARCHAR(49);
    V_PHONE2                VARCHAR(49);
    V_PHONE3                VARCHAR(49);

    V_EMAIL                 GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
    V_EMAILCORP             GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;

    V_SFRSTCR_REC           SFRSTCR%ROWTYPE;

    -- ADD SSBSECT -
    CURSOR C_SFRSTCR IS
      SELECT * FROM SFRSTCR 
      WHERE SFRSTCR_TERM_CODE = P_PERIODO  
      AND SFRSTCR_CRN = P_NRC;
BEGIN

    -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
    OPEN C_SFRSTCR;
    LOOP
        FETCH C_SFRSTCR INTO V_SFRSTCR_REC;
        IF C_SFRSTCR%FOUND THEN
            
            -- GET nombres , ID
            SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_ID INTO V_NAMES, V_ID_ALUMNO  FROM SPRIDEN 
            WHERE SPRIDEN_PIDM = V_SFRSTCR_REC.SFRSTCR_PIDM AND SPRIDEN_CHANGE_IND IS NULL;

            --V_EMAIL     := F_GET_MAIL(V_SFRSTCR_REC.SFRSTCR_PIDM,FALSE);
            V_EMAILCORP := WFK_CONTISCNRC.F_GET_MAIL(V_SFRSTCR_REC.SFRSTCR_PIDM,TRUE);
            V_PHONE1    := WFK_CONTISCNRC.F_GET_PHONE(V_SFRSTCR_REC.SFRSTCR_PIDM,1);
            V_PHONE2    := WFK_CONTISCNRC.F_GET_PHONE(V_SFRSTCR_REC.SFRSTCR_PIDM,2);
            V_PHONE3    := WFK_CONTISCNRC.F_GET_PHONE(V_SFRSTCR_REC.SFRSTCR_PIDM,3);
            V_INDICE    := V_INDICE + 1;
--            IF NVL(TRIM(V_EMAILCORP),'-') <> '-' THEN 
--                  P_MAIL_STDN := CASE WHEN V_INDICE = 1 THEN (P_MAIL_STDN || V_EMAILCORP) ELSE (P_MAIL_STDN || ',' || V_EMAILCORP) END;
--            END IF;            
            
            /***********************************************************************************************************
            -- Exportar reporte de alumnos desmaticulados y retornar los correos para la notificacion a los alumnos
            ***********************************************************************************************************/
            INSERT INTO my_temp_table VALUES (V_INDICE, V_SFRSTCR_REC.SFRSTCR_PIDM, V_ID_ALUMNO,V_NAMES,V_EMAILCORP,V_PHONE1,V_PHONE2,V_PHONE3,V_SFRSTCR_REC.SFRSTCR_RSTS_CODE,V_SFRSTCR_REC.SFRSTCR_ACTIVITY_DATE);
            
        ELSE  
            
            /******************************************************************************************/
            -- CASO LIGA 1 (NRC HIJO): Actualizar los matriculados y vacantes restasnter del NRC PADRE
            /******************************************************************************************/

            -- CALCULAR Numero de matriculados válidos (SSBSECT_ENRL) en el NRC PADRE 
--            SELECT COUNT(*) INTO V_NALUMNOS 
--            FROM SFRSTCR
--            WHERE SFRSTCR_CRN = P_NRCSUB;
            
            EXIT;

        END IF;
    END LOOP;
    CLOSE C_SFRSTCR;

END;


SELECT * FROM my_temp_table;
DROP TABLE my_temp_table PURGE;
