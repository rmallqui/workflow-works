
USE [BDUCCI]
GO
/* ===================================================================================================================
NOMBRE    : -
FECHA   : 08/08/2017
AUTOR   : Richard Mallqui Lopez 
OBJETIVO  : Correción de los registros de la SeccionC "DTA" generados con un IDEscuela que no corresponde.
        - Actualziar la Escuela a "OTR" para la seccionc "DTA" periodo 2017-2 (dbo.tblalumnoEstado y dbo.tblctacorriente)

MODIFICACIONES
NRO   FECHA   USUARIO   MODIFICACION
=================================================================================================================== */
BEGIN 
----- 
  
  SET NOCOUNT ON;
  SET FMTONLY OFF
  BEGIN TRY
    BEGIN TRANSACTION
      
      UPDATE dbo.tblCtaCorriente
                SET IDEscuela = 'OTR'
            WHERE IDDependencia = 'UCCI' 
            AND IDSede      = 'HYO' 
            AND IDSeccionC  = 'DTA'
            AND IDPerAcad   = '2017-2'
            AND FecInic     = '01/07/2017';
    
      UPDATE dbo.tblAlumnoEstado
              SET IDEscuela = 'OTR' 
            WHERE IDDependencia = 'UCCI' 
            AND IDSede      = 'HYO' 
            AND IDSeccionC  = 'DTA'
            AND IDPerAcad   = '2017-2'
            AND FecInic     = '01/07/2017';

      COMMIT TRANSACTION

  END TRY
  BEGIN CATCH

      --INSTRUCCIONES EN CASO DE ERRORES
      DECLARE @ErrorMessage NVARCHAR(4000);  
      DECLARE @ErrorSeverity INT;  
      DECLARE @ErrorState INT;  

      SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  

      -- Use RAISERROR inside the CATCH block to return error  
      -- information about the original error that caused  
      -- execution to jump to the CATCH block.  
      RAISERROR (
        @ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState -- State.  
        );  
    
      ROLLBACK TRANSACTION

  END CATCH
END