/*
-- buscar objecto
select dbms_metadata.get_ddl('TRIGGER','TG_SPRIDEN_DESPUES_INSERT') from dual

-- Desabilitar - disable trigger
ALTER TRIGGER TG_SVRSVPR_DESPUES_INSERT ENABLE;
DROP TRIGGER TG_SVRSVPR_DESPUES_INSERT;

select * from spriden where spriden_id = '70073244'; --325180
*/


CREATE OR REPLACE TRIGGER TG_SVRSVPR_DESPUES_INSERT
BEFORE INSERT
   ON "SATURN"."SVRSVPR"
   FOR EACH ROW
/* ===================================================================================================================
  NOMBRE    : TG_SVASVPR_DESPUES_INSERT
  FECHA     : 06/10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Generar un evento desde la creación de una nueva solicitud del tipo SOL003.

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION
  001     06/12/2016    RMALLQUI      Modificaciòn "CAMBIO DE PLAN" - Se obtendra el periodo del registro "ELEGIBLE" y 
                                      ya no del ultimo periodo matriculado(NRC). Solicitado por Mirian Flores.                                      
  =================================================================================================================== */
DECLARE
      P_PIDM_ALUMNO             SPRIDEN.SPRIDEN_ID%TYPE;
      P_ID_ALUMNO               SPRIDEN.SPRIDEN_ID%TYPE;
      P_PERIODO                 SFBETRM.SFBETRM_TERM_CODE%TYPE;
      P_CORREO_ALUMNO           GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
      P_NOMBRE_ALUMNO           VARCHAR2(180); -- SPRIDEN.SPRIDEN_LAST_NAME, SPRIDEN.SPRIDEN_FIRST_NAME
      P_FOLIO_SOLICITUD         SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE;
      P_FECHA_SOLICITUD         SVRSVPR.SVRSVPR_ACTIVITY_DATE%TYPE;
      P_COMENTARIO_ALUMNO       SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE;
      P_MODALIDAD_ALUMNO        SGBSTDN.SGBSTDN_DEPT_CODE%TYPE; -- Departamento
      P_COD_SEDE                STVCAMP.STVCAMP_CODE%TYPE;  -- SGBSTDN.SGBSTDN_CAMP_CODE%TYPE;
      P_NOMBRE_SEDE             STVCAMP.STVCAMP_DESC%TYPE;
      
      P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
      
      P_SRVC_CODE               SVRSVPR.SVRSVPR_SRVC_CODE%TYPE;
      P_INDICADOR               NUMBER;
      P_MESSAGE                 EXCEPTION;
      
      V_PARAMS          GOKPARM.T_PARAMETERLIST;
      EVENT_CODE        GTVEQNM.GTVEQNM_CODE%TYPE;
BEGIN
-- 
      -- -- -- -- GET CODIGO SERVICIO -- -- -- --
      P_SRVC_CODE            :=      :NEW.SVRSVPR_SRVC_CODE;
      
      P_PIDM_ALUMNO          :=      :NEW.SVRSVPR_PIDM;
      P_FOLIO_SOLICITUD      :=      :NEW.SVRSVPR_PROTOCOL_SEQ_NO;
      P_FECHA_SOLICITUD      :=      :NEW.SVRSVPR_ACTIVITY_DATE;
      -- P_COMENTARIO_ALUMNO    :=      :NEW.SVRSVPR_STU_COMMENT;    -- Comentario por defecto de solicitudes
      
      
      -- GET nombres , ID
      SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_ID INTO P_NOMBRE_ALUMNO, P_ID_ALUMNO  FROM SPRIDEN 
      WHERE SPRIDEN_PIDM = P_PIDM_ALUMNO AND SPRIDEN_CHANGE_IND IS NULL;
     
     
      -- GET email
      SELECT COUNT(*) INTO P_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A';
      IF P_INDICADOR = 0 THEN
           P_CORREO_ALUMNO := '';
      ELSE
          -----------------------
              SELECT COUNT(*) INTO P_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A' 
              AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe');            
              IF (P_INDICADOR > 0) THEN
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_CORREO_ALUMNO FROM (
                    SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL 
                    WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A' AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                    ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              ELSE
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_CORREO_ALUMNO FROM (
                      SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL
                      WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A'
                      ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              END IF;
      END IF;
      
      
      -- GET email   : Comentario Configurado como DATO ADICIONAL
      -- SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO FROM SVRSVAD
      -- WHERE SVRSVAD_PROTOCOL_SEQ_NO = :NEW.SVRSVPR_PROTOCOL_SEQ_NO AND SVRSVAD_ADDL_DATA_SEQ = 2;
      P_COMENTARIO_ALUMNO := '--';
      
      
      ----------------------- GET DATOS SEGUN TIPO DE SOLICITUD ---------------
            -- Codigo evento
            -- Periodo
            -- Departamente
            -- Sede
     ---------------------------------------------------------------------------
      IF P_SRVC_CODE = 'SOL003' THEN --########### RECTIF. REZAGADOS ###########
     ---------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL003');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_SEQNO DESC 
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE DESC
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación - “Nombre Alumno” - “ID Alumno” - “folio_solicitud”
                  v_Params(3).param_value := 'Solicitud de Rectificación de Matrìcula REZAGADOS  Nº '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO ;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL004' THEN --########### CAMBIO DE PLAN ###########
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL004');
              

              -- GET DEPARTAMENTO y CAMPUS 
              SELECT COUNT(*) INTO P_INDICADOR FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO;
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno segun el periodo
                    SELECT SGBSTDN_DEPT_CODE, SGBSTDN_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE FROM(
                          SELECT  SGBSTDN_TERM_CODE_EFF, 
                                  SGBSTDN_DEPT_CODE, 
                                  SGBSTDN_CAMP_CODE
                                  --SGBSTDN_STST_CODE   -- STVSTST : validaciones para matricula y otro
                          FROM SGBSTDN 
                          WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO 
                          ORDER BY SGBSTDN_TERM_CODE_EFF DESC  
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
                  /*-- GET COD PARTE-PERIODO activo ò un PERIODO proximo valido para realizar la solicitud (no "00")*/
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
              
                    -- GET PERIODO ACTIVO 
                    SELECT SOBPTRM_TERM_CODE INTO P_PERIODO
                    FROM (
                        -- GET COD PARTE-PERIODO activo ò un PERIODO proximo valido para realizar el cambio de plan (no "00")
                        SELECT SOBPTRM_TERM_CODE FROM SOBPTRM
                        WHERE  SOBPTRM_PTRM_CODE LIKE ('' || P_PART_PERIODO || '%')
                        --AND SYSDATE BETWEEN SOBPTRM_START_DATE AND SOBPTRM_END_DATE
                        AND SYSDATE <= SOBPTRM_START_DATE
                        AND SOBPTRM_TERM_CODE NOT LIKE ('%00') --- descartar periodos "____00"
                        ORDER BY SOBPTRM_TERM_CODE ASC
                    ) WHERE ROWNUM <= 1;
                    
                    :NEW.SVRSVPR_TERM_CODE := P_PERIODO;

              END IF;
              
              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación - “Nombre Alumno” - “ID Alumno” - “folio_solicitud”
                  v_Params(3).param_value := 'Solicitud de Cambio de Plan Estudios  Nº '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO ;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL005' THEN --########### RESERVA ATRICULA ########### -- OBSERVACION PARA LA OBTENCION DEL PERIODO.
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL005');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_SEQNO DESC 
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE DESC
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación - “Nombre Alumno” - “ID Alumno” - “folio_solicitud”
                  v_Params(3).param_value := 'Solicitud de Reserva de Matrìcula  Nº '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO ;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL006' THEN --## SOBREPASO DE RETENCION DE DOCUMENTOS ## -- 
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL006');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_SEQNO DESC 
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE DESC
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación - “Nombre Alumno” - “ID Alumno” - “folio_solicitud”
                  v_Params(3).param_value := 'Solicitud de Sobrepaso de Retencion de Documentos  Nº '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO ;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL007' THEN --## SOLICITUD DE MODIFICACIÒN DE LA CUOTA INICIAL ##
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL007');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_SEQNO DESC 
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE DESC
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación - “Nombre Alumno” - “ID Alumno” - “folio_solicitud”
                  v_Params(3).param_value := 'Solicitud de Modificación de la Cuota Inicial  Nº '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO ;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL009' THEN --## SOLICITUD DE REINCORPORACIÓN ##
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL009');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_SEQNO DESC 
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE DESC
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
               
              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación - “Nombre Alumno” - “ID Alumno” - “folio_solicitud”
                  v_Params(3).param_value := 'Solicitud de Reincorporaciòn  Nº '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO ;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL012' THEN --## SOLICITUD DE SEGURO MEDICO ##
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL012');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_SEQNO DESC 
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE DESC
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación - “Nombre Alumno” - “ID Alumno” - “folio_solicitud”
                  v_Params(3).param_value := 'Solicitud de Exoneración de seguro de Salud  Nº '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO ;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --        
      END IF;
      
END;

