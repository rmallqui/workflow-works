SET SERVEROUTPUT ON
DECLARE
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE;
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE;
    P_CODE_DEPT       STVDEPT.STVDEPT_CODE%TYPE;
    P_CODE_TERM       STVTERM.STVTERM_CODE%TYPE;
    P_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
    P_PRIORIDAD       NUMBER;
BEGIN
P_CHECK_DEPT_PRIORIDAD_REC('75803','SOL003','UREG','201800','S01',P_PRIORIDAD);
DBMS_OUTPUT.PUT_LINE(P_PRIORIDAD);
END;




CREATE OR REPLACE PROCEDURE P_CHECK_DEPT_PRIORIDAD_REC(
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       IN SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_CODE_DEPT       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CODE_TERM       IN STVTERM.STVTERM_CODE%TYPE,
    P_CODE_CAMP       IN STVCAMP.STVCAMP_CODE%TYPE,
    P_PRIORIDAD       OUT NUMBER
) 
 IS
      V_INDICADOR               NUMBER := -1;
      V_TERMADMIN               NUMBER := -1;
      V_RETURN                  NUMBER := -1;
      V_CODE_DEPT               STVDEPT.STVDEPT_CODE%TYPE;
      V_SVRRSRV_SEQ_NO          SVRRSRV.SVRRSRV_SEQ_NO%TYPE;
      V_SVRRSRV_WEB_IND         SVRRSRV.SVRRSRV_WEB_IND%TYPE;
      
      -- GET DEPT en caso no se envie entre los parametros
      CURSOR C_SORLCUR IS
      SELECT    SORLFOS_DEPT_CODE  
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC,SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;

      -- GET prioridad
      CURSOR C_SVRRSRV IS
      SELECT SVRRSRV_SEQ_NO, SVRRSRV_WEB_IND FROM SVRRSRV 
      WHERE SVRRSRV_SRVC_CODE = P_SRVC_CODE
      AND ( 
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE(TO_CHAR(SVRRSRV_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
            AND
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE(TO_CHAR(SVRRSRV_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          )
      AND SVRRSRV_SEQ_NO IN ( (0 + V_INDICADOR) , (1 + V_INDICADOR), (2 + V_INDICADOR) )
      ORDER BY SVRRSRV_SEQ_NO ASC; 
      
BEGIN  
--    
      P_PRIORIDAD := 0;
      V_CODE_DEPT := P_CODE_DEPT; 
      
      -- en caso se envie DEPT = null o vacio
      IF NVL(TRIM(V_CODE_DEPT),'TRUE') = 'TRUE'  THEN
      
              -- GET datos de alumno 
              OPEN C_SORLCUR;
              LOOP
                FETCH C_SORLCUR INTO V_CODE_DEPT;
                EXIT WHEN C_SORLCUR%NOTFOUND;
                END LOOP;
              CLOSE C_SORLCUR;
              
      END IF;
      DBMS_OUTPUT.PUT_LINE(V_CODE_DEPT||'---'||'----------------1');
      
      
      SELECT CASE V_CODE_DEPT 
                  WHEN 'UREG' THEN 1
                  WHEN 'UPGT' THEN 4
                  WHEN 'UVIR' THEN 7
                  ELSE -99 END INTO V_INDICADOR
      FROM DUAL;
      DBMS_OUTPUT.PUT_LINE(V_INDICADOR||'---'||'----------------2');
      
      -- GET COINCIDENCIAS A LOS FILTROS 
      -- UREG 1,2,3
      -- UPGT 4,5,6
      -- UVIR 7,8,9    
      -- GET DEPARTAMENTO, SEDE
      OPEN C_SVRRSRV;
      LOOP
        FETCH C_SVRRSRV INTO V_SVRRSRV_SEQ_NO, V_SVRRSRV_WEB_IND;
        IF C_SVRRSRV%FOUND THEN
            P_PRIORIDAD := CASE WHEN V_SVRRSRV_WEB_IND = 'N' THEN 0 ELSE V_SVRRSRV_SEQ_NO END;
        ELSE EXIT;
      END IF;
      END LOOP;
      CLOSE C_SVRRSRV;
      DBMS_OUTPUT.PUT_LINE(P_PRIORIDAD||'---'||'----------------3');
      
--
END P_CHECK_DEPT_PRIORIDAD_REC;


SELECT * FROM SVRRSRV
WHERE SVRRSRV_SRVC_CODE = 'SOL003' ORDER BY SVRRSRV_SEQ_NO;

/* *********************************************************************************************************************************************************************************************** */
/* *********************************************************************************************************************************************************************************************** */
/* *********************************************************************************************************************************************************************************************** */



CREATE OR REPLACE PROCEDURE P_GETDATA_ALUMN (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CODE_DEPT       OUT STVDEPT.STVDEPT_CODE%TYPE, 
    P_CODE_TERM       OUT STVTERM.STVTERM_CODE%TYPE,
    P_CODE_CAMP       OUT STVCAMP.STVCAMP_CODE%TYPE
) 
 AS
      P_INDICADOR               NUMBER;
      P_PART_PERIODO            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE;
      
      -- GET DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE                
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM = 1;
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_CODE_DEPT AND STVCAMP_CODE = P_CODE_CAMP;    
      
BEGIN  
--    
      P_CODE_TERM   := NULL;
      P_CODE_DEPT   := NULL;
      P_CODE_CAMP   := NULL;
      
      -- GET DEPARTAMENTO, SEDE
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CODE_CAMP,P_CODE_DEPT;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;
      
      -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
      IF (P_CODE_DEPT IS NULL OR P_CODE_CAMP IS NULL) THEN
          P_CODE_TERM := NULL;
      ELSE
            -----------------------------------------------------------
            -- >> calculando SUB PARTE PERIODO  --
            OPEN C_SFRRSTS_PTRM;
            LOOP
              FETCH C_SFRRSTS_PTRM INTO P_PART_PERIODO;
              EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
            END LOOP;
            CLOSE C_SFRRSTS_PTRM;
            
            -- GET PERIODO ACTIVO --- FORMA: SFARSTS
            SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
            AND SFRRSTS_RSTS_CODE = 'RW' AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE;
            IF P_INDICADOR = 0 THEN
                  P_CODE_TERM := NULL;
            ELSE
                  SELECT SFRRSTS_TERM_CODE INTO  P_CODE_TERM FROM (
                      -- Forma SFARSTS  ---> fechas para las partes de periodo
                      SELECT DISTINCT SFRRSTS_TERM_CODE 
                      FROM SFRRSTS
                      WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                      AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                      AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
                      ORDER BY SFRRSTS_TERM_CODE DESC
                  ) WHERE ROWNUM <= 1;                    
            END IF;
      END IF;
      
--
END P_GETDATA_ALUMN;

/* *********************************************************************************************************************************************************************************************** */
/* *********************************************************************************************************************************************************************************************** */
/* *********************************************************************************************************************************************************************************************** */

SET SERVEROUTPUT ON
DECLARE
 VALOR  VARCHAR2(10);
BEGIN
VALOR := F_RSS_SREM('75803','0','0');
DBMS_OUTPUT.PUT_LINE(VALOR);
END;

CREATE OR REPLACE FUNCTION F_RSS_SREM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
 IS
      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL003';
      V_PRIORIDAD           NUMBER;
      P_INDICADOR           NUMBER;
      V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE; 
      V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;

      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;

      -- Parte-de-Periodo
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM_CODE           SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;      
      
      -- OBTENER PERIODO por modulo de UVIR y UPGT
      CURSOR C_SFRRSTS IS
      SELECT SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE FROM (
          -- Forma SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE
          FROM SFRRSTS
          WHERE SFRRSTS_PTRM_CODE IN (V_SUB_PTRM || '1', V_SUB_PTRM || '2')
          AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por Web e INB
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
          ORDER BY SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE 
      ) WHERE ROWNUM <= 1;    
BEGIN  
--    
    -- DATOS ALUMNO
    P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);

    
    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN C_SFRRSTS_PTRM;
    LOOP
      FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFRRSTS_PTRM;
    DBMS_OUTPUT.PUT_LINE(V_SUB_PTRM||'---'||'----------------1');
    /************************************************************************************************************/
    -- Segun regla para UPGT y UVIR, los que tramitan reserva el mismo periodo que ingresaron 
    /************************************************************************************************************/
    IF  (V_CODE_DEPT = 'UPGT' OR V_CODE_DEPT = 'UVIR') THEN

          OPEN C_SFRRSTS;
            LOOP
              FETCH C_SFRRSTS INTO P_CODE_TERM, V_PTRM_CODE;
              IF C_SFRRSTS%FOUND THEN
                  V_CODE_TERM := P_CODE_TERM;
              ELSE EXIT;
            END IF;
            END LOOP;
          CLOSE C_SFRRSTS;

    END IF;
    DBMS_OUTPUT.PUT_LINE(P_CODE_TERM||'---'||V_PTRM_CODE||'---'||'----------------2');

    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD_REC (P_PIDM, P_CODIGO_SOL, V_CODE_DEPT, NULL, NULL, V_PRIORIDAD);

    -- VERIFICAR QUE YA ESTE MATRICULADO
    ---A VERIFICAR QUE TENGA NRC's
      SELECT COUNT(*) INTO P_INDICADOR FROM SFRSTCR 
      WHERE SFRSTCR_PIDM = P_PIDM
      --AND SFRSTCR_PTRM_CODE = (CASE WHEN V_PTRM_CODE IS NULL THEN (V_SUB_PTRM || '1') ELSE V_PTRM_CODE END)
      --AND SFRSTCR_RSTS_CODE IN ('RE','RW')
      AND SFRSTCR_TERM_CODE = V_CODE_TERM; 
    DBMS_OUTPUT.PUT_LINE(P_INDICADOR||'---'||'----------------3');
    
    IF V_PRIORIDAD > 0 AND P_INDICADOR > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SREM;


SELECT * FROM SFRSTCR
WHERE SFRSTCR_PIDM = '75803' AND SFRSTCR_TERM_CODE = '201800';

SELECT * FROM SFRSRPO
WHERE SFRSRPO_PIDM = '75803' AND SFRSRPO_ROVR_CODE = '03';