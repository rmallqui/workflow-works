/*
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

SET SERVEROUTPUT ON
DECLARE   P_NUMERO_SOLICITUD      SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE   := 19 ;
          P_COD_ATRIBUTO          STVATTS.STVATTS_CODE%TYPE              := 'RECM';
          P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
          P_ERROR                 VARCHAR2(100);
BEGIN
    P_SET_CODDETALLE_ALUMNO(19,'RECM',P_TRAN_NUMBER,P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_TRAN_NUMBER  || '--' || P_ERROR);
END;

*/
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE PROCEDURE P_SET_CODDETALLE_ALUMNO( 
        P_NUMERO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_COD_ATRIBUTO          IN STVATTS.STVATTS_CODE%TYPE,
        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_ERROR                 OUT varchar2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CODDETALLE_ALUMNO
  FECHA     : 10/11/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Agrega un atributo y posteriormente genera la deuda(Cod Detalle) relacionada.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
        P_ID_ALUMNO                 SPRIDEN.SPRIDEN_PIDM%TYPE;
        P_CODIGO_SOLICITUD          SVVSRVC.SVVSRVC_CODE%TYPE;
        P_ESTADO_OLD                SVRSVPR.SVRSVPR_SRVS_CODE%TYPE;
        P_PERIODO                   SFBETRM.SFBETRM_TERM_CODE%TYPE;
        P_PERIODO_NEXT              SFBETRM.SFBETRM_TERM_CODE%TYPE;
        NOM_SERVICIO                VARCHAR2(30);
        
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
        
        --P_COD_ATRIBUTO          STVATTS.STVATTS_CODE%TYPE;            -- COD atributos
        P_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        P_COD_DETALLE           SFRRGFE.SFRRGFE_DETL_CODE%TYPE;
        P_INDICADOR             NUMBER;
        C_CARGO_MINIMO              SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        P_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
        
        SAVE_ACT_DATE_OUT       VARCHAR2(100);
        RETURN_STATUS_IN_OUT    NUMBER;
BEGIN
    
      -- GET VALUES SOLICITUD ALUMNO
      SELECT  SVRSVPR_PIDM,
              SVRSVPR_SRVC_CODE,        -- ejem 'SOL003'
              SVRSVPR_SRVS_CODE,        -- estado solicitud
              --SVRSVPR_WSSO_CODE,        -- metodo entrega
              --SVRSVPR_ACCD_TRAN_NUMBER, -- Numero de transaccion actual
              --SVRSVPR_PROTOCOL_AMOUNT,  -- costo po la entrega
              SVRSVPR_TERM_CODE
      INTO    P_ID_ALUMNO,
              P_CODIGO_SOLICITUD, 
              P_ESTADO_OLD, 
              --P_METOD_ENTREGA, 
              --P_TRAN_NUMBER_OLD,
              --P_COSTO_SOLICITUD,
              P_PERIODO
      FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;  
      
  
      -- GET - NEXT Periodo
      SELECT NEXT_TERM INTO P_PERIODO_NEXT
      FROM (
        SELECT STVTERM_CODE, LEAD(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) NEXT_TERM
        FROM STVTERM ORDER BY STVTERM_CODE DESC
      ) WHERE STVTERM_CODE = P_PERIODO;
      
      -- OPCION QUE SE MANDE POR PARAMETRO AL SP
--      -- GET COD ATRIBUTO 
--      SELECT  CASE P_CODIGO_SOLICITUD WHEN 'SOL003' THEN 'RECM' -- RECARGO DE MATRICULA
--                                      WHEN 'SOL004' THEN 'RECM' --'RES'  -- RESERVA
--                                      WHEN 'SOL005' THEN 'RECM'
--                                      ELSE '-'
--                                      END
--      INTO P_COD_ATRIBUTO FROM DUAL;
                                      
 
      -- #######################################################################
      -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
      SELECT    SORLCUR_SEQNO,      
                SORLCUR_LEVL_CODE,    
                SORLCUR_CAMP_CODE,        
                SORLCUR_COLL_CODE,
                SORLCUR_DEGC_CODE,  
                SORLCUR_PROGRAM,      
                SORLCUR_TERM_CODE_ADMIT,  
                --SORLCUR_STYP_CODE,
                SORLCUR_STYP_CODE,  
                SORLCUR_RATE_CODE,    
                SORLFOS_DEPT_CODE   
      INTO      C_SEQNO,
                C_ALUM_NIVEL, 
                C_ALUM_CAMPUS, 
                C_ALUM_ESCUELA, 
                C_ALUM_GRADO, 
                C_ALUM_PROGRAMA, 
                C_ALUM_PERD_ADM,
                C_ALUM_TIPO_ALUM,
                C_ALUM_TARIFA,
                C_ALUM_DEPARTAMENTO
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                        SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                        SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_ID_ALUMNO 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;
      

      -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
      SELECT SFRRGFE_DETL_CODE, SFRRGFE_MIN_CHARGE INTO P_COD_DETALLE, C_CARGO_MINIMO
      FROM SFRRGFE 
      WHERE SFRRGFE_TERM_CODE = P_PERIODO AND SFRRGFE_ATTS_CODE = P_COD_ATRIBUTO AND SFRRGFE_TYPE = 'STUDENT'
      AND NVL(NVL(SFRRGFE_LEVL_CODE, c_alum_nivel),'-')           = NVL(NVL(c_alum_nivel, SFRRGFE_LEVL_CODE),'-')--------------------- Nivel
      AND NVL(NVL(SFRRGFE_CAMP_CODE, c_alum_campus),'-')          = NVL(NVL(c_alum_campus, SFRRGFE_CAMP_CODE),'-')  ----------------- Campus (sede)
      AND NVL(NVL(SFRRGFE_COLL_CODE, c_alum_escuela),'-')         = NVL(NVL(c_alum_escuela, SFRRGFE_COLL_CODE),'-') --------------- Escuela 
      AND NVL(NVL(SFRRGFE_DEGC_CODE, c_alum_grado),'-')           = NVL(NVL(c_alum_grado, SFRRGFE_DEGC_CODE),'-') ----------- Grado
      AND NVL(NVL(SFRRGFE_PROGRAM, c_alum_programa),'-')          = NVL(NVL(c_alum_programa, SFRRGFE_PROGRAM),'-') ---------- Programa
      AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm),'-')  = NVL(NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT),'-') --------- Periodo Admicion
      -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
      -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
      -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
      AND NVL(NVL(SFRRGFE_DEPT_CODE, c_alum_departamento),'-')    = NVL(NVL(c_alum_departamento, SFRRGFE_DEPT_CODE),'-') ------------ Departamento
      -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
      AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum),'-') = NVL(NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') ------ Tipo Alumno Curriculum
      AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa),'-')   = NVL(NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------- Trf Curriculum (Escala)

      -- GET - Descripcion de CODIGO DETALLE  
      SELECT TBBDETC_DESC INTO NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;
      
     
      -- #######################################################################
      -- SET ATRIBUTO alumno
      INSERT INTO SGRSATT
        (
          SGRSATT_PIDM, 
          SGRSATT_TERM_CODE_EFF,
          SGRSATT_ATTS_CODE,
          SGRSATT_ACTIVITY_DATE,
          SGRSATT_STSP_KEY_SEQUENCE
        )
      SELECT P_ID_ALUMNO, P_PERIODO, P_COD_ATRIBUTO, SYSDATE, NULL
      FROM DUAL
      WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                    WHERE SGRSATT_PIDM = P_ID_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
                    AND (SGRSATT_ATTS_CODE = P_COD_ATRIBUTO OR SGRSATT_ATTS_CODE IS NULL));
      P_INDICADOR := SQL%ROWCOUNT;  
      -- ACTUALIZAR en caso exista NULL con el ATRIBUTO para el periodo.
      IF P_INDICADOR = 0  THEN
          UPDATE SGRSATT SET SGRSATT_ATTS_CODE = P_COD_ATRIBUTO
          WHERE SGRSATT_PIDM = P_ID_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
          AND SGRSATT_ATTS_CODE IS NULL 
          AND NOT EXISTS (SELECT SGRSATT_PIDM FROM SGRSATT 
                    WHERE SGRSATT_PIDM = P_ID_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO
                    AND SGRSATT_ATTS_CODE = P_COD_ATRIBUTO);
      END IF;
   
      -- SET - ATRIBUTO FINALIZAR (LIMITAR VALIDES del atributo A UN SOLO PERIODO)
      INSERT INTO SGRSATT
        ( 
          SGRSATT_PIDM, 
          SGRSATT_TERM_CODE_EFF,
          SGRSATT_ATTS_CODE,
          SGRSATT_ACTIVITY_DATE,
          SGRSATT_STSP_KEY_SEQUENCE
        )
      SELECT P_ID_ALUMNO, P_PERIODO_NEXT, NULL, SYSDATE, NULL
      FROM DUAL
      WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                        WHERE SGRSATT_PIDM = P_ID_ALUMNO 
                        AND SGRSATT_TERM_CODE_EFF = P_PERIODO_NEXT
                    );
      P_INDICADOR := SQL%ROWCOUNT;
    
      -- #######################################################################
      -- GENERAR DEUDA
      TB_RECEIVABLE.p_create ( p_pidm                 =>  P_ID_ALUMNO,        -- PIDEM ALUMNO
                               p_term_code            =>  P_PERIODO,          -- DETALLE
                               p_detail_code          =>  P_COD_DETALLE,   -- CODIGO DETALLE 
                               p_user                 =>  USER,               -- USUARIO
                               p_entry_date           =>  SYSDATE,     
                               p_amount               =>  C_CARGO_MINIMO,
                               p_effective_date       =>  SYSDATE,
                               p_bill_date            =>  NULL,    
                               p_due_date             =>  NULL,    
                               p_desc                 =>  NOM_SERVICIO,    
                               p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                               p_tran_number_paid     =>  NULL,     -- numero de transaccion que ser� pagara
                               p_crossref_pidm        =>  NULL,    
                               p_crossref_number      =>  NULL,    
                               p_crossref_detail_code =>  NULL,     
                               p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                               p_acct_feed_ind        =>  'Y',
                               p_session_number       =>  0,    
                               p_cshr_end_date        =>  NULL,    
                               p_crn                  =>  NULL,
                               p_crossref_srce_code   =>  NULL,
                               p_loc_mdt              =>  NULL,
                               p_loc_mdt_seq          =>  NULL,    
                               p_rate                 =>  NULL,    
                               p_units                =>  NULL,     
                               p_document_number      =>  NULL,    
                               p_trans_date           =>  NULL,    
                               p_payment_id           =>  NULL,    
                               p_invoice_number       =>  NULL,    
                               p_statement_date       =>  NULL,    
                               p_inv_number_paid      =>  NULL,    
                               p_curr_code            =>  NULL,    
                               p_exchange_diff        =>  NULL,    
                               p_foreign_amount       =>  NULL,    
                               p_late_dcat_code       =>  NULL,    
                               p_atyp_code            =>  NULL,    
                               p_atyp_seqno           =>  NULL,    
                               p_card_type_vr         =>  NULL,    
                               p_card_exp_date_vr     =>  NULL,     
                               p_card_auth_number_vr  =>  NULL,    
                               p_crossref_dcat_code   =>  NULL,    
                               p_orig_chg_ind         =>  NULL,    
                               p_ccrd_code            =>  NULL,    
                               p_merchant_id          =>  NULL,    
                               p_data_origin          =>  'WorkFlow',    
                               p_override_hold        =>  'N',     
                               p_tran_number_out      =>  P_TRAN_NUMBER, 
                               p_rowid_out            =>  P_ROWID);
      COMMIT;

      -- #######################################################################
      -- Procesar DEUDA
      SFKFEES.p_processfeeassessment (  P_PERIODO,
                                P_ID_ALUMNO,
                                SYSDATE,      -- assessment effective date(Evaluaci�n de la fecha efectiva)
                                SYSDATE + 1,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                'R',          -- use regular assessment rules(utilizar las reglas de evaluaci�n peri�dica)
                                'Y',          -- create TBRACCD records
                                'SFAREGS',    -- where assessment originated from
                                'Y',          -- commit changes
                                SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                'N',          -- do not ignore SFRFMAX rules
                                RETURN_STATUS_IN_OUT );   -- OUT -- return_status

      COMMIT;
EXCEPTION
  WHEN OTHERS THEN
        ROLLBACK;
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_SET_CODDETALLE_ALUMNO;


----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------
