/*
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;


------------------------------- FALTA PROBAR EL NUEVOO PARAMETRO - P_TRAN_NUMBER
set serveroutput on
DECLARE p_error varchar2(64);
begin
  p_set_coddetalle_alumno('179316','201710','EXM',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;

*/
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

create or replace PROCEDURE P_SET_CODDETALLE_ALUMNO (
  P_ID_ALUMNO       IN TBRACCD.TBRACCD_PIDM%TYPE, 
  P_PERIODO         IN TBRACCD.TBRACCD_TERM_CODE%TYPE , 
  P_CODIGO_DETALLE  IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
  P_TRAN_NUMBER     OUT NUMBER,
  P_ERROR           OUT VARCHAR2
  )
/* ===================================================================================================================
  NOMBRE    : p_set_coddetalle_alumno
  FECHA     : 24/08/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Inserta un c�digo de detalle a la cuenta corriente del alumno para un periodo correspondiente.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */

AS
        -- p_id_alumno               TBRACCD.TBRACCD_PIDM%TYPE;
        C_CODIGO_DETALLE_DEC      TBBDETC.TBBDETC_DESC%TYPE;
        C_USER                    TBRACCD.TBRACCD_USER%TYPE;
        
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%TYPE;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%TYPE;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%TYPE;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%TYPE;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%TYPE;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%TYPE;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%TYPE;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%TYPE;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%TYPE;
        
        C_NIVEL                   SFRRGFE.SFRRGFE_LEVL_CODE%TYPE;           -- STVLEVL
        C_CAMPUS                  SFRRGFE.SFRRGFE_CAMP_CODE%TYPE;           -- STVCAMP
        C_ESCUELA                 SFRRGFE.SFRRGFE_COLL_CODE%TYPE;           -- STVCOLL
        C_GRADO                   SFRRGFE.SFRRGFE_DEGC_CODE%TYPE;           -- STVDEGC
        C_PROGRAMA                SFRRGFE.SFRRGFE_PROGRAM%TYPE;             -- consultar relacion con SFRRGFE_MAJR_CODE-STVMAJR
        C_PERD_ADM                SFRRGFE.SFRRGFE_TERM_CODE_ADMIT%TYPE;     -- STVTERM
        --c_curriculums             SFRRGFE.SFRRGFE_PRIM_SEC_CDE%TYPE;
        C_TIPO_CAMP_STUDIO        SFRRGFE.SFRRGFE_LFST_CODE%TYPE;           -- GTVLFST
        C_COD_CAMP_STUDIO         SFRRGFE.SFRRGFE_MAJR_CODE%TYPE;           -- STVMAJR
        C_DEPTO                   SFRRGFE.SFRRGFE_DEPT_CODE%TYPE;           -- STVDEPT
        --c_camp_studio             SFRRGFE.SFRRGFE_LFST_PRIM_SEC_CDE%TYPE; 
        C_TIPO_ALUMN_CURRICULM    SFRRGFE.SFRRGFE_STYP_CODE_CURRIC%TYPE;    -- STVSTYP
        C_TRF_CURRICULM           SFRRGFE.SFRRGFE_RATE_CODE_CURRIC%TYPE;    -- STVRATE
        C_CARGO_MINIMO            SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        
        --P_TRAN_NUMBER             TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        /* FORMAS */
        C_FORM                    VARCHAR2(16);
BEGIN
--
    ----------------------------------------------------------------------------
        C_FORM := 'SGASTDN';
        /* -- Get datos alumno:
        NOTA: probabilidad de encontrar doble curricula activa para el alumno - saldria error */
        SELECT 
            SORLCUR_LEVL_CODE,
            SORLCUR_CAMP_CODE,
            SORLCUR_COLL_CODE,
            SORLCUR_DEGC_CODE,
            SORLCUR_PROGRAM,
            SORLCUR_TERM_CODE_ADMIT,
            --SORLCUR_STYP_CODE,
            SORLCUR_STYP_CODE,
            SORLCUR_RATE_CODE,
            SORLFOS_DEPT_CODE
        INTO 
            C_ALUM_NIVEL, 
            C_ALUM_CAMPUS, 
            C_ALUM_ESCUELA, 
            C_ALUM_GRADO, 
            C_ALUM_PROGRAMA, 
            C_ALUM_PERD_ADM,/*c_alum_tipo_alum,*/
            C_ALUM_TIPO_ALUM,
            C_ALUM_TARIFA,
            C_ALUM_DEPARTAMENTO
        FROM SORLCUR 
        INNER JOIN SORLFOS
          ON SORLCUR_PIDM =  SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_ID_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
        AND SORLCUR_TERM_CODE = P_PERIODO AND SORLCUR_CACT_CODE = 'ACTIVE';
    ----------------------------------------------------------------------------
        C_FORM := 'SFARGFE';
        -- GET cargo minimo, Filtrando las reglas de curriculum (forma SFARGFE)
        SELECT SFRRGFE_MIN_CHARGE INTO C_CARGO_MINIMO
         FROM SFRRGFE 
        WHERE SFRRGFE_TERM_CODE = P_PERIODO AND SFRRGFE_DETL_CODE = P_CODIGO_DETALLE AND SFRRGFE_TYPE = 'STUDENT'
        AND NVL(SFRRGFE_LEVL_CODE, C_ALUM_NIVEL)            = NVL(C_ALUM_NIVEL, SFRRGFE_LEVL_CODE)  ----------------- Nivel
        AND NVL(SFRRGFE_CAMP_CODE, C_ALUM_CAMPUS)           = NVL(C_ALUM_CAMPUS, SFRRGFE_CAMP_CODE) ----------------- Campus
        AND NVL(SFRRGFE_COLL_CODE, C_ALUM_ESCUELA)          = NVL(C_ALUM_ESCUELA, SFRRGFE_COLL_CODE) ---------------- Escuela
        AND NVL(SFRRGFE_DEGC_CODE, C_ALUM_GRADO)            = NVL(C_ALUM_GRADO, SFRRGFE_DEGC_CODE) ------------------ Grado
        AND NVL(SFRRGFE_PROGRAM, C_ALUM_PROGRAMA)           = NVL(C_ALUM_PROGRAMA, SFRRGFE_PROGRAM) ----------------- Programa
        AND NVL(SFRRGFE_TERM_CODE_ADMIT, C_ALUM_PERD_ADM)   = NVL(C_ALUM_PERD_ADM, SFRRGFE_TERM_CODE_ADMIT) --------- Periodo ADMICION 
        AND NVL(SFRRGFE_DEPT_CODE, C_ALUM_DEPARTAMENTO)     = NVL(C_ALUM_DEPARTAMENTO, SFRRGFE_DEPT_CODE) ----------- Departamento
        AND NVL(SFRRGFE_STYP_CODE_CURRIC, C_ALUM_TIPO_ALUM) = NVL(C_ALUM_TIPO_ALUM, SFRRGFE_STYP_CODE_CURRIC) ------- Tipo Alumno Curricul
        AND NVL(SFRRGFE_RATE_CODE_CURRIC, C_ALUM_TARIFA)    = NVL(C_ALUM_TARIFA, SFRRGFE_RATE_CODE_CURRIC); --------- Trf Curriculms
    ----------------------------------------------------------------------------
    C_FORM := '';
      -- GET descripcion del Codigo Detalle
      SELECT TBBDETC_DESC INTO C_CODIGO_DETALLE_DEC  FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = P_CODIGO_DETALLE AND TBBDETC_TYPE_IND = 'C';
      
      -- GET usuario
      SELECT USER INTO C_USER FROM DUAL;
      
      -- Calcular NUMERO de TRANSACTION
      SELECT TBRACCD_TRAN_NUMBER + 1 INTO P_TRAN_NUMBER FROM (
                        SELECT TBRACCD_TRAN_NUMBER FROM TBRACCD
                        WHERE TBRACCD_PIDM = P_ID_ALUMNO ORDER BY TBRACCD_TRAN_NUMBER DESC
                      )WHERE ROWNUM<= 1;
    
    ----------------------------------------------------------------------------
      INSERT INTO TBRACCD (
        TBRACCD_PIDM ,TBRACCD_TRAN_NUMBER ,TBRACCD_TERM_CODE ,TBRACCD_DETAIL_CODE ,TBRACCD_USER ,TBRACCD_ENTRY_DATE,
        TBRACCD_AMOUNT ,TBRACCD_BALANCE ,TBRACCD_EFFECTIVE_DATE ,TBRACCD_DESC ,TBRACCD_SRCE_CODE,TBRACCD_ACCT_FEED_IND,
        TBRACCD_ACTIVITY_DATE ,TBRACCD_SESSION_NUMBER,TBRACCD_TRANS_DATE
      )
      SELECT
            P_ID_ALUMNO ,P_TRAN_NUMBER ,P_PERIODO ,P_CODIGO_DETALLE ,C_USER ,SYSDATE ,C_CARGO_MINIMO,0 ,SYSDATE ,C_CODIGO_DETALLE_DEC ,'T' ,'Y' ,SYSDATE,0,SYSDATE
      FROM DUAL
      WHERE EXISTS (
           --SELECT TBBDETC_DESC FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = p_codigo_detalle
           SELECT SFRRGFE_MIN_CHARGE 
             FROM SFRRGFE 
            WHERE SFRRGFE_TERM_CODE = P_PERIODO AND SFRRGFE_DETL_CODE = P_CODIGO_DETALLE AND SFRRGFE_TYPE = 'STUDENT'
            AND NVL(SFRRGFE_LEVL_CODE, C_ALUM_NIVEL)            = NVL(C_ALUM_NIVEL, SFRRGFE_LEVL_CODE) 
            AND NVL(SFRRGFE_CAMP_CODE, C_ALUM_CAMPUS)           = NVL(C_ALUM_CAMPUS, SFRRGFE_CAMP_CODE) 
            AND NVL(SFRRGFE_COLL_CODE, C_ALUM_ESCUELA)          = NVL(C_ALUM_ESCUELA, SFRRGFE_COLL_CODE) 
            AND NVL(SFRRGFE_DEGC_CODE, C_ALUM_GRADO)            = NVL(C_ALUM_GRADO, SFRRGFE_DEGC_CODE) 
            AND NVL(SFRRGFE_PROGRAM, C_ALUM_PROGRAMA)           = NVL(C_ALUM_PROGRAMA, SFRRGFE_PROGRAM) 
            AND NVL(SFRRGFE_TERM_CODE_ADMIT, C_ALUM_PERD_ADM)   = NVL(C_ALUM_PERD_ADM, SFRRGFE_TERM_CODE_ADMIT) 
            AND NVL(SFRRGFE_DEPT_CODE, C_ALUM_DEPARTAMENTO)     = NVL(C_ALUM_DEPARTAMENTO, SFRRGFE_DEPT_CODE)
            AND NVL(SFRRGFE_STYP_CODE_CURRIC, C_ALUM_TIPO_ALUM) = NVL(C_ALUM_TIPO_ALUM, SFRRGFE_STYP_CODE_CURRIC)
            AND NVL(SFRRGFE_RATE_CODE_CURRIC, C_ALUM_TARIFA)    = NVL(C_ALUM_TARIFA, SFRRGFE_RATE_CODE_CURRIC)
      );
      
      P_ERROR := 'Correcto';
    ----------------------------------------------------------------------------

--
    COMMIT;
EXCEPTION
  WHEN OTHERS THEN
          -- DBMS_OUTPUT.PUT_LINE ('NO se detectaron REGISTROS - forma : '  || c_form );
          P_ERROR := 'A ocurrido un error - '  || C_FORM ||SQLCODE||' -ERROR- '||SQLERRM;
END P_SET_CODDETALLE_ALUMNO;

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------
