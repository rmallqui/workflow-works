FORMAS:
===============================================================================
 TAISMGR – CUENTAS por cobrar
 TVAAREV - CARGOS ALUMNOS
 SGASTDN - DATOS DE ALUMNO - MATRICULA
 SFARGFE - REGLAS DE CUOTAS (DOGISP DETALLE)
 
 
 SPAIDEN - DATOS PERSONAS
 




--- SGASADD : AREGAR ATRIBUTOS
      -- Analizar funcionamiento de agregar atributos
      -- logica y funcionamiento de las reglas de periodo sobre los atributos ()
      -- Funcionamiento de recalcular las deudas de atributo en SFAREGS (al solo guardar se muestra un mensaje)
      -- enlazar solicitud al la deuda recalculada.
      
      -- La deuda se genera con el atributo, pero no se elimina.
      -- la deuda se genera con el atributo, pero no se puede volver a generar una deuda adicional con el mismo atributo.
      -- La deuda no se genera si en el periodo no tiene el atributo pero no esta ELEGIBLE
      -- Se puede generar la deuda vida SCRIPT N veces, no se si es lo correcto.
      -- ¿Se puede agregar el NRO TRANS generada en el campo de la solicitud destinado a un numero de transaccion sin problemas?
      
--42843000  332660
--44832442  408462
--48486584  130895
--70520998  203946
--70801186  208003
--74961386  219098
--75511661  305021
--76018115  25465
--76791634    408093
  
select * from SPRIDEN WHERE SPRIDEN_PIDM = '284082';
select * from SPRIDEN WHERE SPRIDEN_ID in ('44832442' ,'70520998' ,'75511661','70801186','76018115','74961386','48486584','76791634','42843000');
select * from SVRSVPR;
      
  -- SGASADD -- agregar atributo por periodo.
      -- codigo RECARGO D EMATRICULA - RECM
  SELECT * FROM SGRSATT WHERE SGRSATT_PIDM = 203946;
SELECT * FROM STVATTS; 

    -- 70073244 -- PIDM 325180
    DELETE SGRSATT WHERE SGRSATT_PIDM = 203946; COMMIT; -- SE PUEDE ELIMINAR TODO
    UPDATE SGRSATT SET SGRSATT_ATTS_CODE = 'RECR' , SGRSATT_TERM_CODE_EFF = '201620' WHERE SGRSATT_PIDM = 325180 AND SGRSATT_ATTS_CODE IS NULL;
    
    SELECT * FROM STVTERM;
    
    INSERT
    INTO SATURN.SGRSATT
      (
        SGRSATT_PIDM,
        SGRSATT_TERM_CODE_EFF,
        SGRSATT_ATTS_CODE,
        SGRSATT_ACTIVITY_DATE,
        SGRSATT_STSP_KEY_SEQUENCE
      )
      VALUES
      (
        --325180, '201620', 'RECM', SYSDATE, NULL
        325180, '201710', NULL, SYSDATE, NULL
      );
      COMMIT; 

-- verificar deuda TVAAREV

SELECT * FROM SPRIDEN WHERE SPRIDEN_PIDM IN (SELECT SGRSATT.SGRSATT_PIDM FROM SGRSATT);
SELECT * FROM SPRIDEN WHERE SPRIDEN_PIDM IN (SELECT SVRSVPR.SVRSVPR_PIDM FROM SVRSVPR);

-- SFARGFE - relacion de atributo y codigo detalle
  SELECT * FROM SFRRGFE WHERE SFRRGFE_TERM_CODE = '201710' ;

-- SGASTDN : detalle de alumnos
    SELECT * FROM SORLCUR WHERE SORLCUR_PIDM = 325180;
    SELECT * FROM SOVLCUR WHERE SOVLCUR_PIDM = 325180;
    
    SELECT * FROM SORLFOS WHERE SORLFOS_PIDM = 325180;
    SELECT * FROM SOVLFOS WHERE SOVLFOS_PIDM = 325180;
    
    SELECT * FROM STVCSTS 

-- TFADETC - Control codigo detalle - finansas
  SELECT * FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = 'RMA';
-- TSADETC - Control codigo detalle - alumno
  SELECT * FROM TBBDETC;

                
-- VERIFICAR DEUDAS
SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 439281 AND TBRACCD_TRAN_NUMBER IN (9,12) UNION
SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 284082 AND TBRACCD_DETAIL_CODE = 'RMA' UNION
SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 325180 AND TBRACCD_TRAN_NUMBER = 5 ;
SELECT * FROM TTVSRCE;

-- solictud
SELECT * FROM SVRSVPR WHERE SVRSVPR_PIDM = 325180 AND SVRSVPR_SRVC_CODE = 'SOL004' ;
SELECT * FROM SPRIDEN WHERE SPRIDEN_PIDM = 325180; --70073244

      
      -- ATRIBUTO : GET CARGO y COD_DETALLE
      SELECT SFRRGFE_MIN_CHARGE, SFRRGFE_DETL_CODE INTO P_CARGO_MINIMO, P_COD_DETALLE
      FROM SFRRGFE 
      WHERE SFRRGFE_TERM_CODE = '201620' 
      AND SFRRGFE_DETL_CODE = 'RMA'
      AND SFRRGFE_LEVL_CODE = 'PG'
      AND SFRRGFE_CAMP_CODE = 'S01'
      AND SFRRGFE_DEPT_CODE = 'UREG'
      AND SFRRGFE_ATTS_CODE = 'RECM';

-----####### get next value in colum
SELECT * FROM STVTERM ORDER BY STVTERM_CODE DESC;
SELECT STVTERM_CODE,
LEAD(STVTERM_CODE, 0, 0) OVER (PARTITION BY STVTERM_CODE ORDER BY STVTERM_CODE DESC) NEXT_LOW_SAL,
LAG(STVTERM_CODE, 0, 0) OVER (PARTITION BY STVTERM_CODE ORDER BY STVTERM_CODE DESC) PREV_HIGH_SAL
FROM STVTERM
--WHERE deptno IN (10, 20)
ORDER BY STVTERM_CODE DESC;
-----######## get next value in colum
SELECT STVTERM_CODE, TERM_ORDER,
LEAD(TERM_ORDER, 1, 0) OVER (ORDER BY STVTERM_CODE) NEXT_TERM
FROM (
    SELECT STVTERM_CODE, STVTERM_CODE TERM_ORDER 
    FROM STVTERM ORDER BY STVTERM_CODE
)  ORDER BY TERM_ORDER;
-----#### get next value in colum
SELECT NEXT_TERM
FROM (
  SELECT STVTERM_CODE, LEAD(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) NEXT_TERM
  FROM STVTERM ORDER BY STVTERM_CODE DESC
) WHERE STVTERM_CODE = '201620';

SELECT * FROM SVRSVPR WHERE SVRSVPR_PIDM = 284082; -- 40187961

-- SOATERM : PARTES periodos y Fechas de inicio y fin
SELECT * FROM SOBPTRM
WHERE SYSDATE BETWEEN SOBPTRM_START_DATE AND SOBPTRM_END_DATE;
-- STVPTRM : DETALLE DE PARTES periodos
SELECT * FROM STVPTRM;
-- SEDE
SELECT * FROM STVCAMP;            
-- DEPT
SELECT * FROM STVDEPT;


SELECT SGBSTDN_PIDM, SGBSTDN_TERM_CODE_EFF, SGBSTDN.* FROM SGBSTDN WHERE SGBSTDN_PIDM in (37037,179316);
-- DELETE ::::  DELETE SGRSATT WHERE SGRSATT_PIDM = 284082; COMMIT; -- SE PUEDE ELIMINAR TODO
SELECT * FROM SGRSATT WHERE SGRSATT_PIDM = 284082;
SELECT * FROM SVRSVPR WHERE SVRSVPR_PIDM = 284082;


 /*###################################################################################################################*/


--44832442  408462
SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 408462 AND TBRACCD_DETAIL_CODE = 'RMA'
--delete from TBRACCD WHERE TBRACCD_PIDM = 408462 AND TBRACCD_DETAIL_CODE = 'RMA'; commit;

/* El p_processfeeassessment :  
                  - entre las pruebas es probar q funcione con un alumno con aitrubot , limpio. lo creo correcamente
                  - tambien creo cuando agrege un atributo por script, correctamente 
                  - y cuando elimine la deuda de TBRACCD manualmente la deudas, volvio a generarle la deuda
      RETORNA : 4 en caso hYA CREADO UNA DEUDA, Y NO RETORNA SI VULVES A EJECUTARLO POR SEGUNDA VEZ Y NO HAYA NADA PENDIENTE. */

SET SERVEROUTPUT ON
DECLARE   SAVE_ACT_DATE_OUT    VARCHAR2(100);
          return_status_in_out     NUMBER; 
BEGIN
  SFKFEES.p_processfeeassessment ('201620','408462',SYSDATE,SYSDATE + 1,'R','Y','WORKFLOW','Y',SAVE_ACT_DATE_OUT,'N',return_status_in_out );
  DBMS_OUTPUT.PUT_LINE( SAVE_ACT_DATE_OUT ||'--'|| return_status_in_out );
END;


 /*###################################################################################################################*/


SET SERVEROUTPUT ON
-------------------- MODELO PROCEDURE -----------------------
DECLARE P_NUMERO_SOLICITUD      SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE  := 19;
        --P_ESTADO                SVVSRVS.SVVSRVS_CODE%TYPE             := 'AP';
        P_COD_ATRIBUTO          STVATTS.STVATTS_CODE%TYPE             := 'RECM';
        P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
        P_ERROR                 VARCHAR2(100);
        
        P_ID_ALUMNO                 SPRIDEN.SPRIDEN_PIDM%TYPE;
        P_CODIGO_SOLICITUD          SVVSRVC.SVVSRVC_CODE%TYPE;
        P_ESTADO_OLD                SVRSVPR.SVRSVPR_SRVS_CODE%TYPE;
        --P_METOD_ENTREGA             STVWSSO.STVWSSO_CODE%TYPE;
        --P_TRAN_NUMBER_OLD           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE    := 0;
        --P_COSTO_SOLICITUD           SVRSVPR.SVRSVPR_PROTOCOL_AMOUNT%TYPE;
        P_PERIODO                   SFBETRM.SFBETRM_TERM_CODE%TYPE;
        P_PERIODO_NEXT              SFBETRM.SFBETRM_TERM_CODE%TYPE;
        NOM_SERVICIO                VARCHAR2(30);
        
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
        
        --P_COD_ATRIBUTO          STVATTS.STVATTS_CODE%TYPE;            -- COD atributos
        P_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        P_COD_DETALLE           SFRRGFE.SFRRGFE_DETL_CODE%TYPE;
        P_INDICADOR             NUMBER;
        C_CARGO_MINIMO              SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        P_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
        
        SAVE_ACT_DATE_OUT       VARCHAR2(100);
        RETURN_STATUS_IN_OUT    NUMBER;
BEGIN
    
      -- GET VALUES
      SELECT  SVRSVPR_PIDM,
              SVRSVPR_SRVC_CODE,        -- ejem 'SOL003'
              SVRSVPR_SRVS_CODE,        -- estado solicitud
              --SVRSVPR_WSSO_CODE,        -- metodo entrega
              --SVRSVPR_ACCD_TRAN_NUMBER, -- Numero de transaccion actual
              --SVRSVPR_PROTOCOL_AMOUNT,  -- costo po la entrega
              SVRSVPR_TERM_CODE
      INTO    P_ID_ALUMNO,
              P_CODIGO_SOLICITUD, 
              P_ESTADO_OLD, 
              --P_METOD_ENTREGA, 
              --P_TRAN_NUMBER_OLD,
              --P_COSTO_SOLICITUD,
              P_PERIODO
      FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;  
      
  
      -- GET - NEXT Periodo
      SELECT NEXT_TERM INTO P_PERIODO_NEXT
      FROM (
        SELECT STVTERM_CODE, LEAD(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) NEXT_TERM
        FROM STVTERM ORDER BY STVTERM_CODE DESC
      ) WHERE STVTERM_CODE = P_PERIODO;
      
      -- OPCION QUE SE MANDE POR PARAMETRO AL SP
--      -- GET COD ATRIBUTO 
--      SELECT  CASE P_CODIGO_SOLICITUD WHEN 'SOL003' THEN 'RECM' -- RECARGO DE MATRICULA
--                                      WHEN 'SOL004' THEN 'RECM' --'RES'  -- RESERVA
--                                      WHEN 'SOL005' THEN 'RECM'
--                                      ELSE '-'
--                                      END
--      INTO P_COD_ATRIBUTO FROM DUAL;
                                      
      DBMS_OUTPUT.PUT_LINE(P_ID_ALUMNO || '--' || P_CODIGO_SOLICITUD || '--' || P_ESTADO_OLD || '--' || P_PERIODO || '--' || P_PERIODO_NEXT || '--' || P_COD_ATRIBUTO);
      
      -- #######################################################################
      -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
      SELECT    SORLCUR_SEQNO,      
                SORLCUR_LEVL_CODE,    
                SORLCUR_CAMP_CODE,        
                SORLCUR_COLL_CODE,
                SORLCUR_DEGC_CODE,  
                SORLCUR_PROGRAM,      
                SORLCUR_TERM_CODE_ADMIT,  
                --SORLCUR_STYP_CODE,
                SORLCUR_STYP_CODE,  
                SORLCUR_RATE_CODE,    
                SORLFOS_DEPT_CODE   
      INTO      C_SEQNO,
                C_ALUM_NIVEL, 
                C_ALUM_CAMPUS, 
                C_ALUM_ESCUELA, 
                C_ALUM_GRADO, 
                C_ALUM_PROGRAMA, 
                C_ALUM_PERD_ADM,
                C_ALUM_TIPO_ALUM,
                C_ALUM_TARIFA,
                C_ALUM_DEPARTAMENTO
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                        SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                        SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_ID_ALUMNO 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;
      
      DBMS_OUTPUT.PUT_LINE(C_SEQNO || '--' || C_ALUM_NIVEL  || '--' ||  C_ALUM_CAMPUS  || '--' || C_ALUM_ESCUELA  || '--' || C_ALUM_GRADO  
      || '--' || C_ALUM_PROGRAMA  || '--' || C_ALUM_PERD_ADM || '--' || C_ALUM_TIPO_ALUM || '--' || C_ALUM_TARIFA || '--' || C_ALUM_DEPARTAMENTO);
        
      -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
      SELECT SFRRGFE_DETL_CODE, SFRRGFE_MIN_CHARGE INTO P_COD_DETALLE, C_CARGO_MINIMO
      FROM SFRRGFE 
      WHERE SFRRGFE_TERM_CODE = P_PERIODO AND SFRRGFE_ATTS_CODE = P_COD_ATRIBUTO AND SFRRGFE_TYPE = 'STUDENT'
      AND NVL(NVL(SFRRGFE_LEVL_CODE, c_alum_nivel),'-')           = NVL(NVL(c_alum_nivel, SFRRGFE_LEVL_CODE),'-')--------------------- Nivel
      AND NVL(NVL(SFRRGFE_CAMP_CODE, c_alum_campus),'-')          = NVL(NVL(c_alum_campus, SFRRGFE_CAMP_CODE),'-')  ----------------- Campus (sede)
      AND NVL(NVL(SFRRGFE_COLL_CODE, c_alum_escuela),'-')         = NVL(NVL(c_alum_escuela, SFRRGFE_COLL_CODE),'-') --------------- Escuela 
      AND NVL(NVL(SFRRGFE_DEGC_CODE, c_alum_grado),'-')           = NVL(NVL(c_alum_grado, SFRRGFE_DEGC_CODE),'-') ----------- Grado
      AND NVL(NVL(SFRRGFE_PROGRAM, c_alum_programa),'-')          = NVL(NVL(c_alum_programa, SFRRGFE_PROGRAM),'-') ---------- Programa
      AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm),'-')  = NVL(NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT),'-') --------- Periodo Admicion
      -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
      -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
      -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
      AND NVL(NVL(SFRRGFE_DEPT_CODE, c_alum_departamento),'-')    = NVL(NVL(c_alum_departamento, SFRRGFE_DEPT_CODE),'-') ------------ Departamento
      -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
      AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum),'-') = NVL(NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') ------ Tipo Alumno Curriculum
      AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa),'-')   = NVL(NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-') ------- Trf Curriculum (Escala)
      ;
        
      SELECT TBBDETC_DESC INTO NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;
      --NO_SERVICIO := 'Service Request No ' || to_char(P_NUMERO_SOLICITUD);
      
      DBMS_OUTPUT.PUT_LINE( C_CARGO_MINIMO ||'--'|| NOM_SERVICIO ||'--'|| P_COD_DETALLE);
      
      -- #######################################################################
      -- SET ATRIBUTO alumno
      INSERT INTO SGRSATT
        (
          SGRSATT_PIDM, 
          SGRSATT_TERM_CODE_EFF,
          SGRSATT_ATTS_CODE,
          SGRSATT_ACTIVITY_DATE,
          SGRSATT_STSP_KEY_SEQUENCE
        )
      SELECT P_ID_ALUMNO, P_PERIODO, P_COD_ATRIBUTO, SYSDATE, NULL
      FROM DUAL
      WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                    WHERE SGRSATT_PIDM = P_ID_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
                    AND (SGRSATT_ATTS_CODE = P_COD_ATRIBUTO OR SGRSATT_ATTS_CODE IS NULL));
      P_INDICADOR := SQL%ROWCOUNT;  
      -- ACTUALIZAR en caso exista NULL con el ATRIBUTO para el periodo.
      IF P_INDICADOR = 0  THEN
          UPDATE SGRSATT SET SGRSATT_ATTS_CODE = P_COD_ATRIBUTO
          WHERE SGRSATT_PIDM = P_ID_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
          AND SGRSATT_ATTS_CODE IS NULL 
          AND NOT EXISTS (SELECT SGRSATT_PIDM FROM SGRSATT 
                    WHERE SGRSATT_PIDM = P_ID_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO
                    AND SGRSATT_ATTS_CODE = P_COD_ATRIBUTO);
      END IF;
            
      DBMS_OUTPUT.PUT_LINE('PERIODO ' || P_INDICADOR);  
      
      -- SET - ATRIBUTO FINALIZAR (LIMITAR VALIDES del atributo A UN SOLO PERIODO)
      INSERT INTO SGRSATT
        ( 
          SGRSATT_PIDM, 
          SGRSATT_TERM_CODE_EFF,
          SGRSATT_ATTS_CODE,
          SGRSATT_ACTIVITY_DATE,
          SGRSATT_STSP_KEY_SEQUENCE
        )
      SELECT P_ID_ALUMNO, P_PERIODO_NEXT, NULL, SYSDATE, NULL
      FROM DUAL
      WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                        WHERE SGRSATT_PIDM = P_ID_ALUMNO 
                        AND SGRSATT_TERM_CODE_EFF = P_PERIODO_NEXT
                    );
      P_INDICADOR := SQL%ROWCOUNT;
      
      DBMS_OUTPUT.PUT_LINE('PERIODO FINALIZACION ' || P_INDICADOR);  
      
      -- #######################################################################
      -- GENERAR DEUDA
      TB_RECEIVABLE.p_create ( p_pidm                 =>  P_ID_ALUMNO,        -- PIDEM ALUMNO
                               p_term_code            =>  P_PERIODO,          -- DETALLE
                               p_detail_code          =>  P_COD_DETALLE,   -- CODIGO DETALLE 
                               p_user                 =>  USER,               -- USUARIO
                               p_entry_date           =>  SYSDATE,     
                               p_amount               =>  C_CARGO_MINIMO,
                               p_effective_date       =>  SYSDATE + 3 ,
                               p_bill_date            =>  NULL,    
                               p_due_date             =>  NULL,    
                               p_desc                 =>  NOM_SERVICIO,    
                               p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                               p_tran_number_paid     =>  NULL,     -- numero de transaccion que será pagara
                               p_crossref_pidm        =>  NULL,    
                               p_crossref_number      =>  NULL,    
                               p_crossref_detail_code =>  NULL,     
                               p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                               p_acct_feed_ind        =>  'Y',
                               p_session_number       =>  0,    
                               p_cshr_end_date        =>  NULL,    
                               p_crn                  =>  NULL,
                               p_crossref_srce_code   =>  NULL,
                               p_loc_mdt              =>  NULL,
                               p_loc_mdt_seq          =>  NULL,    
                               p_rate                 =>  NULL,    
                               p_units                =>  NULL,     
                               p_document_number      =>  NULL,    
                               p_trans_date           =>  NULL,    
                               p_payment_id           =>  NULL,    
                               p_invoice_number       =>  NULL,    
                               p_statement_date       =>  NULL,    
                               p_inv_number_paid      =>  NULL,    
                               p_curr_code            =>  NULL,    
                               p_exchange_diff        =>  NULL,    
                               p_foreign_amount       =>  NULL,    
                               p_late_dcat_code       =>  NULL,    
                               p_atyp_code            =>  NULL,    
                               p_atyp_seqno           =>  NULL,    
                               p_card_type_vr         =>  NULL,    
                               p_card_exp_date_vr     =>  NULL,     
                               p_card_auth_number_vr  =>  NULL,    
                               p_crossref_dcat_code   =>  NULL,    
                               p_orig_chg_ind         =>  NULL,    
                               p_ccrd_code            =>  NULL,    
                               p_merchant_id          =>  NULL,    
                               p_data_origin          =>  'WorkFlow',    
                               p_override_hold        =>  'N',     
                               p_tran_number_out      =>  P_TRAN_NUMBER, 
                               p_rowid_out            =>  P_ROWID);


      DBMS_OUTPUT.PUT_LINE( P_TRAN_NUMBER ||'--'|| P_ROWID);
              
      -- #######################################################################
      -- Procesar DEUDA
      SFKFEES.p_processfeeassessment (  P_PERIODO,
                                P_ID_ALUMNO,
                                SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                SYSDATE + 1,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                'Y',          -- create TBRACCD records
                                'SFAREGS',    -- where assessment originated from
                                'Y',          -- commit changes
                                SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                'N',          -- do not ignore SFRFMAX rules
                                RETURN_STATUS_IN_OUT );   -- OUT -- return_status
        
        DBMS_OUTPUT.PUT_LINE( SAVE_ACT_DATE_OUT ||'--'|| RETURN_STATUS_IN_OUT);
        
      COMMIT;
      --ROLLBACK;
EXCEPTION
  WHEN OTHERS THEN
        ROLLBACK;
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END;
      
      
--------------------------------------------------------------------------------
