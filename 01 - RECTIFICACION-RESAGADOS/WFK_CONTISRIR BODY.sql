create or replace PACKAGE BODY WFK_CONTISRIR AS
/*
 WFK_CONTISRIR:
       Conti Package SOLICITUD RECTIFICACIÓN DE INSCRIPCIÓN REZAGADOS
*/
-- FILE NAME..: WFK_CONTISRIR.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISRIR
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              


PROCEDURE P_SET_COD_DETALLE( 
        P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_COD_DETALLE           IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE,
        P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CODDETALLE_ALUMNO
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        NOM_SERVICIO                VARCHAR2(30);
        
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;

        C_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        P_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;

        -- APEC PARAMS
        P_PART_PERIODO          VARCHAR2(9);
        P_APEC_CAMP             VARCHAR2(9);
        P_APEC_DEPT             VARCHAR2(9);
        P_APEC_TERM             VARCHAR2(10);
        P_APEC_IDSECCIONC       VARCHAR2(15);
        P_APEC_FECINIC          DATE;
        P_APEC_MOUNT          NUMBER;
        P_APEC_IDALUMNO       VARCHAR2(10);
BEGIN

      -- #######################################################################
      -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
      SELECT    SORLCUR_SEQNO,      
                SORLCUR_LEVL_CODE,    
                SORLCUR_CAMP_CODE,        
                SORLCUR_COLL_CODE,
                SORLCUR_DEGC_CODE,  
                SORLCUR_PROGRAM,      
                SORLCUR_TERM_CODE_ADMIT,  
                --SORLCUR_STYP_CODE,
                SORLCUR_STYP_CODE,  
                SORLCUR_RATE_CODE,    
                SORLFOS_DEPT_CODE   
      INTO      C_SEQNO,
                C_ALUM_NIVEL, 
                C_ALUM_CAMPUS, 
                C_ALUM_ESCUELA, 
                C_ALUM_GRADO, 
                C_ALUM_PROGRAMA, 
                C_ALUM_PERD_ADM,
                C_ALUM_TIPO_ALUM,
                C_ALUM_TARIFA,
                C_ALUM_DEPARTAMENTO
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                        SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                        SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM <= 1;
      

      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)

            -- GET CRONOGRAMA SECCIONC
            SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
            
            SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
            SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
            SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO

            -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO
            WITH 
                CTE_tblSeccionC AS (
                        -- GET SECCIONC
                        SELECT  "IDSeccionC" IDSeccionC,
                                "FecInic" FecInic
                        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                        WHERE "IDDependencia"='UCCI'
                        AND "IDsede"    = P_APEC_CAMP
                        AND "IDPerAcad" = P_APEC_TERM
                        AND "IDEscuela" = C_ALUM_PROGRAMA
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                        AND "IDSeccionC" <> '15NEX1A'
                        AND SUBSTRB("IDSeccionC",-2,2) IN (
                            -- PARTE PERIODO           
                            SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                        )
                ),
                CTE_tblPersonaAlumno AS (
                        SELECT "IDAlumno" IDAlumno 
                        FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                        WHERE "IDPersona" IN ( 
                            SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                        )
                )
            SELECT "IDAlumno", "IDSeccionC" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
            FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
            AND "IDSede"      = P_APEC_CAMP
            AND "IDPerAcad"   = P_APEC_TERM
            AND "IDEscuela"   = C_ALUM_PROGRAMA
            AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
            AND "IDConcepto"  = P_COD_DETALLE;
            
            -- GET MONTO 
            SELECT "Monto" INTO P_APEC_MOUNT
            FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDSede"      = P_APEC_CAMP
            AND "IDPerAcad"   = P_APEC_TERM
            AND "IDSeccionC"  = P_APEC_IDSECCIONC
            AND "IDConcepto"  = P_COD_DETALLE;

            -- UPDATE MONTO(s)
            UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            SET "Cargo" = "Cargo" + P_APEC_MOUNT
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    = P_APEC_IDALUMNO
            AND "IDSede"      = P_APEC_CAMP
            AND "IDPerAcad"   = P_APEC_TERM
            AND "IDEscuela"   = C_ALUM_PROGRAMA
            AND "IDSeccionC"  = P_APEC_IDSECCIONC
            AND "IDConcepto"  = P_COD_DETALLE;
            
            P_TRAN_NUMBER := 0;

      ELSE -- 1 ---> BANNER

              -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
              SELECT SFRRGFE_MIN_CHARGE INTO C_CARGO_MINIMO
              FROM SFRRGFE 
              WHERE SFRRGFE_TERM_CODE = P_PERIODO AND SFRRGFE_DETL_CODE = P_COD_DETALLE AND SFRRGFE_TYPE = 'STUDENT'
              AND NVL(NVL(SFRRGFE_LEVL_CODE, c_alum_nivel),'-')           = NVL(NVL(c_alum_nivel, SFRRGFE_LEVL_CODE),'-')--------------------- Nivel
              AND NVL(NVL(SFRRGFE_CAMP_CODE, c_alum_campus),'-')          = NVL(NVL(c_alum_campus, SFRRGFE_CAMP_CODE),'-')  ----------------- Campus (sede)
              AND NVL(NVL(SFRRGFE_COLL_CODE, c_alum_escuela),'-')         = NVL(NVL(c_alum_escuela, SFRRGFE_COLL_CODE),'-') --------------- Escuela 
              AND NVL(NVL(SFRRGFE_DEGC_CODE, c_alum_grado),'-')           = NVL(NVL(c_alum_grado, SFRRGFE_DEGC_CODE),'-') ----------- Grado
              AND NVL(NVL(SFRRGFE_PROGRAM, c_alum_programa),'-')          = NVL(NVL(c_alum_programa, SFRRGFE_PROGRAM),'-') ---------- Programa
              AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm),'-')  = NVL(NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT),'-') --------- Periodo Admicion
              -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
              -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
              -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
              AND NVL(NVL(SFRRGFE_DEPT_CODE, c_alum_departamento),'-')    = NVL(NVL(c_alum_departamento, SFRRGFE_DEPT_CODE),'-') ------------ Departamento
              -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
              AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum),'-') = NVL(NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') ------ Tipo Alumno Curriculum
              AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa),'-')   = NVL(NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------- Trf Curriculum (Escala)

              -- GET - Descripcion de CODIGO DETALLE  
              SELECT TBBDETC_DESC INTO NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;
            
              -- #######################################################################
              -- GENERAR DEUDA
              TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM_ALUMNO,        -- PIDEM ALUMNO
                                       p_term_code            =>  P_PERIODO,          -- DETALLE
                                       p_detail_code          =>  P_COD_DETALLE,      -- CODIGO DETALLE 
                                       p_user                 =>  USER,               -- USUARIO
                                       p_entry_date           =>  SYSDATE,     
                                       p_amount               =>  C_CARGO_MINIMO,
                                       p_effective_date       =>  SYSDATE,
                                       p_bill_date            =>  NULL,    
                                       p_due_date             =>  NULL,    
                                       p_desc                 =>  NOM_SERVICIO,    
                                       p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                       p_tran_number_paid     =>  NULL,     -- numero de transaccion que será pagara
                                       p_crossref_pidm        =>  NULL,    
                                       p_crossref_number      =>  NULL,    
                                       p_crossref_detail_code =>  NULL,     
                                       p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                       p_acct_feed_ind        =>  'Y',
                                       p_session_number       =>  0,    
                                       p_cshr_end_date        =>  NULL,    
                                       p_crn                  =>  NULL,
                                       p_crossref_srce_code   =>  NULL,
                                       p_loc_mdt              =>  NULL,
                                       p_loc_mdt_seq          =>  NULL,    
                                       p_rate                 =>  NULL,    
                                       p_units                =>  NULL,     
                                       p_document_number      =>  NULL,    
                                       p_trans_date           =>  NULL,    
                                       p_payment_id           =>  NULL,    
                                       p_invoice_number       =>  NULL,    
                                       p_statement_date       =>  NULL,    
                                       p_inv_number_paid      =>  NULL,    
                                       p_curr_code            =>  NULL,    
                                       p_exchange_diff        =>  NULL,    
                                       p_foreign_amount       =>  NULL,    
                                       p_late_dcat_code       =>  NULL,    
                                       p_atyp_code            =>  NULL,    
                                       p_atyp_seqno           =>  NULL,    
                                       p_card_type_vr         =>  NULL,    
                                       p_card_exp_date_vr     =>  NULL,     
                                       p_card_auth_number_vr  =>  NULL,    
                                       p_crossref_dcat_code   =>  NULL,    
                                       p_orig_chg_ind         =>  NULL,    
                                       p_ccrd_code            =>  NULL,    
                                       p_merchant_id          =>  NULL,    
                                       p_data_origin          =>  'WorkFlow',    
                                       p_override_hold        =>  'N',     
                                       p_tran_number_out      =>  P_TRAN_NUMBER, 
                                       p_rowid_out            =>  P_ROWID);
      
      END IF;


      COMMIT;
EXCEPTION
  WHEN OTHERS THEN
        ROLLBACK;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_COD_DETALLE;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


FUNCTION F_GET_SIDEUDA_ALUMNO
              (
                P_PIDM_ALUMNO         SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_TRAN_NUMBER         TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
                P_PERIODO             SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
                P_COD_DETALLE         SFRRGFE.SFRRGFE_DETL_CODE%TYPE  ------ APEC
              ) RETURN BOOLEAN 
              
/* ===================================================================================================================
  NOMBRE    : F_GET_SIDEUDA_ALUMNO
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un código de detalle verifique la existencia de deuda generada por 
              la solicitud del alumno para el periodo correspondiente, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  =================================================================================================================== */
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR        NUMBER := 0;
      
      -- APEC PARAMS
      P_SERVICE               VARCHAR2(10);
      P_PART_PERIODO          VARCHAR2(9);
      P_APEC_CAMP             VARCHAR2(9);
      P_APEC_DEPT             VARCHAR2(9);
      P_APEC_TERM             VARCHAR2(10);
      P_APEC_IDSECCIONC       VARCHAR2(15);
      P_APEC_FECINIC          DATE;
      P_APEC_DEUDA            NUMBER;
      P_APEC_IDALUMNO         VARCHAR2(10);
      C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
      C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
      C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
      C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
      C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
      C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
      C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
      C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
      C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
      C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
BEGIN
--
    --#################################----- APEC -----#######################################
    --########################################################################################
    -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
    IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          -- #######################################################################
          -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
          SELECT    SORLCUR_SEQNO,      
                    SORLCUR_LEVL_CODE,    
                    SORLCUR_CAMP_CODE,        
                    SORLCUR_COLL_CODE,
                    SORLCUR_DEGC_CODE,  
                    SORLCUR_PROGRAM,      
                    SORLCUR_TERM_CODE_ADMIT,  
                    --SORLCUR_STYP_CODE,
                    SORLCUR_STYP_CODE,  
                    SORLCUR_RATE_CODE,    
                    SORLFOS_DEPT_CODE   
          INTO      C_SEQNO,
                    C_ALUM_NIVEL, 
                    C_ALUM_CAMPUS, 
                    C_ALUM_ESCUELA, 
                    C_ALUM_GRADO, 
                    C_ALUM_PROGRAMA, 
                    C_ALUM_PERD_ADM,
                    C_ALUM_TIPO_ALUM,
                    C_ALUM_TARIFA,
                    C_ALUM_DEPARTAMENTO
          FROM (
                  SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                            SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                            SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
                  FROM SORLCUR        INNER JOIN SORLFOS
                        ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                        AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                      AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                      -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                      AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
          ) WHERE ROWNUM <= 1;
          
          -- GET CRONOGRAMA SECCIONC
          SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                            WHEN 'UPGT' THEN 'W' 
                                            WHEN 'UREG' THEN 'R' 
                                            WHEN 'UPOS' THEN '-' 
                                            WHEN 'ITEC' THEN '-' 
                                            WHEN 'UCIC' THEN '-' 
                                            WHEN 'UCEC' THEN '-' 
                                            WHEN 'ICEC' THEN '-' 
                                            ELSE '1' END ||
                          CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                            WHEN 'F01' THEN 'A' 
                                            WHEN 'F02' THEN 'L' 
                                            WHEN 'F03' THEN 'C' 
                                            WHEN 'V00' THEN 'V' 
                                            ELSE '9' END
                  INTO P_PART_PERIODO
                  FROM STVCAMP,STVDEPT 
                  WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
          
          SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
          SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
          SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
          
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = P_APEC_CAMP
                      AND "IDPerAcad" = P_APEC_TERM
                      AND "IDEscuela" = C_ALUM_PROGRAMA
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                      AND "IDSeccionC" <> '15NEX1A'
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO           
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                      )
              )
          SELECT "IDAlumno", "Deuda" INTO P_APEC_IDALUMNO, P_APEC_DEUDA
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
          AND "IDConcepto"  = P_COD_DETALLE;
          
          P_INDICADOR := P_APEC_DEUDA;
          
          COMMIT;
          
    ELSE --**************************** 1 ---> BANNER ***************************
          
          -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
          TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
          COMMIT;
              
          -- GET deuda
          SELECT COUNT(TZRCDAB_AMOUNT)
          INTO   P_INDICADOR
          FROM   TZRCDAB
          WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
          AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
          --AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE
          AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
          
    END IF;        
      
    RETURN(P_INDICADOR > 0);

END F_GET_SIDEUDA_ALUMNO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_PAGO_REC
              (
                P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_TRAN_NUMBER         IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
                P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE , ------ APEC
                P_COD_DETALLE         IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE , ------ APEC
                P_DEUDA               OUT VARCHAR2
              )
              
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA_ALUMNO
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un código de detalle verifique la existencia de deuda generada por 
              la solicitud del alumno para el periodo correspondiente, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR           NUMBER;
      
      -- APEC PARAMS
      P_SERVICE               VARCHAR2(10);
      P_PART_PERIODO          VARCHAR2(9);
      P_APEC_CAMP             VARCHAR2(9);
      P_APEC_DEPT             VARCHAR2(9);
      P_APEC_TERM             VARCHAR2(10);
      P_APEC_IDSECCIONC       VARCHAR2(15);
      P_APEC_FECINIC          DATE;
      P_APEC_DEUDA            NUMBER;
      P_APEC_IDALUMNO         VARCHAR2(10);
      C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
      C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
      C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
      C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
      C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
      C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
      C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
      C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
      C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
      C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
BEGIN
--
    
    --#################################----- APEC -----#######################################
    --########################################################################################
    -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
    IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          -- #######################################################################
          -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
          SELECT    SORLCUR_SEQNO,      
                    SORLCUR_LEVL_CODE,    
                    SORLCUR_CAMP_CODE,        
                    SORLCUR_COLL_CODE,
                    SORLCUR_DEGC_CODE,  
                    SORLCUR_PROGRAM,      
                    SORLCUR_TERM_CODE_ADMIT,  
                    --SORLCUR_STYP_CODE,
                    SORLCUR_STYP_CODE,  
                    SORLCUR_RATE_CODE,    
                    SORLFOS_DEPT_CODE   
          INTO      C_SEQNO,
                    C_ALUM_NIVEL, 
                    C_ALUM_CAMPUS, 
                    C_ALUM_ESCUELA, 
                    C_ALUM_GRADO, 
                    C_ALUM_PROGRAMA, 
                    C_ALUM_PERD_ADM,
                    C_ALUM_TIPO_ALUM,
                    C_ALUM_TARIFA,
                    C_ALUM_DEPARTAMENTO
          FROM (
                  SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                            SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                            SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
                  FROM SORLCUR        INNER JOIN SORLFOS
                        ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                        AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                      AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                      -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                      AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
          ) WHERE ROWNUM <= 1;
          
          -- GET CRONOGRAMA SECCIONC
          SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                            WHEN 'UPGT' THEN 'W' 
                                            WHEN 'UREG' THEN 'R' 
                                            WHEN 'UPOS' THEN '-' 
                                            WHEN 'ITEC' THEN '-' 
                                            WHEN 'UCIC' THEN '-' 
                                            WHEN 'UCEC' THEN '-' 
                                            WHEN 'ICEC' THEN '-' 
                                            ELSE '1' END ||
                          CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                            WHEN 'F01' THEN 'A' 
                                            WHEN 'F02' THEN 'L' 
                                            WHEN 'F03' THEN 'C' 
                                            WHEN 'V00' THEN 'V' 
                                            ELSE '9' END
                  INTO P_PART_PERIODO
                  FROM STVCAMP,STVDEPT 
                  WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
          
          SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
          SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
          SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
          
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = P_APEC_CAMP
                      AND "IDPerAcad" = P_APEC_TERM
                      AND "IDEscuela" = C_ALUM_PROGRAMA
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                      AND "IDSeccionC" <> '15NEX1A'
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO           
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                      )
              )
          SELECT "IDAlumno", "Deuda" INTO P_APEC_IDALUMNO, P_APEC_DEUDA
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
          AND "IDConcepto"  = P_COD_DETALLE;
          
          P_INDICADOR := P_APEC_DEUDA;
          
          COMMIT;
    ELSE --**************************** 1 ---> BANNER ***************************
          
          -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
          TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
          COMMIT;
              
          -- GET deuda
          SELECT COUNT(TZRCDAB_AMOUNT)
          INTO   P_INDICADOR
          FROM   TZRCDAB
          WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
          AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
          --AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE
          AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
          
    END IF;        
   
    IF P_INDICADOR > 0 THEN
      P_DEUDA   := 'TRUE';
    ELSE
      P_DEUDA   :='FALSE';
    END IF;

END P_VERIFICA_PAGO_REC;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
  P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_FECHA_VALIDA        OUT VARCHAR2
  )
/* ===================================================================================================================
  NOMBRE    : p_validar_fecha_solicitud
  FECHA     : 25/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : se verifiqua que solicitud del alumno del tipo <P_CODIGO_SOLICITUD>( ejem SOL003), 
              se encuentra dentro del rango de fechas hábiles para atender la solicitud.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
       
BEGIN
--

    ----------------------------------------------------------------------------
        SELECT 
                DECODE(COUNT(*),0,'FALSE','TRUE') 
        INTO P_FECHA_VALIDA 
        FROM SVRSVPR 
        INNER JOIN (
              -- get la configuracion (fechas, estados, etc) de la solicitud P_CODIGO_SOLICITUD 
               SELECT SVRRSRV_SEQ_NO          TEMP_PRIORIDAD, 
                      SVRRSRV_SRVC_CODE       TEMP_SRVC_CODE, 
                      SVRRSRV_INACTIVE_IND,
                      SVRRSST_RSRV_SEQ_NO , 
                      SVRRSST_SRVC_CODE , 
                      SVRRSST_SRVS_CODE       TEMP_SRVS_CODE,
                      SVRRSRV_START_DATE      TEMP_START_DATE,--- fecha finalizacion
                      SVRRSRV_END_DATE        TEMP_END_DATE,----- fecha finalizacion
                      SVRRSRV_DELIVERY_DATE , -- fecha estimada
                      SVVSRVS_LOCKED
               FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
               INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
               ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
               AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
               INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
               ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
               WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD 
               AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activo
               -- AND SVRRSRV_WEB_IND = 'Y' ------------------------ Si esta disponible en WEB
        )TEMP
        ON SVRSVPR_SRVS_CODE        = TEMP_SRVS_CODE
        AND SVRSVPR_SRVC_CODE       =  TEMP_SRVC_CODE
        WHERE (TEMP_START_DATE < SYSDATE AND SYSDATE < TEMP_END_DATE)
        --AND TEMP_PRIORIDAD          IN (SELECT SVRSVPR_RSRV_SEQ_NO FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD)
        AND SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;

      
    ----------------------------------------------------------------------------
--
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VALIDAR_FECHA_SOLICITUD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_DEL_COD_DETALLE (
        P_PIDM_ALUMNO       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TRAN_NUMBER       IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_TERM_PERIODO      IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
        P_CODIGO_DETALLE    IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, ------ APEC
        P_ERROR             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_DEL_CODDETALLE_ALUMNO
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cancela una transaccion(DEUDA) de un alumno en un periodo correspondiente.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS    
        P_DNI_ALUMNO            SPRIDEN.SPRIDEN_ID%TYPE;
        P_INDICADOR             NUMBER;
        P_MESSAGE               EXCEPTION;
        P_FOUND_AMOUNT          TBRACCD.TBRACCD_AMOUNT%TYPE;
        P_COD_DETALLE           TBRACCD.TBRACCD_DETAIL_CODE%TYPE;
        P_NOM_SERVICIO          VARCHAR2(30);
        P_PERIODO               SFBETRM.SFBETRM_TERM_CODE%TYPE;
        P_TRAN_NUMBER_OUT       TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
        P_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
        
        -- APEC PARAMS
        P_SERVICE               VARCHAR2(10);
        P_PART_PERIODO          VARCHAR2(9);
        P_APEC_CAMP             VARCHAR2(9);
        P_APEC_DEPT             VARCHAR2(9);
        P_APEC_TERM             VARCHAR2(10);
        P_APEC_IDSECCIONC       VARCHAR2(15);
        P_APEC_FECINIC          DATE;
        P_APEC_MOUNT          NUMBER;
        P_APEC_IDALUMNO       VARCHAR2(10);
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
BEGIN
--
    
    --#################################----- APEC -----#######################################
    --########################################################################################
    -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
    IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          -- #######################################################################
          -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
          SELECT    SORLCUR_SEQNO,      
                    SORLCUR_LEVL_CODE,    
                    SORLCUR_CAMP_CODE,        
                    SORLCUR_COLL_CODE,
                    SORLCUR_DEGC_CODE,  
                    SORLCUR_PROGRAM,      
                    SORLCUR_TERM_CODE_ADMIT,  
                    --SORLCUR_STYP_CODE,
                    SORLCUR_STYP_CODE,  
                    SORLCUR_RATE_CODE,    
                    SORLFOS_DEPT_CODE   
          INTO      C_SEQNO,
                    C_ALUM_NIVEL, 
                    C_ALUM_CAMPUS, 
                    C_ALUM_ESCUELA, 
                    C_ALUM_GRADO, 
                    C_ALUM_PROGRAMA, 
                    C_ALUM_PERD_ADM,
                    C_ALUM_TIPO_ALUM,
                    C_ALUM_TARIFA,
                    C_ALUM_DEPARTAMENTO
          FROM (
                  SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                            SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                            SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
                  FROM SORLCUR        INNER JOIN SORLFOS
                        ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                        AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                      AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                      -- AND SORLCUR_TERM_CODE   =   P_TERM_PERIODO 
                      AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
          ) WHERE ROWNUM <= 1;
          
          -- GET CRONOGRAMA SECCIONC
          SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                            WHEN 'UPGT' THEN 'W' 
                                            WHEN 'UREG' THEN 'R' 
                                            WHEN 'UPOS' THEN '-' 
                                            WHEN 'ITEC' THEN '-' 
                                            WHEN 'UCIC' THEN '-' 
                                            WHEN 'UCEC' THEN '-' 
                                            WHEN 'ICEC' THEN '-' 
                                            ELSE '1' END ||
                          CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                            WHEN 'F01' THEN 'A' 
                                            WHEN 'F02' THEN 'L' 
                                            WHEN 'F03' THEN 'C' 
                                            WHEN 'V00' THEN 'V' 
                                            ELSE '9' END
                  INTO P_PART_PERIODO
                  FROM STVCAMP,STVDEPT 
                  WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
          
          SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
          SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_PERIODO ;-- PERIODO
          SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
          
          -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_DEUDA
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = P_APEC_CAMP
                      AND "IDPerAcad" = P_APEC_TERM
                      AND "IDEscuela" = C_ALUM_PROGRAMA
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                      AND "IDSeccionC" <> '15NEX1A'
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO           
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                      )
              )
          SELECT "IDAlumno", "IDSeccionC" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
          AND "IDConcepto"  = P_CODIGO_DETALLE;
          
          -- GET MONTO 
          SELECT "Monto" INTO P_APEC_MOUNT
          FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDSeccionC"  = P_APEC_IDSECCIONC
          AND "IDConcepto"  = P_CODIGO_DETALLE;
          
          -- UPDATE MONTO(s)
          UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          SET   "Cargo" = "Cargo" - P_APEC_MOUNT
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    = P_APEC_IDALUMNO
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  = P_APEC_IDSECCIONC
          AND "IDConcepto"  = P_CODIGO_DETALLE;
          -- AND "Cargo"       >= P_APEC_MOUNT;
          
    ELSE --**************************** 1 ---> BANNER ***************************    
          
          -- GET DNI alumno
          SELECT SPRIDEN_ID INTO P_DNI_ALUMNO FROM SPRIDEN WHERE SPRIDEN_PIDM = P_PIDM_ALUMNO AND SPRIDEN_CHANGE_IND IS NULL;
          
          -- Validar si la transaccion (DEUDA) NO tubo algun movimiento.
          SELECT COUNT(*) INTO P_INDICADOR
          FROM (
                  -- : listar LOS CODIGOS QUE YA TIENEN ALGUN MOVIMIENTO. : TRAN , CHG 
                  SELECT TBRAPPL_PIDM TEMP_PIDM ,TBRAPPL_PAY_TRAN_NUMBER TEMP_PAY_TRAN_NUMBER
                  FROM TBRAPPL
                  WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM_ALUMNO
                  UNION 
                  SELECT TBRAPPL_PIDM, TBRAPPL_CHG_TRAN_NUMBER
                  FROM TBRAPPL
                  WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM_ALUMNO
          ) WHERE P_TRAN_NUMBER = TEMP_PAY_TRAN_NUMBER;
          
          IF P_INDICADOR = 1 THEN
                RAISE P_MESSAGE;
          END IF;              
              
          -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
          TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
          COMMIT;
              
          -- GET , COD_DETALLE , AMOUNT(NEGATIVO) de la transaccion a cancelar  
          SELECT  (TZRCDAB_AMOUNT * (-1)), 
                  TZRCDAB_DETAIL_CODE, 
                  TZRCDAB_TERM_CODE
          INTO    P_FOUND_AMOUNT,
                  P_COD_DETALLE, 
                  P_PERIODO
          FROM   TZRCDAB
          WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
          AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
          AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
          
          -- GET DESCRIPCION del cod_detalle
          SELECT TBBDETC_DESC INTO P_NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;
      
          -- #######################################################################
          -- GENERAR DEUDA
          TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM_ALUMNO,        -- PIDEM ALUMNO
                                   p_term_code            =>  P_PERIODO,          -- DETALLE
                                   p_detail_code          =>  P_COD_DETALLE,      -- CODIGO DETALLE  -- mismo codigo detalle
                                   p_user                 =>  USER,               -- USUARIO
                                   p_entry_date           =>  SYSDATE,     
                                   p_amount               =>  P_FOUND_AMOUNT,
                                   p_effective_date       =>  SYSDATE,
                                   p_bill_date            =>  NULL,    
                                   p_due_date             =>  NULL,    
                                   p_desc                 =>  P_NOM_SERVICIO,       -- referencia por consultar 
                                   p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                   p_tran_number_paid     =>  P_TRAN_NUMBER,      -- numero de transaccion que será pagara
                                   p_crossref_pidm        =>  NULL,    
                                   p_crossref_number      =>  NULL,    
                                   p_crossref_detail_code =>  NULL,     
                                   p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                   p_acct_feed_ind        =>  'Y',
                                   p_session_number       =>  0,    
                                   p_cshr_end_date        =>  NULL,    
                                   p_crn                  =>  NULL,
                                   p_crossref_srce_code   =>  NULL,
                                   p_loc_mdt              =>  NULL,
                                   p_loc_mdt_seq          =>  NULL,    
                                   p_rate                 =>  NULL,    
                                   p_units                =>  NULL,     
                                   p_document_number      =>  NULL,    
                                   p_trans_date           =>  NULL,    
                                   p_payment_id           =>  NULL,    
                                   p_invoice_number       =>  NULL,    
                                   p_statement_date       =>  NULL,    
                                   p_inv_number_paid      =>  NULL,    
                                   p_curr_code            =>  NULL,    
                                   p_exchange_diff        =>  NULL,    
                                   p_foreign_amount       =>  NULL,    
                                   p_late_dcat_code       =>  NULL,    
                                   p_atyp_code            =>  NULL,    
                                   p_atyp_seqno           =>  NULL,    
                                   p_card_type_vr         =>  NULL,    
                                   p_card_exp_date_vr     =>  NULL,     
                                   p_card_auth_number_vr  =>  NULL,    
                                   p_crossref_dcat_code   =>  NULL,    
                                   p_orig_chg_ind         =>  NULL,    
                                   p_ccrd_code            =>  NULL,    
                                   p_merchant_id          =>  NULL,    
                                   p_data_origin          =>  'WORKFLOW',    
                                   p_override_hold        =>  'N',     
                                   p_tran_number_out      =>  P_TRAN_NUMBER_OUT, 
                                   p_rowid_out            =>  P_ROWID);
      
            ------------------------------------------------------------------------------  
              
                -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
                TZJAPOL.p_run_proc_tvrappl(P_DNI_ALUMNO);
            
            ------------------------------------------------------------------------------  
    END IF;
    
    
    COMMIT;
EXCEPTION
  WHEN P_MESSAGE THEN
        P_ERROR  := '- La deuda tubo movimientos por lo que no se puede cancelar directamente.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_DEL_COD_DETALLE;   


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_AN                  OUT NUMBER,
  P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 12/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : cambia el estado de la solicitud (XXXXXX)

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   10/03/2016    RMALLQUI      Se agrego el parametro P_AN paravalidar si la solicitud este ANULADO.
  =================================================================================================================== */
AS
            P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
            P_ESTADO_PREVIOUS         SVVSRVS.SVVSRVS_CODE%TYPE;
            P_FECHA_SOL               SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
            P_INDICADOR               NUMBER;
            E_INVALID_ESTADO          EXCEPTION;
            V_PRIORIDAD               SVRSVPR.SVRSVPR_RSRV_SEQ_NO%TYPE;
BEGIN
--
  ----------------------------------------------------------------------------
        
        -- GET tipo de solicitud por ejemplo "SOL003" 
        SELECT  SVRSVPR_SRVC_CODE, 
                SVRSVPR_SRVS_CODE,
                SVRSVPR_RECEPTION_DATE,
                SVRSVPR_RSRV_SEQ_NO
        INTO    P_CODIGO_SOLICITUD,
                P_ESTADO_PREVIOUS,
                P_FECHA_SOL,
                V_PRIORIDAD
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
        
        -- SI EL ESTADO ES ANULADO (AN)
        P_AN := 0;
        IF P_ESTADO_PREVIOUS = 'AN' THEN
              P_AN := 1;
        END IF;
        
        -- VALIDAR que :
            -- El ESTADO actual sea modificable segun la configuracion.
            -- El ETSADO enviado pertenesca entre los asignados al tipo de SOLICTUD.
        SELECT COUNT(*) INTO P_INDICADOR
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
        AND SVRSVPR_SRVS_CODE IN (
              -- Estados de solic. MODIFICABLES segun las reglas y configuracion
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              --AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        )
        AND P_ESTADO IN (
              -- Estados de solic. configurados
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              --AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        );
        
        
        IF P_INDICADOR = 0 THEN
              RAISE E_INVALID_ESTADO;
        ELSIF (P_ESTADO_PREVIOUS <> P_ESTADO) THEN
              -- UPDATE SOLICITUD
              UPDATE SVRSVPR
                SET SVRSVPR_SRVS_CODE = P_ESTADO,  --AN , AP , RE entre otros
                --SVRSVPR_ACCD_TRAN_NUMBER = P_TRAN_NUMBER_OLD,
                --SVRSVPR_BILLING_IND = 'N', -- CASE WHEN P_ESTADO = 'AP' THEN 'Y' ELSE 'N' END,
                SVRSVPR_ACTIVITY_DATE = SYSDATE,
                SVRSVPR_DATA_ORIGIN = 'WorkFlow'
              WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
              AND SVRSVPR_SRVS_CODE IN (
                        -- Estados de solic. MODIFICABLES segun las reglas y configuracion
                        SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                        FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                        INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                        ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                        AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                        INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                        ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                        WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                        AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                        AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
                        AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                        --AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
                )
              AND P_ESTADO IN (
                      -- Estados de solic. configurados
                      SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                      FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                      INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                      ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                      AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                      INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                      ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                      WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                      AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                      AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                      --AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
              );
              
              COMMIT;
        END IF;
        
EXCEPTION
  WHEN E_INVALID_ESTADO THEN
          P_ERROR  := '- El "ESTADO" actual no es modificable ò no se encuentra entre los ESTADOS disponibles para la solicitud.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE p_set_gr_inscripc_abierto (
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
      P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
      P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_set_grupo_inscripcion_abierto
  FECHA     : 27/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asignar el código de grupo de inscripción para la rectificación en base al id, la modalidad y a la sede del estudiante.

                          Ejemplo: 99-99WF01
------------------------------------------------------------------------------
    Rectificación               Modalidad                 Sede            
99-99 – Rectificación     R – Regular                   S01 – Huancayo
                          W – Gente que trabaja         F01 – Arequipa 
                          V – Virtual                   F02 – Lima  
                                                        F03 – Cusco
                                                        V00 – Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      P_RPGRP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_MODALIDAD_SEDE      VARCHAR2(4);
      P_MODALIDAD_SEDE2     VARCHAR2(4);
      
      P_MESSAGE             EXCEPTION;
      P_MESSAGE2            EXCEPTION;
      P_MESSAGE3            EXCEPTION;
      P_INDICADOR           NUMBER := 0;
      P_ROW_UPDATE          NUMBER := 0;
      P_COD_RECTF           VARCHAR2(15) := '99-99';
BEGIN
--
      
      P_INDICADOR := 1;      
      SELECT SFBRGRP_RGRP_CODE INTO P_RPGRP
      FROM SFBRGRP 
      WHERE SFBRGRP_PIDM = P_ID_ALUMNO
      AND SFBRGRP_TERM_CODE = P_PERIODO;
      
      -- OBTENER CADENA DE MODALIDAD Y SEDE  del CODIGO DE GRUPO  
      P_MODALIDAD_SEDE := SUBSTR(P_RPGRP,6,4);
      
      -- VALIDACION : CODIGO grupo insc. y datos proporcionados
      SELECT CASE P_MODALIDAD_ALUMNO 
                WHEN 'UVIR' THEN 'V' --#UC-SEMI PRESENCIAL (EV)
                WHEN 'UPGT' THEN 'W' --#UC-SEMI PRESENCIAL (GT)
                WHEN 'UREG' THEN 'R' --#UC-PRESENCIAL
                ELSE '' END INTO P_MODALIDAD_SEDE2 FROM DUAL;
            
      IF (P_MODALIDAD_SEDE != (P_MODALIDAD_SEDE2 || P_COD_SEDE )) THEN
              RAISE P_MESSAGE2;
      ELSIF (P_RPGRP = (P_COD_RECTF || P_MODALIDAD_SEDE)) THEN
              RAISE P_MESSAGE3;
      END IF;
    
      
      -- CODIGO DEL GRUPO DE RECTIFICACIÒN : 99-99<cod_modadlidad><cod_sede>
      SELECT CONCAT(P_COD_RECTF,P_MODALIDAD_SEDE) INTO P_RPGRP_NEW FROM DUAL;
      
      -- Validar que exista el codigo del grupo de inscripcion
      P_INDICADOR := 2;    
      SELECT SFBWCTL_RGRP_CODE 
      INTO P_RPGRP_NEW
      FROM SFBWCTL
      WHERE SFBWCTL_RGRP_CODE  = P_RPGRP_NEW
      AND  SFBWCTL_TERM_CODE = P_PERIODO;
      
      -- ASIGNAR GRUPO DE RECTIFICACIÒN
      UPDATE SFBRGRP 
        -- SELECT SFBRGRP_TERM_CODE, SFBRGRP_PIDM, SFBRGRP.* 
        SET SFBRGRP_RGRP_CODE     = P_RPGRP_NEW,
            SFBRGRP_DATA_ORIGIN   = 'WorkFlow',
            SFBRGRP_ACTIVITY_DATE = SYSDATE
      WHERE SFBRGRP_PIDM = P_ID_ALUMNO
      AND SFBRGRP_TERM_CODE = P_PERIODO
      AND EXISTS (
            ------ LA DEFINICION DE LA VISTA SFVRGRP
            SELECT SFBWCTL.SFBWCTL_TERM_CODE,
              SFBWCTL.SFBWCTL_RGRP_CODE,
              SFBWCTL.SFBWCTL_PRIORITY,
              SFRWCTL.SFRWCTL_BEGIN_DATE,
              SFRWCTL.SFRWCTL_END_DATE,
                    SFRWCTL.SFRWCTL_HOUR_BEGIN,
                    SFRWCTL.SFRWCTL_HOUR_END
              FROM SFRWCTL, SFBWCTL
                    WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                    AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                    AND   SFBWCTL.SFBWCTL_RGRP_CODE = P_RPGRP_NEW
                    AND   SFRWCTL.SFRWCTL_TERM_CODE = P_PERIODO
      );
      P_ROW_UPDATE := P_ROW_UPDATE + SQL%ROWCOUNT;
      --DBMS_OUTPUT.PUT_LINE(P_ROW_UPDATE);
      IF (P_ROW_UPDATE > 1) THEN
            ROLLBACK;
            RAISE P_MESSAGE;
      END IF;

      COMMIT;
EXCEPTION
  WHEN P_MESSAGE THEN
          P_ERROR := 'Se detecto que posee DOBLE grupo de inscripción: ' || P_RPGRP_NEW;
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN P_MESSAGE2 THEN
          P_ERROR := 'El departamento y la sede no coinciden con el grupo de rectificación';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN P_MESSAGE3 THEN
          P_ERROR := 'Advertencia, Ya se encuentra en el grupo de inscripción para su rectificaciòn. No procedio lo solicitado.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN TOO_MANY_ROWS THEN 
          IF (P_INDICADOR = 2) THEN
              P_ERROR := 'Se encontraron mas de un grupo ABIERTO de inscripción: ' || P_RPGRP_NEW;
          ELSIF (P_INDICADOR = 1) THEN
              P_ERROR := 'Se encontraron mas de un grupo INICIAL de inscripción: ' || P_RPGRP_NEW;
          ELSE  P_ERROR := SQLERRM;
          END IF;     
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );  
  WHEN NO_DATA_FOUND THEN
           IF (P_INDICADOR = 2) THEN
              P_ERROR := 'No se encontró el grupo ABIERTO de inscripción: ' || P_RPGRP_NEW;
          ELSIF (P_INDICADOR = 1) THEN
              P_ERROR := 'No se encontró tu grupo INICIAL de inscripción: ' || P_RPGRP_NEW;
          ELSE  P_ERROR := SQLERRM;
          END IF;  
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );  
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END p_set_gr_inscripc_abierto;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE p_set_gr_inscripc_cierre (
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
      P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
      P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_set_gr_inscripc_cierre
  FECHA     : 28/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : asignar el código de grupo de cierre luego de la rectificación de inscripción a asignaturas en base al id, la modalidad y a la sede del estudiante.

                          Ejemplo: 98-98WF01
------------------------------------------------------------------------------
    Rectificación               Modalidad                 Sede            
98-98 – Rectificación     R – Regular                   S01 – Huancayo
                          W – Gente que trabaja         F01 – Arequipa 
                          V – Virtual                   F02 – Lima  
                                                        F03 – Cusco
                                                        V00 – Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      P_RPGRP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_MODALIDAD_SEDE      VARCHAR2(4);
      P_MODALIDAD_SEDE2     VARCHAR2(4);
      
      P_MESSAGE             EXCEPTION;
      P_MESSAGE2            EXCEPTION;
      P_MESSAGE3            EXCEPTION;
      P_INDICADOR           NUMBER := 0;
      P_ROW_UPDATE          NUMBER := 0;
      P_COD_RECTF           VARCHAR2(15) := '98-98';
BEGIN
--
      
      P_INDICADOR := 1;      
      SELECT SFBRGRP_RGRP_CODE INTO P_RPGRP
      FROM SFBRGRP 
      WHERE SFBRGRP_PIDM = P_ID_ALUMNO
      AND SFBRGRP_TERM_CODE = P_PERIODO;
      
      -- OBTENER CADENA DE MODALIDAD Y SEDE  del CODIGO DE GRUPO  
      P_MODALIDAD_SEDE := SUBSTR(P_RPGRP,6,4);
      
      -- VALIDACION : CODIGO grupo insc. y datos proporcionados
      SELECT CASE P_MODALIDAD_ALUMNO 
                WHEN 'UVIR' THEN 'V' --#UC-SEMI PRESENCIAL (EV)
                WHEN 'UPGT' THEN 'W' --#UC-SEMI PRESENCIAL (GT)
                WHEN 'UREG' THEN 'R' --#UC-PRESENCIAL
                ELSE '' END INTO P_MODALIDAD_SEDE2 FROM DUAL;
            
      IF (P_MODALIDAD_SEDE != (P_MODALIDAD_SEDE2 || P_COD_SEDE )) THEN
              RAISE P_MESSAGE2;
      ELSIF (P_RPGRP = (P_COD_RECTF || P_MODALIDAD_SEDE)) THEN
              RAISE P_MESSAGE3;
      END IF;
    
      
      -- CODIGO DEL GRUPO DE RECTIFICACIÒN : 98-98<cod_modadlidad><cod_sede>
      SELECT CONCAT(P_COD_RECTF,P_MODALIDAD_SEDE) INTO P_RPGRP_NEW FROM DUAL;
      
      -- Validar que exista el codigo del grupo de inscripcion
      P_INDICADOR := 2;    
      SELECT SFBWCTL_RGRP_CODE 
      INTO P_RPGRP_NEW
      FROM SFBWCTL
      WHERE SFBWCTL_RGRP_CODE  = P_RPGRP_NEW
      AND  SFBWCTL_TERM_CODE = P_PERIODO;
      
      -- ASIGNAR GRUPO DE RECTIFICACIÒN
      UPDATE SFBRGRP 
        -- SELECT SFBRGRP_TERM_CODE, SFBRGRP_PIDM, SFBRGRP.* 
        SET SFBRGRP_RGRP_CODE   = P_RPGRP_NEW,
            SFBRGRP_USER        = 'WorkFlow',
            SFBRGRP_ACTIVITY_DATE = SYSDATE
      WHERE SFBRGRP_PIDM = P_ID_ALUMNO
      AND SFBRGRP_TERM_CODE = P_PERIODO
      AND EXISTS (
            ------ LA DEFINICION DE LA VISTA SFVRGRP
            SELECT SFBWCTL.SFBWCTL_TERM_CODE,
              SFBWCTL.SFBWCTL_RGRP_CODE,
              SFBWCTL.SFBWCTL_PRIORITY,
              SFRWCTL.SFRWCTL_BEGIN_DATE,
              SFRWCTL.SFRWCTL_END_DATE,
                    SFRWCTL.SFRWCTL_HOUR_BEGIN,
                    SFRWCTL.SFRWCTL_HOUR_END
              FROM SFRWCTL, SFBWCTL
                    WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                    AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                    AND   SFBWCTL.SFBWCTL_RGRP_CODE = P_RPGRP_NEW
                    AND   SFRWCTL.SFRWCTL_TERM_CODE = P_PERIODO
      );
      P_ROW_UPDATE := P_ROW_UPDATE + SQL%ROWCOUNT;
      --DBMS_OUTPUT.PUT_LINE(P_ROW_UPDATE);
      IF (P_ROW_UPDATE > 1) THEN
            ROLLBACK;
            RAISE P_MESSAGE;
      END IF;


      COMMIT;
EXCEPTION
  WHEN P_MESSAGE THEN
          P_ERROR := 'Se detecto que posee DOBLE grupo de inscripcion: ' || P_RPGRP_NEW;
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR ); 
  WHEN P_MESSAGE2 THEN
          P_ERROR := 'El departamento y la sede no coinciden con el grupo de rectificacion';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );   
  WHEN P_MESSAGE3 THEN
          P_ERROR := 'Advertencia, UD. ya se encuentra en el grupo de inscripcion para su rectificaciòn. No procedio lo solicitado.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN TOO_MANY_ROWS THEN 
          IF (P_INDICADOR = 2) THEN
              P_ERROR := 'Se encontraron mas de un grupo de CIERRE para su inscripcion: ' || P_RPGRP_NEW;
          ELSIF (P_INDICADOR = 1) THEN
              P_ERROR := 'Se encontraron mas de un grupo ABIERTO para su inscripción: ' || P_RPGRP_NEW;
          ELSE  P_ERROR := SQLERRM;
          END IF;       
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN NO_DATA_FOUND THEN
           IF (P_INDICADOR = 2) THEN
              P_ERROR := 'No se encontró el codigo del grupo de CIERRE: ' || P_RPGRP_NEW;
          ELSIF (P_INDICADOR = 1) THEN
              P_ERROR := 'No se encontró el grupo ABIERTO para su inscripción: ' || P_RPGRP_NEW;
          ELSE  P_ERROR := SQLERRM;
          END IF;  
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END p_set_gr_inscripc_cierre;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_GET_USUARIO_RRAA (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO_REGACA       OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_get_usuario_regaca
  FECHA     : 03/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.

  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
      P_CORREO                  VARCHAR2(100);
    
      P_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO P_CORREO FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_CORREO_REGACA := P_CORREO_REGACA || P_CORREO || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      P_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_CORREO_REGACA,1,LENGTH(P_CORREO_REGACA) -1) INTO P_CORREO_REGACA
      FROM DUAL
      WHERE SUBSTR(P_CORREO_REGACA,-1,1) = ',';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := 'A ocurrido un problema, se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, se encontraron mas de una ORGANIZACIÒN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre del ROL: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, NO  se encontrò ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO_RRAA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_COMENTARIO_ALUMNO       OUT VARCHAR2,
      P_BEC18                   OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_COMENTARIO_ALUMNO
  FECHA     : 03/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  001   03/03/2017    RMALLQUI    Se agrego parametro de retorno indicando si el alumno es de BEC18
  =================================================================================================================== */
AS
      P_CODIGO_SOLICITUD      SVVSRVC.SVVSRVC_CODE%TYPE;
      P_PIDM                  SPRIDEN.SPRIDEN_PIDM%TYPE;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE      , SVRSVPR_PIDM
      INTO P_CODIGO_SOLICITUD       , P_PIDM
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      
      -- GET COMENTARIO de la solicitud. (comentario PERSONALZIADO.)
      SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO 
      FROM (
            SELECT SVRSVAD_ADDL_DATA_DESC
            FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
            WHERE SVRSRAD_SRVC_CODE = P_CODIGO_SOLICITUD
            AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
            ORDER BY SVRSVAD_ADDL_DATA_SEQ DESC
      ) WHERE ROWNUM = 1;

      -- get si alumno BEC18
      SELECT CASE WHEN DAT = 0 THEN 'No' ELSE 'Si' END  
      INTO P_BEC18 FROM 
      (
            SELECT COUNT(*) DAT FROM SGRSATT 
            WHERE SGRSATT_PIDM = P_PIDM
            AND SGRSATT_ATTS_CODE = 'BE18'
            AND SGRSATT_TERM_CODE_EFF IN (
                SELECT MAX(SGRSATT_TERM_CODE_EFF) FROM SGRSATT
                WHERE SGRSATT_PIDM = P_PIDM
            )
      ) WHERE ROWNUM <= 1;

END P_OBTENER_COMENTARIO_ALUMNO;



--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTISRIR;