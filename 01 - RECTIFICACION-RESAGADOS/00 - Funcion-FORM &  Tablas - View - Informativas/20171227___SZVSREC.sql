/**********************************************************************************************/
/* SZVSREC.sql                                                                                */
/**********************************************************************************************/
/*                                                                                            */
/* Descripci�n corta: Script para generar la vista SZVSREC                                    */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creaci�n del C�digo.                                        LAM           27/DIC/2017   */
/*    --------------------                                                                    */
/*    Se crea la vista SZVSREC para que el estudiante elija si desea realizar el pago por     */
/*    rectificaci�n de matr�cula.                                                             */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/ 

CREATE OR REPLACE VIEW "BANINST1".SZVSREC ("CONFIRM_ID", "CONFIRM") AS 
SELECT 1 CONFIRM_ID, 'Con pago' CONFIRM FROM DUAL UNION
SELECT 0 , 'Sin pago' FROM DUAL;