

FORMA  : SVASVPR
BLOQUE  : SVRSVPR_DETAIL
CAMPO :
TRIGGER : PRE-INSERT

 ------------------------------------------------------------------------------------------------------------
 /*
    Trigguer original obtenida del codigo de BANNER.
 */

DECLARE
       lv_detail  VARCHAR2(4);
       lv_rowid  GB_COMMON.INTERNAL_RECORD_ID_TYPE;
       no_servicio  VARCHAR2(30); -- Add line 7.5.0.2 [BSC:7.5.0.2.3] ENC OCT/12/2010 

  CURSOR PTI_DETAIL IS

       SELECT  SVRRSSO_DETL_CODE
     FROM  SVRRSSO
     WHERE SVRRSSO_RSRV_SEQ_NO= :SVRSVPR_DETAIL.SVRSVPR_RSRV_SEQ_NO
     AND   SVRRSSO_SRVC_CODE  = :SVRSVPR_DETAIL.SVRSVPR_SRVC_CODE
     AND   SVRRSSO_WSSO_CODE  = :SVRSVPR_DETAIL.SVRSVPR_WSSO_CODE ;
                  
BEGIN 

          :SVRSVPR_DETAIL.SVRSVPR_TERM_CODE := bvgkptcl.f_get_current_term;


          IF :SVRSVPR_DETAIL.SVRSVPR_TERM_CODE IS NULL THEN
                :SVRSVPR_DETAIL.SVRSVPR_TERM_CODE:= '999999';
          END IF;

          IF :SVRSVPR_DETAIL.SVRSVPR_BILLING_IND = 'Y' THEN
     
        OPEN PTI_DETAIL ;
        FETCH PTI_DETAIL INTO lv_detail;     
        CLOSE PTI_DETAIL;

         no_servicio := substr (G$_NLS.GET('SVASVPR-0008','FORM','Service Request No ') || to_char ( :SVRSVPR_DETAIL.SVRSVPR_PROTOCOL_SEQ_NO ), 1,30 ); -- 8.5 [BSC:8.5] DLO


        TB_RECEIVABLE.p_create ( p_pidm                 =>  :SVRSVPR_DETAIL.SVRSVPR_PIDM,    
                                 p_term_code            =>  :SVRSVPR_DETAIL.SVRSVPR_TERM_CODE,    
                                 p_detail_code          =>  lv_detail,    
                                 p_user                 =>  :GLOBAL.CURRENT_USER,    
                                 p_entry_date           =>  SYSDATE,     
                                 p_amount               =>  :SVRSVPR_DETAIL.SVRSVPR_PROTOCOL_AMOUNT,
                           p_effective_date       =>   SYSDATE + 3 ,
                           p_bill_date            =>  NULL,    
                           p_due_date             =>  NULL,    
                           p_desc                 =>  no_servicio,    
                           p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                           p_tran_number_paid     =>  NULL,     -- numero de transaccion que será pagara
                           p_crossref_pidm        =>  NULL,    
                           p_crossref_number      =>  NULL,    
                           p_crossref_detail_code =>  NULL,     
                           p_srce_code            =>  'T',    
                           p_acct_feed_ind        =>  'Y',
                           p_session_number       =>  0,    
                           p_cshr_end_date        =>  NULL,    
                           p_crn                  =>  NULL,
                           p_crossref_srce_code   =>  NULL,
                           p_loc_mdt              =>  NULL,
                           p_loc_mdt_seq          =>  NULL,    
                           p_rate                 =>  NULL,    
                           p_units                =>  NULL,     
                           p_document_number      =>  NULL,    
                           p_trans_date           =>  NULL,    
                           p_payment_id           =>  NULL,    
                           p_invoice_number       =>  NULL,    
                           p_statement_date       =>  NULL,    
                           p_inv_number_paid      =>  NULL,    
                           p_curr_code            =>  NULL,    
                           p_exchange_diff        =>  NULL,    
                           p_foreign_amount       =>  NULL,    
                           p_late_dcat_code       =>  NULL,    
                           p_atyp_code            =>  NULL,    
                           p_atyp_seqno           =>  NULL,    
                           p_card_type_vr         =>  NULL,    
                           p_card_exp_date_vr     =>  NULL,     
                           p_card_auth_number_vr  =>  NULL,    
                           p_crossref_dcat_code   =>  NULL,    
                           p_orig_chg_ind         =>  NULL,    
                           p_ccrd_code            =>  NULL,    
                           p_merchant_id          =>  NULL,    
                           p_data_origin          =>  :GLOBAL.DATA_ORIGIN,    
                           p_override_hold        =>  'N',     
                           p_tran_number_out      =>  :SVRSVPR_DETAIL.SVRSVPR_ACCD_TRAN_NUMBER, 
                           p_rowid_out      =>  lv_rowid);

          END IF; 
        EXCEPTION
   WHEN OTHERS THEN
      G$_DISPLAY_ERR_MSG(SQLERRM);
   RAISE FORM_TRIGGER_FAILURE;
END;