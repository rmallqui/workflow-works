/**********************************************************************************************/
/* BWZKSREM.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Rectificación de Matrícula.                                */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Codigo.                                        RML             28/SET/2016 */
/*    --------------------                                                                    */
/*    Creación del paquete de Rectificación de Matrícula.                                     */
/*    Procedure P_SET_COD_DETALLE: Genera una deuda por CODIGO DETALLE.                       */
/*    Procedure P_VERIFICA_PAGO_REC: El objetivo es que en base a un ID y un código de detalle*/ 
/*              verifique la existencia de deuda generada por la solicitud del alumno para el */
/*              periodo correspondiente, divisa PEN.                                          */
/*    Procedure P_VALIDAR_FECHA_SOLICITUD: Se verifiqua que solicitud del alumno del tipo     */
/*              <P_CODIGO_SOLICITUD>( ejem SOL003), se encuentra dentro del rango de fechas   */
/*              habiles para atender la solicitud.                                            */
/*    Procedure P_DEL_COD_DETALLE: Cancela una transaccion(DEUDA) de un alumno en un periodo  */
/*              correspondiente.                                                              */
/*    --Paquete Generico                                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX).         */
/*    Procedure P_SET_GRP_OPEN: Asignar el codigo de grupo de inscripcion para la             */
/*              rectificación en base al id, la modalidad y a la sede del estudiante.         */
/*    Procedure P_SET_GRP_CLOSED: asignar el codigo de grupo de cierre luego de la            */
/*              rectificación de inscripcion a asignaturas en base al id, la modalidad y a la */
/*              sede del estudiante.                                                          */
/*    Procedure P_GET_USUARIO_RRAA: En base a una sede y un rol, el procedimiento LOS CORREOS */
/*              deL(os) responsable(s) de Registros Académicos de esa sede.                   */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene los datos de AUTOSERVICIO ingresados por el     */
/*              estudiante.                                                                   */
/*    Function F_VALIDAR_FECHA_DISPONIBLE: Valida que la fecha ENVIADA este dentro de las 48  */ 
/*             horas para su atencion. se considdera la atencion hasta antes de finalizar el  */
/*             dia de finalizada las 48 horas.                                                */ 
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante cumpla con las condiciones       */
/*              para poder acceder al WF.                                                     */
/*                                                                                            */
/*  2.Actualización P_VERIFICA_PAGO_REC:                          LAM            29/DIC/2017  */
/*    Se renombra el SP P_GET_SIDEUDA_ALUMNO.                                                 */
/*  3.Actualización P_DEL_COD_DETALLE:                            LAM            29/DIC/2017  */
/*    Se renombra el SP P_DEL_CODDETALLE_ALUMNO.                                              */
/*  4.Actualización P_SET_GRP_OPEN:                               LAM            29/DIC/2017  */
/*    Se renombra el SP p_set_gr_inscripc_abierto.                                            */
/*  5.Actualización P_SET_GRP_CLOSED:                             LAM            29/DIC/2017  */
/*    Se renombra el SP p_set_gr_inscripc_cierre.                                             */
/*  6.Actualización P_GET_USUARIO_RRAA:                           LAM            29/DIC/2017  */
/*    Se renombra el SP p_get_usuario_regacad.                                                */
/*  7.Aumento del SP P_VERIFICAR_APTO                             BFV            09/JUL/2018  */
/*    Se aumenta el SP que valida las condiciones de acceso.                                  */
/*    Actualizacion P_SET_GRP_OPEN - P_SET_GRP_CLOSED                                         */
/*    Se aumenta la validación para confirmar que el estudiante ya tenga asignado el grupo.   */
/*  8.Actualizacion Paquete Generico                              BFV            04/OCT/2018  */
/*    Se actualiza el SP de Cambio de estado de solicitud para que se consuma desde un        */
/*      generico.                                                                             */
/*    Actualizacion P_VERIFICAR_APTO                                                          */
/*    Se actualiza el SP para aumentar la restriccion de estudiantes de Beca 18.              */
/*    Actualizacion                                                                           */
/*    Se aumenta en las consultas que trabajan con la tabla SORLCUR la siguiente restriccion  */
/*      "AND SORLCUR_TERM_CODE_END IS NULL" para mejorar la consulta.                         */
/*    Actualizacion F_VALIDA_FECHA_DISPO                                                      */
/*    Se actualiza el calculo de las 48 horas o 2 dias para la validacion de la fecha.        */
/*  9.Actualizacion Consulta                                      BFV            30/ENE/2019  */
/*    Se actualiza y mejora la consulta para obtener la PARTE PERIODO en todo el paquete.     */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/

/**********************************************************************************************/

--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF

/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BWZKSREM AS            

PROCEDURE P_SET_COD_DETALLE( 
        P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_COD_DETALLE           IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE,
        P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_DATE_CARGO            OUT VARCHAR2,
        P_ERROR                 OUT VARCHAR2
);

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_PAGO_REC(
        P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TRAN_NUMBER         IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE , ------ APEC
        P_COD_DETALLE         IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE , ------ APEC
        P_DEUDA               OUT VARCHAR2
);

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_DEL_COD_DETALLE (
        P_PIDM_ALUMNO       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TRAN_NUMBER       IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_TERM_PERIODO      IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
        P_CODIGO_DETALLE    IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, ------ APEC
        P_ERROR             OUT VARCHAR2
);   

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
        P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
        P_AN                  OUT NUMBER,
        P_ERROR               OUT VARCHAR2
);

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_GRP_OPEN (
        P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
        P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
        P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
        P_ERROR               OUT VARCHAR2
);

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_GRP_CLOSED (
        P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
        P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
        P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
        P_ERROR               OUT VARCHAR2
);

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_USUARIO_RRAA (
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
        P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
        P_CORREO_REGACA       OUT VARCHAR2,
        P_ROL_SEDE            OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
);

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_STUDENT_INPUTS(
        P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_COMENTARIO_ALUMNO     OUT VARCHAR2,
        P_TIPO_RECTIFICACION    OUT VARCHAR2,
        P_BEC18                 OUT VARCHAR2
);

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_VALIDA_FECHA_DISPO (
        P_DATE_CARGO   IN VARCHAR2
) RETURN VARCHAR2;  

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
);
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END BWZKSREM;

/**********************************************************************************************/

--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSREM;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSREM FOR BANINST1.BWZKSREM;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSREM
--  START gurgrth BWZKSREM
--  WHENEVER SQLERROR EXIT ROLLBACK

/**********************************************************************************************/