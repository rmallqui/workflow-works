create or replace PACKAGE WFK_CONTISRIR AS
/*
 WFK_CONTISRIR:
       Conti Package SOLICITUD RECTIFICACIÓN DE INSCRIPCIÓN REZAGADOS
*/
-- FILE NAME..: WFK_CONTISRIR.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISRIR
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_SET_COD_DETALLE( 
                    P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                    P_COD_DETALLE           IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE,
                    P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
                    P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
                    P_ERROR                 OUT VARCHAR2
            );

--------------------------------------------------------------------------------

-- VERIFICAR DEUDA : Funcion
FUNCTION F_GET_SIDEUDA_ALUMNO (
              P_PIDM_ALUMNO         SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_TRAN_NUMBER         TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
              P_PERIODO             SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
              P_COD_DETALLE         SFRRGFE.SFRRGFE_DETL_CODE%TYPE  ------ APEC
            ) RETURN BOOLEAN ;
--------------------------------------------------------------------------------
-- VERIFICAR DEUDA : Procedimiento Almacenado
PROCEDURE P_GET_SIDEUDA_ALUMNO (
            P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_TRAN_NUMBER         IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
            P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE , ------ APEC
            P_COD_DETALLE         IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE , ------ APEC
            P_DEUDA               OUT VARCHAR2
          );

--------------------------------------------------------------------------------

PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
          P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
          P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
          P_FECHA_VALIDA        OUT VARCHAR2
        );
              
--------------------------------------------------------------------------------

PROCEDURE P_DEL_CODDETALLE_ALUMNO (
            P_PIDM_ALUMNO       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_TRAN_NUMBER       IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
            P_TERM_PERIODO      IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
            P_CODIGO_DETALLE    IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, ------ APEC
            P_ERROR             OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
            P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
            P_AN                  OUT NUMBER,
            P_ERROR               OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE p_set_gr_inscripc_abierto 
              (
                  P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
                  P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
                  P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
                  P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
                  P_ERROR               OUT VARCHAR2
              );

--------------------------------------------------------------------------------

PROCEDURE p_set_gr_inscripc_cierre 
              (
                  P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
                  P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
                  P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
                  P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
                  P_ERROR               OUT VARCHAR2
              );

--------------------------------------------------------------------------------

PROCEDURE p_get_usuario_regacad (
                P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
                P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
                P_CORREO_REGACA       OUT VARCHAR2,
                P_ROL_SEDE            OUT VARCHAR2,
                P_ERROR               OUT VARCHAR2
          );

--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
              P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
              P_COMENTARIO_ALUMNO       OUT VARCHAR2,
              P_BEC18                   OUT VARCHAR2
          );

--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END WFK_CONTISRIR;