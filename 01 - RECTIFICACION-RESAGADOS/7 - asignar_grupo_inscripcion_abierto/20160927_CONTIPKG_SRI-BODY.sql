create or replace PACKAGE BODY CONTIPKG_SRI AS
/*
 CONTIPKG_SRI:
       Conti Package body SOLICITUD RECTIFICACION DE INSCRIPCION
*/
-- FILE NAME..: CONTIPKG_SRI.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: CONTIPKG_SRI
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              


PROCEDURE p_set_coddetalle_alumno (
  p_id_alumno       IN TBRACCD.TBRACCD_PIDM%TYPE, 
  p_periodo         IN TBRACCD.TBRACCD_TERM_CODE%TYPE , 
  p_codigo_detalle  IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
  p_error           OUT varchar2
  )
/* ===================================================================================================================
  NOMBRE    : p_set_coddetalle_alumno
  FECHA     : 24/08/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Inserta un c�digo de detalle a la cuenta corriente del alumno para un periodo correspondiente.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */

AS
        -- p_id_alumno               TBRACCD.TBRACCD_PIDM%TYPE;
        c_codigo_detalle_dec      TBBDETC.TBBDETC_DESC%TYPE;
        c_user                    TBRACCD.TBRACCD_USER%TYPE;
        c_tran_number             TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
        
        c_alum_nivel              SORLCUR.SORLCUR_LEVL_CODE%TYPE;
        c_alum_campus             SORLCUR.SORLCUR_CAMP_CODE%TYPE;
        c_alum_escuela            SORLCUR.SORLCUR_COLL_CODE%TYPE;
        c_alum_grado              SORLCUR.SORLCUR_DEGC_CODE%TYPE;
        c_alum_programa           SORLCUR.SORLCUR_PROGRAM%TYPE;
        c_alum_perd_adm           SORLCUR.SORLCUR_TERM_CODE_ADMIT%TYPE;
        c_alum_tipo_alum          SORLCUR.SORLCUR_STYP_CODE%TYPE;           -- STVSTYP
        c_alum_tarifa             SORLCUR.SORLCUR_RATE_CODE%TYPE;           -- STVRATE
        c_alum_departamento       SORLFOS.SORLFOS_DEPT_CODE%TYPE;
        
        c_nivel                   SFRRGFE.SFRRGFE_LEVL_CODE%TYPE;           -- STVLEVL
        c_campus                  SFRRGFE.SFRRGFE_CAMP_CODE%TYPE;           -- STVCAMP
        c_escuela                 SFRRGFE.SFRRGFE_COLL_CODE%TYPE;           -- STVCOLL
        c_grado                   SFRRGFE.SFRRGFE_DEGC_CODE%TYPE;           -- STVDEGC
        c_programa                SFRRGFE.SFRRGFE_PROGRAM%TYPE;             -- consultar relacion con SFRRGFE_MAJR_CODE-STVMAJR
        c_perd_adm                SFRRGFE.SFRRGFE_TERM_CODE_ADMIT%TYPE;     -- STVTERM
        --c_curriculums             SFRRGFE.SFRRGFE_PRIM_SEC_CDE%TYPE;
        c_tipo_camp_studio        SFRRGFE.SFRRGFE_LFST_CODE%TYPE;           -- GTVLFST
        c_cod_camp_studio         SFRRGFE.SFRRGFE_MAJR_CODE%TYPE;                   -- STVMAJR
        c_depto                   SFRRGFE.SFRRGFE_DEPT_CODE%TYPE;           -- STVDEPT
        --c_camp_studio             SFRRGFE.SFRRGFE_LFST_PRIM_SEC_CDE%TYPE; 
        c_tipo_alumn_curriculm    SFRRGFE.SFRRGFE_STYP_CODE_CURRIC%TYPE;    -- STVSTYP
        c_trf_curriculm           SFRRGFE.SFRRGFE_RATE_CODE_CURRIC%TYPE;    -- STVRATE
        c_cargo_minimo            SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        
        /* FORMAS */
        c_form                    varchar2(16);
BEGIN
--
    ----------------------------------------------------------------------------
        c_form := 'SGASTDN';
        /* NOTA: probabilidad de encontrar doble curricula activa para el alumno - saldria error */
        SELECT 
            SORLCUR_LEVL_CODE,
            SORLCUR_CAMP_CODE,
            SORLCUR_COLL_CODE,
            SORLCUR_DEGC_CODE,
            SORLCUR_PROGRAM,
            SORLCUR_TERM_CODE_ADMIT,
            --SORLCUR_STYP_CODE,
            SORLCUR_STYP_CODE,
            SORLCUR_RATE_CODE,
            SORLFOS_DEPT_CODE
        INTO 
            c_alum_nivel, c_alum_campus, c_alum_escuela, c_alum_grado, c_alum_programa, c_alum_perd_adm,/*c_alum_tipo_alum,*/
            c_alum_tipo_alum,c_alum_tarifa,c_alum_departamento
        FROM SORLCUR 
        INNER JOIN SORLFOS
          ON SORLCUR_PIDM =  SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = p_id_alumno AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
        AND SORLCUR_TERM_CODE = p_periodo AND SORLCUR_CACT_CODE = 'ACTIVE';
    ----------------------------------------------------------------------------
        c_form := 'SFARGFE';
        --DBMS_OUTPUT.PUT_LINE(c_alum_nivel ||' '|| c_alum_campus  ||' '||  c_alum_escuela  ||' '||  c_alum_grado  ||' '||  c_alum_programa  ||' '||  c_alum_perd_adm  ||' '||  c_alum_tipo_alum ||' '|| c_alum_tarifa  ||' - '||  c_alum_departamento);
--        SELECT 
--            SFRRGFE_LEVL_CODE,
--            SFRRGFE_CAMP_CODE,
--            SFRRGFE_COLL_CODE,
--            SFRRGFE_DEGC_CODE,
--            SFRRGFE_PROGRAM,
--            SFRRGFE_TERM_CODE_ADMIT,
--            --SFRRGFE_PRIM_SEC_CDE,
--            SFRRGFE_LFST_CODE,
--            SFRRGFE_MAJR_CODE,
--            SFRRGFE_DEPT_CODE,
--            --SFRRGFE_LFST_PRIM_SEC_CDE,
--            SFRRGFE_STYP_CODE_CURRIC,
--            SFRRGFE_RATE_CODE_CURRIC
--        INTO  c_nivel ,c_campus ,c_escuela ,c_grado ,c_programa ,c_perd_adm /*,c_curriculums*/,c_tipo_camp_studio
--              ,c_cod_camp_studio ,c_depto /*,c_camp_studio ,c_tipo_alumn_curriculm ,c_trf_curriculm */
        SELECT SFRRGFE_MIN_CHARGE INTO c_cargo_minimo
         FROM SFRRGFE 
        WHERE SFRRGFE_TERM_CODE = p_periodo AND SFRRGFE_DETL_CODE = p_codigo_detalle AND SFRRGFE_TYPE = 'STUDENT'
        AND NVL(SFRRGFE_LEVL_CODE, c_alum_nivel) = NVL(c_alum_nivel, SFRRGFE_LEVL_CODE) 
        AND NVL(SFRRGFE_CAMP_CODE, c_alum_campus) = NVL(c_alum_campus, SFRRGFE_CAMP_CODE) 
        AND NVL(SFRRGFE_COLL_CODE, c_alum_escuela) = NVL(c_alum_escuela, SFRRGFE_COLL_CODE) 
        AND NVL(SFRRGFE_DEGC_CODE, c_alum_grado) = NVL(c_alum_grado, SFRRGFE_DEGC_CODE) 
        AND NVL(SFRRGFE_PROGRAM, c_alum_programa) = NVL(c_alum_programa, SFRRGFE_PROGRAM) 
        AND NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm) = NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT) 
        AND NVL(SFRRGFE_DEPT_CODE, c_alum_departamento) = NVL(c_alum_departamento, SFRRGFE_DEPT_CODE)
        AND NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum) = NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC)
        AND NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa) = NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC);
    ----------------------------------------------------------------------------
      
      SELECT TBBDETC_DESC INTO c_codigo_detalle_dec  FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = p_codigo_detalle;
      SELECT user INTO c_user FROM dual;
      SELECT TBRACCD_TRAN_NUMBER + 1 INTO c_tran_number FROM (
                        SELECT TBRACCD_TRAN_NUMBER FROM TBRACCD
                        WHERE TBRACCD_PIDM = p_id_alumno ORDER BY TBRACCD_TRAN_NUMBER DESC
                      )WHERE ROWNUM<= 1;
    
    ----------------------------------------------------------------------------
      INSERT INTO TBRACCD (
        TBRACCD_PIDM ,TBRACCD_TRAN_NUMBER ,TBRACCD_TERM_CODE ,TBRACCD_DETAIL_CODE ,TBRACCD_USER ,TBRACCD_ENTRY_DATE,
        TBRACCD_AMOUNT ,TBRACCD_BALANCE ,TBRACCD_EFFECTIVE_DATE ,TBRACCD_DESC ,TBRACCD_SRCE_CODE,TBRACCD_ACCT_FEED_IND,
        TBRACCD_ACTIVITY_DATE ,TBRACCD_SESSION_NUMBER,TBRACCD_TRANS_DATE
      )
      SELECT
            p_id_alumno ,c_tran_number ,p_periodo ,p_codigo_detalle ,c_user ,SYSDATE ,c_cargo_minimo,0 ,SYSDATE ,c_codigo_detalle_dec ,'T' ,'Y' ,SYSDATE,0,SYSDATE
      FROM DUAL
      WHERE EXISTS (
          --SELECT TBBDETC_DESC FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = p_codigo_detalle
           SELECT SFRRGFE_MIN_CHARGE 
             FROM SFRRGFE 
            WHERE SFRRGFE_TERM_CODE = p_periodo AND SFRRGFE_DETL_CODE = p_codigo_detalle AND SFRRGFE_TYPE = 'STUDENT'
            AND NVL(SFRRGFE_LEVL_CODE, c_alum_nivel) = NVL(c_alum_nivel, SFRRGFE_LEVL_CODE) 
            AND NVL(SFRRGFE_CAMP_CODE, c_alum_campus) = NVL(c_alum_campus, SFRRGFE_CAMP_CODE) 
            AND NVL(SFRRGFE_COLL_CODE, c_alum_escuela) = NVL(c_alum_escuela, SFRRGFE_COLL_CODE) 
            AND NVL(SFRRGFE_DEGC_CODE, c_alum_grado) = NVL(c_alum_grado, SFRRGFE_DEGC_CODE) 
            AND NVL(SFRRGFE_PROGRAM, c_alum_programa) = NVL(c_alum_programa, SFRRGFE_PROGRAM) 
            AND NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm) = NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT) 
            AND NVL(SFRRGFE_DEPT_CODE, c_alum_departamento) = NVL(c_alum_departamento, SFRRGFE_DEPT_CODE)
            AND NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum) = NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC)
            AND NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa) = NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC)
      );
      
      p_error := 'Correcto';
    ----------------------------------------------------------------------------

--
    COMMIT;
EXCEPTION
  WHEN TOO_MANY_ROWS
           THEN DBMS_OUTPUT.PUT_LINE('Se detecto mas de un REGISTRO (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado. : ' || c_form );
  WHEN NO_DATA_FOUND
           THEN DBMS_OUTPUT.PUT_LINE ('NO se detectaron REGISTROS (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado. : '  || c_form );
  WHEN OTHERS THEN
          p_error := SUBSTR(SQLERRM, 1, 64);
          raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END p_set_coddetalle_alumno;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION f_get_sideuda_alumno
              (
                P_PIDM IN TBRACCD.TBRACCD_PIDM%TYPE, -- PIDM ALUMNO
                P_TERM_CODE IN STVTERM.STVTERM_CODE%TYPE,
                P_COD_DETALLE IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE
              ) RETURN VARCHAR2 
              
/* ===================================================================================================================
  NOMBRE    : f_get_sideuda_alumno
  FECHA     : 07//09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Verifica la existencia de deuda en la cuenta corriente del alumno para un codigo detalle y  
              periodo correspondiente, divisa PEN.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
              
AS
PRAGMA AUTONOMOUS_TRANSACTION;

      C_ROWS_FOUND    NUMBER;
      C_SI_DEUDA      VARCHAR2(5) :='false';
BEGIN

    TZKCDAA.p_calc_deuda_alumno(P_PIDM,'PEN');
    COMMIT;
    
    SELECT COUNT(*)
    INTO   C_ROWS_FOUND
    FROM   TZRCDAB
    WHERE  TZRCDAB_TERM_CODE = P_TERM_CODE AND TZRCDAB_PIDM = P_PIDM 
    AND TZRCDAB_DETAIL_CODE = P_COD_DETALLE AND ROWNUM <= 1;
  
    IF C_ROWS_FOUND > 0 THEN
      C_SI_DEUDA := 'true';
    END IF;
      
    RETURN C_SI_DEUDA;

END f_get_sideuda_alumno;



--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE p_validar_fecha_solicitud (
  p_id_alumno         IN SPRIDEN.SPRIDEN_PIDM%TYPE, 
  p_codigo_solicitud  IN SVVSRVC.SVVSRVC_CODE%TYPE,
  p_fecha_valida      OUT varchar2
  )
/* ===================================================================================================================
  NOMBRE    : p_validar_fecha_solicitud
  FECHA     : 19/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : se verifiqua que solicitud del tipo SOL003, se encuentra dentro del rango de fechas h�biles para atender la solicitud.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */

AS
       
BEGIN
--
    
    p_fecha_valida := 'FALSE';
    
    ----------------------------------------------------------------------------
        SELECT 
                DECODE(COUNT(*),0,'FALSE','TRUE') 
        INTO p_fecha_valida 
        FROM SVRSVPR 
        INNER JOIN (
               SELECT SVRRSRV_SEQ_NO, 
                      SVRRSRV_SRVC_CODE       TEMP_SRVC_CODE, 
                      SVRRSRV_INACTIVE_IND,
                      SVRRSST_RSRV_SEQ_NO , 
                      SVRRSST_SRVC_CODE , 
                      SVRRSST_SRVS_CODE       TEMP_SRVS_CODE,
                      SVRRSRV_END_DATE        TEMP_END_DATE , -- fecha finalizacion
                      SVRRSRV_DELIVERY_DATE , -- fecha estimada
                      SVVSRVS_LOCKED
               FROM SVRRSRV
               INNER JOIN SVRRSST
               ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
               AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
               INNER JOIN SVVSRVS -- total de estados de servicios
               ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
               WHERE SVRRSRV_SRVC_CODE = p_codigo_solicitud 
               AND SVRRSRV_INACTIVE_IND = 'Y' -- Activo
               AND SVVSRVS_LOCKED <> 'L' -- (L)LOCKET , (U)UNLOCKET 
               AND SVVSRVS_CODE IN 'AC' -- (AC)Acitvo , (RE) Rechazado
        )TEMP
        ON SVRSVPR_SRVS_CODE = TEMP_SRVS_CODE
        AND SVRSVPR_SRVC_CODE =  TEMP_SRVC_CODE
        WHERE  TEMP_END_DATE < SYSDATE 
        AND SVRSVPR_PIDM = p_id_alumno;
      
    ----------------------------------------------------------------------------

--
    COMMIT;
EXCEPTION
  WHEN TOO_MANY_ROWS
           THEN DBMS_OUTPUT.PUT_LINE('Se detecto mas de un REGISTRO (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado.');
  WHEN NO_DATA_FOUND
           THEN DBMS_OUTPUT.PUT_LINE ('NO se detectaron REGISTROS (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado.');
  WHEN OTHERS THEN
          raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END p_validar_fecha_solicitud;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE p_del_coddetalle_alumno (
    p_id_alumno        IN TBRACCD.TBRACCD_PIDM%TYPE,
    p_periodo          IN TBRACCD.TBRACCD_TERM_CODE%TYPE,
    p_codigo_detalle   IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
    p_error            OUT varchar2
)
/* ===================================================================================================================
  NOMBRE    : p_del_coddetalle_alumno
  FECHA     : 14/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Elmina un c�digo de detalle a la cuenta corriente del alumno para un periodo correspondiente.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
    p_tran_number      TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
BEGIN
--

    SELECT 
        --TBRACCD.* 
        --TBRACCD.TBRACCD_PIDM ,TBRACCD.TBRACCD_TERM_CODE, TBRACCD.TBRACCD_TRAN_NUMBER, TBRACCD_DETAIL_CODE, TEMP_PAY_TRAN_NUMBER
        TBRACCD.TBRACCD_TRAN_NUMBER
        INTO p_tran_number
    FROM TBRACCD
    LEFT JOIN (
            -- : listar LOS CODIGOS QUE YA TIENEN ALGUN MOVIMIENTO. : TRAN , CHG 
            SELECT TBRAPPL_PIDM TEMP_PIDM ,TBRAPPL_PAY_TRAN_NUMBER TEMP_PAY_TRAN_NUMBER
            FROM TBRAPPL
            WHERE TBRAPPL.TBRAPPL_PIDM = p_id_alumno
            UNION 
            SELECT TBRAPPL_PIDM, TBRAPPL_CHG_TRAN_NUMBER
            FROM TBRAPPL
            WHERE TBRAPPL.TBRAPPL_PIDM = p_id_alumno
    ) TEMP
    ON TBRACCD_PIDM = TEMP_PIDM
    AND TBRACCD_TRAN_NUMBER =  TEMP_PAY_TRAN_NUMBER
    WHERE TEMP_PAY_TRAN_NUMBER IS NULL
    AND TBRACCD.TBRACCD_TERM_CODE = p_periodo
    AND TBRACCD.TBRACCD_PIDM = p_id_alumno
    AND TBRACCD_DETAIL_CODE =  p_codigo_detalle
    AND EXISTS (
        SELECT TBBDETC_DETAIL_CODE FROM TBBDETC WHERE TBBDETC_TYPE_IND = 'C'  AND TBBDETC_DETAIL_CODE = p_codigo_detalle
    );
  ------------------------------------------------------------------------------
    DELETE FROM TBRACCD
    WHERE TBRACCD.TBRACCD_TERM_CODE = p_periodo
    AND TBRACCD.TBRACCD_PIDM = p_id_alumno
    AND TBRACCD_DETAIL_CODE =  p_codigo_detalle
    AND EXISTS (
        SELECT TBBDETC_DETAIL_CODE FROM TBBDETC WHERE TBBDETC_TYPE_IND = 'C'  AND TBBDETC_DETAIL_CODE = p_codigo_detalle
    );
  ------------------------------------------------------------------------------  
    
    p_error := 'Correcto';

--
    COMMIT;
EXCEPTION
  WHEN TOO_MANY_ROWS
           THEN DBMS_OUTPUT.PUT_LINE('Se mostr� MAS DE UN RESULTADO PARA ELIMINAR, se cancelo lo solicitado.' );
  WHEN NO_DATA_FOUND
           THEN DBMS_OUTPUT.PUT_LINE ('NO se detecto REGISTROS que coincidan con lo solicitado para el alumno.');
  WHEN OTHERS THEN
          p_error := SUBSTR(SQLERRM, 1, 64);
          raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
--
END;       

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE p_cambio_estado_solicitud (
  -- p_id_alumno           IN SPRIDEN.SPRIDEN_PIDM%TYPE, 
  p_motivo              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  p_numero_solicitud    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  p_error               OUT varchar2
  )
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 19/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : finaliza la solicitud (SOL003) presentada ya sea por su atenci�n o vencimiento del tiempo de solicitud

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS     
BEGIN
--
    p_error := 'Satisfactorio';
    ----------------------------------------------------------------------------
        UPDATE SVRSVPR
            SET SVRSVPR_SRVS_CODE = p_motivo -- AN , AP , RE
        WHERE EXISTS (
            SELECT 
                SVRSVPR.*,TEMP_END_DATE
            FROM SVRSVPR  
            INNER JOIN (
                   SELECT SVRRSRV_SEQ_NO, 
                          SVRRSRV_SRVC_CODE       TEMP_SRVC_CODE, 
                          SVRRSRV_INACTIVE_IND,
                          SVRRSST_RSRV_SEQ_NO , 
                          SVRRSST_SRVC_CODE , 
                          SVRRSST_SRVS_CODE       TEMP_SRVS_CODE,
                          SVRRSRV_END_DATE        TEMP_END_DATE , -- fecha finalizacion
                          SVRRSRV_DELIVERY_DATE , -- fecha estimada
                          SVVSRVS_LOCKED
                   FROM SVRRSRV -- configuraci�n total de reglas
                   INNER JOIN SVRRSST -- subformulario de reglas -- estados
                   ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                   AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                   INNER JOIN SVVSRVS -- total de estados de servicios
                   ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                   WHERE SVRRSRV_SRVC_CODE = 'SOL003' 
                   AND SVRRSRV_INACTIVE_IND = 'Y' -- Activo
                   AND SVVSRVS_LOCKED <> 'L' -- (L)LOCKET , (U)UNLOCKET 
                   AND SVVSRVS_CODE IN 'AC' -- (AC)Acitvo , (RE) Rechazado
            )TEMP
            ON SVRSVPR_SRVS_CODE = TEMP_SRVS_CODE
            AND SVRSVPR_SRVC_CODE =  TEMP_SRVC_CODE
            WHERE  TEMP_END_DATE < SYSDATE 
            AND SVRSVPR_PROTOCOL_SEQ_NO = p_numero_solicitud
            -- AND SVRSVPR_PIDM = p_id_alumno
        )
        -- AND SVRSVPR_PIDM = p_id_alumno
        AND SVRSVPR_PROTOCOL_SEQ_NO = p_numero_solicitud;
    ----------------------------------------------------------------------------
--
    COMMIT;
EXCEPTION
  WHEN TOO_MANY_ROWS
           THEN DBMS_OUTPUT.PUT_LINE('Se detecto mas de un REGISTRO (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado.');
  WHEN NO_DATA_FOUND
           THEN DBMS_OUTPUT.PUT_LINE ('NO se detectaron REGISTROS (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado.');
  WHEN OTHERS THEN
          raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END p_cambio_estado_solicitud;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE p_set_gr_inscripc_abierto (
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
      P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
      P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_set_grupo_inscripcion_abierto
  FECHA     : 27/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asignar el c�digo de grupo de inscripci�n para la rectificaci�n en base al id, la modalidad y a la sede del estudiante.

                          Ejemplo: 99-99WF01
------------------------------------------------------------------------------
    Rectificaci�n               Modalidad                 Sede            
99-99 � Rectificaci�n     R � Regular                   S01 � Huancayo
                          W � Gente que trabaja         F01 � Arequipa 
                          V � Virtual                   F02 � Lima  
                                                        F03 � Cusco
                                                        V00 � Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      P_RPGRP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_MODALIDAD_SEDE      VARCHAR2(4);
      P_MODALIDAD_SEDE2     VARCHAR2(4);
      
      P_MESSAGE             EXCEPTION;
      P_MESSAGE2            EXCEPTION;
      P_MESSAGE3            EXCEPTION;
      P_INDICADOR           NUMBER := 0;
      P_ROW_UPDATE          NUMBER := 0;
      P_COD_RECTF           VARCHAR2(15) := '99-99';
BEGIN
--
      
      P_INDICADOR := 1;      
      SELECT SFBRGRP_RGRP_CODE INTO P_RPGRP
      FROM SFBRGRP 
      WHERE SFBRGRP_PIDM = P_ID_ALUMNO
      AND SFBRGRP_TERM_CODE = P_PERIODO;
      
      -- OBTENER CADENA DE MODALIDAD Y SEDE  del CODIGO DE GRUPO  
      P_MODALIDAD_SEDE := SUBSTR(P_RPGRP,6,4);
      
      -- VALIDACION : CODIGO grupo insc. y datos proporcionados
      SELECT CASE P_MODALIDAD_ALUMNO 
                WHEN 'UVIR' THEN 'V' --#UC-SEMI PRESENCIAL (EV)
                WHEN 'UPGT' THEN 'W' --#UC-SEMI PRESENCIAL (GT)
                WHEN 'UREG' THEN 'R' --#UC-PRESENCIAL
                ELSE '' END INTO P_MODALIDAD_SEDE2 FROM DUAL;
            
      IF (P_MODALIDAD_SEDE != (P_MODALIDAD_SEDE2 || P_COD_SEDE )) THEN
              RAISE P_MESSAGE2;
      ELSIF (P_RPGRP = (P_COD_RECTF || P_MODALIDAD_SEDE)) THEN
              RAISE P_MESSAGE3;
      END IF;
    
      
      -- CODIGO DEL GRUPO DE RECTIFICACI�N : 99-99<cod_modadlidad><cod_sede>
      SELECT CONCAT('99-99',P_MODALIDAD_SEDE) INTO P_RPGRP_NEW FROM DUAL;
      
      -- Validar que exista el codigo del grupo de inscripcion
      P_INDICADOR := 2;    
      SELECT SFBWCTL_RGRP_CODE 
      INTO P_RPGRP_NEW
      FROM SFBWCTL
      WHERE SFBWCTL_RGRP_CODE  = P_RPGRP_NEW
      AND  SFBWCTL_TERM_CODE = P_PERIODO;
      
      -- ASIGNAR GRUPO DE RECTIFICACI�N
      UPDATE SFBRGRP 
        -- SELECT SFBRGRP_TERM_CODE, SFBRGRP_PIDM, SFBRGRP.* 
        SET SFBRGRP_RGRP_CODE = P_RPGRP_NEW
      WHERE SFBRGRP_PIDM = P_ID_ALUMNO
      AND SFBRGRP_TERM_CODE = P_PERIODO
      AND EXISTS (
            ------ LA DEFINICION DE LA VISTA SFVRGRP
            SELECT SFBWCTL.SFBWCTL_TERM_CODE,
              SFBWCTL.SFBWCTL_RGRP_CODE,
              SFBWCTL.SFBWCTL_PRIORITY,
              SFRWCTL.SFRWCTL_BEGIN_DATE,
              SFRWCTL.SFRWCTL_END_DATE,
                    SFRWCTL.SFRWCTL_HOUR_BEGIN,
                    SFRWCTL.SFRWCTL_HOUR_END
              FROM SFRWCTL, SFBWCTL
                    WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                    AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                    AND   SFBWCTL.SFBWCTL_RGRP_CODE = P_RPGRP_NEW
                    AND   SFRWCTL.SFRWCTL_TERM_CODE = P_PERIODO
      );
      P_ROW_UPDATE := P_ROW_UPDATE + SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(P_ROW_UPDATE);
      IF (P_ROW_UPDATE > 1) THEN
            ROLLBACK;
            RAISE P_MESSAGE;
      END IF;
      
     P_ERROR := 'Satisfactorio';

      COMMIT;
EXCEPTION
  WHEN P_MESSAGE THEN
          DBMS_OUTPUT.PUT_LINE('Se detecto que posee DOBLE grupo de inscripcion:' || P_RPGRP_NEW || '. Acerarce a oficinas para solucionar su caso en particular, Gracias.');          
  WHEN P_MESSAGE2 THEN
          DBMS_OUTPUT.PUT_LINE('El departamento y la sede no coinciden con el grupo de rectificacion');          
  WHEN P_MESSAGE3 THEN
          DBMS_OUTPUT.PUT_LINE('Advertencia, Ya se encuentra en el grupo de inscripcion para su rectificaci�n. No procedio lo solicitado.');          
  WHEN TOO_MANY_ROWS THEN 
          IF (P_INDICADOR = 2) THEN
              DBMS_OUTPUT.PUT_LINE('Se encontraron mas de un grupo INICIAL de inscripcion:' || P_RPGRP_NEW || '. Acerarce a oficinas para solucionar su caso en particular, Gracias.');
          ELSIF (P_INDICADOR = 1) THEN
              DBMS_OUTPUT.PUT_LINE('Se encontraron mas de un grupo LIBRE de inscripcion:' || P_RPGRP_NEW || '. Acerarce a oficinas para solucionar su caso en particular, Gracias.');
          ELSE  DBMS_OUTPUT.PUT_LINE(SQLERRM);
          END IF;       
  WHEN NO_DATA_FOUND THEN
           IF (P_INDICADOR = 2) THEN
              DBMS_OUTPUT.PUT_LINE('No se detecto el grupo INICIAL de inscripcion:' || P_RPGRP_NEW || '. Acerarce a oficinas para solucionar su caso en particular, Gracias.');
          ELSIF (P_INDICADOR = 1) THEN
              DBMS_OUTPUT.PUT_LINE('No se detecto el grupo LIBRE de inscripcion:' || P_RPGRP_NEW || '. Acerarce a oficinas para solucionar su caso en particular, Gracias.');
          ELSE  DBMS_OUTPUT.PUT_LINE(SQLERRM);
          END IF;  
  WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM);
END p_set_gr_inscripc_abierto;



--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END CONTIPKG_SRI;