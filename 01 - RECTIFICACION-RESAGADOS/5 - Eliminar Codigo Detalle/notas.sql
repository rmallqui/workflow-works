FORMAS:
===============================================================================

 TVAAREV - CARGOS ALUMNOS
 SE BASO EN el script de setear codigo de p_del_coddetalle_alumno    

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
SELECT * FROM TBRCPDT;
SELECT * FROM TBBDETC;

SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = '179316';
SELECT * FROM TBRAPPL WHERE TBRAPPL_PIDM = '179316';



SELECT * FROM SPRIDEN WHERE SPRIDEN_PIDM = 284082;
SELECT * FROM TZRCDAB WHERE TZRCDAB_PIDM = 284082;
SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 284082 AND TBRACCD_TRAN_NUMBER = 14 ; 
DELETE FROM TBRACCD WHERE TBRACCD_PIDM = 284082 AND TBRACCD_TRAN_NUMBER = 14; COMMIT;
EXEC  TZKCDAA.p_calc_deuda_alumno(284082,'PEN');

            -- : listar LOS CODIGOS QUE YA TIENEN ALGUN MOVIMIENTO. : TRAN , CHG 
            SELECT TBRAPPL_PIDM TEMP_PIDM ,TBRAPPL_PAY_TRAN_NUMBER TEMP_PAY_TRAN_NUMBER
            FROM TBRAPPL
            WHERE TBRAPPL.TBRAPPL_PIDM = 284082
            UNION 
            SELECT TBRAPPL_PIDM, TBRAPPL_CHG_TRAN_NUMBER
            FROM TBRAPPL
            WHERE TBRAPPL.TBRAPPL_PIDM = 284082

/*
drop procedure P_DEL_CODDETALLE_ALUMNO;
GRANT EXECUTE ON p_del_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_del_coddetalle_alumno TO wfauto;

set serveroutput on
DECLARE p_error varchar2(64);
begin
  p_del_coddetalle_alumno('179316','201710','CAR',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;

*/
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

SET SERVEROUTPUT ON
-------------------- MODELO PROCEDURE -----------------------
DECLARE P_ID_ALUMNO             SPRIDEN.SPRIDEN_PIDM%TYPE             := 284082 ;
        P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE      := 7 ;
        P_ERROR                 VARCHAR2(200);
        
        P_DNI_ALUMNO            SPRIDEN.SPRIDEN_ID%TYPE;
        P_INDICADOR             NUMBER;
        P_MESSAGE               EXCEPTION;
        P_FOUND_AMOUNT          TBRACCD.TBRACCD_AMOUNT%TYPE;
        P_COD_DETALLE           TBRACCD.TBRACCD_DETAIL_CODE%TYPE;
        P_NOM_SERVICIO          VARCHAR2(30);
        P_PERIODO               SFBETRM.SFBETRM_TERM_CODE%TYPE;
        P_TRAN_NUMBER_OUT       TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
        P_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
        
BEGIN
--
    
    SELECT SPRIDEN_ID INTO P_DNI_ALUMNO FROM SPRIDEN WHERE SPRIDEN_PIDM = P_ID_ALUMNO;
    
    -- Validar si la transaccion (DEUDA) NO tubo algun movimiento.
    SELECT COUNT(*) INTO P_INDICADOR
    FROM (
            -- : listar LOS CODIGOS QUE YA TIENEN ALGUN MOVIMIENTO. : TRAN , CHG 
            SELECT TBRAPPL_PIDM TEMP_PIDM ,TBRAPPL_PAY_TRAN_NUMBER TEMP_PAY_TRAN_NUMBER
            FROM TBRAPPL
            WHERE TBRAPPL.TBRAPPL_PIDM = P_ID_ALUMNO
            UNION 
            SELECT TBRAPPL_PIDM, TBRAPPL_CHG_TRAN_NUMBER
            FROM TBRAPPL
            WHERE TBRAPPL.TBRAPPL_PIDM = P_ID_ALUMNO
    ) WHERE P_TRAN_NUMBER = TEMP_PAY_TRAN_NUMBER;
    
    DBMS_OUTPUT.PUT_LINE(P_INDICADOR);
    
    IF P_INDICADOR = 1 THEN
          RAISE P_MESSAGE;
    END IF;
        
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(P_ID_ALUMNO,'PEN');
    COMMIT;
        
    -- GET , COD_DETALLE , AMOUNT(NEGATIVO) de la transaccion a cancelar  
    SELECT  (TZRCDAB_AMOUNT * (-1)), 
            TZRCDAB_DETAIL_CODE, 
            TZRCDAB_TERM_CODE
    INTO    P_FOUND_AMOUNT,
            P_COD_DETALLE, 
            P_PERIODO
    FROM   TZRCDAB
    WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
    AND TZRCDAB_PIDM = P_ID_ALUMNO 
    AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
    
    -- GET DESCRIPCION del cod_detalle
    SELECT TBBDETC_DESC INTO P_NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;
    
    DBMS_OUTPUT.PUT_LINE(P_FOUND_AMOUNT || '--' || P_COD_DETALLE || '--' || P_NOM_SERVICIO || '-- NUM TRAN ' || P_TRAN_NUMBER );
    
    -- #######################################################################
      -- GENERAR DEUDA
      TB_RECEIVABLE.p_create ( p_pidm                 =>  P_ID_ALUMNO,        -- PIDEM ALUMNO
                               p_term_code            =>  P_PERIODO,          -- DETALLE
                               p_detail_code          =>  P_COD_DETALLE,      -- CODIGO DETALLE  -- mismo codigo detalle
                               p_user                 =>  USER,               -- USUARIO
                               p_entry_date           =>  SYSDATE,     
                               p_amount               =>  P_FOUND_AMOUNT,
                               p_effective_date       =>  SYSDATE,
                               p_bill_date            =>  NULL,    
                               p_due_date             =>  NULL,    
                               p_desc                 =>  P_NOM_SERVICIO,       -- referencia por consultar 
                               p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                               p_tran_number_paid     =>  P_TRAN_NUMBER,      -- numero de transaccion que será pagara
                               p_crossref_pidm        =>  NULL,    
                               p_crossref_number      =>  NULL,    
                               p_crossref_detail_code =>  NULL,     
                               p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                               p_acct_feed_ind        =>  'Y',
                               p_session_number       =>  0,    
                               p_cshr_end_date        =>  NULL,    
                               p_crn                  =>  NULL,
                               p_crossref_srce_code   =>  NULL,
                               p_loc_mdt              =>  NULL,
                               p_loc_mdt_seq          =>  NULL,    
                               p_rate                 =>  NULL,    
                               p_units                =>  NULL,     
                               p_document_number      =>  NULL,    
                               p_trans_date           =>  NULL,    
                               p_payment_id           =>  NULL,    
                               p_invoice_number       =>  NULL,    
                               p_statement_date       =>  NULL,    
                               p_inv_number_paid      =>  NULL,    
                               p_curr_code            =>  NULL,    
                               p_exchange_diff        =>  NULL,    
                               p_foreign_amount       =>  NULL,    
                               p_late_dcat_code       =>  NULL,    
                               p_atyp_code            =>  NULL,    
                               p_atyp_seqno           =>  NULL,    
                               p_card_type_vr         =>  NULL,    
                               p_card_exp_date_vr     =>  NULL,     
                               p_card_auth_number_vr  =>  NULL,    
                               p_crossref_dcat_code   =>  NULL,    
                               p_orig_chg_ind         =>  NULL,    
                               p_ccrd_code            =>  NULL,    
                               p_merchant_id          =>  NULL,    
                               p_data_origin          =>  'WORKFLOW',    
                               p_override_hold        =>  'N',     
                               p_tran_number_out      =>  P_TRAN_NUMBER_OUT, 
                               p_rowid_out            =>  P_ROWID);


      DBMS_OUTPUT.PUT_LINE( P_TRAN_NUMBER_OUT ||'--'|| P_ROWID);
  
  ------------------------------------------------------------------------------  
    
      TZJAPOL.p_run_proc_tvrappl(P_DNI_ALUMNO);
  

    COMMIT;
EXCEPTION
  WHEN P_MESSAGE THEN
        P_ERROR := 'A ocurrido un inconveniente  - La deuda tubo movimientos por lo que no se puede cancelar directamente.';
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||P_ERROR);
  WHEN OTHERS THEN
        ROLLBACK;
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END;

       
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------