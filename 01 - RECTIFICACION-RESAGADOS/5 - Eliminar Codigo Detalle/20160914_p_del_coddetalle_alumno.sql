

/*
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_del_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_del_coddetalle_alumno TO wfauto;

set serveroutput on
DECLARE p_error varchar2(64);
begin
  p_del_coddetalle_alumno('179316','201710','CAR',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;

*/
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE p_del_coddetalle_alumno (
    p_id_alumno        IN TBRACCD.TBRACCD_PIDM%TYPE,
    p_periodo          IN TBRACCD.TBRACCD_TERM_CODE%TYPE,
    p_codigo_detalle   IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
    p_error            OUT varchar2
)
/* ===================================================================================================================
  NOMBRE    : p_del_coddetalle_alumno
  FECHA     : 14/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Elmina un código de detalle a la cuenta corriente del alumno para un periodo correspondiente.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
    p_tran_number      TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
BEGIN
--

    SELECT 
        --TBRACCD.* 
        --TBRACCD.TBRACCD_PIDM ,TBRACCD.TBRACCD_TERM_CODE, TBRACCD.TBRACCD_TRAN_NUMBER, TBRACCD_DETAIL_CODE, TEMP_PAY_TRAN_NUMBER
        TBRACCD.TBRACCD_TRAN_NUMBER
        INTO p_tran_number
    FROM TBRACCD
    LEFT JOIN (
            -- : listar LOS CODIGOS QUE YA TIENEN ALGUN MOVIMIENTO. : TRAN , CHG 
            SELECT TBRAPPL_PIDM TEMP_PIDM ,TBRAPPL_PAY_TRAN_NUMBER TEMP_PAY_TRAN_NUMBER
            FROM TBRAPPL
            WHERE TBRAPPL.TBRAPPL_PIDM = p_id_alumno
            UNION 
            SELECT TBRAPPL_PIDM, TBRAPPL_CHG_TRAN_NUMBER
            FROM TBRAPPL
            WHERE TBRAPPL.TBRAPPL_PIDM = p_id_alumno
    ) TEMP
    ON TBRACCD_PIDM = TEMP_PIDM
    AND TBRACCD_TRAN_NUMBER =  TEMP_PAY_TRAN_NUMBER
    WHERE TEMP_PAY_TRAN_NUMBER IS NULL
    AND TBRACCD.TBRACCD_TERM_CODE = p_periodo
    AND TBRACCD.TBRACCD_PIDM = p_id_alumno
    AND TBRACCD_DETAIL_CODE =  p_codigo_detalle
    AND EXISTS (
        SELECT TBBDETC_DETAIL_CODE FROM TBBDETC WHERE TBBDETC_TYPE_IND = 'C'  AND TBBDETC_DETAIL_CODE = p_codigo_detalle
    );
  ------------------------------------------------------------------------------
    DELETE FROM TBRACCD
    WHERE TBRACCD.TBRACCD_TERM_CODE = p_periodo
    AND TBRACCD.TBRACCD_PIDM = p_id_alumno
    AND TBRACCD_DETAIL_CODE =  p_codigo_detalle
    AND EXISTS (
        SELECT TBBDETC_DETAIL_CODE FROM TBBDETC WHERE TBBDETC_TYPE_IND = 'C'  AND TBBDETC_DETAIL_CODE = p_codigo_detalle
    );
  ------------------------------------------------------------------------------  
    
    p_error := 'Correcto';

--
    COMMIT;
EXCEPTION
  WHEN TOO_MANY_ROWS
           THEN DBMS_OUTPUT.PUT_LINE('Se mostrò MAS DE UN RESULTADO PARA ELIMINAR, se cancelo lo solicitado.' );
  WHEN NO_DATA_FOUND
           THEN DBMS_OUTPUT.PUT_LINE ('NO se detecto REGISTROS que coincidan con lo solicitado para el alumno.');
  WHEN OTHERS THEN
          p_error := SUBSTR(SQLERRM, 1, 64);
          raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
--
END;

       
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------