/*
SET SERVEROUTPUT ON
DECLARE P_ARCHIVADOR_APEC VARCHAR2(4000);
    P_REQUISITOS_APEC VARCHAR2(4000);
    P_ARCHIVADOR_BANNER VARCHAR2(4000);
    P_REQUISITOS_BANNER VARCHAR2(4000);
    P_MESSAGE VARCHAR2(4000);
BEGIN
    P_VERIFICAR_REQUISITOS_PREVIOS (389640, '73054048','201910', P_ARCHIVADOR_APEC,P_REQUISITOS_APEC, P_ARCHIVADOR_BANNER,P_REQUISITOS_BANNER,P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_ARCHIVADOR_APEC);
    DBMS_OUTPUT.PUT_LINE(P_REQUISITOS_APEC);
    DBMS_OUTPUT.PUT_LINE(P_ARCHIVADOR_BANNER);
    DBMS_OUTPUT.PUT_LINE(P_REQUISITOS_BANNER);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/
/* ===================================================================================================================
  NOMBRE    : P_VERIFICAR_REQUISITOS_PREVIOS
  FECHA     : 08/11/2018
  AUTOR     : Flores Vilcapoma Brian
  OBJETIVO  : Verifica si el estudiante tiene su cuenta corriente previa con pagos
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_VERIFICAR_REQUISITOS_PREVIOS (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_ARCHIVADOR_APEC   OUT VARCHAR2,
    P_REQUISITOS_APEC   OUT VARCHAR2,
    P_ARCHIVADOR_BANNER OUT VARCHAR2,
    P_REQUISITOS_BANNER OUT VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
)
AS
    V_APEC_TERM         VARCHAR2(10);
    V_FLAG_ARCH_APEC    BOOLEAN := FALSE;
    V_FLAG_ARCH_BANNER  BOOLEAN := FALSE;
    V_FLAG_REQ_APEC     BOOLEAN := FALSE;
    V_FLAG_REQ_BANNER   BOOLEAN := FALSE;
    
    V_ARCH_APEC         VARCHAR2(1000);
    V_ARCH_BANNER       VARCHAR2(1000);
    V_REQ_APEC          VARCHAR2(1000);
    V_REQ_BANNER        VARCHAR2(1000);

    CURSOR C_ARCHIVADOR_APEC IS
        SELECT "Archivador" ArchivadorAPEC
        FROM tblPostulanteRequisitos@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDAlumno" = P_ID
            AND ("Archivador" IS NOT NULL OR "Archivador" <> '');

    CURSOR C_ARCHIVADOR_BANNER IS
        SELECT SARCHKL_CODE_VALUE ArchivadorBANNER
        FROM SARCHKL
        WHERE SARCHKL_TERM_CODE_ENTRY = P_TERM_CODE
            AND (SARCHKL_CODE_VALUE IS NOT NULL OR SARCHKL_CODE_VALUE <> '')
            AND SARCHKL_PIDM = P_PIDM;

    CURSOR C_REQUISITOS_APEC IS
        SELECT CASE 
            WHEN pr."Observacion" IS NOT NULL THEN '- ' || re."Nombre" || ' - Obs: ' || pr."Observacion"
            WHEN pr."Observacion" <> '' THEN '- ' || re."Nombre" || ' - Obs: ' || pr."Observacion"
            ELSE '- ' || re."Nombre"
            END RequisitoAPEC
        FROM tblPostulanteRequisitos@BDUCCI.CONTINENTAL.EDU.PE pr
        INNER JOIN tblRequisitos@BDUCCI.CONTINENTAL.EDU.PE re ON
            pr."IDRequisito" = re."IDRequisito"
        WHERE pr."IDDependencia" = 'UCCI'
            AND pr."IDPerAcad" = V_APEC_TERM
            AND pr."CantEntregada" > 0
        ORDER BY re."Nombre";

    CURSOR C_REQUISITOS_BANNER IS
        SELECT CASE 
            WHEN SARCHKL_COMMENT IS NOT NULL THEN '- ' || STVADMR_DESC || ' - Obs: ' || SARCHKL_COMMENT 
            WHEN SARCHKL_COMMENT <> '' THEN '- ' || STVADMR_DESC || ' - Obs: ' || SARCHKL_COMMENT 
            ELSE '- ' || STVADMR_DESC
            END RequisitoBANNER
        FROM SARCHKL
        INNER JOIN STVADMR ON
            SARCHKL_ADMR_CODE = STVADMR_CODE
        WHERE SARCHKL_TERM_CODE_ENTRY = P_TERM_CODE
        AND SARCHKL_PIDM = P_PIDM
        AND SARCHKL_CKST_CODE = 'ENTREGADO';
BEGIN

    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM 
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- CURSOR PARA OBTENER EL ARCHIVADOR DE APEC
    OPEN C_ARCHIVADOR_APEC;
        FETCH C_ARCHIVADOR_APEC INTO V_ARCH_APEC;
        IF C_ARCHIVADOR_APEC%NOTFOUND THEN
            P_ARCHIVADOR_APEC := '- Sin registro';
        ELSE 
            IF V_FLAG_ARCH_APEC = TRUE THEN
                P_ARCHIVADOR_APEC := P_ARCHIVADOR_APEC || ' - ' || V_ARCH_APEC;
            ELSE
                P_ARCHIVADOR_APEC := '- ' || V_ARCH_APEC;            
            END IF;
            V_FLAG_ARCH_APEC := TRUE;
        END IF;
    CLOSE C_ARCHIVADOR_APEC;

    -- CURSOR PARA OBTENER EL ARCHIVADOR DE BANNER
    OPEN C_ARCHIVADOR_BANNER;
        FETCH C_ARCHIVADOR_BANNER INTO V_ARCH_BANNER;
        IF C_ARCHIVADOR_BANNER%NOTFOUND THEN
            P_ARCHIVADOR_BANNER := '- Sin registro';
        ELSE 
            IF V_FLAG_ARCH_BANNER = TRUE THEN
                P_ARCHIVADOR_BANNER := P_ARCHIVADOR_BANNER || ' - ' || V_ARCH_BANNER;
            ELSE
                P_ARCHIVADOR_BANNER := '- ' || V_ARCH_BANNER;            
            END IF;
            V_FLAG_ARCH_BANNER := TRUE;
        END IF;
    CLOSE C_ARCHIVADOR_BANNER;

    -- CURSOR PARA OBTENER LOS REQUISITOS DE APEC
    OPEN C_REQUISITOS_APEC;
        FETCH C_REQUISITOS_APEC INTO V_REQ_APEC;
        IF C_REQUISITOS_APEC%NOTFOUND THEN
            P_REQUISITOS_APEC := 'Sin registro de requisitos previos';
        ELSE 
            IF V_FLAG_REQ_APEC = TRUE THEN
                P_REQUISITOS_APEC := P_REQUISITOS_APEC || ' - ' || V_REQ_APEC;
            ELSE
                P_REQUISITOS_APEC := '<br />' || '<br />' || V_REQ_APEC;            
            END IF;
            V_FLAG_REQ_APEC := TRUE;
        END IF;
    CLOSE C_REQUISITOS_APEC;

    -- CURSOR PARA OBTENER LOS REQUISITOS DE BANNER
    OPEN C_REQUISITOS_BANNER;
        FETCH C_REQUISITOS_BANNER INTO V_REQ_BANNER;
        IF C_REQUISITOS_BANNER%NOTFOUND THEN
            P_REQUISITOS_BANNER := 'Sin registro de requisitos previos';
        ELSE 
            IF V_FLAG_REQ_BANNER = TRUE THEN
                P_REQUISITOS_BANNER := P_REQUISITOS_BANNER || ' - ' || V_REQ_BANNER;
            ELSE
                P_REQUISITOS_BANNER := '<br />' || '<br />' || V_REQ_BANNER;            
            END IF;
            V_FLAG_REQ_BANNER := TRUE;
        END IF;
    CLOSE C_REQUISITOS_BANNER;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_REQUISITOS_PREVIOS;