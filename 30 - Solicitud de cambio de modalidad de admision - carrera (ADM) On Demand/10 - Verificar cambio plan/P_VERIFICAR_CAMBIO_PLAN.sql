/*
SET SERVEROUTPUT ON
DECLARE P_CAMBIO_PLAN VARCHAR2(4000);
 P_CATALOGO_NEW varchar2(4000);
 P_ATTS_CODE_NEW VARCHAR2(4000);
 P_MESSAGE VARCHAR2(4000);
BEGIN
    P_VERIFICAR_CAMBIO_PLAN(79812, 'UREG', '02', '201520', '201910', P_CAMBIO_PLAN, P_CATALOGO_NEW, P_ATTS_CODE_NEW, P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_CAMBIO_PLAN);
    DBMS_OUTPUT.PUT_LINE(P_CATALOGO_NEW);
    DBMS_OUTPUT.PUT_LINE(P_ATTS_CODE_NEW);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_CAMBIO_PLAN (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MOD_ADM_NEW       IN VARCHAR2,
    P_CATALOGO          IN VARCHAR2,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_PLAN       OUT VARCHAR2,
    P_CATALOGO_NEW      OUT VARCHAR2,
    P_ATTS_CODE_NEW     OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
    V_FLAG_CATALOGO     NUMBER := 0;
	V_APEC_TERM         VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);
    V_APEC_MOD_ADM_NEW  VARCHAR(10);
BEGIN
    
    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;
    
    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
    INTO V_APEC_DEPT
    FROM CZRDEPT 
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- MODALIDAD DE ADMISION
    SELECT "IDModalidadPostu"
        INTO V_APEC_MOD_ADM_NEW
    FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDModRecruiter" = P_MOD_ADM_NEW
    AND "departament" = V_APEC_DEPT;
    
    -- OBTENER SI LA MODALIDAD ES DEL PLAN NUEVO O NO
    SELECT "PrimerCiclo"
        INTO V_FLAG_CATALOGO
    FROM tblModalidadPostu@BDUCCI.CONTINENTAL.EDU.PE
    WHERE TRIM("IDDependencia") = 'UCCI'
    AND TRIM("IDPerAcad") = V_APEC_TERM
    AND TRIM("IDEscuelaADM") = V_APEC_DEPT
    AND "IDModalidadPostu" = V_APEC_MOD_ADM_NEW;
    
    --VALIDAR Y OBTENER LOS NUEVOS DATOS DE CAMBIO
    IF V_FLAG_CATALOGO = 0 THEN
        IF P_CATALOGO < '201810' THEN
            P_CAMBIO_PLAN := 'FALSE';
            P_CATALOGO_NEW := P_CATALOGO;
            P_MESSAGE := 'El estudiante no cambia de plan de estudios y se mantiene en el plan 2015.';
        ELSE
            P_CAMBIO_PLAN := 'TRUE';
            P_CATALOGO_NEW := '201710';
            P_MESSAGE := 'El estudiante cambia de plan de estudios 2015 y su periodo de catalogo ahora sera el 201710.';
        END IF;
        P_ATTS_CODE_NEW := 'P015';
    ELSE
        -- SI EL PLAN ES DESPUES DEL 2018
        IF P_CATALOGO > '201800' THEN
            P_CAMBIO_PLAN := 'FALSE';
            P_CATALOGO_NEW := P_CATALOGO;
            P_MESSAGE := 'El estudiante no cambia de plan de estudios y se mantiene en el plan 2018.';
        ELSE
            P_CAMBIO_PLAN := 'TRUE';
            P_CATALOGO_NEW := P_TERM_CODE;
            P_MESSAGE := 'El estudiante cambia de plan de estudios al 2018 y su periodo de catalogo ahora sera el ' || P_TERM_CODE || '.';
        END IF;
        P_ATTS_CODE_NEW := 'P018';
    END IF;
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_CAMBIO_PLAN;