/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_MESSAGE VARCHAR2(4000);
BEGIN
    P_VERIFICAR_APTO(216, '201820', P_STATUS_APTO, P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_MESSAGE         OUT VARCHAR2
)
AS
        V_SOLTRA            INTEGER := 0; --VERIFICA SOL. TRASLADO INTERNO
        V_SOLPER            INTEGER := 0; --VERIFICA SOL. PERMANENCIA DE PLAN
        V_SOLRES            INTEGER := 0; --VERIFICA SOL. RESERVA
        V_SOLREC            INTEGER := 0; --VERIFICA SOL. RECTIFICACION DE MATRICULA
        V_SOLCAM            INTEGER := 0; --VERIFICA SOL. CAMBIO DE PLAN
        V_SOLCUO            INTEGER := 0; --VERIFICA SOL. CAMBIO DE CUOTA INICIAL
        V_SOLDIR            INTEGER := 0; --VERIFICA SOL. ASIGNTURA DIRIGIDA
        V_SOLCON            INTEGER := 0; --VERIFICA SOL. DESCUENTO POR CONVENIO
        V_ING_PER           INTEGER := 0; --VERIFICA SI EL ESTUDIANTE ES INGRESANTE EN EL PERIODO SOLICITADO
        V_RESPUESTA         VARCHAR2(4000) := NULL; --RESPUESTA ANIDADA DE LAS VALIDACIONES
        V_FLAG              INTEGER := 0; --BANDERA DE VALIDACIÓN
        
      
BEGIN
    ---- P_VERIFICAR_APTO PARA SOLICITUD DE CAMBIO DE CARRERA (ADMISION) ON DEMAND

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');

    IF V_SOLREC > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE CUOTA INICIAL ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCUO
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL007'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCUO > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DESCUENTO POR CONVENIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCON
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL019'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLCON > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Descuento por convenio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Descuento pot convenio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE PERIODO DE INGRESO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_ING_PER
    FROM SARADAP
        WHERE SARADAP_PIDM = P_PIDM
        AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE;

    IF V_ING_PER <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El estudiante no tiene registro de postulacion en el periodo solicitado. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El estudiante no tiene registro de postulacion en el periodo solicitado. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_MESSAGE := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_MESSAGE := 'OK';
    END IF;

EXCEPTION 
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;