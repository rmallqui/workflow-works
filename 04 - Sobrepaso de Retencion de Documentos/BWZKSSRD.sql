/**********************************************************************************************/
/* BWZKSSRD.sql                                                                          */
/**********************************************************************************************/
/*                                                                                            */
/* Descripcion corta: Script para generar el Paquete de automatizacion del proceso de         */
/*                    Solicitud de Sobrepaso de Retencion documentaria                        */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creacion del Código.                                        BYF             09/JUL/2018 */
/*    --------------------                                                                    */
/*    Creación del paquete de Sobrepaso de Retencion de documentos en reemplazo del paquete   */
/*      WFK_CONTISRD para adaptar las nomenclaturas bases para el trabajo.                    */
/*    Procedure P_GET_USUARIO_REGACA: En base a una sede y un rol, el procedimiento LOS       */
/*      CORREOS deL(os) responsable(s) de Registros Academicos de esa sede.                   */
/*    --Paquete generico                                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX)          */
/*    Procedure P_OBTENER_COMENTARIO: Obtiene el comentario y telefono del estudiante         */
/*      registrado en el sistema.                                                             */
/*    Procedure P_ACTUALIZAR_FECHA_RETENCION: Actualiza la fecha de la retencion 02.          */
/* 2. Actualizacion Paquete Generico                                                          */
/*    Se actualiza el SP P_CAMBIO_ESTADO_SOLICITUD del paquete para que llame al SP del       */
/*      paquete generico.                                                                     */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
-- PERMISOS DE EJECUCION
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

create or replace PACKAGE BWZKSSRD AS
/*******************************************************************************
 BWZKSSRD:
       Conti Package body SOLICITUD DE SOBREPASO DE RETENCION DE DOCUMENTOS
*******************************************************************************/
-- FILE NAME..: BWZKSSRD.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSSRD
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2018
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

PROCEDURE P_GET_USUARIO_REGACA (
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
        P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
        P_CORREO_REGACA       OUT VARCHAR2,
        P_ROL_SEDE            OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
        P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
        P_AN                  OUT NUMBER,
        P_ERROR               OUT VARCHAR2
);
              
--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_COMENTARIO          OUT VARCHAR2,
        P_TELEFONO            OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_ACTUALIZAR_FECHA_RETENCION (
        P_ID_ALUMNO           IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_COD_RETENCION       IN  STVHLDD.STVHLDD_CODE%TYPE,
        P_FECHA_RETENCION     IN  VARCHAR2,
        P_FECHA_NEW_RET       OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
);

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSSRD;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSSRD;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSSRD FOR BANINST1.BWZKSSRD;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSSRD
--  START gurgrth BWZKSSRD
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/