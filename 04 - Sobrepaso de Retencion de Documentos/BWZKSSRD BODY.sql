/**********************************************************************************************/
/* BWZKSSRD.sql                                                                          */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Sobrepaso de Retención de Documentos                       */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creacion del Código.                                        BYF             09/JUL/2018 */
/*    --------------------                                                                    */
/*    Creación del paquete de Sobrepaso de Retención documentaria.                            */
/*    Procedure P_GET_USUARIO_REGACA: En base a una sede y un rol, el procedimiento LOS       */
/*      CORREOS deL(os) responsable(s) de Registros Académicos de esa sede.                   */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX)          */
/*    Procedure P_OBTENER_COMENTARIO: Obtiene el comentario y telefono del estudiante         */
/*		registrado en el sistema.                                                             */
/*    Procedure P_ACTUALIZAR_FECHA_RETENCION: Actualiza la fecha de la retención 02.          */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BODY BWZKSSRD AS
/*******************************************************************************
 BWZKSSRD:
       Conti Package body SOLICITUD DE SOBREPASO DE RETENCION DE DOCUMENTOS
*******************************************************************************/
-- FILE NAME..: BWZKSSRD.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSSRD
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2018
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

PROCEDURE P_GET_USUARIO_REGACA (
    P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
    P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
    P_CORREO_REGACA       OUT VARCHAR2,
    P_ROL_SEDE            OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO_REGACA
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    P_ROLE_ID                 NUMBER;
    P_ORG_ID                  NUMBER;
    P_USER_ID                 NUMBER;
    P_ROLE_ASSIGNMENT_ID      NUMBER;
    P_Email_Address           VARCHAR2(100);
    
    P_SECCION_EXCEPT          VARCHAR2(50);
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT *
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID
            AND ROLE_ID = P_ROLE_ID;
    
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
    P_ROL_SEDE := P_ROL || P_COD_SEDE;
    
    -- Obtener el ROL_ID 
    P_SECCION_EXCEPT := 'ROLES';
    
    SELECT ID
        INTO P_ROLE_ID
    FROM WORKFLOW.ROLE 
    WHERE NAME = P_ROL_SEDE;
    
    P_SECCION_EXCEPT := '';
    
    -- Obtener el ORG_ID 
    P_SECCION_EXCEPT := 'ORGRANIZACION';
    
    SELECT ID
        INTO P_ORG_ID
    FROM WORKFLOW.ORGANIZATION
    WHERE NAME = 'Root';
    
    P_SECCION_EXCEPT := '';
    
    -- Obtener los datos de usuarios que relaciona rol y usuario
    P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
    -- #######################################################################
    OPEN C_ROLE_ASSIGNMENT;
    LOOP
        FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
        EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
            -- Obtener Datos Usuario
            SELECT Email_Address
                INTO P_Email_Address
            FROM WORKFLOW.WFUSER
            WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
            
            P_CORREO_REGACA := P_CORREO_REGACA || P_Email_Address || ',';
            
    END LOOP;
    CLOSE C_ROLE_ASSIGNMENT;
    
    P_SECCION_EXCEPT := '';
    
    -- Extraer el ultimo digito en caso sea un "coma"(,)
    SELECT SUBSTR(P_CORREO_REGACA,1,LENGTH(P_CORREO_REGACA) -1)
        INTO P_CORREO_REGACA
    FROM DUAL
    WHERE SUBSTR(P_CORREO_REGACA,-1,1) = ',';
      
EXCEPTION
WHEN TOO_MANY_ROWS THEN 
    IF ( P_SECCION_EXCEPT = 'ROLES') THEN
        -- --DBMS_OUTPUT.PUT_LINE ();
        P_ERROR := 'Ha ocurrido un problema, se encontraron más de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
    ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
        P_ERROR := 'Ha ocurrido un problema, se encontraron más de una ORGANIZACIÒN con el mismo nombre.';
    ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
        P_ERROR := 'Ha ocurrido un problema, Se encontraron más de un usuario con el mismo ROL.';
    ELSE 
        P_ERROR := SQLERRM;
    END IF; 
WHEN NO_DATA_FOUND THEN
    IF ( P_SECCION_EXCEPT = 'ROLES') THEN
      P_ERROR := 'Ha ocurrido un problema, NO se encontró el nombre del ROL: ' || P_ROL || P_COD_SEDE;
    ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
      P_ERROR := 'Ha ocurrido un problema, NO se encontró el nombre de la ORGANIZACIÓN.';
    ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
      P_ERROR := 'Ha ocurrido un problema, NO se encontró ningún usuario con esas características.';
    ELSE
        P_ERROR := SQLERRM;
    END IF; 
WHEN OTHERS THEN
    P_ERROR := 'Ha ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
    
END P_GET_USUARIO_REGACA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_NUMERO_SOLICITUD, P_ESTADO, P_AN, P_ERROR);
        
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_OBTENER_COMENTARIO(
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_COMENTARIO          OUT VARCHAR2,
    P_TELEFONO            OUT VARCHAR2
)
AS
    V_ERROR                   EXCEPTION;
    V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
    V_COMENTARIO              VARCHAR2(4000);
    V_CLAVE                   VARCHAR2(4000);
    V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
    
    -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALIZADO.)
    CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD ON --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;

BEGIN
      
    -- GET CODIGO SOLICITUD
    SELECT SVRSVPR_SRVC_CODE 
        INTO V_CODIGO_SOLICITUD 
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
    OPEN C_SVRSVAD;
    LOOP
        FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
            IF C_SVRSVAD%FOUND THEN
                IF V_ADDL_DATA_SEQ = 1 THEN
                  -- OBTENER COMENTARIO DE LA SOLICITUD
                  P_COMENTARIO := V_COMENTARIO;
                ELSIF V_ADDL_DATA_SEQ = 2 THEN
                  -- OBTENER EL TELEFONO DE LA SOLICITUD 
                  P_TELEFONO := V_COMENTARIO;
                END IF;
            ELSE EXIT;
            END IF;
    END LOOP;
    CLOSE C_SVRSVAD;
    
--    -- CASO ESPECIAL ******
--    IF P_FOLIO_SOLICITUD = 4410 OR P_FOLIO_SOLICITUD = 4405 THEN 
--        P_TELEFONO := '-';
--    END IF;

    --VALIDACIÓN DE DATOS REGISTRADOS
    IF (P_TELEFONO IS NULL OR P_COMENTARIO IS NULL) THEN
        RAISE V_ERROR;
    END IF;
      
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_OBTENER_COMENTARIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_ACTUALIZAR_FECHA_RETENCION (
    P_ID_ALUMNO           IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_COD_RETENCION       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_FECHA_RETENCION     IN  VARCHAR2,
    P_FECHA_NEW_RET       OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
)
AS
    P_INDICADOR               NUMBER;
    E_INVALID_COD_RETENCION   EXCEPTION;
    P_FROM_DATE               SPRHOLD.SPRHOLD_FROM_DATE%TYPE;
BEGIN
      
    -- CONVERTIR LA FECHA A UN FORMATO VALIDO
    --SELECT TO_DATE('30-Dec-2016 16:28:36','DD-MON-YYYY HH24:MI:SS','NLS_DATE_LANGUAGE = American') 
    SELECT TO_DATE(P_FECHA_RETENCION,'DD-MON-YYYY HH24:MI:SS','NLS_DATE_LANGUAGE = American') 
        INTO P_FROM_DATE
    FROM DUAL;
    
    -- VALIDAR codigo del TIPO DE RETENCION - STVHLDD
    SELECT COUNT(*)
        INTO P_INDICADOR 
    FROM STVHLDD
    WHERE STVHLDD_CODE = P_COD_RETENCION;
    
    -- INSERTAR REGISTRO DE FECHA DE RETENCION DOCUMENTOS - SOAHOLD
    IF ( P_INDICADOR = 0 ) THEN
        RAISE E_INVALID_COD_RETENCION;
    ELSE
        UPDATE SPRHOLD
        SET SPRHOLD_FROM_DATE = P_FROM_DATE, SPRHOLD_ACTIVITY_DATE = SYSDATE
        WHERE SPRHOLD_PIDM = P_ID_ALUMNO
        AND SPRHOLD_HLDD_CODE = P_COD_RETENCION;
        
        COMMIT;
    END IF;
    
    -- GET FECHA PARA EL MENSAJE DE NOTIFICACIÓN
    SELECT TO_CHAR(P_FROM_DATE,'dd/mm/yyyy')
        INTO P_FECHA_NEW_RET
    FROM DUAL;

EXCEPTION
WHEN E_INVALID_COD_RETENCION THEN
    P_ERROR  := 'El "CÓDIGO DE RETENCIÓN" enviado es inválido';
WHEN OTHERS THEN
    --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
    P_ERROR := 'Ha ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_ACTUALIZAR_FECHA_RETENCION;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
           
END BWZKSSRD;

/**************************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**************************************************************************************************/