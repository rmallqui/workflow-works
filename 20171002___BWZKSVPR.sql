/******************************************************************************/
/* BWZKSVPR.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para generar el Paquete de procedimientos usados */
/*  en los triggers de inicio de WorkFlow                                     */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del Código.                                    RML 24/NOV/2017 */
/*  Procedure P_CONTISTI_GETDATA: Obtener los datos dept, camp, nombre camp y */
/*    periodo de un PIDM.                                                     */
/*  Procedure P_CONTISPGA_GETDATA: Obtener los datos dept, camp, nombre camp  */
/*    y periodo de un PIDM                                                    */
/*  Procedure P_CONTISPP_GETDATA: Obtener los datos dept, camp, nombre camp   */
/*    y periodo de un PIDM                                                    */
/*  Procedure P_CONTISPBT_GETDATA: Obtener los datos dept, camp, nombre camp  */
/*    y periodo de un PIDM                                                    */
/*  Procedure P_CONTISDPC_GETDATA: Obtener los datos dept, camp, nombre camp  */
/*    y periodo de un PIDM                                                    */
/*  Procedure P_CONTISMCV_GETDATA:                            LAM 06/DIC/2017 */
/*  Obtener los datos dept, camp, nombre camp y periodo de un PIDM.           */
/*  Procedure P_CONTISPPE_GETDATA:                            LAM 10/JUN/2018 */
/*  Obtener los datos dept, camp, nombre camp y periodo de un PIDM.           */
/*  Procedure P_CONTISMAD_GETDATA:                            LAM 25/JUN/2018 */
/*  Obtener los datos dept, camp, nombre camp y periodo de un PIDM.           */
/*                                                                            */
/*    --------------------                                                    */
/*                                                                            */
/* 2. Actualización P_CONTISDPC_GETDATA                       BFV 12/06/2018  */
/*    Se actualiza la consulta para que obtenga el periodo de ingreso de los  */
/*    ingresantes sin depender de la fecha de matricula.                      */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 

/**********************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************/
-- CREATE OR REPLACE PUBLIC SYNONYM "BWZKSVPR" FOR "BANINST1"."BWZKSVPR";
-- GRANT EXECUTE ON BANINST1.BWZKSVPR TO SATURN;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

CREATE OR REPLACE PACKAGE BWZKSVPR AS
           
--## SOLICITUD DE TRASLADO INTERNO (CONTISTI) ## 
PROCEDURE P_CONTISTI_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--------------------------------------------------------------------------------
--## SOLICITUD DE PRORROGA GASTO ADMINISTRATIVO (CONTISPGA) ## 
PROCEDURE P_CONTISPGA_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--------------------------------------------------------------------------------
--## SOLICITUD DE PPRONTO PAGO (CONTISPP) ## 
PROCEDURE P_CONTISPP_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--------------------------------------------------------------------------------
--## SOLICITUD DE PAGO TRAMITE BACHILLER y TITULACION (CONTISPBT) ## 
PROCEDURE P_CONTISPBT_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

--## SOLICITUD DE DESCUENTO POR CONVENIO (CONTISDPC) ## 
PROCEDURE P_CONTISDPC_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-- 

--## SOLICITUD DE MODIFICACIÓN DE CUOTA INICIAL DE VERANO (CONTISMCV) ##

PROCEDURE P_CONTISMCV_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
);

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--

--## SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS (CONTISPPE) ##

PROCEDURE P_CONTISPPE_GETDATA (
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
);

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--

--## SOLICITUD DE MATRICULA DE ASIGNATURA DIRIGIDA (CONTISMAD) ##

PROCEDURE P_CONTISMAD_GETDATA (
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
);

END BWZKSVPR;