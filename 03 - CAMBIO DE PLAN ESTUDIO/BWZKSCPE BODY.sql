/**********************************************************************************************/
/* BWZKSCPE BODY.sql                                                                          */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Cambio de Plan de Estudios.                                */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        RML             04/ENE/2017 */
/*    --------------------                                                                    */
/*    Creación del paquete de Cambio de Plan de Estudios                                      */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante solicitante                      */
/*      cumplas las condiciones para hacer uso del WF.                                        */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el procedimiento LOS CORREOS del  */
/*              responsable(s)de Registros Académicos de esa sede.                            */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Valida el estadp de la solicitud.               */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX).         */
/*    Procedure P_OBTENER_COMENTARIO_ALUMNO: Obtiene COMENTARIO Y TELEFONO de una solicitud.  */
/*    Procedure P_MODIFICAR_CATALOGO: Modifica EL PERIODO CATALOGO.                           */
/*    Procedure P_MODIFICAR_RETENCION_B: Modifica la fecha limite de una retencion a un dia   */
/*              antes a la fecha actual.                                                      */
/*    Procedure P_SET_ATRIBUTO_PLANS : SET ATRIBUTO de PLAN ESTUDIOS a un periodo determinado.*/
/*    Procedure P_SET_CURSO_INTROD: Asigna los cursos introductorios de RM y RV o PR con nota */
/*              11 solo en caso no tuviera alguno.                                            */
/*    Procedure P_CPE_CONV_ASIGN: Convalidad las asignaturas del plan 2007 al plan 2015.      */
/*    Procedure P_CPE_EXECCAPP: Ejecución de CAPP.                                            */
/*    Procedure P_CPE_EXECPROY: Ejecución de proyección.                                      */
/*    Procedure P_APEC_SET_DEUDA: SP - ANIDADO genera una deuda por CODIGO DETALLE --- APEC.  */
/*    Procedure P_GET_ASIG_RECONOCIDAS: Muestra el listado de asignaturas reconocidas.        */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/
CREATE OR REPLACE PACKAGE BODY BWZKSCPE AS

PROCEDURE P_VERIFICA_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_FOLIO_SOLICITUD IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_verificar_apto
  FECHA     : 05/07/2018
  AUTOR     : Flores Vilcapoma, Brian Yusef
  OBJETIVO  : Verifica que el estudiante solicitante cumpla con las condiciones necesarias para hacer uso del WF.
  =================================================================================================================== */
AS
        V_RET01             INTEGER := 0; --VALIDA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VALIDA RETENCIÓN 02
        V_SOLTRA            INTEGER := 0; --VALIDA SOLICITUD TRASLADO INTERNO
        V_SOLRES            INTEGER := 0; --VALIDA SOLICITUD RESERVA
        V_SOLREC            INTEGER := 0; --VALIDA SOLICITUD RECTIFICACION
        V_SOLCUO            INTEGER := 0; --VALIDA SOLICITUD CAMBIO CUOTA INICIAL
        V_SOLPER            INTEGER := 0; --VALIDA SOLICITUD PERMANENCIA
        V_RET               INTEGER := 0; --VALIDA SOLICITUD RETENCIÓN 07
        V_SOLDIR            INTEGER := 0; --VALIDA SOLICITUD DIRIGIDA
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
        
        V_CODE_DEPT      STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_TERM      STVTERM.STVTERM_CODE%TYPE;
        V_CODE_CAMP      STVCAMP.STVCAMP_CODE%TYPE;
        V_PROGRAM        SORLCUR.SORLCUR_PROGRAM%TYPE;
        V_TERMCTLG       SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
        V_TERMCTLG_MAX   SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
        
        V_SUB_PTRM              VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        V_PTRM_CODE             STVPTRM.STVPTRM_CODE%TYPE;
        V_MATRIC_FECHA          SFRRSTS.SFRRSTS_START_DATE%TYPE; --- Fecha de una semana antes de matricula
        
        V_PRIORIDAD             NUMBER;
        V_HAS_RETENCION         NUMBER;
        V_HAS_NRC               NUMBER;
        
        V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
        V_CONFIRMACION            VARCHAR2(4000);
        P_CONFIRMACION            VARCHAR2(4000);
        V_CLAVE                   VARCHAR2(4000);
        V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
        
    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;

    --################################################################################################
    -- OBTENER PARTE DE PERIODO (sub PTRM)
    --################################################################################################
    CURSOR C_SFRRSTS_PTRM IS
        SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2) SUBPTRM
        FROM CZRPTRM 
        WHERE CZRPTRM_DEPT = V_CODE_DEPT
        AND CZRPTRM_CAMP_CODE = V_CODE_CAMP;

    --################################################################################################
    /*
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
        A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
        - y UNA SEMANA ANTES DE INICIO DE CLASES(SOBPTRM_START_DATE-7) -- LOS Q NO ESTUDIARON EL PERIODO ANTERIOR
        - HASTA 1 mes despues de inicio de clases (SOBPTRM_START_DATE) -- ALUMNO NORMAL 
        - Hasta 1 mes despues para calcular el PERIODO debido a las constantes ampliaciones de fechas.
    */
    --################################################################################################
    CURSOR C_STUDN_TERM IS
        SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, FECHA_MATRICULA FROM (
            SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, FECHA_MATRICULA FROM (
                -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
                SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') FECHA_MATRICULA
                FROM SOBPTRM
                INNER JOIN SFRRSTS ON 
                    SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                    AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
                WHERE SFRRSTS_RSTS_CODE = 'RW'
                    AND SOBPTRM_PTRM_CODE IN (V_SUB_PTRM || '1', CASE WHEN V_CODE_DEPT='UVIR' OR V_CODE_DEPT='UPGT' THEN (V_SUB_PTRM||'2') ELSE '-' END) 
                    AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
                    AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE-7,'dd/mm/yyyy'),'dd/mm/yyyy') AND (ADD_MONTHS(SOBPTRM_START_DATE,1))
                ORDER BY SOBPTRM_TERM_CODE 
            ) ORDER BY SOBPTRM_TERM_CODE ,SOBPTRM_PTRM_CODE 
        ) WHERE ROWNUM <= 1;

    -->> OBTENER LA RESPUESTA DE CONFIRMACION
        CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD ON--------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
            --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
        
BEGIN
    ---P_VERIFICAR_APTO PARA SOLICITUD DE CAMBIO DE PLAN
    
    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
    CLOSE C_SORLCUR;

    -->> Obtener PARTE PERIODO
    OPEN C_SFRRSTS_PTRM;
    LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFRRSTS_PTRM;
    
    SELECT SVRSVPR_SRVC_CODE 
      INTO V_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_CONFIRMACION;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 3 THEN
                  -- OBTENER COMENTARIO DE LA SOLICITUD
                P_CONFIRMACION := V_CONFIRMACION;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;
    
    --################################################################################################
    --- REVISIÓN DE PLAN DE ESTUDIOS
    --################################################################################################
    IF (V_TERMCTLG >= V_TERMCTLG_MAX) THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en un plan de estudios vigente. ';
        V_FLAG := 1;
    END IF; 

    --################################################################################################
    --- VALIDACIÓN DE ESTUDIANTES DE PROGRAMA 309 (Administración - MKT y Neg. Internacionales)
    --################################################################################################
    IF V_PROGRAM = '309' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. No esta habilitado para este flujo. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. No esta habilitado para este flujo. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    --- VALIDACIÓN DE ESTUDIANTES DE PROGRAMA 308 (Administración)
    --################################################################################################
    IF V_PROGRAM = '308' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en la carrera de Administración. No esta habilitado para este flujo. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en la carrera de Administración. No esta habilitado para este flujo. ';            
        END IF;
        V_FLAG := 1;
    END IF; 

    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
   ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE CUOTA INICIAL ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCUO
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL007'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCUO > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO MULTIMODAL
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL016'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Multimodal activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Multimodal activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    --- REVISIÓN DE CONFIRMACION DE CAMBIO DE PLAN
    --################################################################################################
    IF ( P_CONFIRMACION = 'No') THEN
        V_RESPUESTA := V_RESPUESTA || '<br />' || '- No acepto realizar el cambio de plan de estudios. ';
        V_FLAG := 1;
    END IF; 

--    ---GET TERM (PERIODO - Solo usando parte de periodo 1) */
--      OPEN C_STUDN_TERM;
--      LOOP
--        FETCH C_STUDN_TERM INTO V_CODE_TERM, V_PTRM_CODE, V_MATRIC_FECHA;
--        EXIT WHEN C_STUDN_TERM%NOTFOUND;
--        END LOOP;
--      CLOSE C_STUDN_TERM;
      
--    /******************************************************************************/
--    -- PARA SEGUNDO MODULO: Verificar que no tenga NRC en el primer modulo
--    IF (V_PTRM_CODE = (V_SUB_PTRM||'2')) THEN
--        SELECT COUNT(*)
--        INTO V_HAS_NRC
--        FROM SFRSTCR  
--        WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
--            AND SFRSTCR_PIDM = P_PIDM
--            AND SFRSTCR_PTRM_CODE = (V_SUB_PTRM||'1');
--        
--        IF  (V_HAS_NRC > 0 OR  -- No haber estudiado en el primer modulo
--            SYSDATE < V_MATRIC_FECHA) -- En segundo modulo: desde inicio de matricula <-> inicio de clases 
--            THEN
--                P_STATUS_APTO := 'FALSE';
--                P_REASON := 'El estudiante tiene matricula';
--                RETURN;
--        END IF;
--    -- PARA PRIMER MODULO O UN CASO NORMAL: verificar si estudió el periodo anterior.
--    ELSE
--        IF (SYSDATE < V_MATRIC_FECHA) -- De haber estudiado el periodo anterior: matricula <-> inicio de clases
--        THEN
--            P_STATUS_APTO := 'FALSE';
--            P_REASON := 'No esta en fecha de matricula';
--            RETURN;
--        END IF;
--    END IF;

    --################################################################################################
    ---VALIDACIÓN DE RETENCION 07 ACTIVA
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '07'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No tiene activo la retención de Cambio de Plan de Estudio. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No tiene activo la retención de Cambio de Plan de Estudio. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICA_APTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_USUARIO (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO_REGACA       OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_get_usuario_regacad
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    P_ROLE_ID                 NUMBER;
    P_ORG_ID                  NUMBER;
    P_USER_ID                 NUMBER;
    P_ROLE_ASSIGNMENT_ID      NUMBER;
    P_Email_Address           VARCHAR2(100);
    
    P_SECCION_EXCEPT          VARCHAR2(50);
      
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID
        AND ROLE_ID = P_ROLE_ID;
    
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
    
BEGIN 
--
    P_ROL_SEDE := P_ROL || P_COD_SEDE;
    
    -- Obtener el ROL_ID 
    P_SECCION_EXCEPT := 'ROLES';
    
    SELECT ID
        INTO P_ROLE_ID
    FROM WORKFLOW.ROLE 
        WHERE NAME = P_ROL_SEDE;
        
    P_SECCION_EXCEPT := '';
    
    -- Obtener el ORG_ID 
    P_SECCION_EXCEPT := 'ORGRANIZACION';
    
    SELECT ID 
        INTO P_ORG_ID
    FROM WORKFLOW.ORGANIZATION
    WHERE NAME = 'Root';
    
    P_SECCION_EXCEPT := '';
    
    -- Obtener los datos de usuarios que relaciona rol y usuario
    P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
    
    -- #######################################################################
    OPEN C_ROLE_ASSIGNMENT;
    LOOP
        FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
        EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                  
            -- Obtener Datos Usuario
            SELECT Email_Address
                INTO P_Email_Address
            FROM WORKFLOW.WFUSER
            WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
            
            P_CORREO_REGACA := P_CORREO_REGACA || P_Email_Address || ',';
        
    END LOOP;
    CLOSE C_ROLE_ASSIGNMENT;
    
    P_SECCION_EXCEPT := '';
    
    -- Extraer el ultimo digito en caso sea un "coma"(,)
    SELECT SUBSTR(P_CORREO_REGACA,1,LENGTH(P_CORREO_REGACA) -1)
        INTO P_CORREO_REGACA
    FROM DUAL
    WHERE SUBSTR(P_CORREO_REGACA,-1,1) = ',';
      
EXCEPTION
    WHEN TOO_MANY_ROWS THEN 
        IF ( P_SECCION_EXCEPT = 'ROLES') THEN
          P_ERROR := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
        ELSIF (P_SECCION_EXCEPT = 'ORGANIZACION') THEN
          P_ERROR := '- Se encontraron mas de una ORGANIZACIÓN con el mismo nombre.';
        ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
          P_ERROR := '- Se encontraron mas de un usuario con el mismo ROL.';
        ELSE  P_ERROR := SQLERRM;
        END IF; 
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
    WHEN NO_DATA_FOUND THEN
        IF ( P_SECCION_EXCEPT = 'ROLES') THEN
          P_ERROR := '- NO se encontró el nombre del ROL: '  || P_ROL || P_COD_SEDE;
        ELSIF (P_SECCION_EXCEPT = 'ORGANIZACION') THEN
          P_ERROR := '- NO se encontró el nombre de la ORGANIZACION.';
        ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
          P_ERROR := '- NO  se encontró ningun usuario con esas caracteristicas.';
        ELSE  P_ERROR := SQLERRM;
        END IF; 
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
    WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STATUS_SOL          OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VERIFICAR_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_STATUS_SOL, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_ESTADO, P_AN, P_ERROR);
        
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

------------------------------------------------------------------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_INSCRIP (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_INSCRIP  OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
)
AS
BEGIN
    BWZKPSPG.P_VERIFICAR_MATRICULA (P_PIDM,P_TERM_CODE,P_STATUS_INSCRIP,P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_INSCRIP;
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_ELIMINAR_NRC (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE         OUT VARCHAR2
    
)
AS
BEGIN
    BWZKPSPG.P_ELIMINAR_NRC (P_PIDM,P_TERM_CODE,P_MESSAGE);
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);    

END P_ELIMINAR_NRC;
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_OBTENER_RESPUESTA (
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO          OUT VARCHAR2,
      P_TELEFONO            OUT VARCHAR2,
      P_RESPUESTA           OUT VARCHAR2,
      P_MENSAJE             OUT VARCHAR2
)

AS

    V_ERROR                   EXCEPTION;
    V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
    V_COMENTARIO              VARCHAR2(4000);
    V_CLAVE                   VARCHAR2(4000);
    V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
    
    -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
    CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD ON--------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
            --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;

BEGIN
      
    -- GET CODIGO SOLICITUD
    SELECT SVRSVPR_SRVC_CODE 
        INTO V_CODIGO_SOLICITUD
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
    OPEN C_SVRSVAD;
    LOOP
        FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
        IF C_SVRSVAD%FOUND THEN
            IF V_ADDL_DATA_SEQ = 1 THEN
                -- OBTENER COMENTARIO DE LA SOLICITUD
                P_COMENTARIO := V_COMENTARIO;
            ELSIF V_ADDL_DATA_SEQ = 2 THEN
                -- OBTENER EL TELEFONO DE LA SOLICITUD 
                P_TELEFONO := V_COMENTARIO;
            ELSIF V_ADDL_DATA_SEQ = 4 THEN
                P_RESPUESTA := V_COMENTARIO;
            END IF;
        ELSE EXIT;
    END IF;
    END LOOP;
    CLOSE C_SVRSVAD;
    
    -- CASO ESPECIAL ******
    IF P_FOLIO_SOLICITUD = 4410 OR P_FOLIO_SOLICITUD = 4405 THEN 
        P_TELEFONO := '-';
    END IF;
    
    IF (P_TELEFONO IS NULL OR P_COMENTARIO IS NULL OR P_RESPUESTA IS NULL) THEN
      RAISE V_ERROR;
    END IF;
      
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_OBTENER_RESPUESTA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/*
PROCEDURE P_OBTENER_PRIORIDAD 
*/

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/*
PROCEDURE P_GET_SIDEUDA_ALUMNO 
*/

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/*
PROCEDURE P_ELIMINAR_RECARGO 
*/

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_MODIFICAR_CATALOGO (
    P_PIDM_ALUMNO      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PERIODO          IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_ERROR            OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_MODIFICAR_CATALOGO (P_PIDM_ALUMNO, P_PERIODO, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
      P_ERROR := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_CATALOGO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_MODIFICAR_RETENCION_B (
    P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE4       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
) 
AS
BEGIN

    BWZKPSPG.P_MODIFICAR_RETENCION_B (P_PIDM, P_FOLIO_SOLICITUD, P_STVHLDD_CODE1, P_STVHLDD_CODE2, P_STVHLDD_CODE3, P_STVHLDD_CODE4, P_MESSAGE);

EXCEPTION
    WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_RETENCION_B;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/*
PROCEDURE P_VALIDAR_FECHA_SOLICITUD 
*/

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_ATRIBUTO_PLANS (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,      -------- COD atributos
    P_ERROR             OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_SET_ATRIBUTO_PLAN (P_PIDM, P_TERM_CODE, P_ATTS_CODE, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_ATRIBUTO_PLANS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CURSO_INTROD (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
)
AS
    V_STATUS_DATE_AP      SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
BEGIN

    -- Get la fecha de aprovación de la solicitud
    SELECT SVRSVPR_STATUS_DATE
    INTO V_STATUS_DATE_AP
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD 
        AND SVRSVPR_SRVS_CODE = 'AP';

    BWZKPSPG.P_SET_CURSO_INTROD (P_PIDM, P_TERM_CODE, V_STATUS_DATE_AP, P_DEPT_CODE, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CURSO_INTROD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CPE_CONV_ASIGN(
    P_TERM_CODE   IN STVTERM.STVTERM_CODE%TYPE,
    P_CATALOGO    IN STVTERM.STVTERM_CODE%TYPE,
    P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_MESSAGE     OUT VARCHAR2
)
AS   
BEGIN

    BWZKPSPG.P_CONV_ASIGN (P_TERM_CODE, P_CATALOGO, P_PIDM, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CPE_CONV_ASIGN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CPE_EXECCAPP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN 

    BWZKPSPG.P_EXECCAPP (P_PIDM, P_TERM_CODE, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CPE_EXECCAPP;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CPE_EXECPROY (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN 

    BWZKPSPG.P_EXECPROY (P_PIDM, P_TERM_CODE, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CPE_EXECPROY;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

-----------------------------------  SP ANIDADO -----------------------------------
PROCEDURE P_APEC_SET_DEUDA ( 
        P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_COD_DETALLE           IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE,
        P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_APEC_SET_DEUDA
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE --- APEC.

            : COMMIT - SP ANIDADO, El "COMMIT" se realiza desde el SP principal.
            
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        
    C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
    C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
    C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
    C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
    C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
    C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
    C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
    C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
    C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
    C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
    
    P_SERVICE               VARCHAR2(10);
    P_PART_PERIODO          VARCHAR2(9);
    P_APEC_CAMP             VARCHAR2(9);
    P_APEC_DEPT             VARCHAR2(9);
    P_APEC_TERM             VARCHAR2(10);
    P_APEC_IDSECCIONC       VARCHAR2(15);
    P_APEC_FECINIC          DATE;
    P_APEC_MOUNT            NUMBER;
    P_APEC_IDALUMNO         VARCHAR2(10);
        
BEGIN

    -- #######################################################################
    -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
    SELECT    SORLCUR_SEQNO,      
            SORLCUR_LEVL_CODE,    
            SORLCUR_CAMP_CODE,        
            SORLCUR_COLL_CODE,
            SORLCUR_DEGC_CODE,  
            SORLCUR_PROGRAM,      
            SORLCUR_TERM_CODE_ADMIT,  
            SORLCUR_STYP_CODE,  
            SORLCUR_RATE_CODE,    
            SORLFOS_DEPT_CODE   
    INTO      C_SEQNO,
            C_ALUM_NIVEL, 
            C_ALUM_CAMPUS, 
            C_ALUM_ESCUELA, 
            C_ALUM_GRADO, 
            C_ALUM_PROGRAMA, 
            C_ALUM_PERD_ADM,
            C_ALUM_TIPO_ALUM,
            C_ALUM_TARIFA,
            C_ALUM_DEPARTAMENTO
    FROM (
        SELECT SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
             SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
             SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
        FROM SORLCUR
        INNER JOIN SORLFOS ON
            SORLCUR_PIDM = SORLFOS_PIDM 
            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_PIDM_ALUMNO 
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y'
            AND SORLCUR_TERM_CODE_END IS NULL
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;

    -- GET CRONOGRAMA SECCIONC
    SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
        INTO P_PART_PERIODO
    FROM CZRPTRM 
    WHERE CZRPTRM_DEPT = C_ALUM_DEPARTAMENTO
    AND CZRPTRM_CAMP_CODE = C_ALUM_CAMPUS;

    SELECT CZRCAMP_CAMP_BDUCCI
        INTO P_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO P_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO P_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
      
    -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_IDSECCIONC
    WITH 
    CTE_tblSeccionC AS (
          -- GET SECCIONC
          SELECT  "IDSeccionC" IDSeccionC,
                  "FecInic" FecInic
          FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDsede" = P_APEC_CAMP
          AND "IDPerAcad" = P_APEC_TERM
          AND "IDEscuela" = C_ALUM_PROGRAMA
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
          AND "IDSeccionC" <> '15NEX1A'
          AND SUBSTRB("IDSeccionC",-2,2) IN (
              -- PARTE PERIODO           
              SELECT CZRPTRM_PTRM_BDUCCI
              FROM CZRPTRM 
              WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
          )
    ),
    CTE_tblPersonaAlumno AS (
        SELECT "IDAlumno" IDAlumno 
        FROM dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDPersona" IN ( 
            SELECT "IDPersona"
            FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDPersonaN" = P_PIDM_ALUMNO
        )
    )
    SELECT "IDAlumno", "IDSeccionC"
        INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno" IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
    AND "IDSede" = P_APEC_CAMP
    AND "IDPerAcad" = P_APEC_TERM
    AND "IDEscuela" = C_ALUM_PROGRAMA
    AND "IDSeccionC" IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
    AND "IDConcepto" = P_COD_DETALLE;
      
    -- GET MONTO 
    SELECT "Monto"
        INTO P_APEC_MOUNT
    FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDSede" = P_APEC_CAMP
    AND "IDPerAcad" = P_APEC_TERM
    AND "IDSeccionC" = P_APEC_IDSECCIONC
    AND "IDConcepto" = P_COD_DETALLE;
    
    -- UPDATE MONTO(s)
    UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        SET "Cargo" = "Cargo" + P_APEC_MOUNT
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno" = P_APEC_IDALUMNO
    AND "IDSede" = P_APEC_CAMP
    AND "IDPerAcad" = P_APEC_TERM
    AND "IDEscuela" = C_ALUM_PROGRAMA
    AND "IDSeccionC" = P_APEC_IDSECCIONC
    AND "IDConcepto" = P_COD_DETALLE;
        
    -- COMMIT; -- SP ANIDADO, "COMMIT" SE REALIZA DESDE EL SP PRINCIPAL.
EXCEPTION
WHEN OTHERS THEN
    P_ERROR := 'Error o inconsistencia detectada P_APEC_SET_DEUDA - '||SQLCODE||' -ERROR- '||SQLERRM;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_APEC_SET_DEUDA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_ASIG_RECONOCIDAS (
    P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_HTML1         OUT VARCHAR2,
    P_HTML2         OUT VARCHAR2,
    P_HTML3         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_GET_ASIG_RECONOCIDAS (P_PIDM, P_HTML1, P_HTML2, P_HTML3);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ASIG_RECONOCIDAS;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--                   
END BWZKSCPE;

/**********************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**********************************************************************************************/