create or replace PACKAGE BODY WFK_CONTICPE AS
/*******************************************************************************
 WFK_CONTICPE:
       Conti Package body SOLICITUD de CAMBIO de PLAN ESTUDIOS
*******************************************************************************/
-- FILE NAME..: WFK_CONTICPE.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTICPE
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/


PROCEDURE p_get_usuario_regacad (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_NOMBRE              OUT VARCHAR2,
      P_CORREO_REGACA       OUT VARCHAR2,
      P_USUARIO_REGACA      OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_get_usuario_regaca
  FECHA     : 29/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : en base a una sede y un rol, el procedimiento obtenga los datos del responsable de Registro académico de esa sede.

  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
    
      P_SECCION_EXCEPT          VARCHAR2(50);
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener el id usuario en la tabla que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
      SELECT USER_ID INTO P_USER_ID FROM WORKFLOW.ROLE_ASSIGNMENT 
      WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      P_SECCION_EXCEPT := '';
      
      -- Obtener Datos Usuario
      SELECT(First_Name || ' ' || Last_Name) names, Email_Address, Logon 
      INTO P_NOMBRE, P_CORREO_REGACA, P_USUARIO_REGACA
      FROM WORKFLOW.WFUSER 
      WHERE ID = P_USER_ID ;
      
      P_ERROR := 'Satisfactorio';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              -- --DBMS_OUTPUT.PUT_LINE ();
              P_ERROR := 'A ocurrido un problema, se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, se encontraron mas de una ORGANIZACIÒN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre del ROL: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, NO  se encontrò ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END p_get_usuario_regacad;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_PERIODO_VIGENTE (
        P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
        P_PERIODO             IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
        P_MODALIDAD_ALUMNO    IN SGBSTDN.SGBSTDN_DEPT_CODE%TYPE,
        P_PERIODO_VIGENTE     OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_periodo_vigente
  FECHA     : 28/10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es determinar si el estudiante se encuentra en el periodo vigente al momento 
              de realizar la solicitud para realizar las validaciones. 
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  
  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
      P_PERIODO_ACTV            SFBETRM.SFBETRM_TERM_CODE%TYPE; --------------- Periodo NRC del alumno
BEGIN 
--
      -- P_ID_ALUMNO : AUN NO ES NECESARIO ESTE DATO.

       /*
      --  1° SIGLA ....................... 2° SIGLA campus .........  3° bloque
          R Regular o Presencial            H hyo                       1 bloque o modulo
          W CPGT                            A aqp                       2 bloque o modulo
          V virtual                         C cuz                       ...
                                            V vir
      */
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END
      INTO P_PART_PERIODO
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
      
      
      -- GET PERIODO ACTIVO 
      SELECT SOBPTRM_TERM_CODE INTO P_PERIODO_ACTV
      FROM (
          -- GET COD PARTE-PERIODO activo segun fechas
          SELECT SOBPTRM_TERM_CODE FROM SOBPTRM
          WHERE  SOBPTRM_PTRM_CODE LIKE ('' || P_PART_PERIODO || '%')
          AND SYSDATE BETWEEN SOBPTRM_START_DATE AND SOBPTRM_END_DATE
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;
      
      -- SI EL ALUMNO SE ENCUENTRA EN EL PERIODO ACTIVO
      IF (P_PERIODO = P_PERIODO_ACTV) THEN
            P_PERIODO_VIGENTE := 'TRUE';
      ELSE
            P_PERIODO_VIGENTE := 'FALSE';
      END IF;
      
      --P_ERROR := '';
      
EXCEPTION
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_PERIODO_VIGENTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 12/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : cambia el estado de la solicitud (XXXXXX)

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  
  =================================================================================================================== */

AS
            P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
            P_ESTADO_PREVIOUS         SVVSRVS.SVVSRVS_CODE%TYPE;
            P_FECHA_SOL               SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
            P_INDICADOR               NUMBER;
            E_INVALID_ESTADO          EXCEPTION;
BEGIN
--
  ----------------------------------------------------------------------------
        
        -- GET tipo de solicitud por ejemplo "SOL003" 
        SELECT  SVRSVPR_SRVC_CODE, 
                SVRSVPR_SRVS_CODE,
                SVRSVPR_RECEPTION_DATE
        INTO    P_CODIGO_SOLICITUD,
                P_ESTADO_PREVIOUS,
                P_FECHA_SOL
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
        
        -- VALIDAR que :
            -- El ESTADO actual sea modificable segun la configuracion.
            -- El ETSADO enviado pertenesca entre los asignados al tipo de SOLICTUD.
        SELECT COUNT(*) INTO P_INDICADOR
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
        AND SVRSVPR_SRVS_CODE IN (
              -- Estados de solic. MODIFICABLES segun las reglas y configuracion
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
              AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        )
        AND P_ESTADO IN (
              -- Estados de solic. configurados
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        );
        
        
        IF P_INDICADOR = 0 THEN
              RAISE E_INVALID_ESTADO;
        ELSIF (P_ESTADO_PREVIOUS <> P_ESTADO) THEN
              -- UPDATE SOLICITUD
              UPDATE SVRSVPR
                SET SVRSVPR_SRVS_CODE = P_ESTADO,  --AN , AP , RE entre otros
                --SVRSVPR_ACCD_TRAN_NUMBER = P_TRAN_NUMBER_OLD,
                --SVRSVPR_BILLING_IND = 'N', -- CASE WHEN P_ESTADO = 'AP' THEN 'Y' ELSE 'N' END,
                SVRSVPR_DATA_ORIGIN = 'WorkFlow'
              WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
              AND SVRSVPR_SRVS_CODE IN (
                        -- Estados de solic. MODIFICABLES segun las reglas y configuracion
                        SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                        FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                        INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                        ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                        AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                        INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                        ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                        WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                        AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                        AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
                        AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
                )
              AND P_ESTADO IN (
                      -- Estados de solic. configurados
                      SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                      FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                      INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                      ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                      AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                      INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                      ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                      WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                      AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                      AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
              );
              
              COMMIT;
        END IF;
        
EXCEPTION
  WHEN E_INVALID_ESTADO THEN
          P_ERROR  := 'El "ESTADO" actual no es modificable ò no se encuentra entre los ESTADOS disponibles para la solicitud.';
  WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_ELIMINAR_ASIGNATURAS( 
      P_ID_ALUMNO               IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_ERROR                   OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_ELIMINAR_ASIGNATURAS
  FECHA     : 28/11/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Eliminar los NRC's.            

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
            P_KEY_SEQNO                 SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
            SAVE_ACT_DATE_OUT           VARCHAR2(100);
            RETURN_STATUS_IN_OUT        NUMBER;            
BEGIN
      
      -- #######################################################################          
      -- VALIDAR - "estimaciones IN-LINE"
      -- #######################################################################          
      

      -- #######################################################################          
      -- Eliminar registros de CRN'S
      -- SFASLST ::: Student Course Registration Repeating Table 
      DELETE FROM SFRSTCR 
      WHERE SFRSTCR_PIDM = P_ID_ALUMNO
      AND SFRSTCR_TERM_CODE = P_PERIODO; 
        
        
        --
        COMMIT;
EXCEPTION
    WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_ELIMINAR_ASIGNATURAS;    

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO_ALUMNO       OUT VARCHAR2
)
AS
      P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO P_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      
      -- GET COMENTARIO de la solicitud. (comentario PERSONALZIADO.)
      SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO 
      FROM (
            SELECT SVRSVAD_ADDL_DATA_DESC
            FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
            WHERE SVRSRAD_SRVC_CODE = P_CODIGO_SOLICITUD
            AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
            ORDER BY SVRSVAD_ADDL_DATA_SEQ DESC
      ) WHERE ROWNUM = 1;

END P_OBTENER_COMENTARIO_ALUMNO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--                   
END WFK_CONTICPE;