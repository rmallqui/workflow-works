-- SOATERM : PARTES periodos y Fechas de inicio y fin
SELECT * FROM SOBPTRM
WHERE SYSDATE BETWEEN SOBPTRM_START_DATE AND SOBPTRM_END_DATE;

-- STVPTRM : DETALLE DE PARTES periodos
SELECT * FROM STVPTRM;

-- SEDE
SELECT * FROM STVCAMP;            

-- DEPT
SELECT * FROM STVDEPT;

SELECT SGBSTDN_PIDM, SGBSTDN_TERM_CODE_EFF, SGBSTDN.* FROM SGBSTDN WHERE SGBSTDN_PIDM in (37037,179316);



------------------- RETURN TRUE  si el alumno esta en el periodo vigente.
SET SERVEROUTPUT ON
DECLARE
    P_ID_ALUMNO               SPRIDEN.SPRIDEN_PIDM%TYPE           := ;
    P_MODALIDAD_ALUMNO        SGBSTDN.SGBSTDN_DEPT_CODE%TYPE      := 'UREG'; -- Departamento
    P_COD_SEDE                STVCAMP.STVCAMP_CODE%TYPE           := 'S01';  -- SGBSTDN.SGBSTDN_CAMP_CODE%TYPE;
    P_PART_PERIODO            VARCHAR2(10);
    P_PERIODO                 SFBETRM.SFBETRM_TERM_CODE%TYPE      := '201620';
    P_PERIODO_ACTV            SFBETRM.SFBETRM_TERM_CODE%TYPE;
    P_PERIODO_VIGENTE         VARCHAR2(10);
BEGIN

    /*
    --  1° Digito ....................... 2° Digito campus .........  3° bloque
        R Regular o Presencial            H hyo                       1 bloque o modulo
        W CPGT                            A aqp                       2 bloque o modulo
        V virtual                         C cuz                       ...
                                          V vir
    */
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END
      INTO P_PART_PERIODO
      FROM STVCAMP,STVDEPT 
      --WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE
      WHERE STVDEPT_CODE = 'UREG' AND STVCAMP_CODE = 'S01';
      
      SELECT SOBPTRM_TERM_CODE INTO P_PERIODO_ACTV
      FROM (
          SELECT SOBPTRM_TERM_CODE FROM SOBPTRM
          WHERE  SOBPTRM_PTRM_CODE LIKE ('' || P_PART_PERIODO || '%')
          AND SYSDATE BETWEEN SOBPTRM_START_DATE AND SOBPTRM_END_DATE
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;
      
      IF (P_PERIODO = P_PERIODO_ACTV) THEN
            P_PERIODO_VIGENTE := 'TRUE';
      ELSE
            P_PERIODO_VIGENTE := 'FALSE';
      END IF;
      
      DBMS_OUTPUT.PUT_LINE(P_PERIODO_VIGENTE);
END;
