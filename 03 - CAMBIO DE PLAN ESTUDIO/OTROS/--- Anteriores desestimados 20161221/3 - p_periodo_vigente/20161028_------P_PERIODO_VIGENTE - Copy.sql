
------------------------------------------------------------------------------------------------------------------------------
DECLARE     P_PERIODO_VIGENTES         VARCHAR2(10);
            P_ERROR                   VARCHAR2(100);
BEGIN
      P_PERIODO_VIGENTE(123,'201620','S01','UREG',P_PERIODO_VIGENTES,P_ERROR);
      DBMS_OUTPUT.PUT_LINE(P_PERIODO_VIGENTES  '-----------------------------'  P_ERROR);
END;


CREATE OR REPLACE PROCEDURE P_PERIODO_VIGENTE (
        P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
        P_PERIODO             IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
        P_MODALIDAD_ALUMNO    IN SGBSTDN.SGBSTDN_DEPT_CODE%TYPE,
        P_PERIODO_VIGENTE     OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_periodo_vigente
  FECHA     : 28/10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es determinar si el estudiante se encuentra en el periodo vigente al momento 
              de realizar la solicitud para realizar las validaciones. 
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  
  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
      P_PERIODO_ACTV            SFBETRM.SFBETRM_TERM_CODE%TYPE; --------------- Periodo NRC del alumno
BEGIN 
--
      -- P_ID_ALUMNO : AUN NO ES NECESARIO ESTE DATO.

       /*
      --  1° SIGLA ....................... 2° SIGLA campus .........  3° bloque
          R Regular o Presencial            H hyo                       1 bloque o modulo
          W CPGT                            A aqp                       2 bloque o modulo
          V virtual                         C cuz                       ...
                                            V vir
      */
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END
      INTO P_PART_PERIODO
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
      
      
      -- GET PERIODO ACTIVO 
      SELECT SOBPTRM_TERM_CODE INTO P_PERIODO_ACTV
      FROM (
          -- GET COD PARTE-PERIODO activo segun fechas
          SELECT SOBPTRM_TERM_CODE FROM SOBPTRM
          WHERE  SOBPTRM_PTRM_CODE LIKE ('' || P_PART_PERIODO || '%')
          AND SYSDATE BETWEEN SOBPTRM_START_DATE AND SOBPTRM_END_DATE
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;
      
      -- SI EL ALUMNO SE ENCUENTRA EN EL PERIODO ACTIVO
      IF (P_PERIODO = P_PERIODO_ACTV) THEN
            P_PERIODO_VIGENTE := 'TRUE';
      ELSE
            P_PERIODO_VIGENTE := 'FALSE';
      END IF;
      
      --P_ERROR := '';
      
EXCEPTION
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_PERIODO_VIGENTE;