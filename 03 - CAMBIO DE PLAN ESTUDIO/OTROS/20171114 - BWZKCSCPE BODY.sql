create or replace PACKAGE BODY BWZKCSCPE AS
/*******************************************************************************
 BWZKCSCPE:
       Paquete Web _ Desarrollo Propio _  Paquete _ Conti Solicitud Cambio Plan Estudio
*******************************************************************************/
-- FILE NAME..: BWZKCSCPE.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSCPE
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

PROCEDURE P_GET_USUARIO (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO_REGACA       OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_get_usuario_regacad
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
      P_Email_Address           VARCHAR2(100);
    
      P_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO P_Email_Address FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_CORREO_REGACA := P_CORREO_REGACA || P_Email_Address || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      P_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_CORREO_REGACA,1,LENGTH(P_CORREO_REGACA) -1) INTO P_CORREO_REGACA
      FROM DUAL
      WHERE SUBSTR(P_CORREO_REGACA,-1,1) = ',';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := '- Se encontraron mas de una ORGANIZACIóN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := '- Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := '- NO se encontrò el nombre del ROL: '  || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := '- NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := '- NO  se encontrÓ ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_AN                  OUT NUMBER,
  P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 12/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : cambia el estado de la solicitud (XXXXXX)

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   10/03/2016    RMALLQUI      Se agrego el parametro P_AN paravalidar si la solicitud este ANULADO.
  002   27/04/2016    RMALLQUI      Se comentó el filtro de fechas para cambir el estado de la solicitud.
                                    Se modifico el param. P_ERROR en el EXCEPTION, de tal meanera que el WF detenga el flujo en cualquier caso de error.
  =================================================================================================================== */
AS
            P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
            P_ESTADO_PREVIOUS         SVVSRVS.SVVSRVS_CODE%TYPE;
            P_FECHA_SOL               SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
            P_INDICADOR               NUMBER;
            E_INVALID_ESTADO          EXCEPTION;
            V_PRIORIDAD               SVRSVPR.SVRSVPR_RSRV_SEQ_NO%TYPE;
BEGIN
--
  ----------------------------------------------------------------------------
        
        -- GET tipo de solicitud por ejemplo "SOL003" 
        SELECT  SVRSVPR_SRVC_CODE, 
                SVRSVPR_SRVS_CODE,
                SVRSVPR_RECEPTION_DATE,
                SVRSVPR_RSRV_SEQ_NO
        INTO    P_CODIGO_SOLICITUD,
                P_ESTADO_PREVIOUS,
                P_FECHA_SOL,
                V_PRIORIDAD
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
        
        -- SI EL ESTADO ES ANULADO (AN)
        P_AN := 0;
        IF P_ESTADO_PREVIOUS = 'AN' THEN
              P_AN := 1;
        END IF;
        
        -- VALIDAR que :
            -- El ESTADO actual sea modificable segun la configuracion.
            -- El ETSADO enviado pertenesca entre los asignados al tipo de SOLICTUD.
        SELECT COUNT(*) INTO P_INDICADOR
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        AND SVRSVPR_SRVS_CODE IN (
              -- Estados de solic. MODIFICABLES segun las reglas y configuracion
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        )
        AND P_ESTADO IN (
              -- Estados de solic. configurados
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        );
        
        
        IF P_INDICADOR = 0 THEN
              RAISE E_INVALID_ESTADO;
        ELSIF (P_ESTADO_PREVIOUS <> P_ESTADO) THEN
              -- UPDATE SOLICITUD
              UPDATE SVRSVPR
                SET SVRSVPR_SRVS_CODE = P_ESTADO,  --AN , AP , RE entre otros
                SVRSVPR_ACTIVITY_DATE = SYSDATE,
                SVRSVPR_DATA_ORIGIN = 'WorkFlow'
              WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
              AND SVRSVPR_SRVS_CODE IN (
                        -- Estados de solic. MODIFICABLES segun las reglas y configuracion
                        SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                        FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                        INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                        ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                        AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                        INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                        ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                        WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                        AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                        AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
                        AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                        -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
                )
              AND P_ESTADO IN (
                      -- Estados de solic. configurados
                      SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                      FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                      INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                      ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                      AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                      INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                      ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                      WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                      AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                      AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                      -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
              );
              
              COMMIT;
        END IF;
        
EXCEPTION
  WHEN E_INVALID_ESTADO THEN
          P_ERROR  := '- El "ESTADO" actual no es modificable ò no se encuentra entre los ESTADOS disponibles para la solicitud.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/*
PROCEDURE P_ELIMINAR_ASIGNATURAS
*/

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO          OUT VARCHAR2,
      P_TELEFONO            OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_COMENTARIO_ALUMNO
  FECHA     : --/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene EL COMENTARIO Y TELEFONO de una solicitud especifica. 
              Se establecio que el penultimo dato es el COMENTARIO y ultimo el TELEFONO.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS

      V_ERROR                   EXCEPTION;
      V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_COMENTARIO              VARCHAR2(4000);
      V_CLAVE                   VARCHAR2(4000);
      V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
      
      -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
      CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
        --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;

BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO V_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 1 THEN
                  -- OBTENER COMENTARIO DE LA SOLICITUD
                  P_COMENTARIO := V_COMENTARIO;
              ELSIF V_ADDL_DATA_SEQ = 2 THEN
                  -- OBTENER EL TELEFONO DE LA SOLICITUD 
                  P_TELEFONO := V_COMENTARIO;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;

      -- CASO ESPECIAL ******
      IF P_FOLIO_SOLICITUD = 4410 OR P_FOLIO_SOLICITUD = 4405 THEN 
            P_TELEFONO := '-';
      END IF;

      IF (P_TELEFONO IS NULL OR P_COMENTARIO IS NULL) THEN
          RAISE V_ERROR;
      END IF;
      
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_OBTENER_COMENTARIO_ALUMNO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/*
PROCEDURE P_OBTENER_PRIORIDAD 
*/

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/*
PROCEDURE P_GET_SIDEUDA_ALUMNO 
*/

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/*
PROCEDURE P_ELIMINAR_RECARGO 
*/

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_MODIFICAR_CATALOGO (
        P_PIDM_ALUMNO        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_PERIODO          IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_ERROR            OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_MODIFICAR_CATALOGO
  FECHA     : 19/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Modifica EL PERIODO CATALOGO y vuelve a estimar las deudas actualziando su CTA CORRIENTE.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      P_DNI_ALUMNO              SPRIDEN.SPRIDEN_ID%TYPE;

          /*----------------------------------------------------------------------------------
        -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
              - Nuevo Registro como Evidencia que alumno realizó TRAMITE
        */
      CURSOR C_SGRSTSP IS
      SELECT * FROM (
          --Ultimo registro cercano al P_TERM_CODE
          SELECT *  FROM SGRSTSP 
          WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
          AND SGRSTSP_TERM_CODE_EFF <= P_PERIODO
          ORDER BY SGRSTSP_KEY_SEQNO DESC
      ) WHERE ROWNUM = 1;
      
      -- ADD SGBSTDN - EN CASO NO EXISTA PARA EL PERIODO INDICADO
      CURSOR C_SGBSTDN IS
      SELECT * FROM (
          SELECT *
          FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
          ORDER BY SGBSTDN_TERM_CODE_EFF DESC
       )WHERE ROWNUM = 1
       AND NOT EXISTS (
          SELECT *
          FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
          AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
       );
      
      -- AGREGAR NUEVO REGISTRO
      CURSOR C_SORLCUR IS
      SELECT * FROM (
          SELECT * FROM SORLCUR 
          WHERE SORLCUR_PIDM = P_PIDM_ALUMNO
          AND SORLCUR_LMOD_CODE = 'LEARNER'
          AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      )WHERE ROWNUM = 1;
      
      V_SGRSTSP_KEY_SEQNO     SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
      V_SGBSTDN_REC           SGBSTDN%ROWTYPE;
      V_SORLCUR_REC           SORLCUR%ROWTYPE;
      V_SORLFOS_REC           SORLFOS%ROWTYPE;
      V_SGRSTSP_REC           SGRSTSP%ROWTYPE;
      P_SORLCUR_SEQNO_INC     SORLCUR.SORLCUR_SEQNO%TYPE;
      P_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE;
      P_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE;
      P_SORLCUR_RATE_CODE     SORLCUR.SORLCUR_RATE_CODE%TYPE; -- TARIFA
      
      SAVE_ACT_DATE_OUT       VARCHAR2(100);
      RETURN_STATUS_IN_OUT    NUMBER;
BEGIN 
      
      -- GET DNI alumno
      SELECT SPRIDEN_ID INTO P_DNI_ALUMNO FROM SPRIDEN WHERE SPRIDEN_PIDM = P_PIDM_ALUMNO AND SPRIDEN_CHANGE_IND IS NULL;

      /* #######################################################################
          -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
              - Nuevo Registro como Evidencia que alumno realizó TRAMITE
          */
          OPEN C_SGRSTSP;
          LOOP
              FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
              EXIT WHEN C_SGRSTSP%NOTFOUND;
              
              -- GET SGRSTSP_KEY_SEQNO NUEVO
              SELECT SGRSTSP_KEY_SEQNO INTO V_SGRSTSP_KEY_SEQNO FROM (
                  SELECT SGRSTSP_KEY_SEQNO  FROM SGRSTSP 
                  WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
                  ORDER BY SGRSTSP_KEY_SEQNO DESC
              ) WHERE ROWNUM = 1;

              
              INSERT INTO SGRSTSP (
                      SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                      SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                      SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                      SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                      SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                      SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                      SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                      SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                      SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
              SELECT 
                      V_SGRSTSP_REC.SGRSTSP_PIDM,       P_PERIODO,                          V_SGRSTSP_KEY_SEQNO,
                      'AS',                             SYSDATE,                              'WorkFlow',
                      USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                      V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                      V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                      V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                      V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
              FROM DUAL;              
          END LOOP;
          CLOSE C_SGRSTSP;

      -- #######################################################################
      -- INSERT  ------> SGBSTDN -
      OPEN C_SGBSTDN;
      LOOP
          FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
          EXIT WHEN C_SGBSTDN%NOTFOUND;
          
            INSERT INTO SGBSTDN (
                      SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                      SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                      SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                      SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                      SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                      SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                      SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                      SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                      SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                      SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                      SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                      SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                      SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                      SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                      SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                      SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                      SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                      SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                      SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                      SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                      SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                      SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                      SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                      SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                      SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                      SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                      SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                      SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                      SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                      SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                      SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                      SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                      SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                      SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                      SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                      SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                      SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
            VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_PERIODO,                                    V_SGBSTDN_REC.SGBSTDN_STST_CODE,
                      V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            V_SGBSTDN_REC.SGBSTDN_STYP_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                      V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                      V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                      V_SGBSTDN_REC.SGBSTDN_COLL_CODE_1,          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_1,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                      V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                      V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                      V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                      V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                      SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                      V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                      V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                      V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                      V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                      V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                      V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                      V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                      V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        V_SGBSTDN_REC.SGBSTDN_PROGRAM_1,              P_PERIODO,
                      V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                      V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                      V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                      V_SGBSTDN_REC.SGBSTDN_CURR_RULE_1,          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_1,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                      V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                      V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                      V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                      V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                      V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                      V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                      V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                      V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                      USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
            
      END LOOP;
      CLOSE C_SGBSTDN;
      
      
      UPDATE SGBSTDN
      SET SGBSTDN_TERM_CODE_CTLG_1 = P_PERIODO
      WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
      AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
      AND SGBSTDN_TERM_CODE_CTLG_1 <> P_PERIODO;
      
      
      -- #######################################################################
      -- INSERT  ------> SORLCUR -
      OPEN C_SORLCUR;
      LOOP
          FETCH C_SORLCUR INTO V_SORLCUR_REC;
          EXIT WHEN C_SORLCUR%NOTFOUND;
          
          -- GET SORLCUR_SEQNO NUEVO 
          --SELECT MAX(SORLCUR_SEQNO) + 1 INTO P_SORLCUR_SEQNO_NEW FROM SORLCUR
          --WHERE SORLCUR_PIDM = P_PIDM_ALUMNO;
          
          -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
              SELECT MAX(SORLCUR_SEQNO + 1) INTO P_SORLCUR_SEQNO_INC  FROM SORLCUR WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
              
              -- GET SORLCUR_SEQNO NUEVO 
              SELECT MAX(SORLCUR_SEQNO + 2) INTO P_SORLCUR_SEQNO_NEW  FROM SORLCUR WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;

          -- GET SORLCUR_SEQNO ANTERIOR 
          P_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;

          -- INACTIVE --- SORLCUR - (1 DE 2)
          INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             P_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          V_SORLCUR_REC.SORLCUR_TERM_CODE,        V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                          V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                          P_PERIODO,                              V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          NULL /*SORLCUR_CURRENT_CDE*/ );
          
          -- ACTIVE --- SORLCUR - (2 DE 2)

          INSERT INTO SORLCUR (
                      SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                      SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                      SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                      SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                      SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                      SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                      SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                      SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                      SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                      SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                      SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                      SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                      SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                      SORLCUR_CURRENT_CDE ) 
          VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             P_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                      P_PERIODO,                              V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                      V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                      'WorkFlow',                             SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                      V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            P_PERIODO,
                      NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                      V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                      V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                      V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                      V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                      V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                      V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                      USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                      V_SORLCUR_REC.SORLCUR_CURRENT_CDE );

          
          -- UPDATE TERM_CODE_END(vigencia curriculum) 
          UPDATE SORLCUR 
          SET   SORLCUR_TERM_CODE_END = P_PERIODO, 
                SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
          WHERE   SORLCUR_PIDM = P_PIDM_ALUMNO 
          AND     SORLCUR_LMOD_CODE = 'LEARNER'
          AND     SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
          
          -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
          UPDATE SORLCUR 
          SET   SORLCUR_CURRENT_CDE = NULL
          WHERE   SORLCUR_PIDM = P_PIDM_ALUMNO 
          AND     SORLCUR_LMOD_CODE = 'LEARNER'
          AND     SORLCUR_TERM_CODE_END = P_PERIODO
          AND     SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
          
      END LOOP;
      CLOSE C_SORLCUR;

      
      -- #######################################################################
      -- INSERT --- SORLFOS -
      INSERT INTO SORLFOS (
            SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
            SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
            SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
            SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
            SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
            SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
            SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
            SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
            SORLFOS_CURRENT_CDE) 
      SELECT 
            V_SORLFOS_REC.SORLFOS_PIDM,             P_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
            V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_PERIODO,                                V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
            V_SORLFOS_REC.SORLFOS_CSTS_CODE,        V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
            USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
            P_PERIODO,                              V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
            V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
            V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
            V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
            'Y'
      FROM SORLFOS V_SORLFOS_REC
      WHERE SORLFOS_PIDM = P_PIDM_ALUMNO
      AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD;

       
      -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo)
      UPDATE SORLFOS 
      SET   SORLFOS_CURRENT_CDE = NULL
      WHERE   SORLFOS_PIDM = P_PIDM_ALUMNO
      AND     SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD
      AND     SORLFOS_TERM_CODE = P_PERIODO
      AND     SORLFOS_CSTS_CODE = 'INPROGRESS';
      
      
       /*
      -- NO ES NECESARIO LA ESTIMACION DE CUOTA EN BANNER Y APEC, YA QUE NO ACTUALIZA SUS PAGOS.

      -- #######################################################################
      -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
      SFKFEES.p_processfeeassessment (  P_PERIODO,
                                        P_ID_ALUMNO,
                                        SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                        SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                        'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                        'Y',          -- create TBRACCD records
                                        'SFAREGS',    -- where assessment originated from
                                        'Y',          -- commit changes
                                        SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                        'N',          -- do not ignore SFRFMAX rules
                                        RETURN_STATUS_IN_OUT );   -- OUT -- return_status
      ------------------------------------------------------------------------------  
    
      -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
      TZJAPOL.p_run_proc_tvrappl(P_DNI_ALUMNO);
    
      ------------------------------------------------------------------------------                                    
      */                                   
        
      COMMIT;
      --
EXCEPTION
WHEN OTHERS THEN
      P_ERROR := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_CATALOGO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_MODIFICAR_RETENCION (
      P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_ERROR               OUT VARCHAR2
) 
/* ===================================================================================================================
  NOMBRE    : P_MODIFICAR_RETENCION
  FECHA     : 22/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Modifica la fecha limite de una retencion a un dia antes a la fecha actual.

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION  
  001     14/06/2017    RMALLQUI      Se AGREGO un codigo adicional "P_STVHLDD_CODE2" y la modificacion de la fecha de retencion sera para un
                                      dia antes de a la fecha de (AP)aprovacion la solicitud.
  002     26/07/2017    RMALLQUI      Se AGREGO un codigo adicional "P_STVHLDD_CODE3"
  =================================================================================================================== */
AS
      
      V_INDICADOR               NUMBER;
      V_STATUS_DATE             SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
      V_INVALID_COD_RETENCION   EXCEPTION;
      
BEGIN
      
      -- VALIDAR si EXISTE codigo del TIPO DE RETENCION - STVHLDD
      SELECT COUNT(*) INTO V_INDICADOR 
      FROM STVHLDD
      WHERE STVHLDD_CODE IN ( P_STVHLDD_CODE1,P_STVHLDD_CODE2,P_STVHLDD_CODE3 );
      
      
      -- GET FECHA de estado APROVADO DE LA SOLICITUD
      SELECT  SVRSVPR_STATUS_DATE
      INTO    V_STATUS_DATE
      FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
      AND SVRSVPR_PIDM = P_PIDM
      AND SVRSVPR_SRVS_CODE = 'AP'; -- ESTADO APROVADO
      
      
      IF ( V_INDICADOR < 3 ) THEN
      
            RAISE V_INVALID_COD_RETENCION;
      
      ELSE
            -- INSERTAR REGISTRO DE FECHA DE RETENCION DOCUMENTOS - SOAHOLD      
            UPDATE SPRHOLD
            SET   
                  --SPRHOLD_FROM_DATE = SYSDATE + 1,
                  SPRHOLD_TO_DATE = V_STATUS_DATE - 1,
                  SPRHOLD_ACTIVITY_DATE = SYSDATE
            WHERE SPRHOLD_PIDM = P_PIDM
            AND SPRHOLD_TO_DATE > V_STATUS_DATE
            AND SPRHOLD_HLDD_CODE IN (P_STVHLDD_CODE1,P_STVHLDD_CODE2,P_STVHLDD_CODE3);
            
            COMMIT;
      END IF;

EXCEPTION
  WHEN V_INVALID_COD_RETENCION THEN
          P_ERROR  := '- El "CÓDIGO DE RETENCION" enviado es inválido.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_RETENCION;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/*
PROCEDURE P_VALIDAR_FECHA_SOLICITUD 
*/

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_SET_ATRIBUTO_PLANS (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,      -------- COD atributos
      P_ERROR             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_ATRIBUTO_PLANS
  FECHA     : 14/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : SET ATRIBUTO de PLAN ESTUDIOS a un periodo determinado.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    
      V_TERM_LAST_CODE      STVTERM.STVTERM_CODE%TYPE;
      V_INDICADOR           NUMBER;
      V_ATTS_CURRENT        STVATTS.STVATTS_CODE%TYPE;
      P_ROWID               GB_COMMON.INTERNAL_RECORD_ID_TYPE;
      
      -- GET el ultimo TERM(Periodo) de los atributos del estudiante - forma SGASADD
      CURSOR C_SGRSATT_TERM IS
      SELECT SGRSATT_TERM_CODE_EFF
      FROM (
          SELECT SGRSATT_TERM_CODE_EFF FROM SGRSATT 
          WHERE SGRSATT_PIDM = P_PIDM
          AND SGRSATT_TERM_CODE_EFF <= P_TERM_CODE
          ORDER BY SGRSATT_TERM_CODE_EFF DESC
      ) WHERE ROWNUM = 1;
      
      V_TERM                STVTERM.STVTERM_CODE%TYPE;
      V_ATTS_CODE           STVATTS.STVATTS_CODE%TYPE;
      
      -- CREAR - ACTUALZIAR ATRIBUTO DE PLAN ESTUDIOS
      CURSOR C_SGRSATT_SET IS
      SELECT  SGRSATT_TERM_CODE_EFF,
              SGRSATT_ATTS_CODE
        FROM SGRSATT 
        WHERE SGRSATT_PIDM =  P_PIDM
        AND SGRSATT_TERM_CODE_EFF = V_TERM_LAST_CODE
        AND P_ATTS_CODE <> V_ATTS_CURRENT;
        
BEGIN  
      
      -- GET el ultimo TERM(Periodo) de los atributos del estudiante - forma SGASADD
      OPEN C_SGRSATT_TERM;
        LOOP
          FETCH C_SGRSATT_TERM INTO V_TERM_LAST_CODE;
          EXIT WHEN C_SGRSATT_TERM%NOTFOUND;
        END LOOP;
      CLOSE C_SGRSATT_TERM;
      
      
      -- #######################################################################
      -- SET ATRIBUTO alumno - SGASADD
      IF V_TERM_LAST_CODE IS NULL THEN
            
          -- CASO QUE NO EXISTA NINGUN REGISTRO DE ATRIBUTO
          INSERT INTO SGRSATT
            (
              SGRSATT_PIDM, 
              SGRSATT_TERM_CODE_EFF,
              SGRSATT_ATTS_CODE,
              SGRSATT_ACTIVITY_DATE,
              SGRSATT_STSP_KEY_SEQUENCE
            )
          SELECT P_PIDM, P_TERM_CODE, P_ATTS_CODE, SYSDATE, NULL
          FROM DUAL
          WHERE NOT EXISTS (SELECT * FROM SGRSATT WHERE SGRSATT_PIDM = P_PIDM);
      
      ELSE
            
          -- GET ATRIBUTO de PLAN ESTUDIO mas cercano al periodo requerido - forma SGASADD
          SELECT SGRSATT_ATTS_CODE INTO V_ATTS_CURRENT 
          FROM SGRSATT 
          WHERE SGRSATT_PIDM = P_PIDM
            AND SGRSATT_TERM_CODE_EFF = V_TERM_LAST_CODE
            AND SGRSATT_ATTS_CODE IN ( 
                    SELECT STVATTS_CODE FROM STVATTS WHERE STVATTS_CODE LIKE ('P0%')
            );
      
          -- CREAR - ACTUALZIAR ATRIBUTO DE PLAN ESTUDIOS
          OPEN C_SGRSATT_SET;
            LOOP
              FETCH C_SGRSATT_SET INTO V_TERM, V_ATTS_CODE;
              IF C_SGRSATT_SET%FOUND THEN
                  IF V_TERM <> P_TERM_CODE THEN
                      
                      INSERT INTO SGRSATT
                      ( SGRSATT_PIDM, SGRSATT_TERM_CODE_EFF, SGRSATT_ATTS_CODE, SGRSATT_ACTIVITY_DATE, SGRSATT_STSP_KEY_SEQUENCE )
                      SELECT P_PIDM, P_TERM_CODE, CASE WHEN V_ATTS_CODE = V_ATTS_CURRENT THEN P_ATTS_CODE ELSE V_ATTS_CODE END, SYSDATE, NULL  
                      FROM DUAL;
                    
                  ELSE
                  
                      UPDATE SGRSATT SET SGRSATT_ATTS_CODE = P_ATTS_CODE
                      WHERE SGRSATT_PIDM = P_PIDM
                      AND SGRSATT_TERM_CODE_EFF = P_TERM_CODE
                      AND SGRSATT_ATTS_CODE = V_ATTS_CURRENT;
                      
                      EXIT;
                      
                  END IF;
              ELSE EXIT;
            END IF;
            END LOOP;
          CLOSE C_SGRSATT_SET;

      END IF;
      
      COMMIT;
      
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_ATRIBUTO_PLANS;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_SET_CURSO_INTROD (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CURSO_INTROD
  FECHA     : 02/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asigna los cursos introducctorios de RM y RV con nota 11 solo en caso no los tubiera alguno.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  001   16/10/2017    rmallqui    Se agrega el curso introductorio de codigo "PR" para UPGT y UVIR
  =================================================================================================================== */
AS
    
    V_STATUS_DATE_AP      SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
    V_CURSO_INTROD        VARCHAR2(2);

    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORTEST_IN IS
      SELECT 'RM' FROM DUAL WHERE P_DEPT_CODE = 'UREG' UNION 
      SELECT 'RV' FROM DUAL WHERE P_DEPT_CODE = 'UREG' UNION
      SELECT 'PR' FROM DUAL WHERE P_DEPT_CODE = 'UPGT' OR  P_DEPT_CODE = 'UVIR';

    
BEGIN
---
    -- Get la fecha de aprovación de la solicitud
    SELECT SVRSVPR_STATUS_DATE INTO  V_STATUS_DATE_AP
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD 
    AND SVRSVPR_SRVS_CODE = 'AP';
    

    OPEN C_SORTEST_IN;
    LOOP
        FETCH C_SORTEST_IN INTO V_CURSO_INTROD;
        EXIT WHEN C_SORTEST_IN%NOTFOUND;
          ----------------------------------------------------------------------------
          -- INSERT curso introductorio 
          INSERT INTO SORTEST ( 
                SORTEST_PIDM,         SORTEST_TESC_CODE,      SORTEST_TEST_DATE,
                SORTEST_TEST_SCORE,   SORTEST_ACTIVITY_DATE,  SORTEST_TERM_CODE_ENTRY,
                SORTEST_RELEASE_IND,  SORTEST_EQUIV_IND,      SORTEST_USER_ID,
                SORTEST_DATA_ORIGIN   ) 
          SELECT 
                P_PIDM,               V_CURSO_INTROD,         V_STATUS_DATE_AP,
                '11.00',              SYSDATE,                P_TERM_CODE,
                'N',                  'N',                    USER,
                'WorkFlow'
          FROM DUAL
          WHERE NOT EXISTS (
              SELECT SORTEST_PIDM FROM SORTEST 
              WHERE SORTEST_PIDM = P_PIDM AND SORTEST_TESC_CODE = V_CURSO_INTROD
              AND SORTEST_TEST_SCORE >= '10.5'
          );
          ----------------------------------------------------------------------------
    END LOOP;
    CLOSE C_SORTEST_IN;
    
    COMMIT;
--
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CURSO_INTROD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CPE_EXECCAPP (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_CPE_EXECCAPP
  FECHA     : 14/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Ejecución de CAPP.
  =================================================================================================================== */
AS
    
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    V_PROGRAMA        SORLCUR.SORLCUR_PROGRAM%TYPE;
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--  
      SELECT    SORLCUR_PROGRAM
      INTO      V_PROGRAMA
      FROM (
              SELECT    SORLCUR_PROGRAM
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
                  ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM <= 1;
    

    SZKECAP.P_EXEC_CAPP(P_PIDM,V_PROGRAMA,P_TERM_CODE,LV_OUT);
    IF(LV_OUT <> '0')THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
  WHEN P_EXCEPTION THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CPE_EXECCAPP;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CPE_EXECPROY (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_CPE_EXECPROY
  FECHA     : 14/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Ejecución de PROYECCION.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    V_PROGRAMA        SORLCUR.SORLCUR_PROGRAM%TYPE;
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SELECT    SORLCUR_PROGRAM
    INTO      V_PROGRAMA
    FROM (
            SELECT    SORLCUR_PROGRAM
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM        =   P_PIDM 
                AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                AND SORLCUR_CURRENT_CDE =   'Y'
                ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;

    SZKECAP.P_EXEC_PROY(P_PIDM,V_PROGRAMA,P_TERM_CODE,LV_OUT);
    IF(LV_OUT <> '0')THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
  WHEN P_EXCEPTION THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CPE_EXECPROY;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

-----------------------------------  SP ANIDADO -----------------------------------
PROCEDURE P_APEC_SET_DEUDA ( 
        P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_COD_DETALLE           IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE,
        P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_APEC_SET_DEUDA
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE --- APEC.

            : COMMIT - SP ANIDADO, El "COMMIT" se realiza desde el SP principal.
            
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;

        P_SERVICE               VARCHAR2(10);
        P_PART_PERIODO          VARCHAR2(9);
        P_APEC_CAMP             VARCHAR2(9);
        P_APEC_DEPT             VARCHAR2(9);
        P_APEC_TERM             VARCHAR2(10);
        P_APEC_IDSECCIONC       VARCHAR2(15);
        P_APEC_FECINIC          DATE;
        P_APEC_MOUNT            NUMBER;
        P_APEC_IDALUMNO         VARCHAR2(10);
        
BEGIN

      -- #######################################################################
      -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
      SELECT    SORLCUR_SEQNO,      
                SORLCUR_LEVL_CODE,    
                SORLCUR_CAMP_CODE,        
                SORLCUR_COLL_CODE,
                SORLCUR_DEGC_CODE,  
                SORLCUR_PROGRAM,      
                SORLCUR_TERM_CODE_ADMIT,  
                SORLCUR_STYP_CODE,  
                SORLCUR_RATE_CODE,    
                SORLFOS_DEPT_CODE   
      INTO      C_SEQNO,
                C_ALUM_NIVEL, 
                C_ALUM_CAMPUS, 
                C_ALUM_ESCUELA, 
                C_ALUM_GRADO, 
                C_ALUM_PROGRAMA, 
                C_ALUM_PERD_ADM,
                C_ALUM_TIPO_ALUM,
                C_ALUM_TARIFA,
                C_ALUM_DEPARTAMENTO
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                        SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                        SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM <= 1;
    

      -- GET CRONOGRAMA SECCIONC
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END
      INTO P_PART_PERIODO
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
      
      SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
      SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
      SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
      
      -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_IDSECCIONC
      WITH 
          CTE_tblSeccionC AS (
                  -- GET SECCIONC
                  SELECT  "IDSeccionC" IDSeccionC,
                          "FecInic" FecInic
                  FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                  WHERE "IDDependencia"='UCCI'
                  AND "IDsede"    = P_APEC_CAMP
                  AND "IDPerAcad" = P_APEC_TERM
                  AND "IDEscuela" = C_ALUM_PROGRAMA
                  AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                  AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                  AND "IDSeccionC" <> '15NEX1A'
                  AND SUBSTRB("IDSeccionC",-2,2) IN (
                      -- PARTE PERIODO           
                      SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                  )
          ),
          CTE_tblPersonaAlumno AS (
                  SELECT "IDAlumno" IDAlumno 
                  FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                  WHERE "IDPersona" IN ( 
                      SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                  )
          )
      SELECT "IDAlumno", "IDSeccionC" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
      FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
      WHERE "IDDependencia" = 'UCCI'
      AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
      AND "IDSede"      = P_APEC_CAMP
      AND "IDPerAcad"   = P_APEC_TERM
      AND "IDEscuela"   = C_ALUM_PROGRAMA
      AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
      AND "IDConcepto"  = P_COD_DETALLE;
      
      -- GET MONTO 
      SELECT "Monto" INTO P_APEC_MOUNT
      FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE 
      WHERE "IDDependencia" = 'UCCI'
      AND "IDSede"      = P_APEC_CAMP
      AND "IDPerAcad"   = P_APEC_TERM
      AND "IDSeccionC"  = P_APEC_IDSECCIONC
      AND "IDConcepto"  = P_COD_DETALLE;
      
      -- UPDATE MONTO(s)
      UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
      SET "Cargo" = "Cargo" + P_APEC_MOUNT
      WHERE "IDDependencia" = 'UCCI'
      AND "IDAlumno"    = P_APEC_IDALUMNO
      AND "IDSede"      = P_APEC_CAMP
      AND "IDPerAcad"   = P_APEC_TERM
      AND "IDEscuela"   = C_ALUM_PROGRAMA
      AND "IDSeccionC"  = P_APEC_IDSECCIONC
      AND "IDConcepto"  = P_COD_DETALLE;
            
      -- COMMIT; -- SP ANIDADO, "COMMIT" SE REALIZA DESDE EL SP PRINCIPAL.
EXCEPTION
  WHEN OTHERS THEN
        P_ERROR := 'Error o inconsistencia detectada P_APEC_SET_DEUDA - '||SQLCODE||' -ERROR- '||SQLERRM;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_APEC_SET_DEUDA;


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--                   
END BWZKCSCPE;