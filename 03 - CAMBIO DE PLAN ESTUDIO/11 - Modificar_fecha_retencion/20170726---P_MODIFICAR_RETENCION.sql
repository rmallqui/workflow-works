/*
SET SERVEROUTPUT ON
DECLARE   P_ERROR               VARCHAR2(200);
BEGIN
    P_MODIFICAR_RETENCION(928,----,'07','08','09',P_ERROR);
    DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' ---- ' || P_ERROR );
END;
*/

CREATE OR REPLACE PROCEDURE P_MODIFICAR_RETENCION (
      P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_ERROR               OUT VARCHAR2
) 
/* ===================================================================================================================
  NOMBRE    : P_MODIFICAR_RETENCION
  FECHA     : 22/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Modifica la fecha limite de una retencion a un dia antes a la fecha actual.

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION  
  001     14/06/2017    RMALLQUI      Se AGREGO un codigo adicional "P_STVHLDD_CODE2" y la modificacion de la fecha de retencion sera para un
                                      dia antes de a la fecha de (AP)aprovacion la solicitud.
  002     26/07/2017    RMALLQUI      Se AGREGO un codigo adicional "P_STVHLDD_CODE3"
  =================================================================================================================== */
AS
      
      V_INDICADOR               NUMBER;
      V_STATUS_DATE             SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
      V_INVALID_COD_RETENCION   EXCEPTION;
      
BEGIN
      
      -- VALIDAR si EXISTE codigo del TIPO DE RETENCION - STVHLDD
      SELECT COUNT(*) INTO V_INDICADOR 
      FROM STVHLDD
      WHERE STVHLDD_CODE IN ( P_STVHLDD_CODE1,P_STVHLDD_CODE2,P_STVHLDD_CODE3 );
      
      
      -- GET FECHA de estado APROVADO DE LA SOLICITUD
      SELECT  SVRSVPR_STATUS_DATE
      INTO    V_STATUS_DATE
      FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
      AND SVRSVPR_PIDM = P_PIDM
      AND SVRSVPR_SRVS_CODE = 'AP'; -- ESTADO APROVADO
      
      
      IF ( V_INDICADOR < 3 ) THEN
      
            RAISE V_INVALID_COD_RETENCION;
      
      ELSE
            -- INSERTAR REGISTRO DE FECHA DE RETENCION DOCUMENTOS - SOAHOLD      
            UPDATE SPRHOLD
            SET   
                  --SPRHOLD_FROM_DATE = SYSDATE + 1,
                  SPRHOLD_TO_DATE = V_STATUS_DATE - 1,
                  SPRHOLD_ACTIVITY_DATE = SYSDATE
            WHERE SPRHOLD_PIDM = P_PIDM
            AND SPRHOLD_TO_DATE > V_STATUS_DATE
            AND SPRHOLD_HLDD_CODE IN (P_STVHLDD_CODE1,P_STVHLDD_CODE2,P_STVHLDD_CODE3);
            
            COMMIT;
      END IF;

EXCEPTION
  WHEN V_INVALID_COD_RETENCION THEN
          P_ERROR  := '- El "CÓDIGO DE RETENCION" enviado es inválido.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_RETENCION;