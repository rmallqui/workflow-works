/*
SET SERVEROUTPUT ON
DECLARE   P_MESSAGE        VARCHAR2(4000);
BEGIN    
    P_SET_CURSO_INTROD(,'201720',,P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

create or replace PROCEDURE P_SET_CURSO_INTROD (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CURSO_INTROD
  FECHA     : 02/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asigna los cursos introducctorios de RM y RV con nota 11 solo en caso no los tubiera alguno.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  001   16/10/2017    rmallqui    Se agrega el curso introductorio de codigo "PR" para UPGT y UVIR
  =================================================================================================================== */
AS
    
    V_STATUS_DATE_AP      SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
    V_CURSO_INTROD        VARCHAR2(2);

    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORTEST_IN IS
      SELECT 'RM' FROM DUAL WHERE P_DEPT_CODE = 'UREG' UNION 
      SELECT 'RV' FROM DUAL WHERE P_DEPT_CODE = 'UREG' UNION
      SELECT 'PR' FROM DUAL WHERE P_DEPT_CODE = 'UPGT' OR  P_DEPT_CODE = 'UVIR';

    
BEGIN
---
    -- Get la fecha de aprovación de la solicitud
    SELECT SVRSVPR_STATUS_DATE INTO  V_STATUS_DATE_AP
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD 
    AND SVRSVPR_SRVS_CODE = 'AP';
    

    OPEN C_SORTEST_IN;
    LOOP
        FETCH C_SORTEST_IN INTO V_CURSO_INTROD;
        EXIT WHEN C_SORTEST_IN%NOTFOUND;
          ----------------------------------------------------------------------------
          -- INSERT curso introductorio 
          INSERT INTO SORTEST ( 
                SORTEST_PIDM,         SORTEST_TESC_CODE,      SORTEST_TEST_DATE,
                SORTEST_TEST_SCORE,   SORTEST_ACTIVITY_DATE,  SORTEST_TERM_CODE_ENTRY,
                SORTEST_RELEASE_IND,  SORTEST_EQUIV_IND,      SORTEST_USER_ID,
                SORTEST_DATA_ORIGIN   ) 
          SELECT 
                P_PIDM,               V_CURSO_INTROD,         V_STATUS_DATE_AP,
                '11.00',              SYSDATE,                P_TERM_CODE,
                'N',                  'N',                    USER,
                'WorkFlow'
          FROM DUAL
          WHERE NOT EXISTS (
              SELECT SORTEST_PIDM FROM SORTEST 
              WHERE SORTEST_PIDM = P_PIDM AND SORTEST_TESC_CODE = V_CURSO_INTROD
              AND SORTEST_TEST_SCORE >= '10.5'
          );
          ----------------------------------------------------------------------------
    END LOOP;
    CLOSE C_SORTEST_IN;
    
    COMMIT;
--
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CURSO_INTROD;
