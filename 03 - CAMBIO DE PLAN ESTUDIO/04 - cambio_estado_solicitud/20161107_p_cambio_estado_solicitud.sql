/*
drop procedure p_cambio_estado_solicitud;
GRANT EXECUTE ON p_cambio_estado_solicitud TO wfobjects;
GRANT EXECUTE ON p_cambio_estado_solicitud TO wfauto;

set serveroutput on
DECLARE p_error varchar2(64);
begin
  WFK_CONTICPE.p_cambio_estado_solicitud('100','CO',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;

*/



CREATE OR REPLACE PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_ERROR               OUT VARCHAR2
  )
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 07/11/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : finaliza la solicitud (XXXXXX) presentada ya sea por su atención o vencimiento del tiempo de solicitud

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   20/10/2016    rmallquil     Validar las los estados disponibles a MODIFICACION DE ESTADO.
  =================================================================================================================== */

AS
       P_CODIGO_SOLICITUD          SVVSRVC.SVVSRVC_CODE%TYPE;
BEGIN
--

    P_ERROR := '';
    
    ----------------------------------------------------------------------------
        -- GET tipo de solicitud por ejemplo "SOL003" 
        SELECT  SVRSVPR_SRVC_CODE
        INTO    P_CODIGO_SOLICITUD
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
        
        
        -- UPDATE SOLICITUD
            UPDATE SVRSVPR
              SET SVRSVPR_SRVS_CODE = P_ESTADO, -- AN , AP , RE
              --SVRSVPR_ACCD_TRAN_NUMBER = P_TRAN_NUMBER_OLD,
              --SVRSVPR_BILLING_IND = 'N', -- CASE WHEN P_ESTADO = 'AP' THEN 'Y' ELSE 'N' END,
              SVRSVPR_DATA_ORIGIN = 'WorkFlow'
            WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
            AND SVRSVPR_SRVS_CODE IN (
               -- Estados de solic. MODIFICABLES segun las reglas y configuracion
               SELECT  SVRRSST_SRVS_CODE       TEMP_SRVS_CODE --- Estado SOL
               FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
               INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
               ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
               AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
               INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
               ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
               WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
               AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
               AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
               AND SVRRSST_SRVS_CODE <> P_ESTADO ---------------- actualizar a un estado distinto
            );
      
      --------------------------------------------------------------------------
--
    COMMIT;
EXCEPTION
  WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_CAMBIO_ESTADO_SOLICITUD;






