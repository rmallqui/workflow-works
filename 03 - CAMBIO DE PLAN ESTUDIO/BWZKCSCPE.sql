create or replace PACKAGE BWZKCSCPE AS
/*******************************************************************************
 BWZKCSCPE:
       Paquete Web _ Desarrollo Propio _  Paquete _ Conti Solicitud Cambio Plan Estudio
*******************************************************************************/
-- FILE NAME..: BWZKCSCPE.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSCPE
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

PROCEDURE P_GET_USUARIO (
            P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
            P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
            P_CORREO_REGACA       OUT VARCHAR2,
            P_ROL_SEDE            OUT VARCHAR2,
            P_ERROR               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
            P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
            P_AN                  OUT NUMBER,
            P_ERROR               OUT VARCHAR2
        );
              
--------------------------------------------------------------------------------

/*
-- 201720 - No se utiliza
PROCEDURE P_ELIMINAR_ASIGNATURAS( 
                  P_PIDM_ALUMNO               IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                  P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
                  P_ERROR                   OUT VARCHAR2
              );
*/

--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
                P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                P_COMENTARIO          OUT VARCHAR2,
                P_TELEFONO            OUT VARCHAR2
          );

--------------------------------------------------------------------------------

/*
-- 201720 - No se utiliza
PROCEDURE P_OBTENER_PRIORIDAD (
                  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                  P_COD_DETALLE         IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, ------------- APEC
                  P_PERIODO             IN SFBETRM.SFBETRM_TERM_CODE%TYPE, ------------- APEC
                  P_PRIORIDAD           OUT SVRRSRV.SVRRSRV_SEQ_NO%TYPE, ------------ Sequence number of the Service Rule
                  P_ERROR               OUT VARCHAR2 ----------------------------------- APEC
              );
*/

--------------------------------------------------------------------------------

/*
-- 201720 - No se utiliza
PROCEDURE P_GET_SIDEUDA_ALUMNO (
          P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
          P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
          P_DEUDA               OUT VARCHAR2
        );
*/

--------------------------------------------------------------------------------

/*
-- 201720 - No se utiliza
PROCEDURE P_ELIMINAR_RECARGO (
        P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_NUMERO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_TERM_PERIODO          IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
        P_CODIGO_DETALLE        IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, ------ APEC
        P_ERROR                 OUT VARCHAR2
    );
*/

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_CATALOGO (
            P_PIDM_ALUMNO        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_PERIODO          IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
            P_ERROR            OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_RETENCION (
              P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
              P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
              P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
              P_ERROR               OUT VARCHAR2
        );

--------------------------------------------------------------------------------

/*
-- 201720 - No se utiliza
PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
            P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
            P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_FECHA_VALIDA        OUT VARCHAR2
        );
*/

--------------------------------------------------------------------------------

PROCEDURE P_SET_ATRIBUTO_PLANS (
              P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
              P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,      -------- COD atributos
              P_ERROR             OUT VARCHAR2
        );

-------------------------------------------------------------------------------

PROCEDURE P_SET_CURSO_INTROD (
              P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
              P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
              P_MESSAGE             OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE P_CPE_EXECCAPP (
              P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
              P_MESSAGE           OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE P_CPE_EXECPROY (
              P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
              P_MESSAGE           OUT VARCHAR2
        );

--------------------------------------------------------------------------------
-- SP Anidado
PROCEDURE P_APEC_SET_DEUDA ( 
            P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_COD_DETALLE           IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE,
            P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
            P_ERROR                 OUT VARCHAR2
        );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKCSCPE;