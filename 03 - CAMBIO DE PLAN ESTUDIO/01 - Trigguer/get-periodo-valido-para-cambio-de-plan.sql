-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SET SERVEROUTPUT ON
DECLARE P_PIDM_ALUMNO             SPRIDEN.SPRIDEN_ID%TYPE := 29896;
        P_MODALIDAD_ALUMNO        SGBSTDN.SGBSTDN_DEPT_CODE%TYPE; -- Departamento
        P_COD_SEDE                STVCAMP.STVCAMP_CODE%TYPE;  -- SGBSTDN.SGBSTDN_CAMP_CODE%TYPE;
        P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo      
        P_SRVC_CODE               SVRSVPR.SVRSVPR_SRVC_CODE%TYPE;
        P_INDICADOR               NUMBER;
        P_PERIODO                 SFBETRM.SFBETRM_TERM_CODE%TYPE;
        P_NOMBRE_SEDE             STVCAMP.STVCAMP_DESC%TYPE;
BEGIN

      -- GET DEPARTAMENTO y CAMPUS 
      SELECT COUNT(*) INTO P_INDICADOR FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO;
      IF P_INDICADOR = 0 THEN
            P_MODALIDAD_ALUMNO    := '-';
            P_COD_SEDE            := '-';
            P_NOMBRE_SEDE         := '-';
      ELSE  
            -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno segun el periodo
            SELECT SGBSTDN_DEPT_CODE, SGBSTDN_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE FROM(
                  SELECT  SGBSTDN_TERM_CODE_EFF, 
                          SGBSTDN_DEPT_CODE, 
                          SGBSTDN_CAMP_CODE
                          --SGBSTDN_STST_CODE   -- STVSTST : validaciones para matricula y otro
                  FROM SGBSTDN 
                  WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO 
                  ORDER BY SGBSTDN_TERM_CODE_EFF DESC  
            ) WHERE ROWNUM <= 1;
            
            -- GET nombre campus 
            SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
      END IF;
      
      
      
      -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
      IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
          P_PERIODO := '-';
      ELSE
            -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
            SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                      WHEN 'UPGT' THEN 'W' 
                                      WHEN 'UREG' THEN 'R' 
                                      WHEN 'UPOS' THEN '-' 
                                      WHEN 'ITEC' THEN '-' 
                                      WHEN 'UCIC' THEN '-' 
                                      WHEN 'UCEC' THEN '-' 
                                      WHEN 'ICEC' THEN '-' 
                                      ELSE '1' END ||
                    CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                      WHEN 'F01' THEN 'A' 
                                      WHEN 'F02' THEN 'L' 
                                      WHEN 'F03' THEN 'C' 
                                      WHEN 'V00' THEN 'V' 
                                      ELSE '9' END
            INTO P_PART_PERIODO
            FROM STVCAMP,STVDEPT 
            WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
      
            -- GET PERIODO ACTIVO 
            SELECT SOBPTRM_TERM_CODE INTO P_PERIODO
            FROM (
                -- GET COD PARTE-PERIODO activo ò un PERIODO proximo que sea valido para realizar la solicitud (no "00")
                SELECT SOBPTRM_TERM_CODE FROM SOBPTRM
                WHERE  SOBPTRM_PTRM_CODE LIKE ('' || 'RA' || '%')
                --AND SYSDATE BETWEEN SOBPTRM_START_DATE AND SOBPTRM_END_DATE
                AND SYSDATE <= SOBPTRM_START_DATE
                AND SOBPTRM_TERM_CODE NOT LIKE ('%00')
                ORDER BY SOBPTRM_TERM_CODE ASC
            ) WHERE ROWNUM <= 1;
            
            --:NEW.SVRSVPR_TERM_CODE := P_PERIODO;

      END IF;
      
      DBMS_OUTPUT.PUT_LINE(' : ' || P_MODALIDAD_ALUMNO || ' : ' || P_COD_SEDE || ' : ' || P_PERIODO );
END;