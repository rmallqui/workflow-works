/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_REASON VARCHAR2(4000);
BEGIN
    P_VERIFICAR_APTO(320724, '201810', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_ASIGNATURA      IN VARCHAR2,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0;
        V_RET02             INTEGER := 0;
        V_SOLPER            INTEGER := 0;
        V_SOLRES            INTEGER := 0;
        V_SOLREC            INTEGER := 0;
        V_SOLCAM            INTEGER := 0;
        V_SOLTRA            INTEGER := 0;
        V_ASI_MAT           INTEGER := 0;
        V_DIRIGIDOS         INTEGER := 0;
        V_CREDITOS          INTEGER;
        V_CICLO             INTEGER;
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
        
BEGIN
   --- SP DE VALIDACION PARA CURSO DIRIGIDO
   ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SPRHOLD_TO_DATE;
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '- Estudiante tiene retención activa de deuda';
        V_FLAG := 1;
        RETURN;
    END IF;
   
   ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SPRHOLD_TO_DATE;
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || chr(13) || '- Estudiante tiene retención activa de documento pendiente. ';
        ELSE
            V_RESPUESTA := '- Estudiante tiene retención activa de documento pendiente. ';            
        END IF;
        V_FLAG := 1;
    END IF;
    
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || chr(13) || '- Estudiante tiene solicitud de Permamencia de Plan de Estudios activa. ';
        ELSE
            V_RESPUESTA := '- Estudiante tiene solicitud de Permamencia de Plan de Estudios activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;
    
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || chr(13) || '- Estudiante tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '- Estudiante tiene solicitud de Reserva de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;
    
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || chr(13) || '- Estudiante tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '- Estudiante tiene solicitud de Rectificación de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;
    
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || chr(13) || '- Estudiante tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '- Estudiante tiene solicitud de Cambio de Plan de Estudio activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    -- >> VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR 
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLTRA > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || chr(13) || '- Estudiante tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '- Estudiante tiene solicitud de Traslado Interno activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    -- >> VALIDACIÓN DE NRC DIRIGIDO: SOLO 1 POR PERIODO
    SELECT COUNT(*)
    INTO V_DIRIGIDOS
    FROM SFRSTCR
    INNER JOIN SSBSECT ON
        SFRSTCR_TERM_CODE = SSBSECT_TERM_CODE
        and SFRSTCR_CRN = SSBSECT_CRN
    WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_RSTS_CODE IN ('RW','RE')
        AND SSBSECT_SSTS_CODE = 'D';

    IF V_DIRIGIDOS > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || chr(13) || '- Estudiante ya tiene una asignatura dirigida en el periodo solicitado. ';
        ELSE
            V_RESPUESTA := '- Estudiante ya tiene una asignatura dirigida en el periodo solicitado. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    -- >> VALIDACIÓN DE NRC DIRIGIDO: SOLO 1 POR PERIODO
    SELECT COUNT(*)
    INTO V_ASI_MAT
    FROM SFRSTCR
    INNER JOIN SSBSECT ON
        SFRSTCR_TERM_CODE = SSBSECT_TERM_CODE
        and SFRSTCR_CRN = SSBSECT_CRN
    WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_RSTS_CODE IN ('RW','RE')
        AND SSBSECT_SUBJ_CODE || SSBSECT_CRSE_NUMB = P_ASIGNATURA;

    IF V_ASI_MAT > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || chr(13) || '- Estudiante ya esta matriculado en la asignatura solicitada. ';
        ELSE
            V_RESPUESTA := '- Estudiante ya esta matriculado en la asignatura solicitada. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    -----------------------------------------------
    -- OBTENER CREDITOS Y CICLO
    BWZKWFFN.P_CREDITOS_CICLO(P_PIDM,V_CREDITOS,V_CICLO);

    IF V_CREDITOS >= 154 THEN
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
        RETURN;
    ELSE
        P_STATUS_APTO := 'FALSE';
        P_REASON := 'Estudiante no tiene los créditos solicitados (mínimo 154 créditos aprobados)';
        RETURN;
    END IF;

    IF V_CREDITOS >= 154 THEN
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
        RETURN;
    ELSE
        P_STATUS_APTO := 'FALSE';
        P_REASON := 'Estudiante no tiene los créditos solicitados (mínimo 154 créditos aprobados)';
        RETURN;
    END IF;
    
    P_STATUS_APTO := 'FALSE';
    P_REASON := 'Estudiante no puede acceder';
    RETURN;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;