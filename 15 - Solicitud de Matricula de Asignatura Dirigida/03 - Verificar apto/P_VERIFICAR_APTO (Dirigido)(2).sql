/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_REASON VARCHAR2(4000);
BEGIN
    P_VERIFICAR_APTO(142, '201810', 'AAUC00424', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_ASIGNATURA_CODE   IN VARCHAR2,
        P_STATUS_APTO       OUT VARCHAR2,
        P_REASON            OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0; --VALIDA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VALIDA RETENCIÓN 02
        V_RET04             INTEGER := 0; --VALIDA RETENCIÓN 04
        V_RET07             INTEGER := 0; --VALIDA RETENCIÓN 07
        V_RETPER            INTEGER := 0; --VALIDA RETENCIÓN 08 - 16
        V_SOLPER            INTEGER := 0; --VALIDA SOLICITUD DE PERMANENCIA
        V_SOLRES            INTEGER := 0; --VALIDA SOLICITUD DE RESERVA
        V_SOLREC            INTEGER := 0; --VALIDA SOLICITUD DE RECTIFICACION
        V_SOLCAM            INTEGER := 0; --VALIDA CAMBIO DE PLAN
        V_SOLTRA            INTEGER := 0; --VALIDA TRASLADO INTERNO
        V_ASI_MAT           INTEGER := 0; --VALIDA LA ASIGNATURA MATRICULADA
        V_DIRIGIDOS         INTEGER := 0; --VALIDA CURSOS DIRIGIDOS
        V_CREDITOS          INTEGER;
        V_CICLO             INTEGER;
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
        
BEGIN
   --- SP DE VALIDACION PARA CURSO DIRIGIDO
    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención por deuda pendiente activa.';
        V_FLAG := 1;
    END IF;
    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención activa de documento pendiente. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención por documento pendiente activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 04 (PAGO DE MATRICULA Y PRIMERA CUOTA)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET04
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '04'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET04 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de pago de matricula y primera cuota activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de pago de matricula y primera cuota activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 07 (CAMBIO DE PLAN DE ESTUDIO)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET07    
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '07'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET07 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de cambio de plan activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de cambio de plan activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 08 - 16 (PERMANENCIA DE PLAN)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RETPER
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE IN ('08','16')
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RETPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de permanencia de plan de estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de permanencia de plan de estudios activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permamencia de Plan de Estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permamencia de Plan de Estudios activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    -- >> VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR 
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLTRA > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    -- >> VALIDACIÓN DE NRC DIRIGIDO: SOLO 2 POR PERIODO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_DIRIGIDOS
    FROM SFRSTCR
    INNER JOIN SSBSECT ON
        SFRSTCR_TERM_CODE = SSBSECT_TERM_CODE
        and SFRSTCR_CRN = SSBSECT_CRN
    WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_RSTS_CODE IN ('RW','RE')
        AND SSBSECT_SSTS_CODE = 'D';

    IF V_DIRIGIDOS >= 2 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene dos o más asignaturas dirigidas matriculadas en el periodo solicitado. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene dos o más asignaturas dirigidas matriculadas en el periodo solicitado. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    -- >> VALIDACIÓN DE CURSO MATRICULADO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_ASI_MAT
    FROM SFRSTCR
    INNER JOIN SSBSECT ON
        SFRSTCR_TERM_CODE = SSBSECT_TERM_CODE
        and SFRSTCR_CRN = SSBSECT_CRN
    WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_RSTS_CODE IN ('RW','RE')
        AND SSBSECT_SUBJ_CODE || SSBSECT_CRSE_NUMB = P_ASIGNATURA_CODE;

    IF V_ASI_MAT > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Ya esta matriculado en la asignatura solicitada. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Ya esta matriculado en la asignatura solicitada. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    -- OBTENER CREDITOS Y CICLO
    --################################################################################################
    BWZKWFFN.P_CREDITOS_CICLO(P_PIDM,V_CREDITOS,V_CICLO);

    --################################################################################################
    --VALIDACIÓN DE CREDITOS MINIMOS (154)
    --################################################################################################
    IF V_CREDITOS < 154 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No tiene los créditos solicitados (mínimo 154 créditos aprobados). ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No tiene los créditos solicitados (mínimo 154 créditos aprobados). ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    --VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG = 1 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;