/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_REASON VARCHAR2(4000);
BEGIN
    P_GET_COORDINADOR(123287, '201810', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_GET_COORDINADOR (
        P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_ROL_DC              IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a mayusculas 'coordinador'
        P_CORREO_COORD        OUT VARCHAR2,
        P_ROL_COORD           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_ROLE_ID                 NUMBER;
    V_ORG_ID                  NUMBER;
    V_INDICADOR               NUMBER;
    V_Email_Address           VARCHAR2(500);
    V_EXEPTION                EXCEPTION;
    
    V_CODE_DEPT      STVDEPT.STVDEPT_CODE%TYPE;
    V_CODE_CAMP      STVCAMP.STVCAMP_CODE%TYPE;
    V_PROGRAM        SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_TERMCTLG       SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE;
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;
      
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
      
    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;
        
BEGIN 
        --################################################################################################
        -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
        --################################################################################################
        OPEN C_SORLCUR;
        LOOP
            FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
            EXIT WHEN C_SORLCUR%NOTFOUND;
            END LOOP;
        CLOSE C_SORLCUR;
        
        IF P_DEPT_CODE = 'UREG' THEN
            P_ROL_COORD := P_ROL_DC || P_DEPT_CODE || V_PROGRAM || P_CAMP_CODE;
        ELSE
            P_ROL_COORD := P_ROL_DC || P_DEPT_CODE || '000' || P_CAMP_CODE;
        END IF;
        
        -- Obtener el ROL_ID 
        SELECT ID 
        INTO V_ROLE_ID 
        FROM WORKFLOW.ROLE 
        WHERE NAME = P_ROL_COORD;
        
        -- Obtener el ORG_ID 
        SELECT ID 
        INTO V_ORG_ID 
        FROM WORKFLOW.ORGANIZATION 
        WHERE NAME = 'Root';
        
        --Validar que exista el ROL
        SELECT COUNT(*)
        INTO V_INDICADOR 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;
        
        IF V_INDICADOR = 0 THEN
          RAISE V_EXEPTION;
        END if;
        
        -- #######################################################################
        OPEN C_ROLE_ASSIGNMENT;
        LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
              -- Obtener Datos Usuario
              SELECT Email_Address 
              INTO V_Email_Address 
              FROM WORKFLOW.WFUSER 
              WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
  
              P_CORREO_COORD := P_CORREO_COORD || V_Email_Address || ',';
              
        END LOOP;
        CLOSE C_ROLE_ASSIGNMENT;
        
        -- Extraer el ultimo digito en caso sea un "coma"(,)
        SELECT SUBSTR(P_CORREO_COORD,1,LENGTH(P_CORREO_COORD) -1)
        INTO P_CORREO_COORD
        FROM DUAL
        WHERE SUBSTR(P_CORREO_COORD,-1,1) = ',';

EXCEPTION
  WHEN V_EXEPTION THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No se encontro el ROL: ' || P_ROL_COORD);
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_COORDINADOR;