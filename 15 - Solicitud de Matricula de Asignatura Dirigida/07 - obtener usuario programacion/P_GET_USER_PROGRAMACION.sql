/*
SET SERVEROUTPUT ON
DECLARE P_CORREO_PROGR VARCHAR2(4000);
 P_ROL_PROGR VARCHAR2(4000);
BEGIN
    P_GET_USER_PROGRAMACION('UREG', 'S01', 'prograacad', P_CORREO_PROGR, P_ROL_PROGR);
    DBMS_OUTPUT.PUT_LINE(P_CORREO_PROGR);
    DBMS_OUTPUT.PUT_LINE(P_ROL_PROGR);
END;
*/

CREATE OR REPLACE PROCEDURE P_GET_USER_PROGRAMACION (
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_ROL_PROGRAMACION    IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a minusculas 'progracad'
        P_CORREO_PROGR        OUT VARCHAR2,
        P_ROL_PROGR           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_ROLE_ID                   NUMBER;
    V_ORG_ID                    NUMBER;
    V_INDICADOR                 NUMBER;
    V_Email_Address             VARCHAR2(500);
    V_EXEPTION                  EXCEPTION;
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;
      
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
        
BEGIN 

        P_ROL_PROGR := P_ROL_PROGRAMACION || P_DEPT_CODE || P_CAMP_CODE;

        -- Obtener el ROL_ID 
        SELECT ID 
        INTO V_ROLE_ID 
        FROM WORKFLOW.ROLE 
        WHERE NAME = P_ROL_PROGR;

        -- Obtener el ORG_ID 
        SELECT ID 
        INTO V_ORG_ID 
        FROM WORKFLOW.ORGANIZATION 
        WHERE NAME = 'Root';

        --Validar que exista el ROL
        SELECT COUNT(*)
        INTO V_INDICADOR 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;

        IF V_INDICADOR = 0 THEN
          RAISE V_EXEPTION;
        END if;

        -- #######################################################################
        OPEN C_ROLE_ASSIGNMENT;
        LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
              -- Obtener Datos Usuario
              SELECT Email_Address 
              INTO V_Email_Address 
              FROM WORKFLOW.WFUSER 
              WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;

              P_CORREO_PROGR := P_CORREO_PROGR || V_Email_Address || ',';
              
        END LOOP;
        CLOSE C_ROLE_ASSIGNMENT;
        
        -- Extraer el ultimo digito en caso sea un "coma"(,)
        SELECT SUBSTR(P_CORREO_PROGR,1,LENGTH(P_CORREO_PROGR) -1)
        INTO P_CORREO_PROGR
        FROM DUAL
        WHERE SUBSTR(P_CORREO_PROGR,-1,1) = ',';

EXCEPTION
  WHEN V_EXEPTION THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No se encontro el ROL ' || P_ROL_PROGR);
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USER_PROGRAMACION;