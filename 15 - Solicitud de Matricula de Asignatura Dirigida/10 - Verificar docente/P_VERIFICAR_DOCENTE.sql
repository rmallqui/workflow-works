/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_DOC VARCHAR2(4000);
 P_REASON_DOC VARCHAR2(4000);
 P_DOCENTE_DNI VARCHAR2(4000);
 P_DOCENTE_NAME VARCHAR2(4000);
 P_DOCENTE_EMAIL VARCHAR2(4000);
BEGIN
    P_VERIFICAR_DOCENTE('ptorres', '201820', P_DOCENTE_DNI, P_DOCENTE_NAME, P_DOCENTE_EMAIL, P_STATUS_DOC, P_REASON_DOC);
    DBMS_OUTPUT.PUT_LINE(P_DOCENTE_DNI);
    DBMS_OUTPUT.PUT_LINE(P_DOCENTE_NAME);
    DBMS_OUTPUT.PUT_LINE(P_DOCENTE_EMAIL);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_DOC);
    DBMS_OUTPUT.PUT_LINE(P_REASON_DOC);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_DOCENTE (
        P_DOCENTE           IN VARCHAR2,
        P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_DOCENTE_DNI       OUT VARCHAR2,
        P_DOCENTE_NAME      OUT VARCHAR2,
        P_DOCENTE_EMAIL     OUT VARCHAR2,
        P_STATUS_DOC        OUT VARCHAR2,
        P_REASON_DOC        OUT VARCHAR2
)
AS
        V_DOCENTE           NUMBER := 0;
BEGIN

    IF P_DOCENTE IS NULL OR P_DOCENTE = '' THEN
        P_STATUS_DOC := 'FALSE';
        P_REASON_DOC := 'Tiene que registrar el usuario del docente.';
        P_DOCENTE_NAME := '';
        P_DOCENTE_DNI := '';
        P_DOCENTE_EMAIL := '';
        RETURN;
    END IF;

--################################################################################################
---VALIDACIÓN DE DOCENTE
--################################################################################################

    SELECT COUNT(*)
    INTO V_DOCENTE
    FROM
    (
    WITH T_DOCENTE_NRC AS (
        SELECT SIRASGN_CRN NRC,
             SPRIDEN_LAST_NAME|| ' ' ||INITCAP(SPRIDEN_FIRST_NAME) AS DOCE,
             SPRIDEN_PIDM PIDM,
             SPRIDEN_ID AS IDDOCE,
             MAX(SIRASGN_PRIMARY_IND) DATO,
             SIRASGN_TERM_CODE
        FROM SIRASGN
        INNER JOIN SPRIDEN ON 
            SPRIDEN_PIDM = SIRASGN_PIDM
            AND SPRIDEN_CHANGE_IND IS NULL
        GROUP BY SIRASGN_CRN, SPRIDEN_LAST_NAME|| ' ' ||INITCAP(SPRIDEN_FIRST_NAME), SPRIDEN_PIDM, SPRIDEN_ID, SIRASGN_TERM_CODE
    ), T_USER AS (
        SELECT GORADID_PIDM, GORADID_ADDITIONAL_ID USER_DOCE
        FROM GORADID
        WHERE GORADID_ADID_CODE = 'UAD'
    )
        SELECT UNIQUE
        T_USER.GORADID_PIDM, IDDOCE, DOCE, T_USER.USER_DOCE, 
        SSBSECT_PTRM_CODE, SSBSECT_INSM_CODE, SSBSECT_CAMP_CODE, SSBSECT_TERM_CODE
        FROM T_DOCENTE_NRC
        INNER JOIN SSBSECT ON 
            SSBSECT_CRN = T_DOCENTE_NRC.NRC
            AND SSBSECT_TERM_CODE = T_DOCENTE_NRC.SIRASGN_TERM_CODE
        LEFT JOIN T_USER ON
            T_USER.GORADID_PIDM = PIDM
        WHERE SSBSECT_SUBJ_CODE <> 'ASEX'
    )
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
        AND USER_DOCE = LOWER(P_DOCENTE);
    
    IF V_DOCENTE > 0 THEN
        SELECT "Appat" || ' ' || "ApMat" || ', ' || "Nombres" Docente,
        DNI,
        LOWER(P_DOCENTE)||'@continental.edu.pe' CorreoDoc
        INTO P_DOCENTE_NAME, 
        P_DOCENTE_DNI, 
        P_DOCENTE_EMAIL
        FROM TBLPERSONA@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDUsuario" = LOWER(P_DOCENTE);
        
        IF P_DOCENTE_NAME IS NULL OR P_DOCENTE_NAME = ', ' THEN
            P_STATUS_DOC := 'FALSE';
            P_REASON_DOC := 'El docente no tiene registrado su nombre.';
            P_DOCENTE_NAME := '';
            P_DOCENTE_DNI := '';
            P_DOCENTE_EMAIL := '';
            RETURN;
        END IF;
        
        IF P_DOCENTE_DNI IS NULL OR P_DOCENTE_DNI = '' THEN
            P_STATUS_DOC := 'FALSE';
            P_REASON_DOC := 'El docente no tiene registrado su DNI.';
            P_DOCENTE_NAME := '';
            P_DOCENTE_DNI := '';
            P_DOCENTE_EMAIL := '';
            RETURN;
        END IF;
        
        P_STATUS_DOC := 'TRUE';
        P_REASON_DOC := 'OK';
                
    ELSE
        P_DOCENTE_NAME := '';
        P_DOCENTE_DNI := '';
        P_DOCENTE_EMAIL := '';
        P_STATUS_DOC := 'FALSE';
        P_REASON_DOC := 'El docente no esta activo o no esta registrado en el periodo vigente';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_DOCENTE;