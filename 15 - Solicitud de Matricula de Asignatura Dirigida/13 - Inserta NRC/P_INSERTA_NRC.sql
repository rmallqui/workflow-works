/*
SET SERVEROUTPUT ON
DECLARE P_MESSAGE VARCHAR2(4000);
BEGIN
    P_INSERTA_NRC(37577, 7163, '201810', P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_INSERTA_NRC (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_NRC_CODE        IN SSBSECT.SSBSECT_CRN%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_MESSAGE         OUT VARCHAR2
)
AS
        V_HORMA             INTEGER := 0; --VERIFICACIÓN DE HORAS MAX
        V_MATRI             INTEGER := 0; --VERIFICACIÓN DE MATRICULA
        V_STATU             INTEGER := 0; --VERIFICACIÓN DE ESTATUS ELEGIBLE
        V_SEQNC             INTEGER := 0; 
        V_PTRM              VARCHAR2(100);
        V_CAMP              VARCHAR2(100);
        V_CRED              INTEGER := 0;
BEGIN

    --################################################################################################
    ---VERIFICAR HORAS MAX DE MATRICULA EN EL PERIODO 
    --################################################################################################
        SELECT COUNT(*)
        INTO V_HORMA
        FROM SFBETRM
        WHERE SFBETRM_PIDM = P_PIDM
        AND SFBETRM_TERM_CODE = P_TERM_CODE;
        
        IF V_HORMA = 0 THEN
            INSERT INTO SFBETRM (
                            SFBETRM_TERM_CODE,          SFBETRM_PIDM,               SFBETRM_ESTS_CODE,      SFBETRM_ESTS_DATE,      SFBETRM_MHRS_OVER,
                            SFBETRM_AR_IND,             SFBETRM_ASSESSMENT_DATE,    SFBETRM_ADD_DATE,       SFBETRM_ACTIVITY_DATE,  SFBETRM_TMST_CODE,      
                            SFBETRM_TMST_DATE,          SFBETRM_TMST_MAINT_IND,     SFBETRM_USER,           SFBETRM_REFUND_DATE,    SFBETRM_DATA_ORIGIN,
                            SFBETRM_INITIAL_REG_DATE,   SFBETRM_MIN_HRS,            SFBETRM_MINH_SRCE_CDE,  SFBETRM_MAXH_SRCE_CDE)
    
                        VALUES (
                            P_TERM_CODE,                P_PIDM,                     'EL',                   SYSDATE,                27,
                            'N',                        SYSDATE,                    SYSDATE,                SYSDATE,                'CI',
                            SYSDATE,                    'S',                        'WFAUTO',               SYSDATE,                'BANNER',
                            SYSDATE,                    0,                          'M',                    'U');
            COMMIT;
        END IF;
    --################################################################################################
    ---VERIFICAR STATUS ELEGIBLE
    --################################################################################################
        SELECT COUNT(*)
        INTO V_STATU
        FROM SFRENSP
        WHERE SFRENSP_PIDM = P_PIDM
        AND SFRENSP_TERM_CODE = P_TERM_CODE;
        
        IF V_STATU = 0 THEN
            INSERT INTO SFRENSP (
                            SFRENSP_TERM_CODE,      SFRENSP_PIDM,           SFRENSP_KEY_SEQNO,      SFRENSP_ESTS_CODE,      SFRENSP_ESTS_DATE, 
                            SFRENSP_ADD_DATE,       SFRENSP_ACTIVITY_DATE,  SFRENSP_USER,           SFRENSP_DATA_ORIGIN)
                        VALUES (
                            P_TERM_CODE,            P_PIDM,                  1,                      'EL',                   SYSDATE,
                            SYSDATE,                SYSDATE,                'WFAUTO',               'Banner');
            COMMIT;
        END IF;

    --################################################################################################
    ---VERIFICAR MATRICULA EN EL PERIODO
    --################################################################################################
        SELECT COUNT(*)
        INTO V_MATRI
        FROM SFRSTCR
        WHERE SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_CRN = P_NRC_CODE;
        
        IF V_MATRI = 0 THEN
            
            SELECT NVL(MAX(SFRSTCR_REG_SEQ),0)
            INTO V_SEQNC
            FROM SFRSTCR
            WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
            AND SFRSTCR_CRN = P_NRC_CODE;
            
            SELECT DISTINCT SSBSECT_PTRM_CODE, SSBSECT_CAMP_CODE, SSBSECT_CREDIT_HRS
            INTO V_PTRM, V_CAMP, V_CRED
            FROM SSBSECT
            WHERE SSBSECT_TERM_CODE = P_TERM_CODE
            AND SSBSECT_CRN = P_NRC_CODE;
            
            INSERT INTO SFRSTCR (
                SFRSTCR_TERM_CODE,              SFRSTCR_PIDM,           SFRSTCR_CRN,                        SFRSTCR_CLASS_SORT_KEY,     SFRSTCR_REG_SEQ,
                SFRSTCR_PTRM_CODE,              SFRSTCR_RSTS_CODE,      SFRSTCR_RSTS_DATE,                  SFRSTCR_ERROR_FLAG,         SFRSTCR_MESSAGE,
                SFRSTCR_BILL_HR,                SFRSTCR_WAIV_HR,        SFRSTCR_CREDIT_HR,                  SFRSTCR_BILL_HR_HOLD,       SFRSTCR_CREDIT_HR_HOLD,
                SFRSTCR_GMOD_CODE,              SFRSTCR_GRDE_CODE,      SFRSTCR_GRDE_CODE_MID,              SFRSTCR_GRDE_DATE,          SFRSTCR_DUPL_OVER,
                SFRSTCR_LINK_OVER,              SFRSTCR_CORQ_OVER,      SFRSTCR_PREQ_OVER,                  SFRSTCR_TIME_OVER,          SFRSTCR_CAPC_OVER,
                SFRSTCR_LEVL_OVER,              SFRSTCR_COLL_OVER,      SFRSTCR_MAJR_OVER,                  SFRSTCR_CLAS_OVER,          SFRSTCR_APPR_OVER,
                SFRSTCR_APPR_RECEIVED_IND,      SFRSTCR_ADD_DATE,       SFRSTCR_ACTIVITY_DATE,              SFRSTCR_LEVL_CODE,          SFRSTCR_CAMP_CODE,
                SFRSTCR_RESERVED_KEY,           SFRSTCR_ATTEND_HR,      SFRSTCR_REPT_OVER,                  SFRSTCR_RPTH_OVER,          SFRSTCR_TEST_OVER,
                SFRSTCR_CAMP_OVER,              SFRSTCR_USER,           SFRSTCR_DEGC_OVER,                  SFRSTCR_PROG_OVER,          SFRSTCR_LAST_ATTEND,
                SFRSTCR_GCMT_CODE,              SFRSTCR_DATA_ORIGIN,    SFRSTCR_ASSESS_ACTIVITY_DATE,       SFRSTCR_DEPT_OVER,          SFRSTCR_ATTS_OVER,
                SFRSTCR_CHRT_OVER,              SFRSTCR_RMSG_CDE,       SFRSTCR_WL_PRIORITY,                SFRSTCR_WL_PRIORITY_ORIG,   SFRSTCR_GRDE_CODE_INCMP_FINAL,
                SFRSTCR_INCOMPLETE_EXT_DATE,    SFRSTCR_MEXC_OVER,      SFRSTCR_STSP_KEY_SEQUENCE,          SFRSTCR_BRDH_SEQ_NUM,       SFRSTCR_BLCK_CODE,
                SFRSTCR_STRH_SEQNO,             SFRSTCR_STRD_SEQNO,     SFRSTCR_VERSION,                    SFRSTCR_USER_ID,            SFRSTCR_VPDI_CODE,          
                SFRSTCR_SESSIONID,              SFRSTCR_CURRENT_TIME)
            VALUES (
                P_TERM_CODE,                    P_PIDM,                 P_NRC_CODE,                         NULL,                       V_SEQNC, 
                V_PTRM,                         'RE',                   SYSDATE,                            NULL,                       NULL, 
                V_CRED,                         V_CRED,                 V_CRED,                             V_CRED,                     V_CRED,
                'S',                            NULL,                   NULL,                               SYSDATE,                    'N', 
                'N',                            'N',                    'N',                                'N',                        'N',
                'N',                            'N',                    'N',                                'N',                        'N', 
                NULL,                           SYSDATE,                SYSDATE,                            'PG',                       V_CAMP,
                NULL,                           NULL,                   'N',                                'N',                        NULL, 
                'N',                            'BANNER',               'N',                                'N',                        NULL, 
                NULL,                           NULL,                   SYSDATE,                            'N',                        'N', 
                'N',                            NULL,                   NULL,                               NULL,                       NULL, 
                NULL,                           'N',                    1,                                  NULL,                       NULL, 
                NULL,                           NULL,                   NULL,                               NULL,                       NULL,
                32533092,                       CAST(SYSDATE AS TIMESTAMP(9)));
            COMMIT;

            UPDATE SSBSECT SET
            SSBSECT_ENRL = SSBSECT_ENRL + 1
            WHERE SSBSECT_CRN = P_NRC_CODE
            AND SSBSECT_TERM_CODE = P_TERM_CODE;
            COMMIT;
            
            UPDATE SSBSECT SET
            SSBSECT_SEATS_AVAIL = SSBSECT_MAX_ENRL - SSBSECT_ENRL
            WHERE SSBSECT_CRN = P_NRC_CODE
            AND SSBSECT_TERM_CODE = P_TERM_CODE;
            COMMIT;

            P_MESSAGE := 'Se registró la asignatura al estudiante.';
        ELSE
            P_MESSAGE := 'El estudiante ya tiene registrado la asignatura solicitada.';
        END IF;
        
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_INSERTA_NRC;