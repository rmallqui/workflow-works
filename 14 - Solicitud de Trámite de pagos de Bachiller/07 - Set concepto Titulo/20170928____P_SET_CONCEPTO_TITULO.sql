/*
-- buscar objecto
select dbms_metadata.get_ddl('PROCEDURE','P_SET_CONCEPTO_TITULO') from dual
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

SET SERVEROUTPUT ON
DECLARE   P_MESSAGE               VARCHAR2(2000);
BEGIN
    P_SET_CONCEPTO_TITULO(145364,'04026324','6669','UREG','201710','105','TI','C01','C02',P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;

SET SERVEROUTPUT ON
DECLARE   P_MESSAGE               VARCHAR2(2000);
BEGIN    
    P_SET_CONCEPTO_TITULO(424925,'00516627','6669','UPGT','201710','108','TI','C01','C02',P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

/* ===================================================================================================================
  NOMBRE    : P_SET_CONCEPTO_TITULO
  FECHA     : 28/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Genera cargos de Titulacion

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_SET_CONCEPTO_TITULO ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
        P_SECCION_CAJA_TITU     IN VARCHAR2,
        P_CONCEPTO_TITU_01      IN VARCHAR2,
        P_CONCEPTO_TITU_02      IN VARCHAR2,
        P_MESSAGE               OUT VARCHAR2
)
AS

    V_SECCION_CAJA_TITU       VARCHAR2(5);
    V_RECEPTION_DATE          SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    
    -- APEC PARAMS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(50);
    V_APEC_IDESCUELA        VARCHAR2(50);
    V_APEC_FECINIC          DATE;
    
BEGIN
      
      -- GET FECHA DE SOLICITUD
      SELECT  SVRSVPR_RECEPTION_DATE
      INTO    V_RECEPTION_DATE
      FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      /*************************************************************************
                                    Ejemplo: BAR - 103
      --------------------------------------------------------------------------
          Rectificación          Modalidad                        Carrera
      BA – Bachiller            R – Regular          -          xxx id Carrera
                                W – Gente Trabaja
                                V - Virtual  
      *************************************************************************/
      V_SECCION_CAJA_TITU := P_SECCION_CAJA_TITU || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
       -- IDSeccionC
      V_APEC_IDSECCIONC   := V_SECCION_CAJA_TITU || ' - ' || P_PROGRAM;
      
      -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
      V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
      SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
      
      SELECT  t1."FecInic",         t2."IDEscuela"
      INTO    V_APEC_FECINIC,       V_APEC_IDESCUELA
      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t1
      INNER JOIN dbo.tblEscuela@BDUCCI.CONTINENTAL.EDU.PE t2
      ON t1."IDDependencia" = t2."IDDependencia"
      AND t1."IDEscuela" = t2."IDEscuela" 
      WHERE t1."IDDependencia" = 'UCCI'
      AND t1."IDsede"     = V_APEC_CAMP
      AND t1."IDPerAcad"  = V_APEC_TERM
      AND t1."IDSeccionC" = V_APEC_IDSECCIONC
      AND t2."IDEscuela"  = 'GYT'
      AND t2."IDTipoEsc"  = 'EXT';
        
      --**********************************************************************++***********************
      -- INSERT ALUMNO ESTADO en caso no exista el registro.
      SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE
      WHERE "IDSede"      = V_APEC_CAMP 
      AND "IDAlumno"      = P_ID_ALUMNO 
      AND "IDPerAcad"     = V_APEC_TERM
      AND "IDDependencia" = 'UCCI' 
      AND "IDSeccionC"    = V_APEC_IDSECCIONC 
      AND "FecInic"       = V_APEC_FECINIC
      AND "IDEscuela"     = V_APEC_IDESCUELA;
      ----- INSERT
      IF (V_INDICADOR = 0) THEN

            INSERT INTO dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE 
                    ("IDSede",                      "IDAlumno", 
                    "IDPerAcad",                    "IDDependencia", 
                    "IDSeccionC",                   "FecInic", 
                    "IDPersonal",                   "IDEscuela", 
                    "IDEscala",                     "IDInstitucion", 
                    "FecMatricula",                 "Beca",  
                    "Estado",                       "FechaEstado", 
                    "MatriculaTipo",                "Carnet", 
                    "PlanPago" )
            VALUES(
                    V_APEC_CAMP,                    P_ID_ALUMNO, 
                    V_APEC_TERM,                    'UCCI', 
                    V_APEC_IDSECCIONC,              V_APEC_FECINIC, 
                    'Banner', /*idpersonal*/        V_APEC_IDESCUELA,
                    'X',/*idescala*/                '00000000000' , /*institucion*/ 
                    V_APEC_FECINIC,                 0, /*beca*/ 
                    'M', /*estado*/                 V_APEC_FECINIC, /*fechaestado*/
                    'C', /*matriculatipo*/          0, /*carnet*/ 
                    5 /*planpago*/ );

      END IF;


      -- *********************************************************************************************
      -- ASINGANDO el cargo a CTACORRIENTE
      SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
      WHERE "IDDependencia" = 'UCCI' 
      AND "IDSede"      = V_APEC_CAMP 
      AND "IDAlumno"    = P_ID_ALUMNO
      AND "IDPerAcad"   = V_APEC_TERM 
      AND "IDSeccionC"  = V_APEC_IDSECCIONC
      AND "FecInic"     = V_APEC_FECINIC
      AND "IDConcepto"  IN (P_CONCEPTO_TITU_01,P_CONCEPTO_TITU_02);

      IF (V_INDICADOR = 0) THEN
            
          INSERT INTO dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                  ("IDSede",            "IDAlumno",               "IDPerAcad", 
                  "IDDependencia",      "IDSeccionC",             "FecInic", 
                  "IDConcepto",         "FecCargo",               "Prorroga", 
                  "Cargo",              "DescMora",               "IDEscuela", 
                  "IDCuenta",           "IDInstitucion",          "Moneda", 
                  "IDEscala",           "Beca",                   "Estado", 
                  "Abono" )
          SELECT  t2."IDsede",          P_ID_ALUMNO,              V_APEC_TERM,
                  'UCCI',               V_APEC_IDSECCIONC,        V_APEC_FECINIC,
                  t1."IDConcepto",      V_RECEPTION_DATE,         '0',
                  "Monto",              0,                        t1."IDEscuela",
                  "IDCuenta",           '00000000000',            t1."Moneda",
                  '',                   0,                        'M',
                  0
          FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
          INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
            ON t1."IDSede"          = t2."IDsede"
            AND t1."IDPerAcad"      = t2."IDPerAcad" 
            AND t1."IDDependencia"  = t2."IDDependencia" 
            AND t1."IDSeccionC"     = t2."IDSeccionC" 
            AND t1."FecInic"        = t2."FecInic"
          WHERE t2."IDDependencia"  = 'UCCI'
          AND t2."IDsede"         = V_APEC_CAMP
          AND t2."IDSeccionC"     = V_APEC_IDSECCIONC
          AND t2."IDSeccionC"     <> '15NEX1A'
          AND t2."IDPerAcad"      = V_APEC_TERM
          AND T1."IDPerAcad"      = V_APEC_TERM
          AND t2."FecInic"        = V_APEC_FECINIC
          AND t1."IDConcepto"     IN (P_CONCEPTO_TITU_01,P_CONCEPTO_TITU_02);

      ELSIF(V_INDICADOR > 1) THEN
          RAISE V_EXCEP_NOTFOUND_CARGO;                  
      END IF;
      
      COMMIT;
EXCEPTION
  WHEN V_EXCEP_NOTFOUND_CARGO THEN
        P_MESSAGE := '- Se encontro cargos ya generados anteriormente.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
        ROLLBACK;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CONCEPTO_TITULO;