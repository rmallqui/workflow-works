/*
SET SERVEROUTPUT ON
declare P_DIPLOMA_ID    VARCHAR2(4000);
        P_IDIOMA_ID     VARCHAR2(4000);
begin
    P_GET_STUDENT_INPUTS(6669,P_DIPLOMA_ID, P_IDIOMA_ID);
    DBMS_OUTPUT.PUT_LINE(P_DIPLOMA_ID);
    DBMS_OUTPUT.PUT_LINE(P_IDIOMA_ID);    
end;
*/

CREATE or replace PROCEDURE P_GET_STUDENT_INPUTS (
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_DIPLOMA_ID          OUT VARCHAR2,
      P_IDIOMA_ID           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 19/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene EL COMENTARIO y el NUMERO DE CUOTAS(para cancelar su pensión) como datos ingresados por el estudiante.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
      V_ERROR                   EXCEPTION;
      V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_ADDL_DATA_CDE           SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE;
      V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
      
      -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
      CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
        --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO V_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_ADDL_DATA_CDE;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 1 THEN
                P_DIPLOMA_ID := V_ADDL_DATA_CDE;
              ELSIF V_ADDL_DATA_SEQ = 2 THEN
                P_IDIOMA_ID := V_ADDL_DATA_CDE;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;

      IF P_DIPLOMA_ID IS NULL THEN
          RAISE V_ERROR;
      END IF;
      
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el identificador del DIPLOMA.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;
