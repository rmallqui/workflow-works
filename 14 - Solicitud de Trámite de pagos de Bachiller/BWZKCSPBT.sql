/* ---------------------------------------------------------------------------------------------
-- buscar objecto
select dbms_metadata.get_ddl('PACKAGE','BWZKCSPBT') from dual
--------------------------------------------------------------------------------------------- */

create or replace PACKAGE BWZKCSPBT AS
/*
 BWZKCSPBT:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti Solicitud  Tramite PAGOS BACHILLER/TITULACION
*/
-- FILE NAME..: BWZKCSPBT.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSPBT
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              


PROCEDURE P_GET_STUDENT_INPUTS (
            P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_DIPLOMA_CODE        OUT VARCHAR2,
            P_DIPLOMA_DESC        OUT VARCHAR2,
            P_CONCEPTO_IDIOMA     OUT VARCHAR2,
            P_IDIOMA_DESC         OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
            P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
            P_AN                  OUT NUMBER,
            P_ERROR               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_CONCEPTOS_BACHILLER ( 
            P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
            P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
            P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
            P_SECCION_CAJA_BACH     IN VARCHAR2,
            P_CONCEPTO_BACH_01      IN VARCHAR2,
            P_CONCEPTO_BACH_02      IN VARCHAR2,
            P_CONCEPTO_BACH_03      IN VARCHAR2,
            P_CONCEPTO_BACH_04      IN VARCHAR2,
            P_CONCEPTO_BACH_05      IN VARCHAR2,
            P_CONCEPTO_BACH_06      IN VARCHAR2,
            P_CONCEPTO_BACH_07      IN VARCHAR2,
            P_CONCEPTO_BACH_08      IN VARCHAR2,
            P_CONCEPTO_BACH_09      IN VARCHAR2,   
            P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,     
            P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
            P_MESSAGE               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_CONCEPTOS_TITULO ( 
            P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
            P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
            P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
            P_SECCION_CAJA_TITU     IN VARCHAR2,
            P_CONCEPTO_TITU_01      IN VARCHAR2,
            P_CONCEPTO_TITU_02      IN VARCHAR2,
            P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,
            P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
            P_MESSAGE               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

    PROCEDURE P_SET_CONCEPTO_BACHILLER ( 
            P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
            P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
            P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
            P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
            P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
            P_SECCION_CAJA_BACH     IN VARCHAR2,
            P_CONCEPTO_BACH_01      IN VARCHAR2,
            P_CONCEPTO_BACH_02      IN VARCHAR2,
            P_CONCEPTO_BACH_03      IN VARCHAR2,
            P_CONCEPTO_BACH_04      IN VARCHAR2,
            P_CONCEPTO_BACH_06      IN VARCHAR2,
            P_CONCEPTO_BACH_07      IN VARCHAR2,           
            P_CONCEPTO_IDIOMA       IN VARCHAR2,
            P_MESSAGE               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_SET_CONCEPTO_TITULO ( 
            P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
            P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
            P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
            P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
            P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
            P_SECCION_CAJA_TITU     IN VARCHAR2,
            P_CONCEPTO_TITU_01      IN VARCHAR2,
            P_CONCEPTO_TITU_02      IN VARCHAR2,
            P_MESSAGE               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END BWZKCSPBT;