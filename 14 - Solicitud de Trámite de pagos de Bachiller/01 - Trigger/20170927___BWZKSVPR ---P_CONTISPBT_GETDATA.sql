/**********************************************************************++
-- creación BANNIS  Y PERMISOS
/**********************************************************************/
  CREATE OR REPLACE PUBLIC SYNONYM "BWZKSVPR" FOR "BANINST1"."BWZKSVPR";
  GRANT EXECUTE ON BANINST1.BWZKSVPR TO SATURN;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

/*
SET SERVEROUTPUT ON
DECLARE P_DEPT_CODE            STVDEPT.STVDEPT_CODE%TYPE;
        P_CAMP_CODE            STVCAMP.STVCAMP_CODE%TYPE;
        P_CAMP_DESC            STVCAMP.STVCAMP_DESC%TYPE;
        P_TERM_CODE            STVTERM.STVTERM_CODE%TYPE;
BEGIN
    P_CONTISPBT_GETDATA(557036,'28/10/2016',P_DEPT_CODE,P_CAMP_CODE,P_CAMP_DESC,P_TERM_CODE); -- 43605864
    DBMS_OUTPUT.PUT_LINE('::::::::: ' || P_DEPT_CODE|| ' *** ' || P_CAMP_CODE|| ' *** ' || P_CAMP_DESC|| ' *** ' || P_TERM_CODE);
END;
*/

--## SOLICITUD DE PAGO TRAMITE BACHILLER y TITULACION (CONTISPBT) ## 
/* ===================================================================================================================
  NOMBRE    : P_CONTISPBT_GETDATA
  FECHA     : 27/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtener los datos dept, camp, nombre camp y periodo de un PIDM

  MODIFICACIONES
  NRO     FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
create or replace PROCEDURE P_CONTISPBT_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

     --> Get PERIODO, El periodo es anual para el pago del tramite de bachiller y titulacion
      P_TERM_CODE := TO_CHAR(SYSDATE, 'YYYY') || '10';

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISPBT_GETDATA;

