/******************************************************************************/
/* BWZKSPPE.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para generar el Paquete relacionado a los        */
/*                    procedimientos de Permanencia de Plan de Estudios       */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del código.                                    BFV 03/JUL/2018 */
/*    Creación del paquete de Permanencia de Plan de Estudios                 */
/*                                                                            */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante solicitante      */
/*      cumplas las condiciones para hacer uso del WF.                        */
/*    Procedure P_OBTENER_COMENTARIO: Obtiene el comentario que redacta el    */
/*      estudiante en el WF.                                                  */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el                */
/*      procedimiento obtiene LOS CORREOS del(os) responsable(s) asignados    */
/*      a dicho ROL + SEDE (ROL_SEDE).                                        */
/*    Procedure P_GET_COORDINADOR: Obtiene el rol del coordinador de carrera  */
/*      del estudiante.                                                       */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Verifica si se anulo la         */
/*      solicitud por el mismo estudiante o no.                               */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud   */
/*    Procedure P_VERIFICA_INSCRIP: Verifica que el estudiante este           */
/*      matriculado o no en el periodo solicitado.                            */
/*    Procedure P_ELIMINAR_NRC: Elimina la matricula del estudiante en el     */
/*      periodo solicitado.                                                   */
/*    Procedure P_MOD_RETENCION: Actualiza la retención del estudiante        */
/*    Procedure P_MODIFICAR_CATALOGO, P_SET_ATRIBUTO_PLANS, P_SET_CURSO_INTROD*/
/*      P_PPE_CONV_ASIGN, P_PPE_EXECCAPP y P_PPE_EXECPROY son SP del WF de    */
/*      Traslado Interno.                                                     */
/*    Procedure P_GET_ASIG_RECONOCIDAS:  Muestra el reporte de asignaturas    */
/*      al estudiante.                                                        */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/**********************************************************************/
-- Creación BANNIS  Y PERMISOS
/**********************************************************************/
--     CREATE OR REPLACE PUBLIC SYNONYM "BWZKSPPE" FOR "BANINST1"."BWZKSPPE";
--     GRANT EXECUTE ON BANINST1.BWZKSPPE TO SATURN;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY BANINST1.BWZKSPPE AS
/*
  BWZKSPPE:
       Paquete Web _ Desarrollo Propio _ Paquete _ Permanencia Plan De Estudios
*/
-- FILE NAME..: BWZKSPPE.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSPPE
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0; --VERIFICAR RETENCION 01
        V_RET02             INTEGER := 0; --VERIFICAR RETENCION 01
        V_SOLTRA            INTEGER := 0; --VERIFICAR SOLICITUD DE TRASLADO
        V_SOLRES            INTEGER := 0; --VERIFICAR SOLICITUD DE RESERVA
        V_SOLREC            INTEGER := 0; --VERIFICAR SOLICITUD DE RECTIFICACION
        V_SOLCAM            INTEGER := 0; --VERIFICAR SOLICITUD DE CAMBIO DE PLAN
        V_SOLDIR            INTEGER := 0; --VERIFICAR SOLICITUD DE ASIGNATURA DIRIGIDA
        V_RET               INTEGER := 0; --VERIFICAR RETENCION 08 Ó 16 ACTIVA
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
        
        V_CODE_DEPT         STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_CAMP         STVCAMP.STVCAMP_CODE%TYPE;
        V_PROGRAM           SORLCUR.SORLCUR_PROGRAM%TYPE;
        V_TERMCTLG          SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE;
        V_ATRIB             SGRSATT.SGRSATT_ATTS_CODE%TYPE;

    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;

BEGIN

    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*) INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################    
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE RETENCIONES 08 Ó 16 ACTIVAS
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE in ('08','16')
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No tiene activo la retención de Permanencia de Plan de Estudios. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No tiene activo la retención de Permanencia de Plan de Estudios. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
    CLOSE C_SORLCUR;

    --################################################################################################
    --- VALIDACIÓN DE ESTUDIANTES DE PROGRAMA 309 (Administración - MKT y Neg. Internacionales)
    --################################################################################################
    IF V_PROGRAM = '309' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. Por favor, acercarse a la oficina del CAU para su atención. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. Por favor, acercarse a la oficina del CAU para su atención. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE CATALOGO DE ESTUDIO 2015
    --################################################################################################
    IF '201510' <= V_TERMCTLG AND V_TERMCTLG < '201810' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en un plan de estudios vigente (Plan de Estudios 2015). ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en un plan de estudios vigente (Plan de Estudios 2015). ';
        END IF;
        V_FLAG := 1;
    END IF; 

    --################################################################################################
    ---VALIDACIÓN DE CATALOGO DE ESTUDIO 2018
    --################################################################################################
    IF '201810' <= V_TERMCTLG THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en un plan de estudios vigente (Plan de Estudios 2018). ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en un plan de estudios vigente (Plan de Estudios 2018). ';
        END IF;
        V_FLAG := 1;
    END IF; 

    --################################################################################################
    ---VALIDACIÓN DE ATRIBUTO DE PLAN P003
    --################################################################################################
    SELECT SGRSATT_ATTS_CODE
    INTO V_ATRIB
    FROM (
        SELECT SGRSATT_ATTS_CODE 
        FROM SGRSATT
            WHERE SGRSATT_PIDM = P_PIDM
            AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
            ORDER BY SGRSATT_TERM_CODE_EFF DESC
    )WHERE ROWNUM = 1;
    
    IF V_ATRIB = 'P003' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Debe solicitar proceso de CAPP Personalizado.';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || 'Debe solicitar proceso de CAPP Personalizado.';
        END IF;
        V_FLAG := 1;
    END IF; 
    
    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_OBTENER_COMENTARIO (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_COMENTARIO          OUT VARCHAR2,
        P_TELEFONO            OUT VARCHAR2
)
AS
    V_ERROR                   EXCEPTION;
    V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
    V_COMENTARIO              VARCHAR2(4000);
    V_CLAVE                   VARCHAR2(4000);
    V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
    
    -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
    CURSOR C_SVRSVAD IS
    SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
    FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
    INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
    ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
    WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
        --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
    ORDER BY SVRSVAD_ADDL_DATA_SEQ;

BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO V_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 1 THEN
                  -- OBTENER COMENTARIO DE LA SOLICITUD
                  P_COMENTARIO := V_COMENTARIO;
              ELSIF V_ADDL_DATA_SEQ = 2 THEN
                  -- OBTENER EL TELEFONO DE LA SOLICITUD 
                  P_TELEFONO := V_COMENTARIO;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;

      -- CASO ESPECIAL ******
      IF P_FOLIO_SOLICITUD = 4410 OR P_FOLIO_SOLICITUD = 4405 THEN 
            P_TELEFONO := '-';
      END IF;

      IF (P_TELEFONO IS NULL OR P_COMENTARIO IS NULL) THEN
          RAISE V_ERROR;
      END IF;
      
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_OBTENER_COMENTARIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_USUARIO (
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
        P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
        P_CORREO_USER         OUT VARCHAR2,
        P_ROL_SEDE            OUT VARCHAR2,
        P_MESSAGE             OUT VARCHAR2
)
AS
        -- @PARAMETERS
        P_ROLE_ID                 NUMBER;
        P_ORG_ID                  NUMBER;
        P_USER_ID                 NUMBER;
        P_ROLE_ASSIGNMENT_ID      NUMBER;
        P_Email_Address           VARCHAR2(100);
        
        P_SECCION_EXCEPT          VARCHAR2(50);
        
        CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
        
        V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO P_Email_Address FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_CORREO_USER := P_CORREO_USER || P_Email_Address || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      P_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_CORREO_USER,1,LENGTH(P_CORREO_USER) -1) INTO P_CORREO_USER
      FROM DUAL
      WHERE SUBSTR(P_CORREO_USER,-1,1) = ',';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_MESSAGE := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_MESSAGE := '- Se encontraron mas de una ORGANIZACIóN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_MESSAGE := '- Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_MESSAGE := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_MESSAGE := '- NO se encontrò el nombre del ROL: '  || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_MESSAGE := '- NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_MESSAGE := '- NO  se encontrÓ ningun usuario con esas caracteristicas.';
          ELSE  P_MESSAGE := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_COORDINADOR (
        P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_ROL_DC              IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a mayusculas 'coordinador'
        P_CORREO_COORD        OUT VARCHAR2,
        P_ROL_COORD           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_ROLE_ID                 NUMBER;
    V_ORG_ID                  NUMBER;
    V_INDICADOR               NUMBER;
    V_Email_Address           VARCHAR2(500);
    V_EXEPTION                EXCEPTION;
    
    V_CODE_DEPT      STVDEPT.STVDEPT_CODE%TYPE;
    V_CODE_CAMP      STVCAMP.STVCAMP_CODE%TYPE;
    V_PROGRAM        SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_TERMCTLG       SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE;
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;
      
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
      
    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;
        
BEGIN 
        --################################################################################################
        -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
        --################################################################################################
        OPEN C_SORLCUR;
        LOOP
            FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
            EXIT WHEN C_SORLCUR%NOTFOUND;
            END LOOP;
        CLOSE C_SORLCUR;

        IF P_DEPT_CODE = 'UREG' THEN
            P_ROL_COORD := P_ROL_DC || P_DEPT_CODE || V_PROGRAM || P_CAMP_CODE;
        ELSE
            P_ROL_COORD := P_ROL_DC || P_DEPT_CODE || '000' || P_CAMP_CODE;
        END IF;
        
        -- Obtener el ROL_ID 
        SELECT ID 
        INTO V_ROLE_ID 
        FROM WORKFLOW.ROLE 
        WHERE NAME = P_ROL_COORD;
        
        -- Obtener el ORG_ID 
        SELECT ID 
        INTO V_ORG_ID 
        FROM WORKFLOW.ORGANIZATION 
        WHERE NAME = 'Root';
        
        --Validar que exista el ROL
        SELECT COUNT(*)
        INTO V_INDICADOR 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;
        
        IF V_INDICADOR = 0 THEN
          RAISE V_EXEPTION;
        END if;
        
        -- #######################################################################
        OPEN C_ROLE_ASSIGNMENT;
        LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
              -- Obtener Datos Usuario
              SELECT Email_Address 
              INTO V_Email_Address 
              FROM WORKFLOW.WFUSER 
              WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
  
              P_CORREO_COORD := P_CORREO_COORD || V_Email_Address || ',';
              
        END LOOP;
        CLOSE C_ROLE_ASSIGNMENT;
        
        -- Extraer el ultimo digito en caso sea un "coma"(,)
        SELECT SUBSTR(P_CORREO_COORD,1,LENGTH(P_CORREO_COORD) -1)
        INTO P_CORREO_COORD
        FROM DUAL
        WHERE SUBSTR(P_CORREO_COORD,-1,1) = ',';

EXCEPTION
  WHEN V_EXEPTION THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No se encontro el ROL: ' || P_ROL_COORD);
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_COORDINADOR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_SOL          OUT VARCHAR2,
        P_MESSAGE             OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VERIFICAR_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_STATUS_SOL, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
        P_AN                  OUT NUMBER,
        P_MESSAGE             OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_ESTADO, P_AN, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_INSCRIP (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_INSCRIP  OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VERIFICAR_MATRICULA (P_PIDM, P_TERM_CODE, P_STATUS_INSCRIP, P_MESSAGE); 

EXCEPTION
WHEN OTHERS THEN
    P_STATUS_INSCRIP := 'FALSE';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_INSCRIP;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_ELIMINAR_NRC (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_MESSAGE         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_ELIMINAR_NRC (P_PIDM, P_TERM_CODE, P_MESSAGE); 

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_ELIMINAR_NRC;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_MODIFICAR_RETENCION_A (
      P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_MESSAGE             OUT VARCHAR2
) 
AS
BEGIN

    BWZKPSPG.P_MODIFICAR_RETENCION_A (P_PIDM, P_FOLIO_SOLICITUD, P_STVHLDD_CODE1, P_STVHLDD_CODE2, P_STVHLDD_CODE3, P_MESSAGE); 

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_RETENCION_A;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_MODIFICAR_RETENCION_B (
      P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE4       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_MESSAGE             OUT VARCHAR2
) 
AS
BEGIN

    BWZKPSPG.P_MODIFICAR_RETENCION_B (P_PIDM, P_FOLIO_SOLICITUD, P_STVHLDD_CODE1, P_STVHLDD_CODE2, P_STVHLDD_CODE3, P_STVHLDD_CODE4, P_MESSAGE); 

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_RETENCION_B;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_MODIFICAR_CATALOGO (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN 
    BWZKPSPG.P_MODIFICAR_CATALOGO (P_PIDM, P_TERM_CODE, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_CATALOGO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_ATRIBUTO_PLANS (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,
        P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN
    BWZKPSPG.P_SET_ATRIBUTO_PLAN (P_PIDM, P_TERM_CODE, P_ATTS_CODE, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_ATRIBUTO_PLANS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CURSO_INTROD (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
)
AS
    V_STATUS_DATE_AP      SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
BEGIN

    -- Get la fecha de aprovación de la solicitud
    SELECT SVRSVPR_STATUS_DATE
    INTO V_STATUS_DATE_AP
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD 
        AND SVRSVPR_SRVS_CODE = 'AP';

    BWZKPSPG.P_SET_CURSO_INTROD (P_PIDM, P_TERM_CODE, V_STATUS_DATE_AP, P_DEPT_CODE, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CURSO_INTROD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_PPE_CONV_ASIGN(
        P_TERM_CODE     IN STVTERM.STVTERM_CODE%TYPE,
        P_CATALOGO      IN STVTERM.STVTERM_CODE%TYPE,
        P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_MESSAGE       OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CONV_ASIGN (P_TERM_CODE, P_CATALOGO, P_PIDM, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_PPE_CONV_ASIGN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_PPE_EXECCAPP (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_EXECCAPP (P_PIDM, P_TERM_CODE, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_PPE_EXECCAPP;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_PPE_EXECPROY (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_EXECPROY (P_PIDM, P_TERM_CODE, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_PPE_EXECPROY;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_ASIG_RECONOCIDAS (
    P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_HTML1         OUT VARCHAR2,
    P_HTML2         OUT VARCHAR2,
    P_HTML3         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_GET_ASIG_RECONOCIDAS (P_PIDM, P_HTML1, P_HTML2, P_HTML3);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ASIG_RECONOCIDAS;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSPPE;

/**********************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**********************************************************************************************/