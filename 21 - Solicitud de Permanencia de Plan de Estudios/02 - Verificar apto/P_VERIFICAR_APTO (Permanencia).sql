/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_REASON VARCHAR2(4000);
BEGIN
    P_VERIFICAR_APTO(87313, '201810', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0; --VERIFICAR RETENCION 01
        V_RET02             INTEGER := 0; --VERIFICAR RETENCION 01
        V_SOLTRA            INTEGER := 0; --VERIFICAR SOLICITUD DE TRASLADO
        V_SOLRES            INTEGER := 0; --VERIFICAR SOLICITUD DE RESERVA
        V_SOLREC            INTEGER := 0; --VERIFICAR SOLICITUD DE RECTIFICACION
        V_SOLCAM            INTEGER := 0; --VERIFICAR SOLICITUD DE CAMBIO DE PLAN
        V_SOLDIR            INTEGER := 0; --VERIFICAR SOLICITUD DE ASIGNATURA DIRIGIDA
        V_RET               INTEGER := 0; --VERIFICAR RETENCION 08 Ó 16 ACTIVA
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
        
        V_CODE_DEPT         STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_CAMP         STVCAMP.STVCAMP_CODE%TYPE;
        V_PROGRAM           SORLCUR.SORLCUR_PROGRAM%TYPE;
        V_TERMCTLG          SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE;
        V_ATRIB             SGRSATT.SGRSATT_ATTS_CODE%TYPE;

    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;

BEGIN

    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*) INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################    
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE RETENCIONES 08 Ó 16 ACTIVAS
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE in ('08','16')
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No tiene activo la retención de Permanencia de Plan de Estudios. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No tiene activo la retención de Permanencia de Plan de Estudios. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
    CLOSE C_SORLCUR;

    --################################################################################################
    --- VALIDACIÓN DE ESTUDIANTES DE PROGRAMA 309 (Administración - MKT y Neg. Internacionales)
    --################################################################################################
    IF V_PROGRAM = '309' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. Por favor, acercarse a la oficina del CAU para su atención. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. Por favor, acercarse a la oficina del CAU para su atención. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE CATALOGO DE ESTUDIO 2015
    --################################################################################################
    IF '201510' <= V_TERMCTLG AND V_TERMCTLG < '201810' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en un plan de estudios vigente (Plan de Estudios 2015). ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en un plan de estudios vigente (Plan de Estudios 2015). ';
        END IF;
        V_FLAG := 1;
    END IF; 

    --################################################################################################
    ---VALIDACIÓN DE CATALOGO DE ESTUDIO 2018
    --################################################################################################
    IF '201810' <= V_TERMCTLG THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en un plan de estudios vigente (Plan de Estudios 2018). ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en un plan de estudios vigente (Plan de Estudios 2018). ';
        END IF;
        V_FLAG := 1;
    END IF; 

    --################################################################################################
    ---VALIDACIÓN DE ATRIBUTO DE PLAN P003
    --################################################################################################
    SELECT SGRSATT_ATTS_CODE
    INTO V_ATRIB
    FROM (
        SELECT SGRSATT_ATTS_CODE 
        FROM SGRSATT
            WHERE SGRSATT_PIDM = P_PIDM
            AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
            ORDER BY SGRSATT_TERM_CODE_EFF DESC
    )WHERE ROWNUM = 1;
    
    IF V_ATRIB = 'P003' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Debe solicitar proceso de CAPP Personalizado.';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || 'Debe solicitar proceso de CAPP Personalizado.';
        END IF;
        V_FLAG := 1;
    END IF; 
    
    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;