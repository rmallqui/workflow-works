/******************************************************************************/
/* BWZKSPPE.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para generar el Paquete relacionado a los        */
/*                    procedimientos de Permanencia de Plan de Estudios       */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del código.                                    BFV 03/JUL/2018 */
/*    Creación del paquete de Permanencia de Plan de Estudios                 */
/*                                                                            */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante solicitante      */
/*      cumplas las condiciones para hacer uso del WF.                        */
/*    Procedure P_OBTENER_COMENTARIO: Obtiene el comentario que redacta el    */
/*      estudiante en el WF.                                                  */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el                */
/*      procedimiento obtiene LOS CORREOS del(os) responsable(s) asignados    */
/*      a dicho ROL + SEDE (ROL_SEDE).                                        */
/*    Procedure P_GET_COORDINADOR: Obtiene el rol del coordinador de carrera  */
/*      del estudiante.                                                       */
/*    --Paquete generico                                                      */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Verifica si se anulo la         */
/*      solicitud por el mismo estudiante o no.                               */
/*    --Paquete generico                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud   */
/*    Procedure P_VERIFICA_INSCRIP: Verifica que el estudiante este           */
/*      matriculado o no en el periodo solicitado.                            */
/*    --Paquete generico                                                      */
/*    Procedure P_ELIMINAR_NRC: Elimina la matricula del estudiante en el     */
/*      periodo solicitado.                                                   */
/*    Procedure P_MOD_RETENCION: Actualiza la retención del estudiante        */
/*    --Paquete generico                                                      */
/*    Procedure P_MODIFICAR_CATALOGO, P_SET_ATRIBUTO_PLANS, P_SET_CURSO_INTROD*/
/*      P_PPE_CONV_ASIGN, P_PPE_EXECCAPP y P_PPE_EXECPROY son SP del WF de    */
/*      Traslado Interno.                                                     */
/*    --Paquete generico                                                      */
/*    Procedure P_GET_ASIG_RECONOCIDAS:  Muestra el reporte de asignaturas    */
/*      al estudiante.                                                        */
/*    --------------------                                                    */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BANINST1.BWZKSPPE AS
/*
 BWZKSPPE:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti Solicitud Permanencia Plan De Estudios
*/
-- FILE NAME..: BWZKSPPE.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSPPE
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_VERIFICAR_APTO (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO       OUT VARCHAR2,
        P_REASON            OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_COMENTARIO          OUT VARCHAR2,
        P_TELEFONO            OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO (
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
        P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
        P_CORREO_USER         OUT VARCHAR2,
        P_ROL_SEDE            OUT VARCHAR2,
        P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_COORDINADOR (
        P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_ROL_DC              IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a mayusculas 'coordinador'
        P_CORREO_COORD        OUT VARCHAR2,
        P_ROL_COORD           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_SOL          OUT VARCHAR2,
        P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
        P_AN                  OUT NUMBER,
        P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_INSCRIP (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_INSCRIP  OUT VARCHAR2,
        P_MESSAGE         OUT VARCHAR2
);
    
--------------------------------------------------------------------------------

PROCEDURE P_ELIMINAR_NRC (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_RETENCION_A (
      P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_RETENCION_B (
      P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE4       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_CATALOGO (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_MESSAGE           OUT VARCHAR2
);
        
--------------------------------------------------------------------------------

PROCEDURE P_SET_ATRIBUTO_PLANS (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,
        P_MESSAGE           OUT VARCHAR2
);

-------------------------------------------------------------------------------

PROCEDURE P_SET_CURSO_INTROD (
        P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
        P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_PPE_CONV_ASIGN(
        P_TERM_CODE     IN STVTERM.STVTERM_CODE%TYPE,
        P_CATALOGO      IN STVTERM.STVTERM_CODE%TYPE,
        P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_MESSAGE       OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_PPE_EXECCAPP (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_PPE_EXECPROY (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_ASIG_RECONOCIDAS (
        P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_HTML1         OUT VARCHAR2,
        P_HTML2         OUT VARCHAR2,
        P_HTML3         OUT VARCHAR2
);

END BWZKSPPE;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSPPE;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSPPE FOR BANINST1.BWZKSPPE;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSPPE
--  START gurgrth BWZKSPPE
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/