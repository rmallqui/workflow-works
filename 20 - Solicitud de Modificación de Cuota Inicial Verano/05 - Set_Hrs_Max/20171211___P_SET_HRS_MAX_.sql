/* ===================================================================================================================
  NOMBRE    : P_SET_HRS_MAX
  FECHA     : 11/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Actualiza las horas máximas(créditos) a matricular y el monto de la C01 a pagar si las opciones son 
              entre 1 al 3 créditos, si la opción elegida es 4 - 12 créditos, actualiza la primera C01 a en base a 4
              créditos y las horas máximas se actualizan a 12 créditos.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */

CREATE OR REPLACE PROCEDURE P_SET_HRS_MAX (

    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
 AS
    V_INDICADOR   NUMBER;
    V_TERM_CODE   STVTERM.STVTERM_CODE%TYPE;  

 BEGIN
   
    -- VERIFICAR SI TIENE REGISTROS DE HORAS MÍNINAS Y MÁXIMAS EN EL PERIODO ACTIVO 
    SELECT COUNT(SFBETRM_VERSION) INTO V_INDICADOR
    FROM SFBETRM
    WHERE SFBETRM_PIDM = P_PIDM
          AND SFBETRM_TERM_CODE = P_TERM_CODE;


    IF P_CREDITOS_CODE <= 3 AND V_INDICADOR = 0 THEN

      INSERT INTO SFBETRM (SFBETRM_TERM_CODE,
                           SFBETRM_PIDM,
                           SFBETRM_ESTS_CODE,
                           SFBETRM_ESTS_DATE,
                           SFBETRM_MHRS_OVER,
                           SFBETRM_AR_IND,
                           SFBETRM_ASSESSMENT_DATE,
                           SFBETRM_ADD_DATE,
                           SFBETRM_ACTIVITY_DATE,
                           SFBETRM_RGRE_CODE,
                           SFBETRM_TMST_CODE,
                           SFBETRM_TMST_DATE,
                           SFBETRM_TMST_MAINT_IND,
                           SFBETRM_USER,
                           SFBETRM_REFUND_DATE,
                           SFBETRM_DATA_ORIGIN,
                           SFBETRM_INITIAL_REG_DATE,
                           SFBETRM_MIN_HRS,
                           SFBETRM_MINH_SRCE_CDE,
                           SFBETRM_MAXH_SRCE_CDE
                           )

      VALUES (P_TERM_CODE,
              P_PIDM,
              'EL',
              SYSDATE,
              P_CREDITOS_CODE,
              'N',
              SYSDATE,
              SYSDATE,
              SYSDATE,
              NULL,
              'CI',
              SYSDATE,
              'S',
              'WFAUTO',
              SYSDATE,
              'BANNER',
              SYSDATE,
              0,
              'M',
              'U'
        );

      COMMIT;

    ELSIF P_CREDITOS_CODE <= 3 AND V_INDICADOR = 1 THEN

      UPDATE SFBETRM
        SET SFBETRM_MHRS_OVER = P_CREDITOS_CODE, 
          SFBETRM_ASSESSMENT_DATE = SYSDATE, 
          SFBETRM_ACTIVITY_DATE = SYSDATE,
          SFBETRM_USER ='WFAUTO',
          SFBETRM_REFUND_DATE = SYSDATE 
      WHERE SFBETRM_TERM_CODE = P_TERM_CODE
            AND SFBETRM_PIDM = P_PIDM;

    COMMIT;

    ELSIF P_CREDITOS_CODE > 3 AND V_INDICADOR = 0 THEN

      INSERT INTO SFBETRM (SFBETRM_TERM_CODE,
                           SFBETRM_PIDM,
                           SFBETRM_ESTS_CODE,
                           SFBETRM_ESTS_DATE,
                           SFBETRM_MHRS_OVER,
                           SFBETRM_AR_IND,
                           SFBETRM_ASSESSMENT_DATE,
                           SFBETRM_ADD_DATE,
                           SFBETRM_ACTIVITY_DATE,
                           SFBETRM_RGRE_CODE,
                           SFBETRM_TMST_CODE,
                           SFBETRM_TMST_DATE,
                           SFBETRM_TMST_MAINT_IND,
                           SFBETRM_USER,
                           SFBETRM_REFUND_DATE,
                           SFBETRM_DATA_ORIGIN,
                           SFBETRM_INITIAL_REG_DATE,
                           SFBETRM_MIN_HRS,
                           SFBETRM_MINH_SRCE_CDE,
                           SFBETRM_MAXH_SRCE_CDE)

      VALUES (P_TERM_CODE,
              P_PIDM,
              'EL',
              SYSDATE,
              12,
              'N',
              SYSDATE,
              SYSDATE,
              SYSDATE,
              NULL,
              'CI',
              SYSDATE,
              'S',
              'WFAUTO',
              SYSDATE,
              'BANNER',
              SYSDATE,
              0,
              'M',
              'U'
        );

      COMMIT;

    ELSE

      UPDATE SFBETRM
        SET SFBETRM_MHRS_OVER = 12, 
          SFBETRM_ASSESSMENT_DATE = SYSDATE, 
          SFBETRM_ACTIVITY_DATE = SYSDATE,
          SFBETRM_USER ='WFAUTO',
          SFBETRM_REFUND_DATE = SYSDATE 
      WHERE SFBETRM_TERM_CODE = P_TERM_CODE
            AND SFBETRM_PIDM = P_PIDM;
     COMMIT;
    END IF; 
    
    EXCEPTION
      WHEN OTHERS THEN
        P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_SET_HRS_MAX;