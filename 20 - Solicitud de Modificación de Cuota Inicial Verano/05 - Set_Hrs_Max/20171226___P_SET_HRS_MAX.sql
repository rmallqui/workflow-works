/* ===================================================================================================================
  NOMBRE    : P_SET_HRS_MAX
  FECHA     : 11/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Actualiza las horas máximas(créditos) a matricular y el monto de la C01 a pagar si las opciones son 
              entre 1 al 3 créditos, si la opción elegida es 4 - 12 créditos, actualiza la primera C01 a en base a 4
              créditos y las horas máximas se actualizan a 12 créditos.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */

CREATE OR REPLACE PROCEDURE P_SET_HRS_MAX (

    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
 AS
    V_DEPT_CODE   STVDEPT.STVDEPT_CODE%TYPE;
    V_INDICADOR   NUMBER;
    V_TERM_CODE   STVTERM.STVTERM_CODE%TYPE;
    V_ERROR       EXCEPTION;


    -- GET DEPARTMENTO ('UREG','UPGT','UVIR')
    CURSOR GET_DEPT IS
        SELECT    SORLFOS_DEPT_CODE  
      FROM (
            SELECT  SORLFOS_DEPT_CODE
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM        =   P_PIDM
                AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                AND SORLCUR_CURRENT_CDE =   'Y'
            ORDER BY SORLCUR_TERM_CODE DESC,SORLCUR_SEQNO DESC 
    ) WHERE ROWNUM = 1;

 BEGIN
    
    -- GET DEPT OR LEARNER
    OPEN GET_DEPT;
    LOOP
      FETCH GET_DEPT INTO V_DEPT_CODE;
        EXIT WHEN GET_DEPT%NOTFOUND;
    END LOOP;
    CLOSE GET_DEPT;


    -- VERIFICAR SI TIENE REGISTROS DE HORAS MÍNINAS Y MÁXIMAS EN EL PERIODO ACTIVO 
    SELECT COUNT(SFBETRM_VERSION) INTO V_INDICADOR
    FROM SFBETRM
    WHERE SFBETRM_PIDM = P_PIDM
          AND SFBETRM_TERM_CODE = P_TERM_CODE;


    IF V_DEPT_CODE = 'UREG' THEN

        IF P_CREDITOS_CODE <= 3 AND V_INDICADOR = 0 THEN

          INSERT INTO SFBETRM (SFBETRM_TERM_CODE,
                           SFBETRM_PIDM,
                           SFBETRM_ESTS_CODE,
                           SFBETRM_ESTS_DATE,
                           SFBETRM_MHRS_OVER,
                           SFBETRM_AR_IND,
                           SFBETRM_ASSESSMENT_DATE,
                           SFBETRM_ADD_DATE,
                           SFBETRM_ACTIVITY_DATE,
                           SFBETRM_RGRE_CODE,
                           SFBETRM_TMST_CODE,
                           SFBETRM_TMST_DATE,
                           SFBETRM_TMST_MAINT_IND,
                           SFBETRM_USER,
                           SFBETRM_REFUND_DATE,
                           SFBETRM_DATA_ORIGIN,
                           SFBETRM_INITIAL_REG_DATE,
                           SFBETRM_MIN_HRS,
                           SFBETRM_MINH_SRCE_CDE,
                           SFBETRM_MAXH_SRCE_CDE
                           )

          VALUES (P_TERM_CODE,
                P_PIDM,
                'EL',
                SYSDATE,
                P_CREDITOS_CODE,
                'N',
                SYSDATE,
                SYSDATE,
                SYSDATE,
                NULL,
                'CI',
                SYSDATE,
                'S',
                'WFAUTO',
                SYSDATE,
                'BANNER',
                SYSDATE,
                0,
                'M',
               'U');

          COMMIT;

        ELSIF P_CREDITOS_CODE <= 3 AND V_INDICADOR = 1 THEN

          UPDATE SFBETRM
            SET SFBETRM_MHRS_OVER = P_CREDITOS_CODE, 
            SFBETRM_ASSESSMENT_DATE = SYSDATE, 
            SFBETRM_ACTIVITY_DATE = SYSDATE,
            SFBETRM_USER ='WFAUTO',
            SFBETRM_REFUND_DATE = SYSDATE 
          WHERE SFBETRM_TERM_CODE = P_TERM_CODE
                AND SFBETRM_PIDM = P_PIDM;

          COMMIT;

        ELSIF P_CREDITOS_CODE > 3 AND V_INDICADOR = 0 THEN

          INSERT INTO SFBETRM (SFBETRM_TERM_CODE,
                           SFBETRM_PIDM,
                           SFBETRM_ESTS_CODE,
                           SFBETRM_ESTS_DATE,
                           SFBETRM_MHRS_OVER,
                           SFBETRM_AR_IND,
                           SFBETRM_ASSESSMENT_DATE,
                           SFBETRM_ADD_DATE,
                           SFBETRM_ACTIVITY_DATE,
                           SFBETRM_RGRE_CODE,
                           SFBETRM_TMST_CODE,
                           SFBETRM_TMST_DATE,
                           SFBETRM_TMST_MAINT_IND,
                           SFBETRM_USER,
                           SFBETRM_REFUND_DATE,
                           SFBETRM_DATA_ORIGIN,
                           SFBETRM_INITIAL_REG_DATE,
                           SFBETRM_MIN_HRS,
                           SFBETRM_MINH_SRCE_CDE,
                           SFBETRM_MAXH_SRCE_CDE)

          VALUES (P_TERM_CODE,
                  P_PIDM,
                  'EL',
                  SYSDATE,
                  12,
                  'N',
                  SYSDATE,
                  SYSDATE,
                  SYSDATE,
                  NULL,
                  'CI',
                  SYSDATE,
                  'S',
                  'WFAUTO',
                  SYSDATE,
                  'BANNER',
                  SYSDATE,
                  0,
                  'M',
                  'U');

          COMMIT;

        ELSE

          UPDATE SFBETRM
          SET SFBETRM_MHRS_OVER = 12, 
            SFBETRM_ASSESSMENT_DATE = SYSDATE, 
            SFBETRM_ACTIVITY_DATE = SYSDATE,
            SFBETRM_USER ='WFAUTO',
            SFBETRM_REFUND_DATE = SYSDATE 
          WHERE SFBETRM_TERM_CODE = P_TERM_CODE
            AND SFBETRM_PIDM = P_PIDM;
          COMMIT;
        END IF;

    ELSIF V_DEPT_CODE IN ('UPGT','UVIR') THEN
      
      IF P_CREDITOS_CODE <= 5 AND V_INDICADOR = 0 THEN

          INSERT INTO SFBETRM (SFBETRM_TERM_CODE,
                           SFBETRM_PIDM,
                           SFBETRM_ESTS_CODE,
                           SFBETRM_ESTS_DATE,
                           SFBETRM_MHRS_OVER,
                           SFBETRM_AR_IND,
                           SFBETRM_ASSESSMENT_DATE,
                           SFBETRM_ADD_DATE,
                           SFBETRM_ACTIVITY_DATE,
                           SFBETRM_RGRE_CODE,
                           SFBETRM_TMST_CODE,
                           SFBETRM_TMST_DATE,
                           SFBETRM_TMST_MAINT_IND,
                           SFBETRM_USER,
                           SFBETRM_REFUND_DATE,
                           SFBETRM_DATA_ORIGIN,
                           SFBETRM_INITIAL_REG_DATE,
                           SFBETRM_MIN_HRS,
                           SFBETRM_MINH_SRCE_CDE,
                           SFBETRM_MAXH_SRCE_CDE
                           )

          VALUES (P_TERM_CODE,
                P_PIDM,
                'EL',
                SYSDATE,
                P_CREDITOS_CODE,
                'N',
                SYSDATE,
                SYSDATE,
                SYSDATE,
                NULL,
                'CI',
                SYSDATE,
                'S',
                'WFAUTO',
                SYSDATE,
                'BANNER',
                SYSDATE,
                0,
                'M',
               'U');

          COMMIT;

        ELSIF P_CREDITOS_CODE <= 5 AND V_INDICADOR = 1 THEN

          UPDATE SFBETRM
            SET SFBETRM_MHRS_OVER = P_CREDITOS_CODE, 
            SFBETRM_ASSESSMENT_DATE = SYSDATE, 
            SFBETRM_ACTIVITY_DATE = SYSDATE,
            SFBETRM_USER ='WFAUTO',
            SFBETRM_REFUND_DATE = SYSDATE 
          WHERE SFBETRM_TERM_CODE = P_TERM_CODE
                AND SFBETRM_PIDM = P_PIDM;

          COMMIT;

        ELSIF P_CREDITOS_CODE > 5 AND V_INDICADOR = 0 THEN

          INSERT INTO SFBETRM (SFBETRM_TERM_CODE,
                           SFBETRM_PIDM,
                           SFBETRM_ESTS_CODE,
                           SFBETRM_ESTS_DATE,
                           SFBETRM_MHRS_OVER,
                           SFBETRM_AR_IND,
                           SFBETRM_ASSESSMENT_DATE,
                           SFBETRM_ADD_DATE,
                           SFBETRM_ACTIVITY_DATE,
                           SFBETRM_RGRE_CODE,
                           SFBETRM_TMST_CODE,
                           SFBETRM_TMST_DATE,
                           SFBETRM_TMST_MAINT_IND,
                           SFBETRM_USER,
                           SFBETRM_REFUND_DATE,
                           SFBETRM_DATA_ORIGIN,
                           SFBETRM_INITIAL_REG_DATE,
                           SFBETRM_MIN_HRS,
                           SFBETRM_MINH_SRCE_CDE,
                           SFBETRM_MAXH_SRCE_CDE)

          VALUES (P_TERM_CODE,
                  P_PIDM,
                  'EL',
                  SYSDATE,
                  12,
                  'N',
                  SYSDATE,
                  SYSDATE,
                  SYSDATE,
                  NULL,
                  'CI',
                  SYSDATE,
                  'S',
                  'WFAUTO',
                  SYSDATE,
                  'BANNER',
                  SYSDATE,
                  0,
                  'M',
                  'U');

          COMMIT;

        ELSE

          UPDATE SFBETRM
          SET SFBETRM_MHRS_OVER = 12, 
            SFBETRM_ASSESSMENT_DATE = SYSDATE, 
            SFBETRM_ACTIVITY_DATE = SYSDATE,
            SFBETRM_USER ='WFAUTO',
            SFBETRM_REFUND_DATE = SYSDATE 
          WHERE SFBETRM_TERM_CODE = P_TERM_CODE
            AND SFBETRM_PIDM = P_PIDM;
          COMMIT;
        END IF;

    ELSE
      RAISE V_ERROR;
    END IF;
    
    EXCEPTION
      WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - Código de Departamento inválido.');
      WHEN OTHERS THEN
        P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_HRS_MAX;