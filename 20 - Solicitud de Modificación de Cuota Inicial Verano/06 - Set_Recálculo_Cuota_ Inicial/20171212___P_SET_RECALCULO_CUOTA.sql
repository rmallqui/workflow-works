/* ===================================================================================================================
  NOMBRE    : P_SET_RECALCULO_CUOTA
  FECHA     : 11/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Actualizar el monto de la C01 en base a la cantidad de créditos que soliciten. 
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */



CREATE OR REPLACE PROCEDURE P_SET_RECALCULO_CUOTA (

    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
 AS
    ------------------------ APEC PARAMETERS ----------------------------
    P_PART_PERIODO          VARCHAR2(9);
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    P_APEC_IDSECCIONC       VARCHAR2(15);
    P_APEC_IDALUMNO         VARCHAR2(10);
    ---------------------------------------------------------------------
    V_CAMP_CODE             SORLCUR.SORLCUR_CAMP_CODE%type;    -- STVRATE
    V_DEPT_CODE             SORLFOS.SORLFOS_DEPT_CODE%type;
    V_PROGRAM               SORLCUR.SORLCUR_PROGRAM%type;
    V_FECHA                 DATE := SYSDATE;
    V_INDICADOR             NUMBER;
    V_MESSAGE               EXCEPTION;
    P_RESULT                INTEGER;

 BEGIN
    
    -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
            SELECT    SORLCUR_CAMP_CODE,        
                      SORLFOS_DEPT_CODE,
                      SORLCUR_PROGRAM
            INTO      V_CAMP_CODE,
                      V_DEPT_CODE,
                      V_PROGRAM
            FROM (
                    SELECT    SORLCUR_CAMP_CODE,    SORLFOS_DEPT_CODE,    SORLCUR_PROGRAM
                    FROM SORLCUR        INNER JOIN SORLFOS
                          ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                          AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
                    WHERE   SORLCUR_PIDM        =   P_PIDM
                        AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                        AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                        AND SORLCUR_CURRENT_CDE = 'Y'
                    ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
            ) WHERE ROWNUM <= 1;
    
    -- GET CRONOGRAMA SECCIONC
            SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                      WHEN 'UPGT' THEN 'W' 
                                      WHEN 'UREG' THEN 'R' 
                                      WHEN 'UPOS' THEN '-' 
                                      WHEN 'ITEC' THEN '-' 
                                      WHEN 'UCIC' THEN '-' 
                                      WHEN 'UCEC' THEN '-' 
                                      WHEN 'ICEC' THEN '-' 
                                      ELSE '1' END ||
                    CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                      WHEN 'F01' THEN 'A' 
                                      WHEN 'F02' THEN 'L' 
                                      WHEN 'F03' THEN 'C' 
                                      WHEN 'V00' THEN 'V' 
                                      ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = V_DEPT_CODE AND STVCAMP_CODE = V_CAMP_CODE;
            
            SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = V_CAMP_CODE;-- CAMPUS 
            SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
            SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = V_DEPT_CODE;-- DEPARTAMENTO
            
            -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_IDSECCIONC
            WITH 
                CTE_tblSeccionC AS (
                        -- GET SECCIONC
                        SELECT  "IDSeccionC" IDSeccionC,
                                "FecInic" FecInic
                        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                        WHERE "IDDependencia"='UCCI'
                        AND "IDsede"    = V_APEC_CAMP
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuela" = V_PROGRAM
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                        AND "IDSeccionC" <> '15NEX1A'
                        AND SUBSTRB("IDSeccionC",-2,2) IN (
                            -- PARTE PERIODO           
                            SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                        )
                ),
                CTE_tblPersonaAlumno AS (
                        SELECT "IDAlumno" IDAlumno 
                        FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                        WHERE "IDPersona" IN ( 
                            SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM
                        )
                )
            SELECT "IDAlumno", "IDSeccionC" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
            FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
            AND "IDSede"      = V_APEC_CAMP
            AND "IDPerAcad"   = V_APEC_TERM
            AND "IDEscuela"   = V_PROGRAM
            AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
            AND "IDConcepto"  = 'C01'; -- CUALQUIER CONCEPTO seguro SOLO PARA OBTENER LOS DATOS IDALUMNO Y SECCIONC
  
        SELECT COUNT (P_APEC_IDALUMNO) INTO V_INDICADOR
        FROM dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDSeccionC" = P_APEC_IDSECCIONC
        AND "IDSede" = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDAlumno" = P_APEC_IDALUMNO;

        IF V_INDICADOR = 0 THEN -- IDAlumno IDPerAcad IDDependencia IDSede  IDSeccionC  ncredWF FechaCalc

          IF P_CREDITOS_CODE <> 6 THEN
            INSERT INTO dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE VALUES (P_APEC_IDALUMNO, V_APEC_TERM, 'UCCI', V_APEC_CAMP, P_APEC_IDSECCIONC, '5,5', V_FECHA);
            COMMIT; 

          ELSE
            INSERT INTO dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE VALUES (P_APEC_IDALUMNO, V_APEC_TERM, 'UCCI', V_APEC_CAMP, P_APEC_IDSECCIONC, P_CREDITOS_CODE, V_FECHA);
            COMMIT;

          END IF;

        ELSIF V_INDICADOR = 1 THEN

          IF P_CREDITOS_CODE = 6 THEN
            UPDATE dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE
            SET "ncredWF" = '5,5',
                "FechaCalc" = V_FECHA
            WHERE "IDDependencia" = 'UCCI'
            AND "IDSeccionC" = P_APEC_IDSECCIONC
            AND "IDSede" = V_APEC_CAMP
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDAlumno" = P_APEC_IDALUMNO;
            COMMIT;

          ELSE
            UPDATE dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE
            SET "ncredWF" = P_CREDITOS_CODE,
                "FechaCalc" = V_FECHA
            WHERE "IDDependencia" = 'UCCI'
            AND "IDSeccionC" = P_APEC_IDSECCIONC
            AND "IDSede" = V_APEC_CAMP
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDAlumno" = P_APEC_IDALUMNO;
            COMMIT;
          END IF;

        ELSE 
           RAISE V_MESSAGE;
        END IF;

        -- ACTUALIZAR CUENTA CORRIENTE DE VERANO

        P_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
                  'dbo.sp_ActualizarMontoPagarBanner "'
                  || 'UCCI' ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_APEC_TERM ||'" , "'|| P_APEC_IDALUMNO ||'"' 
            );
  
  EXCEPTION
  WHEN V_MESSAGE THEN
        P_MESSAGE := 'Tiene doble registro para este periodo en la tabla';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE);
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_RECALCULO_CUOTA;