/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 06/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Obtener la cantidad de créditos que el estudiante desea inscribir para el periodo verano.  
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */


CREATE OR REPLACE PROCEDURE P_GET_STUDENT_INPUTS (
      P_FOLIO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_CREDITOS_CODE        OUT SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
      P_CREDITOS_DESC        OUT SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE
)

AS
      V_ERROR                   EXCEPTION;

BEGIN

      SELECT SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
      INTO P_CREDITOS_CODE, P_CREDITOS_DESC
      FROM SVRSVAD 
           INNER JOIN SVRSRAD 
           ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
           WHERE SVRSRAD_SRVC_CODE = 'SOL020'
           AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
           ORDER BY SVRSVAD_ADDL_DATA_SEQ;

      IF P_CREDITOS_CODE IS NULL THEN
          RAISE V_ERROR;
      END IF;
      
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el identificador de la solicitud.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;
