/**********************************************************************************************/
/* SZVMCIV.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar la vista SZVMCIV                                    */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        LAM           06/DIC/2017   */
/*    --------------------                                                                    */
/*    Se crea la vista SZVMCIV para elegir los créditos aptos a inscripción para el ciclo     */
/*    verano entre el rago de 1 a 4 créditos.                                                 */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/ 

CREATE OR REPLACE VIEW "BANINST1".SZVMCIV 
(
  "CONFIRM_CRED_ID", 
  "CONFIRM_CRED_DESC"
) AS 
SELECT 1 SZVMCIV_CRED_ID, '1 crédito' SZVMCIV_CRED_DESC FROM DUAL UNION
SELECT 2, '2 créditos' FROM DUAL UNION
SELECT 3, '3 créditos' FROM DUAL UNION
SELECT 4, '4 - 12 créditos' FROM DUAL;
