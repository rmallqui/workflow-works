/**********************************************************************************************/
/* BWZKSMCV.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Modificación de Cuota Inicial Verano                       */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        LAM             06/DIC/2017 */
/*    --------------------                                                                    */
/*      Creación del paquete de Modificación de Cuota Inicial - Verano                        */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene los datos de AutoServicio ingresados            */
/*              por el estudiante concepto del idioma y si cumple con el requisito.           */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud.                  */
/*    Procedure P_VERIFICAR_CTA_CTE: Verifica si tiene cta cte creada en el periodo académico */
/*              requerido                                                                     */
/*    Procedure P_SET_HRS_MAX: Actualiza las horas máximas(créditos) en el SFAREGS            */
/*    Procedure P_SET_RECALCULO_CUOTA: Actualizar el monto de la C01 en base a los créditos   */
/*                solicitados                                                                 */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/

/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BODY BWZKSMCV AS

PROCEDURE P_GET_STUDENT_INPUTS (
    P_FOLIO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_CREDITOS_CODE        OUT SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_CREDITOS_DESC        OUT SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE
)
AS
      V_ERROR                   EXCEPTION;
BEGIN
    SELECT SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        INTO P_CREDITOS_CODE, P_CREDITOS_DESC
    FROM SVRSVAD
    INNER JOIN SVRSRAD ON
        SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
    WHERE SVRSRAD_SRVC_CODE = 'SOL020'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
    ORDER BY SVRSVAD_ADDL_DATA_SEQ;
    
    IF P_CREDITOS_CODE IS NULL THEN
        RAISE V_ERROR;
    END IF;
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el identificador de la solicitud.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_ESTADO, P_AN, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_CTA_CTE (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO       IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_COD_SEDE        IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CTA_CTE         OUT VARCHAR2,
    P_DESCRIP_CTA_CTE OUT VARCHAR2,
    P_ERROR           OUT VARCHAR2
)
AS
    V_ERROR       EXCEPTION;
    V_CONTADOR    NUMBER;
    V_APEC_TERM   VARCHAR2(10);
    V_APEC_CAMP   VARCHAR2(10);
BEGIN
    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;
    
    -- CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_COD_SEDE;
    
    WITH
    CTE_tblCronogramas AS (
        -- GET Cronogramas activos
        SELECT "IDSede" IDSede, "Cronograma" Cronograma
        FROM tblCronogramas@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "Activo" = '1'
            AND "Pronabec" = '0'
            AND "IDDepartamento" = P_DEPT_CODE
            AND "IDCampus" = P_COD_SEDE
    ),
    CTE_tblCtaCorriente AS (
        -- GET Cuenta Corriente
        SELECT DISTINCT "IDAlumno" IDAlumno, "IDSede" IDSede, "IDSeccionC" IDSeccionC
        FROM  tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDSede" = V_APEC_CAMP
            AND "IDPerAcad" = V_APEC_TERM
            AND LENGTH("IDSeccionC") = 5
            AND "IDConcepto" in ('C00','C01','C02')
            AND "IDAlumno" = P_ID_ALUMNO
    )
    SELECT COUNT(IDAlumno)
        INTO V_CONTADOR
    FROM CTE_tblCtaCorriente cc
    INNER JOIN CTE_tblCronogramas cr ON
        cc.IDSede = cr.IDSede
        AND SUBSTR(cc.IDSeccionC,-2,2) = cr.Cronograma;
    
    IF V_CONTADOR > 0 THEN
        P_CTA_CTE := '1';
        P_DESCRIP_CTA_CTE := 'OK';
    ELSE
        P_CTA_CTE := '0';
        P_DESCRIP_CTA_CTE := 'No tiene cuenta corriente creada.';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_CTA_CTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_HRS_MAX (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
AS
    V_DEPT_CODE   STVDEPT.STVDEPT_CODE%TYPE;
    V_INDICADOR   NUMBER;
    V_TERM_CODE   STVTERM.STVTERM_CODE%TYPE;
    V_CRED_MAX    NUMBER := 11;
    V_CREDB_PRE   NUMBER := 4;
    V_CREDB_SEM   NUMBER := 5;
    V_ERROR       EXCEPTION;

    -- GET DEPARTMENTO ('UREG','UPGT','UVIR')
    CURSOR GET_DEPT IS
        SELECT SORLFOS_DEPT_CODE  
        FROM (
            SELECT SORLFOS_DEPT_CODE
            FROM SORLCUR
            INNER JOIN SORLFOS ON
                SORLCUR_PIDM = SORLFOS_PIDM 
                AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM = P_PIDM
                AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
            ORDER BY SORLCUR_TERM_CODE DESC,SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;

 BEGIN
    
    -- GET DEPT OR LEARNER
    OPEN GET_DEPT;
    LOOP
        FETCH GET_DEPT INTO V_DEPT_CODE;
        EXIT WHEN GET_DEPT%NOTFOUND;
    END LOOP;
    CLOSE GET_DEPT;
    
    -- VERIFICAR SI TIENE REGISTROS DE HORAS MÍNINAS Y MÁXIMAS EN EL PERIODO ACTIVO 
    SELECT COUNT(SFBETRM_VERSION)
        INTO V_INDICADOR
    FROM SFBETRM
    WHERE SFBETRM_PIDM = P_PIDM
        AND SFBETRM_TERM_CODE = P_TERM_CODE;
    
    IF V_DEPT_CODE = 'UREG' THEN
    
        IF P_CREDITOS_CODE < V_CREDB_PRE AND V_INDICADOR = 0 THEN
    
            INSERT INTO SFBETRM (SFBETRM_TERM_CODE,         SFBETRM_PIDM,               SFBETRM_ESTS_CODE,
                           SFBETRM_ESTS_DATE,               SFBETRM_MHRS_OVER,          SFBETRM_AR_IND,
                           SFBETRM_ASSESSMENT_DATE,         SFBETRM_ADD_DATE,           SFBETRM_ACTIVITY_DATE,
                           SFBETRM_RGRE_CODE,               SFBETRM_TMST_CODE,          SFBETRM_TMST_DATE,
                           SFBETRM_TMST_MAINT_IND,          SFBETRM_USER,               SFBETRM_REFUND_DATE,
                           SFBETRM_DATA_ORIGIN,             SFBETRM_INITIAL_REG_DATE,   SFBETRM_MIN_HRS,
                           SFBETRM_MINH_SRCE_CDE,           SFBETRM_MAXH_SRCE_CDE)
    
            VALUES (        P_TERM_CODE,                    P_PIDM,                     'EL',
                            SYSDATE,                        P_CREDITOS_CODE,            'N',
                            SYSDATE,                        SYSDATE,                    SYSDATE,
                            NULL,                           'CI',                       SYSDATE,
                            'S',                            'WFAUTO',                   SYSDATE,
                            'BANNER',                       SYSDATE,                    0,
                            'M',                            'U');
    
          COMMIT;
    
        ELSIF P_CREDITOS_CODE < V_CREDB_PRE AND V_INDICADOR > 0 THEN
            
            UPDATE SFBETRM
                SET SFBETRM_MHRS_OVER = P_CREDITOS_CODE, 
                SFBETRM_ASSESSMENT_DATE = SYSDATE, 
                SFBETRM_ACTIVITY_DATE = SYSDATE,
                SFBETRM_USER ='WFAUTO',
                SFBETRM_REFUND_DATE = SYSDATE 
            WHERE SFBETRM_TERM_CODE = P_TERM_CODE
                AND SFBETRM_PIDM = P_PIDM;
            
            COMMIT;
    
        ELSIF P_CREDITOS_CODE >= V_CREDB_PRE AND V_INDICADOR = 0 THEN
    
            INSERT INTO SFBETRM (SFBETRM_TERM_CODE,                 SFBETRM_PIDM,                   SFBETRM_ESTS_CODE,
                            SFBETRM_ESTS_DATE,                      SFBETRM_MHRS_OVER,              SFBETRM_AR_IND,
                            SFBETRM_ASSESSMENT_DATE,                SFBETRM_ADD_DATE,               SFBETRM_ACTIVITY_DATE,
                            SFBETRM_RGRE_CODE,                      SFBETRM_TMST_CODE,              SFBETRM_TMST_DATE,
                            SFBETRM_TMST_MAINT_IND,                 SFBETRM_USER,                   SFBETRM_REFUND_DATE,
                            SFBETRM_DATA_ORIGIN,                    SFBETRM_INITIAL_REG_DATE,       SFBETRM_MIN_HRS,
                            SFBETRM_MINH_SRCE_CDE,                  SFBETRM_MAXH_SRCE_CDE)
            
            VALUES (        P_TERM_CODE,                            P_PIDM,                         'EL',
                            SYSDATE,                                V_CRED_MAX,                     'N',
                            SYSDATE,                                SYSDATE,                        SYSDATE,
                            NULL,                                   'CI',                           SYSDATE,
                            'S',                                    'WFAUTO',                       SYSDATE,
                            'BANNER',                               SYSDATE,                        0,
                            'M',                                    'U');
            
            COMMIT;
    
        ELSE
    
            UPDATE SFBETRM
                SET SFBETRM_MHRS_OVER = V_CRED_MAX, 
                SFBETRM_ASSESSMENT_DATE = SYSDATE, 
                SFBETRM_ACTIVITY_DATE = SYSDATE,
                SFBETRM_USER ='WFAUTO',
                SFBETRM_REFUND_DATE = SYSDATE 
            WHERE SFBETRM_TERM_CODE = P_TERM_CODE
                AND SFBETRM_PIDM = P_PIDM;
                
            COMMIT;
        END IF;
    
    ELSIF V_DEPT_CODE IN ('UPGT','UVIR') THEN
      
        IF P_CREDITOS_CODE < V_CREDB_SEM AND V_INDICADOR = 0 THEN
    
            INSERT INTO SFBETRM (SFBETRM_TERM_CODE,                 SFBETRM_PIDM,                   SFBETRM_ESTS_CODE,
                            SFBETRM_ESTS_DATE,                      SFBETRM_MHRS_OVER,              SFBETRM_AR_IND,
                            SFBETRM_ASSESSMENT_DATE,                SFBETRM_ADD_DATE,               SFBETRM_ACTIVITY_DATE,
                            SFBETRM_RGRE_CODE,                      SFBETRM_TMST_CODE,              SFBETRM_TMST_DATE,
                            SFBETRM_TMST_MAINT_IND,                 SFBETRM_USER,                   SFBETRM_REFUND_DATE,
                            SFBETRM_DATA_ORIGIN,                    SFBETRM_INITIAL_REG_DATE,       SFBETRM_MIN_HRS,
                            SFBETRM_MINH_SRCE_CDE,                  SFBETRM_MAXH_SRCE_CDE)
            
            VALUES (        P_TERM_CODE,                            P_PIDM,                         'EL',
                            SYSDATE,                                P_CREDITOS_CODE,                'N',
                            SYSDATE,                                SYSDATE,                        SYSDATE,
                            NULL,                                   'CI',                           SYSDATE,
                            'S',                                    'WFAUTO',                       SYSDATE,
                            'BANNER',                               SYSDATE,                        0,
                            'M',                                    'U');
        
                COMMIT;
    
        ELSIF P_CREDITOS_CODE < V_CREDB_SEM AND V_INDICADOR > 0 THEN
    
            UPDATE SFBETRM
                SET SFBETRM_MHRS_OVER = P_CREDITOS_CODE, 
                SFBETRM_ASSESSMENT_DATE = SYSDATE, 
                SFBETRM_ACTIVITY_DATE = SYSDATE,
                SFBETRM_USER ='WFAUTO',
                SFBETRM_REFUND_DATE = SYSDATE 
            WHERE SFBETRM_TERM_CODE = P_TERM_CODE
                AND SFBETRM_PIDM = P_PIDM;
    
          COMMIT;
    
        ELSIF P_CREDITOS_CODE >= V_CREDB_SEM AND V_INDICADOR = 0 THEN
    
            INSERT INTO SFBETRM (SFBETRM_TERM_CODE,                 SFBETRM_PIDM,                   SFBETRM_ESTS_CODE,
                            SFBETRM_ESTS_DATE,                      SFBETRM_MHRS_OVER,              SFBETRM_AR_IND,
                            SFBETRM_ASSESSMENT_DATE,                SFBETRM_ADD_DATE,               SFBETRM_ACTIVITY_DATE,
                            SFBETRM_RGRE_CODE,                      SFBETRM_TMST_CODE,              SFBETRM_TMST_DATE,
                            SFBETRM_TMST_MAINT_IND,                 SFBETRM_USER,                   SFBETRM_REFUND_DATE,
                            SFBETRM_DATA_ORIGIN,                    SFBETRM_INITIAL_REG_DATE,       SFBETRM_MIN_HRS,
                            SFBETRM_MINH_SRCE_CDE,                  SFBETRM_MAXH_SRCE_CDE)
            
            VALUES (            P_TERM_CODE,                        P_PIDM,                         'EL',
                                SYSDATE,                            V_CRED_MAX,                     'N',
                                SYSDATE,                            SYSDATE,                        SYSDATE,
                                NULL,                               'CI',                           SYSDATE,
                                'S',                                'WFAUTO',                       SYSDATE,
                                'BANNER',                           SYSDATE,                        0,
                                'M',                                'U');
            
            COMMIT;
    
        ELSE
    
        UPDATE SFBETRM
            SET SFBETRM_MHRS_OVER = V_CRED_MAX, 
            SFBETRM_ASSESSMENT_DATE = SYSDATE, 
            SFBETRM_ACTIVITY_DATE = SYSDATE,
            SFBETRM_USER ='WFAUTO',
            SFBETRM_REFUND_DATE = SYSDATE 
        WHERE SFBETRM_TERM_CODE = P_TERM_CODE
            AND SFBETRM_PIDM = P_PIDM;
            
        COMMIT;
        
        END IF;
    
    ELSE
        RAISE V_ERROR;
    END IF;
    
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - Código de Departamento inválido.');
WHEN OTHERS THEN
    P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_HRS_MAX;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_RECALCULO_CUOTA (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
AS
    ------------------------ APEC PARAMETERS ----------------------------
    P_PART_PERIODO          VARCHAR2(9);
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    P_APEC_IDSECCIONC       VARCHAR2(15);
    P_APEC_IDALUMNO         VARCHAR2(10);
    ---------------------------------------------------------------------
    V_CAMP_CODE             SORLCUR.SORLCUR_CAMP_CODE%type;    -- STVRATE
    V_DEPT_CODE             SORLFOS.SORLFOS_DEPT_CODE%type;
    V_PROGRAM               SORLCUR.SORLCUR_PROGRAM%type;
    V_FECHA                 DATE := SYSDATE;
    V_INDICADOR             NUMBER;
    V_MESSAGE               EXCEPTION;
    P_RESULT                INTEGER;
BEGIN
    -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
    SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
        INTO V_CAMP_CODE, V_DEPT_CODE, V_PROGRAM
    FROM (
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
        FROM SORLCUR
        INNER JOIN SORLFOS ON 
            SORLCUR_PIDM = SORLFOS_PIDM 
            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y'
            AND SORLCUR_TERM_CODE_END IS NULL
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;
    
    -- GET CRONOGRAMA SECCIONC
    SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
        INTO P_PART_PERIODO
    FROM CZRPTRM 
    WHERE CZRPTRM_DEPT = V_DEPT_CODE
    AND CZRPTRM_CAMP_CODE = V_CAMP_CODE;

    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = V_CAMP_CODE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = V_DEPT_CODE;-- DEPARTAMENTO
            
    -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_IDSECCIONC
    WITH 
    CTE_tblSeccionC AS (
        -- GET SECCIONC
        SELECT  "IDSeccionC" IDSeccionC, "FecInic" FecInic
        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia"='UCCI'
            AND "IDsede" = V_APEC_CAMP
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuela" = V_PROGRAM
            AND LENGTH("IDSeccionC") = 5
            AND SUBSTRB("IDSeccionC",-2,2) IN (
                -- PARTE PERIODO           
                SELECT CZRPTRM_PTRM_BDUCCI
                FROM CZRPTRM
                WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
        )
    ),
    CTE_tblPersonaAlumno AS (
        SELECT "IDAlumno" IDAlumno 
        FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDPersona" IN ( 
            SELECT "IDPersona" 
            FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDPersonaN" = P_PIDM
        )
    )
    SELECT "IDAlumno", "IDSeccionC"
        INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno" IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
        AND "IDSede" = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuela" = V_PROGRAM
        AND "IDSeccionC" IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
        AND "IDConcepto" = 'C01'; -- CUALQUIER CONCEPTO seguro SOLO PARA OBTENER LOS DATOS IDALUMNO Y SECCIONC
  
    SELECT COUNT (P_APEC_IDALUMNO)
        INTO V_INDICADOR
    FROM dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDDependencia" = 'UCCI'
        AND "IDSeccionC" = P_APEC_IDSECCIONC
        AND "IDSede" = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDAlumno" = P_APEC_IDALUMNO;

    IF V_INDICADOR = 0 THEN -- IDAlumno IDPerAcad IDDependencia IDSede  IDSeccionC  ncredWF FechaCalc

        IF P_CREDITOS_CODE = 5 THEN
            INSERT INTO dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE VALUES (P_APEC_IDALUMNO, V_APEC_TERM, 'UCCI', V_APEC_CAMP, P_APEC_IDSECCIONC, '5', V_FECHA);
            COMMIT; 
        ELSE
            INSERT INTO dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE VALUES (P_APEC_IDALUMNO, V_APEC_TERM, 'UCCI', V_APEC_CAMP, P_APEC_IDSECCIONC, P_CREDITOS_CODE, V_FECHA);
            COMMIT;
        END IF;

    ELSIF V_INDICADOR = 1 THEN

        IF P_CREDITOS_CODE = 5 THEN
            UPDATE dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE
            SET "ncredWF" = '5', "FechaCalc" = V_FECHA
            WHERE "IDDependencia" = 'UCCI'
                AND "IDSeccionC" = P_APEC_IDSECCIONC
                AND "IDSede" = V_APEC_CAMP
                AND "IDPerAcad" = V_APEC_TERM
                AND "IDAlumno" = P_APEC_IDALUMNO;
            COMMIT;
        
        ELSE
            UPDATE dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE
            SET "ncredWF" = P_CREDITOS_CODE, "FechaCalc" = V_FECHA
            WHERE "IDDependencia" = 'UCCI'
                AND "IDSeccionC" = P_APEC_IDSECCIONC
                AND "IDSede" = V_APEC_CAMP
                AND "IDPerAcad" = V_APEC_TERM
                AND "IDAlumno" = P_APEC_IDALUMNO;
            COMMIT;
        END IF;

    ELSE 
        RAISE V_MESSAGE;
    END IF;

    -- ACTUALIZAR CUENTA CORRIENTE DE VERANO

    P_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
              'dbo.sp_ActualizarMontoPagarBanner "'
              || 'UCCI' ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_APEC_TERM ||'" , "'|| P_APEC_IDALUMNO ||'"' 
        );
  
EXCEPTION
WHEN V_MESSAGE THEN
    P_MESSAGE := 'Tiene doble registro para este periodo en la tabla';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE);
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_RECALCULO_CUOTA;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--         
END BWZKSMCV;

--/
--show errors
--
--SET SCAN ON