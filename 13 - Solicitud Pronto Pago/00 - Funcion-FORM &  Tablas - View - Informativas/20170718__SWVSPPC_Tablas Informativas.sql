CREATE OR REPLACE VIEW "BANINST1".SWVSPPC ("NCOUTA_CODE", "NCUOTA_DESC") AS 
/* ===================================================================================================================
  NOMBRE    : SWVSPPC
              SWView WorkFlow SOLICITUD PRONTO PAGO # de CUOTAS
  FECHA     : 17/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso 
  OBJETIVO  : Lista de opciones para la selecion del numero de cuotas con el cual va a realizar el pago de su pension.
  =================================================================================================================== */
SELECT  1 NCOUTA_CODE, '1 cuota – 10% de descuento' NCUOTA_DESC FROM DUAL UNION
SELECT  2, '2 cuotas – 6% de descuento' FROM DUAL UNION
SELECT  3, '3 cuotas – 4% de descuento' FROM DUAL;

