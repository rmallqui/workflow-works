CREATE OR REPLACE VIEW "BANINST1".SWVSPPC ("NCOUTA_CODE", "NCUOTA_DESC") AS 
/* ===================================================================================================================
  NOMBRE    : SWVSPPC
              SWView WorkFlow SOLICITUD PRONTO PAGO # de CUOTAS
  FECHA     : 17/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso 
  OBJETIVO  : Lista de opciones para la selecion del numero de cuotas con el cual va a realizar el pago de su pension.
  =================================================================================================================== */
	SELECT NCOUTA_CODE, NCUOTA_DESC FROM (
		    SELECT    CASE WHEN SORLFOS_DEPT_CODE = 'UREG' THEN 'UREG' ELSE 'UVIR-UPGT' END SORLFOS_DEPT_CODE
		    FROM (
		            SELECT    SORLFOS_DEPT_CODE
		            FROM SORLCUR        INNER JOIN SORLFOS
		                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
		                  AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
		            WHERE   SORLCUR_PIDM        = BVSKOSAJ.F_GetParamValue('SOL017',999)
		                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
		                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
		                AND SORLCUR_CURRENT_CDE = 'Y'
		            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
		    ) WHERE ROWNUM <= 1
	) INNER JOIN (
			SELECT  1 NCOUTA_CODE, 'PLAN 1 - Descuento 10% - En una cuota' NCUOTA_DESC, 'UREG' DEPT  FROM DUAL UNION
			SELECT  2, 'PLAN 2 - Descuento  6% - En dos cuotas', 'UREG' FROM DUAL UNION
			SELECT  3, 'PLAN 3 - Descuento  4% - En tres cuotas', 'UREG' FROM DUAL UNION
			SELECT  4, 'PLAN 1 - Descuento 10% - En una cuota (Ciclo completo)', 'UVIR-UPGT' FROM DUAL UNION
			SELECT  5, 'PLAN 2 - Descuento  5% - En una cuota (Solo un módulo)', 'UVIR-UPGT' FROM DUAL 
	) ON SORLFOS_DEPT_CODE = DEPT;