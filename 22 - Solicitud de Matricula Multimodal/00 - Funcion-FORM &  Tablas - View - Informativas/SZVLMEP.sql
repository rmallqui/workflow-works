CREATE OR REPLACE VIEW "BANINST1"."SZVLMEP" ("CONFIRM_DEPT_CODE", "CONFIRM_DEPT_DESC") AS 
  SELECT DISTINCT CZRDEPT_CODE, CZRDEPT_DESCRIPTION 
FROM CZRDEPT
INNER JOIN SORCMJR
ON CZRDEPT.CZRDEPT_CODE = SORCMJR_DEPT_CODE
WHERE CZRDEPT_LEVL_CODE = 'PG'
AND SORCMJR_DEPT_CODE <> (SELECT SORLFOS_DEPT_CODE
                            FROM SORLCUR        
                            INNER JOIN SORLFOS
                            ON SORLCUR_PIDM = SORLFOS_PIDM 
                            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
                            WHERE SORLCUR_PIDM = BVSKOSAJ.F_GetParamValue('SOL016',999)
                            AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                            AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                            AND SORLCUR_CURRENT_CDE =   'Y'
                            AND SORLCUR_TERM_CODE_END IS NULL);