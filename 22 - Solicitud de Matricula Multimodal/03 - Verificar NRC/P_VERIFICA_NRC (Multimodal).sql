/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_NRC VARCHAR2(4000);
 P_FECHA_INICIO VARCHAR2(4000);
 P_FECHA_FIN VARCHAR2(4000);
 P_REASON_NRC VARCHAR2(4000);
BEGIN
    P_VERIFICA_NRC_MULTI(118534, 6541, '201820', 'S01', 'UREG', 'ASUC00584', P_FECHA_INICIO, P_FECHA_FIN, P_STATUS_NRC, P_REASON_NRC);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_NRC);
    DBMS_OUTPUT.PUT_LINE(P_FECHA_INICIO);
    DBMS_OUTPUT.PUT_LINE(P_FECHA_FIN);
    DBMS_OUTPUT.PUT_LINE(P_REASON_NRC);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICA_NRC_MULTI (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_NRC_CODE          IN SSBSECT.SSBSECT_CRN%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_CAMP_SEDE         IN STVCAMP.STVCAMP_CODE%TYPE,
        P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_ASIGNATURA_CODE   IN VARCHAR2,
        P_FECHA_INICIO      OUT VARCHAR2,
        P_FECHA_FIN         OUT VARCHAR2,
        P_STATUS_NRC        OUT VARCHAR2,
        P_REASON_NRC        OUT VARCHAR2
)
AS
        V_NRC_ACTIVO        INTEGER := 0; --NRC ACTIVO
        V_NRC_ASIG          INTEGER := 0; --ASIGNATURA ASIGNADA AL NRC
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_EXISTENCIA_NRC    INTEGER := 0;
        V_CONFLICTO_HORARIO INTEGER := 0;
        V_NRC_MULTI         INTEGER := 0;
        V_VACANTES_MULTI    INTEGER := 0;
        V_FLAG              INTEGER := 0;

BEGIN
    --################################################################################################
    --VALIDACIÃ“N DEL INPUT DE NRC
    --################################################################################################

    IF P_NRC_CODE IS NULL OR P_NRC_CODE = '' THEN
        V_RESPUESTA := '<br />' || '<br />' || '- No ha registrado el cÃ³digo del NRC. ';
        
        P_STATUS_NRC := 'FALSE';
        P_REASON_NRC := V_RESPUESTA;
        RETURN;
    END IF;

    --################################################################################################
    --VALIDACIÃ“N DE ESTADO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_NRC_ACTIVO
    FROM SSBSECT
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
    AND SSBSECT_CRN = P_NRC_CODE
    AND SSBSECT_SSTS_CODE = 'A';
    
    IF V_NRC_ACTIVO <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC no estÃ¡ activo. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC no estÃ¡ activo. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÃ“N DE ASIGNATURA ASIGNADA AL NRC
    --################################################################################################
    SELECT COUNT(*)
    INTO V_NRC_ASIG
    FROM SSBSECT
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
    AND SSBSECT_CRN = P_NRC_CODE
    AND SSBSECT_SUBJ_CODE || SSBSECT_CRSE_NUMB = P_ASIGNATURA_CODE;
    
    IF V_NRC_ASIG <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- La asignatura no fue asignada al NRC. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- La asignatura no fue asignada al NRC. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÃ“N DE EXISTENCIA DE NRC MULTIMODAL
    --################################################################################################
    SELECT COUNT(*)
    INTO V_EXISTENCIA_NRC
    FROM SSBSECT 
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
    and SSBSECT_CRN = P_NRC_CODE
    and SSBSECT_SSTS_CODE = 'A' 
    and SSBSECT_SEQ_NUMB <> '0';
    
    IF V_EXISTENCIA_NRC <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC no existe o es semilla. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC no existeo es semilla. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÃ“N DE CONFLICTO DE HORARIO DEL ESTUDIANTE
    --################################################################################################
    SELECT COUNT(*)
    INTO V_CONFLICTO_HORARIO
    FROM SVQ_SSRMEET_TIMECONFLICT
    WHERE SSRMEET_TERM_CODE = P_TERM_CODE
    AND SSRMEET_CRN = P_NRC_CODE
    AND SSRMEET_CRN_CONFLICT IN 
        (SELECT SFRSTCR_CRN 
            FROM SFRSTCR 
            WHERE SFRSTCR_TERM_CODE = P_TERM_CODE 
            AND SFRSTCR_PIDM = P_PIDM
    );
    
    IF V_CONFLICTO_HORARIO > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC tiene cruce de horario con lo matriculado por el estudiante. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC tiene cruce de horario con lo matriculado por el estudiante. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÃ“N DE NRC MULTIMODAL
    --################################################################################################
    SELECT COUNT(*)
    INTO V_NRC_MULTI
    FROM SSRRESV
    WHERE SSRRESV_TERM_CODE = P_TERM_CODE
    AND SSRRESV_CRN = P_NRC_CODE
    AND SSRRESV_DEPT_CODE <> P_DEPT_CODE;

    IF V_NRC_MULTI <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC no es multimodal. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC no es multimodal. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÃ“N DE VACANTES MULTIMODAL
    --################################################################################################

    IF V_NRC_MULTI > 0 THEN
        SELECT COUNT(*)
        INTO V_VACANTES_MULTI
        FROM SSRRESV
        WHERE SSRRESV_TERM_CODE = P_TERM_CODE 
        AND SSRRESV_CRN = P_NRC_CODE
        AND SSRRESV_DEPT_CODE <> P_DEPT_CODE
        AND SSRRESV_LEVL_CODE IS NOT NULL 
        AND SSRRESV_SEATS_AVAIL > 0;
    ELSE
        SELECT COUNT(*)
        INTO V_VACANTES_MULTI
        FROM SSBSECT
        WHERE SSBSECT_TERM_CODE = P_TERM_CODE
        AND SSBSECT_CRN = P_NRC_CODE 
        AND SSBSECT_SEATS_AVAIL > 0;
    END IF;

    IF V_VACANTES_MULTI <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC no tiene vacantes. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC no tiene vacantes. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    --VALIDACIÃ“N FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_NRC := 'FALSE';
        P_FECHA_INICIO := '';
        P_FECHA_FIN := '';
        P_REASON_NRC := V_RESPUESTA;
    ELSE
        SELECT DISTINCT TO_DATE(SSBSECT_PTRM_START_DATE), TO_DATE(SSBSECT_PTRM_END_DATE)
        INTO P_FECHA_INICIO, P_FECHA_FIN
        FROM SSBSECT 
        WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
        and SSBSECT_CRN = P_NRC_CODE;
        
        P_STATUS_NRC := 'TRUE';
        P_REASON_NRC := 'OK';
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_NRC_MULTI;
