/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_REASON VARCHAR2(4000);
BEGIN
    P_VERIFICAR_APTO(123287, '201810', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0; --VALIDA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VALIDA RETENCIÓN 02
        V_RET04             INTEGER := 0; --VALIDA RETENCIÓN 04
        V_RET07             INTEGER := 0; --VALIDA RETENCIÓN 07
        V_RETPER            INTEGER := 0; --VALIDA RETENCIÓN 08 - 16
        V_SOLTRA            INTEGER := 0; --VALIDA SOLICITUD TRASLADO INTERNO
        V_SOLRES            INTEGER := 0; --VALIDA SOLICITUD RESERVA
        V_SOLCAM            INTEGER := 0; --VALIDA SOLICITUD CAMBIO DE PLAN
        V_SOLCUO            INTEGER := 0; --VALIDA SOLICITUD CAMBIO CUOTA INICIAL
        V_SOLPER            INTEGER := 0; --VALIDA SOLICITUD PERMANENCIA
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;

        ---VARIABLES RECIBIDAS DE SP PARA SABER SI ESTUDIANTE ESTA ACTIVO
        V_ESTADO            VARCHAR2(4000);
        V_DESCRIP           VARCHAR2(4000);
        
        V_CODE_DEPT         STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_TERM         STVTERM.STVTERM_CODE%TYPE;
        V_CODE_CAMP         STVCAMP.STVCAMP_CODE%TYPE;
        V_PROGRAM           SORLCUR.SORLCUR_PROGRAM%TYPE;
        V_TERMCTLG          SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
        V_TERMCTLG_MAX      SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
        
        V_SUB_PTRM          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        V_PTRM_CODE         STVPTRM.STVPTRM_CODE%TYPE;
        V_MATRIC_FECHA      SFRRSTS.SFRRSTS_START_DATE%TYPE; --- Fecha de una semana antes de matricula
        
        V_PRIORIDAD         NUMBER;
        V_HAS_RETENCION     NUMBER;
        V_HAS_NRC           NUMBER;
        
    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;
        
BEGIN
    ---P_VERIFICAR_APTO PARA SOLICITUD DE CAMBIO DE PLAN
    
    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
    CLOSE C_SORLCUR;
    
    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';            
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 04 (PAGO DE MATRICULA Y PRIMERA CUOTA)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET04
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '04'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET04 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de pago de matricula y primera cuota activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de pago de matricula y primera cuota activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 07 (CAMBIO DE PLAN DE ESTUDIO)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET07    
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '07'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET07 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de cambio de plan activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de cambio de plan activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 08 - 16 (PERMANENCIA DE PLAN)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RETPER
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE IN ('08','16')
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RETPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de Permanencia de plan de estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de Permanencia de plan de estudios activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VERIFICAR SI ESTA EN ESTADO ACTIVO
    --################################################################################################
    BWZKRIOD.P_VERIFICA_ESTADO_INACTIVO(P_PIDM, V_CODE_DEPT, V_CODE_CAMP, P_TERM_CODE, V_ESTADO, V_DESCRIP);

    IF V_ESTADO = 'TRUE' THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Está inactivo del estudiante. Solicitar proceso de Reincorporación. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Está inactivo del estudiante. Solicitar proceso de Reincorporación. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;