/******************************************************************************/
/* BWZKPSPG.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para el Paquete de procedimientos genericos      */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del código.                                    BFV 25/SEP/2018 */
/*    Creación del paquete de procedimientos genéricos.                       */
/*                                                                            */
/*    Procedure P_VALID_ID: Verifica si el DNI es correcto y existe.          */
/*    Procedure P_GET_INFO_STUDENT: Obtiene información base del estudiante   */
/*      para casos de WF On Demand                                            */
/*    Procedure P_VERIFICA_FECHA_DISPO: Verifica si las fechas para solicitud */
/*      permite que la solicitud este disponible.                             */
/*    Procedure P_VERIFICAR_MATRICULA: Verifica que el estudiante presente    */
/*      matricula en el periodo solicitado.                                   */
/*    Procedure P_MODIFICAR_RETENCION_A: Actualiza el registro a 3 retenciones*/
/*    Procedure P_MODIFICAR_RETENCION_B: Actualiza el registro a 4 retenciones*/
/*    Procedure P_VERIFICAR_CTA_CTE: Verifica que tenga cuenta corriente      */
/*    Procedure P_SET_RECALCULO_CUOTA: Recalculo de las cuotas segun pedido   */
/*    Procedure P_EXECCAPP: Ejecutar CAPP desde sistema                       */
/*    Procedure P_EXECPROY: Ejecutar Proyección desde sistema                 */
/*    Procedure P_MODIFICAR_CATALOGO: Modifica el periodo catalogo            */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Verifica el estado de la        */
/*      solicitud para que continue el proceso.                               */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud   */
/*      para que continue el proceso y no pueda anularse la solicitud.        */
/*    Procedure P_SET_CURSO_INTROD: Registrar cursos introductorios           */
/*    Proceudre P_ELIMINAR_NRC: Elimina la matricula del estudiante.          */
/*    Procedure P_CONV_ASIGN: Realiza el proceso de convalidacion             */
/*    Procedure P_SET_ATRIBUTO_PLAN: Registra el atributo de plan             */
/*    Procedure P_GET_ASIG_RECONOCIDAS: Reporte de asignaturas reconocidas.   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/**********************************************************************/
-- Creación BANINST1 Y PERMISOS
/**********************************************************************/
--     CREATE OR REPLACE PUBLIC SYNONYM "BWZKPSPG" FOR "BANINST1"."BWZKPSPG";
--     GRANT EXECUTE ON BANINST1.BWZKPSPG TO SATURN;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY BANINST1.BWZKPSPG AS
/*
  BWZKPSPG:
       Paquete Web _ Desarrollo Propio _ Paquete _ Matricula Multimodal
*/
-- FILE NAME..: BWZKPSPG.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKPSPG
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_VALID_ID (
    P_ID            IN  SPRIDEN.SPRIDEN_ID%TYPE,
    P_DNI_VALID     OUT VARCHAR2
)
AS
    V_INDICADOR      NUMBER;
    V_ERROR          EXCEPTION;
BEGIN

    SELECT COUNT(*)
        INTO V_INDICADOR
    FROM SPRIDEN
    WHERE SPRIDEN.SPRIDEN_ID = P_ID
        AND SPRIDEN_CHANGE_IND IS NULL;

    IF V_INDICADOR = 1 THEN
        P_DNI_VALID:= 'TRUE';
    ELSIF V_INDICADOR > 1 THEN
        RAISE V_ERROR;
    ELSE
        P_DNI_VALID:= 'FALSE'; 
    END IF;

EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada - '||SQLCODE|| ' - ID con doble registro de PIDM.' );
WHEN OTHERS THEN
    P_DNI_VALID:= 'FALSE';
END P_VALID_ID;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_INFO_STUDENT (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
    P_PART_PERIODO      OUT SOBPTRM.SOBPTRM_PTRM_CODE%TYPE, 
    P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
    P_NAME              OUT VARCHAR2,
    P_FECHA_SOL         OUT VARCHAR2
)
/* ===================================================================================================================
NOMBRE    : P_GET_INFO_STUDENT
FECHA     : 05/12/16
AUTOR     : Mallqui Lopez, Richard Alfonso
OBJETIVO  : Obtiene informacion de un usuario(estudiante) 

MODIFICACIONES
NRO     FECHA         USUARIO       MODIFICACION
001     05/12/2017    KARANA        Se agrega los parametros de P_TERM_CODE y P_PART_PERIODO para encontrar el periodo 
                                    activo a reincorporar, se hace una relacion con la tabla SOBPTRM para restringir las
                                    fechas de inicio a fin de la activacion de reincorporaciones.
002     03/01/2018    KARANA        Se agrega el parametro de fecha para guardar la fecha en la que se realiza la solicitud         
=================================================================================================================== */
AS
    -- @PARAMETERS
    V_INDICADOR         NUMBER;
    V_FECHA_SOL         DATE;
    V_PART_PERIODO      VARCHAR2(2);

    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_CAMPDEPT IS
        SELECT      SORLCUR_CAMP_CODE,
                    SORLCUR_PROGRAM,
                    SORLFOS_DEPT_CODE,
                    STVCAMP_DESC
        FROM (
            SELECT  SORLCUR_CAMP_CODE, 
                    SORLCUR_PROGRAM, 
                    SORLFOS_DEPT_CODE, 
                    STVCAMP_DESC
            FROM SORLCUR
            INNER JOIN SORLFOS ON 
                SORLCUR_PIDM = SORLFOS_PIDM
                AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
            INNER JOIN STVCAMP ON
                SORLCUR_CAMP_CODE = STVCAMP_CODE
            WHERE SORLCUR_PIDM = P_PIDM 
                AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM = 1;

    -- CURSOR GET PERIODO ACTIVO
    CURSOR C_SFRRSTS_PTRM IS
        SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2) SUBPTRM
        FROM CZRPTRM 
        WHERE CZRPTRM_DEPT = P_DEPT_CODE
        AND CZRPTRM_CAMP_CODE = P_CAMP_CODE;

BEGIN 
    -----------------------------------------------------------------------------
    -- GET FECHA
    SELECT TO_CHAR(SYSDATE,'dd/mm/yyyy'), TRUNC(SYSDATE)
        INTO P_FECHA_SOL, V_FECHA_SOL
    FROM DUAL;

    -----------------------------------------------------------------------------
    -- GET nombres , ID
    SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_PIDM
        INTO P_NAME, P_PIDM
    FROM SPRIDEN 
    WHERE SPRIDEN_ID = P_ID
    AND SPRIDEN_CHANGE_IND IS NULL;

    -----------------------------------------------------------------------------
    -- >> GET DEPARTAMENTO y CAMPUS --
    OPEN C_CAMPDEPT;
    LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_PROGRAM,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
    END LOOP;
    CLOSE C_CAMPDEPT;

    -----------------------------------------------------------------------------
    -- GET email
    SELECT COUNT(*)
        INTO V_INDICADOR
    FROM GOREMAL 
    WHERE GOREMAL_PIDM = P_PIDM
        AND GOREMAL_STATUS_IND = 'A';

    IF V_INDICADOR = 0 THEN
        P_EMAIL := '-';
    ELSE
        SELECT COUNT(*)
            INTO V_INDICADOR 
        FROM GOREMAL 
        WHERE GOREMAL_PIDM = P_PIDM
            AND GOREMAL_STATUS_IND = 'A' 
            AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe');

        IF (V_INDICADOR > 0) THEN
            SELECT GOREMAL_EMAIL_ADDRESS
                INTO P_EMAIL
            FROM (
                SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND 
                FROM GOREMAL 
                WHERE GOREMAL_PIDM = P_PIDM
                    AND GOREMAL_STATUS_IND = 'A' 
                    AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                ORDER BY GOREMAL_PREFERRED_IND DESC
            ) WHERE ROWNUM <= 1;
        ELSE
            SELECT GOREMAL_EMAIL_ADDRESS
            INTO P_EMAIL 
            FROM (
                SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND
                FROM GOREMAL
                WHERE GOREMAL_PIDM = P_PIDM
                    AND GOREMAL_STATUS_IND = 'A'
                ORDER BY GOREMAL_PREFERRED_IND DESC
            ) WHERE ROWNUM <= 1;
        END IF;
    END IF;

    -----------------------------------------------------------------------------
    -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
    IF (P_DEPT_CODE = '-' OR P_CAMP_CODE = '-') THEN
        P_TERM_CODE := '-';
    ELSE
        -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
        OPEN C_SFRRSTS_PTRM;
        LOOP
            FETCH C_SFRRSTS_PTRM INTO V_PART_PERIODO;
            EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
        END LOOP;
        CLOSE C_SFRRSTS_PTRM;

        -- GET PERIODO ACTIVO (Para UVIR y UPGT se agrega parte de periodo 2)
        SELECT COUNT(SFRRSTS_TERM_CODE)
            INTO V_INDICADOR
        FROM SFRRSTS
        INNER JOIN SOBPTRM ON
            SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
        WHERE SFRRSTS_RSTS_CODE = 'RW'
            AND V_FECHA_SOL BETWEEN (SFRRSTS_START_DATE-14) AND (SFRRSTS_END_DATE)
            --AND TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') BETWEEN (TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')-14) AND TO_DATE(TO_CHAR(SFRRSTS_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
            --AND TO_DATE(V_FECHA_SOL,'dd/mm/yyyy') BETWEEN (TO_DATE(SFRRSTS_START_DATE,'dd/mm/yyyy')-14) AND TO_DATE(SFRRSTS_END_DATE,'dd/mm/yyyy')
        ORDER BY SFRRSTS_TERM_CODE DESC;

        IF V_INDICADOR = 0 THEN
            P_TERM_CODE := '-';
        ELSE
            SELECT SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE
                INTO P_TERM_CODE, P_PART_PERIODO
            FROM (
                -- Forma SFARSTS  ---> fechas para las partes de periodo
                SELECT DISTINCT SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE
                FROM SFRRSTS
                INNER JOIN SOBPTRM ON
                    SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                    AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
                WHERE SFRRSTS_PTRM_CODE IN (V_PART_PERIODO||'1', (V_PART_PERIODO || CASE WHEN (P_DEPT_CODE = 'UVIR' OR P_DEPT_CODE = 'UPGT') THEN '2' ELSE '-' END) ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                    -- fechas de incio de la solicitud este dentro de las fechas programadas 
                    --AND TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') BETWEEN (TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')-14) AND (TO_DATE(TO_CHAR(SOBPTRM_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')+21)
                    AND V_FECHA_SOL BETWEEN (SFRRSTS_START_DATE-14) AND (SOBPTRM_START_DATE+21)
                ORDER BY SFRRSTS_TERM_CODE 
            ) WHERE ROWNUM <= 1;

        END IF;

    END IF;
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_INFO_STUDENT;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_FECHA_DISPO (
    P_TERM_CODE      IN SOBPTRM.SOBPTRM_TERM_CODE%TYPE,
    P_FECHA_SOL      IN VARCHAR2,
    P_PART_PERIODO   IN SOBPTRM.SOBPTRM_PTRM_CODE%TYPE,
    P_FECHA_VALIDA   OUT VARCHAR2,
    P_MESSAGE        OUT VARCHAR2 
)
/* ===================================================================================================================
NOMBRE    : P_VERIFICA_FECHA_DISPO
FECHA     : 03/01/2018
AUTOR     : Arana Milla, Karina Lizbeth
OBJETIVO  : Verificar si la fecha de solicitud se encuentra dentro de las fecha configuradas para el proceso.

MODIFICACIONES
NRO     FECHA         USUARIO       MODIFICACION      
=================================================================================================================== */
AS
    V_INDICADOR   NUMBER;
    V_ERROR       EXCEPTION;
    V_MESSAGE     VARCHAR2(50);

BEGIN

    SELECT COUNT(*)
        INTO V_INDICADOR
    FROM SFRRSTS
    INNER JOIN SOBPTRM
        ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
        AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
    WHERE SFRRSTS_TERM_CODE = P_TERM_CODE
        AND SFRRSTS_PTRM_CODE = P_PART_PERIODO
        AND SFRRSTS_RSTS_CODE = 'RW'
        AND TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') BETWEEN (TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')-14) AND (TO_DATE(TO_CHAR(SOBPTRM_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')+21);

    IF V_INDICADOR = 0 THEN
        P_FECHA_VALIDA := 'FALSE';
        P_MESSAGE := 'NO ESTA DENTRO DE LAS FECHAS PERMITIDAS';

    ELSIF V_INDICADOR = 1 THEN
        P_FECHA_VALIDA := 'TRUE';
        P_MESSAGE := 'OK';

    ELSE
        RAISE V_ERROR;
    END IF;

EXCEPTION
WHEN V_ERROR THEN
    V_MESSAGE  := 'Se encontró doble registro de parte periodo.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| V_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_FECHA_DISPO; 

  --*****************************************************************************************************************************+********--
  --*****************************************************************************************************************************+********--
  --*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_MATRICULA (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_MAT      OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
)
AS
        V_MATRI           INTEGER := 0;
BEGIN

---VERIFICAR MATRICULA EN EL PERIODO
    SELECT COUNT(*)
    INTO V_MATRI
    FROM SFRSTCR
    WHERE SFRSTCR_PIDM = P_PIDM
    AND SFRSTCR_TERM_CODE = P_TERM_CODE
    AND SFRSTCR_RSTS_CODE in ('RW','RE','DD');

    IF V_MATRI > 0 THEN
        P_STATUS_MAT := 'TRUE';
        P_MESSAGE := 'Matriculado';
    ELSE
        P_STATUS_MAT := 'FALSE';
        P_MESSAGE := 'No Matriculado';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    P_STATUS_MAT := 'FALSE';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_MATRICULA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_MODIFICAR_RETENCION_A (
    P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
) 
AS
    V_INDICADOR               NUMBER;
    V_STATUS_DATE             SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
    V_INVALID_COD_RETENCION   EXCEPTION;

BEGIN
    -- VALIDAR si EXISTE codigo del TIPO DE RETENCION - STVHLDD
    SELECT COUNT(*)
        INTO V_INDICADOR 
    FROM STVHLDD
    WHERE STVHLDD_CODE IN ( P_STVHLDD_CODE1, P_STVHLDD_CODE2, P_STVHLDD_CODE3 );

    -- GET FECHA de estado APROVADO DE LA SOLICITUD
    SELECT SVRSVPR_STATUS_DATE
        INTO V_STATUS_DATE
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        AND SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_SRVS_CODE = 'AP'; -- ESTADO APROVADO

    IF ( V_INDICADOR < 3 ) THEN
        RAISE V_INVALID_COD_RETENCION;
    ELSE
        -- INSERTAR REGISTRO DE FECHA DE RETENCION DOCUMENTOS - SOAHOLD      
        UPDATE SPRHOLD
            SET SPRHOLD_TO_DATE = V_STATUS_DATE - 1, SPRHOLD_ACTIVITY_DATE = SYSDATE, SPRHOLD_RELEASE_IND = 'Y', SPRHOLD_USER = 'WFAUTO'
        WHERE SPRHOLD_PIDM = P_PIDM
            AND SPRHOLD_TO_DATE > V_STATUS_DATE
            AND SPRHOLD_HLDD_CODE IN (P_STVHLDD_CODE1, P_STVHLDD_CODE2, P_STVHLDD_CODE3);
        COMMIT;
    END IF;

EXCEPTION
WHEN V_INVALID_COD_RETENCION THEN
    P_MESSAGE  := '- El "CÓDIGO DE RETENCIÓN" enviado es inválido.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_RETENCION_A;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_MODIFICAR_RETENCION_B (
    P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_FOLIO_SOLICITUD     IN  SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE4       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
) 
AS
    V_INDICADOR               NUMBER;
    V_STATUS_DATE             SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
    V_INVALID_COD_RETENCION   EXCEPTION;
BEGIN

    -- VALIDAR si EXISTE codigo del TIPO DE RETENCION - STVHLDD
    SELECT COUNT(*)
        INTO V_INDICADOR 
    FROM STVHLDD
    WHERE STVHLDD_CODE IN ( P_STVHLDD_CODE1, P_STVHLDD_CODE2, P_STVHLDD_CODE3, P_STVHLDD_CODE4 );

    -- GET FECHA de estado APROVADO DE LA SOLICITUD
    SELECT SVRSVPR_STATUS_DATE
        INTO V_STATUS_DATE
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        AND SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_SRVS_CODE = 'AP'; -- ESTADO APROVADO

    IF ( V_INDICADOR < 4 ) THEN
        RAISE V_INVALID_COD_RETENCION;
    ELSE
        -- INSERTAR REGISTRO DE FECHA DE RETENCION DOCUMENTOS - SOAHOLD      
        UPDATE SPRHOLD
            SET SPRHOLD_TO_DATE = V_STATUS_DATE - 1, SPRHOLD_ACTIVITY_DATE = SYSDATE, SPRHOLD_RELEASE_IND = 'Y', SPRHOLD_USER = 'WFAUTO'
        WHERE SPRHOLD_PIDM = P_PIDM
            AND SPRHOLD_TO_DATE > V_STATUS_DATE
            AND SPRHOLD_HLDD_CODE IN (P_STVHLDD_CODE1, P_STVHLDD_CODE2, P_STVHLDD_CODE3, P_STVHLDD_CODE4);

        COMMIT;
    END IF;

EXCEPTION
WHEN V_INVALID_COD_RETENCION THEN
      P_MESSAGE  := '- El "CÓDIGO DE RETENCIÓN" enviado es inválido.';
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_MODIFICAR_RETENCION_B;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_CTA_CTE (
    P_PIDM			    IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO			IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_COD_SEDE          IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CTA_CTE			OUT VARCHAR2,
    P_DESCRIP_CTA_CTE	OUT VARCHAR2,
    P_MESSAGE   		OUT VARCHAR2
)
AS
    V_ERROR       EXCEPTION;
	V_CONTADOR    NUMBER;
	V_APEC_TERM   VARCHAR2(10);
	V_APEC_CAMP	  VARCHAR2(10);

BEGIN
    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_COD_SEDE;

    WITH
    CTE_tblCronogramas AS (
        -- GET Cronogramas activos
        SELECT  "IDSede" IDSede,
                "Cronograma" Cronograma
        FROM tblCronogramas@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "Activo" = '1'
            AND "Pronabec" = '0'
            AND "IDDepartamento" = P_DEPT_CODE
            AND "IDCampus" = P_COD_SEDE
),
    CTE_tblCtaCorriente AS (
		-- GET Cuenta Corriente
        SELECT DISTINCT "IDAlumno" IDAlumno,
        "IDSede" IDSede,
        "IDSeccionC" IDSeccionC
        FROM  tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDSede" = V_APEC_CAMP
            AND "IDPerAcad" = V_APEC_TERM
            AND LENGTH("IDSeccionC") = 5
            AND "IDConcepto" in ('C00','C01','C02','C03','C04','C05')
            AND "IDAlumno" = P_ID_ALUMNO
)
	SELECT COUNT(IDAlumno)
        INTO V_CONTADOR
	FROM CTE_tblCtaCorriente cc
	INNER JOIN CTE_tblCronogramas cr ON 
        cc.IDSede = cr.IDSede
        AND SUBSTR(cc.IDSeccionC,-2,2) = cr.Cronograma;

	IF V_CONTADOR > 0 THEN
		P_CTA_CTE := '1';
		P_DESCRIP_CTA_CTE := 'OK';
	ELSE
		P_CTA_CTE := '0';
		P_DESCRIP_CTA_CTE := 'No tiene cuenta corriente generada en el periodo académico vigente.';
	END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_CTA_CTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_RECALCULO_CUOTA (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
AS
    ------------------------ APEC PARAMETERS ----------------------------
    P_PART_PERIODO          VARCHAR2(9);
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    P_APEC_IDSECCIONC       VARCHAR2(15);
    P_APEC_IDALUMNO         VARCHAR2(10);
    ---------------------------------------------------------------------
    V_CAMP_CODE             SORLCUR.SORLCUR_CAMP_CODE%type;    -- STVRATE
    V_DEPT_CODE             SORLFOS.SORLFOS_DEPT_CODE%type;
    V_PROGRAM               SORLCUR.SORLCUR_PROGRAM%type;
    V_FECHA                 DATE := SYSDATE;
    V_INDICADOR             NUMBER;
    V_MESSAGE               EXCEPTION;
    V_MESSAGE_CTA           EXCEPTION;
    P_RESULT                INTEGER;

    V_CTACORRIENTE          NUMBER;

BEGIN

-- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
    SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
        INTO V_CAMP_CODE, V_DEPT_CODE, V_PROGRAM
    FROM (
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
        FROM SORLCUR
        INNER JOIN SORLFOS ON
            SORLCUR_PIDM = SORLFOS_PIDM 
            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y'
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;

-- GET CRONOGRAMA SECCIONC
    SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
    INTO P_PART_PERIODO
    FROM CZRPTRM 
    WHERE CZRPTRM_DEPT = V_DEPT_CODE
    AND CZRPTRM_CAMP_CODE = V_CAMP_CODE;

    -- CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
    INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = V_CAMP_CODE;

    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
    INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
    INTO V_APEC_DEPT
    FROM CZRDEPT 
    WHERE CZRDEPT_CODE = V_DEPT_CODE;

    -- VALIDAR CTA CORRIENTE
    WITH
    CTE_tblSeccionC AS (
        -- GET SECCIONC
        SELECT "IDSeccionC" IDSeccionC, "FecInic" FecInic
        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
            AND "IDsede" = V_APEC_CAMP
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuela" = V_PROGRAM
            AND LENGTH("IDSeccionC") = 5
            AND SUBSTRB("IDSeccionC",-2,2) IN (
                -- PARTE PERIODO           
                SELECT CZRPTRM_PTRM_BDUCCI
                FROM CZRPTRM
                WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                )
    ),
    CTE_tblPersonaAlumno AS (
        SELECT "IDAlumno" IDAlumno 
        FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDPersona" IN ( 
            SELECT "IDPersona"
            FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDPersonaN" = P_PIDM
            )
    )
    SELECT COUNT(*)
        INTO V_CTACORRIENTE
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno" IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
        AND "IDSede" = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuela" = V_PROGRAM
        AND "IDSeccionC" IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
        AND "IDConcepto" = 'C01'; -- CUALQUIER CONCEPTO seguro SOLO PARA OBTENER LOS DATOS IDALUMNO Y SECCIONC

    IF V_CTACORRIENTE = 0 THEN
        RAISE V_MESSAGE_CTA;
    ELSE
        -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_IDSECCIONC
        WITH
        CTE_tblSeccionC AS (
            -- GET SECCIONC
            SELECT "IDSeccionC" IDSeccionC, "FecInic" FecInic
            FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
                AND "IDsede" = V_APEC_CAMP
                AND "IDPerAcad" = V_APEC_TERM
                AND "IDEscuela" = V_PROGRAM
                AND LENGTH("IDSeccionC") = 5
                AND SUBSTRB("IDSeccionC",-2,2) IN (
                    -- PARTE PERIODO           
                    SELECT CZRPTRM_PTRM_BDUCCI
                    FROM CZRPTRM
                    WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                    )
        ),
        CTE_tblPersonaAlumno AS (
            SELECT "IDAlumno" IDAlumno 
            FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDPersona" IN ( 
                SELECT "IDPersona"
                FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE
                WHERE "IDPersonaN" = P_PIDM
                )
        )
        SELECT "IDAlumno", "IDSeccionC"
            INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno" IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
            AND "IDSede" = V_APEC_CAMP
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuela" = V_PROGRAM
            AND "IDSeccionC" IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
            AND "IDConcepto" = 'C01'; -- CUALQUIER CONCEPTO seguro SOLO PARA OBTENER LOS DATOS IDALUMNO Y SECCIONC

        SELECT COUNT (P_APEC_IDALUMNO)
            INTO V_INDICADOR
        FROM dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDSeccionC" = P_APEC_IDSECCIONC
            AND "IDSede" = V_APEC_CAMP
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDAlumno" = P_APEC_IDALUMNO;

            IF V_INDICADOR = 0 THEN -- IDAlumno IDPerAcad IDDependencia IDSede  IDSeccionC  ncredWF FechaCalc

            INSERT INTO dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE VALUES (P_APEC_IDALUMNO, V_APEC_TERM, 'UCCI', V_APEC_CAMP, P_APEC_IDSECCIONC, P_CREDITOS_CODE, V_FECHA);
            COMMIT;

            ELSIF V_INDICADOR = 1 THEN

                UPDATE dbo.tblCtaAlumnosWF@BDUCCI.CONTINENTAL.EDU.PE
                    SET "ncredWF" = P_CREDITOS_CODE, "FechaCalc" = V_FECHA
                WHERE "IDDependencia" = 'UCCI'
                    AND "IDSeccionC" = P_APEC_IDSECCIONC
                    AND "IDSede" = V_APEC_CAMP
                    AND "IDPerAcad" = V_APEC_TERM
                    AND "IDAlumno" = P_APEC_IDALUMNO;
                COMMIT;

            ELSE 
               RAISE V_MESSAGE;
            END IF;

        -- ACTUALIZAR CUENTA CORRIENTE
        P_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
                  'dbo.sp_ActualizarMontoPagarBanner "'
                  || 'UCCI' ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_APEC_TERM ||'" , "'|| P_APEC_IDALUMNO ||'"' 
            );
    END IF;
EXCEPTION
WHEN V_MESSAGE_CTA THEN
    P_MESSAGE := 'El estudiante no tiene cuenta corriente con su carrera vigente';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada - '|| SQLCODE || P_MESSAGE);
WHEN V_MESSAGE THEN
    P_MESSAGE := 'Tiene doble registro para este periodo en la tabla';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada - '|| SQLCODE || P_MESSAGE);
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada - '|| SQLCODE ||'-ERROR- '|| SQLERRM);
END P_SET_RECALCULO_CUOTA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_EXECCAPP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    V_PROGRAMA        SORLCUR.SORLCUR_PROGRAM%TYPE;
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
    --  
    SELECT SORLCUR_PROGRAM
    INTO V_PROGRAMA
    FROM (
        SELECT SORLCUR_PROGRAM
        FROM SORLCUR
        INNER JOIN SORLFOS ON
            SORLCUR_PIDM = SORLFOS_PIDM
            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_PIDM 
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y'
            AND SORLCUR_TERM_CODE_END IS NULL    
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;

    --EJECUCIÓN CAPP
    SZKECAP.P_EXEC_CAPP(P_PIDM,V_PROGRAMA,P_TERM_CODE,LV_OUT);

    IF (LV_OUT <> '0') THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;

EXCEPTION
WHEN P_EXCEPTION THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_EXECCAPP;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_EXECPROY (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    V_PROGRAMA        SORLCUR.SORLCUR_PROGRAM%TYPE;
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SELECT SORLCUR_PROGRAM
    INTO V_PROGRAMA
    FROM (
        SELECT SORLCUR_PROGRAM
        FROM SORLCUR
        INNER JOIN SORLFOS ON 
            SORLCUR_PIDM = SORLFOS_PIDM 
            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_PIDM 
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y'
            AND SORLCUR_TERM_CODE_END IS NULL
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;

    --EJECUCIÓN PROY
    SZKECAP.P_EXEC_PROY(P_PIDM,V_PROGRAMA,P_TERM_CODE,LV_OUT);

    IF (LV_OUT <> '0') THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;

EXCEPTION
WHEN P_EXCEPTION THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_EXECPROY;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_MODIFICAR_CATALOGO (
    P_PIDM_ALUMNO      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PERIODO          IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_MESSAGE          OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_MODIFICAR_CATALOGO
  FECHA     : 19/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Modifica EL PERIODO CATALOGO y vuelve a estimar las deudas actualziando su CTA CORRIENTE.

  MODIFICACIONES
  NRO   FECHA         USUARIO   MODIFICACION
  001   17/11/2017    KARANA    Se agrego el CURSOR C_SGRSTSP
  002   17/11/2017    KARANA    Insert del registro INACTIVE en SORLCUR
  003   17/11/2017    KARANA    Insert del registro CHANGED e INECTIVE SORLFOS
  =================================================================================================================== */
AS
    P_DNI_ALUMNO              SPRIDEN.SPRIDEN_ID%TYPE;
    /*----------------------------------------------------------------------------------
    -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
          - Nuevo Registro como Evidencia que alumno realizó TRAMITE
    */
    CURSOR C_SGRSTSP IS
        SELECT * 
        FROM (
            --Ultimo registro cercano al P_TERM_CODE
            SELECT *
            FROM SGRSTSP 
            WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
                AND SGRSTSP_TERM_CODE_EFF <= P_PERIODO
            ORDER BY SGRSTSP_KEY_SEQNO DESC
        ) WHERE ROWNUM = 1;

    -- ADD SGBSTDN - EN CASO NO EXISTA PARA EL PERIODO INDICADO
    CURSOR C_SGBSTDN IS
        SELECT * FROM (
            SELECT *
            FROM SGBSTDN
            WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
            )
        WHERE ROWNUM = 1
        AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN
            WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
                AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
            );

    -- AGREGAR NUEVO REGISTRO
    CURSOR C_SORLCUR IS
        SELECT *
        FROM (
            SELECT *
            FROM SORLCUR 
            WHERE SORLCUR_PIDM = P_PIDM_ALUMNO
                AND SORLCUR_LMOD_CODE = 'LEARNER'
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    )WHERE ROWNUM = 1;

    V_SGBSTDN_REC           SGBSTDN%ROWTYPE;
    V_SORLCUR_REC           SORLCUR%ROWTYPE;
    V_SORLFOS_REC           SORLFOS%ROWTYPE;

    V_SGRSTSP_KEY_SEQNO     SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
    V_SORLCUR_SEQNO_INC     SORLCUR.SORLCUR_SEQNO%TYPE; -- ADD
    V_SGRSTSP_REC           SGRSTSP%ROWTYPE; --ADD

    P_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE; 
    P_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE;
    P_SORLCUR_RATE_CODE     SORLCUR.SORLCUR_RATE_CODE%TYPE; -- TARIFA

    SAVE_ACT_DATE_OUT       VARCHAR2(100);
    RETURN_STATUS_IN_OUT    NUMBER;
    V_PERCAT_CAMBIO         SFBETRM.SFBETRM_TERM_CODE%TYPE:='201800';

    V_SGRSTSP_COUNT         NUMBER; --VALIDAR REGISTRO PREVIO EN SGRSTSP 

BEGIN 
    -- GET DNI alumno
    SELECT SPRIDEN_ID 
        INTO P_DNI_ALUMNO
    FROM SPRIDEN
    WHERE SPRIDEN_PIDM = P_PIDM_ALUMNO
        AND SPRIDEN_CHANGE_IND IS NULL;

    /* #######################################################################
    -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
      - Nuevo Registro como Evidencia que alumno realizó TRAMITE
    */
    OPEN C_SGRSTSP;
    LOOP
        FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
        EXIT WHEN C_SGRSTSP%NOTFOUND;

        -- GET SGRSTSP_KEY_SEQNO
        SELECT SGRSTSP_KEY_SEQNO
            INTO V_SGRSTSP_KEY_SEQNO
        FROM (
            SELECT SGRSTSP_KEY_SEQNO
            FROM SGRSTSP 
            WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
            ORDER BY SGRSTSP_KEY_SEQNO DESC
        ) WHERE ROWNUM = 1;

        SELECT COUNT(*)
        INTO V_SGRSTSP_COUNT
        FROM SGRSTSP 
        WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
        AND SGRSTSP_TERM_CODE_EFF = P_PERIODO
        AND SGRSTSP_KEY_SEQNO = V_SGRSTSP_KEY_SEQNO;

        IF V_SGRSTSP_COUNT = 0 THEN
            INSERT INTO SGRSTSP (
                  SGRSTSP_PIDM,                     SGRSTSP_TERM_CODE_EFF,                  SGRSTSP_KEY_SEQNO,
                  SGRSTSP_STSP_CODE,                SGRSTSP_ACTIVITY_DATE,                  SGRSTSP_DATA_ORIGIN,
                  SGRSTSP_USER_ID,                  SGRSTSP_FULL_PART_IND,                  SGRSTSP_SESS_CODE,
                  SGRSTSP_RESD_CODE,                SGRSTSP_ORSN_CODE,                      SGRSTSP_PRAC_CODE,
                  SGRSTSP_CAPL_CODE,                SGRSTSP_EDLV_CODE,                      SGRSTSP_INCM_CODE,
                  SGRSTSP_EMEX_CODE,                SGRSTSP_APRN_CODE,                      SGRSTSP_TRCN_CODE,
                  SGRSTSP_GAIN_CODE,                SGRSTSP_VOED_CODE,                      SGRSTSP_BLCK_CODE,
                  SGRSTSP_EGOL_CODE,                SGRSTSP_BSKL_CODE,                      SGRSTSP_ASTD_CODE,
                  SGRSTSP_PREV_CODE,                SGRSTSP_CAST_CODE ) 
            SELECT 
                  V_SGRSTSP_REC.SGRSTSP_PIDM,       P_PERIODO,                              V_SGRSTSP_KEY_SEQNO,
                  'AS',                             SYSDATE,                                'WorkFlow',
                  USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,    V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                  V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,        V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                  V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,        V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                  V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,        V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                  V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,        V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                  V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,        V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                  V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
            FROM DUAL;
        END IF;
    END LOOP;
    CLOSE C_SGRSTSP;

    -- #######################################################################
    -- INSERT  ------> SGBSTDN -
    OPEN C_SGBSTDN;
    LOOP
        FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
        EXIT WHEN C_SGBSTDN%NOTFOUND;

        INSERT INTO SGBSTDN (
                  SGBSTDN_PIDM,                                 SGBSTDN_TERM_CODE_EFF,                          SGBSTDN_STST_CODE,
                  SGBSTDN_LEVL_CODE,                            SGBSTDN_STYP_CODE,                              SGBSTDN_TERM_CODE_MATRIC,
                  SGBSTDN_TERM_CODE_ADMIT,                      SGBSTDN_EXP_GRAD_DATE,                          SGBSTDN_CAMP_CODE,
                  SGBSTDN_FULL_PART_IND,                        SGBSTDN_SESS_CODE,                              SGBSTDN_RESD_CODE,
                  SGBSTDN_COLL_CODE_1,                          SGBSTDN_DEGC_CODE_1,                            SGBSTDN_MAJR_CODE_1,
                  SGBSTDN_MAJR_CODE_MINR_1,                     SGBSTDN_MAJR_CODE_MINR_1_2,                     SGBSTDN_MAJR_CODE_CONC_1,
                  SGBSTDN_MAJR_CODE_CONC_1_2,                   SGBSTDN_MAJR_CODE_CONC_1_3,                     SGBSTDN_COLL_CODE_2,
                  SGBSTDN_DEGC_CODE_2,                          SGBSTDN_MAJR_CODE_2,                            SGBSTDN_MAJR_CODE_MINR_2,
                  SGBSTDN_MAJR_CODE_MINR_2_2,                   SGBSTDN_MAJR_CODE_CONC_2,                       SGBSTDN_MAJR_CODE_CONC_2_2,
                  SGBSTDN_MAJR_CODE_CONC_2_3,                   SGBSTDN_ORSN_CODE,                              SGBSTDN_PRAC_CODE,
                  SGBSTDN_ADVR_PIDM,                            SGBSTDN_GRAD_CREDIT_APPR_IND,                   SGBSTDN_CAPL_CODE,
                  SGBSTDN_LEAV_CODE,                            SGBSTDN_LEAV_FROM_DATE,                         SGBSTDN_LEAV_TO_DATE,
                  SGBSTDN_ASTD_CODE,                            SGBSTDN_TERM_CODE_ASTD,                         SGBSTDN_RATE_CODE,
                  SGBSTDN_ACTIVITY_DATE,                        SGBSTDN_MAJR_CODE_1_2,                          SGBSTDN_MAJR_CODE_2_2,
                  SGBSTDN_EDLV_CODE,                            SGBSTDN_INCM_CODE,                              SGBSTDN_ADMT_CODE,
                  SGBSTDN_EMEX_CODE,                            SGBSTDN_APRN_CODE,                              SGBSTDN_TRCN_CODE,
                  SGBSTDN_GAIN_CODE,                            SGBSTDN_VOED_CODE,                              SGBSTDN_BLCK_CODE,
                  SGBSTDN_TERM_CODE_GRAD,                       SGBSTDN_ACYR_CODE,                              SGBSTDN_DEPT_CODE,
                  SGBSTDN_SITE_CODE,                            SGBSTDN_DEPT_CODE_2,                            SGBSTDN_EGOL_CODE,
                  SGBSTDN_DEGC_CODE_DUAL,                       SGBSTDN_LEVL_CODE_DUAL,                         SGBSTDN_DEPT_CODE_DUAL,
                  SGBSTDN_COLL_CODE_DUAL,                       SGBSTDN_MAJR_CODE_DUAL,                         SGBSTDN_BSKL_CODE,
                  SGBSTDN_PRIM_ROLL_IND,                        SGBSTDN_PROGRAM_1,                              SGBSTDN_TERM_CODE_CTLG_1,
                  SGBSTDN_DEPT_CODE_1_2,                        SGBSTDN_MAJR_CODE_CONC_121,                     SGBSTDN_MAJR_CODE_CONC_122,
                  SGBSTDN_MAJR_CODE_CONC_123,                   SGBSTDN_SECD_ROLL_IND,                          SGBSTDN_TERM_CODE_ADMIT_2,
                  SGBSTDN_ADMT_CODE_2,                          SGBSTDN_PROGRAM_2,                              SGBSTDN_TERM_CODE_CTLG_2,
                  SGBSTDN_LEVL_CODE_2,                          SGBSTDN_CAMP_CODE_2,                            SGBSTDN_DEPT_CODE_2_2,
                  SGBSTDN_MAJR_CODE_CONC_221,                   SGBSTDN_MAJR_CODE_CONC_222,                     SGBSTDN_MAJR_CODE_CONC_223,
                  SGBSTDN_CURR_RULE_1,                          SGBSTDN_CMJR_RULE_1_1,                          SGBSTDN_CCON_RULE_11_1,
                  SGBSTDN_CCON_RULE_11_2,                       SGBSTDN_CCON_RULE_11_3,                         SGBSTDN_CMJR_RULE_1_2,
                  SGBSTDN_CCON_RULE_12_1,                       SGBSTDN_CCON_RULE_12_2,                         SGBSTDN_CCON_RULE_12_3,
                  SGBSTDN_CMNR_RULE_1_1,                        SGBSTDN_CMNR_RULE_1_2,                          SGBSTDN_CURR_RULE_2,
                  SGBSTDN_CMJR_RULE_2_1,                        SGBSTDN_CCON_RULE_21_1,                         SGBSTDN_CCON_RULE_21_2,
                  SGBSTDN_CCON_RULE_21_3,                       SGBSTDN_CMJR_RULE_2_2,                          SGBSTDN_CCON_RULE_22_1,
                  SGBSTDN_CCON_RULE_22_2,                       SGBSTDN_CCON_RULE_22_3,                         SGBSTDN_CMNR_RULE_2_1,
                  SGBSTDN_CMNR_RULE_2_2,                        SGBSTDN_PREV_CODE,                              SGBSTDN_TERM_CODE_PREV,
                  SGBSTDN_CAST_CODE,                            SGBSTDN_TERM_CODE_CAST,                         SGBSTDN_DATA_ORIGIN,
                  SGBSTDN_USER_ID,                              SGBSTDN_SCPC_CODE ) 
        VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                   P_PERIODO,                                      V_SGBSTDN_REC.SGBSTDN_STST_CODE,
                  V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,              V_SGBSTDN_REC.SGBSTDN_STYP_CODE,                V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                  V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,        V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,            V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                  V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,          V_SGBSTDN_REC.SGBSTDN_SESS_CODE,                V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                  V_SGBSTDN_REC.SGBSTDN_COLL_CODE_1,            V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_1,              V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,       V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                  V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,              V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,         V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,     V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,                V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                  V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,              V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,     V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                  V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,              V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,           V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                  V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,           V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                  SYSDATE,                                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                  V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,              V_SGBSTDN_REC.SGBSTDN_INCM_CODE,                V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                  V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,              V_SGBSTDN_REC.SGBSTDN_APRN_CODE,                V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                  V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,              V_SGBSTDN_REC.SGBSTDN_VOED_CODE,                V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                  V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,         V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,                V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                  V_SGBSTDN_REC.SGBSTDN_SITE_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,              V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                  V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,           V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                  V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,           V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                  V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_1,                P_PERIODO,
                  V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,     V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                  V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,                V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                  V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,            V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                  V_SGBSTDN_REC.SGBSTDN_CURR_RULE_1,            V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_1,            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,           V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,           V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                  V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,          V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,            V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                  V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,           V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,           V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                  V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_PREV_CODE,                V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                  V_SGBSTDN_REC.SGBSTDN_CAST_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,           'WorkFlow',
                  USER,                                         V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );

    END LOOP;
    CLOSE C_SGBSTDN;

    UPDATE SGBSTDN
    SET SGBSTDN_TERM_CODE_CTLG_1 = V_PERCAT_CAMBIO
    WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
        AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
        AND SGBSTDN_TERM_CODE_CTLG_1 <> P_PERIODO;

    -- #######################################################################
    -- INSERT  ------> SORLCUR -
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_SORLCUR_REC;
        EXIT WHEN C_SORLCUR%NOTFOUND;    

        -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
        SELECT MAX(SORLCUR_SEQNO + 1)
            INTO V_SORLCUR_SEQNO_INC
        FROM SORLCUR
        WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;

        -- GET SORLCUR_SEQNO NUEVO 
        SELECT MAX(SORLCUR_SEQNO + 2)
            INTO P_SORLCUR_SEQNO_NEW
        FROM SORLCUR
        WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;

        -- GET SORLCUR_SEQNO ANTERIOR 
        P_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;

        -- INACTIVE --- SORLCUR - (1 DE 2)
        INSERT INTO SORLCUR (
                  SORLCUR_PIDM,                             SORLCUR_SEQNO,                              SORLCUR_LMOD_CODE,
                  SORLCUR_TERM_CODE,                        SORLCUR_KEY_SEQNO,                          SORLCUR_PRIORITY_NO,
                  SORLCUR_ROLL_IND,                         SORLCUR_CACT_CODE,                          SORLCUR_USER_ID,
                  SORLCUR_DATA_ORIGIN,                      SORLCUR_ACTIVITY_DATE,                      SORLCUR_LEVL_CODE,
                  SORLCUR_COLL_CODE,                        SORLCUR_DEGC_CODE,                          SORLCUR_TERM_CODE_CTLG,
                  SORLCUR_TERM_CODE_END,                    SORLCUR_TERM_CODE_MATRIC,                   SORLCUR_TERM_CODE_ADMIT,
                  SORLCUR_ADMT_CODE,                        SORLCUR_CAMP_CODE,                          SORLCUR_PROGRAM,
                  SORLCUR_START_DATE,                       SORLCUR_END_DATE,                           SORLCUR_CURR_RULE,
                  SORLCUR_ROLLED_SEQNO,                     SORLCUR_STYP_CODE,                          SORLCUR_RATE_CODE,
                  SORLCUR_LEAV_CODE,                        SORLCUR_LEAV_FROM_DATE,                     SORLCUR_LEAV_TO_DATE,
                  SORLCUR_EXP_GRAD_DATE,                    SORLCUR_TERM_CODE_GRAD,                     SORLCUR_ACYR_CODE,
                  SORLCUR_SITE_CODE,                        SORLCUR_APPL_SEQNO,                         SORLCUR_APPL_KEY_SEQNO,
                  SORLCUR_USER_ID_UPDATE,                   SORLCUR_ACTIVITY_DATE_UPDATE,               SORLCUR_GAPP_SEQNO,
                  SORLCUR_CURRENT_CDE ) 
        VALUES (  V_SORLCUR_REC.SORLCUR_PIDM,               V_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                  P_PERIODO,                                V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                  V_SORLCUR_REC.SORLCUR_ROLL_IND,           'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                  V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,        SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                  V_SORLCUR_REC.SORLCUR_COLL_CODE,          V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                  P_PERIODO,                                V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                  V_SORLCUR_REC.SORLCUR_ADMT_CODE,          V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                  V_SORLCUR_REC.SORLCUR_START_DATE,         V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                  V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,       V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                  V_SORLCUR_REC.SORLCUR_LEAV_CODE,          V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                  V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,      V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                  V_SORLCUR_REC.SORLCUR_SITE_CODE,          V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                  USER,                                     SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                  NULL /*SORLCUR_CURRENT_CDE*/ );

        -- ACTIVE --- SORLCUR - (2 DE 2)
        INSERT INTO SORLCUR (
                  SORLCUR_PIDM,                             SORLCUR_SEQNO,                              SORLCUR_LMOD_CODE,
                  SORLCUR_TERM_CODE,                        SORLCUR_KEY_SEQNO,                          SORLCUR_PRIORITY_NO,
                  SORLCUR_ROLL_IND,                         SORLCUR_CACT_CODE,                          SORLCUR_USER_ID,
                  SORLCUR_DATA_ORIGIN,                      SORLCUR_ACTIVITY_DATE,                      SORLCUR_LEVL_CODE,
                  SORLCUR_COLL_CODE,                        SORLCUR_DEGC_CODE,                          SORLCUR_TERM_CODE_CTLG,
                  SORLCUR_TERM_CODE_END,                    SORLCUR_TERM_CODE_MATRIC,                   SORLCUR_TERM_CODE_ADMIT,
                  SORLCUR_ADMT_CODE,                        SORLCUR_CAMP_CODE,                          SORLCUR_PROGRAM,
                  SORLCUR_START_DATE,                       SORLCUR_END_DATE,                           SORLCUR_CURR_RULE,
                  SORLCUR_ROLLED_SEQNO,                     SORLCUR_STYP_CODE,                          SORLCUR_RATE_CODE,
                  SORLCUR_LEAV_CODE,                        SORLCUR_LEAV_FROM_DATE,                     SORLCUR_LEAV_TO_DATE,
                  SORLCUR_EXP_GRAD_DATE,                    SORLCUR_TERM_CODE_GRAD,                     SORLCUR_ACYR_CODE,
                  SORLCUR_SITE_CODE,                        SORLCUR_APPL_SEQNO,                         SORLCUR_APPL_KEY_SEQNO,
                  SORLCUR_USER_ID_UPDATE,                   SORLCUR_ACTIVITY_DATE_UPDATE,               SORLCUR_GAPP_SEQNO,
                  SORLCUR_CURRENT_CDE ) 
        VALUES (  V_SORLCUR_REC.SORLCUR_PIDM,               P_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                  P_PERIODO,                                V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                  V_SORLCUR_REC.SORLCUR_ROLL_IND,           V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                  'WorkFlow',                               SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                  V_SORLCUR_REC.SORLCUR_COLL_CODE,          V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_PERCAT_CAMBIO,
                  NULL,                                     V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                  V_SORLCUR_REC.SORLCUR_ADMT_CODE,          V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                  V_SORLCUR_REC.SORLCUR_START_DATE,         V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                  V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,       V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                  V_SORLCUR_REC.SORLCUR_LEAV_CODE,          V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                  V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,      V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                  V_SORLCUR_REC.SORLCUR_SITE_CODE,          V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                  USER,                                     SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                  V_SORLCUR_REC.SORLCUR_CURRENT_CDE );

        -- UPDATE TERM_CODE_END(vigencia curriculum) 
        UPDATE SORLCUR 
            SET SORLCUR_TERM_CODE_END = P_PERIODO, 
        SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
        WHERE SORLCUR_PIDM = P_PIDM_ALUMNO 
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;

        -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
        UPDATE SORLCUR 
            SET SORLCUR_CURRENT_CDE = NULL
        WHERE SORLCUR_PIDM = P_PIDM_ALUMNO 
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_TERM_CODE_END = P_PERIODO
            AND SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;

    END LOOP;
    CLOSE C_SORLCUR;

    -- #######################################################################
    -- **INSERT --- SORLFOS** --

    -- INSERT --- SORLFOS - (1 DE 2) 
    INSERT INTO SORLFOS (
        SORLFOS_PIDM,                           SORLFOS_LCUR_SEQNO,                         SORLFOS_SEQNO,
        SORLFOS_LFST_CODE,                      SORLFOS_TERM_CODE,                          SORLFOS_PRIORITY_NO,
        SORLFOS_CSTS_CODE,                      SORLFOS_CACT_CODE,                          SORLFOS_DATA_ORIGIN,
        SORLFOS_USER_ID,                        SORLFOS_ACTIVITY_DATE,                      SORLFOS_MAJR_CODE,
        SORLFOS_TERM_CODE_CTLG,                 SORLFOS_TERM_CODE_END,                      SORLFOS_DEPT_CODE,
        SORLFOS_MAJR_CODE_ATTACH,               SORLFOS_LFOS_RULE,                          SORLFOS_CONC_ATTACH_RULE,
        SORLFOS_START_DATE,                     SORLFOS_END_DATE,                           SORLFOS_TMST_CODE,
        SORLFOS_ROLLED_SEQNO,                   SORLFOS_USER_ID_UPDATE,                     SORLFOS_ACTIVITY_DATE_UPDATE,
        SORLFOS_CURRENT_CDE) 
    SELECT 
        V_SORLFOS_REC.SORLFOS_PIDM,             V_SORLCUR_SEQNO_INC,                        1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
        V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_PERIODO,                                  V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
        'CHANGED'/*SORLFOS_CSTS_CODE*/,         'INACTIVE'/*SORLFOS_CACT_CODE*/,            V_SORLFOS_REC.SORLFOS_DATA_ORIGIN,
        USER,                                   SYSDATE,                                    V_SORLFOS_REC.SORLFOS_MAJR_CODE,
        V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,        V_SORLFOS_REC.SORLFOS_DEPT_CODE,
        V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,            V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
        V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,             V_SORLFOS_REC.SORLFOS_TMST_CODE,
        V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                       SYSDATE,
        NULL /*SORLFOS_CURRENT_CDE*/
    FROM SORLFOS V_SORLFOS_REC
    WHERE SORLFOS_PIDM = P_PIDM_ALUMNO
        AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD
        AND ROWNUM = 1;

    -- INSERT --- SORLFOS - (2 DE 2)
    INSERT INTO SORLFOS (
        SORLFOS_PIDM,                           SORLFOS_LCUR_SEQNO,                         SORLFOS_SEQNO,
        SORLFOS_LFST_CODE,                      SORLFOS_TERM_CODE,                          SORLFOS_PRIORITY_NO,
        SORLFOS_CSTS_CODE,                      SORLFOS_CACT_CODE,                          SORLFOS_DATA_ORIGIN,
        SORLFOS_USER_ID,                        SORLFOS_ACTIVITY_DATE,                      SORLFOS_MAJR_CODE,
        SORLFOS_TERM_CODE_CTLG,                 SORLFOS_TERM_CODE_END,                      SORLFOS_DEPT_CODE,
        SORLFOS_MAJR_CODE_ATTACH,               SORLFOS_LFOS_RULE,                          SORLFOS_CONC_ATTACH_RULE,
        SORLFOS_START_DATE,                     SORLFOS_END_DATE,                           SORLFOS_TMST_CODE,
        SORLFOS_ROLLED_SEQNO,                   SORLFOS_USER_ID_UPDATE,                     SORLFOS_ACTIVITY_DATE_UPDATE,
        SORLFOS_CURRENT_CDE) 
    SELECT 
        V_SORLFOS_REC.SORLFOS_PIDM,             P_SORLCUR_SEQNO_NEW,                        1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
        V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_PERIODO,                                  V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
        V_SORLFOS_REC.SORLFOS_CSTS_CODE,        V_SORLFOS_REC.SORLFOS_CACT_CODE,            'WorkFlow',
        USER,                                   SYSDATE,                                    V_SORLFOS_REC.SORLFOS_MAJR_CODE,
        V_PERCAT_CAMBIO,                        V_SORLFOS_REC.SORLFOS_TERM_CODE_END,        V_SORLFOS_REC.SORLFOS_DEPT_CODE,
        V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,            V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
        V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,             V_SORLFOS_REC.SORLFOS_TMST_CODE,
        V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                       SYSDATE,
        'Y'
    FROM SORLFOS V_SORLFOS_REC
    WHERE SORLFOS_PIDM = P_PIDM_ALUMNO
        AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD;

    -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo)
    UPDATE SORLFOS 
        SET SORLFOS_CURRENT_CDE = NULL
    WHERE SORLFOS_PIDM = P_PIDM_ALUMNO
        AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD
        AND SORLFOS_TERM_CODE = P_PERIODO
        AND SORLFOS_CSTS_CODE = 'INPROGRESS';

    /*
    -- NO ES NECESARIO LA ESTIMACION DE CUOTA EN BANNER Y APEC, YA QUE NO ACTUALIZA SUS PAGOS.
    -- #######################################################################
    -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
    SFKFEES.p_processfeeassessment (  P_PERIODO,
                                    P_ID_ALUMNO,
                                    SYSDATE,      -- assessment effective date(Evaluacion de la fecha efectiva)
                                    SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                    'R',          -- use regular assessment rules(utilizar las reglas de evaluacion periodica)
                                    'Y',          -- create TBRACCD records
                                    'SFAREGS',    -- where assessment originated from
                                    'Y',          -- commit changes
                                    SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                    'N',          -- do not ignore SFRFMAX rules
                                    RETURN_STATUS_IN_OUT );   -- OUT -- return_status
    ------------------------------------------------------------------------------  

    -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
    TZJAPOL.p_run_proc_tvrappl(P_DNI_ALUMNO);

    ------------------------------------------------------------------------------                                    
    */                                   

    COMMIT;
    --
EXCEPTION
WHEN OTHERS THEN
    P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_CATALOGO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STATUS_SOL          OUT VARCHAR2,
    P_MESSAGE             OUT VARCHAR2
)
AS
    V_SOL                     NUMBER := 0;
    V_ESTADO                  SVVSRVS.SVVSRVS_CODE%TYPE;
    V_PRIORIDAD               SVRSVPR.SVRSVPR_RSRV_SEQ_NO%TYPE;
    V_ERROR                   EXCEPTION;
BEGIN

    --################################################################################################
    --VERIFICAR EXISTENCIA DE SOLICITUD
    --################################################################################################
    SELECT COUNT(*)
        INTO V_SOL
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;

    IF V_SOL > 0 THEN
        -- Obtener estado de solicitud
        SELECT SVRSVPR_SRVS_CODE
            INTO V_ESTADO
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;

        IF V_ESTADO = 'AN' THEN
            P_STATUS_SOL := 'FALSE';
        ELSE
            P_STATUS_SOL := 'TRUE';
        END IF;
    ELSE
        RAISE V_ERROR;
    END IF;

EXCEPTION
WHEN V_ERROR THEN
    P_MESSAGE := 'El número de la solicitud no existe.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
WHEN OTHERS THEN
    P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_MESSAGE             OUT VARCHAR2
)
AS
    P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
    P_ESTADO_PREVIOUS         SVVSRVS.SVVSRVS_CODE%TYPE;
    P_FECHA_SOL               SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
    P_INDICADOR               NUMBER;
    E_INVALID_ESTADO          EXCEPTION;
    V_PRIORIDAD               SVRSVPR.SVRSVPR_RSRV_SEQ_NO%TYPE;
BEGIN
--
  ----------------------------------------------------------------------------
    -- GET tipo de solicitud
    SELECT SVRSVPR_SRVC_CODE, SVRSVPR_SRVS_CODE, SVRSVPR_RECEPTION_DATE, SVRSVPR_RSRV_SEQ_NO
        INTO P_CODIGO_SOLICITUD, P_ESTADO_PREVIOUS, P_FECHA_SOL, V_PRIORIDAD
    FROM SVRSVPR
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;

    -- SI EL ESTADO ES ANULADO (AN)
    P_AN := 0;

    IF P_ESTADO_PREVIOUS = 'AN' THEN
        P_AN := 1;
    END IF;

    -- VALIDAR que :
        -- El ESTADO actual sea modificable segun la configuracion.
        -- El ETSADO enviado pertenesca entre los asignados al tipo de SOLICTUD.
    SELECT COUNT(*)
        INTO P_INDICADOR
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        AND SVRSVPR_SRVS_CODE IN (
            -- Estados de solic. MODIFICABLES segun las reglas y configuracion
            SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
            FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
            INNER JOIN SVRRSST ON------------------------------- subformulario de reglas -- estados
                SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
            INNER JOIN SVVSRVS ON ------------------------------- total de estados de servicios
                SVRRSST_SRVS_CODE = SVVSRVS_CODE
            WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
                AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        )
        AND P_ESTADO IN (
            -- Estados de solic. configurados
            SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
            FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
            INNER JOIN SVRRSST ON ------------------------------- subformulario de reglas -- estados
                SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
            INNER JOIN SVVSRVS ON ------------------------------- total de estados de servicios
                SVRRSST_SRVS_CODE = SVVSRVS_CODE
            WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
    );

    IF P_INDICADOR = 0 THEN
        RAISE E_INVALID_ESTADO;
    ELSIF (P_ESTADO_PREVIOUS <> P_ESTADO) THEN
        -- UPDATE SOLICITUD
        UPDATE SVRSVPR
            SET SVRSVPR_SRVS_CODE = P_ESTADO,  --AN , AP , RE entre otros
            SVRSVPR_ACTIVITY_DATE = SYSDATE,
            SVRSVPR_DATA_ORIGIN = 'WorkFlow'
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
            AND SVRSVPR_SRVS_CODE IN (
                -- Estados de solic. MODIFICABLES segun las reglas y configuracion
                SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                INNER JOIN SVRRSST ON ------------------------------- subformulario de reglas -- estados
                    SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                    AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                INNER JOIN SVVSRVS ON ------------------------------- total de estados de servicios
                    SVRRSST_SRVS_CODE = SVVSRVS_CODE
                WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                    AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                    AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
                    AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
            )
            AND P_ESTADO IN (
                -- Estados de solic. configurados
                SELECT SVRRSST_SRVS_CODE ------------------------ Estado SOL
                FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                INNER JOIN SVRRSST ON ------------------------------- subformulario de reglas -- estados
                    SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                    AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                INNER JOIN SVVSRVS ON ------------------------------- total de estados de servicios
                    SVRRSST_SRVS_CODE = SVVSRVS_CODE
                WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                    AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                    AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                    -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
            );

          COMMIT;
    END IF;

EXCEPTION
WHEN E_INVALID_ESTADO THEN
    P_MESSAGE  := '- El "ESTADO" actual no es modificable o no se encuentra entre los ESTADOS disponibles para la solicitud.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CURSO_INTROD (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_FECHA_SOL           IN VARCHAR2,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
NOMBRE    : P_SET_CURSO_INTROD
FECHA     : 02/08/2017
AUTOR     : Mallqui Lopez, Richard Alfonso
OBJETIVO  : Asigna los cursos introducctorios de RM y RV con nota 11 solo en caso no los tubiera alguno.

MODIFICACIONES
NRO   FECHA         USUARIO     MODIFICACION
001   16/10/2017    rmallqui    Se agrega el curso introductorio de codigo "PR" para UPGT y UVIR
=================================================================================================================== */
AS
    V_CURSO_INTROD        VARCHAR2(2);
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORTEST_IN IS
        SELECT 'RM' FROM DUAL WHERE P_DEPT_CODE = 'UREG'
        UNION
        SELECT 'RV' FROM DUAL WHERE P_DEPT_CODE = 'UREG'
        UNION
        SELECT 'PR' FROM DUAL WHERE P_DEPT_CODE = 'UPGT' OR P_DEPT_CODE = 'UVIR';
BEGIN
    OPEN C_SORTEST_IN;
    LOOP
        FETCH C_SORTEST_IN INTO V_CURSO_INTROD;
        EXIT WHEN C_SORTEST_IN%NOTFOUND;
        ----------------------------------------------------------------------------
        -- INSERT curso introductorio 
        INSERT INTO SORTEST ( 
              SORTEST_PIDM,         SORTEST_TESC_CODE,      SORTEST_TEST_DATE,
              SORTEST_TEST_SCORE,   SORTEST_ACTIVITY_DATE,  SORTEST_TERM_CODE_ENTRY,
              SORTEST_RELEASE_IND,  SORTEST_EQUIV_IND,      SORTEST_USER_ID,
              SORTEST_DATA_ORIGIN   ) 
        SELECT 
              P_PIDM,               V_CURSO_INTROD,         TO_DATE(P_FECHA_SOL,'dd/mm/yyyy'),
              '11.00',              SYSDATE,                P_TERM_CODE,
              'N',                  'N',                    USER,
              'WorkFlow'
        FROM DUAL
        WHERE NOT EXISTS (
            SELECT SORTEST_PIDM
            FROM SORTEST 
            WHERE SORTEST_PIDM = P_PIDM 
                AND SORTEST_TESC_CODE = V_CURSO_INTROD
                AND SORTEST_TEST_SCORE >= '10.5'
        );
        ----------------------------------------------------------------------------
    END LOOP;
    CLOSE C_SORTEST_IN;

    COMMIT;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CURSO_INTROD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_ELIMINAR_NRC (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
AS
    V_MAT               NUMBER := 0; --VALIDACION DE MATRICULA
    V_STP               NUMBER := 0; 
    V_INI               NUMBER := 0;
BEGIN

    -- #######################################################################
    ---ELIMINAR MATRICULA EN EL PERIODO
    -- #######################################################################
    SELECT COUNT(*)
        INTO V_MAT
    FROM SFRSTCR
    WHERE SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_RSTS_CODE in ('RW','RE','DD');

    IF V_MAT > 0 THEN
        DELETE SFRSTCR
        WHERE SFRSTCR_PIDM = P_PIDM
            AND SFRSTCR_TERM_CODE = P_TERM_CODE
            AND SFRSTCR_RSTS_CODE in ('RW','RE','DD');

        COMMIT;
    END IF;

    -- #######################################################################
    -- ELIMINAR - STUDY PATH -> SFAREGS
    -- #######################################################################
    SELECT COUNT(*)
        INTO V_STP
    FROM SFRENSP 
    WHERE SFRENSP_PIDM = P_PIDM
        AND SFRENSP_TERM_CODE = P_TERM_CODE;

    IF V_STP > 0 THEN
        DELETE SFRENSP 
        WHERE SFRENSP_PIDM = P_PIDM
            AND SFRENSP_TERM_CODE = P_TERM_CODE;

        COMMIT;
    END IF;

    -- #######################################################################
    -- ELIMINAR - INFORMACION DE INGRESO -> SFAREGS  ||  registro que determina al alumno Elegible 
    -- #######################################################################
    SELECT COUNT(*)
        INTO V_INI
    FROM SFBETRM
    WHERE SFBETRM_PIDM = P_PIDM
        AND SFBETRM_TERM_CODE = P_TERM_CODE;

    IF V_INI > 0 THEN
        DELETE SFBETRM
        WHERE SFBETRM_PIDM = P_PIDM
            AND SFBETRM_TERM_CODE = P_TERM_CODE;
        COMMIT;
    END IF;

    COMMIT;

    P_MESSAGE := 'OK';

EXCEPTION
WHEN OTHERS THEN
    P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_ELIMINAR_NRC;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CONV_ASIGN(
    P_TERM_CODE   IN STVTERM.STVTERM_CODE%TYPE,
    P_CATALOGO    IN STVTERM.STVTERM_CODE%TYPE,
    P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_MESSAGE     OUT VARCHAR2
)
AS   
    V_PROGRAM     SORLCUR.SORLCUR_PROGRAM%TYPE;
BEGIN

    SELECT SORLCUR_PROGRAM
    INTO V_PROGRAM
    FROM (
        SELECT SORLCUR_PROGRAM
        FROM SORLCUR
        INNER JOIN SORLFOS ON 
            SORLCUR_PIDM = SORLFOS_PIDM 
            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_PIDM 
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y'
            AND SORLCUR_TERM_CODE_END IS NULL
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM = 1;

    -- PACKAGE CONVALIDACION
    SZKCAEE.P_CONV_ASIGN_EQUIV_WF(P_TERM_CODE,P_CATALOGO,P_PIDM,V_PROGRAM,P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONV_ASIGN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_ATRIBUTO_PLAN (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,      -------- COD atributos
    P_MESSAGE           OUT VARCHAR2
)
AS

    V_TERM_LAST_CODE      STVTERM.STVTERM_CODE%TYPE;
    V_INDICADOR           NUMBER;
    V_ATTS_CURRENT        STVATTS.STVATTS_CODE%TYPE;
    P_ROWID               GB_COMMON.INTERNAL_RECORD_ID_TYPE;

    V_TERM                STVTERM.STVTERM_CODE%TYPE;
    V_ATTS_CODE           STVATTS.STVATTS_CODE%TYPE;

    -- GET el ultimo TERM(Periodo) de los atributos del estudiante - forma SGASADD
    CURSOR C_SGRSATT_TERM IS
        SELECT SGRSATT_TERM_CODE_EFF
        FROM (
            SELECT SGRSATT_TERM_CODE_EFF
            FROM SGRSATT 
            WHERE SGRSATT_PIDM = P_PIDM
                AND SGRSATT_TERM_CODE_EFF <= P_TERM_CODE
            ORDER BY SGRSATT_TERM_CODE_EFF DESC
        ) WHERE ROWNUM = 1;

    -- CREAR - ACTUALZIAR ATRIBUTO DE PLAN ESTUDIOS
    CURSOR C_SGRSATT_SET IS
        SELECT SGRSATT_TERM_CODE_EFF, SGRSATT_ATTS_CODE
        FROM SGRSATT 
        WHERE SGRSATT_PIDM = P_PIDM
            AND SGRSATT_TERM_CODE_EFF = V_TERM_LAST_CODE
            AND P_ATTS_CODE <> V_ATTS_CURRENT;

BEGIN
    -- GET el ultimo TERM(Periodo) de los atributos del estudiante - forma SGASADD
    OPEN C_SGRSATT_TERM;
    LOOP
        FETCH C_SGRSATT_TERM INTO V_TERM_LAST_CODE;
        EXIT WHEN C_SGRSATT_TERM%NOTFOUND;
    END LOOP;
    CLOSE C_SGRSATT_TERM;

    -- #######################################################################
    -- SET ATRIBUTO alumno - SGASADD
    IF V_TERM_LAST_CODE IS NULL THEN
        -- CASO QUE NO EXISTA NINGUN REGISTRO DE ATRIBUTO
        INSERT INTO SGRSATT
        (       SGRSATT_PIDM, 
                SGRSATT_TERM_CODE_EFF,
                SGRSATT_ATTS_CODE,
                SGRSATT_ACTIVITY_DATE,
                SGRSATT_STSP_KEY_SEQUENCE
        )
        SELECT  P_PIDM, 
                P_TERM_CODE, 
                P_ATTS_CODE, 
                SYSDATE, 
                NULL
        FROM DUAL
        WHERE NOT EXISTS (
            SELECT * 
            FROM SGRSATT 
            WHERE SGRSATT_PIDM = P_PIDM
            );
    ELSE
        -- GET ATRIBUTO de PLAN ESTUDIO mas cercano al periodo requerido - forma SGASADD
        SELECT SGRSATT_ATTS_CODE
            INTO V_ATTS_CURRENT 
        FROM SGRSATT 
        WHERE SGRSATT_PIDM = P_PIDM
            AND SGRSATT_TERM_CODE_EFF = V_TERM_LAST_CODE
            AND SGRSATT_ATTS_CODE IN ( 
                SELECT STVATTS_CODE
                FROM STVATTS
                WHERE STVATTS_CODE LIKE ('P0%')
            );

    -- CREAR - ACTUALIZAR ATRIBUTO DE PLAN ESTUDIOS
    OPEN C_SGRSATT_SET;
    LOOP
        FETCH C_SGRSATT_SET INTO V_TERM, V_ATTS_CODE;
        IF C_SGRSATT_SET%FOUND THEN
            IF V_TERM <> P_TERM_CODE THEN
                INSERT INTO SGRSATT( 
                    SGRSATT_PIDM,
                    SGRSATT_TERM_CODE_EFF,
                    SGRSATT_ATTS_CODE,
                    SGRSATT_ACTIVITY_DATE,
                    SGRSATT_STSP_KEY_SEQUENCE )
                SELECT 
                    P_PIDM,
                    P_TERM_CODE,
                    CASE WHEN V_ATTS_CODE = V_ATTS_CURRENT THEN P_ATTS_CODE ELSE V_ATTS_CODE END,
                    SYSDATE,
                    NULL
                FROM DUAL;
            ELSE
                UPDATE SGRSATT 
                    SET SGRSATT_ATTS_CODE = P_ATTS_CODE
                WHERE SGRSATT_PIDM = P_PIDM
                    AND SGRSATT_TERM_CODE_EFF = P_TERM_CODE
                    AND SGRSATT_ATTS_CODE = V_ATTS_CURRENT;
                EXIT;
            END IF;
        ELSE EXIT;
        END IF;
    END LOOP;
    CLOSE C_SGRSATT_SET;

    END IF;

    COMMIT;

EXCEPTION
WHEN OTHERS THEN
    P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_ATRIBUTO_PLAN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_ASIG_RECONOCIDAS (
    P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_HTML1         OUT VARCHAR2,
    P_HTML2         OUT VARCHAR2,
    P_HTML3         OUT VARCHAR2
)
AS
    V_MESSAGE             EXCEPTION;
    V_HTML                VARCHAR2(4000);
    V_ORIGEN              VARCHAR2(30);
    V_NHTML               NUMBER := 0;
    V_CREDIT_SUM          NUMBER := 0;
    V_CREDIT_SUM_HTML     VARCHAR2(200);  -- PARA MOSTRAR EN EL REPORTE html LA SUMA DE CREDITOS RECONOCIDOS

    V_REQUEST_NO          SMRRQCM.SMRRQCM_REQUEST_NO%TYPE;
    V_CRSE_NUMB           SMRDOUS.SMRDOUS_CRSE_NUMB%TYPE;
    V_CRN                 SMRDOUS.SMRDOUS_CRN%TYPE;
    V_CRN_VAR             VARCHAR2(15);
    V_TITLE               SMRDOUS.SMRDOUS_TITLE%TYPE;
    V_CRSE_SOURCE         SMRDOUS.SMRDOUS_CRSE_SOURCE%TYPE; 
    V_CREDIT_HOURS        SMRDOUS.SMRDOUS_CREDIT_HOURS%TYPE;
    V_GRDE_CODE           SMRDOUS.SMRDOUS_GRDE_CODE%TYPE;


    -- CURSOR GET ultima ejecucion de CAP
    CURSOR C_REQUEST_NO IS
        SELECT SMRRQCM_REQUEST_NO 
        FROM (
            SELECT SMRRQCM_REQUEST_NO
            FROM SMRRQCM
            WHERE SMRRQCM_PIDM = P_PIDM 
            ORDER BY SMRRQCM_REQUEST_NO DESC
        ) WHERE ROWNUM = 1;

    /* GET LIST ASIGNATURAS RECONOCIDAS.
            SMRDOUS_CRSE_SOURCE:   H - History, T - Transfer, R - In-Progress, P - Planned
    */
    CURSOR C_ASIG_CAP IS
        SELECT SMRDOUS_CRSE_NUMB,SMRDOUS_CRN,SMRDOUS_TITLE, SMRDOUS_CRSE_SOURCE, SMRDOUS_CREDIT_HOURS, SMRDOUS_GRDE_CODE
        FROM (
            SELECT SMRDOUS_CRSE_NUMB,SMRDOUS_CRN,SMRDOUS_TITLE, SMRDOUS_CRSE_SOURCE, SMRDOUS_CREDIT_HOURS, SMRDOUS_GRDE_CODE
            FROM SMRDOUS 
            WHERE SMRDOUS_PIDM = P_PIDM
                AND SMRDOUS_REQUEST_NO = V_REQUEST_NO
                AND SMRDOUS_CRSE_SOURCE IN ('H','T')
            ORDER BY SMRDOUS_COMPLIANCE_ORDER
        ) WHERE ROWNUM < 80;

BEGIN

    P_HTML1 := '<tr></tr>';
    P_HTML2 := '<tr></tr>';
    P_HTML3 := '<tr></tr>';

    -- -- CURSOR GET ID ultima ejecucion de CAP   
    OPEN C_REQUEST_NO;
    LOOP
        FETCH C_REQUEST_NO INTO V_REQUEST_NO;
        EXIT WHEN C_REQUEST_NO%NOTFOUND;
    END LOOP;
    CLOSE C_REQUEST_NO;

    -- GET LIST ASIGNATURAS RECONOCIDAS.
    OPEN C_ASIG_CAP;
    LOOP
        FETCH C_ASIG_CAP INTO V_CRSE_NUMB, V_CRN, V_TITLE, V_CRSE_SOURCE, V_CREDIT_HOURS, V_GRDE_CODE;
        IF C_ASIG_CAP%FOUND THEN

            V_CREDIT_SUM := V_CREDIT_SUM + V_CREDIT_HOURS;

            V_HTML := '';
            /* ORIGEN:   H - History, T - Transfer, R - In-Progress, P - Planned*/
            V_ORIGEN := (CASE V_CRSE_SOURCE WHEN 'H' THEN 'Historial' WHEN 'T' THEN 'Transfer' ELSE '-' END);

            V_CRN_VAR := CASE WHEN NVL(TRIM(V_CRN),'-')='-' THEN '-' ELSE V_CRN END;

            /***********************************************************************************************************
            -- Exportar reporte de asignaturas reconocidas
            ***********************************************************************************************************/
            V_HTML := '<tr><td>' || V_CRSE_NUMB || '</td><td>' || V_CRN_VAR || '</td><td>' || V_TITLE || '</td><td>' || V_ORIGEN || '</td><td>' || V_CREDIT_HOURS || '</td><td>' || V_GRDE_CODE || '</td></tr>';

            IF (LENGTH(P_HTML1) + LENGTH(V_HTML) < 4000) AND V_NHTML < 1 THEN
                IF (P_HTML1 = '<tr></tr>') THEN 
                    P_HTML1 := ''; 
                END IF;
                P_HTML1 := P_HTML1 || V_HTML;
            ELSIF (LENGTH(P_HTML2) + LENGTH(V_HTML) < 4000) AND V_NHTML < 2 THEN 
                IF (P_HTML2 = '<tr></tr>') THEN
                    P_HTML2 := '';
                END IF;
                P_HTML2 := P_HTML2 || V_HTML;
                V_NHTML  := 1;
            ELSIF (LENGTH(P_HTML3) + LENGTH(V_HTML) < 4000) AND V_NHTML < 3 THEN 
                IF (P_HTML3 = '<tr></tr>') THEN
                    P_HTML3 := '';
                END IF;
                P_HTML3 := P_HTML3 || V_HTML;
                V_NHTML  := 2;
            ELSE
                RAISE V_MESSAGE;
            END IF;

        ELSE
            EXIT;
        END IF;
    END LOOP;
    CLOSE C_ASIG_CAP;

    V_CREDIT_SUM_HTML := '<tr style="border-top: 1px solid #202629;"><td style="" colspan="4"></td><td style="background-color: #a3a3a3;color: #202629;">'|| V_CREDIT_SUM ||'</td><td></td></tr>';
    --*************************************************************************************************************
    --  SUMA DE CREDITOS 
    IF (LENGTH(P_HTML3) + LENGTH(V_CREDIT_SUM_HTML)) < 4000 THEN 
        P_HTML3 := P_HTML3 || V_CREDIT_SUM_HTML;
    ELSE
        RAISE V_MESSAGE;
    END IF;

EXCEPTION
WHEN V_MESSAGE THEN 
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| '- La cantidad de registros supera la capacidad de los parámetros retornados.' );
WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| '-ERROR- '|| SQLERRM);
END P_GET_ASIG_RECONOCIDAS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VALIDAR_CONVA (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_CONVA      OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
    V_CONVA             NUMBER; --VALIDA SI TIENE ALGUN REGISTRO DE CONVALIDACION
BEGIN

    SELECT COUNT(*)
        INTO V_CONVA
    FROM SHRTRAM
    WHERE SHRTRAM_PIDM = P_PIDM
        AND SHRTRAM_LEVL_CODE = 'PG'
        AND SHRTRAM_TERM_CODE_ENTERED = P_TERM_CODE;

    IF V_CONVA > 0 THEN
        P_STATUS_CONVA := 'TRUE';
        P_MESSAGE := 'El estudiante presenta una convalidación registrada en el periodo ' || P_TERM_CODE || '.';
    ELSE
        P_STATUS_CONVA := 'FALSE';
        P_MESSAGE := 'No tiene registro de alguna convalidación en el sistema.';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VALIDAR_CONVA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_PAGOS_CTA_DUP(
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_NEW_PLAN          IN VARCHAR2,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_CTA        OUT VARCHAR2,
    P_CTA_DESC			OUT VARCHAR2
)
AS
    V_ERROR             EXCEPTION;
    V_APEC_TERM         VARCHAR2(10);
    V_APEC_CAMP	        VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);

    P_RESULT            INTEGER;

BEGIN

    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM 
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE;

    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- VERIFICAR SI TIENE CUENTA CORRIENTE DUPLICADA O NO
    P_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
              'dbo.sp_VerificarCtaCorrienteDup "'
              || 'UCCI' ||'" , "'|| P_ID ||'" , "'|| V_APEC_TERM ||'" , "'|| V_APEC_CAMP ||'" , "'|| P_PROGRAM ||'" , "'|| V_APEC_DEPT ||'" , "'|| P_NEW_PLAN ||'" , "'|| P_CAMBIO_ADM ||'"' 
        );
    COMMIT;

    -- OBTENER RESULTADOS
    SELECT "status", "observacion"
        INTO P_STATUS_CTA, P_CTA_DESC
    FROM ADM.tblRptaValidacionCtaCte@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "pidm" = P_PIDM
        AND "term" = P_TERM_CODE
        AND "idtipoCambioADM" = P_CAMBIO_ADM;

    IF P_STATUS_CTA = '1' THEN
        P_STATUS_CTA := 'TRUE';
    ELSE
        P_STATUS_CTA := 'FALSE';
    END IF;

EXCEPTION
WHEN V_ERROR THEN 
    P_CTA_DESC := '- No se ejecuto correctamente el procedimiento correspondiente de verificación de cuentas corrientes duplicadas.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_CTA_DESC );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_PAGOS_CTA_DUP;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_REGISTRAR_CAMBIO_ADM (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_DEPT_CODE         IN VARCHAR2,
    P_DEPT_CODE_NEW     IN VARCHAR2,
    P_CAMP_CODE         IN VARCHAR2,
    P_CAMP_CODE_NEW     IN VARCHAR2,
    P_MOD_ADM           IN VARCHAR2,
    P_MOD_ADM_NEW       IN VARCHAR2,
    P_PROGRAM           IN VARCHAR2,
    P_PROGRAM_NEW       IN VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
)
AS
    V_CONTADOR          INTEGER;
    V_FECHA             DATE;
BEGIN

    SELECT COUNT(*)
        INTO V_CONTADOR
    FROM ADM.tblCambiosADM@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "pidm" = P_PIDM
        AND "term" = P_TERM_CODE;

    IF V_CONTADOR <= 0 THEN

        V_FECHA := SYSDATE;

        INSERT INTO ADM.tblCambiosADM@BDUCCI.CONTINENTAL.EDU.PE("fechaRegistro", "pidm", "term", "idtipoCambioADM", "departament", "departamentNew", "campus", "campusNew", "modAdm", "modAdmNew", "program", "programNew")
        VALUES (V_FECHA, P_PIDM, P_TERM_CODE, P_CAMBIO_ADM, P_DEPT_CODE, P_DEPT_CODE_NEW, P_CAMP_CODE, P_CAMP_CODE_NEW, P_MOD_ADM, P_MOD_ADM_NEW, P_PROGRAM, P_PROGRAM_NEW);
        COMMIT;

    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_REGISTRAR_CAMBIO_ADM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_INFO_INGRESANTE (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
    P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_DESC      OUT SMRPRLE.SMRPRLE_PROGRAM_DESC%TYPE,
    P_MOD_ADM           OUT VARCHAR2,
    P_MOD_ADM_DESC      OUT VARCHAR2,
    P_CATALOGO          OUT VARCHAR2,
    P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
    P_NAME              OUT VARCHAR2,
    P_FECHA_SOL         OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_INDICADOR         NUMBER;
    V_FECHA_SOL         DATE;
    V_PART_PERIODO      VARCHAR2(2);

    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_CAMPDEPT IS
        SELECT      SORLCUR_CAMP_CODE,
                    SORLCUR_PROGRAM,
                    SORLFOS_DEPT_CODE,
                    STVCAMP_DESC,
                    SORLCUR_TERM_CODE_CTLG
        FROM (
            SELECT  SORLCUR_CAMP_CODE, 
                    SORLCUR_PROGRAM, 
                    SORLFOS_DEPT_CODE, 
                    STVCAMP_DESC,
                    SORLCUR_TERM_CODE_CTLG
            FROM SORLCUR
            INNER JOIN SORLFOS ON 
                SORLCUR_PIDM = SORLFOS_PIDM
                AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
            INNER JOIN STVCAMP ON
                SORLCUR_CAMP_CODE = STVCAMP_CODE
            WHERE SORLCUR_PIDM = P_PIDM 
                AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM = 1;

BEGIN 
    -----------------------------------------------------------------------------
    -- GET FECHA
    SELECT TO_CHAR(SYSDATE,'dd/mm/yyyy'), TRUNC(SYSDATE)
        INTO P_FECHA_SOL, V_FECHA_SOL
    FROM DUAL;

    -----------------------------------------------------------------------------
    -- GET nombres , ID
    SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_PIDM
        INTO P_NAME, P_PIDM
    FROM SPRIDEN 
    WHERE SPRIDEN_ID = P_ID
    AND SPRIDEN_CHANGE_IND IS NULL;

    -----------------------------------------------------------------------------
    -- >> GET DEPARTAMENTO y CAMPUS --
    OPEN C_CAMPDEPT;
    LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_PROGRAM,P_DEPT_CODE,P_CAMP_DESC,P_CATALOGO ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
    END LOOP;
    CLOSE C_CAMPDEPT;

    -----------------------------------------------------------------------------
    -- GET email
    SELECT COUNT(*)
        INTO V_INDICADOR
    FROM GOREMAL 
    WHERE GOREMAL_PIDM = P_PIDM
        AND GOREMAL_STATUS_IND = 'A';

    IF V_INDICADOR = 0 THEN
        P_EMAIL := '-';
    ELSE
        SELECT COUNT(*)
            INTO V_INDICADOR 
        FROM GOREMAL 
        WHERE GOREMAL_PIDM = P_PIDM
            AND GOREMAL_STATUS_IND = 'A' 
            AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe');

        IF (V_INDICADOR > 0) THEN
            SELECT GOREMAL_EMAIL_ADDRESS
                INTO P_EMAIL
            FROM (
                SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND 
                FROM GOREMAL 
                WHERE GOREMAL_PIDM = P_PIDM
                    AND GOREMAL_STATUS_IND = 'A' 
                    AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                ORDER BY GOREMAL_PREFERRED_IND DESC
            ) WHERE ROWNUM <= 1;
        ELSE
            SELECT GOREMAL_EMAIL_ADDRESS
            INTO P_EMAIL 
            FROM (
                SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND
                FROM GOREMAL
                WHERE GOREMAL_PIDM = P_PIDM
                    AND GOREMAL_STATUS_IND = 'A'
                ORDER BY GOREMAL_PREFERRED_IND DESC
            ) WHERE ROWNUM <= 1;
        END IF;
    END IF;

    -----------------------------------------------------------------------------
    -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
    IF (P_DEPT_CODE = '-' OR P_CAMP_CODE = '-') THEN
        P_TERM_CODE := '-';
    ELSE
        SELECT SARADAP_TERM_CODE_ENTRY, SARADAP_ADMT_CODE
        INTO P_TERM_CODE, P_MOD_ADM
        FROM (
            SELECT SARADAP_TERM_CODE_ENTRY, SARADAP_ADMT_CODE
            FROM SARADAP
            WHERE SARADAP_PIDM = P_PIDM
            ORDER BY SARADAP_TERM_CODE_ENTRY DESC
        ) WHERE ROWNUM = 1;
    END IF;
    -----------------------------------------------------------------------------
    -- >> DESC PROGRAM
    SELECT SMRPRLE_PROGRAM_DESC
        INTO P_PROGRAM_DESC
    FROM SMRPRLE
    WHERE SMRPRLE_PROGRAM = P_PROGRAM;
    -----------------------------------------------------------------------------
    -- >> DESC MODALIDAD DE ADMISION
    SELECT STVADMT_DESC
        INTO P_MOD_ADM_DESC
    FROM STVADMT
    WHERE STVADMT_CODE = P_MOD_ADM;
    -----------------------------------------------------------------------------

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_INFO_INGRESANTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_BENEFICIO_PREVIO (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_BEN        OUT VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
)
AS
    V_APEC_TERM         VARCHAR2(10);
    V_APEC_CAMP	        VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);

    V_RESULT            INTEGER;

BEGIN

    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM 
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE;

    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- VERIFICAR SI TIENE CUENTA CORRIENTE DUPLICADA O NO
    V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
              'dbo.sp_VerificarBeneficioPrevio "'
              || 'UCCI' ||'" , "'|| P_ID ||'" , "'|| V_APEC_TERM ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_APEC_DEPT ||'" , "'|| P_CAMBIO_ADM ||'"' 
        );
    COMMIT;

    SELECT "status", "observacion"
        INTO P_STATUS_BEN, P_MESSAGE
    FROM ADM.tblRptaValidBenefPrevio@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "pidm" = P_PIDM
    AND "term" = P_TERM_CODE
    AND "idtipoCambioADM" = P_CAMBIO_ADM;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_BENEFICIO_PREVIO;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--
END BWZKPSPG;

/**********************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**********************************************************************************************/