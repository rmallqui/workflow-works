/*
SET SERVEROUTPUT ON
DECLARE   P_MESSAGE                 VARCHAR2(4000);
BEGIN
    P_SET_BENEFICIO(533127, '2011125473','UREG','S01','201520','CON00001',P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/
/* ===================================================================================================================
  NOMBRE    : P_SET_BENEFICIO
  FECHA     : 10/11/2017
  AUTOR     : Flores Vilcapoma, Brian Yusef
  OBJETIVO  : APEC-BDUCCI: Crea un registro al estudiante sobre el beneficio previamente asignado.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_SET_BENEFICIO ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_IDCONVENIO            IN VARCHAR2,
        P_ERROR                 OUT VARCHAR2
)
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(10);
      V_APEC_TERM             VARCHAR2(10);
      V_APEC_DEPT             VARCHAR2(10);
      V_CONTADOR              NUMBER;
      V_ERROR                 EXCEPTION;
      D_AHORA                 DATE := SYSDATE;
    
BEGIN
--    
    --**************************************************************************************************************

    SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO

    /**********************************************************************************************
        -- CREANDO EL REGISTRO DE ALUMNO BENEFICIO
    ********/
    
    SELECT COUNT("IDAlumno") INTO V_CONTADOR
    FROM BNE.tblAlumnoBeneficio@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDDependencia" = 'UCCI'
    AND "IDEscuelaADM" = V_APEC_DEPT
    AND "IDSede" = V_APEC_CAMP
    AND "IDPerAcad" = V_APEC_TERM
    AND "IDAlumno" = P_ID_ALUMNO
    AND "Activo" = '1';
    
    IF V_CONTADOR > 0 THEN
      RAISE V_ERROR;
    ELSE
      INSERT INTO BNE.tblAlumnoBeneficio@BDUCCI.CONTINENTAL.EDU.PE VALUES ('UCCI', V_APEC_CAMP, V_APEC_TERM, V_APEC_DEPT, P_ID_ALUMNO, 'C', P_IDCONVENIO, 'Convenio x WF', 'WFAUTO', D_AHORA, 1);
      COMMIT;
    END IF;
      
EXCEPTION
  WHEN V_ERROR THEN
        P_ERROR := 'Ya cuenta con un beneficio activo.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_BENEFICIO;