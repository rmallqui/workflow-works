/******************************************************************************/
/* BWZKDECO.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripcion corta: Script para generar el Paquete relacionado a los        */
/*                    procedimientos de Descuento por Convenio                */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creacion del Codigo.                                    EAG 23/NOV/2017 */
/*    Creacion del paquete de Descuento por convenio                          */
/*                                                                            */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene los datos de AUTOSERVICIO       */
/*      ingresados por el alumno: Codigo de Convenio                          */
/*    Procedure P_VERIFICA_OTRO_BENEFICIO: Verifica si el estudiante tiene un */
/*      Beneficio economico activo (Convenio - Descuento - Beca)              */
/*    --Paquete generico                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud   */
/*    Procedure P_GET_USUARIO_DSCTOC: En base a una sede y un rol, el         */
/*      procedimiento obtiene LOS CORREOS del(os) responsable(s) asignados    */
/*      a dicho ROL + SEDE (ROL_SEDE).                                        */
/*    --Paquete generico                                                      */
/*    Procedure P_VERIFICAR_CTA_CTE: Verifica si el estudiante tiene una      */
/*      cuenta corriente activa                                               */
/*    Procedure P_CLAUSULA_DESAPRUEBA: Obtiene si el Convenio tiene Clausula  */
/*      de desaprobado o no.                                                  */
/*    Procedure P_SET_DSCTO: Asigna el descuento correspondiente segun los    */
/*      parametros enviados.                                                  */
/*    Procedure P_SET_BENEFICIO: APEC-BDUCCI: Crea un registro al estudiante  */
/*      sobre el beneficio previamente asignado.                              */
/*    Procedure P_VERIFICA_DEUDA: Verifica si se tiene deuda anterior al      */
/*      periodo solicitado o no.                                              */
/*    Procedure P_VERIFICA_DESAPRUEBA: Verifica si el estudiante en un periodo*/
/*       anterior se desaprobo o no.                                          */
/* 2. Actualizacion Paquete Generico                         BYF  04/FEB/2019 */
/*    Se actualiza los SP´s P_CAMBIO_ESTADO_SOLICITUD, P_VERIFICAR_CTA_CTE    */
/*    para que el codigo se obtenga desde el paquete generico.                */
/*    --------------------                                                    */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 
/* ---------------------------------------------------------------------------------------------
-- buscar objecto
select dbms_metadata.get_ddl('PACKAGE','BWZKCSDPC') from dual

--------------------------------------------------------------------------------------------- */

create or replace PACKAGE baninst1.BWZKDECO AS
/*
 BWZKCSDPC:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti Solicitud Descuento Por Convenio
*/
-- FILE NAME..: BWZKCSDPC.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSDPC
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_GET_STUDENT_INPUTS (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_DEPT_CODE			      IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ID_CONVENIO		      OUT VARCHAR2,
    P_EMPRESA_DESC		    OUT VARCHAR2,
    P_TIPO_BENEFICIO	    OUT VARCHAR2,
    P_DSCTO_MATRIC		    OUT VARCHAR2,
    P_DSCTO_CUOTAS		    OUT	VARCHAR2,
    P_FAMILIARIDAD		    OUT VARCHAR2,
    P_ERROR				        OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_OTRO_BENEFICIO (
    P_PIDM				    IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO			  IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			  IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE			  IN STVTERM.STVTERM_CODE%TYPE,
    P_OTRO_BENEFICIO	OUT VARCHAR2,
    P_DESC_BENEFICIO  OUT VARCHAR2,
    P_ERROR				    OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO_DSCTOC (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE, -- Case sensitive
      P_ROLE_CODE           IN WORKFLOW.ROLE.NAME%TYPE, -- Case sensitive
      P_ROLE_SEDE           OUT VARCHAR2,
      P_ROLE_EMAILS         OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_CTA_CTE (
    P_PIDM					  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO				IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			  IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE	      IN STVTERM.STVTERM_CODE%TYPE,
    P_COD_SEDE        IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CTA_CTE				  OUT VARCHAR2,
    P_DESCRIP_CTA_CTE	OUT VARCHAR2,
    P_ERROR					  OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_CLAUSULA_DESAPRUEBA (
    P_ID_CONVENIO		      IN VARCHAR2,
    P_CLAUSULA_DESAP	    OUT VARCHAR2,
    P_ERROR				        OUT VARCHAR2
    );
    
--------------------------------------------------------------------------------

PROCEDURE P_SET_DSCTO (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_ID_CONVENIO         IN VARCHAR2,
      P_TIPO_BENEFICIO      IN VARCHAR2,
      P_DSCTO_MATRIC        IN NUMBER,
      P_DSCTO_CUOTAS        IN NUMBER,
      P_ERROR               OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_SET_BENEFICIO ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_COD_SEDE              IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_IDCONVENIO            IN VARCHAR2,
        P_ERROR                 OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_DEUDA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_DEUDA               OUT VARCHAR2,
      P_DESC_DEUDA          OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_DESAPRUEBA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_DESAPROBO           OUT VARCHAR2,
      P_ASIG_DESAP          OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
    );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END BWZKDECO;