/******************************************************************************/
/* BWZKDECO.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripcion corta: Script para generar el Paquete relacionado a los        */
/*                    procedimientos de Descuento por Convenio                */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creacion del Codigo.                                    EAG 23/NOV/2017 */
/*    Creacion del paquete de Descuento por convenio                          */
/*                                                                            */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene los datos de AUTOSERVICIO       */
/*      ingresados por el alumno: Codigo de Convenio                          */
/*    Procedure P_VERIFICA_OTRO_BENEFICIO: Verifica si el estudiante tiene un */
/*      Beneficio economico activo (Convenio - Descuento - Beca)              */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud   */
/*    Procedure P_GET_USUARIO_DSCTOC: En base a una sede y un rol, el         */
/*      procedimiento obtiene LOS CORREOS del(os) responsable(s) asignados    */
/*      a dicho ROL + SEDE (ROL_SEDE).                                        */
/*    Procedure P_VERIFICAR_CTA_CTE: Verifica si el estudiante tiene una      */
/*      cuenta corriente activa                                               */
/*    Procedure P_CLAUSULA_DESAPRUEBA: Obtiene si el Convenio tiene Clausula  */
/*      de desaprobado o no.                                                  */
/*    Procedure P_SET_DSCTO: Asigna el descuento correspondiente seg�n los    */
/*      parametros enviados.                                                  */
/*    Procedure P_SET_BENEFICIO: APEC-BDUCCI: Crea un registro al estudiante  */
/*      sobre el beneficio previamente asignado.                              */
/*    Procedure P_VERIFICA_DEUDA: Verifica si se tiene deuda anterior al      */
/*      periodo solicitado o no.                                              */
/*    Procedure P_VERIFICA_DESAPRUEBA: Verifica si el estudiante en un periodo*/
/*       anterior se desaprobo o no.                                          */
/*    --------------------                                                    */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 

create or replace PACKAGE BODY baninst1.BWZKDECO AS
/*
  BWZKCSDPC:
       Paquete Web _ Desarrollo Propio _ Paquete _ DEscuento por COnvenio
*/
-- FILE NAME..: BWZKCSDPC.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSDPC
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              


/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 02/11/2017
  AUTOR     : Flores Vilcapoma Brian
  OBJETIVO  : Obtiene los datos de AUTOSERVICIO ingresados por el alumno: Codigo de Convenio
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_GET_STUDENT_INPUTS (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_DEPT_CODE			      IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ID_CONVENIO		      OUT VARCHAR2,
    P_EMPRESA_DESC		    OUT VARCHAR2,
    P_TIPO_BENEFICIO	    OUT VARCHAR2,
    P_DSCTO_MATRIC		    OUT VARCHAR2,
    P_DSCTO_CUOTAS		    OUT	VARCHAR2,
    P_FAMILIARIDAD		    OUT VARCHAR2,
    P_ERROR				        OUT VARCHAR2
)
AS
    V_ERROR					      EXCEPTION;
    P_CONVENIO_DESC			  VARCHAR2(250);

BEGIN

	SELECT SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        INTO P_ID_CONVENIO, P_CONVENIO_DESC
	FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
	INNER JOIN SVRSRAD ON--------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
	WHERE SVRSRAD_SRVC_CODE = 'SOL019'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
	ORDER BY SVRSVAD_ADDL_DATA_SEQ;

	WITH
	CTE_tblConvenios AS (
        -- GET Convenios
        SELECT  "IDConvenio" IDConvenio,
                "NomEmpresa" NomEmpresa,
                gf."Descripcion" GrdFam, 
                "ClauDesap" ClauDesap
        FROM BNE.tblConvenios@BDUCCI.CONTINENTAL.EDU.PE CO
        INNER JOIN BNE.tblGradFamiliaridad@BDUCCI.CONTINENTAL.EDU.PE GF ON
            co."IDGrdFam" = gf."IDGrdFam"
        WHERE "Activo"='1'
	),
	CTE_tblConveniosDetalle AS (
        -- GET Detalles de Convenio
        SELECT "IDConvenio" IDConvenio,
            "Modalidad" Modalidad, 
            "MatriPorcent" MatriPorcent, 
            "CuotaPorcent" CuotaPorcent,
            CASE WHEN NVL("MatriPorcent",0) > 0 THEN 1 ELSE 0 END BeneficioMat,
            CASE WHEN NVL("CuotaPorcent",0) > 0 THEN 1 ELSE 0 END BeneficioCuo
        FROM BNE.tblConveniosDetalle@BDUCCI.CONTINENTAL.EDU.PE
        WHERE ("MatriPorcent" <> 0 OR "CuotaPorcent" <> 0)
        AND ("MatriPorcent" IS NOT NULL OR "CuotaPorcent" IS NOT NULL)
	)
	SELECT DISTINCT co.NomEmpresa, co.GrdFam, CASE 
        	WHEN cd.BeneficioMat = 1 AND cd.BeneficioCuo = 0 THEN 'M'
            WHEN cd.BeneficioMat = 0 AND cd.BeneficioCuo = 1 THEN 'C'
            WHEN cd.BeneficioMat = 1 AND cd.BeneficioCuo = 1 THEN 'A'
            ELSE 'N' END, cd.MatriPorcent, cd.CuotaPorcent
        INTO P_EMPRESA_DESC, P_FAMILIARIDAD, P_TIPO_BENEFICIO, P_DSCTO_MATRIC, P_DSCTO_CUOTAS
	FROM CTE_tblConvenios co
	INNER JOIN CTE_tblConveniosDetalle cd ON
		co.IDConvenio = cd.IDConvenio
	WHERE co.IDConvenio = P_ID_CONVENIO
		AND Modalidad = P_DEPT_CODE;

	IF P_EMPRESA_DESC IS NULL OR P_EMPRESA_DESC = '' THEN
        RAISE V_ERROR;
    END IF;
	
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontr� datos de la empresa.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_OTRO_BENEFICIO
  FECHA     : 03/11/2017
  AUTOR     : Flores Vilcapoma Brian
  OBJETIVO  : Verifica si el estudiante tiene un Beneficio economico activo (Convenio - Descuento - Beca)
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_VERIFICA_OTRO_BENEFICIO (
    P_PIDM				    IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO			    IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			    IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE			    IN STVTERM.STVTERM_CODE%TYPE,
    P_OTRO_BENEFICIO	    OUT VARCHAR2,
    P_DESC_BENEFICIO        OUT VARCHAR2,
    P_ERROR				    OUT VARCHAR2
)
AS
    V_ERROR         EXCEPTION;
    V_BENEFICIO     VARCHAR2(5);
    V_CONTADOR      NUMBER;
    V_APEC_CAMP     VARCHAR2(10);
    V_APEC_DEPT     VARCHAR2(10);
    V_APEC_TERM     VARCHAR2(10);
BEGIN

    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    SELECT COUNT("IDAlumno")
        INTO V_CONTADOR
    FROM BNE.tblAlumnoBeneficio@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDEscuelaADM" = V_APEC_DEPT
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDAlumno" = P_ID_ALUMNO
        AND "Activo" = '1'
    ORDER BY "FechaRegistro" DESC;
  
    IF V_CONTADOR > 0 THEN
        SELECT "TipoBeneficio"
            INTO V_BENEFICIO
        FROM BNE.tblAlumnoBeneficio@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDEscuelaADM" = V_APEC_DEPT
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDAlumno" = P_ID_ALUMNO
            AND "Activo" = '1'
            AND ROWNUM <= 1
        ORDER BY "FechaRegistro" DESC;
        
        CASE V_BENEFICIO 
        WHEN 'C' THEN
            P_OTRO_BENEFICIO := 'TRUE';
            P_DESC_BENEFICIO := 'Convenio';
        WHEN 'B' THEN
            P_OTRO_BENEFICIO := 'TRUE';
            P_DESC_BENEFICIO := 'Beca/Media Beca';
        WHEN 'D' THEN
            P_OTRO_BENEFICIO := 'TRUE';
            P_DESC_BENEFICIO := 'Descuento';
        ELSE
            P_OTRO_BENEFICIO := 'FALSE';
            P_DESC_BENEFICIO := 'Ninguno';
        END CASE;
      
    ELSE
        P_OTRO_BENEFICIO := 'FALSE';
        P_DESC_BENEFICIO := 'Ninguno';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_OTRO_BENEFICIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/* ===================================================================================================================
  NOMBRE    : P_CAMBIO_ESTADO_SOLICITUD
  FECHA     : 12/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : cambia el estado de la solicitud (XXXXXX)

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_ESTADO, P_AN, P_ERROR); 

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO_DSCTOC
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento obtiene LOS CORREOS del(os) responsable(s) 
              asignados a dicho ROL + SEDE (ROL_SEDE).
  =================================================================================================================== */
PROCEDURE P_GET_USUARIO_DSCTOC (
    P_COD_SEDE           IN STVCAMP.STVCAMP_CODE%TYPE, -- Case sensitive
    P_ROLE_CODE           IN WORKFLOW.ROLE.NAME%TYPE, -- Case sensitive
    P_ROLE_SEDE           OUT VARCHAR2,
    P_ROLE_EMAILS         OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_ROLE_ID                 NUMBER;
    V_ORG_ID                  NUMBER;
    V_Email_Address           VARCHAR2(100);
    
    V_SECCION_EXCEPT          VARCHAR2(50);
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID
        AND ROLE_ID = V_ROLE_ID;
    
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
    P_ROLE_SEDE := P_ROLE_CODE || '_DPC_' || P_COD_SEDE;
    
    -- Obtener el ROL_ID 
    V_SECCION_EXCEPT := 'ROLES';
    
    SELECT ID
        INTO V_ROLE_ID 
    FROM WORKFLOW.ROLE 
    WHERE NAME = P_ROLE_SEDE;
    
    V_SECCION_EXCEPT := '';
    
    -- Obtener el ORG_ID 
    V_SECCION_EXCEPT := 'ORGRANIZACION';
    
    SELECT ID 
        INTO V_ORG_ID
    FROM WORKFLOW.ORGANIZATION
    WHERE NAME = 'Root';
    
    V_SECCION_EXCEPT := '';
    
    -- Obtener los datos de usuarios que relaciona rol y usuario
    V_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
    
    -- #######################################################################
    OPEN C_ROLE_ASSIGNMENT;
    LOOP
        FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
        EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                  
        -- Obtener Datos Usuario
        SELECT Email_Address
            INTO V_Email_Address
        FROM WORKFLOW.WFUSER
        WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
        
        P_ROLE_EMAILS := P_ROLE_EMAILS || V_Email_Address || ',';
        
    END LOOP;
    CLOSE C_ROLE_ASSIGNMENT;
    
    V_SECCION_EXCEPT := '';
    
    -- Extraer el ultimo digito en caso sea un "coma"(,)
    SELECT SUBSTR(P_ROLE_EMAILS,1,LENGTH(P_ROLE_EMAILS) -1)
        INTO P_ROLE_EMAILS
    FROM DUAL
    WHERE SUBSTR(P_ROLE_EMAILS,-1,1) = ',';
      
EXCEPTION
WHEN TOO_MANY_ROWS THEN 
    IF (V_SECCION_EXCEPT = 'ROLES') THEN
        P_ERROR := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROLE_CODE || P_COD_SEDE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
        P_ERROR := '- Se encontraron mas de una ORGANIZACI�N con el mismo nombre.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
        P_ERROR := '- Se encontraron mas de un usuario con el mismo ROL.';
    ELSE  P_ERROR := SQLERRM;
    END IF; 
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN NO_DATA_FOUND THEN
    IF ( V_SECCION_EXCEPT = 'ROLES') THEN
        P_ERROR := '- NO se encontr� el nombre del ROL: ' || P_ROLE_CODE || P_COD_SEDE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
        P_ERROR := '- NO se encontr� el nombre de la ORGANIZACION.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
        P_ERROR := '- NO  se encontr� ningun usuario con esas caracteristicas.';
    ELSE  P_ERROR := SQLERRM;
    END IF; 
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO_DSCTOC;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/* ===================================================================================================================
  NOMBRE    : P_VERIFICAR_CTA_CTE
  FECHA     : 03/11/2017
  AUTOR     : Flores Vilcapoma Brian
  OBJETIVO  : Verifica si el estudiante tiene una cuenta corriente activa
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_VERIFICAR_CTA_CTE (
    P_PIDM			    IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO			IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_COD_SEDE          IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CTA_CTE			OUT VARCHAR2,
    P_DESCRIP_CTA_CTE	OUT VARCHAR2,
    P_ERROR				OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VERIFICAR_CTA_CTE (P_PIDM, P_ID_ALUMNO, P_DEPT_CODE, P_TERM_CODE, P_COD_SEDE, P_CTA_CTE, P_DESCRIP_CTA_CTE, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_CTA_CTE;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/* ===================================================================================================================
  NOMBRE    : P_CLAUSULA_DESAPRUEBA
  FECHA     : 08/11/2017
  AUTOR     : Flores Vilcapoma Brian
  OBJETIVO  : Obtiene si el Convenio tiene Clausula de desaprobado o no.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_CLAUSULA_DESAPRUEBA (
    P_ID_CONVENIO		      IN VARCHAR2,
    P_CLAUSULA_DESAP	    OUT VARCHAR2,
    P_ERROR				        OUT VARCHAR2
)
AS
    V_ERROR					      EXCEPTION;
    V_CONTADOR            NUMBER;
    V_TIPO_CLAUSULA			  VARCHAR2(5);
BEGIN
    SELECT COUNT(*)
        INTO V_CONTADOR
    FROM BNE.tblConvenios@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "Activo"='1'
    AND "IDConvenio" = P_ID_CONVENIO;
    
    IF V_CONTADOR = 0 THEN
        RAISE V_ERROR;
    ELSE
        SELECT "ClauDesap"
            INTO V_TIPO_CLAUSULA
        FROM BNE.tblConvenios@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "Activo"='1'
        AND "IDConvenio" = P_ID_CONVENIO;
    
        IF V_TIPO_CLAUSULA = '1' THEN
            P_CLAUSULA_DESAP := 'SI';
        ELSE
            P_CLAUSULA_DESAP := 'NO';
        END IF;
    END IF;
	
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontr� datos del convenio.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CLAUSULA_DESAPRUEBA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/* ===================================================================================================================
  NOMBRE    : P_SET_DSCTO
  FECHA     : 09/11/2017
  AUTOR     : Flores Vilcapoma, Brian
  OBJETIVO  : Asigna el descuento correspondiente seg�n los parametros enviados

MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
PROCEDURE P_SET_DSCTO (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_ID_CONVENIO         IN VARCHAR2,
    P_TIPO_BENEFICIO      IN VARCHAR2,
    P_DSCTO_MATRIC        IN NUMBER,
    P_DSCTO_CUOTAS        IN NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_RESULT                INTEGER;
    V_INDICADOR             INTEGER;
    V_ERROR                 EXCEPTION;

BEGIN 
    --**************************************************************************************************************
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_COD_SEDE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    SELECT COUNT(cc."IDAlumno")
        INTO V_INDICADOR
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE cc
    INNER JOIN dbo.tblCronogramas@BDUCCI.CONTINENTAL.EDU.PE cr on
        cc."IDSede" = cr."IDSede"
        and cr."IDEscuelaADM" = V_APEC_DEPT
        and SUBSTR(cc."IDSeccionC",-2,2) = cr."Cronograma"
    WHERE "IDDependencia" = 'UCCI' 
        AND cc."IDSede"      = V_APEC_CAMP 
        AND "IDAlumno"    = P_ID_ALUMNO
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDConcepto"  IN ('C00','C01','C02','C03','C04','C05');
    
    IF V_INDICADOR > 0 THEN
        V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
            'BNE.sp_Actualizar_Descuento "'
            || 'UCCI' ||'" , "'|| V_APEC_DEPT ||'" , "'|| V_APEC_TERM ||'" , "'|| P_ID_ALUMNO ||'" , "'|| V_APEC_CAMP ||'" , "'|| P_ID_CONVENIO ||'" , "'|| P_DSCTO_MATRIC ||'" , "'|| P_DSCTO_CUOTAS ||'"' 
        );
        COMMIT; 
    ELSIF(V_INDICADOR > 0) THEN
        RAISE V_ERROR;
    END IF;
           
EXCEPTION
WHEN V_ERROR THEN
    P_ERROR := 'No se encontro los conceptos generados.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_DSCTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/* ===================================================================================================================
  NOMBRE    : P_SET_BENEFICIO
  FECHA     : 10/11/2017
  AUTOR     : Flores Vilcapoma, Brian Yusef
  OBJETIVO  : APEC-BDUCCI: Crea un registro al estudiante sobre el beneficio previamente asignado.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_SET_BENEFICIO ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_COD_SEDE              IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_IDCONVENIO            IN VARCHAR2,
    P_ERROR                 OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_APEC_CAMP             VARCHAR2(10);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_DEPT             VARCHAR2(10);
    V_CONTADOR              NUMBER;
    V_ERROR                 EXCEPTION;
    D_AHORA                 DATE := SYSDATE;
BEGIN
--    
    --**************************************************************************************************************
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_COD_SEDE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM 
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO

    /**********************************************************************************************
        -- CREANDO EL REGISTRO DE ALUMNO BENEFICIO
    ********/
    
    SELECT COUNT("IDAlumno")
        INTO V_CONTADOR
    FROM BNE.tblAlumnoBeneficio@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDDependencia" = 'UCCI'
        AND "IDEscuelaADM" = V_APEC_DEPT
        AND "IDSede" = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDAlumno" = P_ID_ALUMNO
        AND "Activo" = '1';
    
    IF V_CONTADOR > 0 THEN
        RAISE V_ERROR;
    ELSE
        INSERT INTO BNE.tblAlumnoBeneficio@BDUCCI.CONTINENTAL.EDU.PE VALUES ('UCCI', V_APEC_CAMP, V_APEC_TERM, V_APEC_DEPT, P_ID_ALUMNO, 'C', P_IDCONVENIO, 'Convenio x WF', 'WFAUTO', D_AHORA, 1);
        COMMIT;
    END IF;
      
EXCEPTION
WHEN V_ERROR THEN
    P_ERROR := 'Ya cuenta con un beneficio activo.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_BENEFICIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_DEUDA
  FECHA     : 10/11/2017
  AUTOR     : Flores Vilcapoma, Brian
  OBJETIVO  : Verifica si se tiene deuda anterior al periodo solicitado o no.

MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
PROCEDURE P_VERIFICA_DEUDA (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_DEUDA               OUT VARCHAR2,
    P_DESC_DEUDA          OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_APEC_TERM             VARCHAR2(9);
    V_MATR                  INTEGER;
    V_CUOTA                 INTEGER;
    V_ERROR                 EXCEPTION;

BEGIN 
    --**************************************************************************************************************
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        
    SELECT COUNT(cc."IDAlumno")
        INTO V_MATR
    FROM tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE cc
    INNER JOIN DAT_Alumno@BDUCCI.CONTINENTAL.EDU.PE da ON
        cc."IDAlumno" = da."IDAlumno"
    WHERE cc."IDDependencia" = 'UCCI'
        AND "IDPerAcad" < V_APEC_TERM
        AND cc."IDConcepto" = 'C00'
        AND LENGTH(cc."IDSeccionC") = 5
        AND da."DNI" = P_ID_ALUMNO
        AND cc."FecCargo" < SYSDATE
        AND cc."FecProrroga" < SYSDATE
        AND cc."Deuda" > 0;

    SELECT COUNT(cc."IDAlumno")
        INTO V_CUOTA
    FROM tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE cc
    INNER JOIN DAT_Alumno@BDUCCI.CONTINENTAL.EDU.PE da ON
        cc."IDAlumno" = da."IDAlumno"
    WHERE cc."IDDependencia" = 'UCCI'
        AND "IDPerAcad" < V_APEC_TERM
        AND cc."IDConcepto" IN ('C01','C02','C03','C04','C05','C06')
        AND LENGTH(cc."IDSeccionC") = 5
        AND da."DNI" = P_ID_ALUMNO
        AND cc."FecCargo" < SYSDATE
        AND cc."FecProrroga" < SYSDATE
        AND cc."Deuda" > 0;
    
    IF V_MATR > 0 AND V_CUOTA > 0 THEN
        P_DEUDA := 'TRUE';
        P_DESC_DEUDA := 'MATRICULA Y PENSION';
    ELSIF V_MATR > 0 AND V_CUOTA = 0 THEN
        P_DEUDA := 'TRUE';
        P_DESC_DEUDA := 'MATRICULA';      
    ELSIF V_MATR = 0 AND V_CUOTA > 0 THEN
        P_DEUDA := 'TRUE';
        P_DESC_DEUDA := 'PENSION';
    ELSE
        P_DEUDA := 'FALSE';
        P_DESC_DEUDA := 'NINGUNO';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_DEUDA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_DESAPRUEBA
  FECHA     : 10/11/2017
  AUTOR     : Flores Vilcapoma, Brian
  OBJETIVO  : Verifica si el estudiante en un periodo anterior se desaprobo o no.

MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
PROCEDURE P_VERIFICA_DESAPRUEBA (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_DESAPROBO           OUT VARCHAR2,
    P_ASIG_DESAP          OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_CONTADOR            NUMBER := 0;
    V_CURSO               VARCHAR2(100);
    V_ERROR               EXCEPTION;
    
    CURSOR C_CURSOS IS
    WITH 
    Aprob AS (
        SELECT   
        /*+ FULL(S) FULL(E) FULL(M)  */ 
        SMRRQCM_PIDM PIDM,
        S.SMRDOUS_SUBJ_CODE||''||E.SCBCRSE_CRSE_NUMB CODIGO,
        E.SCBCRSE_TITLE CURSO,
        M.SMRRQCM_PROGRAM PROGRAM,
        SMRDOUS_TERM_CODE AS TERM,
        nvl(SMRDOUS_GRDE_CODE, 0) AS SCORE,
        nvl(SCBCRSE_CREDIT_HR_HIGH, 0) CREDITO,
        SMRDOUS_CRSE_SOURCE ORIGEN
        FROM SMRRQCM M
        INNER JOIN SMRDOUS S ON 
            M.SMRRQCM_PIDM=S.SMRDOUS_PIDM
            AND M.SMRRQCM_REQUEST_NO=S.SMRDOUS_REQUEST_NO
        INNER JOIN SCBCRSE E ON 
            S.SMRDOUS_SUBJ_CODE = E.SCBCRSE_SUBJ_CODE
            AND S.SMRDOUS_CRSE_NUMB = E.SCBCRSE_CRSE_NUMB 
        WHERE M.SMRRQCM_REQUEST_NO=
            (
            SELECT MAX(SMRRQCM_REQUEST_NO)
            FROM SMRRQCM
            WHERE SMRRQCM_PIDM=P_PIDM
            AND SMRRQCM_PROGRAM=M.SMRRQCM_PROGRAM
            )
        AND nvl(SUBSTR( SMRDOUS_AREA, 4 , 1 ), 0) <> 'D'
        AND E.SCBCRSE_CRSE_NUMB NOT IN ('CN001','CN002')
        AND S.SMRDOUS_CRSE_SOURCE IN ('H','T')
        AND M.SMRRQCM_PIDM = P_PIDM
    ),
    Desaprob AS (
        SELECT 
        /*+ FULL(N) FULL(E) FULL(M) FULL(N)  */ 
        UNIQUE
        N.SMRDOCN_PIDM PIDM,
        E.SCBCRSE_SUBJ_CODE||''||N.SMRDOCN_CRSE_NUMB CODIGO,
        E.SCBCRSE_TITLE CURSO,
        M.SMRRQCM_PROGRAM PROGRAM,
        SMRDOCN_TERM_CODE AS TERM,
        nvl(SMRDOCN_GRDE_CODE, 0) AS SCORE,
        nvl( SCBCRSE_CREDIT_HR_HIGH , 0) CREDITO,
        SMRDOCN_CRSE_SOURCE ORIGEN
        FROM SMRRQCM M
        INNER JOIN SMRDOCN N ON 
            N.SMRDOCN_PIDM = M.SMRRQCM_PIDM
            AND N.SMRDOCN_REQUEST_NO = M.SMRRQCM_REQUEST_NO
            AND SMRDOCN_GRDE_CODE <= '10' 
            AND SMRDOCN_CRSE_SOURCE IN ('T','H')
        LEFT JOIN SCBCRSE E ON 
            E.SCBCRSE_CRSE_NUMB = N.SMRDOCN_CRSE_NUMB
            AND E.SCBCRSE_SUBJ_CODE = N.SMRDOCN_SUBJ_CODE
        INNER JOIN SORLCUR R ON 
            M.SMRRQCM_PIDM=R.SORLCUR_PIDM
            AND M.SMRRQCM_PROGRAM=R.SORLCUR_PROGRAM
        WHERE M.SMRRQCM_REQUEST_NO=
            (
              SELECT MAX(SMRRQCM_REQUEST_NO)
              FROM SMRRQCM
              WHERE SMRRQCM_PIDM = P_PIDM
              AND SMRRQCM_PROGRAM=M.SMRRQCM_PROGRAM
             )
        AND SORLCUR_LMOD_CODE='LEARNER'
        AND SORLCUR_ROLL_IND = 'Y'
        AND SORLCUR_CACT_CODE ='ACTIVE'
        AND M.SMRRQCM_PIDM = P_PIDM
        AND SMRDOCN_TERM_CODE >=SORLCUR_TERM_CODE
        AND SMRDOCN_TERM_CODE <= NVL(SORLCUR_TERM_CODE_END,'99999')
        AND SORLCUR_TERM_CODE_CTLG<>'000000'
    ),
    TODO AS (
        SELECT * 
        FROM Aprob
        UNION ALL
        SELECT * 
        FROM Desaprob
    ),
    MaxTerm AS (
        SELECT MAX(TERM) TERM
        FROM TODO
        WHERE TERM < P_TERM_CODE
    ),
    DesaprobTerm AS (
        SELECT * 
        FROM DESAPROB DP
        INNER JOIN MaxTerm MT ON
            DP.TERM = MT.TERM
    )
    select CURSO
    from DesaprobTerm;
    
BEGIN

    OPEN C_CURSOS;
    LOOP
        FETCH C_CURSOS INTO V_CURSO;
        EXIT WHEN C_CURSOS%NOTFOUND ;
             
        IF V_CONTADOR = 0 THEN
            P_ASIG_DESAP := to_char(V_CURSO);
            V_CONTADOR := 1;
        ELSE 
            P_ASIG_DESAP := P_ASIG_DESAP || ', ' || to_char(V_CURSO);
            V_CONTADOR := V_CONTADOR + 1;
        END IF;
    END LOOP;
    CLOSE C_CURSOS;
    
    IF V_CONTADOR > 0 THEN
        P_DESAPROBO := 'TRUE';
    ELSE
        P_DESAPROBO := 'FALSE';
        P_ASIG_DESAP := 'NINGUNO';
    END IF;
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_DESAPRUEBA;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKDECO;