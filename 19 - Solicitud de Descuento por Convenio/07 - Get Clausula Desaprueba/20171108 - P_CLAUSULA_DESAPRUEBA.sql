/* ===================================================================================================================
  NOMBRE    : P_CLAUSULA_DESAPRUEBA
  FECHA     : 08/11/2017
  AUTOR     : Flores Vilcapoma Brian
  OBJETIVO  : Obtiene si el Convenio tiene Clausula de desaprobado o no.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_CLAUSULA_DESAPRUEBA (
    P_ID_CONVENIO		      IN VARCHAR2,
    P_CLAUSULA_DESAP	    OUT VARCHAR2,
    P_ERROR				        OUT VARCHAR2
)
AS
    V_ERROR					      EXCEPTION;
    V_CONTADOR            NUMBER;
    V_TIPO_CLAUSULA			  VARCHAR2(5);

BEGIN
  
  SELECT COUNT(*) INTO V_CONTADOR
  FROM BNE.tblConvenios@BDUCCI.CONTINENTAL.EDU.PE 
  WHERE "Activo"='1'
  AND "IDConvenio" = P_ID_CONVENIO;
  
  IF V_CONTADOR = 0 THEN
    RAISE V_ERROR;
  ELSE
    SELECT "ClauDesap" INTO V_TIPO_CLAUSULA
    FROM BNE.tblConvenios@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "Activo"='1'
    AND "IDConvenio" = P_ID_CONVENIO;
    
    IF V_TIPO_CLAUSULA = '1' THEN
      P_CLAUSULA_DESAP := 'SI';
    ELSE
      P_CLAUSULA_DESAP := 'NO';
    END IF;
  END IF;
	
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontr� datos del convenio.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CLAUSULA_DESAPRUEBA;
