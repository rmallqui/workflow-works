/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 02/11/2017
  AUTOR     : Flores Vilcapoma Brian
  OBJETIVO  : Obtiene los datos de AUTOSERVICIO ingresados por el alumno: Codigo de Convenio
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_GET_STUDENT_INPUTS (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_DEPT_CODE			      IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ID_CONVENIO		      OUT VARCHAR2,
    P_EMPRESA_DESC		    OUT VARCHAR2,
    P_TIPO_BENEFICIO	    OUT VARCHAR2,
    P_DSCTO_MATRIC		    OUT VARCHAR2,
    P_DSCTO_CUOTAS		    OUT	VARCHAR2,
    P_FAMILIARIDAD		    OUT VARCHAR2,
    P_ERROR				        OUT VARCHAR2
)
AS
    V_ERROR					      EXCEPTION;
    P_CONVENIO_DESC			  VARCHAR2(250);

BEGIN

	SELECT SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
	INTO P_ID_CONVENIO, P_CONVENIO_DESC
	FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
	INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
	ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
	WHERE SVRSRAD_SRVC_CODE = 'SOL019'
	AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
	ORDER BY SVRSVAD_ADDL_DATA_SEQ;

	WITH
	CTE_tblConvenios AS (
			-- GET Convenios
			SELECT  "IDConvenio" IDConvenio,
					"NomEmpresa" NomEmpresa,
					gf."Descripcion" GrdFam, 
          "ClauDesap" ClauDesap
			FROM BNE.tblConvenios@BDUCCI.CONTINENTAL.EDU.PE CO
      INNER JOIN BNE.tblGradFamiliaridad@BDUCCI.CONTINENTAL.EDU.PE GF ON
        co."IDGrdFam" = gf."IDGrdFam"
			WHERE "Activo"='1'
	),
	CTE_tblConveniosDetalle AS (
			-- GET Detalles de Convenio
			SELECT "IDConvenio" IDConvenio,
				"Modalidad" Modalidad, 
				"MatriPorcent" MatriPorcent, 
				"CuotaPorcent" CuotaPorcent,
				CASE WHEN NVL("MatriPorcent",0) > 0 THEN 1 ELSE 0 END BeneficioMat,
				CASE WHEN NVL("CuotaPorcent",0) > 0 THEN 1 ELSE 0 END BeneficioCuo
			FROM  BNE.tblConveniosDetalle@BDUCCI.CONTINENTAL.EDU.PE
			WHERE ("MatriPorcent" <> 0 OR "CuotaPorcent" <> 0)
			AND ("MatriPorcent" IS NOT NULL OR "CuotaPorcent" IS NOT NULL)
	)
	SELECT DISTINCT co.NomEmpresa, co.GrdFam, CASE 
		WHEN cd.BeneficioMat = 1 AND cd.BeneficioCuo = 0 THEN 'M'
		WHEN cd.BeneficioMat = 0 AND cd.BeneficioCuo = 1 THEN 'C'
		WHEN cd.BeneficioMat = 1 AND cd.BeneficioCuo = 1 THEN 'A'
		ELSE 'N' END, cd.MatriPorcent, cd.CuotaPorcent
	INTO P_EMPRESA_DESC, P_FAMILIARIDAD, P_TIPO_BENEFICIO, P_DSCTO_MATRIC, P_DSCTO_CUOTAS
	FROM CTE_tblConvenios co
	INNER JOIN CTE_tblConveniosDetalle cd ON
		co.IDConvenio = cd.IDConvenio
	WHERE co.IDConvenio = P_ID_CONVENIO
		AND Modalidad = P_DEPT_CODE;

	IF P_EMPRESA_DESC IS NULL OR P_EMPRESA_DESC = '' THEN
	RAISE V_ERROR;
  END IF;
	
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontr� datos de la empresa.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;
