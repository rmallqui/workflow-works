/*
SET SERVEROUTPUT ON
DECLARE P_ERROR VARCHAR2(4000);
 P_DEUDA VARCHAR2(4000);
 P_DESC_DEUDA VARCHAR2(4000);
BEGIN
    P_VERIFICA_DEUDA(533127,'44322404','201720',P_DEUDA,P_DESC_DEUDA, P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_DEUDA);
    DBMS_OUTPUT.PUT_LINE(P_DESC_DEUDA);
    DBMS_OUTPUT.PUT_LINE(P_ERROR);
END;
*/
/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_DEUDA
  FECHA     : 10/11/2017
  AUTOR     : Flores Vilcapoma, Brian
  OBJETIVO  : Verifica si se tiene deuda anterior al periodo solicitado o no.

MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_VERIFICA_DEUDA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_DEUDA               OUT VARCHAR2,
      P_DESC_DEUDA          OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
AS
      -- @PARAMETERS
      V_APEC_TERM             VARCHAR2(9);
      V_MATR                  INTEGER;
      V_CUOTA                 INTEGER;
      V_ERROR                 EXCEPTION;

BEGIN 
    --**************************************************************************************************************
    SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        
    SELECT COUNT(cc."IDAlumno") INTO V_MATR
    FROM tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE cc
    INNER JOIN DAT_Alumno@BDUCCI.CONTINENTAL.EDU.PE da ON
      cc."IDAlumno" = da."IDAlumno"
    WHERE cc."IDDependencia" = 'UCCI'
    AND "IDPerAcad" < V_APEC_TERM
    AND cc."IDConcepto" = 'C00'
    AND LENGTH(cc."IDSeccionC") = 5
    AND da."DNI" = P_ID_ALUMNO
    AND cc."FecCargo" < SYSDATE
    AND cc."FecProrroga" < SYSDATE
    AND cc."Deuda" > 0;

    SELECT COUNT(cc."IDAlumno") INTO V_CUOTA
    FROM tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE cc
    INNER JOIN DAT_Alumno@BDUCCI.CONTINENTAL.EDU.PE da ON
      cc."IDAlumno" = da."IDAlumno"
    WHERE cc."IDDependencia" = 'UCCI'
    AND "IDPerAcad" < V_APEC_TERM
    AND cc."IDConcepto" IN ('C01','C02','C03','C04','C05','C06')
    AND LENGTH(cc."IDSeccionC") = 5
    AND da."DNI" = P_ID_ALUMNO
    AND cc."FecCargo" < SYSDATE
    AND cc."FecProrroga" < SYSDATE
    AND cc."Deuda" > 0;
    
    IF V_MATR > 0 AND V_CUOTA > 0 THEN
      P_DEUDA := 'TRUE';
      P_DESC_DEUDA := 'MATRICULA Y PENSION';
    ELSIF V_MATR > 0 AND V_CUOTA = 0 THEN
      P_DEUDA := 'TRUE';
      P_DESC_DEUDA := 'MATRICULA';      
    ELSIF V_MATR = 0 AND V_CUOTA > 0 THEN
      P_DEUDA := 'TRUE';
      P_DESC_DEUDA := 'PENSION';
    ELSE
      P_DEUDA := 'FALSE';
      P_DESC_DEUDA := 'NINGUNO';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_DEUDA;