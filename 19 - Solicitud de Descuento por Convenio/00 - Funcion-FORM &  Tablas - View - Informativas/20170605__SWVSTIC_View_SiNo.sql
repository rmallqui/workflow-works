CREATE OR REPLACE VIEW "BANINST1".SWVSTIC ("CONFIRM_ID", "CONFIRM") AS 
/* ===================================================================================================================
  NOMBRE    : SWVSTIC
              SWView WorkFlow Solicitud Traslado Interno CONFIRMACION (Si/No)
  FECHA     : 05/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso 
  OBJETIVO  : Confirmación Consejeria Academica
  =================================================================================================================== */
SELECT 1 CONFIRM_ID, 'Si' CONFIRM FROM DUAL UNION
SELECT 0 , 'No' FROM DUAL;