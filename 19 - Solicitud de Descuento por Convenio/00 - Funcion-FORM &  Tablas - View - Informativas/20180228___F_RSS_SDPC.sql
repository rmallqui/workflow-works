--===============================================================
SET SERVEROUTPUT ON
DECLARE
 P_RESULTADO VARCHAR2(4000);
BEGIN
 P_RESULTADO := F_RSS_SDPC('605220','0','0');
 DBMS_OUTPUT.PUT_LINE(P_RESULTADO);
END;
--===============================================================

CREATE OR REPLACE FUNCTION F_RSS_SDPC (

    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2

IS
      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE := 'SOL019';
      V_RETUR               BOOLEAN := FALSE;
      V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE;
      V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;
      V_PROGRAM             SORLCUR.SORLCUR_PROGRAM%TYPE;

      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM                SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
      V_NRC_N               INTEGER;

      V_FEC_PRORRG_INI      DATE;
      V_FEC_PRORRG_FIN      DATE;

      V_FEC_EXA             DATE;
      V_INI_CLS             DATE;

      V_APEC_DEPT           VARCHAR2(10);
      V_ING_TERM            VARCHAR2(10);
      V_CONT_ING            NUMBER;
      V_CONT_ANT            NUMBER;
      V_ROWNUM              NUMBER;

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR GET_DATACURR_C IS

        SELECT    SORLCUR_CAMP_CODE,
                  SORLFOS_DEPT_CODE,
                  SORLCUR_PROGRAM
        FROM (
                SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
                FROM SORLCUR
                INNER JOIN SORLFOS ON
                  SORLCUR_PIDM = SORLFOS_PIDM
                  AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
                WHERE SORLCUR_PIDM = P_PIDM
                    --AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/
                    AND SORLCUR_CACT_CODE = 'ACTIVE'
                    AND SORLCUR_CURRENT_CDE = 'Y'
                ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC)
        WHERE ROWNUM = 1;

      CURSOR GET_PTRM_C IS

          SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V'
                                WHEN 'UPGT' THEN 'W'
                                WHEN 'UREG' THEN 'R'
                                WHEN 'UPOS' THEN '-'
                                WHEN 'ITEC' THEN '-'
                                WHEN 'UCIC' THEN '-'
                                WHEN 'UCEC' THEN '-'
                                WHEN 'ICEC' THEN '-'
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H'
                                WHEN 'F01' THEN 'A'
                                WHEN 'F02' THEN 'L'
                                WHEN 'F03' THEN 'C'
                                WHEN 'V00' THEN 'V'
                                ELSE '9' END SUBPTRM
          FROM STVCAMP,STVDEPT
          WHERE STVDEPT_CODE = V_CODE_DEPT
          AND STVCAMP_CODE = V_CODE_CAMP;

BEGIN 
--   
    -- >> GET DEPARTAMENTO, CAMPUS y PROGRAM --
    OPEN GET_DATACURR_C;
    LOOP
        FETCH GET_DATACURR_C INTO V_CODE_CAMP,V_CODE_DEPT,V_PROGRAM ;
        EXIT WHEN GET_DATACURR_C%NOTFOUND;
    END LOOP;
    CLOSE GET_DATACURR_C;

        ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN GET_PTRM_C;
    LOOP
        FETCH GET_PTRM_C INTO V_SUB_PTRM;
        EXIT WHEN GET_PTRM_C%NOTFOUND;
    END LOOP;
    CLOSE GET_PTRM_C;

    ----TRANSFORMAMOS EL VALOR DE BANNER A APEC
    ----------------------------------------------------------------------------------
    SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = V_CODE_DEPT;-- MODALIDAD

    ----OBTENEMOS EL ULTIMO PERIODO ACTIVO DE INGRESANTES
    SELECT
    MAX("IDPerAcad") INTO V_ING_TERM
    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDDependencia" = 'UCCI'
    AND "Ingresante" = '1'
    AND "Renuncia" = '0'
    AND "IDEscuelaADM" = V_APEC_DEPT
    AND "IDExamen" < SYSDATE;

    ----VERIFICAR SI ES INGRESANTE O NO
    SELECT
    COUNT("IDAlumno") INTO V_CONT_ING
    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDDependencia" = 'UCCI'
    AND "Ingresante" = '1'
    AND "Renuncia" = '0'
    AND "IDEscuelaADM" = V_APEC_DEPT
    AND "IDPerAcad" = V_ING_TERM
    AND "IDAlumno" = (SELECT SPRIDEN_ID FROM SPRIDEN WHERE SPRIDEN_PIDM = P_PIDM);

    IF V_CONT_ING > 0 THEN

      SELECT "IDExamen" INTO V_FEC_EXA
      FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
      WHERE "IDDependencia" = 'UCCI'
      AND "Ingresante" = '1'
      AND "Renuncia" = '0'
      AND "IDEscuelaADM" = V_APEC_DEPT
      AND "IDPerAcad" = V_ING_TERM
      AND "IDAlumno" = (SELECT SPRIDEN_ID FROM SPRIDEN WHERE SPRIDEN_PIDM = P_PIDM)
      AND ROWNUM = 1
      ORDER BY "IDExamen" DESC;

      SELECT SOBPTRM_START_DATE+21 INTO V_INI_CLS
      FROM SOBPTRM
      WHERE SOBPTRM_TERM_CODE = SUBSTR(V_ING_TERM,1,4)||SUBSTR(V_ING_TERM,6,1)||'0'
      AND SOBPTRM_PTRM_CODE = V_SUB_PTRM||'1';

      IF (V_FEC_EXA <= (SYSDATE) AND (SYSDATE) < V_INI_CLS) THEN
        RETURN 'Y';
      ELSE
        RETURN 'N';
      END IF;

    ELSE

      SELECT COUNT(*) INTO V_CONT_ANT
      FROM SFRRSTS
      WHERE SFRRSTS_TERM_CODE = SUBSTR(V_ING_TERM,1,4)||SUBSTR(V_ING_TERM,6,1)||'0'
      AND SFRRSTS_RSTS_CODE = 'RW'
      AND SFRRSTS_PTRM_CODE = V_SUB_PTRM||'1'
      AND SFRRSTS_START_DATE <= (SYSDATE)
      AND SFRRSTS_END_DATE > (SYSDATE);

      IF (V_CONT_ANT > 0) THEN
        RETURN 'Y';
      ELSE
        RETURN 'N';
      END IF;

    END IF;
--
EXCEPTION
  WHEN OTHERS THEN
        RETURN 'N';

END F_RSS_SDPC;
