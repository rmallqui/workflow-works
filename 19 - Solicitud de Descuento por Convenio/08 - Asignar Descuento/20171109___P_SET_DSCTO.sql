/*
SET SERVEROUTPUT ON
DECLARE P_ERROR VARCHAR2(4000);
BEGIN
    P_SET_DSCTO(533127,'2011125473','UREG','S01','201520','A',50, 10, P_ERROR);
END;
*/
/* ===================================================================================================================
  NOMBRE    : BNE.sp_Actualizar_Descuento
  FECHA     : 09/11/2017
  AUTOR     : Flores Vilcapoma, Brian
  OBJETIVO  : Asigna el descuento correspondiente seg�n los parametros enviados

MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_SET_DSCTO (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_ID_CONVENIO         IN VARCHAR2,
      P_TIPO_BENEFICIO      IN VARCHAR2,
      P_DSCTO_MATRIC        IN NUMBER,
      P_DSCTO_CUOTAS        IN NUMBER,
      P_ERROR               OUT VARCHAR2
)
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_RESULT                INTEGER;
      V_INDICADOR             INTEGER;
      V_ERROR                 EXCEPTION;

BEGIN 
    --**************************************************************************************************************
    SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_COD_SEDE;-- CAMPUS 
    SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    SELECT COUNT(cc."IDAlumno") INTO V_INDICADOR
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE cc
    INNER JOIN dbo.tblCronogramas@BDUCCI.CONTINENTAL.EDU.PE cr on
      cc."IDSede" = cr."IDSede"
      and cr."IDEscuelaADM" = V_APEC_DEPT
      and SUBSTR(cc."IDSeccionC",-2,2) = cr."Cronograma"
    WHERE "IDDependencia" = 'UCCI' 
    AND cc."IDSede"      = V_APEC_CAMP 
    AND "IDAlumno"    = P_ID_ALUMNO
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDConcepto"  IN ('C00','C01','C02','C03','C04','C05');
    
    IF V_INDICADOR > 0 THEN
      V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
            'BNE.sp_Actualizar_Descuento "'
            || 'UCCI' ||'" , "'|| V_APEC_DEPT ||'" , "'|| V_APEC_TERM ||'" , "'|| P_ID_ALUMNO ||'" , "'|| V_APEC_CAMP ||'" , "'|| P_ID_CONVENIO ||'" , "'|| P_DSCTO_MATRIC ||'" , "'|| P_DSCTO_CUOTAS ||'"' 
      );
      COMMIT; 
    ELSIF(V_INDICADOR > 0) THEN
      RAISE V_ERROR;
    END IF;
           
EXCEPTION
  WHEN V_ERROR THEN
      P_ERROR := 'No se encontro los conceptos generados.';
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
      ROLLBACK;
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_DSCTO;