

SET SERVEROUTPUT ON
DECLARE
    P_PIDM       SPRIDEN.SPRIDEN_PIDM%TYPE;
    P_PER_CAT    VARCHAR2(4000);
    P_MESSAGE    VARCHAR2(4000);
BEGIN
    P_GET_PERCAT('50601',P_PER_CAT,P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_PIDM||'---'||P_PER_CAT);
END;



CREATE OR REPLACE PROCEDURE P_GET_PERCAT(
    P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PER_CAT    OUT VARCHAR2,
    P_MESSAGE    OUT VARCHAR2
)

/* ===================================================================================================================
  NOMBRE    : P_GET_PERCAT
  FECHA     : 15/01/2018
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Obtener el periodo de cat�logo activo que tiene asigando el estudiante.

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION  
  =================================================================================================================== */

AS
  
    V_PERCAT  SORLCUR.SORLCUR_TERM_CODE%TYPE;
    
BEGIN
 
    SELECT SORLCUR_TERM_CODE_CTLG INTO V_PERCAT
    FROM( SELECT * FROM SORLCUR
          WHERE SORLCUR_PIDM = P_PIDM
                AND SORLCUR_LMOD_CODE = 'LEARNER'
                AND SORLCUR_ROLL_IND = 'Y'
                AND SORLCUR_CACT_CODE = 'ACTIVE'
                ORDER BY SORLCUR_SEQNO DESC
                ) WHERE ROWNUM = 1;
     
    P_PER_CAT := TO_NUMBER(V_PERCAT,'999999');
     
  EXCEPTION
     WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_PERCAT;

