SET SERVEROUTPUT ON
DECLARE
      P_PIDM             SPRIDEN.SPRIDEN_PIDM%TYPE;
      P_TERM_CODE        SFBRGRP.SFBRGRP_TERM_CODE%TYPE ;
      P_DEPT_CODE        STVDEPT.STVDEPT_CODE%TYPE;
      P_CAMP_CODE        STVCAMP.STVCAMP_CODE%TYPE;
      P_MESSAGE          VARCHAR2(4000);
BEGIN
P_SET_GRP_OPEN('133808','201800','UREG','S01',P_MESSAGE);
END;


SELECT * FROM SPRIDEN
WHERE SPRIDEN_ID = '72081166';

DELETE SFBRGRP
WHERE SFBRGRP_PIDM = '82727' AND SFBRGRP_TERM_CODE = '201800';
COMMIT;

CREATE OR REPLACE PROCEDURE P_SET_GRP_OPEN (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
      P_TERM_CODE           IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE , 
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE ,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_set_grupo_inscripcion_abierto
  FECHA     : 27/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asignar el c�digo de grupo de inscripci�n para la rectificaci�n en base al id, la modalidad y a la sede del estudiante.

                          Ejemplo: 99-99WF01
------------------------------------------------------------------------------
    Rectificaci�n               Modalidad                 Sede            
99-99 � Rectificaci�n     R � Regular                   S01 � Huancayo
                          W � Gente que trabaja         F01 � Arequipa 
                          V � Virtual                   F02 � Lima  
                                                        F03 � Cusco
                                                        V00 � Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      P_RPGRP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_RGRP_CODE           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_DEPT_CAMP           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_GROUP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_MODALIDAD_SEDE      VARCHAR2(4);
      P_IND_CAMP            VARCHAR2(1);
      P_INDICADOR_TERM      SFBRGRP.SFBRGRP_TERM_CODE%TYPE;
      
      V_MESSAGE1            EXCEPTION;
      V_MESSAGE2            EXCEPTION;
      V_MESSAGE3            EXCEPTION;
      V_MESSAGE4            EXCEPTION;
      P_INDICADOR           NUMBER;
      P_ROW_UPDATE          NUMBER := 0;
      P_COD_GROUP           VARCHAR2(15); --:= '00-20'; --->>> EN PERIODO "00" ES "00-20"
                                                     --->>> EN CICLO REGULAR ES "99-99"
      CURSOR C_SFBRGRP_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END SUBPTRM                        
                    FROM STVDEPT 
                    WHERE STVDEPT_CODE = P_DEPT_CODE;                                                     
      
BEGIN
--
      P_INDICADOR_TERM := SUBSTR(P_TERM_CODE,5,1);
      
      IF P_INDICADOR_TERM = '0' THEN
         P_COD_GROUP := '00-20';
      ELSE
         P_COD_GROUP := '99-99';
      END IF;
      
      OPEN C_SFBRGRP_PTRM;
           LOOP
               FETCH C_SFBRGRP_PTRM INTO P_IND_CAMP;
                     EXIT WHEN C_SFBRGRP_PTRM%NOTFOUND;
                     END LOOP;
      CLOSE C_SFBRGRP_PTRM;
      
      P_MODALIDAD_SEDE := (P_IND_CAMP||P_CAMP_CODE);
      P_RPGRP_NEW := (P_COD_GROUP||P_MODALIDAD_SEDE);
      
      SELECT COUNT(*) INTO P_INDICADOR
      FROM SFBRGRP
      WHERE SFBRGRP_PIDM = P_PIDM
            AND SFBRGRP_TERM_CODE = P_TERM_CODE;
            
      IF P_INDICADOR = 0 THEN
      
         INSERT INTO SFBRGRP (SFBRGRP_TERM_CODE,          SFBRGRP_PIDM,           SFBRGRP_RGRP_CODE,                 SFBRGRP_USER, 
                              SFBRGRP_ACTIVITY_DATE,      SFBRGRP_USER_ID,        SFBRGRP_DATA_ORIGIN)
                      VALUES (P_TERM_CODE,                P_PIDM,                 P_RPGRP_NEW,                       'WorkFlow',   
                              SYSDATE,                    USER,                   'WFAUTO');
         COMMIT;
      ELSIF P_INDICADOR = 1 THEN
         SELECT SFBRGRP_RGRP_CODE INTO P_RGRP_CODE
         FROM SFBRGRP
         WHERE SFBRGRP_PIDM = P_PIDM
               AND SFBRGRP_TERM_CODE = P_TERM_CODE;
         
         P_GROUP := SUBSTR(P_RGRP_CODE,1,5);
         P_DEPT_CAMP := SUBSTR(P_RGRP_CODE,6,4);
         
         
         IF P_GROUP = P_COD_GROUP AND P_DEPT_CAMP = P_MODALIDAD_SEDE THEN
            RAISE V_MESSAGE1; --
    
         ELSIF P_GROUP <> P_COD_GROUP AND P_DEPT_CAMP = P_MODALIDAD_SEDE THEN
            UPDATE SFBRGRP 
            -- SELECT SFBRGRP_TERM_CODE, SFBRGRP_PIDM, SFBRGRP.* 
            SET SFBRGRP_RGRP_CODE     = P_RPGRP_NEW,
                SFBRGRP_DATA_ORIGIN   = 'WorkFlow',
                SFBRGRP_ACTIVITY_DATE = SYSDATE
            WHERE SFBRGRP_PIDM = P_PIDM
            AND SFBRGRP_TERM_CODE = P_TERM_CODE
            AND EXISTS (
            ------ LA DEFINICION DE LA VISTA SFVRGRP
            SELECT SFBWCTL.SFBWCTL_TERM_CODE,
              SFBWCTL.SFBWCTL_RGRP_CODE,
              SFBWCTL.SFBWCTL_PRIORITY,
              SFRWCTL.SFRWCTL_BEGIN_DATE,
              SFRWCTL.SFRWCTL_END_DATE,
                    SFRWCTL.SFRWCTL_HOUR_BEGIN,
                    SFRWCTL.SFRWCTL_HOUR_END
              FROM SFRWCTL, SFBWCTL
                    WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                    AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                    AND   SFBWCTL.SFBWCTL_RGRP_CODE = P_RPGRP_NEW
                    AND   SFRWCTL.SFRWCTL_TERM_CODE = P_TERM_CODE
            );
            P_ROW_UPDATE := P_ROW_UPDATE + SQL%ROWCOUNT;
            --DBMS_OUTPUT.PUT_LINE(P_ROW_UPDATE);
            IF (P_ROW_UPDATE > 1) THEN
            ROLLBACK;
               RAISE V_MESSAGE2; --
            END IF;

            COMMIT;
         ELSIF P_DEPT_CAMP <> P_MODALIDAD_SEDE THEN
            RAISE V_MESSAGE3; --
         END IF; 
       ELSE
          RAISE V_MESSAGE4; --
       END IF;
       
       EXCEPTION
          WHEN V_MESSAGE1 THEN
            P_MESSAGE := 'Ya tiene asignado el grupo para realizar inscripci�n';
            RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
          WHEN V_MESSAGE2 THEN
            P_MESSAGE := 'Se detecto que posee DOBLE grupo de inscripci�n';
            RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
          WHEN V_MESSAGE3 THEN
            P_MESSAGE := 'El departamento y la sede no coinciden con el grupo que le corresponde para realizar inscripci�n';
            RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
          WHEN V_MESSAGE4 THEN
            P_MESSAGE := 'Se encontraron m�s de un grupo asignado para el mismo periodo acad�mico';
            RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
          WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_GRP_OPEN;
