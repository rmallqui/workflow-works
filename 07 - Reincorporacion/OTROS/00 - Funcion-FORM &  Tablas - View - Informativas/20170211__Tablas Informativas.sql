/******************************************************************************/
/******************************************************************************/
CREATE OR REPLACE VIEW SWVMAJR
 (    SWVMAJR_CODE,
      SWVMAJR_DESCRIPTION) AS
  SELECT SZVMAJR_CODE, SZVMAJR_DESCRIPTION FROM SZVMAJR
  WHERE SZVMAJR_CODE IN 
  ( '103' ,'104' ,'105' ,'106' ,'107' ,'108' ,'109' ,'110' ,'111' ,'112' ,'113' ,'114' ,'308' ,
    '309' ,'310' ,'312' ,'313' ,'314' ,'315' ,'316' ,'317' ,'318' ,'319' ,'501' ,'502' ,'503' ,
    '507' ,'508' ) ORDER BY SZVMAJR_DESCRIPTION;

  COMMENT ON TABLE SWVMAJR IS 'Listado de Carreras, informativo solo para mostrar en WorkFlow.';

/******************************************************************************/
/******************************************************************************/
CREATE OR REPLACE VIEW SWVDEPT 
 (    SWVDEPT_CODE,
      SWVDEPT_DEPT) AS
    SELECT STVDEPT_CODE, 'Presencial' SWVDEPT_DEPT FROM STVDEPT WHERE STVDEPT_CODE = 'UREG' UNION
    SELECT STVDEPT_CODE, 'Semi Presencial (Gente que Trabaja)' FROM STVDEPT WHERE STVDEPT_CODE = 'UPGT' UNION
    SELECT STVDEPT_CODE, 'Semi Presencial (Entorno Virtual)' FROM STVDEPT WHERE STVDEPT_CODE = 'UVIR' ORDER BY 2;

  COMMENT ON TABLE SWVDEPT IS 'Listado de Modalidades(Departamento), solo informativo para mostrar en WorkFlow.';
  COMMENT ON COLUMN SWVDEPT.SWVDEPT_CODE IS 'Codigo Modalidad';
  COMMENT ON COLUMN SWVDEPT.SWVDEPT_DEPT IS 'Nombre de la Modalidad';

/******************************************************************************/
/******************************************************************************/
CREATE OR REPLACE VIEW SWVCAMP
 (    SWVCAMP_CODE,
      SWVCAMP_CAMP) AS
    SELECT STVCAMP_CODE, 'Huancayo' FROM STVCAMP WHERE STVCAMP_CODE = 'S01' UNION
    SELECT STVCAMP_CODE, 'Arequipa' FROM STVCAMP WHERE STVCAMP_CODE = 'F01' UNION
    SELECT STVCAMP_CODE, 'Cuzco' FROM STVCAMP WHERE STVCAMP_CODE = 'F03' ORDER BY STVCAMP_CODE;

COMMENT ON TABLE SWVCAMP IS 'Listado de Sedes, informativo solo para mostrar en WorkFlow.';
COMMENT ON COLUMN SWVCAMP.SWVCAMP_CODE IS 'Codigo Sede';
COMMENT ON COLUMN SWVCAMP.SWVCAMP_CAMP IS 'Nombre de la Sede';
