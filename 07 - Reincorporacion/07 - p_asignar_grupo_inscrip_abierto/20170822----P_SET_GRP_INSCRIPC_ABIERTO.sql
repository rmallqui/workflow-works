/*
drop procedure P_SET_GRP_INSCRIPC_ABIERTO;
GRANT EXECUTE ON P_SET_GRP_INSCRIPC_ABIERTO TO wfobjects;
GRANT EXECUTE ON P_SET_GRP_INSCRIPC_ABIERTO TO wfauto;

set serveroutput on
DECLARE p_error varchar2(64);
begin
  P_SET_GRP_INSCRIPC_ABIERTO('30691','201610','UREG','S01',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;
*/


CREATE OR REPLACE PROCEDURE P_SET_GRP_INSCRIPC_ABIERTO (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_GRP_INSCRIPC_ABIERTO
  FECHA     : 05/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asignar el código de grupo de inscripción del estudiante.

                          Ejemplo: 99-99WF01
------------------------------------------------------------------------------
    Rectificación               Modalidad                 Sede            
99-99 – Rectificación     R – Regular                   S01 – Huancayo
                          W – Gente que trabaja         F01 – Arequipa 
                          V – Virtual                   F02 – Lima  
                                                        F03 – Cusco
                                                        V00 – Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      V_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      V_RPGRP_OLD           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      V_DEPT_CAMP           VARCHAR2(4);
      
      V_INDICADOR           NUMBER := 0;
      V_COD_RECTF           VARCHAR2(15) := '99-99';
      V_ROW_UPDATE          NUMBER := 0;
      V_GRP_INSCRIPCION     EXCEPTION;
      
      -- Si tiene GRUPO INSCRIPCION
      CURSOR C_SFBRGRP_RGRP IS
      SELECT SFBRGRP_RGRP_CODE
      FROM SFBRGRP 
      WHERE SFBRGRP_PIDM = P_PIDM
      AND SFBRGRP_TERM_CODE = P_TERM_CODE;
      
BEGIN
-- 
      ------------------------------------------------------------
      -- Si tiene GRUPO INSCRIPCION
      OPEN C_SFBRGRP_RGRP;
      LOOP
        FETCH C_SFBRGRP_RGRP INTO V_RPGRP_OLD;
        EXIT WHEN C_SFBRGRP_RGRP%NOTFOUND;
      END LOOP;
      CLOSE C_SFBRGRP_RGRP;
      
      
      -- #######################################################################
      -- VALIDACION : CODIGO grupo insc. y datos proporcionados
      SELECT CASE P_DEPT_CODE 
                WHEN 'UVIR' THEN 'V' --#UC-SEMI PRESENCIAL (EV)
                WHEN 'UPGT' THEN 'W' --#UC-SEMI PRESENCIAL (GT)
                WHEN 'UREG' THEN 'R' --#UC-PRESENCIAL
                ELSE '' END 
            || P_CAMP_CODE INTO V_DEPT_CAMP FROM DUAL;
      -- CODIGO DEL GRUPO DE RECTIFICACIÒN : 99-99<cod_modadlidad><cod_sede>
      SELECT CONCAT(V_COD_RECTF,V_DEPT_CAMP) INTO V_RPGRP_NEW FROM DUAL;
      
      
      IF V_RPGRP_OLD IS NULL THEN
            
            -- ASIGNAR GRUPO DE INSCRIPCION - forma SFARGRP
            INSERT INTO SFBRGRP (
                  SFBRGRP_TERM_CODE, 
                  SFBRGRP_PIDM, 
                  SFBRGRP_RGRP_CODE, 
                  SFBRGRP_USER, 
                  SFBRGRP_ACTIVITY_DATE,
                  SFBRGRP_DATA_ORIGIN
              )
            SELECT P_TERM_CODE, P_PIDM, V_RPGRP_NEW, USER, SYSDATE , 'WorkFlow'
            FROM DUAL
            WHERE EXISTS (
                  ------ LA DEFINICION DE LA VISTA SFVRGRP
                  SELECT SFBWCTL.SFBWCTL_TERM_CODE,
                  SFBWCTL.SFBWCTL_RGRP_CODE,
                  SFBWCTL.SFBWCTL_PRIORITY,
                  SFRWCTL.SFRWCTL_BEGIN_DATE,
                  SFRWCTL.SFRWCTL_END_DATE,
                        SFRWCTL.SFRWCTL_HOUR_BEGIN,
                        SFRWCTL.SFRWCTL_HOUR_END
                  FROM SFRWCTL, SFBWCTL
                        WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                        AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                        AND   SFBWCTL.SFBWCTL_RGRP_CODE = V_RPGRP_NEW
                        AND   SFRWCTL.SFRWCTL_TERM_CODE = P_TERM_CODE
            ) AND ROWNUM = 1;
            V_ROW_UPDATE := SQL%ROWCOUNT;
            
      ELSE
            
            -- ASIGNAR GRUPO DE RECTIFICACIÓN
            UPDATE SFBRGRP 
              SET SFBRGRP_RGRP_CODE     = V_RPGRP_NEW,
                  SFBRGRP_USER          = USER,
                  SFBRGRP_ACTIVITY_DATE = SYSDATE
            WHERE SFBRGRP_PIDM = P_PIDM
            AND SFBRGRP_TERM_CODE = P_TERM_CODE
            AND EXISTS (
                  ------ LA DEFINICION DE LA VISTA SFVRGRP
                  SELECT SFBWCTL.SFBWCTL_TERM_CODE,
                    SFBWCTL.SFBWCTL_RGRP_CODE,
                    SFBWCTL.SFBWCTL_PRIORITY,
                    SFRWCTL.SFRWCTL_BEGIN_DATE,
                    SFRWCTL.SFRWCTL_END_DATE,
                          SFRWCTL.SFRWCTL_HOUR_BEGIN,
                          SFRWCTL.SFRWCTL_HOUR_END
                    FROM SFRWCTL, SFBWCTL
                          WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                          AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                          AND   SFBWCTL.SFBWCTL_RGRP_CODE = V_RPGRP_NEW
                          AND   SFRWCTL.SFRWCTL_TERM_CODE = P_TERM_CODE
            );
            V_ROW_UPDATE := SQL%ROWCOUNT;
           
      END IF;
      
      IF V_ROW_UPDATE <> 1 THEN
            RAISE V_GRP_INSCRIPCION;
      END IF;

      COMMIT;

EXCEPTION
  WHEN V_GRP_INSCRIPCION THEN
          P_ERROR := '- No se logró asignar grupo de inscripción, "No data found".';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||SQLERRM );
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM );
END P_SET_GRP_INSCRIPC_ABIERTO;
