USE [BDUCCI]
GO

CREATE TABLE [dbo].[tblCronogramas](
	[IDEscuelaADM] [varchar](4) NOT NULL,
	[IDSede] [varchar](4) NOT NULL,
	[Cronograma] [varchar](2) NOT NULL,
	[Traslado] [varchar](2) NOT NULL,
	[Reincorporacion] [varchar](2) NOT NULL,
	[IDDepartamento] [varchar](4) NULL,
	[IDCampus] [varchar](4) NULL,
	[Pronabec] [int] NOT NULL,
	[Activo] [int] NOT NULL,
 CONSTRAINT [PK_tblCronogramas] PRIMARY KEY CLUSTERED 
(
	[IDEscuelaADM] ASC,
	[IDSede] ASC,
	[Cronograma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO

insert into tblCronogramas values ('ADM','HYO','01','2','1','UREG','S01',0,1)
insert into tblCronogramas values ('ADM','HYO','02','2','1','UREG','S01',0,0)
insert into tblCronogramas values ('ADM','HYO','20','2','1','UREG','S01',1,1)
insert into tblCronogramas values ('ADM','HYO','21','2','1','UREG','S01',1,0)
insert into tblCronogramas values ('ADM','HYO','22','2','1','UREG','S01',1,0)
insert into tblCronogramas values ('ADM','ARQP','16','2','1','UREG','F01',0,1)
insert into tblCronogramas values ('ADG','HYO','03','2','1','UPGT','S01',0,1)
insert into tblCronogramas values ('ADG','HYO','05','2','1','UPGT','S01',0,0)
insert into tblCronogramas values ('ADG','HYO','11','2','1','UPGT','S01',0,0)
insert into tblCronogramas values ('ADG','HYO','12','2','1','UPGT','S01',0,0)
insert into tblCronogramas values ('ADG','ARQP','10','2','1','UPGT','F01',0,1)
insert into tblCronogramas values ('ADG','ARQP','13','2','1','UPGT','F01',0,0)
insert into tblCronogramas values ('ADG','ARQP','17','2','1','UPGT','F01',0,0)
insert into tblCronogramas values ('ADG','CUZ','14','2','1','UPGT','F03',0,1)
insert into tblCronogramas values ('ADV','VIR','08','1','1','UVIR','V00',0,1)
insert into tblCronogramas values ('ADV','VIR','09','1','1','UVIR','V00',0,0)