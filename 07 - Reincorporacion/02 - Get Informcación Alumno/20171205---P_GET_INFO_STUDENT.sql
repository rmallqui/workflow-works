
--set serveroutput on
--DECLARE
--P_PIDM              SPRIDEN.SPRIDEN_PIDM%TYPE;
--P_TERM_CODE         SOBPTRM.SOBPTRM_TERM_CODE%TYPE; 
--P_PART_PERIODO      SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
--P_DEPT_CODE         STVDEPT.STVDEPT_CODE%TYPE;
--P_CAMP_CODE         STVCAMP.STVCAMP_CODE%TYPE;
--P_CAMP_DESC         STVCAMP.STVCAMP_DESC%TYPE;
--P_PROGRAM           SOBCURR.SOBCURR_PROGRAM%TYPE;
--P_EMAIL             GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
--P_NAME              VARCHAR2(4000);
--begin
--  P_GET_INFO_STUDENT('70163334',P_PIDM,P_TERM_CODE,P_PART_PERIODO,P_DEPT_CODE,P_CAMP_CODE,P_CAMP_DESC,P_PROGRAM,P_EMAIL,P_NAME);
--  DBMS_OUTPUT.PUT_LINE(P_PIDM || '---' || P_TERM_CODE || '---' || P_PART_PERIODO || '---' || P_DEPT_CODE || '---' ||  P_CAMP_CODE || '---' || P_CAMP_DESC || '---' || P_PROGRAM || '---' || P_EMAIL || '---' || P_NAME);
--end;


CREATE OR REPLACE PROCEDURE P_GET_INFO_STUDENT (
      P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
      P_PART_PERIODO      OUT SOBPTRM.SOBPTRM_PTRM_CODE%TYPE, 
      P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
      P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
      P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
      P_NAME              OUT VARCHAR2,
      P_FECHA_SOL         OUT DATE
)
/* ===================================================================================================================
  NOMBRE    : P_GET_INFO_STUDENT
  FECHA     : 05/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene información de un usuario(estudiante) 
  
  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION
  001     05/12/2017    KARANA        Se agrega los parámetros de P_TERM_CODE y P_PART_PERIODO para encontrar el periodo 
                                      activo a reincorporar, se hace una relación con la tabla SOBPTRM para restringir las
                                      fechas de inicio a fin de la activación de reincorporaciones.
  002     03/01/2017    KARANA        Se agrega el parámetro de fecha para guardar la fecha en la que se realizó la solicitud         
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_INDICADOR         NUMBER;
      V_PART_PERIODO      VARCHAR2(2);

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLCUR_PROGRAM,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLCUR_PROGRAM, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;


      -- CURSOR GET PERIODO ACTIVO
      CURSOR C_SFRRSTS_PTRM IS
                                SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                                          WHEN 'UPGT' THEN 'W' 
                                                          WHEN 'UREG' THEN 'R' 
                                                          WHEN 'UPOS' THEN '-' 
                                                          WHEN 'ITEC' THEN '-' 
                                                          WHEN 'UCIC' THEN '-' 
                                                          WHEN 'UCEC' THEN '-' 
                                                          WHEN 'ICEC' THEN '-' 
                                                          ELSE '1' END ||
                                        CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                                          WHEN 'F01' THEN 'A' 
                                                          WHEN 'F02' THEN 'L' 
                                                          WHEN 'F03' THEN 'C' 
                                                          WHEN 'V00' THEN 'V' 
                                                          ELSE '9' END SUBPTRM                        
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;

BEGIN 
      -----------------------------------------------------------------------------
      -- GET FECHA
         P_FECHA_SOL := SYSDATE;
         
      -----------------------------------------------------------------------------
      -- GET nombres , ID
      SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_PIDM INTO P_NAME, P_PIDM  FROM SPRIDEN 
      WHERE SPRIDEN_ID = P_ID AND SPRIDEN_CHANGE_IND IS NULL;


      -----------------------------------------------------------------------------
      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_PROGRAM,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;


      -----------------------------------------------------------------------------
      -- GET email
      SELECT COUNT(*) INTO V_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A';
      IF V_INDICADOR = 0 THEN
           P_EMAIL := '-';
      ELSE
              SELECT COUNT(*) INTO V_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A' 
              AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe');            
              IF (V_INDICADOR > 0) THEN
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_EMAIL FROM (
                    SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL 
                    WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A' AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                    ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              ELSE
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_EMAIL FROM (
                      SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL
                      WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A'
                      ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              END IF;
      END IF;

      -----------------------------------------------------------------------------
      -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_DEPT_CODE = '-' OR P_CAMP_CODE = '-') THEN
                  P_TERM_CODE := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    
                    OPEN C_SFRRSTS_PTRM;
                    LOOP
                        FETCH C_SFRRSTS_PTRM INTO V_PART_PERIODO;
                        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
                        END LOOP;
                    CLOSE C_SFRRSTS_PTRM;
                    
                    -- GET PERIODO ACTIVO (Para UVIR y UPGT se agrega parte de periodo 2)
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO V_INDICADOR FROM SFRRSTS
                    INNER JOIN SOBPTRM
                          ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                          AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
                    WHERE SFRRSTS_RSTS_CODE = 'RW' AND TO_DATE(TO_CHAR(P_FECHA_SOL,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN (TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')-14) AND TO_DATE(TO_CHAR(SFRRSTS_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
                    ORDER BY SFRRSTS_TERM_CODE DESC;
                    IF V_INDICADOR = 0 THEN
                          P_TERM_CODE := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE INTO P_TERM_CODE, P_PART_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE
                              FROM SFRRSTS
                              INNER JOIN SOBPTRM
                                    ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                                    AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
                              WHERE SFRRSTS_PTRM_CODE IN (V_PART_PERIODO||'1' , (V_PART_PERIODO || CASE WHEN (P_DEPT_CODE='UVIR' OR P_DEPT_CODE='UPGT') THEN '2' ELSE '-' END) ) 
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              -- fechas de incio de la solicitud este dentro de las fechas programadas 
                              AND TO_DATE(TO_CHAR(P_FECHA_SOL,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN (TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')-14) AND (TO_DATE(TO_CHAR(SOBPTRM_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')+7)
                              ORDER BY SFRRSTS_TERM_CODE 
                          ) WHERE ROWNUM <= 1;

                    END IF;
                    
              END IF;
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_INFO_STUDENT;