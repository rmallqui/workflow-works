/*
SET SERVEROUTPUT ON
DECLARE
   P_PIDM        SPRIDEN.SPRIDEN_PIDM%TYPE;
   P_ATRIB_PLAN  SGRSATT.SGRSATT_ATTS_CODE%TYPE;
   P_MESSAGE     VARCHAR2(4000);
BEGIN
   P_GET_ATRIBUTO_PLAN('75803',P_ATRIB_PLAN,P_MESSAGE);
   DBMS_OUTPUT.PUT_LINE(P_PIDM||'---'||P_ATRIB_PLAN||'---'||P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_GET_ATRIBUTO_PLAN(

   P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
   P_ATRIB_PLAN  OUT SGRSATT.SGRSATT_ATTS_CODE%TYPE,
   P_MESSAGE     OUT VARCHAR2

)

/* ===================================================================================================================
  NOMBRE    : P_GET_ATRIBUTO_PLAN
  FECHA     : 19/01/2018
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Obtener el atributo de plan de estudio del estudiante.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
  
AS 

   V_ERROR       EXCEPTION;
   
   CURSOR C_SGRSATT IS
     SELECT SGRSATT_ATTS_CODE
     FROM (SELECT SGRSATT_ATTS_CODE FROM SGRSATT
          WHERE SGRSATT_PIDM = P_PIDM
          AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
          ORDER BY SGRSATT_TERM_CODE_EFF DESC
          )WHERE ROWNUM = 1;
BEGIN

    OPEN C_SGRSATT;
       FETCH C_SGRSATT INTO P_ATRIB_PLAN;
       IF C_SGRSATT%NOTFOUND THEN
          RAISE V_ERROR;
       END IF;
    CLOSE C_SGRSATT;
    
    EXCEPTION
       WHEN V_ERROR THEN
            P_MESSAGE:='No tiene registrado atributo de plan de estudio';
            RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
       WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ATRIBUTO_PLAN;