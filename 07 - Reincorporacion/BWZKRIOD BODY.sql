/**********************************************************************************************/
/* BWZKRIOD BODY.sql                                                                          */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatizacion del proceso de         */
/*                    Solicitud de Reincorporacion                                            */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creacion del Codigo.                                        LAM             05/DIC/2017 */
/*    --------------------                                                                    */
/*    Creacion del paquete de Reserva de Matricula On Demand                                  */
/*    Procedure P_VALID_ID: Valida si el DNI ingresado existe en la DB-BANNER.                */
/*    Procedure P_GET_INFO_STUDENT: Obtiene informacion del estudiante mediante el ID.        */
/*    Procedure P_VERIF_PER_0: Verifica si el periodo de catalogo es '000000'.                */
/*    Procedure P_VERIF_P001: Verifica si el estudiante tiene el atributo de plan 'P001'.     */
/*    Procedure P_VERIF_PGM_309: Verifica si el programa es 309.                              */
/*    Procedure P_VERIFICA_ESTADO_INACTIVO: Verifica si el status del estudiante en las formas*/
/*              SGASTDN es 'IS' y en el plan de estudio es diferente de 'AS'.                 */
/*    Procedure P_VERIFICA_FECHA_DISPO: Verifica si la peticion se encuentra dentro de las    */
/*              fechas establecidas.                                                          */
/*    Procedure P_VERIFICA_RETENCION: Verificar si la fecha de solicitud se encuentra dentro  */
/*              de las fecha configuradas para el proceso.                                    */
/*    Procedure P_CREATE_CTACORRIENTE: Asigna escala y crea la CTA Corriente de un alumno     */
/*              Valida Cargo de CARNET para poder asignarle.                                  */
/*    Procedure P_SET_CARNE: APEC-BDUCCI: Crea un registro al estudiante con el CONCEPTO.     */
/*    Procedure P_SET_REINCORPORACION: Insertar el registro de nuevo periodo a reincorporar.  */
/*    Procedure P_CONV_ASIGN: Realiza la convalidacion del plan 2007 al 2015                  */
/*    Procedure P_SRI_EXECCAPP: Ejecuta CAPP.                                                 */
/*    Procedure P_SRI_EXECPROY: Ejecuta proyeccion.                                           */
/*    Procedure P_SET_CURSO_INTROD: De acuerdo a la modalidad de estudio insertar el o los    */          
/*              atributos PR o RM - RV.                                                       */
/*    Procedure P_SET_ATRIBUTO_PLAN: Inserta el atributo de plan 2015 (P015).                 */
/*    Procedure P_SET_PERIODO_CAT: Cambio de pla de estudios del 2007 al 2015.                */ 
/*    Procedure P_MOD_RETENCION: Se se realiza cambio de plan se libera la retencion 07, 08,  */
/*              16 y 09 (depende de la modalidad).                                            */
/*    Procedure P_GETUSER_COORDINADOR: Se establecio que el usuario se compone por            */
/*              "COORDINADOR + PROGRAM_CODE + CAMPUS"  - Ejm: COORDINADOR110S01               */
/*    Procedure P_SET_RETENCION: Inserta la retencion 17 - Matricula Presencial.              */
/*    Procedure P_GET_PERCAT: Obtiene el periodo de catalogo.                                 */
/*    Procedure P_GET_ATRIBUTO_PLAN: Obtiene el atributo de plan de estudio - Ejm: P002.      */
/*    Procedure P_GET_CRED_APROB: Obtiene la cantidad de creditos aprobados del ultimo CAPP   */
/*    Procedure P_GET_ASIG_PENDIENTES: Verifica si el estudiante tiene asiganturas pendientes */
/*              en un rango de ciclos especificos de acuerdo a la modalidad de estudio.       */
/*    Procedure P_SET_GRP_OPEN: Inserta el grupo de inscripcion abierto para realizar el      */
/*              proceso de matricula.                                                         */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
  
-- PERMISOS DE EJECUCION
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/


CREATE OR REPLACE PACKAGE BODY BWZKRIOD AS

/* ===================================================================================================================
    NOMBRE    : P_VALID_ID
    FECHA     : 04/12/2017
    AUTOR     : Arana Milla, Karina Lizbeth
    OBJETIVO  : Valida si el DNI ingresado existe en la DB-BANNER. 

=================================================================================================================== */

PROCEDURE P_VALID_ID (
    P_ID            IN  SPRIDEN.SPRIDEN_ID%TYPE,
    P_DNI_VALID     OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VALID_ID (P_ID, P_DNI_VALID);

EXCEPTION
WHEN OTHERS THEN
    P_DNI_VALID:= 'FALSE';
END P_VALID_ID;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_INFO_STUDENT (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
    P_PART_PERIODO      OUT SOBPTRM.SOBPTRM_PTRM_CODE%TYPE, 
    P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
    P_NAME              OUT VARCHAR2,
    P_FECHA_SOL         OUT VARCHAR2
)
AS
BEGIN 

    BWZKPSPG.P_GET_INFO_STUDENT (P_ID, P_PIDM, P_TERM_CODE, P_PART_PERIODO, P_DEPT_CODE, P_CAMP_CODE, P_CAMP_DESC, P_PROGRAM, P_EMAIL, P_NAME, P_FECHA_SOL);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_INFO_STUDENT;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIF_PER_0(
  P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
  P_PER_CAT_0  OUT VARCHAR2,
  P_DESCRIP    OUT VARCHAR2
)
/* ===================================================================================================================
NOMBRE    : P_GET_PER_0
FECHA     : 15/01/2018
AUTOR     : Arana Milla, Karina Lizbeth
OBJETIVO  : verificar si es periodo de catalogo '000000'.

MODIFICACIONES
NRO     FECHA         USUARIO       MODIFICACION  
=================================================================================================================== */
AS

  V_PERCAT  SORLCUR.SORLCUR_TERM_CODE%TYPE;
  V_ERROR   EXCEPTION;

BEGIN

  SELECT SORLCUR_TERM_CODE_CTLG INTO V_PERCAT
  FROM( SELECT * FROM SORLCUR
        WHERE SORLCUR_PIDM = P_PIDM
              AND SORLCUR_LMOD_CODE = 'LEARNER'
              AND SORLCUR_ROLL_IND = 'Y'
              AND SORLCUR_CACT_CODE = 'ACTIVE'
              AND SORLCUR_TERM_CODE_END IS NULL
              ORDER BY SORLCUR_SEQNO DESC
              ) WHERE ROWNUM = 1;
  
  IF V_PERCAT ='000000' THEN
     P_PER_CAT_0 := 'TRUE';
     P_DESCRIP := 'PERIODO DE CATÁLOGO NO VÁLIDO (000000), REINCORPORACIÓN MANUAL - INFORMAR A REGISTROS ACADÉMICOS';
     
  ELSIF V_PERCAT <> '000000' THEN
     P_PER_CAT_0 := 'FALSE';
     P_DESCRIP := 'OK';
  ELSE
     RAISE V_ERROR;
  END IF;

EXCEPTION
   WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No tiene registro de periodo de catálogo');
   WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIF_PER_0;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIF_P001(
    P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_P001       OUT VARCHAR2,
    P_DESCRIP    OUT VARCHAR2
)
AS
    V_ERROR        EXCEPTION;
    V_ATRIB_PLAN   VARCHAR2(6);
    
    CURSOR C_SGRSATT IS
        SELECT SGRSATT_ATTS_CODE
        FROM (
            SELECT SGRSATT_ATTS_CODE
            FROM SGRSATT
            WHERE SGRSATT_PIDM = P_PIDM
                AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
            ORDER BY SGRSATT_TERM_CODE_EFF DESC
        )WHERE ROWNUM = 1;
 
BEGIN
    OPEN C_SGRSATT;
        FETCH C_SGRSATT INTO V_ATRIB_PLAN;
            IF C_SGRSATT%NOTFOUND THEN
                RAISE V_ERROR;
            ELSIF V_ATRIB_PLAN = 'P001' THEN
                P_P001 := 'TRUE';
                P_DESCRIP := 'EL ESTUDIANTE ES DEL PLAN CONAFU (P001), REINCORPORACIÓN MANUAL - COMUNICARSE CON EL DIRECTOR(A) DE CARRERA';
            ELSE
                P_P001 := 'FALSE';
                P_DESCRIP := 'OK';
            END IF;
    CLOSE C_SGRSATT;
       
 EXCEPTION
     WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No tiene registrado atributo de plan de estudio');
     WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);              
END P_VERIF_P001;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIF_PGM_309 (
    P_PIDM     IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PGM_309  OUT VARCHAR2,
    P_DESCRIP  OUT VARCHAR2
)
AS

    V_PROGRAM  SORLCUR.SORLCUR_PROGRAM%TYPE;
 
BEGIN
 
    SELECT SORLCUR_PROGRAM INTO V_PROGRAM
    FROM(
        SELECT *
        FROM SORLCUR
        WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_ROLL_IND = 'Y'
            AND SORLCUR_CACT_CODE = 'ACTIVE'
            AND SORLCUR_TERM_CODE_END IS NULL
        ORDER BY SORLCUR_SEQNO DESC
              ) WHERE ROWNUM = 1;
              
    IF V_PROGRAM = '309' THEN
        P_PGM_309 := 'TRUE';
        P_DESCRIP := 'EL ESTUDIANTE ES DEL PROGRAMA (309), REINCORPORACIÓN MANUAL - COMUNICARSE CON EL DIRECTOR(A) DE CARRERA';
    ELSE
        P_PGM_309 := 'FALSE';
        P_DESCRIP := 'OK';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIF_PGM_309;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_ESTADO_INACTIVO (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ESTADO            OUT VARCHAR2,
    P_DESCRIP           OUT VARCHAR2
)
/* ===================================================================================================================
NOMBRE    : P_VERIFICA_ESTADO_INACTIVO
FECHA     : 15/08/2017
AUTOR     : Mallqui Lopez, Richard Alfonso
OBJETIVO  : Valida los datos segun las reglas establecidas para el FLUJO de REINCORPORACIÓN

=================================================================================================================== */
AS
    -- @PARAMETERS
    V_INDICADOR         NUMBER;
    V_STST_CODE         SGBSTDN.SGBSTDN_STST_CODE%TYPE;
    V_STSP_CODE         SGRSTSP.SGRSTSP_STSP_CODE%TYPE;
    V_TERM_OLD          STVTERM.STVTERM_CODE%TYPE;
    V_SUB_PTRM          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
    V_TERM_VALIDO       NUMBER; -- (0 / 1) Si el periodo ingreasado es valido

    -- GET estatus alumno FORMA GASTDN
    CURSOR C_SGBSTDN_STST IS
        SELECT SGBSTDN_TERM_CODE_EFF, SGBSTDN_STST_CODE
        FROM (
              SELECT SGBSTDN_TERM_CODE_EFF, SGBSTDN_STST_CODE 
              FROM SGBSTDN 
              WHERE SGBSTDN_PIDM = P_PIDM
              ORDER BY SGBSTDN_TERM_CODE_EFF DESC
        ) WHERE ROWNUM = 1;
    
    -- GET estatus alumno de BANNER-->PLAN ESTUDIO
    CURSOR C_SGRSTSP_STSP IS
        SELECT SGRSTSP_STSP_CODE
        FROM(
            SELECT SGRSTSP_STSP_CODE
            FROM SGRSTSP 
            WHERE SGRSTSP_PIDM = P_PIDM
            AND SGRSTSP_TERM_CODE_EFF = V_TERM_OLD
            ORDER BY SGRSTSP_KEY_SEQNO DESC
        ) WHERE ROWNUM = 1;
    
    -- Calculando parte PERIODO (sub PTRM)
    CURSOR C_SFRRSTS_PTRM IS
        SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2) SUBPTRM
        FROM CZRPTRM 
        WHERE CZRPTRM_DEPT = P_DEPT_CODE
        AND CZRPTRM_CAMP_CODE = P_CAMP_CODE;

BEGIN 
    
    --P_ESTADO:= 0;
    
    -----------------------------------------------------------------------------
    -- >> GET ESTADIO DEL ALUMNO --
    OPEN C_SGBSTDN_STST;
    LOOP
        FETCH C_SGBSTDN_STST INTO V_TERM_OLD, V_STST_CODE;
        EXIT WHEN C_SGBSTDN_STST%NOTFOUND;
    END LOOP;
    CLOSE C_SGBSTDN_STST;
    
    -----------------------------------------------------------------------------
    -- GET estatus alumno de BANNER-->PLAN ESTUDIO
    OPEN C_SGRSTSP_STSP;
    LOOP
        FETCH C_SGRSTSP_STSP INTO V_STSP_CODE;
        EXIT WHEN C_SGRSTSP_STSP%NOTFOUND;
    END LOOP;
    CLOSE C_SGRSTSP_STSP;

    -- >> calculando PARTE PERIODO  --
    OPEN C_SFRRSTS_PTRM;
    LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFRRSTS_PTRM;
    /******************************************************************************************************
      GET TERM (PERIODO - Solo usando parte de periodo 1) 
           DESDE INICIO DE MATRICULA(SFRRSTS_START_DATE)
           - HASTA FINALIZAR EL PERIODO DE CLASES(SOBPTRM_END_DATE)
    */
    SELECT COUNT(SOBPTRM_TERM_CODE)
        INTO V_TERM_VALIDO 
    FROM (
        -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
        SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
        FROM SOBPTRM
        INNER JOIN SFRRSTS ON
            SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
        WHERE SFRRSTS_RSTS_CODE = 'RW'
            AND SOBPTRM_PTRM_CODE IN (V_SUB_PTRM || '1',(V_SUB_PTRM || CASE WHEN (P_DEPT_CODE='UVIR' OR P_DEPT_CODE='UPGT') THEN '2' ELSE '-' END) ) -- Solo parte de periodo '%1'
            -- AND SOBPTRM_PTRM_CODE NOT LIKE '%0'         -- VERANO
            AND SOBPTRM_TERM_CODE = P_TERM_CODE
            AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN (TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')-14) AND (TO_DATE(TO_CHAR(SOBPTRM_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')+21)
    ) WHERE ROWNUM <= 1;

    /******************************************************************************************************/
    -- CHECK REGLAS
    
    IF (V_STST_CODE = 'IS' OR V_STSP_CODE <> 'AS') AND (V_TERM_VALIDO > 0 OR V_TERM_OLD = P_TERM_CODE) THEN
       P_ESTADO := 'TRUE';
       P_DESCRIP := 'OK';
    ELSIF (V_STST_CODE = 'AS') AND (V_STSP_CODE = 'AS') AND (V_TERM_OLD = P_TERM_CODE) THEN
       P_ESTADO := 'FALSE';
       P_DESCRIP := 'YA TIENE ESTADO REINCORPORADO';
    ELSE
       P_ESTADO := 'FALSE';
       P_DESCRIP := 'ESTADO NO APTO PARA REALIZAR REINCORPORACIÓN';
    END IF;
       
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_ESTADO_INACTIVO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_FECHA_DISPO (
    P_TERM_CODE      IN SOBPTRM.SOBPTRM_TERM_CODE%TYPE,
    P_FECHA_SOL      IN VARCHAR2,
    P_PART_PERIODO   IN SOBPTRM.SOBPTRM_PTRM_CODE%TYPE,
    P_FECHA_VALIDA   OUT VARCHAR2,
    P_DESCRIP        OUT VARCHAR2 
)
AS
BEGIN

    BWZKPSPG.P_VERIFICA_FECHA_DISPO (P_TERM_CODE, P_FECHA_SOL, P_PART_PERIODO, P_FECHA_VALIDA, P_DESCRIP);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_FECHA_DISPO; 

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_RETENCION (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_RET_CODE        IN SPRHOLD.SPRHOLD_HLDD_CODE%TYPE,
    P_FECHA_SOL       IN VARCHAR2,
    P_RETENCION       OUT VARCHAR2,
    P_DESCRIP         OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
)
AS
    V_INDICADOR          NUMBER;
BEGIN
  
    SELECT COUNT(*)
        INTO V_INDICADOR
    FROM SPRHOLD
    WHERE SPRHOLD_HLDD_CODE = P_RET_CODE
       AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS') > TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS')
       AND SPRHOLD_PIDM = P_PIDM;
    
    IF V_INDICADOR = 0 THEN
        P_RETENCION := 'FALSE';
        P_DESCRIP := 'OK';
    ELSE
        P_RETENCION := 'TRUE';
        P_DESCRIP := 'TIENE RETENCIÓN POR DOCUMENTO PENDIENTE';
    END IF;
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_RETENCION;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CREATE_CTACORRIENTE (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE
)
/* ===================================================================================================================
NOMBRE    : P_CREATE_CTACORRIENTE
FECHA     : 23/08/2017
AUTOR     : Mallqui Lopez, Richard Alfonso
OBJETIVO  : Asigna escala y crea la CTA Corriente de un alumno
            Valida Cargo de CARNET para poder asignarle.

MODIFICACIONES
NRO   FECHA         USUARIO       MODIFICACION
001   08/11/2017    KARANA        Se agrego el filtro de NuevoPlan=0
=================================================================================================================== */
AS
    -- @PARAMETERS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_SECCIONC         VARCHAR2(20);
    V_NUM_CTACORRIENTE      NUMBER;
    V_PART_PERIODO          VARCHAR2(9);
    V_RESULT                INTEGER;

    V_ESCALA                VARCHAR2(9);
    V_CRONOGRAMA            VARCHAR2(9);
    V_OBSERVACION           VARCHAR(255) := 'Asignacion de escala - Reincorporacion WORKFLOW';
BEGIN 
--
    --**************************************************************************************************************
    -- GET CRONOGRAMA SECCIONC
    SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
        INTO V_PART_PERIODO
    FROM CZRPTRM 
    WHERE CZRPTRM_DEPT = P_DEPT_CODE
    AND CZRPTRM_CAMP_CODE = P_CAMP_CODE;

    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI 
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    -- Validar si el estuidnate tiene CTA CORRIENTE. como C00(CONCPETO MATRICULA)
    WITH 
    CTE_tblSeccionC AS (
        -- GET SECCIONC
        SELECT  "IDSeccionC" IDSeccionC,
              "FecInic" FecInic
        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia"='UCCI'
        AND "IDsede"    = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuela" = P_PROGRAM
        AND LENGTH("IDSeccionC") = 5
        AND SUBSTRB("IDSeccionC",-2,2) IN (
          -- PARTE PERIODO           
          SELECT CZRPTRM_PTRM_BDUCCI
          FROM CZRPTRM
          WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
        )
    )
    SELECT COUNT("IDAlumno")
        INTO V_NUM_CTACORRIENTE
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno"    = P_ID
    AND "IDSede"      = V_APEC_CAMP
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDEscuela"   = P_PROGRAM
    AND "IDSeccionC"  = V_APEC_SECCIONC
    AND "IDConcepto" = 'C00'; -- C00 Concepto Matricul
    
    -- Get ESCALA y CRONOGRAMA
    SELECT "Cronograma", "Reincorporacion" REINCORPORACION
        INTO V_CRONOGRAMA, V_ESCALA
    FROM tblCronogramas@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDepartamento" = P_DEPT_CODE
    AND "IDCampus" = P_CAMP_CODE
    AND "Activo" = 1
    AND "Pronabec" <> 1
    AND "NuevoPlan" = 0;
    
    -----------------------------------------------------------------------------------------------
    --- Asignacion de escala y Creacion CTA CORRIENTE.
    IF(V_NUM_CTACORRIENTE = 0) THEN
        ---------------------------------------
        /* OBSERVACION: Para pruebas acceder al SCRIPT y comentar y descomentar la conexcion de "AT BANNER" --> "AT MIGR"
              dentro del SP : sp_ActualizarMontoPagarBanner(YA INCLUIDO en el SP dbo.sp_AsignarEscala_CtaCorriente)
        */
        ---------------------------------------
        V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
              'dbo.sp_AsignarEscala_CtaCorriente "'
              || 'UCCI' ||'" , "'|| P_ID ||'" , "'|| V_APEC_TERM ||'" , "'|| V_APEC_DEPT ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_ESCALA ||'" , "'|| V_CRONOGRAMA ||'" , "'|| V_OBSERVACION ||'" , "'|| USER ||'"' 
        );
    END IF;
    
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CREATE_CTACORRIENTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CARNE ( 
      P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
      P_APEC_CONCEPTO         IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
      P_PROGRAM               IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_MESSAGE               OUT VARCHAR2
)
/* ===================================================================================================================
NOMBRE    : P_SET_CARGO_CONCEPTO
FECHA     : 23/08/2017
AUTOR     : Mallqui Lopez, Richard Alfonso
OBJETIVO  : APEC-BDUCCI: Crea un registro al estudiante con el CONCEPTO.

MODIFICACIONES
NRO   FECHA   USUARIO   MODIFICACION
=================================================================================================================== */
AS
        -- @PARAMETERS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_SECCIONC         VARCHAR2(20);
    V_PART_PERIODO          VARCHAR2(9);
    V_CARGO_CARNET          NUMBER := 0;
    P_INDICADOR_TERM        STVTERM.STVTERM_CODE%TYPE;
    V_MONTO_CAR             NUMBER := 0;
    
  
BEGIN
--    
    P_INDICADOR_TERM := SUBSTR(P_TERM_CODE,5,1);
    
    IF P_INDICADOR_TERM <> '0' THEN
--**************************************************************************************************************
        -- GET CRONOGRAMA SECCIONC
        SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
            INTO V_PART_PERIODO
        FROM CZRPTRM 
        WHERE CZRPTRM_DEPT = P_DEPT_CODE
        AND CZRPTRM_CAMP_CODE = P_CAMP_CODE;
        
        SELECT CZRCAMP_CAMP_BDUCCI
            INTO V_APEC_CAMP 
        FROM CZRCAMP 
        WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        
        SELECT CZRTERM_TERM_BDUCCI
            INTO V_APEC_TERM 
        FROM CZRTERM
        WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        
        SELECT CZRDEPT_DEPT_BDUCCI
            INTO V_APEC_DEPT
        FROM CZRDEPT
        WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
        
        --Get SECCIONC
        WITH 
        CTE_tblSeccionC AS (
            -- GET SECCIONC
            SELECT  "IDSeccionC" IDSeccionC,
                    "FecInic" FecInic
            FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia"='UCCI'
            AND "IDsede"    = V_APEC_CAMP
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuela" = P_PROGRAM
            AND LENGTH("IDSeccionC") = 5
            AND SUBSTRB("IDSeccionC",-2,2) IN (
                -- PARTE PERIODO           
                SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
            )
        )
        SELECT "IDSeccionC"
            INTO V_APEC_SECCIONC
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    = P_ID
            AND "IDSede"      = V_APEC_CAMP
            AND "IDPerAcad"   = V_APEC_TERM
            AND "IDEscuela"   = P_PROGRAM
            AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
            AND "IDConcepto" = 'C00'; -- C00 Concepto Matricula
        
        /*******
           -- Get CARGO de CARNET "CAR"
        ********/
        SELECT  "Monto"
            INTO V_CARGO_CARNET
        FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
        INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t2 ON 
            t1."IDSede"          = t2."IDsede"
            AND t1."IDPerAcad"      = t2."IDPerAcad" 
            AND t1."IDDependencia"  = t2."IDDependencia" 
            AND t1."IDSeccionC"     = t2."IDSeccionC" 
            AND t1."FecInic"        = t2."FecInic"
        WHERE t2."IDDependencia" = 'UCCI'
            AND t2."IDSeccionC"    = V_APEC_SECCIONC
            AND LENGTH(t2."IDSeccionC") = 5
            AND t2."IDPerAcad"     = V_APEC_TERM
            AND T1."IDPerAcad"     = V_APEC_TERM
            AND t1."IDConcepto"    = P_APEC_CONCEPTO;
          
        SELECT "Cargo"
            INTO V_MONTO_CAR
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI' 
            AND "IDAlumno"    = P_ID
            AND "IDSede"      = V_APEC_CAMP 
            AND "IDPerAcad"   = V_APEC_TERM 
            AND "IDEscuela"   = P_PROGRAM
            AND "IDSeccionC"  = V_APEC_SECCIONC
            AND "IDConcepto"  = P_APEC_CONCEPTO;
        
        IF V_MONTO_CAR = 0 THEN
          
            /**********************************************************************************************
            -- ASINGANDO el cargo CARNET a CTACORRIENTE
            ********/
            UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                SET "Cargo" = "Cargo" + V_CARGO_CARNET  
            WHERE "IDDependencia" = 'UCCI' 
                AND "IDAlumno"    = P_ID
                AND "IDSede"      = V_APEC_CAMP 
                AND "IDPerAcad"   = V_APEC_TERM 
                AND "IDEscuela"   = P_PROGRAM
                AND "IDSeccionC"  = V_APEC_SECCIONC
                AND "IDConcepto"  = P_APEC_CONCEPTO; 
        
            COMMIT;
        END IF;
    END IF;
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CARNE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_REINCORPORACION(
    P_PIDM        IN	SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE   IN	STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE     OUT	VARCHAR2
)
AS
    V_SGBSTDN_REC           SGBSTDN%ROWTYPE;
    V_SGRSTSP_REC           SGRSTSP%ROWTYPE;
    V_SORLCUR_REC           SORLCUR%ROWTYPE; --ADD
    V_SORLFOS_REC           SORLFOS%ROWTYPE; --ADD
    V_SORLCUR_SEQNO_INC     SORLCUR.SORLCUR_SEQNO%TYPE; -- ADD
    P_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE; -- ADD 
    P_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE; -- ADD
    V_STSP_CODE             SGRSTSP.SGRSTSP_STSP_CODE%TYPE;
    V_STST_CODE             SGBSTDN.SGBSTDN_STST_CODE%TYPE; 
    V_STYP_CODE             SGBSTDN.SGBSTDN_STYP_CODE%TYPE;
   
    -- GET FORMA SGASTDN
    CURSOR C_SGBSTDN IS
        SELECT *
        FROM (
            SELECT *
            FROM SGBSTDN 
            WHERE SGBSTDN_PIDM = P_PIDM
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         ) WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN 
            WHERE SGBSTDN_PIDM = P_PIDM
            AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE);
          
    -- GET estatus alumno de BANNER-->PLAN ESTUDIO
    CURSOR C_SGRSTSP IS
        SELECT * FROM (
            SELECT * 
            FROM SGRSTSP 
            WHERE SGRSTSP_PIDM = P_PIDM
            ORDER BY SGRSTSP_TERM_CODE_EFF DESC
        ) WHERE ROWNUM = 1;
      
    -- AGREGAR NUEVO REGISTRO
    CURSOR C_SORLCUR IS
        SELECT *
        FROM (
            SELECT * 
            FROM SORLCUR 
            WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        )WHERE ROWNUM = 1
        AND NOT EXISTS (
            SELECT *
            FROM SORLCUR
            WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_TERM_CODE = P_TERM_CODE);
        
BEGIN

    -- INSERT  ------> SGBSTDN -
    
    OPEN C_SGBSTDN;
        FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
        IF C_SGBSTDN%FOUND THEN
         INSERT INTO SGBSTDN (
                    SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                    SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                    SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                    SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                    SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                    SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                    SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                    SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                    SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                    SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                    SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                    SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                    SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                    SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                    SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                    SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                    SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                    SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                    SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                    SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                    SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                    SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                    SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                    SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                    SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                    SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                    SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                    SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                    SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                    SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                    SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                    SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                    SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                    SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                    SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                    SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                    SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
          VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_TERM_CODE,                                  'AS',
                    V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            'R',                                          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                    V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                    V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                    V_SGBSTDN_REC.SGBSTDN_COLL_CODE_1,          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_1,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1,
                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                    V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                    V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                    V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                    V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                    SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                    V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                    V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                    V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                    V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                    V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                    V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                    V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                    V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        V_SGBSTDN_REC.SGBSTDN_PROGRAM_1,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_1,
                    V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                    V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                    V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                    V_SGBSTDN_REC.SGBSTDN_CURR_RULE_1,          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_1,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                    V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                    V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                    V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                    V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                    V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                    V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                    V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                    V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                    USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
                    
                    COMMIT;
                    
          ELSE
          
           SELECT SGBSTDN_STST_CODE, SGBSTDN_STYP_CODE
            INTO V_STST_CODE, V_STYP_CODE
           FROM SGBSTDN
           WHERE SGBSTDN_PIDM = P_PIDM
                 AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
              
              IF V_STST_CODE = 'IS' AND V_STYP_CODE <> 'R' THEN
                 
                 UPDATE SGBSTDN
                 SET SGBSTDN_STST_CODE = 'AS',
                     SGBSTDN_STYP_CODE = 'R',
                     SGBSTDN_ACTIVITY_DATE = SYSDATE,
                     SGBSTDN_DATA_ORIGIN = 'WorkFlow',
                     SGBSTDN_USER_ID = USER
                 WHERE SGBSTDN_PIDM = P_PIDM
                       AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
                    
                 COMMIT;
              
              ELSIF V_STST_CODE = 'AS' AND V_STYP_CODE <> 'R' THEN
                 
                 UPDATE SGBSTDN
                 SET SGBSTDN_STYP_CODE = 'R',
                     SGBSTDN_ACTIVITY_DATE = SYSDATE,
                     SGBSTDN_DATA_ORIGIN = 'WorkFlow',
                     SGBSTDN_USER_ID = USER
                 WHERE SGBSTDN_PIDM = P_PIDM
                       AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
                    
                 COMMIT;
               
               ELSIF V_STST_CODE = 'IS' AND V_STYP_CODE = 'R' THEN
               
                  UPDATE SGBSTDN
                  SET SGBSTDN_STST_CODE = 'AS',
                     SGBSTDN_ACTIVITY_DATE = SYSDATE,
                     SGBSTDN_DATA_ORIGIN = 'WorkFlow',
                     SGBSTDN_USER_ID = USER
                  WHERE SGBSTDN_PIDM = P_PIDM
                        AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
                  COMMIT;
               END IF;
          END IF;       
    CLOSE C_SGBSTDN;

    OPEN C_SGRSTSP;
         FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
         --IF C_SGRSTSP%FOUND THEN
         INSERT INTO SGRSTSP (
                    SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                    SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                    SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                    SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                    SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                    SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                    SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                    SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                    SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
            SELECT  V_SGRSTSP_REC.SGRSTSP_PIDM,       P_TERM_CODE,                          V_SGRSTSP_REC.SGRSTSP_KEY_SEQNO,
                    'AS',                             SYSDATE,                              'WFAUTO',
                    USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                    V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                    V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                    V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                    V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                    V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                    V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
            FROM DUAL;
         COMMIT;
      CLOSE C_SGRSTSP;
      
      OPEN C_SORLCUR;
           FETCH C_SORLCUR INTO V_SORLCUR_REC;
           IF C_SORLCUR%FOUND THEN
              
            -- 1. SORLCUR
              -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
              SELECT MAX(SORLCUR_SEQNO + 1)
                INTO V_SORLCUR_SEQNO_INC
              FROM SORLCUR
              WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
          
              -- GET SORLCUR_SEQNO NUEVO 
              SELECT MAX(SORLCUR_SEQNO + 2)
                INTO P_SORLCUR_SEQNO_NEW
              FROM SORLCUR
              WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
          
              -- GET SORLCUR_SEQNO ANTERIOR 
              P_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;

          
              -- INACTIVE --- SORLCUR - (1 DE 2)
              INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                          V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          NULL /*SORLCUR_CURRENT_CDE*/ );
               COMMIT;  
              
      
               -- ACTIVE --- SORLCUR - (2 DE 2)
               INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
               VALUES (   V_SORLCUR_REC.SORLCUR_PIDM,             P_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                          'WorkFlow',                             SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                          NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          V_SORLCUR_REC.SORLCUR_CURRENT_CDE );
               COMMIT;


      
                -- UPDATE TERM_CODE_END(vigencia curriculum) 
                UPDATE SORLCUR 
                    SET SORLCUR_TERM_CODE_END = P_TERM_CODE, SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
                WHERE SORLCUR_PIDM = P_PIDM
                AND SORLCUR_LMOD_CODE = 'LEARNER'
                AND SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
                COMMIT;
      
                -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
                UPDATE SORLCUR 
                    SET SORLCUR_CURRENT_CDE = NULL
                WHERE   SORLCUR_PIDM = P_PIDM
                AND SORLCUR_LMOD_CODE = 'LEARNER'
                AND SORLCUR_TERM_CODE_END = P_TERM_CODE
                AND SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
                COMMIT;
                
              -- 2. SORLFOS
                -- INSERT --- SORLFOS - (1 DE 2) 
                INSERT INTO SORLFOS (
                  SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                  SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                  SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                  SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                  SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                  SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                  SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                  SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                  SORLFOS_CURRENT_CDE) 
                SELECT 
                  V_SORLFOS_REC.SORLFOS_PIDM,             V_SORLCUR_SEQNO_INC,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                  V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                  'CHANGED'/*SORLFOS_CSTS_CODE*/,         'INACTIVE'/*SORLFOS_CACT_CODE*/,          V_SORLFOS_REC.SORLFOS_DATA_ORIGIN,
                  USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
                  V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                  V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                  V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                  V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
                  NULL /*SORLFOS_CURRENT_CDE*/
                  FROM SORLFOS V_SORLFOS_REC
                  WHERE SORLFOS_PIDM = P_PIDM
                  AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD
                  AND ROWNUM = 1;
                COMMIT;
  
                -- INSERT --- SORLFOS - (2 DE 2)
                INSERT INTO SORLFOS (
                  SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                  SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                  SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                  SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                  SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                  SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                  SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                  SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                  SORLFOS_CURRENT_CDE) 
                SELECT 
                  V_SORLFOS_REC.SORLFOS_PIDM,             P_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                  V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                  V_SORLFOS_REC.SORLFOS_CSTS_CODE,        V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
                  USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
                  V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                  V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                  V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                  V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
                 'Y'
                 FROM SORLFOS V_SORLFOS_REC
                 WHERE SORLFOS_PIDM = P_PIDM
                 AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
               COMMIT;

   
               -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo)
                UPDATE SORLFOS 
                    SET SORLFOS_CURRENT_CDE = NULL
                WHERE SORLFOS_PIDM = P_PIDM
                AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD
                AND SORLFOS_TERM_CODE = P_TERM_CODE
                AND SORLFOS_CSTS_CODE = 'INPROGRESS';
                COMMIT;
                
               END IF;       
    CLOSE C_SORLCUR;
      
EXCEPTION
WHEN OTHERS THEN
    P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);          
END P_SET_REINCORPORACION;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CONV_ASIGN(
   P_TERM_CODE   IN STVTERM.STVTERM_CODE%TYPE,
   P_CATALOGO    IN STVTERM.STVTERM_CODE%TYPE,
   P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
   P_PROGRAM     IN SOBCURR.SOBCURR_PROGRAM%TYPE,
   P_MESSAGE     OUT VARCHAR2
)
AS
BEGIN
    -- PACKAGE CONVALIDACION
    SZKCAEE.P_CONV_ASIGN_EQUIV_WF(P_TERM_CODE,P_CATALOGO,P_PIDM,P_PROGRAM,P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONV_ASIGN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SRI_EXECCAPP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN 

    BWZKPSPG.P_EXECCAPP (P_PIDM, P_TERM_CODE, P_MESSAGE);
  
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SRI_EXECCAPP;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SRI_EXECPROY (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN 

    BWZKPSPG.P_EXECPROY (P_PIDM, P_TERM_CODE, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SRI_EXECPROY;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CURSO_INTROD (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_FECHA_SOL           IN VARCHAR2,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_SET_CURSO_INTROD (P_PIDM, P_TERM_CODE, P_FECHA_SOL, P_DEPT_CODE, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CURSO_INTROD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_ATRIBUTO_PLAN (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,      -------- COD atributos
    P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
NOMBRE    : P_SET_ATRIBUTO_PLANS
FECHA     : 14/06/2017
AUTOR     : Mallqui Lopez, Richard Alfonso
OBJETIVO  : SET ATRIBUTO de PLAN ESTUDIOS a un periodo determinado.

MODIFICACIONES
NRO   FECHA   USUARIO   MODIFICACION
=================================================================================================================== */
AS
BEGIN  

    BWZKPSPG.P_SET_ATRIBUTO_PLAN (P_PIDM, P_TERM_CODE, P_ATTS_CODE, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_ATRIBUTO_PLAN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_PERIODO_CAT (
    P_PIDM         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE    IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_MESSAGE      OUT VARCHAR2
)
/* ===================================================================================================================
NOMBRE    : P_SET_PERIODO_CAT
FECHA     : 18/03/2018
AUTOR     : Arana Milla, Karina Lizbeth
OBJETIVO  : Actualiza el periodo de catalogo en las tablas SGBSTDN, SORLCUR y SORLFOS.
=================================================================================================================== */
AS
   
BEGIN 
    -- UPDATE PER CATALOGO - SGBSTDN    
    UPDATE SGBSTDN
    SET SGBSTDN_TERM_CODE_CTLG_1 = '201800'
    WHERE SGBSTDN_PIDM = P_PIDM
          AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE
          AND SGBSTDN_STYP_CODE = 'R';
    COMMIT;
    
    -- UPDATE PER CATALOGO - SORLCUR
    UPDATE SORLCUR
    SET SORLCUR_TERM_CODE_CTLG = '201800'
    WHERE SORLCUR_PIDM = P_PIDM
          AND SORLCUR_LMOD_CODE = 'LEARNER'
          AND SORLCUR_CACT_CODE = 'ACTIVE'
          AND SORLCUR_CURRENT_CDE = 'Y'
          AND SORLCUR_TERM_CODE_END IS NULL
          AND SORLCUR_TERM_CODE = P_TERM_CODE;
    COMMIT;
    
    -- UPDATE PER CATALOGO - SORLFOS
    UPDATE SORLFOS
    SET SORLFOS_TERM_CODE_CTLG = '201800'
    WHERE SORLFOS_PIDM = P_PIDM     
          AND SORLFOS_CACT_CODE = 'ACTIVE'
          AND SORLFOS_CURRENT_CDE = 'Y'
          AND SORLFOS_TERM_CODE = P_TERM_CODE;
    COMMIT;

EXCEPTION
WHEN OTHERS THEN
    P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_PERIODO_CAT;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_MOD_RETENCION(
    P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_FECHA_SOL      IN VARCHAR2,
    P_STVHLDD_CODE1  IN STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE2  IN STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE3  IN STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE4  IN STVHLDD.STVHLDD_CODE%TYPE,
    P_MESSAGE        OUT VARCHAR2
)

AS
    V_DEPT_CODE   SORLFOS.SORLFOS_DEPT_CODE%TYPE;
    V_INDICADOR   NUMBER;
    V_MESSAGE     EXCEPTION;

BEGIN
    
    SELECT SORLFOS_DEPT_CODE
        INTO V_DEPT_CODE
    FROM (SELECT SORLFOS_DEPT_CODE 
        FROM SORLFOS
        WHERE SORLFOS_PIDM = P_PIDM
            AND SORLFOS_CACT_CODE = 'ACTIVE'
            AND SORLFOS_CURRENT_CDE = 'Y'
        ORDER BY SORLFOS_TERM_CODE DESC, SORLFOS_LCUR_SEQNO DESC
        )WHERE ROWNUM = 1;
    
    -- VALIDAR si EXISTE codigo del TIPO DE RETENCION - STVHLDD
    SELECT COUNT(*)
        INTO V_INDICADOR 
    FROM STVHLDD
    WHERE STVHLDD_CODE IN ( P_STVHLDD_CODE1,P_STVHLDD_CODE2,P_STVHLDD_CODE3,P_STVHLDD_CODE4);
    
    IF V_INDICADOR < 4 THEN
        RAISE V_MESSAGE;
    ELSE
      IF V_DEPT_CODE = 'UREG' THEN
        
        UPDATE SPRHOLD
        SET SPRHOLD_TO_DATE = TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') - 1,
            SPRHOLD_ACTIVITY_DATE = SYSDATE
        WHERE SPRHOLD_PIDM = P_PIDM
          AND TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND TO_DATE(TO_CHAR(SPRHOLD_TO_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          AND SPRHOLD_HLDD_CODE IN (P_STVHLDD_CODE1,P_STVHLDD_CODE2,P_STVHLDD_CODE3);
        
        COMMIT;
        P_MESSAGE := 'OK';
        
      ELSIF V_DEPT_CODE = 'UPGT' THEN
        
        UPDATE SPRHOLD
        SET SPRHOLD_TO_DATE = TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') - 1,
            SPRHOLD_ACTIVITY_DATE = SYSDATE
        WHERE SPRHOLD_PIDM = P_PIDM
          AND TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND TO_DATE(TO_CHAR(SPRHOLD_TO_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          AND SPRHOLD_HLDD_CODE IN (P_STVHLDD_CODE1,P_STVHLDD_CODE2);
        
        COMMIT;
        P_MESSAGE := 'OK';
         
      ELSE
        
        UPDATE SPRHOLD
        SET SPRHOLD_TO_DATE = TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') - 1,
            SPRHOLD_ACTIVITY_DATE = SYSDATE
        WHERE SPRHOLD_PIDM = P_PIDM
          AND TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND TO_DATE(TO_CHAR(SPRHOLD_TO_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          AND SPRHOLD_HLDD_CODE IN (P_STVHLDD_CODE1,P_STVHLDD_CODE4);
        
        COMMIT;
        P_MESSAGE := 'OK';
        
      END IF;
         
    END IF;
 
EXCEPTION
WHEN V_MESSAGE THEN
    P_MESSAGE  := '- El "CÓDIGO DE RETENCIÓN" enviado es inválido.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MOD_RETENCION;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GETUSER_COORDINADOR (
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sencible mayuscula 'S01'
    P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE, -- sencible a mayusculas 'coordinador'
    P_PROGRAM             IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_DESC        OUT STVMAJR.STVMAJR_DESC%TYPE,
    P_ROL_COORDINADOR     OUT VARCHAR2,
    P_ROL_MAILS           OUT VARCHAR2
)
/* ===================================================================================================================
NOMBRE    : P_GETUSER_COORDINADOR
FECHA     : 22/05/2017
AUTOR     : Mallqui Lopez, Richard Alfonso
OBJETIVO  : Obtiene el usuario WF del cordinador,
            Se establecio que el usuario se compone por "COORDINADOR + PROGRAM_CODE + CAMPUS"
                - Ejm: COORDINADOR110S01 

MODIFICACIONES
NRO   FECHA         USUARIO       MODIFICACION
001   26/07/2017    RMALLQUI      Se agrego la obtencion de correos de usuarios que pertenecen a dicho rol. 
=================================================================================================================== */
AS
          -- @PARAMETERS
    V_ROLE_ID                 NUMBER;
    V_ORG_ID                  NUMBER;
    V_INDICADOR               NUMBER;
    V_Email_Address           VARCHAR2(500);
    V_EXEPTION                EXCEPTION;
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID
        AND ROLE_ID = V_ROLE_ID;
    
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
---.
  --GET DESCRIPTION OF MAJOR
    SELECT STVMAJR_DESC
        INTO P_PROGRAM_DESC
    FROM STVMAJR
    WHERE STVMAJR_CODE = P_PROGRAM;
    
    P_ROL_COORDINADOR := P_ROL ||  P_PROGRAM || P_CAMP_CODE;
    
    -- Obtener el ROL_ID 
    SELECT ID
        INTO V_ROLE_ID
    FROM WORKFLOW.ROLE
    WHERE NAME = P_ROL_COORDINADOR;
    
    -- Obtener el ORG_ID 
    SELECT ID
        INTO V_ORG_ID 
    FROM WORKFLOW.ORGANIZATION
    WHERE NAME = 'Root';
    
    --Validar que exista el ROL
    SELECT COUNT(*) 
        INTO V_INDICADOR
    FROM WORKFLOW.ROLE_ASSIGNMENT 
    WHERE ORG_ID = V_ORG_ID AND ROLE_ID = V_ROLE_ID;
    
    IF V_INDICADOR <> 1 THEN
        RAISE V_EXEPTION;
    END if;

     -- #######################################################################
    OPEN C_ROLE_ASSIGNMENT;
    LOOP
        FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
        EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                    
            -- Obtener Datos Usuario
            SELECT Email_Address
                INTO V_Email_Address
            FROM WORKFLOW.WFUSER
            WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
    
            P_ROL_MAILS := P_ROL_MAILS || V_Email_Address || ',';
            
    END LOOP;
    CLOSE C_ROLE_ASSIGNMENT;
    
    -- Extraer el ultimo digito en caso sea un "coma"(,)
    SELECT SUBSTR(P_ROL_MAILS,1,LENGTH(P_ROL_MAILS) -1)
        INTO P_ROL_MAILS
    FROM DUAL
    WHERE SUBSTR(P_ROL_MAILS,-1,1) = ',';
    
EXCEPTION
WHEN V_EXEPTION THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No se encontro el ROL.' || P_ROL_COORDINADOR);
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GETUSER_COORDINADOR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_RETENCION(
    P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_STVHLDD_CODE   IN STVHLDD.STVHLDD_CODE%TYPE,
    P_FECHA_SOL      IN VARCHAR2,
    P_MESSAGE        OUT VARCHAR2
)

/* ===================================================================================================================
NOMBRE    : P_SET_RETENCION
FECHA     : 15/01/2018
AUTOR     : Arana Milla, Karina Lizbeth
OBJETIVO  : Insertar la retencion 17 (Matricula Presencial) en la forma SOAHOLD.

MODIFICACIONES
NRO     FECHA         USUARIO       MODIFICACION  
=================================================================================================================== */
AS 
    V_INDICADOR    NUMBER;
BEGIN

    SELECT COUNT(*)
        INTO V_INDICADOR
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
       AND SPRHOLD_HLDD_CODE = P_STVHLDD_CODE
       AND TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') BETWEEN SPRHOLD_FROM_DATE AND SPRHOLD_TO_DATE;
    
    IF V_INDICADOR = 0 THEN
        INSERT INTO SPRHOLD (
               SPRHOLD_PIDM,	      SPRHOLD_HLDD_CODE,	              SPRHOLD_USER,	               SPRHOLD_FROM_DATE,        SPRHOLD_TO_DATE,	
               SPRHOLD_RELEASE_IND,	SPRHOLD_REASON,                   SPRHOLD_ACTIVITY_DATE,	     SPRHOLD_DATA_ORIGIN,               SPRHOLD_USER_ID)
    
        SELECT P_PIDM,              P_STVHLDD_CODE,                   'WFAUTO',                    TO_DATE(P_FECHA_SOL,'dd/mm/yyyy'),          TO_DATE('31/12/2099','dd/mm/yyyy'), 
               'N',                 'ACÉRCATE A LA COORDINACIÓN GQT', SYSDATE,                     'WorkFlow',               'WFAUTO'
        FROM DUAL;
        
        COMMIT;
        
        P_MESSAGE:='OK';
    ELSE
        P_MESSAGE:= 'LA RETENCIÓN 17 ESTA INSERTADA Y ACTIVA';
    END IF;
    
    EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);   
END P_SET_RETENCION;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_PERCAT(
    P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PER_CAT    OUT NUMBER,
    P_MESSAGE    OUT VARCHAR2
)

/* ===================================================================================================================
NOMBRE    : P_GET_PERCAT
FECHA     : 15/01/2018
AUTOR     : Arana Milla, Karina Lizbeth
OBJETIVO  : Obtener el periodo de catalogo activo que tiene asigando el estudiante.

MODIFICACIONES
NRO     FECHA         USUARIO       MODIFICACION  
=================================================================================================================== */

AS
    V_PERCAT  SORLCUR.SORLCUR_TERM_CODE%TYPE;
BEGIN

    SELECT SORLCUR_TERM_CODE_CTLG
    INTO V_PERCAT
    FROM( 
        SELECT * 
        FROM SORLCUR
        WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_ROLL_IND = 'Y'
            AND SORLCUR_CACT_CODE = 'ACTIVE'
            AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_SEQNO DESC
        ) WHERE ROWNUM = 1;
    
    P_PER_CAT := TO_NUMBER(V_PERCAT,'999999');
   
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_PERCAT;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_ATRIBUTO_PLAN(
    P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ATRIB_PLAN  OUT SGRSATT.SGRSATT_ATTS_CODE%TYPE,
    P_MESSAGE     OUT VARCHAR2
)

/* ===================================================================================================================
NOMBRE    : P_GET_ATRIBUTO_PLAN
FECHA     : 19/01/2018
AUTOR     : Arana Milla, Karina Lizbeth
OBJETIVO  : Obtener el atributo de plan de estudio del estudiante.

MODIFICACIONES
NRO   FECHA   USUARIO   MODIFICACION
=================================================================================================================== */
AS 
    V_ERROR        EXCEPTION;
    V_ERROR1       EXCEPTION;
    V_ATRIB_PLAN   SGRSATT.SGRSATT_ATTS_CODE%TYPE;
    
    CURSOR C_SGRSATT IS
        SELECT SGRSATT_ATTS_CODE
        FROM (
            SELECT SGRSATT_ATTS_CODE
            FROM SGRSATT
            WHERE SGRSATT_PIDM = P_PIDM
                AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
            ORDER BY SGRSATT_TERM_CODE_EFF DESC
        )WHERE ROWNUM = 1;
BEGIN
    OPEN C_SGRSATT;
    FETCH C_SGRSATT INTO P_ATRIB_PLAN;
        IF C_SGRSATT%NOTFOUND THEN
            RAISE V_ERROR;
        ELSE 
            V_ATRIB_PLAN := SUBSTR(P_ATRIB_PLAN,3,2);
            IF (V_ATRIB_PLAN <> '02' AND V_ATRIB_PLAN <> '03') THEN
                RAISE V_ERROR1;
            END IF;
        END IF;
    CLOSE C_SGRSATT;
EXCEPTION
WHEN V_ERROR THEN
    P_MESSAGE:='No tiene registrado atributo de plan de estudio.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
WHEN V_ERROR1 THEN
    P_MESSAGE:='El periodo de catálogo no coincide con el atributo de plan de estudio.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ATRIBUTO_PLAN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_CRED_APROB(
 P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
 P_PROGRAM    IN SOBCURR.SOBCURR_PROGRAM%TYPE,
 P_CANT_CRED  OUT SMBPOGN.SMBPOGN_ACT_CREDITS_OVERALL%TYPE,
 P_MESSAGE    OUT VARCHAR2
)
AS
/* ===================================================================================================================
NOMBRE    : P_GET_CRED_APROB
FECHA     : 19/01/2018
AUTOR     : Arana Milla, Karina Lizbeth
OBJETIVO  : Obtener la cantidad de creditos aprobados por el estudiante.

MODIFICACIONES
NRO   FECHA   USUARIO   MODIFICACION
=================================================================================================================== */
    CURSOR C_SMBPOGN IS
        SELECT SMBPOGN_ACT_CREDITS_OVERALL
        FROM (
            SELECT SMBPOGN_ACT_CREDITS_OVERALL 
            FROM SMBPOGN
            WHERE SMBPOGN_PIDM = P_PIDM
                AND SMBPOGN_PROGRAM = P_PROGRAM
            ORDER BY SMBPOGN_TERM_CODE_EFF DESC, SMBPOGN_REQUEST_NO DESC
        ) WHERE ROWNUM = 1;
BEGIN
    OPEN C_SMBPOGN;
    FETCH C_SMBPOGN INTO P_CANT_CRED;
        IF C_SMBPOGN%NOTFOUND THEN
            P_MESSAGE := 'No tiene registro de créditos aprobados';
        END IF;
    CLOSE C_SMBPOGN;
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_CRED_APROB;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_ASIG_PENDIENTES(
    P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CICLO_FIN   IN VARCHAR2,
    P_DEPT_CODE   IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE   IN STVCAMP.STVCAMP_CODE%TYPE,
    P_ASIG_PEND   OUT VARCHAR2,
    P_MESSAGE     OUT VARCHAR2
)
AS
    V_INDICADOR_CAPP      SMRRQCM.SMRRQCM_REQUEST_NO%TYPE;
    V_INDICADOR_ASIG      NUMBER;
    V_PROGRAM             SOBCURR.SOBCURR_PROGRAM%TYPE;
    V_CICLO_INICIO        VARCHAR2(3):='P01';
    V_IND_CICLO_MESSAGE   VARCHAR2(1);
BEGIN
    SELECT SORLCUR_PROGRAM
        INTO V_PROGRAM
    FROM(
        SELECT *
        FROM SORLCUR
        WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_ROLL_IND = 'Y'
            AND SORLCUR_CACT_CODE = 'ACTIVE'
            AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_SEQNO DESC
    ) WHERE ROWNUM = 1;
    
    -- GET LAST REQUEST_NO OF CAPP
    SELECT SMRRQCM_REQUEST_NO
        INTO V_INDICADOR_CAPP
    FROM(
        SELECT SMRRQCM_REQUEST_NO 
        FROM SMRRQCM
        INNER JOIN SMBPOGN ON
            SMBPOGN_PIDM = SMRRQCM_PIDM
            AND SMBPOGN_REQUEST_NO = SMRRQCM_REQUEST_NO
        WHERE SMRRQCM_PIDM = P_PIDM
            AND SMRRQCM_DEPT_CODE = P_DEPT_CODE
        ORDER BY SMRRQCM_REQUEST_NO DESC
    ) WHERE ROWNUM = 1;
   
    SELECT COUNT(DISTINCT SMRDORQ_AREA)
        INTO V_INDICADOR_ASIG
    FROM SMRDORQ
    INNER JOIN SMBAOGN ON
        SMBAOGN_PIDM = SMRDORQ_PIDM
        AND SMBAOGN_REQUEST_NO = SMRDORQ_REQUEST_NO
        AND SMBAOGN_AREA = SMRDORQ_AREA
    INNER JOIN SMBPOGN ON
        SMBPOGN_PIDM = SMBAOGN_PIDM
        AND SMBPOGN_REQUEST_NO = SMBAOGN_REQUEST_NO
    WHERE SMRDORQ_PIDM = P_PIDM
    AND SMBPOGN_REQUEST_NO = V_INDICADOR_CAPP
    AND SUBSTR(SMRDORQ_AREA,7,4) = P_DEPT_CODE
    AND SMBAOGN_AREA BETWEEN (V_PROGRAM||V_CICLO_INICIO||P_DEPT_CODE) AND (V_PROGRAM||P_CICLO_FIN||P_DEPT_CODE)
    AND SMBPOGN_DETL_REQ_MET_IND = 'N'
    AND SMRDORQ_MET_IND = 'N';
    
    IF V_INDICADOR_ASIG > 0 THEN
        P_ASIG_PEND := 'TRUE';
        P_MESSAGE := 'TIENE ASIGNATURAS PENDIENTES';
    ELSE 
        P_ASIG_PEND := 'FALSE';
        P_MESSAGE := 'OK';
    END IF;
       
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ASIG_PENDIENTES;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_GRP_OPEN (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
    P_TERM_CODE           IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE , 
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE ,
    P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
NOMBRE    : p_set_grupo_inscripcion_abierto
FECHA     : 27/09/16
AUTOR     : Mallqui Lopez, Richard Alfonso
OBJETIVO  : Asignar el código de grupo de inscripcion para la rectificacion en base al id, la modalidad y a la sede del estudiante.

                        Ejemplo: 99-99WF01
------------------------------------------------------------------------------
  Rectificacion               Modalidad                 Sede            
99-99 - Rectificacion     R - Regular                   S01 - Huancayo
                        W - Gente que trabaja         F01 - Arequipa 
                        V - Virtual                   F02 - Lima  
                                                      F03 - Cusco
                                                      V00 - Virtual

MODIFICACIONES
NRO   FECHA   USUARIO   MODIFICACION

=================================================================================================================== */
AS
    P_RPGRP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
    P_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
    P_RGRP_CODE           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
    P_DEPT_CAMP           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
    P_GROUP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
    P_MODALIDAD_SEDE      VARCHAR2(4);
    P_IND_CAMP            VARCHAR2(1);
    P_INDICADOR_TERM      SFBRGRP.SFBRGRP_TERM_CODE%TYPE;
    
    V_MESSAGE1            EXCEPTION;
    V_MESSAGE2            EXCEPTION;
    V_MESSAGE3            EXCEPTION;
    V_MESSAGE4            EXCEPTION;
    P_INDICADOR           NUMBER;
    P_ROW_UPDATE          NUMBER := 0;
    P_COD_GROUP           VARCHAR2(15); --:= '00-20'; --->>> EN PERIODO "00" ES "00-20"
                                                   --->>> EN CICLO REGULAR ES "99-99"
    CURSOR C_SFBRGRP_PTRM IS
        SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,1) SUBPTRM
        FROM CZRPTRM 
        WHERE CZRPTRM_DEPT = P_DEPT_CODE;

BEGIN
--
    P_INDICADOR_TERM := SUBSTR(P_TERM_CODE,5,1);
    
    IF P_INDICADOR_TERM = '0' THEN
        P_COD_GROUP := '00-20';
    ELSE
        P_COD_GROUP := '99-99';
    END IF;
    
    OPEN C_SFBRGRP_PTRM;
    LOOP
        FETCH C_SFBRGRP_PTRM INTO P_IND_CAMP;
        EXIT WHEN C_SFBRGRP_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFBRGRP_PTRM;
    
    P_MODALIDAD_SEDE := (P_IND_CAMP||P_CAMP_CODE);
    
    P_RPGRP_NEW := (P_COD_GROUP||P_MODALIDAD_SEDE);
    
    SELECT COUNT(*)
        INTO P_INDICADOR
    FROM SFBRGRP
    WHERE SFBRGRP_PIDM = P_PIDM
        AND SFBRGRP_TERM_CODE = P_TERM_CODE;
          
    IF P_INDICADOR = 0 THEN
    
       INSERT INTO SFBRGRP (SFBRGRP_TERM_CODE,          SFBRGRP_PIDM,           SFBRGRP_RGRP_CODE,                 SFBRGRP_USER, 
                            SFBRGRP_ACTIVITY_DATE,      SFBRGRP_USER_ID,        SFBRGRP_DATA_ORIGIN)
                    VALUES (P_TERM_CODE,                P_PIDM,                 P_RPGRP_NEW,                       'WorkFlow',   
                            SYSDATE,                    USER,                   'WFAUTO');
       COMMIT;
       
    ELSIF P_INDICADOR = 1 THEN
        SELECT SFBRGRP_RGRP_CODE
            INTO P_RGRP_CODE
        FROM SFBRGRP
        WHERE SFBRGRP_PIDM = P_PIDM
        AND SFBRGRP_TERM_CODE = P_TERM_CODE;
        
        P_GROUP := SUBSTR(P_RGRP_CODE,1,5);
        
        P_DEPT_CAMP := SUBSTR(P_RGRP_CODE,6,4);
       
        IF P_GROUP = P_COD_GROUP AND P_DEPT_CAMP = P_MODALIDAD_SEDE THEN
            RAISE V_MESSAGE1;
        ELSIF P_GROUP <> P_COD_GROUP AND P_DEPT_CAMP = P_MODALIDAD_SEDE THEN
            UPDATE SFBRGRP 
            -- SELECT SFBRGRP_TERM_CODE, SFBRGRP_PIDM, SFBRGRP.* 
            SET SFBRGRP_RGRP_CODE     = P_RPGRP_NEW,
                SFBRGRP_DATA_ORIGIN   = 'WorkFlow',
                SFBRGRP_ACTIVITY_DATE = SYSDATE
            WHERE SFBRGRP_PIDM = P_PIDM
            AND SFBRGRP_TERM_CODE = P_TERM_CODE
            AND EXISTS (
                ---- LA DEFINICION DE LA VISTA SFVRGRP
                SELECT SFBWCTL.SFBWCTL_TERM_CODE,
                SFBWCTL.SFBWCTL_RGRP_CODE,
                SFBWCTL.SFBWCTL_PRIORITY,
                SFRWCTL.SFRWCTL_BEGIN_DATE,
                SFRWCTL.SFRWCTL_END_DATE,
                SFRWCTL.SFRWCTL_HOUR_BEGIN,
                SFRWCTL.SFRWCTL_HOUR_END
                FROM SFRWCTL, SFBWCTL
                WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                    AND SFRWCTL.SFRWCTL_PRIORITY = SFBWCTL.SFBWCTL_PRIORITY
                    AND SFBWCTL.SFBWCTL_RGRP_CODE = P_RPGRP_NEW
                    AND SFRWCTL.SFRWCTL_TERM_CODE = P_TERM_CODE
                );
            
            P_ROW_UPDATE := P_ROW_UPDATE + SQL%ROWCOUNT;
            --DBMS_OUTPUT.PUT_LINE(P_ROW_UPDATE);
            
            IF (P_ROW_UPDATE > 1) THEN
                ROLLBACK;
                RAISE V_MESSAGE2;
            END IF;
            
        COMMIT;
            
        ELSIF P_DEPT_CAMP <> P_MODALIDAD_SEDE THEN
            RAISE V_MESSAGE3;
        END IF; 
     ELSE
        RAISE V_MESSAGE4;
     END IF;
     
EXCEPTION
WHEN V_MESSAGE1 THEN
    P_MESSAGE := 'Ya tiene asignado el grupo para realizar inscripción';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN V_MESSAGE2 THEN
    P_MESSAGE := 'Se detecto que posee DOBLE grupo de inscripción';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN V_MESSAGE3 THEN
    P_MESSAGE := 'El departamento y la sede no coinciden con el grupo que le corresponde para realizar inscripción';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN V_MESSAGE4 THEN
    P_MESSAGE := 'Se encontraron más de un grupo asignado para el mismo periodo académico';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_GRP_OPEN;

END BWZKRIOD;

/**********************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**********************************************************************************************/