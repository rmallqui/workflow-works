SET SERVEROUTPUT ON
DECLARE
        P_PIDM         SPRIDEN.SPRIDEN_PIDM%TYPE;
        P_TERM_CODE    SFBETRM.SFBETRM_TERM_CODE%TYPE;
        P_MESSAGE      VARCHAR2(4000);
BEGIN
P_SET_PERIODO_CAT ('129935','201800',P_MESSAGE);
END;
--
--
select * FROM SPRIDEN
WHERE SPRIDEN_ID='70142254';
----PIDM'S CON LOS QUE SE PROB�
---- 127703 ---> OBS JALA UN PERIODO 201720 QUE SE ENLAZA CON OTRO KEYSEQNO


SELECT * FROM SGRSTSP
WHERE SGRSTSP_PIDM = '129935';


CREATE OR REPLACE PROCEDURE P_SET_PERIODO_CAT (
        P_PIDM         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE    IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_MESSAGE      OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_PERIODO_CAT
  FECHA     : 19/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Modifica EL PERIODO CATALOGO y vuelve a estimar las deudas actualziando su CTA CORRIENTE.

  MODIFICACIONES
  NRO   FECHA         USUARIO   MODIFICACION
  001   17/11/2017    KARANA    Se agreg� el CURSOR C_SGRSTSP
  002   17/11/2017    KARANA    Insert del registro INACTIVE en SORLCUR
  003   17/11/2017    KARANA    Insert del registro CHANGED e INACTIVE SORLFOS
  =================================================================================================================== */
AS
      P_DNI_ALUMNO              SPRIDEN.SPRIDEN_ID%TYPE;


       /*----------------------------------------------------------------------------------
        -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
              - Nuevo Registro como Evidencia que alumno realizó TRAMITE
        */
        CURSOR C_SGRSTSP IS
          SELECT * FROM (
              --Ultimo registro cercano al P_TERM_CODE
              SELECT *  FROM SGRSTSP 
              WHERE SGRSTSP_PIDM = P_PIDM
              AND SGRSTSP_TERM_CODE_EFF <= P_TERM_CODE
              ORDER BY SGRSTSP_KEY_SEQNO DESC
          ) WHERE ROWNUM = 1;
      
      --AGREGAR NUEVO REGISTRO
      CURSOR C_SORLCUR IS
      SELECT * FROM (
          SELECT * FROM SORLCUR 
          WHERE SORLCUR_PIDM = P_PIDM
          AND SORLCUR_LMOD_CODE = 'LEARNER'
          AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      )WHERE ROWNUM = 1;
      
  
      V_SORLCUR_REC           SORLCUR%ROWTYPE;
      V_SORLFOS_REC           SORLFOS%ROWTYPE;
      
      --V_SGRSTSP_KEY_SEQNO     SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
      V_SORLCUR_SEQNO_INC     SORLCUR.SORLCUR_SEQNO%TYPE; -- ADD
      V_SGRSTSP_REC           SGRSTSP%ROWTYPE; --ADD
      V_TERM_CODE             SGRSTSP.SGRSTSP_TERM_CODE_EFF%TYPE; --ADD
      
      P_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE; 
      P_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE;
      P_SORLCUR_RATE_CODE     SORLCUR.SORLCUR_RATE_CODE%TYPE; -- TARIFA
      
      SAVE_ACT_DATE_OUT       VARCHAR2(100);
      RETURN_STATUS_IN_OUT    NUMBER;
BEGIN 
      
      -- GET DNI alumno
      -- SELECT SPRIDEN_ID INTO P_DNI_ALUMNO FROM SPRIDEN WHERE SPRIDEN_PIDM = P_PIDM AND SPRIDEN_CHANGE_IND IS NULL;

      
      /* #######################################################################
          -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
              - Nuevo Registro como Evidencia que alumno realiz� TR�MITE
          */
      OPEN C_SGRSTSP;
          LOOP
              FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
              EXIT WHEN C_SGRSTSP%NOTFOUND;
              
              -- GET SGRSTSP_KEY_SEQNO NUEVO
              SELECT SGRSTSP_TERM_CODE_EFF INTO V_TERM_CODE
              FROM ( SELECT SGRSTSP_TERM_CODE_EFF, SGRSTSP_KEY_SEQNO
                     FROM SGRSTSP 
                     WHERE SGRSTSP_PIDM = P_PIDM
                     AND SGRSTSP_TERM_CODE_EFF <= P_TERM_CODE
                     ORDER BY SGRSTSP_KEY_SEQNO DESC
              ) WHERE ROWNUM = 1;
              DBMS_OUTPUT.PUT_LINE(V_TERM_CODE);
              
              IF V_TERM_CODE <> P_TERM_CODE THEN
          
               INSERT INTO SGRSTSP (
                      SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                      SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                      SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                      SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                      SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                      SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                      SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                      SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                      SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
               SELECT V_SGRSTSP_REC.SGRSTSP_PIDM,       P_TERM_CODE,                          1,
                      'AS',                             SYSDATE,                              'WFAUTO',
                      USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                      V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                      V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                      V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                      V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
                FROM DUAL;
                COMMIT;
               END IF;              
          END LOOP;
        CLOSE C_SGRSTSP;
        DBMS_OUTPUT.PUT_LINE('Insert� registro de SGRSTSP');
      
      UPDATE SGBSTDN
      SET SGBSTDN_TERM_CODE_CTLG_1 = '201800'
      WHERE SGBSTDN_PIDM = P_PIDM
      AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE
      AND SGBSTDN_STYP_CODE = 'R'
      AND SGBSTDN_TERM_CODE_CTLG_1 <> P_TERM_CODE;
      COMMIT;
      DBMS_OUTPUT.PUT_LINE('Actualiz� registro en el SGASTDN');
      
--      -- #######################################################################
--      -- INSERT  ------> SORLCUR -
      OPEN C_SORLCUR;
          LOOP
              FETCH C_SORLCUR INTO V_SORLCUR_REC;
              EXIT WHEN C_SORLCUR%NOTFOUND;    
              
              -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
              SELECT MAX(SORLCUR_SEQNO + 1) INTO V_SORLCUR_SEQNO_INC  FROM SORLCUR WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
              
              -- GET SORLCUR_SEQNO NUEVO 
              SELECT MAX(SORLCUR_SEQNO + 2) INTO P_SORLCUR_SEQNO_NEW  FROM SORLCUR WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
              
               -- GET SORLCUR_SEQNO ANTERIOR 
               P_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;

              
              -- INACTIVE --- SORLCUR - (1 DE 2)
              INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                          V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          NULL /*SORLCUR_CURRENT_CDE*/ );
          DBMS_OUTPUT.PUT_LINE('Insert� registro en el SORLCUR INACTIVE');
          
          -- ACTIVE --- SORLCUR - (2 DE 2)
          INSERT INTO SORLCUR (
                      SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                      SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                      SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                      SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                      SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                      SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                      SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                      SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                      SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                      SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                      SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                      SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                      SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                      SORLCUR_CURRENT_CDE ) 
          VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             P_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                      P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                      V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                      'WorkFlow',                             SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                      V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            '201800',
                      NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                      V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                      V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                      V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                      V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                      V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                      V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                      USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                      V_SORLCUR_REC.SORLCUR_CURRENT_CDE );
           DBMS_OUTPUT.PUT_LINE('Insert� registro en el SORLCUR ACTIVE');
          
          -- UPDATE TERM_CODE_END(vigencia curriculum) 
          UPDATE SORLCUR 
          SET   SORLCUR_TERM_CODE_END = P_TERM_CODE, 
                SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
          WHERE   SORLCUR_PIDM = P_PIDM 
          AND     SORLCUR_LMOD_CODE = 'LEARNER'
          AND     SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
          DBMS_OUTPUT.PUT_LINE('Actualiz� registro en el SORLCUR INACTIVE');
          
          -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
          UPDATE SORLCUR 
          SET   SORLCUR_CURRENT_CDE = NULL
          WHERE   SORLCUR_PIDM = P_PIDM 
          AND     SORLCUR_LMOD_CODE = 'LEARNER'
          AND     SORLCUR_TERM_CODE_END = P_TERM_CODE
          AND     SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
          DBMS_OUTPUT.PUT_LINE('Actualiz� registro en el SORLCUR ACTIVE');
          
      END LOOP;
      CLOSE C_SORLCUR;

      
      -- #######################################################################
      -- INSERT --- SORLFOS -
      
      -- INSERT --- SORLFOS - (1 DE 2) 
      INSERT INTO SORLFOS (
                SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                SORLFOS_CURRENT_CDE) 
          SELECT 
                V_SORLFOS_REC.SORLFOS_PIDM,             V_SORLCUR_SEQNO_INC,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                'CHANGED'/*SORLFOS_CSTS_CODE*/,         'INACTIVE'/*SORLFOS_CACT_CODE*/,          V_SORLFOS_REC.SORLFOS_DATA_ORIGIN,
                USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
                V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
                NULL /*SORLFOS_CURRENT_CDE*/
          FROM SORLFOS V_SORLFOS_REC
          WHERE SORLFOS_PIDM = P_PIDM
          AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD
          AND ROWNUM = 1;
          DBMS_OUTPUT.PUT_LINE('Insert� registro en el SORLFOS INACTIVE');
          
      -- INSERT --- SORLFOS - (2 DE 2)
      INSERT INTO SORLFOS (
            SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
            SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
            SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
            SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
            SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
            SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
            SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
            SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
            SORLFOS_CURRENT_CDE) 
      SELECT 
            V_SORLFOS_REC.SORLFOS_PIDM,             P_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
            V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
            V_SORLFOS_REC.SORLFOS_CSTS_CODE,        V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
            USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
            '201800',                               V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
            V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
            V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
            V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
            'Y'
      FROM SORLFOS V_SORLFOS_REC
      WHERE SORLFOS_PIDM = P_PIDM
      AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
      DBMS_OUTPUT.PUT_LINE('Insert� registro en el SORLFOS ACTIVE');
       
      -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo)
      UPDATE SORLFOS 
      SET   SORLFOS_CURRENT_CDE = NULL
      WHERE   SORLFOS_PIDM = P_PIDM
      AND     SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD
      AND     SORLFOS_TERM_CODE = P_TERM_CODE
      AND     SORLFOS_CSTS_CODE = 'INPROGRESS';
      DBMS_OUTPUT.PUT_LINE('Actualiz� registro en el SORLFOS ACTIVE');
      
       /*
      -- NO ES NECESARIO LA ESTIMACION DE CUOTA EN BANNER Y APEC, YA QUE NO ACTUALIZA SUS PAGOS.

      -- #######################################################################
      -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
      SFKFEES.p_processfeeassessment (  P_TERM_CODE,
                                        P_ID_ALUMNO,
                                        SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                        SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                        'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                        'Y',          -- create TBRACCD records
                                        'SFAREGS',    -- where assessment originated from
                                        'Y',          -- commit changes
                                        SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                        'N',          -- do not ignore SFRFMAX rules
                                        RETURN_STATUS_IN_OUT );   -- OUT -- return_status
      ------------------------------------------------------------------------------  
    
      -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
      TZJAPOL.p_run_proc_tvrappl(P_DNI_ALUMNO);
    
      ------------------------------------------------------------------------------                                    
      */                                   
        
      COMMIT;
      --
EXCEPTION
WHEN OTHERS THEN
      P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_PERIODO_CAT;