
set serveroutput on;
declare lv_out varchar2(300 char);
begin
  szkecap.p_exec_capp(342961,'105','201710',lv_out);
  dbms_output.put_line(lv_out);
end;
--------------------------------
--------------------------------
--------------------------------


PROCEDURE P_STI_EXECCAPP (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_STI_EXECCAPP
  FECHA     : 08/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Ejecución de CAPP.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SZKECAP.P_EXEC_CAPP(P_PIDM,P_PROGRAM,P_TERM_CODE,LV_OUT);
    IF(LV_OUT <> '0')THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
  WHEN P_EXCEPTION THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_STI_EXECCAPP;
