APUNTES
=================================================================================
ENALCE COMPARTIDO: https://drive.google.com/open?id=0B6g9PRwmQiqoVUswS3hSOXNCX2M

Se creo un paquete en la tarea 2: contendra en orden todas las funciones, procedimeintos creados para este fin.

	######## SOLICITUD DE REINCORPORACIÓN ########
		
			- Nombre del paquete : 			BWZKCSREI
			- Cuerpo del pakquete :			BWZKCSREI BODY


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 00 --> carpeta 0 : TABLA INFORMATIVA - CONFIGURACION SOLICITUD DE SERVICIO
---------------------------------------------------------------------------------------------------------------------------------
	

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 1 --> carpeta 1 : Ttrigger 
---------------------------------------------------------------------------------------------------------------------------------

			--------
			 NOTA
			------ El nuevo proceso no requiere del EVENTO(TRIGGER) ------
			------ El nuevo proceso no requiere FUNCION (FILTROS) ------

			** El flujo se inicia por el usuaro responsable() desde mismo WF.

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 2 --> carpeta 2 : Obtiene datos del alumno
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_GET_INFO_STUDENT (
			      P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
			      P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
			      P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
			      P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
			      P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
			      P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
			      P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
			      P_NAME              OUT VARCHAR2      
			)


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 3 --> carpeta 3 : Valida que la informacion ingresada sea correcta
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_REGLAS_VALIDARDATA (
			            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			            P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
			            P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
			            P_TERM_NEW          IN STVTERM.STVTERM_CODE%TYPE,
			            P_DATOS_VALIDOS     OUT NUMBER
			      );

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 4 --> carpeta 4 - Cambia los STATUS del alumno para considerarlo como reincorporado.
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_CAMBIAR_ESTADO_ALUMNO( 
			            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			            P_TERM_CODE           IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
			            P_ERROR               OUT VARCHAR2
			      );


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 5 --> carpeta 5 - Crear CTA Corriente y cargo de carnet
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_CREATE_CTACORRIENTE (
			            P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
			            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			            P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
			            P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
			            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
			            P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE
			      );


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 6 --> carpeta 6 - Asignar Cargo apec
---------------------------------------------------------------------------------------------------------------------------------
			------------ APEC -----------------
			PROCEDURE P_SET_CARGO_CONCEPTO ( 
			        P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
			        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
			        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
			        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
			        P_APEC_CONCEPTO         IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
			        P_PROGRAM               IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			        P_MESSAGE               OUT VARCHAR2
			)

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 7 --> carpeta 7 - Asigna grupo de inscripcion abierto
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_SET_GRP_INSCRIPC_ABIERTO (
			            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			            P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
			            P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
			            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
			            P_ERROR               OUT VARCHAR2
			      );     


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 8 --> carpeta 8 - Ejecucion de CAPP
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_STI_EXECCAPP (
			            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			            P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
			            P_MESSAGE           OUT VARCHAR2
			      );    

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 9 --> carpeta 9 - Ejecucion de Proyeccion
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_STI_EXECPROY (
			            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			            P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
			            P_MESSAGE           OUT VARCHAR2
			      );   

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

EVENTOS: