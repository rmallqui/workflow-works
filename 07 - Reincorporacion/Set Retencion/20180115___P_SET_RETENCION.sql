/*SET SERVEROUTPUT ON
DECLARE
   P_PIDM           SPRIDEN.SPRIDEN_PIDM%TYPE;
   P_STVHLDD_CODE1  STVHLDD.STVHLDD_CODE%TYPE;
   P_FECHA_SOL      VARCHAR2(4000);
   P_MESSAGE        VARCHAR2(4000);
BEGIN
   P_SET_RETENCION('7500','17','16/01/2018',P_MESSAGE);
   dbms_output.put_line(P_PIDM||'---'||P_STVHLDD_CODE1||'---'||P_FECHA_SOL||'---'||P_MESSAGE);
END;
*/

           

CREATE OR REPLACE PROCEDURE P_SET_RETENCION(

   P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
   P_STVHLDD_CODE1  IN STVHLDD.STVHLDD_CODE%TYPE,
   P_FECHA_SOL      IN VARCHAR2,
   P_MESSAGE        OUT VARCHAR2
)

/* ===================================================================================================================
  NOMBRE    : P_SET_RETENCION
  FECHA     : 15/01/2018
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Insertar la retenci�n 17 (Matr�cula Presencial) en la forma SOAHOLD.

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION  
  =================================================================================================================== */

AS 
   V_INDICADOR NUMBER;
BEGIN

   SELECT COUNT(*) INTO V_INDICADOR
   FROM SPRHOLD
   WHERE SPRHOLD_PIDM = P_PIDM
         AND SPRHOLD_HLDD_CODE = P_STVHLDD_CODE1
         AND TO_DATE(P_FECHA_SOL,'dd/mm/yyyy') BETWEEN SPRHOLD_FROM_DATE AND SPRHOLD_TO_DATE;
   
   IF V_INDICADOR = 0 THEN
     INSERT INTO SPRHOLD (
                 SPRHOLD_PIDM,	SPRHOLD_HLDD_CODE,	SPRHOLD_USER,	SPRHOLD_FROM_DATE, SPRHOLD_TO_DATE,	
                 SPRHOLD_RELEASE_IND,	SPRHOLD_REASON, SPRHOLD_ACTIVITY_DATE,	SPRHOLD_DATA_ORIGIN, SPRHOLD_USER_ID)
     
     VALUES (P_PIDM, P_STVHLDD_CODE1, 'WFAUTO', TO_DATE(P_FECHA_SOL,'dd/mm/yyyy'), '31/12/2099', 'N', 'AC�RCATE A LA COORDINACI�N GQT', SYSDATE, 'WorkFlow', 'WFAUTO');
     COMMIT;
     P_MESSAGE:='OK';
   ELSE
     P_MESSAGE:='LA RETENCI�N 17 ESTA INSERTADA Y ACTIVA';
   END IF;
   
   EXCEPTION
     WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);   
END P_SET_RETENCION;


