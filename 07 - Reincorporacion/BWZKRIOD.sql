/**********************************************************************************************/
/* BWZKRIOD.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripcin corta: Script para generar el Paquete de automatizacion del proceso de         */
/*                    Solicitud de Reserva de Matricula On Demand                             */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creacion del Codigo.                                        LAM             05/DIC/2017 */
/*    --------------------                                                                    */
/*    Creacion del paquete de Reserva de Matricula On Demand                                  */
/*    --Paquete generico                                                                      */
/*    Procedure P_VALID_ID: Valida si el DNI ingresado existe en la DB-BANNER.                */
/*    --Paquete generico                                                                      */
/*    Procedure P_GET_INFO_STUDENT: Obtiene informacion del estudiante mediante el ID.        */
/*    Procedure P_VERIF_PER_0: Verifica si el periodo de catalogo es '000000'.                */
/*    Procedure P_VERIF_P001: Verifica si el estudiante tiene el atributo de plan 'P001'.     */
/*    Procedure P_VERIF_PGM_309: Verifica si el programa es 309.                              */
/*    Procedure P_VERIFICA_ESTADO_INACTIVO: Verifica si el status del estudiante en las formas*/
/*              SGASTDN es 'IS' y en el plan de estudio es diferente de 'AS'.                 */
/*    --Paquete generico                                                                      */
/*    Procedure P_VERIFICA_FECHA_DISPO: Verifica si la peticion se encuentra dentro de las    */
/*              fechas establecidas.                                                          */
/*    Procedure P_VERIFICA_RETENCION: Verificar si la fecha de solicitud se encuentra dentro  */
/*              de las fecha configuradas para el proceso.                                    */
/*    Procedure P_CREATE_CTACORRIENTE: Asigna escala y crea la CTA Corriente de un alumno     */
/*              Valida Cargo de CARNET para poder asignarle.                                  */
/*    Procedure P_SET_CARNE: APEC-BDUCCI: Crea un registro al estudiante con el CONCEPTO.     */
/*    Procedure P_SET_REINCORPORACION: Insertar el registro de nuevo periodo a reincorporar.  */
/*    --Paquete generico                                                                      */
/*    Procedure P_CONV_ASIGN: Realiza la convalidacion del plan 2007 al 2015                  */
/*    --Paquete generico                                                                      */
/*    Procedure P_SRI_EXECCAPP: Ejecuta CAPP.                                                 */
/*    --Paquete generico                                                                      */
/*    Procedure P_SRI_EXECPROY: Ejecuta proyeccion.                                           */
/*    --Paquete generico                                                                      */
/*    Procedure P_SET_CURSO_INTROD: De acuerdo a la modalidad de estudio insertar el o los    */          
/*              atributos PR o RM - RV.                                                       */
/*    --Paquete generico                                                                      */
/*    Procedure P_SET_ATRIBUTO_PLAN: Inserta el atributo de plan 2015 (P015).                 */
/*    Procedure P_SET_PERIODO_CAT: Cambio de pla de estudios del 2007 al 2015.                */ 
/*    Procedure P_MOD_RETENCION: Se se realiza cambio de plan se libera la retencion 07, 08,  */
/*              16 y 09 (depende de la modalidad).                                            */
/*    Procedure P_GETUSER_COORDINADOR: Se establecio que el usuario se compone por            */
/*              "COORDINADOR + PROGRAM_CODE + CAMPUS"  - Ejm: COORDINADOR110S01               */
/*    Procedure P_SET_RETENCION: Inserta la retencion 17 - Matricula Presencial.              */
/*    Procedure P_GET_PERCAT: Obtiene el periodo de catalogo.                                 */
/*    Procedure P_GET_ATRIBUTO_PLAN: Obtiene el atributo de plan de estudio - Ejm: P002.      */
/*    Procedure P_GET_CRED_APROB: Obtiene la cantidad de creditos aprobados del ultimo CAPP   */
/*    Procedure P_GET_ASIG_PENDIENTES: Verifica si el estudiante tiene asiganturas pendientes */
/*              en un rango de ciclos especificos de acuerdo a la modalidad de estudio.       */
/*    Procedure P_SET_GRP_OPEN: Inserta el grupo de inscripcion abierto para realizar el      */
/*              proceso de matricula.                                                         */
/*                                                                                            */
/* 2. Actualizacion P_CONV_ASIGN                               LAM/HGH            02/MAR/2018 */
/*    Se agrega este paquete ya que ahora el cambio de plan de estudio sera una convalidacion */
/* 3. Actualizacion P_SET_REINCORPORACION                      LAM                06/MAR/2018 */
/*    Se agrega los cortes de periodo en las tablas SGBSTDN, SORLCUR, SORLFOS y SGRSTSP.      */
/* 4. Actualizacion P_SET_PERIODO_CAT                          LAM                06/MAR/2018 */
/*    Update al campo periodo de catalogo en las tablas SGBSTDN, SORLCUR, SORLFOS.            */
/* 5. Actualizacion P_VERIFICA_FECHA_DISPO                     LAM                19/MAR/2018 */
/*    Se amplia la fecha de fin de una semana a dos semanas despues del incio de clases.      */
/* 6. Actualizacion P_VERIFICA_RETENCION                       LAM                20/MAR/2018 */
/*    Se agrega las horas, minutos y segundos para la validacion de fecha de fin de la        */
/*    retencion documentaria(02).                                                             */
/* 7. Actualización paquete generico                            BYF               14/NOV/2018 */
/*    Se actualiza el paquete para que use procedimientos de un paquete genérico.             */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/

-- PERMISOS DE EJECUCION
/**********************************************************************************************/
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BWZKRIOD AS

/* ===================================================================================================================
  NOMBRE    : P_VALID_ID
  FECHA     : 04/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Valida si el DNI ingresado existe en la DB-BANNER. 

  =================================================================================================================== */

PROCEDURE P_VALID_ID (
    P_ID            IN  SPRIDEN.SPRIDEN_ID%TYPE,
    P_DNI_VALID     OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_GET_INFO_STUDENT (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
    P_PART_PERIODO      OUT SOBPTRM.SOBPTRM_PTRM_CODE%TYPE, 
    P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
    P_NAME              OUT VARCHAR2,
    P_FECHA_SOL         OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_VERIF_PER_0(
    P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PER_CAT_0  OUT VARCHAR2,
    P_DESCRIP    OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_VERIF_P001(
    P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_P001       OUT VARCHAR2,
    P_DESCRIP    OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_VERIF_PGM_309 (
    P_PIDM     IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PGM_309  OUT VARCHAR2,
    P_DESCRIP  OUT VARCHAR2
);
  
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_ESTADO_INACTIVO (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ESTADO            OUT VARCHAR2,
    P_DESCRIP           OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_FECHA_DISPO (
    P_TERM_CODE      IN SOBPTRM.SOBPTRM_TERM_CODE%TYPE,
    P_FECHA_SOL      IN VARCHAR2,
    P_PART_PERIODO   IN SOBPTRM.SOBPTRM_PTRM_CODE%TYPE,
    P_FECHA_VALIDA   OUT VARCHAR2,
    P_DESCRIP        OUT VARCHAR2 
);

--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_RETENCION (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_RET_CODE        IN SPRHOLD.SPRHOLD_HLDD_CODE%TYPE,
    P_FECHA_SOL       IN VARCHAR2,
    P_RETENCION       OUT VARCHAR2,
    P_DESCRIP         OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_CREATE_CTACORRIENTE (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE
);

--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CARNE ( 
    P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_APEC_CONCEPTO         IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
    P_PROGRAM               IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MESSAGE               OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_SET_REINCORPORACION(
    P_PIDM        IN	SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE   IN	STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE     OUT	VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_CONV_ASIGN(
    P_TERM_CODE   IN STVTERM.STVTERM_CODE%TYPE,
    P_CATALOGO    IN STVTERM.STVTERM_CODE%TYPE,
    P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM     IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MESSAGE     OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_SRI_EXECCAPP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_SRI_EXECPROY (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CURSO_INTROD (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_FECHA_SOL           IN VARCHAR2,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_SET_ATRIBUTO_PLAN (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_SET_PERIODO_CAT (
    P_PIDM         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE    IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_MESSAGE      OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_MOD_RETENCION(
    P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_FECHA_SOL      IN VARCHAR2,
    P_STVHLDD_CODE1  IN STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE2  IN STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE3  IN STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE4  IN STVHLDD.STVHLDD_CODE%TYPE,
    P_MESSAGE        OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_GETUSER_COORDINADOR (
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sencible mayuscula 'S01'
    P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE, -- sencible a mayusculas 'coordinador'
    P_PROGRAM             IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_DESC        OUT STVMAJR.STVMAJR_DESC%TYPE,
    P_ROL_COORDINADOR     OUT VARCHAR2,
    P_ROL_MAILS           OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_SET_RETENCION(
    P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_STVHLDD_CODE   IN STVHLDD.STVHLDD_CODE%TYPE,
    P_FECHA_SOL      IN VARCHAR2,
    P_MESSAGE        OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_GET_PERCAT(
    P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PER_CAT    OUT NUMBER,
    P_MESSAGE    OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_GET_ATRIBUTO_PLAN(
    P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ATRIB_PLAN  OUT SGRSATT.SGRSATT_ATTS_CODE%TYPE,
    P_MESSAGE     OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_GET_CRED_APROB(
    P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM    IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_CANT_CRED  OUT SMBPOGN.SMBPOGN_ACT_CREDITS_OVERALL%TYPE,
    P_MESSAGE    OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_GET_ASIG_PENDIENTES(
    P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CICLO_FIN   IN VARCHAR2,
    P_DEPT_CODE   IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE   IN STVCAMP.STVCAMP_CODE%TYPE,
    P_ASIG_PEND   OUT VARCHAR2,
    P_MESSAGE     OUT VARCHAR2
);

--*****************************************************************************************************************************+********--

PROCEDURE P_SET_GRP_OPEN (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
    P_TERM_CODE           IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE , 
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE ,
    P_MESSAGE             OUT VARCHAR2
);

END BWZKRIOD;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKRIOD;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKRIOD FOR BANINST1.BWZKRIOD;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKRIOD
--  START gurgrth BWZKRIOD
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/