/*
drop procedure P_GET_INFO_STUDENT;
GRANT EXECUTE ON P_OBTENER_USUARIO TO wfobjects;
GRANT EXECUTE ON P_OBTENER_USUARIO TO wfauto;

set serveroutput on
DECLARE
P_DATOS_VALIDOS     VARCHAR2(4000);
begin
  P_REGLAS_VALIDARDATA(28085,'UREG','S01','201720',P_DATOS_VALIDOS);
  DBMS_OUTPUT.PUT_LINE(P_DATOS_VALIDOS);
end;
*/

create or replace PROCEDURE P_REGLAS_VALIDARDATA (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_NEW          IN STVTERM.STVTERM_CODE%TYPE,
      P_DATOS_VALIDOS     OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_GET_INFO_STUDENT
  FECHA     : 15/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida los datos segun als reglas establecidad para al FLUJO de REINCORPORACIÓN

  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_INDICADOR         NUMBER;
      V_STST_CODE         SGBSTDN.SGBSTDN_STST_CODE%TYPE;
      V_STSP_CODE         SGRSTSP.SGRSTSP_STSP_CODE%TYPE;
      V_TERM_OLD          STVTERM.STVTERM_CODE%TYPE;
      V_SUB_PTRM          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_TERM_VALIDO       NUMBER; -- (0 / 1) Si el periodo ingreasado es valido

      -- GET estatus alumno FORMA GASTDN
      CURSOR C_SGBSTDN_STST IS
      SELECT SGBSTDN_TERM_CODE_EFF, SGBSTDN_STST_CODE FROM (
            SELECT SGBSTDN_TERM_CODE_EFF, SGBSTDN_STST_CODE 
            FROM SGBSTDN 
            WHERE SGBSTDN_PIDM = P_PIDM ORDER BY SGBSTDN_TERM_CODE_EFF DESC
      ) WHERE ROWNUM = 1;
      
      -- GET estatus alumno de BANNER-->PLAN ESTUDIO
      CURSOR C_SGRSTSP_STSP IS
      SELECT SGRSTSP_STSP_CODE FROM(
          SELECT SGRSTSP_STSP_CODE
          FROM SGRSTSP 
          WHERE SGRSTSP_PIDM = P_PIDM
          AND SGRSTSP_TERM_CODE_EFF = V_TERM_OLD
          ORDER BY SGRSTSP_KEY_SEQNO DESC
      ) WHERE ROWNUM = 1;
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
BEGIN 
      
      P_DATOS_VALIDOS := 0;
      
      -----------------------------------------------------------------------------
      -- >> GET ESTADIO DEL ALUMNO --
      OPEN C_SGBSTDN_STST;
      LOOP
        FETCH C_SGBSTDN_STST INTO V_TERM_OLD, V_STST_CODE;
        EXIT WHEN C_SGBSTDN_STST%NOTFOUND;
      END LOOP;
      CLOSE C_SGBSTDN_STST;

      -----------------------------------------------------------------------------
      -- GET estatus alumno de BANNER-->PLAN ESTUDIO
      OPEN C_SGRSTSP_STSP;
      LOOP
        FETCH C_SGRSTSP_STSP INTO V_STSP_CODE;
        EXIT WHEN C_SGRSTSP_STSP%NOTFOUND;
      END LOOP;
      CLOSE C_SGRSTSP_STSP;


      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;      
      
      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             DESDE INICIO DE MATRICULA(SFRRSTS_START_DATE)
             - HASTA FINALIZAR EL PERIODO DE CLASES(SOBPTRM_END_DATE)
      */
      SELECT COUNT(SOBPTRM_TERM_CODE) INTO V_TERM_VALIDO FROM (
            -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
            SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
            FROM SOBPTRM
            INNER JOIN SFRRSTS
              ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
              AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
            WHERE SFRRSTS_RSTS_CODE = 'RW'
            AND SOBPTRM_PTRM_CODE = (V_SUB_PTRM || '1') -- Solo parte de periodo '%1'
            -- AND SOBPTRM_PTRM_CODE NOT LIKE '%0'         -- VERANO
            AND SOBPTRM_TERM_CODE = P_TERM_NEW
            AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (SOBPTRM_END_DATE)
      ) WHERE ROWNUM <= 1;
      

      /******************************************************************************************************/
      -- CHECK REGLAS
      IF (V_STST_CODE = 'IS' AND (V_STSP_CODE = 'AB' OR V_STSP_CODE = 'RV') 
          AND V_TERM_VALIDO > 0) THEN
                P_DATOS_VALIDOS := 1;
      END IF;
  

  
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_REGLAS_VALIDARDATA;

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------