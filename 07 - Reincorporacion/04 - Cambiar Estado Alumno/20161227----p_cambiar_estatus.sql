/*
DECLARE P_ERROR                   VARCHAR2(100);
BEGIN
    P_CAMBIAR_ESTATUS('307294',P_ERROR);
    DBMS_OUTPUT.PUT_LINE('ERROR :' || P_ERROR || ' -- AFECTADOS :' || SQL%ROWCOUNT);
END;
*/


CREATE OR REPLACE PROCEDURE P_CAMBIAR_ESTATUS( 
      P_PIDM_ALUMNO             IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ERROR                   OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_CAMBIAR_ESTATUS
  FECHA     : 27/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cambia los estados de un alumno(forma SGASTDN) : Plan Estudio, Status Alumno, Tipo Alumno
                - Plan Estudio    :  'RE' - Reincorporado
                - Status Alumno   :  'AS' - Activo
                - Tipo Alumno     :  'R' - UC Reincorporado

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
            P_KEY_SEQNO             SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
            P_PERIODO               SFBETRM.SFBETRM_TERM_CODE%TYPE;
BEGIN
      
        -- #######################################################################
        -- CAMBIO ESTADO PLAN ESTUDIO  --- FORMA SGASTDN        
            
            -- Get lasT periodo
        SELECT MAX(SGRSTSP_TERM_CODE_EFF) INTO P_PERIODO
        FROM SGRSTSP 
        WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO;
            
        SELECT  MIN(SGRSTSP_KEY_SEQNO) INTO P_KEY_SEQNO 
        FROM SGRSTSP 
        WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO 
        AND SGRSTSP_TERM_CODE_EFF = P_PERIODO;
        
        UPDATE SGRSTSP 
            SET SGRSTSP_STSP_CODE = 'RE' ------------------ ESTADOS STVSTSP : 'RE' - Reincorporado
        WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO 
        AND SGRSTSP_TERM_CODE_EFF = P_PERIODO 
        AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO
        AND SGRSTSP_STSP_CODE <> 'RE';
          
        
        -- #######################################################################
        -- CAMBIO ESTADO ALUMNO  
          
        UPDATE SGBSTDN
            SET SGBSTDN_STST_CODE = 'AS'  ------------------ ESTADOS STVSTST : 'AS' - Activo
            ,   SGBSTDN_STYP_CODE = 'R'   ------------------ ESTADOS STVSTYP : 'R' - UC Reincorporado
        WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
        AND SGBSTDN_TERM_CODE_EFF = P_PERIODO;
        
        --
        COMMIT;
EXCEPTION
    WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_CAMBIAR_ESTATUS;   