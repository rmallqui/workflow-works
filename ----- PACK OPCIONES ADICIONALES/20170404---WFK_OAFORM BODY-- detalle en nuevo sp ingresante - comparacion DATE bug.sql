CREATE OR REPLACE PACKAGE BODY WFK_OAFORM AS
/*******************************************************************************
 WFK_OAFORM:
       Conti Package OPCIONES ADICIONALES FORMA
*******************************************************************************/
-- FILE NAME..: WFK_OAFORM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_OAFORM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -  
  DESCRIPTION END 
*******************************************************************************/


FUNCTION F_RSS_SRIR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRIR
              "Regla Solicitud de Servicio de SOLICITUD RECTIFICACIÓN DE INSCRIPCIÓN REZAGADOS"
  FECHA     : 20/02/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 

  NRO     FECHA         USUARIO       MODIFICACION
  001     09/02/2017    RMALLQUI      Agrego validacion solo para matriculados (CON NRC)   
  =================================================================================================================== */
 IS
      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL003';
      V_PRIORIDAD           NUMBER;
      P_INDICADOR           NUMBER;
      V_CODE_DEPT           SORLFOS.SORLFOS_DEPT_CODE%TYPE;
      V_CODE_TERM           SFBETRM.SFBETRM_TERM_CODE%TYPE;
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;
BEGIN  
--    
    -- DATOS ALUMNO
    P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);

    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, V_CODE_DEPT, NULL, NULL, V_PRIORIDAD);

    -- VERIFICAR QUE YA ESTE MATRICULADO
    ---A VERIFICAR QUE TENGA NRC's
      SELECT COUNT(*) INTO P_INDICADOR FROM SFRSTCR 
      WHERE SFRSTCR_PIDM = P_PIDM
      AND SFRSTCR_TERM_CODE = V_CODE_TERM; 
    
    IF V_PRIORIDAD > 0 AND P_INDICADOR > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SRIR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SRM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRM
              "Regla Solicitud de Servicio de SOLICITUD RESERVA DE MATRICULA"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      V_CODIGO_SOL      SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL005';
      V_CODE_DEPT       SORLFOS.SORLFOS_DEPT_CODE%TYPE;
      V_CODE_TERM       SFBETRM.SFBETRM_TERM_CODE%TYPE;
      V_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
      V_PRIORIDAD       NUMBER;
      V_TERM_ADMI       NUMBER;
      P_PERIODOCTLG     SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      
      V_INGRESANTE      BOOLEAN := FALSE;
      V_NCURSOS         NUMBER;
      V_FECHA_EX        DATE;

BEGIN  
--    
    -- DATOS ALUMNO
    P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);

    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, V_CODIGO_SOL, V_CODE_DEPT, V_CODE_TERM, V_CODE_CAMP, V_PRIORIDAD);

    /************************************************************************************************************/
    -- Segun regla para UPGT y UVIR, los que tramitan reserva el mismo periodo que ingresaron 
    /************************************************************************************************************/
    IF  (V_CODE_DEPT = 'UPGT' AND V_PRIORIDAD = 4) OR     -- se les RESERVA las prioridades "4" 
        (V_CODE_DEPT = 'UVIR' AND V_PRIORIDAD = 7)        -- se les RESERVA las prioridades "7"
        THEN

        -- VALIDAR QUE EL ALUMNO SEA INGRESANTE EN EL MISMO PERIODO DEL TRAMITE. (PERIODO DE INGRESO = PERIODO ACTUAL)
        SELECT  COUNT(*) INTO V_TERM_ADMI
        FROM SORLCUR        INNER JOIN SORLFOS
        ON    SORLCUR_PIDM          = SORLFOS_PIDM 
        AND   SORLCUR_SEQNO         = SORLFOS.SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM          =  P_PIDM
        AND SORLCUR_LMOD_CODE       = 'LEARNER' /*#Estudiante*/ 
        AND SORLCUR_CACT_CODE       = 'ACTIVE' 
        AND SORLCUR_CURRENT_CDE     = 'Y'
        AND SORLCUR_TERM_CODE_ADMIT = V_CODE_TERM -- VALIDACION (PERIODO DE INGRESO = PERIODO ACTUAL)
        AND SORLFOS_DEPT_CODE       = V_CODE_DEPT;

    /************************************************************************************************************/
    -- para casos de INGREASANTES solo regular huancayo - VALIDACIONES POR FECHA DE EXAMEN
    /************************************************************************************************************/
    ELSIF  (V_CODE_DEPT = 'UREG' AND V_PRIORIDAD = 2 AND V_CODE_CAMP = 'S01')  THEN     -- Ingresanter periodo activo regular  con prioridad '2'

        P_INGRESANTE_TERM(P_PIDM , V_CODE_CAMP,V_CODE_TERM,V_CODE_DEPT,V_INGRESANTE,V_FECHA_EX);

        SELECT COUNT(*) INTO V_NCURSOS FROM SFRSTCR 
        WHERE SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_TERM_CODE = V_CODE_TERM;

        IF (V_INGRESANTE AND (V_NCURSOS = 0 OR V_FECHA_EX = '26/03/2017')) THEN
            V_TERM_ADMI := 1;
        ELSE 
            V_TERM_ADMI := 0;
        END IF;

    ELSE
        V_TERM_ADMI := 1;
    END IF;

    IF (V_PRIORIDAD > 0 AND V_TERM_ADMI > 0) THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';

--
END F_RSS_SRM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_CPE (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_CPE
              "Regla Solicitud de Servicio de CAMBIO de PLAN ESTUDIO"
  FECHA     : 07/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_PERIODOCTLG            SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      P_PERIODOCTLG_MAX        SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL004';
      P_PRIORIDAD              NUMBER;
      
      -- OBTENER PERIODO DE CATALOGO
      CURSOR C_SORLCUR IS
      SELECT SORLCUR_TERM_CODE_CTLG
      FROM(
        SELECT SORLCUR_TERM_CODE_CTLG 
        FROM SORLCUR 
        WHERE SORLCUR_PIDM      =  P_PIDM
        AND SORLCUR_CACT_CODE   = 'ACTIVE' 
        AND SORLCUR_LMOD_CODE   = 'LEARNER'
        AND SORLCUR_CURRENT_CDE = 'Y'
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM = 1;

BEGIN  
--    
    OPEN C_SORLCUR;
    LOOP
      FETCH C_SORLCUR INTO P_PERIODOCTLG;
      EXIT WHEN C_SORLCUR%NOTFOUND;
      END LOOP;
    CLOSE C_SORLCUR;
    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, NULL,NULL,NULL, P_PRIORIDAD);
    
    IF(P_PERIODOCTLG < P_PERIODOCTLG_MAX) AND P_PRIORIDAD > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_CPE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SRD (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRD
              "Regla Solicitud de Servicio de SOLICITUD DE SOBREPASO DE RETENCION DE DOCUMENTOS"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL006';
      P_PRIORIDAD              NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, NULL,NULL,NULL, P_PRIORIDAD);
    
    IF P_PRIORIDAD > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SRD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SMCI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SMCI
              "Regla Solicitud de Servicio 
              de MODIFICACIÒN de la CUOTA INICIAL (MATRICULA ESPECIAL)  SOL007"
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : en base a una sede y un rol, el procedimiento obtenga los datos del responsable 
              de Registro acadèmico de esa sede.

  =================================================================================================================== */
 IS
      P_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE := 0;
      P_DEPARTAMENTO            SORLFOS.SORLFOS_DEPT_CODE%TYPE := '-';
      P_DEPARTAMENTO_VALIDO     SORLFOS.SORLFOS_DEPT_CODE%TYPE := 'UREG';
      P_CODIGO_SOL              SVRRSRV.SVRRSRV_SRVC_CODE%TYPE := 'SOL007';
      P_PRIORIDAD               NUMBER := 0;
      
      -- GET datos de alumno 
      CURSOR C_SORLCUR IS
      SELECT    SORLCUR_SEQNO, SORLFOS_DEPT_CODE  
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;
      
BEGIN  
--
    -- #######################################################################
    -- GET datos de alumno 
    OPEN C_SORLCUR;
    LOOP
      FETCH C_SORLCUR INTO P_SEQNO, P_DEPARTAMENTO;
      EXIT WHEN C_SORLCUR%NOTFOUND;
      END LOOP;
    CLOSE C_SORLCUR;
    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, P_DEPARTAMENTO_VALIDO,NULL,NULL, P_PRIORIDAD);
    
    IF P_DEPARTAMENTO = P_DEPARTAMENTO_VALIDO AND P_PRIORIDAD > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
        
END F_RSS_SMCI;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SR
              "Regla Solicitud de Servicio de SOLICITUD DE REINCORPORACIÒN"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL009';
      P_PRIORIDAD              NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, NULL,NULL,NULL, P_PRIORIDAD);
    
    IF P_PRIORIDAD > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SESM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SESM
              "Regla Solicitud de Servicio de SOLICITUD DE EXONERACIÓN DE SEGURO DE SALUD"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL012';
      P_PRIORIDAD              NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, NULL,NULL,NULL, P_PRIORIDAD);
    
    IF P_PRIORIDAD > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SESM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CHECK_DEPT_PRIORIDAD(
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       IN SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_CODE_DEPT       IN SORLFOS.SORLFOS_DEPT_CODE%TYPE,
    P_CODE_TERM       IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_CODE_CAMP       IN STVCAMP.STVCAMP_CODE%TYPE,
    P_PRIORIDAD       OUT NUMBER
) 
/* ===================================================================================================================
  NOMBRE    : P_CHECK_DEPT_PRIORIDAD
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida la fecha de hoy este dentro de las fechas para generar una solicitud, 
              incluyendo el departamento que le corresponde.
  =================================================================================================================== */
 IS
      V_INDICADOR               NUMBER := -1;
      V_TERMADMIN               NUMBER := -1;
      V_RETURN                  NUMBER := -1;
      V_CODE_DEPT               SORLFOS.SORLFOS_DEPT_CODE%TYPE;
      V_SVRRSRV_SEQ_NO          SVRRSRV.SVRRSRV_SEQ_NO%TYPE;
      V_SVRRSRV_WEB_IND         SVRRSRV.SVRRSRV_WEB_IND%TYPE;
      
      -- GET DEPT en caso no se envie entre los parametros
      CURSOR C_SORLCUR IS
      SELECT    SORLFOS_DEPT_CODE  
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;

      -- GET prioridad
      CURSOR C_SVRRSRV IS
      SELECT SVRRSRV_SEQ_NO, SVRRSRV_WEB_IND FROM SVRRSRV 
      WHERE SVRRSRV_SRVC_CODE = P_SRVC_CODE
      AND ( 
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE(TO_CHAR(SVRRSRV_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
            AND
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE(TO_CHAR(SVRRSRV_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          )
      AND SVRRSRV_SEQ_NO IN ( (0 + V_INDICADOR) , (1 + V_INDICADOR), (2 + V_INDICADOR) )
      ORDER BY SVRRSRV_SEQ_NO ASC; 
      
BEGIN  
--    
      P_PRIORIDAD := 0;
      V_CODE_DEPT := P_CODE_DEPT; 
      
      -- en caso se envie DEPT = null o vacio
      IF NVL(TRIM(V_CODE_DEPT),'TRUE') = 'TRUE'  THEN
      
              -- GET datos de alumno 
              OPEN C_SORLCUR;
              LOOP
                FETCH C_SORLCUR INTO V_CODE_DEPT;
                EXIT WHEN C_SORLCUR%NOTFOUND;
                END LOOP;
              CLOSE C_SORLCUR;
              
      END IF;
      
      SELECT CASE V_CODE_DEPT 
                  WHEN 'UREG' THEN 1
                  WHEN 'UPGT' THEN 4
                  WHEN 'UVIR' THEN 7
                  ELSE -99 END INTO V_INDICADOR
      FROM DUAL;
      
      -- GET COINCIDENCIAS A LOS FILTROS 
      -- UREG 1,2,3
      -- UPGT 4,5,6
      -- UVIR 7,8,9    
      -- GET DEPARTAMENTO, SEDE
      OPEN C_SVRRSRV;
      LOOP
        FETCH C_SVRRSRV INTO V_SVRRSRV_SEQ_NO, V_SVRRSRV_WEB_IND;
        IF C_SVRRSRV%FOUND THEN
            P_PRIORIDAD := CASE WHEN V_SVRRSRV_WEB_IND = 'N' THEN 0 ELSE V_SVRRSRV_SEQ_NO END;
        ELSE EXIT;
      END IF;
      END LOOP;
      CLOSE C_SVRRSRV;

--
END P_CHECK_DEPT_PRIORIDAD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GETDATA_ALUMN (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CODE_DEPT       OUT STVDEPT.STVDEPT_CODE%TYPE, 
    P_CODE_TERM       OUT STVTERM.STVTERM_CODE%TYPE,
    P_CODE_CAMP       OUT STVCAMP.STVCAMP_CODE%TYPE
) 
/* ===================================================================================================================
  NOMBRE    : P_GETDATA_ALUMN
  FECHA     : 14/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Retorn el periodo, Departamento y la sede del alumno

  NRO     FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
 AS
      P_INDICADOR               NUMBER;
      P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
      V_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE;
      
      -- GET DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE                
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM = 1;
      
BEGIN  
--    
      P_CODE_TERM   := NULL;
      P_CODE_DEPT   := NULL;
      P_CODE_CAMP   := NULL;
      
      -- GET DEPARTAMENTO, SEDE
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CODE_CAMP,P_CODE_DEPT;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;
      
      -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
      IF (P_CODE_DEPT IS NULL OR P_CODE_CAMP IS NULL) THEN
          P_CODE_TERM := NULL;
      ELSE
            -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
            SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                      WHEN 'UPGT' THEN 'W' 
                                      WHEN 'UREG' THEN 'R' 
                                      WHEN 'UPOS' THEN '-' 
                                      WHEN 'ITEC' THEN '-' 
                                      WHEN 'UCIC' THEN '-' 
                                      WHEN 'UCEC' THEN '-' 
                                      WHEN 'ICEC' THEN '-' 
                                      ELSE '1' END ||
                    CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                      WHEN 'F01' THEN 'A' 
                                      WHEN 'F02' THEN 'L' 
                                      WHEN 'F03' THEN 'C' 
                                      WHEN 'V00' THEN 'V' 
                                      ELSE '9' END
            INTO P_PART_PERIODO
            FROM STVCAMP,STVDEPT 
            WHERE STVDEPT_CODE = P_CODE_DEPT AND STVCAMP_CODE = P_CODE_CAMP;
            
            -- GET PERIODO ACTIVO --- FORMA: SFARESTS
            SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
            AND SFRRSTS_RSTS_CODE = 'RW' AND SYSDATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
            IF P_INDICADOR = 0 THEN
                  P_CODE_TERM := NULL;
            ELSE
                  SELECT SFRRSTS_TERM_CODE INTO  P_CODE_TERM FROM (
                      -- Forma SFARSTS  ---> fechas para las partes de periodo
                      SELECT DISTINCT SFRRSTS_TERM_CODE 
                      FROM SFRRSTS
                      WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                      AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                      AND SYSDATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                      ORDER BY SFRRSTS_TERM_CODE DESC
                  ) WHERE ROWNUM <= 1;                    
            END IF;
      END IF;
      
--
END P_GETDATA_ALUMN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_INGRESANTE_TERM (
    P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CAMP           IN STVCAMP.STVCAMP_CODE%type,
    P_TERM           IN STVTERM.STVTERM_CODE%TYPE,
    P_DEPT           IN STVDEPT.STVDEPT_CODE%type,
    P_INGRESANTE     OUT BOOLEAN,
    P_FECHA          OUT DATE
)
/* ===================================================================================================================
  NOMBRE    : F_INGRESANTE_TERM
  FECHA     : 03/04/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida si el alumno es ingresante para un periodo,sede,dept especifico.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
       
      P_APEC_CAMP      VARCHAR2(9);
      P_APEC_DEPT      VARCHAR2(9);
      P_APEC_TERM      VARCHAR2(10);
      
      V_C              INTEGER;
      V_ROW1           INTEGER;
      V_OUT            VARCHAR2(15);
      V_QUERY_INGRE    VARCHAR2(4000);
      V_CONTADOR       INTEGER := 0;

BEGIN

      SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP;-- CAMPUS 
      SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM;-- PERIODO
      SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT;-- DEPARTAMENTO

      V_QUERY_INGRE := '
      --CALCULAR A LOS MATRICULADOS BECA 18
      SELECT IDPersonaN, max(IDExamen) IDExamen --convert(NVARCHAR, max(IDExamen), 103) IDExamen
      FROM tblPersona PP
      INNER JOIN tblPersonaAlumno  PA
      ON PP.IDPersona = PA.IDPersona
      INNER JOIN (
          select distinct p.idalumno,p.IDExamen
          from tblalumnoestado a 
          INNER JOIN tblpostulante p
            ON a.IDDependencia = P.IDDependencia
            and a.IDPerAcad = P.IDPerAcad
            and a.IDAlumno = P.IDAlumno
            and a.FecInic = p.IDExamen
            and a.idescuela = p.idescuelaadm
          INNER JOIN tblEscuela 
            ON tblEscuela.IDEscuela = P.IDEscuela
                  and tblEscuela.IDDependencia =p .IDDependencia
          inner join tblModalidadPostu mpost 
            on mpost.IDModalidadPostu = P.IDModalidadPostu
                  and mpost.IDPerAcad = P.IDPerAcad
                  and mpost.IDDependencia = P.IDDependencia
                  and mpost.IDEscuelaADM = A.IDEscuela
          where p.iddependencia = ''UCCI''
          and p.idperacad = ''' || P_APEC_TERM || '''
          and a.idescuela = ''' || P_APEC_DEPT || '''
          and p.ingresante = ''1''
          and a.idsede = ''' || P_APEC_CAMP || '''
          and Renuncia=''0''
          and tblEscuela.Activo=''1''
          and p.IDEscuela not in (''506'',''510'',''601'')
      ) TM
      ON  PA.IDAlumno = TM.IDAlumno 
      AND IDPersonaN = ' || TO_CHAR(P_PIDM) || ' 
      GROUP BY IDPersonaN ';
     V_C := DBMS_HS_PASSTHROUGH.OPEN_CURSOR@BDUCCI.CONTINENTAL.EDU.PE;
     DBMS_HS_PASSTHROUGH.PARSE@BDUCCI.CONTINENTAL.EDU.PE(V_C,V_QUERY_INGRE);
     LOOP
          V_ROW1 := DBMS_HS_PASSTHROUGH.FETCH_ROW@BDUCCI.CONTINENTAL.EDU.PE(V_C);
          EXIT WHEN V_ROW1 = 0;
          DBMS_HS_PASSTHROUGH.GET_VALUE@BDUCCI.CONTINENTAL.EDU.PE (V_C,2, V_OUT);
          P_INGRESANTE := TRUE;    
          P_FECHA      := V_OUT;--TO_DATE(V_OUT, 'dd/mm/yyyy');
     END LOOP;
     DBMS_HS_PASSTHROUGH.CLOSE_CURSOR@BDUCCI.CONTINENTAL.EDU.PE(V_C);

END P_INGRESANTE_TERM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


END WFK_OAFORM;