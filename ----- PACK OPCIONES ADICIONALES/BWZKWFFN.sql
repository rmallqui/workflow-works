/******************************************************************************/
/* BWZKWFFN.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripcion corta: Script para generar el Paquete de funciones para el     */
/*                    WorkFlow                                                */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creacion del Codigo.                                    RML 23/NOV/2017 */
/* Se cambia el nombre del procedimiento WFK_OAFORM POR BWZKWFFN              */
/*  Function F_RSS_SREM: "Regla Solicitud de Servicio de SOLICITUD            */
/*    RECTIFICACION DE INSCRIPCION REZAGADOS"                                 */
/*  Function F_RSS_SRM: "Regla Solicitud de Servicio de SOLICITUD             */
/*    RESERVA DE MATRICULA"                                                   */
/*  Function F_RSS_CPE: "Regla Solicitud de Servicio de CAMBIO PLAN ESTUDIO"  */
/*  Function F_RSS_SRD: "Regla Solicitud de Servicio de SOLICITUD DE SOBREPASO*/
/*    DE RETENCION DE DOCUMENTOS"                                             */
/*  Function F_RSS_SMCI: "Regla Solicitud de Servicio de MODIFICACION de la   */
/*    CUOTA INICIAL (MATRICULA ESPECIAL)  SOL007"                             */
/*  Function F_RSS_SR: "Regla Solicitud de Servicio de SOLICITUD DE           */
/*    REINCORPORACION"                                                        */
/*  Function F_RSS_SESM: "Regla Solicitud de Servicio de SOLICITUD DE         */
/*    EXONERACION DE SEGURO DE SALUD"                                         */
/*  Function F_RSS_STI: "Regla Solicitud de Servicio de SOLICITUD DE          */
/*    TRASLADO INTERNO "                                                      */
/*  Function F_RSS_SPGA: "Regla Solicitud de Servicio de SOLICITUD DE         */
/*    PRORROGA GASTOS ADMINISTRATIVOS "                                       */
/*  Function F_RSS_SPBT: "Regla Solicitud de Servicio de SOLICITUD DE         */
/*    TRASLADO INTERNO "                                                      */
/*  Function F_RSS_SDPC: "Regla Solicitud de Servicio DESCUENTO POR CONVENIO" */
/*  Function F_RSS_SMAD: "Regla Solicitud de Servicio de SOLICITUD MATRICULA  */
/*    DE ASIGNATURA DIRIGIDA"                                                 */
/*  Procedure P_CHECK_DEPT_PRIORIDAD: Valida la fecha de hoy este dentro de   */
/*    las fechas para generar una solicitud, incluyendo el departamento       */
/*    que le corresponde.                                                     */
/*  Procedure P_GETDATA_ALUMN: Retorn el periodo, Departamento y la           */
/*    sede del alumno.                                                        */
/*  Procdure P_DATOS_INGRESO: Valida si el alumno es ingresante para un       */
/*    periodo,sede,dept especifico.                                           */
/*  Procedure P_CREDITOS_CICLO: Calcula  los creditos y ciclo del estudiante. */
/*  Function F_RSS_SMCV: "Regla Solicitud de Modificacion de Cuota Inicial -  */ 
/*    Verano".                                                                */
/*                                                                            */
/*  2.Actualizacion F_RSS_SREM                                RML 09/02/2017  */
/*    Agrego validacion solo para matriculados (CON NRC)                      */
/*  3.Actualizacion F_RSS_SREM                                RML 26/04/2017  */
/*    Se agrego validacion para el segundo modulo de UVIR y UPGT              */
/*  4.Actualizacion F_RSS_SRM                                 RML 26/04/2017  */
/*    Se agrego validacion para el segundo modulo de UVIR y UPGT              */
/*  5.Actualizacion F_RSS_SREM                                RML 13/06/2017  */
/*    Se prioriza y obtiene el periodo mas antiguo en caso de cruce de fechas.*/
/*  6.Actualizacion F_RSS_SRM                                 RML 13/06/2017  */
/*    Se prioriza y obtiene el periodo mas antiguo en caso de cruce de fechas.*/
/*  7.Actualizacion F_RSS_CPE                                 RML 16/06/2017  */
/*    Se agregaron filtros: Especificados en el acta 006-WF-2017 con          */
/*    fecha 07-Jun-2017                                                       */
/*  8.Actualizacion F_RSS_STI                                 RML 27/07/2017  */
/*    Se agrego la VALIDACION de retencion ('01'-deuda pendiente)             */
/*  9.Actualizacion F_RSS_STI                                 RML 02/08/2017  */
/*    Se agrego la validacion de que la solicitud solo se puede realizar una  */
/*    vez por periodo.                                                        */
/*  10.Actualizacion F_RSS_SPBT                               LAM 23/11/2017  */
/*     Se modifica el filtro a V_CICLO>=10                                    */
/*  11.Actualizacion F_RSS_SMCV                               LAM 26/12/2017  */
/*     Se modifico la restriccion de departamento, ahora puede ser visualizado*/
/*     para los departamentos UREG, UPGT y UVIR, mientras no sean cachimbos.  */
/*  12.Actualizacion F_RSS_SREM                               LAM 29/12/2017  */
/*     Se renonmbra la funcion F_RSS_SRIR.                                    */
/*  13.Actualizacion F_RSS_SREM                               LAM 29/12/2017  */
/*     Se actualiza la validacion para que los estudiantes que hicieron       */
/*     matricula por INB(status "RE"), tambien puedan realizar la             */
/*      rectificacion                                                         */
/*  14.Actualizacion F_RSS_SDPC                               BFV 13/06/2018  */
/*     Se actualiza el filtro para obtener el periodo de ingreso general      */
/*  15.Actualizacion F_RSS_STI                                BFV 13/06/2018  */
/*     Se actualiza la cantidad de creditos base a 72 para la visualizacion.  */
/*  16.Actualizacion general GETDATA                          BFV 13/06/2018  */
/*     Se actualiza la consulta para obtener el ultimo registro del periodo   */
/*     de catalogo.                                                           */
/*  17.Actualización F_RSS_SMAD                               BFV 16/07/2018  */
/*     Se crea la función para validar que los estudiantes de UVIR no accedan.*/
/*     Actualización F_RSS_SRD                                                */
/*     Se actualiza para que solo los estudiantes que tengan la Retención 02  */
/*     puedan acceder al WF.                                                  */
/*  18.Actualizacion F_RSS_SDPC                               BFV 02/11/2018  */
/*     Se actualiza la consulta para obtener la fecha de examen de las        */
/*     modalidades GQT y VIR.                                                 */
/*    --------------------                                                    */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 

-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BWZKWFFN AS

/******************************************************************************************************************+
 * F_RSS_SRIR : "Regla Solicitud de Servicio de RECTIFICACION DE INSCRIPCION REZAGADOS"
 */ 
FUNCTION F_RSS_SREM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SRM : "Regla Solicitud de Servicio de SOLICITUD RESERVA DE MATRICULA"
 */ 
FUNCTION F_RSS_SRM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_CPE : "Regla Solicitud de Servicio de CAMBIO de PLAN ESTUDIO"
 */ 
FUNCTION F_RSS_CPE (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SRD : "Regla Solicitud de Servicio de SOLICITUD DE SOBREPASO DE RETENCION DE DOCUMENTOS"
 */ 
FUNCTION F_RSS_SRD (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SMCI : "Regla Solicitud de Servicio de MODIFICACION de la CUOTA INICIAL"
 */ 
FUNCTION F_RSS_SMCI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SR : "Regla Solicitud de Servicio de SOLICITUD DE REINCORPORACION"
 */ 
FUNCTION F_RSS_SR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SESM : "Regla Solicitud de Servicio de SOLICITUD DE EXONERACION DE SEGURO DE SALUD"
 */ 
FUNCTION F_RSS_SESM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_STI : "Regla Solicitud de Servicio de SOLICITUD DE TRASLADO INTERNO (CONTISTI)"
 */ 
FUNCTION F_RSS_STI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SPGA : "Regla Solicitud de Servicio de SOLICITUD DE PRORROGA para GASTO ADMINISTATIVO"
 */ 
FUNCTION F_RSS_SPGA (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SPBT : "Regla Solicitud de Servicio de SOLICITUD PAGOS de BACHILLER y TITULO"
 */ 
FUNCTION F_RSS_SPBT (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SPBT : "Regla Solicitud de Servicio de SOLICITUD DESCUENTO POR CONVENIO"
 */ 
FUNCTION F_RSS_SDPC (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SPBT : "Regla Solicitud de Servicio de SOLICITUD MATRICULA DE ASIGNATURA DIRIGIDA"
 */ 
FUNCTION F_RSS_SMAD (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

-----------------------------------------------------------------------------------------------------------------

PROCEDURE P_CHECK_DEPT_PRIORIDAD(
          P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_SRVC_CODE       IN SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
          P_CODE_DEPT       IN STVDEPT.STVDEPT_CODE%TYPE,
          P_CODE_TERM       IN STVTERM.STVTERM_CODE%TYPE,
          P_CODE_CAMP       IN STVCAMP.STVCAMP_CODE%TYPE,
          P_PRIORIDAD       OUT NUMBER
      );
-----------------------------------------------------------------------------------------------------------------
PROCEDURE P_GETDATA_ALUMN (
          P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_CODE_DEPT       OUT STVDEPT.STVDEPT_CODE%TYPE, 
          P_CODE_TERM       OUT STVTERM.STVTERM_CODE%TYPE,
          P_CODE_CAMP       OUT STVCAMP.STVCAMP_CODE%TYPE
      );
-----------------------------------------------------------------------------------------------------------------
PROCEDURE P_DATOS_INGRESO (
            P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_CAMP           IN STVCAMP.STVCAMP_CODE%type,
            P_TERM           IN STVTERM.STVTERM_CODE%TYPE,
            P_DEPT           IN STVDEPT.STVDEPT_CODE%type,
            P_INGRESANTE     OUT BOOLEAN,
            P_FECHA          OUT DATE
        );
-----------------------------------------------------------------------------------------------------------------
PROCEDURE P_CREDITOS_CICLO (
            P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_CREDITOS        OUT INTEGER,
            P_CICLO           OUT INTEGER
        );

/******************************************************************************************************************+
 * F_RSS_SMCV : "Regla Solicitud de Modificacion de Cuota Inicial - Verano"
*/
 FUNCTION F_RSS_SMCV (

    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE

) RETURN VARCHAR2;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    
END BWZKWFFN;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKWFFN;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKWFFN FOR BANINST1.BWZKWFFN;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKWFFN
--  START gurgrth BWZKWFFN
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/
