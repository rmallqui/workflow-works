/*
select dbms_metadata.get_ddl('PROCEDURE','P_INGRESANTE_TERM') from dual;

SET SERVEROUTPUT ON;
DECLARE P_INGRESANTE    BOOLEAN;
        P_FECHA         DATE;
BEGIN
  P_INGRESANTE_TERM(528091 , 'S01','201710','UREG',P_INGRESANTE,P_FECHA);
  DBMS_OUTPUT.PUT_LINE(case when P_INGRESANTE then 'true' else 'false' end );
  DBMS_OUTPUT.PUT_LINE(P_FECHA);
END;
*/


CREATE OR REPLACE PROCEDURE P_INGRESANTE_TERM (
    P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CAMP           IN STVCAMP.STVCAMP_CODE%type,
    P_TERM           IN STVTERM.STVTERM_CODE%TYPE,
    P_DEPT           IN STVDEPT.STVDEPT_CODE%type,
    P_INGRESANTE     OUT BOOLEAN,
    P_FECHA          OUT DATE
)
/* ===================================================================================================================
  NOMBRE    : F_INGRESANTE_TERM
  FECHA     : 03/04/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida si el alumno es ingresante para un periodo,sede,dept especifico.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
       
      P_APEC_CAMP      VARCHAR2(9);
      P_APEC_DEPT      VARCHAR2(9);
      P_APEC_TERM      VARCHAR2(10);
      
      V_C              INTEGER;
      V_ROW1           INTEGER;
      V_OUT            VARCHAR2(15);
      V_QUERY_INGRE    VARCHAR2(4000);
      V_CONTADOR       INTEGER := 0;

BEGIN

      SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP;-- CAMPUS 
      SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM;-- PERIODO
      SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT;-- DEPARTAMENTO

      V_QUERY_INGRE := '
      --CALCULAR A LOS MATRICULADOS BECA 18
      SELECT IDPersonaN, max(IDExamen) IDExamen --convert(NVARCHAR, max(IDExamen), 103) IDExamen
      FROM tblPersona PP
      INNER JOIN tblPersonaAlumno  PA
      ON PP.IDPersona = PA.IDPersona
      INNER JOIN (
          select distinct p.idalumno,p.IDExamen
          from tblalumnoestado a 
          INNER JOIN tblpostulante p
            ON a.IDDependencia = P.IDDependencia
            and a.IDPerAcad = P.IDPerAcad
            and a.IDAlumno = P.IDAlumno
            and a.FecInic = p.IDExamen
            and a.idescuela = p.idescuelaadm
          INNER JOIN tblEscuela 
            ON tblEscuela.IDEscuela = P.IDEscuela
                  and tblEscuela.IDDependencia =p .IDDependencia
          inner join tblModalidadPostu mpost 
            on mpost.IDModalidadPostu = P.IDModalidadPostu
                  and mpost.IDPerAcad = P.IDPerAcad
                  and mpost.IDDependencia = P.IDDependencia
                  and mpost.IDEscuelaADM = A.IDEscuela
          where p.iddependencia = ''UCCI''
          and p.idperacad = ''' || P_APEC_TERM || '''
          and a.idescuela = ''' || P_APEC_DEPT || '''
          and p.ingresante = ''1''
          and a.idsede = ''' || P_APEC_CAMP || '''
          and Renuncia=''0''
          and tblEscuela.Activo=''1''
          and p.IDEscuela not in (''506'',''510'',''601'')
      ) TM
      ON  PA.IDAlumno = TM.IDAlumno 
      AND IDPersonaN = ' || TO_CHAR(P_PIDM) || ' 
      GROUP BY IDPersonaN ';
     V_C := DBMS_HS_PASSTHROUGH.OPEN_CURSOR@BDUCCI.CONTINENTAL.EDU.PE;
     DBMS_HS_PASSTHROUGH.PARSE@BDUCCI.CONTINENTAL.EDU.PE(V_C,V_QUERY_INGRE);
     LOOP
          V_ROW1 := DBMS_HS_PASSTHROUGH.FETCH_ROW@BDUCCI.CONTINENTAL.EDU.PE(V_C);
          EXIT WHEN V_ROW1 = 0;
          DBMS_HS_PASSTHROUGH.GET_VALUE@BDUCCI.CONTINENTAL.EDU.PE (V_C,2, V_OUT);
          P_INGRESANTE := TRUE;    
          P_FECHA      := V_OUT;--TO_DATE(V_OUT, 'dd/mm/yyyy');
          DBMS_OUTPUT.PUT_LINE(V_OUT);
     END LOOP;
     DBMS_HS_PASSTHROUGH.CLOSE_CURSOR@BDUCCI.CONTINENTAL.EDU.PE(V_C);

END P_INGRESANTE_TERM;
