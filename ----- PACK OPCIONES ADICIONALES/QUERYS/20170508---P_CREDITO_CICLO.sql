/*
SET SERVEROUTPUT ON
DECLARE   
    P_CREDITOS        INTEGER;
    P_CICLO           INTEGER;
BEGIN
    P_CREDITOS_CICLO(123544,P_CREDITOS,P_CICLO);
    DBMS_OUTPUT.PUT_LINE( '************ ' || P_CREDITOS || ' & ' || P_CICLO);
END;
*/

CREATE OR REPLACE PROCEDURE P_CREDITOS_CICLO (
      P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_CREDITOS        OUT INTEGER,
      P_CICLO           OUT INTEGER
)
/* ===================================================================================================================
  NOMBRE    : P_CREDITOS_CICLO
  FECHA     : 08/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Calcula  los creditos y ciclo del estudiante.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    
    V_CRED_OBLIG          INTEGER := 0;  
    V_CRED_TOTAL          INTEGER := 0;
    V_CICLO_ACTL          INTEGER := 0;
    V_CRED_MATRI          INTEGER := 0;

    -- CURSOR GET TERM
    CURSOR C_CRED_CICLO IS
            SELECT UNIQUE
                   --PERIODO,
                   --PROGRAMA,
                   --DESCRIPCION_PROGRAMA,
                   --CAMPUS,
          --         NVL(CASE
          --             WHEN DIPLOMADO > CREDITOS_ELECTIVO THEN TO_NUMBER(REPLACE(DIPLOMADO,''))
          --             WHEN CREDITOS_ELECTIVO > DIPLOMADO THEN TO_NUMBER(REPLACE(CREDITOS_ELECTIVO,''))
          --             WHEN CREDITOS_ELECTIVO = DIPLOMADO THEN TO_NUMBER(REPLACE(DIPLOMADO,''))
          --             ELSE TO_NUMBER(REPLACE(CREDITOS_ELECTIVO,''))
          --         END,'0') AS CREDITOS_ELECTIVO,
                   NVL(CREDITOS_OBLIGATORIO,'0') AS CREDITOS_OBLIGATORIO,
                   NVL(CREDITOS_ACTUAL,'0') AS CREDITOS_TOTAL,

                   CASE WHEN CREDITOS_ACTUAL IS NULL THEN '1'
                   ELSE
                   NVL(CASE
                    WHEN CREDITOS_ACTUAL <= 22 AND EFF = '201510' THEN '1'
                    WHEN CREDITOS_ACTUAL > 22 AND CREDITOS_ACTUAL <= 44 AND EFF = '201510' THEN '2'
                    WHEN CREDITOS_ACTUAL > 44 AND CREDITOS_ACTUAL <= 66 AND EFF = '201510' THEN '3'
                    WHEN CREDITOS_ACTUAL > 66 AND CREDITOS_ACTUAL <= 88 AND EFF = '201510' THEN '4'
                    WHEN CREDITOS_ACTUAL > 88 AND CREDITOS_ACTUAL <= 110 AND EFF = '201510' THEN '5'

                    WHEN CREDITOS_ACTUAL > 110 AND CREDITOS_ACTUAL <= 135 AND EFF = '201510' THEN '6'
                    WHEN CREDITOS_ACTUAL > 135 AND CREDITOS_ACTUAL <= 160 AND EFF = '201510' THEN '7'

                    WHEN CREDITOS_ACTUAL > 160 AND CREDITOS_ACTUAL <= 182 AND EFF = '201510' THEN '8'
                    WHEN CREDITOS_ACTUAL > 182 AND CREDITOS_ACTUAL <= 204 AND EFF = '201510' THEN '9'
                    WHEN CREDITOS_ACTUAL > 204 AND CREDITOS_ACTUAL <= 226 AND EFF = '201510' THEN '10'
                    WHEN CREDITOS_ACTUAL > 226 AND CREDITOS_ACTUAL <= 248 AND EFF = '201510' AND PROGRAMA IN ('312','502') THEN '11'
                    WHEN CREDITOS_ACTUAL > 248 AND CREDITOS_ACTUAL <= 270 AND EFF = '201510' AND PROGRAMA IN ('312','502') THEN '12'
                    WHEN CREDITOS_ACTUAL > 270 AND CREDITOS_ACTUAL <= 314 AND EFF = '201510' AND PROGRAMA IN ('502') THEN '13'
                    WHEN CREDITOS_ACTUAL > 314 AND PROGRAMA IN ('502') THEN '14'
                    ELSE REPLACE(NVL(CASE
                             WHEN CREDITOS_ACTUAL <= 22 AND EFF = '200710' THEN '1'
                             WHEN CREDITOS_ACTUAL > 22 AND CREDITOS_ACTUAL <= 44 AND EFF = '200710' THEN '2'
                             WHEN CREDITOS_ACTUAL > 44 AND CREDITOS_ACTUAL <= 66 AND EFF = '200710' THEN '3'
                             WHEN CREDITOS_ACTUAL > 66 AND CREDITOS_ACTUAL <= 88 AND EFF = '200710' THEN '4'
                             WHEN CREDITOS_ACTUAL > 88 AND CREDITOS_ACTUAL <= 110 AND EFF = '200710' THEN '5'

                             WHEN CREDITOS_ACTUAL > 110 AND CREDITOS_ACTUAL <= 132 AND EFF = '200710' THEN '6'
                             WHEN CREDITOS_ACTUAL > 132 AND CREDITOS_ACTUAL <= 154 AND EFF = '200710' THEN '7'

                             WHEN CREDITOS_ACTUAL > 154 AND CREDITOS_ACTUAL <= 176 AND EFF = '200710' THEN '8'
                             WHEN CREDITOS_ACTUAL > 176 AND CREDITOS_ACTUAL <= 198 AND EFF = '200710' THEN '9'
                             WHEN CREDITOS_ACTUAL > 198 AND CREDITOS_ACTUAL <= 220 AND EFF = '200710' THEN '10'
                             WHEN CREDITOS_ACTUAL > 220 AND CREDITOS_ACTUAL <= 242 AND EFF = '200710' AND PROGRAMA IN ('312','502') THEN '11'
                             WHEN CREDITOS_ACTUAL > 242 AND CREDITOS_ACTUAL <= 264 AND EFF = '200710' AND PROGRAMA IN ('312','502') THEN '12'
                             WHEN CREDITOS_ACTUAL > 264 AND CREDITOS_ACTUAL <= 286 AND EFF = '200710' AND PROGRAMA IN ('502') THEN '13'
                             WHEN CREDITOS_ACTUAL > 286 AND PROGRAMA IN ('502') THEN '14'
                             ELSE REPLACE(NVL(CASE
                                      WHEN CREDITOS_ACTUAL <= 22 AND EFF = '201710' THEN '1'
                                      WHEN CREDITOS_ACTUAL > 22 AND CREDITOS_ACTUAL <= 44 AND EFF = '201710' THEN '2'
                                      WHEN CREDITOS_ACTUAL > 44 AND CREDITOS_ACTUAL <= 66 AND EFF = '201710' THEN '3'
                                      WHEN CREDITOS_ACTUAL > 66 AND CREDITOS_ACTUAL <= 88 AND EFF = '201710' THEN '4'
                                      WHEN CREDITOS_ACTUAL > 88 AND CREDITOS_ACTUAL <= 110 AND EFF = '201710' THEN '5'

                                      WHEN CREDITOS_ACTUAL > 110 AND CREDITOS_ACTUAL <= 135 AND EFF = '201710' THEN '6'
                                      WHEN CREDITOS_ACTUAL > 135 AND CREDITOS_ACTUAL <= 160 AND EFF = '201710' THEN '7'

                                      WHEN CREDITOS_ACTUAL > 160 AND CREDITOS_ACTUAL <= 182 AND EFF = '201710' THEN '8'
                                      WHEN CREDITOS_ACTUAL > 182 AND CREDITOS_ACTUAL <= 204 AND EFF = '201710' THEN '9'
                                      WHEN CREDITOS_ACTUAL > 204 AND CREDITOS_ACTUAL <= 226 AND EFF = '201710' THEN '10'
                                      WHEN CREDITOS_ACTUAL > 226 AND CREDITOS_ACTUAL <= 248 AND EFF = '201710' AND PROGRAMA IN ('312','502') THEN '11'
                                      WHEN CREDITOS_ACTUAL > 248 AND CREDITOS_ACTUAL <= 270 AND EFF = '201710' AND PROGRAMA IN ('312','502') THEN '12'
                                      WHEN CREDITOS_ACTUAL > 270 AND CREDITOS_ACTUAL <= 314 AND EFF = '201710' AND PROGRAMA IN ('502') THEN '13'
                                      WHEN CREDITOS_ACTUAL > 314 AND PROGRAMA IN ('502') THEN '14'
                                  END,''),'','')
                          END,''),'','')
                   END,'10') END AS CICLO_ACTUAL,

                   CREDITO AS CREDITOS_MATRICULADO
          FROM (
          WITH T1 AS (
            SELECT DISTINCT sfrstcr_pidm CODIGO,
                            sfrstcr_term_code AS PERIODO,
                            sorlcur_program PROGRAMA,
                            szvmajr_desc DESCRIPCION_PROGRAMA,
                            SORLCUR_LEVL_CODE NIVEL,
                            SORLCUR_CAMP_CODE CAMPUS,
                            SORLFOS_DEPT_CODE DEPARTAMENTO,
                            sum(sfrstcr_credit_hr) credito
                            FROM   sfrstcr f
                                   INNER JOIN sorlcur r
                                           ON r.sorlcur_pidm = f.sfrstcr_pidm
                                   INNER JOIN SORLFOS
                                           ON SORLFOS_PIDM = R.SORLCUR_PIDM
                                           AND SORLFOS_LCUR_SEQNO = R.SORLCUR_SEQNO
                                           AND SORLFOS_TERM_CODE = R.SORLCUR_TERM_CODE
                                   INNER JOIN szvmajr ES
                                           ON r.sorlcur_program = ES.szvmajr_code
                            WHERE  sfrstcr_rsts_code IN ( 'RW', 'RE' )
                                   AND R.sorlcur_roll_ind = 'Y'
                                   AND R.sorlcur_cact_code = 'ACTIVE'
                                   AND R.sorlcur_term_code = (SELECT Max(sorlcur_term_code)
                                                              FROM   sorlcur
                                                              WHERE
                                                               sorlcur_pidm = R.sorlcur_pidm
                                                               AND sorlcur_roll_ind = 'Y'
                                                               AND sorlcur_cact_code =
                                                                   'ACTIVE')
                                   AND sorlcur_term_code_end IS NULL
                                   AND sfrstcr_PIDM = 342961
                            -- Termina Bloque de condiciones
                            GROUP  BY sfrstcr_pidm,
                                      sfrstcr_term_code,
                                      sorlcur_program,
                                      SORLFOS_DEPT_CODE,
                                      SORLCUR_CAMP_CODE,
                                      SORLCUR_LEVL_CODE,
                                      szvmajr_desc
          ), T2 AS (
                     SELECT UNIQUE SMBPOGN_PIDM,
                     SMBPOGN_REQUEST_NO,
                     SMRDOUS_CRSE_SOURCE,
                     SMBPOGN_TERM_CODE_EFF,
                     CASE
                       WHEN SMBPOGN_TERM_CODE_EFF >= '200710' AND SMBPOGN_TERM_CODE_EFF < '201510' THEN '200710'
                       WHEN SMBPOGN_TERM_CODE_EFF >= '201510' AND SMBPOGN_TERM_CODE_EFF < '201710' THEN '201510'
                       --WHEN SMBPOGN_TERM_CODE_EFF >= '201710' AND SMBPOGN_TERM_CODE_EFF < '' THEN ''
                       ELSE REPLACE(NVL(SMBPOGN_TERM_CODE_EFF,''),'','')
                     END EFF,
                     SMBPOGN_REQ_CREDITS_OVERALL,
                     SMBPOGN_ACT_CREDITS_OVERALL,
                     SMBPOGN_CAMP_CODE
              FROM SMBPOGN LEFT JOIN SMRDOUS
              ON SMBPOGN_PIDM = SMRDOUS_PIDM
              AND SMBPOGN_REQUEST_NO = SMRDOUS_REQUEST_NO
              INNER JOIN (
                    SELECT SMBPOGN_PIDM AS PIDPOGN,
                    MAX(SMBPOGN_REQUEST_NO) AS RQPOGN
                    FROM SMBPOGN
                    GROUP BY SMBPOGN_PIDM
                    ) ON SMBPOGN_PIDM=PIDPOGN
                      AND SMBPOGN_REQUEST_NO=RQPOGN

          ),T3 AS (

               SELECT SMBPOGN_PIDM,
                 SMRDRRQ_REQUEST_NO,

                 --ORIGINAL
                 SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) AS CREDITOS_ELECTIVO,
                 --SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) AS CREDITOS_ELECTIVO,

                 --ORIGINAL
                 SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) -
                 --SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) -
                 SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) -
                 SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END) AS CREDITOS_OBLIGATORIO,

                 SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) AS DIPLOMADO,

                 --ORIGINAL SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) +
                 --SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) +
                 --(
                 --SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) -
                 --SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) -
                 --SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END)
                 --) AS CREDITOS_ACTUAL

                 CASE
                     WHEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) > SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0))
                          THEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) + (SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) - SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) - SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END))
                     WHEN SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) > SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS))
                          THEN SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) + (SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) - SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) - SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END))
                     WHEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) = SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0))
                          THEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) + (SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) - SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) - SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END))
                     ELSE SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) +
                          (
                           SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) -
                           SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) -
                           SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END)
                          )
                 END AS CREDITOS_ACTUAL

                FROM SMRDOUS
            LEFT JOIN SMRDRRQ
              ON SMRDRRQ_PIDM = SMRDOUS_PIDM
              AND SMRDRRQ_REQUEST_NO = SMRDOUS_REQUEST_NO
              AND SMRDOUS_AREA = SMRDRRQ_AREA
              AND SMRDOUS_CAA_SEQNO = SMRDRRQ_CAA_SEQNO
              AND SMRDOUS_KEY_RULE = SMRDRRQ_KEY_RULE
              AND SMRDOUS_TERM_CODE_EFF = SMRDRRQ_TERM_CODE_EFF
              AND SMRDOUS_RUL_SEQNO = SMRDRRQ_RUL_SEQNO
            LEFT JOIN SMBPOGN ON SMBPOGN_PIDM = SMRDOUS_PIDM
              AND SMBPOGN_REQUEST_NO = SMRDOUS_REQUEST_NO
            WHERE SMRDOUS_CRSE_SOURCE IN ('H','T')
            GROUP BY SMBPOGN_PIDM,SMRDRRQ_REQUEST_NO
          )
          SELECT DISTINCT * FROM T1
          LEFT JOIN T2 ON T2.SMBPOGN_PIDM = T1.CODIGO
          LEFT JOIN T3 ON T2.SMBPOGN_PIDM = T3.SMBPOGN_PIDM
                       AND T2.SMBPOGN_REQUEST_NO = T3.SMRDRRQ_REQUEST_NO
          );

BEGIN
    
    OPEN C_CRED_CICLO;
    LOOP
      FETCH C_CRED_CICLO INTO V_CRED_OBLIG, V_CRED_TOTAL, V_CICLO_ACTL, V_CRED_MATRI;
      EXIT WHEN C_CRED_CICLO%NOTFOUND;
    END LOOP;
    CLOSE C_CRED_CICLO;

    P_CREDITOS     := V_CRED_TOTAL;
    P_CICLO        := V_CICLO_ACTL;

END P_CREDITOS_CICLO;
