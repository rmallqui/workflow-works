/*
SET SERVEROUTPUT ON
DECLARE   P_PRIORIDAD       NUMBER; 
          P_CODE_TERM       SFBETRM.SFBETRM_TERM_CODE%TYPE;
          P_CODE_DEPT       SGBSTDN.SGBSTDN_DEPT_CODE%TYPE;
          P_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
BEGIN
    P_GETDATA_ALUMN(111893,P_CODE_DEPT,P_CODE_TERM,P_CODE_CAMP);
    DBMS_OUTPUT.PUT_LINE(P_CODE_DEPT || ' & ' || P_CODE_TERM || ' & ' || P_CODE_CAMP);
    
    P_CHECK_DEPT_PRIORIDAD(111893,'SOL005',P_CODE_DEPT,P_CODE_TERM,P_CODE_CAMP,P_PRIORIDAD);
    DBMS_OUTPUT.PUT_LINE(P_PRIORIDAD);
END;
*/

CREATE OR REPLACE PROCEDURE P_CHECK_DEPT_PRIORIDAD(
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       IN SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_CODE_DEPT       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CODE_TERM       IN STVTERM.STVTERM_CODE%TYPE,
    P_CODE_CAMP       IN STVCAMP.STVCAMP_CODE%TYPE,
    P_PRIORIDAD       OUT NUMBER
) 
/* ===================================================================================================================
  NOMBRE    : P_CHECK_DEPT_PRIORIDAD
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida la fecha de hoy este dentro de las fechas para generar una solicitud, 
              incluyendo el departamento que le corresponde.
  =================================================================================================================== */
 IS
      V_INDICADOR               NUMBER := -1;
      V_TERMADMIN               NUMBER := -1;
      V_RETURN                  NUMBER := -1;
      V_CODE_DEPT               STVDEPT.STVDEPT_CODE%TYPE;
      V_SVRRSRV_SEQ_NO          SVRRSRV.SVRRSRV_SEQ_NO%TYPE;
      V_SVRRSRV_WEB_IND         SVRRSRV.SVRRSRV_WEB_IND%TYPE;
      
      -- GET DEPT en caso no se envie entre los parametros
      CURSOR C_SORLCUR IS
      SELECT    SORLFOS_DEPT_CODE  
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC,SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;

      -- GET prioridad
      CURSOR C_SVRRSRV IS
      SELECT SVRRSRV_SEQ_NO, SVRRSRV_WEB_IND FROM SVRRSRV 
      WHERE SVRRSRV_SRVC_CODE = P_SRVC_CODE
      AND ( 
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE(TO_CHAR(SVRRSRV_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
            AND
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE(TO_CHAR(SVRRSRV_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          )
      AND SVRRSRV_SEQ_NO IN ( (0 + V_INDICADOR) , (1 + V_INDICADOR), (2 + V_INDICADOR) )
      ORDER BY SVRRSRV_SEQ_NO ASC; 
      
BEGIN  
--    
      P_PRIORIDAD := 0;
      V_CODE_DEPT := P_CODE_DEPT; 
      
      -- en caso se envie DEPT = null o vacio
      IF NVL(TRIM(V_CODE_DEPT),'TRUE') = 'TRUE'  THEN
      
              -- GET datos de alumno 
              OPEN C_SORLCUR;
              LOOP
                FETCH C_SORLCUR INTO V_CODE_DEPT;
                EXIT WHEN C_SORLCUR%NOTFOUND;
                END LOOP;
              CLOSE C_SORLCUR;
              
      END IF;
      
      SELECT CASE V_CODE_DEPT 
                  WHEN 'UREG' THEN 1
                  WHEN 'UPGT' THEN 4
                  WHEN 'UVIR' THEN 7
                  ELSE -99 END INTO V_INDICADOR
      FROM DUAL;
      
      -- GET COINCIDENCIAS A LOS FILTROS 
      -- UREG 1,2,3
      -- UPGT 4,5,6
      -- UVIR 7,8,9    
      -- GET DEPARTAMENTO, SEDE
      OPEN C_SVRRSRV;
      LOOP
        FETCH C_SVRRSRV INTO V_SVRRSRV_SEQ_NO, V_SVRRSRV_WEB_IND;
        IF C_SVRRSRV%FOUND THEN
            P_PRIORIDAD := CASE WHEN V_SVRRSRV_WEB_IND = 'N' THEN 0 ELSE V_SVRRSRV_SEQ_NO END;
        ELSE EXIT;
      END IF;
      END LOOP;
      CLOSE C_SVRRSRV;
      
--
END P_CHECK_DEPT_PRIORIDAD;