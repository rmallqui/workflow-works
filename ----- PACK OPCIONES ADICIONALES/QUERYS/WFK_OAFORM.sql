CREATE OR REPLACE PACKAGE WFK_OAFORM AS
/*******************************************************************************
 WFK_OAFORM:
       Conti Package OPCIONES ADICIONALES FORMA
*******************************************************************************/
-- FILE NAME..: WFK_OAFORM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_OAFORM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

/******************************************************************************************************************+
 * F_RSS_CPE : "Regla Solicitud de Servicio de CAMBIO de PLAN ESTUDIO"
 */ 
FUNCTION F_RSS_CPE (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SMCI : "Regla Solicitud de Servicio de MODIFICACIÓN de la CUOTA INICIAL"
 */ 
FUNCTION F_RSS_SMCI (
			P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
			P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
			P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
		) RETURN VARCHAR2;


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    
END WFK_OAFORM;