/******************************************************************************/
/* BWZKWFFN.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripci�n corta: Script para generar el Paquete de funciones para el     */
/*                    WorkFlow                                                */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creaci�n del C�digo.                                    EAG 23/NOV/2017 */
/* Se cambia el nombre del procedimiento WFK_OAFORM POR BWZKWFFN              */
/*  Function F_RSS_SRIR: "Regla Solicitud de Servicio de SOLICITUD            */
/*    RECTIFICACI�N DE INSCRIPCI�N REZAGADOS"                                 */
/*  Function F_RSS_SRM: "Regla Solicitud de Servicio de SOLICITUD             */
/*    RESERVA DE MATRICULA"                                                   */
/*  Function F_RSS_CPE: "Regla Solicitud de Servicio de CAMBIO PLAN ESTUDIO"  */
/*  Function F_RSS_SRD: "Regla Solicitud de Servicio de SOLICITUD DE SOBREPASO*/
/*    DE RETENCION DE DOCUMENTOS"                                             */
/*  Function F_RSS_SMCI: "Regla Solicitud de Servicio de MODIFICACI�N de la   */
/*    CUOTA INICIAL (MATRICULA ESPECIAL)  SOL007"                             */
/*  Function F_RSS_SR: "Regla Solicitud de Servicio de SOLICITUD DE           */
/*    REINCORPORACI�N"                                                        */
/*  Function F_RSS_SESM: "Regla Solicitud de Servicio de SOLICITUD DE         */
/*    EXONERACI�N DE SEGURO DE SALUD"                                         */
/*  Function F_RSS_STI: "Regla Solicitud de Servicio de SOLICITUD DE          */
/*    TRASLADO INTERNO "                                                      */
/*  Function F_UA_RSS_SPGA: "Regla Solicitud de Servicio de SOLICITUD DE      */
/*    PRORROGA GASTOS ADMINISTRATIVOS "                                       */
/*  Function F_RSS_SPBT: "Regla Solicitud de Servicio de SOLICITUD DE         */
/*    TRASLADO INTERNO "                                                      */
/*  Function F_RSS_SDPC: "Regla Solicitud de Servicio DESCUENTO POR CONVENIO" */
/*  Procedure P_CHECK_DEPT_PRIORIDAD: Valida la fecha de hoy este dentro de   */
/*    las fechas para generar una solicitud, incluyendo el departamento       */
/*    que le corresponde.                                                     */
/*  Procedure P_GETDATA_ALUMN: Retorn el periodo, Departamento y la           */
/*    sede del alumno.                                                        */
/*  Procdure P_DATOS_INGRESO: Valida si el alumno es ingresante para un       */
/*    periodo,sede,dept especifico.                                           */
/*  Procedure P_CREDITOS_CICLO: Calcula  los creditos y ciclo del estudiante. */

/*  2.Actualizaci�n F_RSS_SRIR                                RML 09/02/2017  */
/*    Agrego validacion solo para matriculados (CON NRC)                      */
/*  3.Actualizaci�n F_RSS_SRIR                                RML 26/04/2017  */
/*    Se agrego validacion para el segundo modulo de UVIR y UPGT              */
/*  4.Actualizaci�n F_RSS_SRM                                 RML 26/04/2017  */
/*    Se agrego validacion para el segundo modulo de UVIR y UPGT              */
/*  5.Actualizaci�n F_RSS_SRIR                                RML 13/06/2017  */
/*    Se prioriza y obtiene el periodo mas antiguo en caso de cruce de fechas.*/
/*  6.Actualizaci�n F_RSS_SRM                                 RML 13/06/2017  */
/*    Se prioriza y obtiene el periodo mas antiguo en caso de cruce de fechas.*/
/*  7.Actualizaci�n F_RSS_CPE                                 RML 16/06/2017  */
/*    Se agregaron filtros: Especificados en el acta 006-WF-2017 con          */
/*    fecha 07-Jun-2017                                                       */
/*  8.Actualizaci�n F_RSS_STI                                 RML 27/07/2017  */
/*    Se agrego la VALIDACION de retencion ('01'-deuda pendiente)             */
/*  9.Actualizaci�n F_RSS_STI                                 RML 02/08/2017  */
/*    Se agrego la validacion de que la solicitud solo se puede realizar una  */
/*    vez por periodo.                                                        */
/*    --------------------                                                    */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 

CREATE OR REPLACE PACKAGE BWZKWFFN AS
/*******************************************************************************
 BWZKWFFN:
       Conti Package OPCIONES ADICIONALES FORMA
*******************************************************************************/
-- FILE NAME..: BWZKWFFN.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKWFFN
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

/******************************************************************************************************************+
 * F_RSS_SRIR : "Regla Solicitud de Servicio de RECTIFICACI�N DE INSCRIPCI�N REZAGADOS"
 */ 
FUNCTION F_RSS_SRIR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SRM : "Regla Solicitud de Servicio de SOLICITUD RESERVA DE MATRICULA"
 */ 
FUNCTION F_RSS_SRM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_CPE : "Regla Solicitud de Servicio de CAMBIO de PLAN ESTUDIO"
 */ 
FUNCTION F_RSS_CPE (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SRD : "Regla Solicitud de Servicio de SOLICITUD DE SOBREPASO DE RETENCION DE DOCUMENTOS"
 */ 
FUNCTION F_RSS_SRD (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SMCI : "Regla Solicitud de Servicio de MODIFICACI�N de la CUOTA INICIAL"
 */ 
FUNCTION F_RSS_SMCI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SR : "Regla Solicitud de Servicio de SOLICITUD DE REINCORPORACI�N"
 */ 
FUNCTION F_RSS_SR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SESM : "Regla Solicitud de Servicio de SOLICITUD DE EXONERACI�N DE SEGURO DE SALUD"
 */ 
FUNCTION F_RSS_SESM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_STI : "Regla Solicitud de Servicio de SOLICITUD DE TRASLADO INTERNO (CONTISTI)"
 */ 
FUNCTION F_RSS_STI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SPGA : "Regla Solicitud de Servicio de SOLICITUD DE PRORROGA para GASTO ADMINISTATIVO"
 */ 
FUNCTION F_RSS_SPGA (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SPBT : "Regla Solicitud de Servicio de SOLICITUD PAGOS de BACHILLER y TITULO"
 */ 
FUNCTION F_RSS_SPBT (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SPBT : "Regla Solicitud de Servicio de SOLICITUD DESCUENTO POR CONVENIO"
 */ 
FUNCTION F_RSS_SDPC (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

-----------------------------------------------------------------------------------------------------------------

PROCEDURE P_CHECK_DEPT_PRIORIDAD(
          P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_SRVC_CODE       IN SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
          P_CODE_DEPT       IN STVDEPT.STVDEPT_CODE%TYPE,
          P_CODE_TERM       IN STVTERM.STVTERM_CODE%TYPE,
          P_CODE_CAMP       IN STVCAMP.STVCAMP_CODE%TYPE,
          P_PRIORIDAD       OUT NUMBER
      );
-----------------------------------------------------------------------------------------------------------------
PROCEDURE P_GETDATA_ALUMN (
          P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_CODE_DEPT       OUT STVDEPT.STVDEPT_CODE%TYPE, 
          P_CODE_TERM       OUT STVTERM.STVTERM_CODE%TYPE,
          P_CODE_CAMP       OUT STVCAMP.STVCAMP_CODE%TYPE
      );
-----------------------------------------------------------------------------------------------------------------
PROCEDURE P_DATOS_INGRESO (
            P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_CAMP           IN STVCAMP.STVCAMP_CODE%type,
            P_TERM           IN STVTERM.STVTERM_CODE%TYPE,
            P_DEPT           IN STVDEPT.STVDEPT_CODE%type,
            P_INGRESANTE     OUT BOOLEAN,
            P_FECHA          OUT DATE
        );
-----------------------------------------------------------------------------------------------------------------
PROCEDURE P_CREDITOS_CICLO (
            P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_CREDITOS        OUT INTEGER,
            P_CICLO           OUT INTEGER
        );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    
END BWZKWFFN;
