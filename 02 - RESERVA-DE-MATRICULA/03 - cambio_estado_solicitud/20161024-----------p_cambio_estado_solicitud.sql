/*
drop procedure p_cambio_estado_solicitud;
GRANT EXECUTE ON p_cambio_estado_solicitud TO wfobjects;
GRANT EXECUTE ON p_cambio_estado_solicitud TO wfauto;

set serveroutput on
DECLARE P_ERROR VARCHAR2(100);
BEGIN
    P_CAMBIO_ESTADO_SOLICITUD(85,'AS',P_ERROR);
END;

*/



create or replace PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_ERROR               OUT VARCHAR2
  )
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 17/10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : finaliza la solicitud (XXXXXX) presentada ya sea por su atención o vencimiento del tiempo de solicitud

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   20/10/2016    rmallquil     Validar las los estados disponibles a MODIFICACION DE ESTADO.
  =================================================================================================================== */

AS
                P_ID_ALUMNO                 SPRIDEN.SPRIDEN_PIDM%TYPE;
                P_CODIGO_SOLICITUD          SVVSRVC.SVVSRVC_CODE%TYPE;
                P_ESTADO_OLD                SVRSVPR.SVRSVPR_SRVS_CODE%TYPE;
                P_METOD_ENTREGA             STVWSSO.STVWSSO_CODE%TYPE;
                P_COSTO_SOLICITUD           SVRSVPR.SVRSVPR_PROTOCOL_AMOUNT%TYPE;
                P_CODIGO_DETALLE            SVRRSSO.SVRRSSO_DETL_CODE%TYPE;
                P_PERIODO                   SFBETRM.SFBETRM_TERM_CODE%TYPE;
                NO_SERVICIO                 VARCHAR2(30);
                P_TRAN_NUMBER_NEW           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE    := 0;
                P_TRAN_NUMBER_OLD           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE    := 0;
                P_BILLING_IND_OLD           SVRSVPR.SVRSVPR_BILLING_IND%TYPE;
                P_ROWID                     GB_COMMON.INTERNAL_RECORD_ID_TYPE;
BEGIN
--

    P_ERROR := '';
    
    ----------------------------------------------------------------------------
        -- GET VALUES
        SELECT  SVRSVPR_PIDM,
                SVRSVPR_SRVC_CODE,        -- ejem 'SOL003'
                SVRSVPR_SRVS_CODE,        -- estado solicitud
                SVRSVPR_WSSO_CODE,        -- metodo entrega
                SVRSVPR_ACCD_TRAN_NUMBER, -- Numero de transaccion actual
                SVRSVPR_PROTOCOL_AMOUNT,  -- costo po la entrega
                SVRSVPR_TERM_CODE
        INTO    P_ID_ALUMNO,
                P_CODIGO_SOLICITUD, 
                P_ESTADO_OLD, 
                P_METOD_ENTREGA, 
                P_TRAN_NUMBER_OLD,
                P_COSTO_SOLICITUD,
                P_PERIODO
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
        
        IF P_PERIODO IS NULL THEN
              P_PERIODO := '999999';
        END IF;
        
        -- GET CODE_DETAIL segun el metodo entrega de la SOLICITUD
        SELECT SVRRSSO_DETL_CODE INTO P_CODIGO_DETALLE
        FROM  SVRRSSO
        WHERE SVRRSSO_RSRV_SEQ_NO  = 1         
        AND   SVRRSSO_SRVC_CODE    = P_CODIGO_SOLICITUD  
        AND   SVRRSSO_WSSO_CODE    = P_METOD_ENTREGA;     
        
        NO_SERVICIO := 'Service Request No ' || to_char(P_NUMERO_SOLICITUD);
        
        -- VALIDAR si ya se genero una deuda anterior de solicitud por un estado 'AP'.
            -- configuracion AP - aprovado con deuda
            --               AS - aprovado sin deuda
        IF (P_ESTADO = 'AP' AND P_ESTADO_OLD <> 'AP' AND P_TRAN_NUMBER_OLD IS NULL ) 
        THEN
            -- GENERAR DEUDA
            TB_RECEIVABLE.p_create ( p_pidm                 =>  P_ID_ALUMNO,        -- PIDEM ALUMNO
                                     p_term_code            =>  P_PERIODO,          -- DETALLE
                                     p_detail_code          =>  P_CODIGO_DETALLE,   -- CODIGO DETALLE 
                                     p_user                 =>  USER,               -- USUARIO
                                     p_entry_date           =>  SYSDATE,     
                                     p_amount               =>  P_COSTO_SOLICITUD,
                                     p_effective_date       =>  SYSDATE + 3 ,
                                     p_bill_date            =>  NULL,    
                                     p_due_date             =>  NULL,    
                                     p_desc                 =>  NO_SERVICIO,    
                                     p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                     p_tran_number_paid     =>  NULL,     -- numero de transaccion que será pagara
                                     p_crossref_pidm        =>  NULL,    
                                     p_crossref_number      =>  NULL,    
                                     p_crossref_detail_code =>  NULL,     
                                     p_srce_code            =>  'T',    
                                     p_acct_feed_ind        =>  'Y',
                                     p_session_number       =>  0,    
                                     p_cshr_end_date        =>  NULL,    
                                     p_crn                  =>  NULL,
                                     p_crossref_srce_code   =>  NULL,
                                     p_loc_mdt              =>  NULL,
                                     p_loc_mdt_seq          =>  NULL,    
                                     p_rate                 =>  NULL,    
                                     p_units                =>  NULL,     
                                     p_document_number      =>  NULL,    
                                     p_trans_date           =>  NULL,    
                                     p_payment_id           =>  NULL,    
                                     p_invoice_number       =>  NULL,    
                                     p_statement_date       =>  NULL,    
                                     p_inv_number_paid      =>  NULL,    
                                     p_curr_code            =>  NULL,    
                                     p_exchange_diff        =>  NULL,    
                                     p_foreign_amount       =>  NULL,    
                                     p_late_dcat_code       =>  NULL,    
                                     p_atyp_code            =>  NULL,    
                                     p_atyp_seqno           =>  NULL,    
                                     p_card_type_vr         =>  NULL,    
                                     p_card_exp_date_vr     =>  NULL,     
                                     p_card_auth_number_vr  =>  NULL,    
                                     p_crossref_dcat_code   =>  NULL,    
                                     p_orig_chg_ind         =>  NULL,    
                                     p_ccrd_code            =>  NULL,    
                                     p_merchant_id          =>  NULL,    
                                     p_data_origin          =>  'WorkFlow',    
                                     p_override_hold        =>  'N',     
                                     p_tran_number_out      =>  P_TRAN_NUMBER_NEW, 
                                     p_rowid_out            =>  P_ROWID);
            
            -- UPDATE SOLICITUD
            UPDATE SVRSVPR
              SET SVRSVPR_SRVS_CODE = P_ESTADO, -- AN , AP , RE
              SVRSVPR_ACCD_TRAN_NUMBER = P_TRAN_NUMBER_NEW,
              SVRSVPR_BILLING_IND = 'Y',
              SVRSVPR_DATA_ORIGIN = 'WorkFlow'
            WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
            
        ELSE
            
            -- UPDATE SOLICITUD
            UPDATE SVRSVPR
              SET SVRSVPR_SRVS_CODE = P_ESTADO, -- AN , AP , RE
              SVRSVPR_ACCD_TRAN_NUMBER = P_TRAN_NUMBER_OLD,
              SVRSVPR_BILLING_IND = CASE WHEN P_ESTADO = 'AP' THEN 'Y' ELSE 'N' END,
              SVRSVPR_DATA_ORIGIN = 'WorkFlow'
            WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
            AND SVRSVPR_SRVS_CODE IN (
               -- Estados de solic. MODIFICABLES segun las reglas y configuracion
               SELECT  SVRRSST_SRVS_CODE       TEMP_SRVS_CODE --- Estado SOL
               FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
               INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
               ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
               AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
               INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
               ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
               WHERE SVRRSRV_SRVC_CODE = 'SOL003'
               AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
               AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
               AND SVRRSST_SRVS_CODE <> P_ESTADO ---------------- actualizar a un estado distinto
            );
        END IF;

      
      --------------------------------------------------------------------------
--
    COMMIT;
EXCEPTION
  WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_CAMBIO_ESTADO_SOLICITUD;