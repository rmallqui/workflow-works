create or replace PACKAGE WFK_CONTISRM AS
/*******************************************************************************
 WFK_CONTISRM:
       Conti Package body SOLICITUD RESERVA DE MATRICULA
*******************************************************************************/
-- FILE NAME..: WFK_CONTISRM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISRM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

FUNCTION F_GET_SIDEUDA_ALUMNO
              (
                P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
                P_PERIODO             IN STVTERM.STVTERM_CODE%TYPE,
                P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE
              ) RETURN VARCHAR2 ;

--------------------------------------------------------------------------------

PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
                P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
                P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                P_FECHA_VALIDA        OUT VARCHAR2
              );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
                P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
                P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                P_ERROR               OUT VARCHAR2
              );


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTISRM;