
SET SERVEROUTPUT ON
DECLARE BOOL BOOLEAN;
BEGIN
        BOOL :=  F_GET_SIDEUDA_ALUMNO(325180,'C00');
        DBMS_OUTPUT.PUT_LINE(CASE WHEN BOOL = TRUE THEN 'TRUE' ELSE 'FALSE' END);
END;

create or replace FUNCTION F_GET_SIDEUDA_ALUMNO
              (
                P_PIDM_ALUMNO         SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_CODIGO_DETALLE      TBRACCD.TBRACCD_DETAIL_CODE%TYPE
              ) RETURN BOOLEAN 
              
/* ===================================================================================================================
  NOMBRE    : F_GET_SIDEUDA_ALUMNO
  FECHA     : 08//11/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un código de detalle verifique la existencia de 
              deuda de MATRICULA del alumno en cualquier periodo, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  
  =================================================================================================================== */
              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR        NUMBER := 0;
BEGIN
--
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
    COMMIT;
        
    -- GET deuda
    SELECT COUNT(TZRCDAB_AMOUNT)
    INTO   P_INDICADOR
    FROM   TZRCDAB
    WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
    AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
    AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE;         -- CODIGO MATRICULA 'C00'
    --AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
      
    RETURN(P_INDICADOR > 0);

END F_GET_SIDEUDA_ALUMNO;

----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------

SET SERVEROUTPUT ON
DECLARE P_DEUDA VARCHAR2(10);
BEGIN
        P_GET_SIDEUDA_ALUMNO(325180,'C00',P_DEUDA);
        DBMS_OUTPUT.PUT_LINE(P_DEUDA);
END;

create or replace PROCEDURE P_GET_SIDEUDA_ALUMNO
              (
                P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
                P_DEUDA               OUT VARCHAR2
              )              
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA_ALUMNO
  FECHA     : 08//11/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un código de detalle verifique la existencia de 
              deuda de MATRICULA del alumno en cualquier periodo, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  
  =================================================================================================================== */
              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR           NUMBER;
BEGIN
--
    
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
    COMMIT;
        
    -- GET deuda
    SELECT COUNT(TZRCDAB_AMOUNT)
    INTO   P_INDICADOR
    FROM   TZRCDAB
    WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
    AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
    AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE;         -- CODIGO MATRICULA 'C00'
    --AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
   
    IF P_INDICADOR > 0 THEN
      P_DEUDA   := 'TRUE';
    ELSE
      P_DEUDA   :='FALSE';
    END IF;

END P_GET_SIDEUDA_ALUMNO;