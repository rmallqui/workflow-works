CREATE OR REPLACE FUNCTION F_GET_SIDEUDA_ALUMNO
              (
                P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
                P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
                P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
              ) RETURN VARCHAR2 
              
/* ===================================================================================================================
  NOMBRE    : F_GET_SIDEUDA_ALUMNO
  FECHA     : 12//10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un código de detalle verifique la existencia de deuda en la cuenta 
              corriente del alumno para el periodo correspondiente, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  001   21/10/2016    rmallqui    Se omitio usar el parametro P_PERIODO, se actualizo la obtencion de deuda de una solicitud especifica
  =================================================================================================================== */
              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_FOUND_AMOUNT        NUMBER;
      C_SI_DEUDA            VARCHAR2(5) :='false';
      P_TRAN_NUMBER         TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
BEGIN
--
    
    -- GET numero de transaccion
    SELECT TBRACCD_TRAN_NUMBER INTO P_TRAN_NUMBER 
    FROM TBRACCD 
    WHERE TBRACCD_PIDM = P_ID_ALUMNO 
    AND TBRACCD_DETAIL_CODE = P_CODIGO_DETALLE 
    AND TBRACCD_DESC = 'Activity Fee-SR No. ' || P_NUMERO_SOLICITUD;
    
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(P_ID_ALUMNO,'PEN');
    COMMIT;
    
    -- GET deuda
    SELECT TZRCDAB_AMOUNT
    INTO   P_FOUND_AMOUNT
    FROM   TZRCDAB
    WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
    AND TZRCDAB_PIDM = P_ID_ALUMNO 
    AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE
    AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
   
    IF P_FOUND_AMOUNT > 0 THEN
      C_SI_DEUDA := 'true';
    END IF;
      
    RETURN(C_SI_DEUDA);

END f_get_sideuda_alumno;