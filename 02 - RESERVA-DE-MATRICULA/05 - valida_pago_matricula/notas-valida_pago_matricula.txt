drop procedure p_validar_fecha_solicitud;
GRANT EXECUTE ON WFK_CONTISRM.p_validar_fecha_solicitud TO wfobjects;
GRANT EXECUTE ON WFK_CONTISRM.p_validar_fecha_solicitud TO wfauto;

-- buscar objecto
select dbms_metadata.get_ddl('TRIGGER','TG_SPRIDEN_DESPUES_INSERT') from dual




SELECT * FROM TZRCDAB;
SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 325180;
EXEC  TZKCDAA.p_calc_deuda_alumno(325180,'PEN');


PROBLEMA PROBAR FUNCION
==================================

Se logro probar la funcion metiendola en un package "WFK_CONTISRM" con la siguiente sintaxis:


-- Actualziar las dedudas en el API
		
		#	begin
			    TZKCDAA.p_calc_deuda_alumno(179316,'PEN');
			end;
		#	Session al ejecutar el API USERENV('SESSIONID')

-- consultar deudas con el pidm que se quieres realizar pruebas de la funcion creada
		
		# SELECT * FROM   TZRCDAB WHERE   TZRCDAB_PIDM = 179316;

-- probar la funcion --------------------------------------------------------------------------------
		-- FUNCION
		SET SERVEROUTPUT ON
		DECLARE BOOL BOOLEAN;
		BEGIN
		        BOOL :=  F_GET_SIDEUDA_ALUMNO(325180,'3');
		        DBMS_OUTPUT.PUT_LINE(CASE WHEN BOOL = TRUE THEN 'TRUE' ELSE 'FALSE' END);
		END;

		-- PROCEDURE
		SET SERVEROUTPUT ON
		DECLARE P_DEUDA VARCHAR2(10);
		BEGIN
		        P_GET_SIDEUDA_ALUMNO(325180,'35',P_DEUDA);
		        DBMS_OUTPUT.PUT_LINE(P_DEUDA);
		END;