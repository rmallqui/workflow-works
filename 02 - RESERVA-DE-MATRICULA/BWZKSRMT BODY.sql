/**********************************************************************************************/
/* BWZKSRMT BODY.sql                                                                          */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Reserva de Matrícula.                                      */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        RML             19/ENE/2017 */
/*    --------------------                                                                    */
/*    Creación del paquete de Reserva de Matrícula                                            */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante solicitante                      */
/*              cumpla las condiciones para hacer uso del WF.                                 */
/*    Procedure P_GET_SIDEUDA_ALUMNO: El objetivo es que en base a un ID y un codigo de       */
/*              detalle verifique la existencia de deuda de MATRICULA del alumno en cualquier */
/*              periodo, divisa PEN.                                                          */          
/*    Procedure P_VALIDAR_FECHA_SOLICITUD: se verifiqua que solicitud del alumno del tipo     */
/*              <P_CODIGO_SOLICITUD>( ejem SOL003), se encuentra dentro del rango de fechas   */
/*              habiles para atender la solicitud.                                            */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX).         */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el procedimiento LOS CORREOS      */
/*             deL(os) responsable(s) de Registros Academicos de esa sede.                    */
/*    Procedure P_ELIMINAR_NRCS: Eliminar los NRC's , STUPDY PATH, Informaciob de Ingreso.    */
/*             Vuelve a estimar sus deudas cancelando sus deudas generadas(MONTOS EN NEGATIVO)*/
/*    Procedure P_OBTENER_COMENTARIO_ALUMNO: Obtiene EL COMENTARIO Y TELEFONO de una solicitud*/ 
/*              especifica.                                                                   */
/*    Procedure P_GET_PAGOS: Obtener el monto abonado de matricula y de las cuotas.           */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BODY BWZKSRMT AS

PROCEDURE P_VERIFICAR_APTO (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_APTO     OUT VARCHAR2,
    P_REASON          OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_VERIFICAR_APTO
  FECHA     : 05/07/2018
  AUTOR     : Flores Vilcapoma, Brian Yusef
  OBJETIVO  : El objetivo es que en base a un ID y un codigo de detalle verifique la existencia de 
              deuda de MATRICULA del alumno en cualquier periodo, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  =================================================================================================================== */
AS
        V_CODIGO_SOL        SVRRSRV.SVRRSRV_SRVC_CODE%TYPE := 'SOL005';   
        V_RET01             INTEGER := 0; --VERIFICA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VERIFICA RETENCIÓN 02
        V_SOLTRA            INTEGER := 0; --VERIFICA SOL. TRASLADO INTERNO
        V_SOLPER            INTEGER := 0; --VERIFICA SOL. PERMANENCIA DE PLAN
        V_SOLREC            INTEGER := 0; --VERIFICA SOL. RECTIFICACION DE MATRICULA
        V_SOLCAM            INTEGER := 0; --VERIFICA SOL. CAMBIO DE PLAN
        V_SOLCUO            INTEGER := 0; --VERIFICA SOL. CAMBIO DE CUOTA INICIAL
        V_SOLDIR            INTEGER := 0; --VERIFICA SOL. ASIGNTURA DIRIGIDA
        V_BECA18            INTEGER := 0; --VERIFICA SI ES BECA 18
        V_RESPUESTA         VARCHAR2(4000) := NULL; --RESPUESTA ANIDADA DE LAS VALIDACIONES
        V_FLAG              INTEGER := 0; --BANDERA DE VALIDACIÓN
        P_APEC_TERM         VARCHAR2(4000);
        
        V_CODE_DEPT       STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_TERM       STVTERM.STVTERM_CODE%TYPE;
        V_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
        V_PRIORIDAD       NUMBER;
        V_TERM_ADMI       NUMBER;
        P_PERIODOCTLG     SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
        
        V_INGRESANTE      BOOLEAN := FALSE;
        V_NCURSOS         NUMBER;
        V_FECHA_EXM       DATE;
        V_FECHA_VALIDA    BOOLEAN;
        
        P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
        V_PART_PERIODO        VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        
        -- Calculando parte PERIODO (sub PTRM)
        CURSOR C_SFRRSTS_PTRM IS
            SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
            FROM CZRPTRM 
            WHERE CZRPTRM_DEPT = V_CODE_DEPT
                AND CZRPTRM_CAMP_CODE = V_CODE_CAMP;
        
        -- OBTENER PERIODO por modulo de UVIR y UPGT
        CURSOR C_SFRRSTS IS
            SELECT SFRRSTS_TERM_CODE FROM (
              -- Forma SFARSTS  ---> fechas para las partes de periodo
                SELECT DISTINCT SFRRSTS_TERM_CODE 
                FROM SFRRSTS
                WHERE SFRRSTS_PTRM_CODE IN (V_PART_PERIODO || '1', V_PART_PERIODO || '2')
                    AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                    AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
                ORDER BY SFRRSTS_TERM_CODE
            ) WHERE ROWNUM <= 1;
      
BEGIN
    ---- P_VERIFICAR_APTO PARA SOLICITUD DE RESERVA

    SELECT CZRTERM_TERM_BDUCCI
    INTO P_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE ;-- PERIODO

    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
   
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE CUOTA INICIAL ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCUO
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL007'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCUO > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE BECA 18
    --################################################################################################
        WITH
        CTE_tblPersonaAlumno AS (
            SELECT "IDAlumno" IDAlumno 
            FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDPersona" IN ( 
                SELECT "IDPersona"
                FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE
                WHERE "IDPersonaN" = P_PIDM
                )
        )
        SELECT COUNT(*)
            INTO V_BECA18
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno" IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
            AND "IDPerAcad" = P_APEC_TERM
            AND LENGTH("IDSeccionC") = 5
            AND SUBSTRB("IDSeccionC",-2,2) = '20'
            AND "Estado" = 'M'
            AND "IDConcepto" in ('C01','C02','C03','C04','C05'); -- CUALQUIER CONCEPTO seguro SOLO PARA OBTENER LOS DATOS IDALUMNO Y SECCIONC
    
    IF V_BECA18 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Es estudiante de Beca 18. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Es estudiante de Beca 18. ';
        END IF;
        V_FLAG := 1;
    END IF;

--    -- DATOS ALUMNO
--    BWZKWFFN.P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);
--    ------------------------------------------------------------
--
--    -- >> calculando SUB PARTE PERIODO  --
--    OPEN C_SFRRSTS_PTRM;
--    LOOP
--      FETCH C_SFRRSTS_PTRM INTO V_PART_PERIODO;
--      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
--    END LOOP;
--    CLOSE C_SFRRSTS_PTRM;
--      
--    IF (V_CODE_DEPT = 'UPGT' OR V_CODE_DEPT = 'UVIR') THEN
--          OPEN C_SFRRSTS;
--            LOOP
--              FETCH C_SFRRSTS INTO P_CODE_TERM;
--              IF C_SFRRSTS%FOUND THEN
--                  V_CODE_TERM := P_CODE_TERM;
--              ELSE EXIT;
--            END IF;
--            END LOOP;
--          CLOSE C_SFRRSTS;
--    END IF;
--
--    -- VALIDAR MODALIDAD Y PRIORIDAD
--    BWZKWFFN.P_CHECK_DEPT_PRIORIDAD(P_PIDM, V_CODIGO_SOL, V_CODE_DEPT, V_CODE_TERM, V_CODE_CAMP, V_PRIORIDAD);
--
--    /************************************************************************************************************/
--    -- Segun regla, los que tramitan reserva el mismo periodo que ingresaron 
--    /************************************************************************************************************/
--    IF  (V_CODE_DEPT = 'UREG' AND V_PRIORIDAD = 2) OR     -- se les RESERVA las prioridades "2"
--        (V_CODE_DEPT = 'UPGT' AND V_PRIORIDAD = 4) OR     -- se les RESERVA las prioridades "4" 
--        (V_CODE_DEPT = 'UVIR' AND V_PRIORIDAD = 7)        -- se les RESERVA las prioridades "7"
--        THEN
--        -- VALIDAR QUE EL ALUMNO SEA INGRESANTE EN EL MISMO PERIODO DEL TRAMITE. (PERIODO DE INGRESO = PERIODO ACTUAL)
--        SELECT COUNT(*)
--        INTO V_TERM_ADMI
--        FROM SORLCUR
--        INNER JOIN SORLFOS
--        ON    SORLCUR_PIDM          = SORLFOS_PIDM 
--        AND   SORLCUR_SEQNO         = SORLFOS.SORLFOS_LCUR_SEQNO
--        WHERE SORLCUR_PIDM          =  P_PIDM
--            AND SORLCUR_LMOD_CODE       = 'LEARNER' /*#Estudiante*/ 
--            AND SORLCUR_CACT_CODE       = 'ACTIVE' 
--            AND SORLCUR_CURRENT_CDE     = 'Y'
--            AND SORLCUR_TERM_CODE_ADMIT = P_TERM_CODE -- VALIDACION (PERIODO DE INGRESO = PERIODO ACTUAL)
--            AND SORLFOS_DEPT_CODE       = V_CODE_DEPT;
--    ELSE
--        V_TERM_ADMI := 1;
--    END IF;
--    
--    IF (V_PRIORIDAD > 0 AND V_TERM_ADMI > 0) THEN
--        P_STATUS_APTO := 'TRUE';
--        P_REASON := 'OK';
--        RETURN;
--    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION 
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_SIDEUDA_ALUMNO (
                P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
                P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
                P_DEUDA               OUT VARCHAR2
              )              
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA_ALUMNO
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un codigo de detalle verifique la existencia de 
              deuda de MATRICULA del alumno en cualquier periodo, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  =================================================================================================================== */
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR           NUMBER;
      
      -- APEC PARAMS
      P_SERVICE               VARCHAR2(10);
      P_PART_PERIODO          VARCHAR2(9);
      P_APEC_CAMP             VARCHAR2(9);
      P_APEC_DEPT             VARCHAR2(9);
      P_APEC_TERM             VARCHAR2(10);
      P_APEC_IDSECCIONC       VARCHAR2(15);
      P_APEC_FECINIC          DATE;
      P_APEC_DEUDA            NUMBER;
      P_APEC_IDALUMNO         VARCHAR2(10);
      C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
      C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
      C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
      C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
      C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
      C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
      C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
      C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
      C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
      C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
      
BEGIN
--
    
      --#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          -- #######################################################################
          -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
          SELECT    SORLCUR_SEQNO,      
                    SORLCUR_LEVL_CODE,    
                    SORLCUR_CAMP_CODE,        
                    SORLCUR_COLL_CODE,
                    SORLCUR_DEGC_CODE,  
                    SORLCUR_PROGRAM,      
                    SORLCUR_TERM_CODE_ADMIT,  
                    --SORLCUR_STYP_CODE,
                    SORLCUR_STYP_CODE,  
                    SORLCUR_RATE_CODE,    
                    SORLFOS_DEPT_CODE   
          INTO      C_SEQNO,
                    C_ALUM_NIVEL, 
                    C_ALUM_CAMPUS, 
                    C_ALUM_ESCUELA, 
                    C_ALUM_GRADO, 
                    C_ALUM_PROGRAMA, 
                    C_ALUM_PERD_ADM,
                    C_ALUM_TIPO_ALUM,
                    C_ALUM_TARIFA,
                    C_ALUM_DEPARTAMENTO
          FROM (
                  SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                            SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                            SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
                  FROM SORLCUR        INNER JOIN SORLFOS
                        ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                        AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                      AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                      -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                      AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
          ) WHERE ROWNUM <= 1;
          
          -- GET CRONOGRAMA SECCIONC
            SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
                INTO P_PART_PERIODO
            FROM CZRPTRM 
            WHERE CZRPTRM_DEPT = C_ALUM_DEPARTAMENTO
            AND CZRPTRM_CAMP_CODE = C_ALUM_CAMPUS;

        SELECT CZRCAMP_CAMP_BDUCCI 
            INTO P_APEC_CAMP 
        FROM CZRCAMP 
        WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
        
        SELECT CZRTERM_TERM_BDUCCI 
            INTO P_APEC_TERM
        FROM CZRTERM
        WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
        
        SELECT CZRDEPT_DEPT_BDUCCI
            INTO P_APEC_DEPT
        FROM CZRDEPT
        WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
          
          -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_DEUDA
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = P_APEC_CAMP
                      AND "IDPerAcad" = P_APEC_TERM
                      AND "IDEscuela" = C_ALUM_PROGRAMA
                      AND LENGTH("IDSeccionC") = 5
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO           
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                      )
              )
          SELECT "IDAlumno", "Deuda" INTO P_APEC_IDALUMNO, P_APEC_DEUDA
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
          AND "IDConcepto"  = P_CODIGO_DETALLE;
          
          P_INDICADOR := P_APEC_DEUDA;
          
          COMMIT;
          
    ELSE --**************************** 1 ---> BANNER ***************************
          
          -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
          TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
          COMMIT;
              
          -- GET deuda
          SELECT COUNT(TZRCDAB_AMOUNT)
          INTO   P_INDICADOR
          FROM   TZRCDAB
          WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
          AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
          AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE;         -- CODIGO MATRICULA 'C00'
          --AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
    
    END IF;
   
    IF P_INDICADOR > 0 THEN
      P_DEUDA   := 'TRUE';
    ELSE
      P_DEUDA   :='FALSE';
    END IF;

END P_GET_SIDEUDA_ALUMNO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
  P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_FECHA_VALIDA        OUT VARCHAR2
  )
/* ===================================================================================================================
  NOMBRE    : p_validar_fecha_solicitud
  FECHA     : 25/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : se verifiqua que solicitud del alumno del tipo <P_CODIGO_SOLICITUD>( ejem SOL003), 
              se encuentra dentro del rango de fechas habiles para atender la solicitud.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
       
BEGIN
--
----------------------------------------------------------------------------
        SELECT 
                DECODE(COUNT(*),0,'FALSE','TRUE') 
        INTO P_FECHA_VALIDA 
        FROM SVRSVPR 
        INNER JOIN (
              -- get la configuracion (fechas, estados, etc) de la solicitud P_CODIGO_SOLICITUD 
               SELECT SVRRSRV_SEQ_NO          TEMP_PRIORIDAD, 
                      SVRRSRV_SRVC_CODE       TEMP_SRVC_CODE, 
                      SVRRSRV_INACTIVE_IND,
                      SVRRSST_RSRV_SEQ_NO , 
                      SVRRSST_SRVC_CODE , 
                      SVRRSST_SRVS_CODE       TEMP_SRVS_CODE,
                      SVRRSRV_START_DATE      TEMP_START_DATE,--- fecha finalizacion
                      SVRRSRV_END_DATE        TEMP_END_DATE,----- fecha finalizacion
                      SVRRSRV_DELIVERY_DATE , -- fecha estimada
                      SVVSRVS_LOCKED
               FROM SVRRSRV ------------------------------------- configuracion total de reglas
               INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
               ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
               AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
               INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
               ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
               WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD 
               AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activo
               -- AND SVRRSRV_WEB_IND = 'Y' ------------------------ Si esta disponible en WEB
        )TEMP
        ON SVRSVPR_SRVS_CODE        = TEMP_SRVS_CODE
        AND SVRSVPR_SRVC_CODE       =  TEMP_SRVC_CODE
        WHERE TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(TEMP_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND (TEMP_END_DATE + 1)
        AND SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
        AND TEMP_PRIORIDAD          IN (SELECT SVRSVPR_RSRV_SEQ_NO FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD);

      
    ----------------------------------------------------------------------------
--
EXCEPTION
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VALIDAR_FECHA_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_SOL          OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VERIFICAR_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_STATUS_SOL, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_NUMERO_SOLICITUD, P_ESTADO, P_AN, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_USUARIO (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO              OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Academicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
      P_Email_Address           VARCHAR2(100);
    
      P_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO P_Email_Address FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_CORREO := P_CORREO || P_Email_Address || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      P_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_CORREO,1,LENGTH(P_CORREO) -1) INTO P_CORREO
      FROM DUAL
      WHERE SUBSTR(P_CORREO,-1,1) = ',';
      
EXCEPTION
WHEN TOO_MANY_ROWS THEN 
  IF ( P_SECCION_EXCEPT = 'ROLES') THEN
      P_ERROR := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
  ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
      P_ERROR := '- Se encontraron mas de una ORGANIZACIÓN con el mismo nombre.';
  ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
      P_ERROR := '- Se encontraron mas de un usuario con el mismo ROL.';
  ELSE  P_ERROR := SQLERRM;
  END IF; 
  RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN NO_DATA_FOUND THEN
  IF ( P_SECCION_EXCEPT = 'ROLES') THEN
      P_ERROR := '- NO se encontró el nombre del ROL: ' || P_ROL || P_COD_SEDE;
  ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
      P_ERROR := '- NO se encontró el nombre de la ORGANIZACION.';
  ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
      P_ERROR := '- NO  se encontró ningun usuario con esas caracteristicas.';
  ELSE  P_ERROR := SQLERRM;
  END IF; 
  RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_ELIMINAR_NRCS ( 
      P_PIDM_ALUMNO             IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_CODE_DEPT               IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CODE_CAMP               IN STVCAMP.STVCAMP_CODE%TYPE,
      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_ERROR                   OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_ELIMINAR_NRCS
  FECHA     : 28/11/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Eliminar los NRC's , STUPDY PATH, Informaciob de Ingreso. Vuelve a estimar sus deudas cancelando 
              sus deudas generadas (MONTOS EN NEGATIVO)                

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   27/04/2016    RMALLQUI      Se adapto el proced. para reservas realizadas en el el 2do Modulo de UVIR y UPGT
  002   15/09/2017    RMALLQUI      Se agrearon 14 dias sobre la fecha final de modulo(UVIR/UPGT) para la ejecucion.
  =================================================================================================================== */
AS
        P_KEY_SEQNO                 SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
        SAVE_ACT_DATE_OUT           VARCHAR2(100);
        RETURN_STATUS_IN_OUT        NUMBER;
        P_INDICADOR                 NUMBER;
        V_SGRSTSP_REC               SGRSTSP%ROWTYPE;
        V_SGBSTDN_REC               SGBSTDN%ROWTYPE;
        
        V_NUM_NRC                   INTEGER := 0;
        
        --- ADD SGRSTSP ->  EN CASO NO EXISTA PARA EL PERIODO INDICADO
              -- cambiando el estado del plan ( FORMA SFAREGS --> plan estudio)
        CURSOR C_SGRSTSP IS
        SELECT * FROM (
            SELECT *
            FROM SGRSTSP WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
            AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO
            ORDER BY SGRSTSP_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            -- validar QUE ya existe el REGISTRO
            SELECT *
            FROM SGRSTSP WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
            AND SGRSTSP_TERM_CODE_EFF = P_PERIODO
            AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO
         );
         
        -- ADD SGBSTDN - EN CASO NO EXISTA PARA EL PERIODO INDICADO
        CURSOR C_SGBSTDN IS
        SELECT * FROM (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
            AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
         );

        V_FECHA_SOL           SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
        V_MODULO              INTEGER;
        V_PART_PERIODO        VARCHAR2(9);
BEGIN
      
        SELECT COUNT(*)
        INTO V_NUM_NRC
        FROM SFRSTCR
        WHERE SFRSTCR_PIDM = P_PIDM_ALUMNO
        AND SFRSTCR_TERM_CODE = P_PERIODO;
        
        IF V_NUM_NRC > 0 THEN
      
            -- #######################################################################          
            -- Determinar el modulo(grupo) - PARA UVIR, UPGT en caso esten en el 2do MODULO 
            -- #######################################################################  
            IF P_CODE_DEPT = 'UPGT' OR P_CODE_DEPT = 'UVIR'  THEN
                -- GET parte PERIODO
                SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
                    INTO V_PART_PERIODO
                FROM CZRPTRM 
                WHERE CZRPTRM_DEPT = P_CODE_DEPT
                AND CZRPTRM_CAMP_CODE = P_CODE_CAMP;

                -- GET FECHA de la solicitud 
                SELECT SVRSVPR_RECEPTION_DATE
                INTO V_FECHA_SOL
                FROM SVRSVPR 
                WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
            
                -- Forma SFARSTS  ---> fechas para las partes de periodo
                SELECT MODULO INTO V_MODULO FROM(
                    SELECT CASE WHEN SFRRSTS_PTRM_CODE = V_PART_PERIODO|| '1' THEN 1 WHEN  SFRRSTS_PTRM_CODE = V_PART_PERIODO || '2' THEN 2 END MODULO 
                    FROM SFRRSTS
                    WHERE SFRRSTS_PTRM_CODE IN (V_PART_PERIODO || '1', V_PART_PERIODO || '2')
                        AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                        AND TO_DATE(TO_CHAR(V_FECHA_SOL,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND (SFRRSTS_END_DATE + 14)
                        AND SFRRSTS_TERM_CODE = P_PERIODO
                    ORDER BY 1 ASC
                ) WHERE ROWNUM = 1; 
            
                    /******************************************************************************
                    -- Desmatricular y Estimar deuda (BANNER)
                    *******************************************************************************/
            --                UPDATE SFRSTCR 
            --                SET SFRSTCR_RSTS_CODE = 'DD',           SFRSTCR_RSTS_DATE = SYSDATE,
            --                    SFRSTCR_ERROR_FLAG = 'D',           SFRSTCR_BILL_HR = 0,
            --                    SFRSTCR_WAIV_HR = 0,                SFRSTCR_CREDIT_HR = 0,
            --                    SFRSTCR_ACTIVITY_DATE = SYSDATE,    SFRSTCR_USER = 'WorkFlow',
            --                    SFRSTCR_ASSESS_ACTIVITY_DATE = SYSDATE      
            --                WHERE SFRSTCR_TERM_CODE = P_PERIODO
            --                AND SFRSTCR_PIDM = P_PIDM_ALUMNO
            --                AND SFRSTCR_RSTS_CODE IN  ('RE','RW')  -- Estados validos (SFQRSTS)
            --                AND SFRSTCR_PTRM_CODE = V_PART_PERIODO || '2'
            --                AND V_MODULO = 2;
            
                    DELETE SFRSTCR 
                    WHERE SFRSTCR_TERM_CODE = P_PERIODO
                    AND SFRSTCR_PIDM = P_PIDM_ALUMNO
                    AND SFRSTCR_RSTS_CODE IN  ('RE','RW')  -- Estados validos (SFQRSTS)
                    AND SFRSTCR_PTRM_CODE = V_PART_PERIODO || '2'
                    AND V_MODULO = 2;
                    
                    COMMIT;
            ELSE
                V_MODULO := 1;
            END IF;           
            
            
            -- #######################################################################          
            -- VALIDAR - "estimaciones IN-LINE"
            -- #######################################################################          
            
            
            -- #######################################################################          
            -- Eliminar registros de CRN'S
            -- SFASLST ::: Student Course Registration Repeating Table 
            DELETE FROM SFRSTCR 
            WHERE SFRSTCR_PIDM = P_PIDM_ALUMNO
            AND SFRSTCR_TERM_CODE = P_PERIODO
            AND V_MODULO = 1; 
            
            
            -- #######################################################################
            -- AMORTIGUAR DEUDA
            SFKFEES.P_PROCESS_ETRM_DROP(  P_PIDM_ALUMNO, 
                                        P_PERIODO, 
                                        SYSDATE );
            
            
            -- #######################################################################
            -- Procesar DEUDA
            SFKFEES.P_PROCESSFEEASSESSMENT(   P_PERIODO,
                                            P_PIDM_ALUMNO,
                                            NULL,
                                            SYSDATE,
                                            'R',
                                            'N',                  -- create TBRACCD records
                                            'SFAREGS',    
                                            'Y',                  -- commit changes
                                            SAVE_ACT_DATE_OUT,
                                            'N',
                                            RETURN_STATUS_IN_OUT); 
                                            
            
            -- #######################################################################
            -- ELIMINAR - STUDY PATH -> SFAREGS 
            DELETE FROM SFRENSP 
            WHERE SFRENSP_PIDM = P_PIDM_ALUMNO
            AND SFRENSP_TERM_CODE = P_PERIODO
            AND V_MODULO = 1;
            
            
            -- #######################################################################
            -- ELIMINAR - INFORMACION DE INGRESO -> SFAREGS  ||  registro que determina al alumno Elegible 
            DELETE FROM SFBETRM 
            WHERE SFBETRM_PIDM = P_PIDM_ALUMNO
            AND SFBETRM_TERM_CODE = P_PERIODO
            AND V_MODULO = 1; 
            
            
            -- #######################################################################
            -- CAMBIO ESTADO PLAN ESTUDIO ::::  
            -- #######################################################################  
            SELECT    SORLCUR_KEY_SEQNO 
            INTO      P_KEY_SEQNO -- P_KEY_SEQNO -> se obtiene de SORLCUR_KEY_SEQNO
            FROM (
                    SELECT    SORLCUR_SEQNO,      SORLCUR_PROGRAM,      SORLCUR_TERM_CODE,
                              SORLFOS_DEPT_CODE,  SORLCUR_KEY_SEQNO
                    FROM SORLCUR        INNER JOIN SORLFOS
                          ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                          AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                    WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                        AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                        -- AND SORLCUR_TERM_CODE   =   P_TERM_PERIODO 
                        AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                        AND SORLCUR_CURRENT_CDE = 'Y'
                    ORDER BY SORLCUR_SEQNO DESC 
            ) WHERE ROWNUM <= 1;
            
            --- cambio de plan FORMA SFAREGS --> plan estudio
            OPEN C_SGRSTSP;
            LOOP
                FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
                EXIT WHEN C_SGRSTSP%NOTFOUND;
                  
                  INSERT INTO SGRSTSP (
                            SGRSTSP_PIDM,                 SGRSTSP_TERM_CODE_EFF,          SGRSTSP_KEY_SEQNO,
                            SGRSTSP_STSP_CODE,            SGRSTSP_ACTIVITY_DATE,          SGRSTSP_DATA_ORIGIN,
                            SGRSTSP_USER_ID,              SGRSTSP_FULL_PART_IND,          SGRSTSP_SESS_CODE,
                            SGRSTSP_RESD_CODE,            SGRSTSP_ORSN_CODE,              SGRSTSP_PRAC_CODE,
                            SGRSTSP_CAPL_CODE,            SGRSTSP_EDLV_CODE,              SGRSTSP_INCM_CODE,
                            SGRSTSP_EMEX_CODE,            SGRSTSP_APRN_CODE,              SGRSTSP_TRCN_CODE,
                            SGRSTSP_GAIN_CODE,            SGRSTSP_VOED_CODE,              SGRSTSP_BLCK_CODE,
                            SGRSTSP_EGOL_CODE,            SGRSTSP_BSKL_CODE,              SGRSTSP_ASTD_CODE,
                            SGRSTSP_PREV_CODE,            SGRSTSP_CAST_CODE  ) 
                  VALUES (  V_SGRSTSP_REC.SGRSTSP_PIDM,          P_PERIODO,                               P_KEY_SEQNO,
                            'RV',/*STVSTSP: 'RV'-Reserva*/       SYSDATE,                                 'WorkFlow',
                            USER,                                V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,     V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                            V_SGRSTSP_REC.SGRSTSP_RESD_CODE,     V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,         V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                            V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,     V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,         V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                            V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,     V_SGRSTSP_REC.SGRSTSP_APRN_CODE,         V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                            V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,     V_SGRSTSP_REC.SGRSTSP_VOED_CODE,         V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                            V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,     V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,         V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                            V_SGRSTSP_REC.SGRSTSP_PREV_CODE,     V_SGRSTSP_REC.SGRSTSP_CAST_CODE  );
                  
            END LOOP;
            CLOSE C_SGRSTSP;
            
            -- PARA EL CASO QUE YA EXISTA EL PLAN PARA EL PERIODO -> Actualizar el estado
            UPDATE SGRSTSP 
                SET SGRSTSP_STSP_CODE = 'RV' ------------------ ESTADOS STVSTSP : 'RV' - Reserva
            WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO 
            AND SGRSTSP_TERM_CODE_EFF = P_PERIODO 
            AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO
            AND SGRSTSP_STSP_CODE <> 'RV'; 
            
            
            -- #######################################################################
            -- CAMBIO ESTADO DEL ALUMNO PARA DICHO PERIODO 
            -- ####################################################################### 
            OPEN C_SGBSTDN;
            LOOP
                FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
                EXIT WHEN C_SGBSTDN%NOTFOUND;
                
                  INSERT INTO SGBSTDN (
                            SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                            SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                            SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                            SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                            SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                            SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                            SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                            SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                            SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                            SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                            SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                            SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                            SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                            SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                            SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                            SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                            SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                            SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                            SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                            SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                            SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                            SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                            SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                            SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                            SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                            SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                            SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                            SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                            SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                            SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                            SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                            SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                            SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                            SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                            SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                            SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                            SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
                  VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_PERIODO,                                    'IS',/*STVSTST: 'IS'-Inactivo*/
                            V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            V_SGBSTDN_REC.SGBSTDN_STYP_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                            V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                            V_SGBSTDN_REC.SGBSTDN_COLL_CODE_1,          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_1,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                            V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                            V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                            V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                            V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                            SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                            V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                            V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                            V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                            V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                            V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                            V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                            V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        V_SGBSTDN_REC.SGBSTDN_PROGRAM_1,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_1,
                            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                            V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                            V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                            V_SGBSTDN_REC.SGBSTDN_CURR_RULE_1,          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_1,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                            V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                            V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                            V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                            V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                            USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
                  
            END LOOP;
            CLOSE C_SGBSTDN;
            
            -- PARA EL CASO QUE YA EXISTA EL PLAN PARA EL PERIODO -> Actualizar el estado
            UPDATE SGBSTDN
                SET SGBSTDN_STST_CODE = 'IS'  ------------------ ESTADOS STVSTST : 'IS' - Inactivo
            WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
            AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
            AND SGBSTDN_STST_CODE <> 'IS';
            
            
            --
            COMMIT; 
        END IF;
EXCEPTION
    WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_ELIMINAR_NRCS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO_ALUMNO       OUT VARCHAR2,
      P_TELEFONO_ALUMNO         OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_COMENTARIO_ALUMNO
  FECHA     : 12/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Para Solicitudes 'SOL005' : 
              Obtiene EL COMENTARIO Y TELEFONO de una solicitud especifica. 
              Se establecio que el penultimo dato es el COMENTARIO y ultimo el TELEFONO.

  NRO     FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
AS
      V_ERROR                   EXCEPTION;
      V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_COMENTARIO              VARCHAR2(4000);
      V_CLAVE                   VARCHAR2(4000);
      V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
      
      -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
      CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
        --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
        
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO V_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 1 THEN
                  -- OBTENER COMENTARIO DE LA SOLICITUD
                  P_COMENTARIO_ALUMNO := V_COMENTARIO;
              ELSIF V_ADDL_DATA_SEQ = 2 THEN
                  -- OBTENER EL TELEFONO DE LA SOLICITUD 
                  P_TELEFONO_ALUMNO := V_COMENTARIO;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;

      IF (P_TELEFONO_ALUMNO IS NULL OR P_COMENTARIO_ALUMNO IS NULL) THEN
          RAISE V_ERROR;
      END IF;
      
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontro el comentario y/o el telefono.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_OBTENER_COMENTARIO_ALUMNO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_PAGOS (
                P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
                P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                P_ABONO_MATRIC        OUT NUMBER,
                P_ABONO_CUOTAS        OUT NUMBER
              )              
/* ===================================================================================================================
  NOMBRE    : P_GET_PAGOS
  FECHA     : 27/02/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtener el monto abonado de matricula y de las cuotas.

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION  
  001   27/04/2016    RMALLQUI      Se adapto el proced. para reservas realizadas en el 2do Modulo de UVIR y UPGT (RETORNA SOLO LA 3ra Y 4ta CUOTA)
  002   15/09/2017    RMALLQUI      Se agrearon 14 dias sobre la fecha final de modulo(UVIR/UPGT) para la ejecucion.
  =================================================================================================================== */
AS
      -- PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR             NUMBER;
      P_COD_MATRIC            TBRACCD.TBRACCD_DETAIL_CODE%TYPE := 'C00';
      P_COD_CUOTAS            TBRACCD.TBRACCD_DETAIL_CODE%TYPE := 'C0%';
      P_COD_RECONOC           TBRACCD.TBRACCD_DETAIL_CODE%TYPE := 'RFND'; -- Cod Reconociemiento
      
      -- APEC PARAMS
      P_SERVICE               VARCHAR2(10);
      P_PART_PERIODO          VARCHAR2(9);
      P_APEC_CAMP             VARCHAR2(9);
      P_APEC_DEPT             VARCHAR2(9);
      P_APEC_TERM             VARCHAR2(10);
      P_APEC_IDSECCIONC       VARCHAR2(15);
      P_APEC_FECINIC          DATE;
      P_APEC_DEUDA            NUMBER;
      P_APEC_IDALUMNO         VARCHAR2(10);
      C_SEQNO                   SORLCUR.SORLCUR_SEQNO%type;
      C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
      C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
      C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
      C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
      C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
      C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
      C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
      C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
      C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
      
      V_FECHA_SOL           SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
      V_MODULO              INTEGER;
BEGIN
--
      P_ABONO_MATRIC := 0;
      P_ABONO_CUOTAS := 0;
      --#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          -- #######################################################################
          -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
          SELECT    SORLCUR_SEQNO,      
                    SORLCUR_LEVL_CODE,    
                    SORLCUR_CAMP_CODE,        
                    SORLCUR_COLL_CODE,
                    SORLCUR_DEGC_CODE,  
                    SORLCUR_PROGRAM,      
                    SORLCUR_TERM_CODE_ADMIT,  
                    SORLCUR_STYP_CODE,  
                    SORLCUR_RATE_CODE,    
                    SORLFOS_DEPT_CODE   
          INTO      C_SEQNO,
                    C_ALUM_NIVEL, 
                    C_ALUM_CAMPUS, 
                    C_ALUM_ESCUELA, 
                    C_ALUM_GRADO, 
                    C_ALUM_PROGRAMA, 
                    C_ALUM_PERD_ADM,
                    C_ALUM_TIPO_ALUM,
                    C_ALUM_TARIFA,
                    C_ALUM_DEPARTAMENTO
          FROM (
                  SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                            SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                            SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
                  FROM SORLCUR        INNER JOIN SORLFOS
                        ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                        AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                      AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                      -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                      AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_SEQNO DESC 
          ) WHERE ROWNUM <= 1;
          
          -- GET CRONOGRAMA SECCIONC
            SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
            INTO P_PART_PERIODO
            FROM CZRPTRM 
            WHERE CZRPTRM_DEPT = C_ALUM_DEPARTAMENTO
            AND CZRPTRM_CAMP_CODE = C_ALUM_CAMPUS;

          -- #######################################################################          
          -- Determinar el modulo(grupo) - PARA UVIR, UPGT en caso esten en el 2do MODULO 
          -- #######################################################################  
          IF C_ALUM_DEPARTAMENTO = 'UPGT' OR C_ALUM_DEPARTAMENTO = 'UVIR'  THEN

                  -- GET FECHA de la solicitud 
                  SELECT  SVRSVPR_RECEPTION_DATE INTO V_FECHA_SOL
                  FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;

                  -- Forma SFARSTS  ---> fechas para las partes de periodo
                  SELECT MODULO INTO V_MODULO FROM(
                      SELECT CASE WHEN SFRRSTS_PTRM_CODE = (P_PART_PERIODO || '1') THEN 1 WHEN  SFRRSTS_PTRM_CODE = (P_PART_PERIODO || '2') THEN 2 END MODULO
                      FROM SFRRSTS
                      WHERE SFRRSTS_PTRM_CODE IN (P_PART_PERIODO || '1', P_PART_PERIODO || '2')
                        AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                        AND TO_DATE(TO_CHAR(V_FECHA_SOL,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND (SFRRSTS_END_DATE + 14)
                        AND SFRRSTS_TERM_CODE = P_PERIODO
                      ORDER BY 1 ASC
                  ) WHERE ROWNUM = 1;    

          ELSE
                
                V_MODULO := 1;
          
          END IF;                
          
          SELECT CZRCAMP_CAMP_BDUCCI
            INTO P_APEC_CAMP 
          FROM CZRCAMP 
          WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
          
          SELECT CZRTERM_TERM_BDUCCI
            INTO P_APEC_TERM 
          FROM CZRTERM 
          WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
          
          SELECT CZRDEPT_DEPT_BDUCCI
            INTO P_APEC_DEPT 
          FROM CZRDEPT 
          WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
          
          -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_DEUDA
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = P_APEC_CAMP
                      AND "IDPerAcad" = P_APEC_TERM
                      AND "IDEscuela" = C_ALUM_PROGRAMA
                      AND LENGTH("IDSeccionC") = 5
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO           
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                      )
              )
          SELECT "IDAlumno", "IDSeccionC", "Abono" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC, P_ABONO_MATRIC
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
          AND "IDConcepto"  = P_COD_MATRIC;
          
          -- calculando MONTOS tanto para regular(UREG) como para  segundo modulo (UVIR,UPGT)
          IF V_MODULO = 2 THEN

              P_ABONO_MATRIC := 0;
              -- GET MONTO 
              SELECT SUM("Abono") INTO P_ABONO_CUOTAS
              FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
              WHERE "IDDependencia" = 'UCCI'
              AND "IDAlumno"    = P_APEC_IDALUMNO
              AND "IDSede"      = P_APEC_CAMP
              AND "IDPerAcad"   = P_APEC_TERM
              AND "IDEscuela"   = C_ALUM_PROGRAMA
              AND "IDSeccionC"  = P_APEC_IDSECCIONC
              AND "IDConcepto"  IN ('C03','C04') -- CUOTAS USADAS SOLO PARA UVIR,UPGT
              AND "IDConcepto"  <> P_COD_MATRIC;
          
          ELSE 
          
              SELECT SUM("Abono") INTO P_ABONO_CUOTAS
              FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
              WHERE "IDDependencia" = 'UCCI'
              AND "IDAlumno"    = P_APEC_IDALUMNO
              AND "IDSede"      = P_APEC_CAMP
              AND "IDPerAcad"   = P_APEC_TERM
              AND "IDEscuela"   = C_ALUM_PROGRAMA
              AND "IDSeccionC"  = P_APEC_IDSECCIONC
              AND "IDConcepto"  LIKE P_COD_CUOTAS
              AND "IDConcepto"  <> P_COD_MATRIC;

              SELECT "Abono" INTO P_ABONO_MATRIC
              FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
              WHERE "IDDependencia" = 'UCCI'
              AND "IDAlumno"    = P_APEC_IDALUMNO
              AND "IDSede"      = P_APEC_CAMP
              AND "IDPerAcad"   = P_APEC_TERM
              AND "IDEscuela"   = C_ALUM_PROGRAMA
              AND "IDSeccionC"  = P_APEC_IDSECCIONC
              AND "IDConcepto"  = P_COD_MATRIC;
          
          END IF;

    ELSE --**************************** 1 ---> BANNER ***************************
          
          -- GET RECONOCIEMIENTO - COD DETAIL "RFND"
          SELECT COUNT(TBRACCD_AMOUNT) 
          INTO P_INDICADOR
          FROM (
              SELECT TBRACCD_AMOUNT FROM TBRACCD 
              WHERE TBRACCD_PIDM = P_PIDM_ALUMNO  
              AND TBRACCD_DETAIL_CODE = P_COD_RECONOC
              AND TBRACCD_TERM_CODE = P_PERIODO
              -- ORDER BY TBRACCD_TERM_CODE DESC
          ) WHERE ROWNUM = 1;
          
          IF P_INDICADOR = 0 THEN
                P_ABONO_MATRIC := 0;
                P_ABONO_CUOTAS := 0;
          ELSE
                SELECT  TBRACCD_AMOUNT
                INTO    P_ABONO_MATRIC
                FROM (
                        SELECT TBRACCD_AMOUNT 
                        FROM  TBRACCD 
                        WHERE TBRACCD_PIDM = P_PIDM_ALUMNO  
                        AND TBRACCD_DETAIL_CODE = P_COD_RECONOC
                        AND TBRACCD_TERM_CODE = P_PERIODO
                        -- ORDER BY TBRACCD_TERM_CODE DESC
                ) WHERE ROWNUM = 1;
          END IF;
    
    END IF;

END P_GET_PAGOS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSRMT;

/**********************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**********************************************************************************************/