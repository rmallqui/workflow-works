
/*
SET SERVEROUTPUT ON
DECLARE   P_ID_ALUMNO             SPRIDEN.SPRIDEN_PIDM%TYPE             := 284082 ;
          P_PERIODO               SFBETRM.SFBETRM_TERM_CODE%TYPE        := '201620';
          P_DEUDA                 VARCHAR2(5);
BEGIN
    P_VALIDAR_SIHORARIO(284082,'201620',P_DEUDA);
    DBMS_OUTPUT.PUT_LINE(P_DEUDA);
END;
*/

CREATE OR REPLACE PROCEDURE P_VALIDAR_SIHORARIO (
          P_ID_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
          P_PERIODO           IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
          P_HORARIO           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_VALIDAR_HORARIO
  FECHA     : 14/11/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Retornar TRUE/FALSE si alumno tiene NRCs (Horario)

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
            P_INDICADOR             NUMBER;
BEGIN
-- 
    -- COUNT alumno NRCs
    SELECT COUNT(*) INTO P_INDICADOR
    FROM SFRSTCR 
    WHERE SFRSTCR_PIDM = P_ID_ALUMNO
    AND SFRSTCR_TERM_CODE = P_PERIODO;
    
    IF P_INDICADOR > 0 THEN
      P_HORARIO   := 'TRUE';
    ELSE
      P_HORARIO   :='FALSE';
    END IF;

END P_VALIDAR_SIHORARIO;