/*
SET SERVEROUTPUT ON;
DECLARE P_ABONO_MATRIC        NUMBER;
        P_ABONO_CUOTAS        NUMBER;
BEGIN
    P_GET_PAGOS(342961,'201710',4160,P_ABONO_MATRIC,P_ABONO_CUOTAS);
    DBMS_OUTPUT.PUT_LINE(P_ABONO_MATRIC || ' -- ' || P_ABONO_CUOTAS);
END;
*/

CREATE OR REPLACE PROCEDURE P_GET_PAGOS (
                P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
                P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                P_ABONO_MATRIC        OUT NUMBER,
                P_ABONO_CUOTAS        OUT NUMBER
              )              
/* ===================================================================================================================
  NOMBRE    : P_GET_PAGOS
  FECHA     : 27/02/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtener el monto abonado de matricula y de las cuotas.

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION  
  001   27/04/2016    RMALLQUI      Se adapto el proced. para reservas realizadas en el 2° Modulo de UVIR y UPGT (RETORNA SOLO LA 3° Y 4° CUOTA)
  =================================================================================================================== */
AS
      -- PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR             NUMBER;
      P_COD_MATRIC            TBRACCD.TBRACCD_DETAIL_CODE%TYPE := 'C00';
      P_COD_CUOTAS            TBRACCD.TBRACCD_DETAIL_CODE%TYPE := 'C0%';
      P_COD_RECONOC           TBRACCD.TBRACCD_DETAIL_CODE%TYPE := 'RFND'; -- Cod Reconociemiento
      
      -- APEC PARAMS
      P_SERVICE               VARCHAR2(10);
      P_PART_PERIODO          VARCHAR2(9);
      P_APEC_CAMP             VARCHAR2(9);
      P_APEC_DEPT             VARCHAR2(9);
      P_APEC_TERM             VARCHAR2(10);
      P_APEC_IDSECCIONC       VARCHAR2(15);
      P_APEC_FECINIC          DATE;
      P_APEC_DEUDA            NUMBER;
      P_APEC_IDALUMNO         VARCHAR2(10);
      C_SEQNO                   SORLCUR.SORLCUR_SEQNO%type;
      C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
      C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
      C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
      C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
      C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
      C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
      C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
      C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
      C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
      
      V_FECHA_SOL           SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
      V_MODULO              INTEGER;
BEGIN
--
      P_ABONO_MATRIC := 0;
      P_ABONO_CUOTAS := 0;
      --#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          -- #######################################################################
          -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
          SELECT    SORLCUR_SEQNO,      
                    SORLCUR_LEVL_CODE,    
                    SORLCUR_CAMP_CODE,        
                    SORLCUR_COLL_CODE,
                    SORLCUR_DEGC_CODE,  
                    SORLCUR_PROGRAM,      
                    SORLCUR_TERM_CODE_ADMIT,  
                    SORLCUR_STYP_CODE,  
                    SORLCUR_RATE_CODE,    
                    SORLFOS_DEPT_CODE   
          INTO      C_SEQNO,
                    C_ALUM_NIVEL, 
                    C_ALUM_CAMPUS, 
                    C_ALUM_ESCUELA, 
                    C_ALUM_GRADO, 
                    C_ALUM_PROGRAMA, 
                    C_ALUM_PERD_ADM,
                    C_ALUM_TIPO_ALUM,
                    C_ALUM_TARIFA,
                    C_ALUM_DEPARTAMENTO
          FROM (
                  SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                            SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                            SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
                  FROM SORLCUR        INNER JOIN SORLFOS
                        ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                        AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                      AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                      -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                      AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_SEQNO DESC 
          ) WHERE ROWNUM <= 1;
          
          -- GET CRONOGRAMA SECCIONC
          SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                    WHEN 'UPGT' THEN 'W' 
                                    WHEN 'UREG' THEN 'R' 
                                    WHEN 'UPOS' THEN '-' 
                                    WHEN 'ITEC' THEN '-' 
                                    WHEN 'UCIC' THEN '-' 
                                    WHEN 'UCEC' THEN '-' 
                                    WHEN 'ICEC' THEN '-' 
                                    ELSE '1' END ||
                  CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                    WHEN 'F01' THEN 'A' 
                                    WHEN 'F02' THEN 'L' 
                                    WHEN 'F03' THEN 'C' 
                                    WHEN 'V00' THEN 'V' 
                                    ELSE '9' END
          INTO P_PART_PERIODO
          FROM STVCAMP,STVDEPT 
          WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;



          -- #######################################################################          
          -- Determinar el modulo(grupo) - PARA UVIR, UPGT en caso esten en el 2° MODULO 
          -- #######################################################################  
          IF C_ALUM_DEPARTAMENTO = 'UPGT' OR C_ALUM_DEPARTAMENTO = 'UVIR'  THEN

                  -- GET FECHA de la solicitud 
                  SELECT  SVRSVPR_RECEPTION_DATE INTO V_FECHA_SOL
                  FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;

                  -- Forma SFARSTS  ---> fechas para las partes de periodo
                  SELECT CASE WHEN SFRRSTS_PTRM_CODE = (P_PART_PERIODO || '1') THEN 1 WHEN  SFRRSTS_PTRM_CODE = (P_PART_PERIODO || '2') THEN 2 END MODULO INTO V_MODULO
                  FROM SFRRSTS
                  WHERE SFRRSTS_PTRM_CODE IN (P_PART_PERIODO || '1', P_PART_PERIODO || '2')
                  AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                  AND TO_DATE(TO_CHAR(V_FECHA_SOL,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
                  AND SFRRSTS_TERM_CODE = P_PERIODO; 
          ELSE
                
                V_MODULO := 1;
          
          END IF;                
          


          SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
          SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
          SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
          
          -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_DEUDA
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = P_APEC_CAMP
                      AND "IDPerAcad" = P_APEC_TERM
                      AND "IDEscuela" = C_ALUM_PROGRAMA
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                      AND "IDSeccionC" <> '15NEX1A'
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO           
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                      )
              )
          SELECT "IDAlumno", "IDSeccionC", "Abono" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC, P_ABONO_MATRIC
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
          AND "IDConcepto"  = P_COD_MATRIC;
          
          -- calculando MONTOS tanto para regular(UREG) como para  segundo modulo (UVIR,UPGT)
          IF V_MODULO = 2 THEN

              P_ABONO_MATRIC := 0;
              -- GET MONTO 
              SELECT SUM("Abono") INTO P_ABONO_CUOTAS
              FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
              WHERE "IDDependencia" = 'UCCI'
              AND "IDAlumno"    = P_APEC_IDALUMNO
              AND "IDSede"      = P_APEC_CAMP
              AND "IDPerAcad"   = P_APEC_TERM
              AND "IDEscuela"   = C_ALUM_PROGRAMA
              AND "IDSeccionC"  = P_APEC_IDSECCIONC
              AND "IDConcepto"  IN ('C03','C04') -- CUOTAS USADAS SOLO PARA UVIR,UPGT
              AND "IDConcepto"  <> P_COD_MATRIC;
          
          ELSE 
          
              SELECT SUM("Abono") INTO P_ABONO_CUOTAS
              FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
              WHERE "IDDependencia" = 'UCCI'
              AND "IDAlumno"    = P_APEC_IDALUMNO
              AND "IDSede"      = P_APEC_CAMP
              AND "IDPerAcad"   = P_APEC_TERM
              AND "IDEscuela"   = C_ALUM_PROGRAMA
              AND "IDSeccionC"  = P_APEC_IDSECCIONC
              AND "IDConcepto"  LIKE P_COD_CUOTAS
              AND "IDConcepto"  <> P_COD_MATRIC;

              SELECT "Abono" INTO P_ABONO_MATRIC
              FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
              WHERE "IDDependencia" = 'UCCI'
              AND "IDAlumno"    = P_APEC_IDALUMNO
              AND "IDSede"      = P_APEC_CAMP
              AND "IDPerAcad"   = P_APEC_TERM
              AND "IDEscuela"   = C_ALUM_PROGRAMA
              AND "IDSeccionC"  = P_APEC_IDSECCIONC
              AND "IDConcepto"  = P_COD_MATRIC;
          
          END IF;

    ELSE --**************************** 1 ---> BANNER ***************************
          
          -- GET RECONOCIEMIENTO - COD DETAIL "RFND"
          SELECT COUNT(TBRACCD_AMOUNT) 
          INTO P_INDICADOR
          FROM (
              SELECT TBRACCD_AMOUNT FROM TBRACCD 
              WHERE TBRACCD_PIDM = P_PIDM_ALUMNO  
              AND TBRACCD_DETAIL_CODE = P_COD_RECONOC
              AND TBRACCD_TERM_CODE = P_PERIODO
              -- ORDER BY TBRACCD_TERM_CODE DESC
          ) WHERE ROWNUM = 1;
          
          IF P_INDICADOR = 0 THEN
                P_ABONO_MATRIC := 0;
                P_ABONO_CUOTAS := 0;
          ELSE
                SELECT  TBRACCD_AMOUNT
                INTO    P_ABONO_MATRIC
                FROM (
                        SELECT TBRACCD_AMOUNT 
                        FROM  TBRACCD 
                        WHERE TBRACCD_PIDM = P_PIDM_ALUMNO  
                        AND TBRACCD_DETAIL_CODE = P_COD_RECONOC
                        AND TBRACCD_TERM_CODE = P_PERIODO
                        -- ORDER BY TBRACCD_TERM_CODE DESC
                ) WHERE ROWNUM = 1;
          END IF;
    
    END IF;

END P_GET_PAGOS;