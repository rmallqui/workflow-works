/*
set serveroutput on
DECLARE p_fecha_valida varchar2(64);
begin
  WFK_CONTISRM.P_VALIDAR_FECHA_SOLICITUD('SOL003','51',p_fecha_valida);
  DBMS_OUTPUT.PUT_LINE(p_fecha_valida);
end;
*/


PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
  P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_FECHA_VALIDA        OUT VARCHAR2
  )
/* ===================================================================================================================
  NOMBRE    : p_validar_fecha_solicitud
  FECHA     : 25/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : se verifiqua que solicitud del alumno del tipo <P_CODIGO_SOLICITUD>( ejem SOL003), 
              se encuentra dentro del rango de fechas hábiles para atender la solicitud.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
       
BEGIN
--

    ----------------------------------------------------------------------------
        SELECT 
                DECODE(COUNT(*),0,'FALSE','TRUE') 
        INTO P_FECHA_VALIDA 
        FROM SVRSVPR 
        INNER JOIN (
              -- get la configuracion (fechas, estados, etc) de la solicitud P_CODIGO_SOLICITUD 
               SELECT SVRRSRV_SEQ_NO          TEMP_PRIORIDAD, 
                      SVRRSRV_SRVC_CODE       TEMP_SRVC_CODE, 
                      SVRRSRV_INACTIVE_IND,
                      SVRRSST_RSRV_SEQ_NO , 
                      SVRRSST_SRVC_CODE , 
                      SVRRSST_SRVS_CODE       TEMP_SRVS_CODE,
                      SVRRSRV_START_DATE      TEMP_START_DATE,--- fecha finalizacion
                      SVRRSRV_END_DATE        TEMP_END_DATE,----- fecha finalizacion
                      SVRRSRV_DELIVERY_DATE , -- fecha estimada
                      SVVSRVS_LOCKED
               FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
               INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
               ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
               AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
               INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
               ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
               WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD 
               AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activo
               -- AND SVRRSRV_WEB_IND = 'Y' ------------------------ Si esta disponible en WEB
        )TEMP
        ON SVRSVPR_SRVS_CODE        = TEMP_SRVS_CODE
        AND SVRSVPR_SRVC_CODE       =  TEMP_SRVC_CODE
        WHERE (TEMP_START_DATE < SYSDATE AND SYSDATE < TEMP_END_DATE)
        AND SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
        AND TEMP_PRIORIDAD          IN (SELECT SVRSVPR_RSRV_SEQ_NO FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD);

      
    ----------------------------------------------------------------------------
--
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END P_VALIDAR_FECHA_SOLICITUD;
