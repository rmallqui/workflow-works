NOTAS
=============================================================
=============================================================


/*
drop procedure p_validar_fecha_solicitud;
GRANT EXECUTE ON p_validar_fecha_solicitud TO wfobjects;
GRANT EXECUTE ON p_validar_fecha_solicitud TO wfauto;

*/

set serveroutput on
DECLARE p_fecha_valida varchar2(64);
begin
  WFK_CONTISRM.P_VALIDAR_FECHA_SOLICITUD('SOL003','51',p_fecha_valida);
  DBMS_OUTPUT.PUT_LINE(p_fecha_valida);
end;



--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  -- formas  --------------------- SFARGRP --------------------
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- tipos de servicios definidos por el usuario
    select SVVSRCA_CODE,SVVSRCA.* from SVVSRCA;

    -- SVVSRCA ::::::::  VALIDACION CODIGOS SERVICIO - 
    select SVVSRVC_CODE,SVVSRVC_SRCA_CODE,SVVSRVC.* from SVVSRVC;

    -- SVVSRVS :::::::::: los estados que un servicio puede adquirir 
    SELECT SVVSRVS_CODE,SVVSRVS.* FROM SVVSRVS;
      
    -- SVASVPR : :::::::: tabla SVRSVPR ; VISTA  SVRSVPR1
    SELECT SVRSVPR_PROTOCOL_SEQ_NO,SVRSVPR_SRVC_CODE,SVRSVPR_SRVS_CODE,SVRSVPR_TERM_CODE,
    SVRSVPR_WSSO_CODE,SVRSVPR_ORIG_CODE,SVRSVPR_CHNL_CODE,SVRSVPR_RQST_CODE,
    SVRSVPR_CAMP_CODE,SVRSVPR.* FROM SVRSVPR;

    -- SVVCHNL :::::::::: permite definir los canales de asistencia
    select SVVCHNL_CODE,* from SVVCHNL;

    -- SVVRQST :::::::::::::::: permite definir los tipos de solicitud
    SELECT SVVRQST_CODE,SVVRQST.* FROM SVVRQST;

    -- STVWSSO :::::::::: configurar los métodos de entrega 
        -- no se usa "CARGO" ni "EMITIDO A" , STVWSSO_CHRG,STVWSSO_ISSUE_TO_COMMENT
    SELECT STVWSSO_CODE,STVWSSO.* FROM STVWSSO;

    -- SVVSRCT :::::::: configurar los valores predefinidos para los distintos 
    --                  atributos de la solicitud de servicios
    SELECT SVVSRCT_CODE,SVVSRCT.* FROM SVVSRCT;


    -- SVARSRV ::::::::::. configurar las reglas de negocio de cada uno de los servicios definidos
    --                      ; COMO REGLAS PARA SOL003
    SELECT * FROM SVRRSRV;  -- tiene multiples tablas enlazadas, pero esta contiene la configuraciòn total.;
    SELECT * FROM SVRRSST;  -- subformulario de reglas -- estados.


    -- SVARSCM ::::::::::: permite asociar un servicio con un campus (STVCAMP) y un centro de costos


    -- SVASVPR ::::::::::: despues de generarse un solcitud puede visualizarse con la forma “Administración de
    --                      Solicitud de Servicios” (SVASVPR).


=============================================================
=============================================================

SELECT * FROM SVRSRAD;

--===============================
select * from SPRIDEN
 where SPRIDEN_PIDM = '30539';
 
 
 
 
 
-----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
--====================================================================================
--====================================================================================
--====================================================================================
--====================================================================================
--====================================================================================
-- tipos de servicios definidos por el usuario
select SVVSRCA_CODE,SVVSRCA.* from SVVSRCA;
-- SVVSRCA ::::::::  VALIDACION CODIGOS SERVICIO - 
select SVVSRVC_CODE,SVVSRVC_SRCA_CODE,SVVSRVC.* from SVVSRVC;
-- SVASVPR : :::::::: tabla SVRSVPR ; VISTA  SVRSVPR1
SELECT SVRSVPR_PROTOCOL_SEQ_NO,SVRSVPR_SRVC_CODE,SVRSVPR_SRVS_CODE,SVRSVPR_TERM_CODE,
SVRSVPR_WSSO_CODE,SVRSVPR_ORIG_CODE,SVRSVPR_CHNL_CODE,SVRSVPR_RQST_CODE,
SVRSVPR_CAMP_CODE,SVRSVPR.* FROM SVRSVPR;

SELECT 
    SVVSRVC_CODE,SVVSRCA_CODE
FROM  SVVSRVC
INNER JOIN SVVSRCA
ON SVVSRVC_SRCA_CODE = SVVSRCA_CODE
WHERE SVVSRCA_CODE = 'SOLICITUDES'
AND SVVSRVC_CODE = 'SOL003' ;






SELECT 
--    SVRSVPR_PROTOCOL_SEQ_NO,SVRSVPR_SRVC_CODE,SVRSVPR_SRVS_CODE,SVRSVPR_TERM_CODE,
--    SVRSVPR_WSSO_CODE,SVRSVPR_ORIG_CODE,SVRSVPR_CHNL_CODE,SVRSVPR_RQST_CODE,
--    SVRSVPR_CAMP_CODE
     SVRSVPR.*
FROM SVRSVPR
INNER JOIN 
  (
      SELECT 
          SVVSRVC_CODE TEMP_SVVSRVC_CODE,
          SVVSRCA_CODE TEMP_SVVSRCA_CODE
      FROM  SVVSRVC
      INNER JOIN SVVSRCA
      ON SVVSRVC_SRCA_CODE = SVVSRCA_CODE
      WHERE SVVSRCA_CODE = 'SOLICITUDES'
      AND SVVSRVC_CODE = 'SOL003' 
   ) TEMP
ON SVRSVPR_SRVC_CODE = TEMP_SVVSRVC_CODE
WHERE SVRSVPR_SRVS_CODE IN (
      SELECT SVVSRVS_CODE FROM SVVSRVS
      WHERE SVVSRVS_LOCKED <> 'l'
      AND SVVSRVS_CODE IN ('AC','PE') 
      AND SVRSVPR_ESTIMATED_DATE >= SYSDATE
  );

SELECT SYSDATE FROM DUAL;








SET SERVEROUTPUT ON
DECLARE P_ROW_FOUND varchar2(5);
BEGIN
      
      SELECT DECODE(COUNT(*),0,'FALSE','TRUE')          
      --INTO P_ROW_FOUND
      FROM SVRSVPR
      INNER JOIN 
        (
            SELECT 
                SVVSRVC_CODE TEMP_SVVSRVC_CODE,
                SVVSRCA_CODE TEMP_SVVSRCA_CODE
            FROM  SVVSRVC
            INNER JOIN SVVSRCA
            ON SVVSRVC_SRCA_CODE = SVVSRCA_CODE
            WHERE SVVSRCA_CODE = 'SOLICITUDES'
            AND SVVSRVC_CODE = 'SOL003' 
         ) TEMP
      ON SVRSVPR_SRVC_CODE = TEMP_SVVSRVC_CODE
      WHERE SVRSVPR_SRVS_CODE IN (
            SELECT SVVSRVS_CODE FROM SVVSRVS
            WHERE SVVSRVS_LOCKED <> 'L'
            AND SVVSRVS_CODE IN ('AC','PE') -- Acitvo , En Proceso
        );
END;