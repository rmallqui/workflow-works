SET SERVEROUTPUT ON
DECLARE BOOL VARCHAR2(1);
BEGIN
        BOOL :=  WFK_OAFORM.F_RSS_SRM('428093','0','0');
        --DBMS_OUTPUT.PUT_LINE(CASE WHEN BOOL = TRUE THEN 'TRUE' ELSE 'FALSE' END);
        DBMS_OUTPUT.PUT_LINE(BOOL);
END;


FUNCTION F_RSS_SRM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRM
              "Regla Solicitud de Servicio de SOLICITUD RESERVA DE MATRICULA"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 

  NRO     FECHA         USUARIO       MODIFICACION
  001     26/04/2017    RMALLQUI      Se agrego validacion para el segundo modulo de UVIR y UPGT
  002     13/06/2017    RMALLQUI      Se prioriza y obtiene el periodo mas antiguo en caso de cruce de fechas.
  =================================================================================================================== */
 IS
      V_CODIGO_SOL      SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL005';
      V_CODE_DEPT       STVDEPT.STVDEPT_CODE%TYPE;
      V_CODE_TERM       STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
      V_PRIORIDAD       NUMBER;
      V_TERM_ADMI       NUMBER;
      P_PERIODOCTLG     SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      
      V_INGRESANTE      BOOLEAN := FALSE;
      V_NCURSOS         NUMBER;
      V_FECHA_EXM       DATE;
      V_FECHA_VALIDA    BOOLEAN;

      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_PART_PERIODO        VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;
      
      -- OBTENER PERIODO por modulo de UVIR y UPGT
      CURSOR C_SFRRSTS IS
      SELECT SFRRSTS_TERM_CODE FROM (
          -- Forma SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SFRRSTS_TERM_CODE 
          FROM SFRRSTS
          WHERE SFRRSTS_PTRM_CODE IN (V_PART_PERIODO || '1', V_PART_PERIODO || '2')
          AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
          ORDER BY SFRRSTS_TERM_CODE
      ) WHERE ROWNUM <= 1;   
BEGIN  
--    
    -- DATOS ALUMNO
    P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);

    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN C_SFRRSTS_PTRM;
    LOOP
      FETCH C_SFRRSTS_PTRM INTO V_PART_PERIODO;
      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFRRSTS_PTRM;
    
      
    IF  (V_CODE_DEPT = 'UPGT' OR V_CODE_DEPT = 'UVIR') THEN

          OPEN C_SFRRSTS;
            LOOP
              FETCH C_SFRRSTS INTO P_CODE_TERM;
              IF C_SFRRSTS%FOUND THEN
                  V_CODE_TERM := P_CODE_TERM;
              ELSE EXIT;
            END IF;
            END LOOP;
          CLOSE C_SFRRSTS;
        
    END IF;

    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD(P_PIDM, V_CODIGO_SOL, V_CODE_DEPT, V_CODE_TERM, V_CODE_CAMP, V_PRIORIDAD);

    /************************************************************************************************************/
    -- Segun regla para UPGT y UVIR, los que tramitan reserva el mismo periodo que ingresaron 
    /************************************************************************************************************/
    IF  (V_CODE_DEPT = 'UPGT' AND V_PRIORIDAD = 4) OR     -- se les RESERVA las prioridades "4" 
        (V_CODE_DEPT = 'UVIR' AND V_PRIORIDAD = 7)        -- se les RESERVA las prioridades "7"
        THEN

        -- VALIDAR QUE EL ALUMNO SEA INGRESANTE EN EL MISMO PERIODO DEL TRAMITE. (PERIODO DE INGRESO = PERIODO ACTUAL)
        SELECT  COUNT(*) INTO V_TERM_ADMI
        FROM SORLCUR        INNER JOIN SORLFOS
        ON    SORLCUR_PIDM          = SORLFOS_PIDM 
        AND   SORLCUR_SEQNO         = SORLFOS.SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM          =  P_PIDM
        AND SORLCUR_LMOD_CODE       = 'LEARNER' /*#Estudiante*/ 
        AND SORLCUR_CACT_CODE       = 'ACTIVE' 
        AND SORLCUR_CURRENT_CDE     = 'Y'
        AND SORLCUR_TERM_CODE_ADMIT = V_CODE_TERM -- VALIDACION (PERIODO DE INGRESO = PERIODO ACTUAL)
        AND SORLFOS_DEPT_CODE       = V_CODE_DEPT;

    /************************************************************************************************************/
    -- para casos de INGREASANTES solo regular huancayo - VALIDACIONES POR FECHA DE EXAMEN
    /************************************************************************************************************/
    ELSIF  (V_CODE_DEPT = 'UREG' AND V_PRIORIDAD = 2 AND V_CODE_CAMP = 'S01')  THEN     -- Ingresanter periodo activo regular  con prioridad '2'

        P_DATOS_INGRESO(P_PIDM , V_CODE_CAMP,V_CODE_TERM,V_CODE_DEPT,V_INGRESANTE,V_FECHA_EXM);
        
        SELECT COUNT(*) INTO V_NCURSOS FROM SFRSTCR 
        WHERE SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_TERM_CODE = V_CODE_TERM;
        
        V_FECHA_VALIDA := CASE WHEN TO_CHAR(V_FECHA_EXM,'dd/mm/yyyy') = '26/03/2017' THEN TRUE
                               WHEN TO_CHAR(V_FECHA_EXM,'dd/mm/yyyy') = '02/04/2017' THEN TRUE
                               WHEN V_NCURSOS = 0 THEN TRUE ELSE FALSE END;
         
        IF (V_INGRESANTE AND V_FECHA_VALIDA) THEN
            V_TERM_ADMI := 1;
        ELSE 
            V_TERM_ADMI := 0;
        END IF;

    ELSE
        V_TERM_ADMI := 1;
    END IF;

    
    IF (V_PRIORIDAD > 0 AND V_TERM_ADMI > 0) THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';

--
END F_RSS_SRM;