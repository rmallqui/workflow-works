/******************************************************************************/
/* BWZKSDOD.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para generar el Paquete relacionado a los        */
/*                    procedimientos de Cambio de modalidad de estudios y/o   */
/*                    campus en Admisión.                                     */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del código.                                    BFV 10/ENE/2019 */
/*    Creación del paquete de Permanencia de Plan de Estudios                 */
/*                                                                            */
/*    Procedure P_VALID_ID: Verifica si el DNI ingresado existe en la DB      */
/*    Procedure P_GET_INFO_INGRESANTE: El objetivo es que se obtenga los      */
/*      datos del estudiante.                                                 */
/*    Procedure P_VERIFICAR_APTO: El objetivo es que se saber si el           */
/*      estudiante cumple con las verificaciones necesarias para acceder al   */
/*      servicio del Workflow.                                                */
/*    Procedure P_VERIFICAR_REQUISITOS_PREVIOS: Se verifica si el estudiante  */
/*      ha traido sus requisitos de admision                                  */
/*    Procedure P_GET_CAMBIO_ADM: Obtiene el cambio realizado en RECRUITER    */
/*    Procedure P_GET_USUARIOS_ADMISION: Obtienes el correo de los usuarios   */
/*      de las oficinas involucradas                                          */
/*    Procedure P_VERIFICAR_MATRICULA: Verifica si tiene matricula activa.    */
/*    Procedure P_ELIMINAR_NRC: Elimina la matricula del estudiante en el     */
/*      periodo solicitado.                                                   */
/*    Procedure P_VALIDAR_CONVA: El objetivo es verificar si tiene registrado */
/*      una convalidacion                                                     */
/*    Procedure P_VERIFICAR_PAGOS_ADM: Se verifica si el estudiante tiene     */
/*      pagos en su cuenta anterior de admisión                               */
/*    Procedure P_ACTIVAR_INGRESANTE:  Se registra el ingreso de la nueva     */
/*      postulacion                                                           */
/*    Procedure P_VERIFICAR_PAGOS_CTA: Se verifica si el estudiante tiene     */
/*      pagos en su cuenta anterior                                           */
/*    Procedure P_VERIFICAR_BENEFICIO_PREVIO: Elimina la cuenta anterior      */
/*      del estudiante                                                        */
/*    Procedure P_REGISTRAR_CAMBIO_ADM: Registra en la BD el cambio del       */
/*      estudiante solicitante                                                */
/* 2. Actualización P_VERIFICAR_REQUISITOS_PREVIOS            BFV 24/ENE/2019 */
/*    Se optimiza el script para agilizar la toma de los requisitos de ADM.   */
/* 3. Actualizacion P_GET_CAMBIO_ADM                          BFV 05/FEB/2019 */
/*    Se optmiza la consulta para cuando el estudiante realiza un cambio solo */
/*      de sede.                                                              */
/* 4. Actualizacion P_ACTIVAR_INGRESANTE                      BFV 07/FEB/2019 */
/*    Se optimiza la consulta a BDUCCI.tblpostulante quitando la sentencia    */
/*      "TRIM".                                                               */
/*    Actualizacion P_VERIFICAR_PAGOS_CTA                                     */
/*    Se aumenta la validación de la sede LIMA y el plan nuevo.               */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BANINST1.BWZKSDOD AS
/*
 BWZKSDOD:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti Cambio Sede Departamento On Demand ADM
*/
-- FILE NAME..: BWZKSDOD.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSDOD
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_VALID_ID (
    P_ID            IN  SPRIDEN.SPRIDEN_ID%TYPE,
    P_DNI_VALID     OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_INFO_INGRESANTE (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
    P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_DESC      OUT SMRPRLE.SMRPRLE_PROGRAM_DESC%TYPE,
    P_MOD_ADM           OUT VARCHAR2,
    P_MOD_ADM_DESC      OUT VARCHAR2,
    P_CATALOGO          OUT VARCHAR2,
    P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
    P_NAME              OUT VARCHAR2,
    P_FECHA_SOL         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_APTO (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_APTO     OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_REQUISITOS_PREVIOS (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ARCHIVADOR_APEC   OUT VARCHAR2,
    P_REQUISITOS_APEC   OUT VARCHAR2,
    P_ARCHIVADOR_BANNER OUT VARCHAR2,
    P_REQUISITOS_BANNER OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_CAMBIO_ADM (
    P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE	            IN STVTERM.STVTERM_CODE%TYPE,
    P_DEPT_CODE			    IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC             IN STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM               IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MOD_ADM               IN VARCHAR2,
    P_MOD_ADM_DESC          IN VARCHAR2,
    P_CAMBIO_ADM            OUT VARCHAR2,
    P_DEPT_CODE_NEW		    OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE_DEST        OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW		    OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_NEW_DESC         OUT VARCHAR2,
    P_MOD_ADM_NEW           OUT VARCHAR2,
    P_MOD_ADM_NEW_DESC      OUT VARCHAR2,
    P_PROGRAM_NEW           OUT VARCHAR2,
    P_PROGRAM_NEW_DESC      OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIOS_ADMISION (
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CORREO_RRAA           OUT VARCHAR2,
    P_ROL_DOC_RRAA          OUT VARCHAR2,
    P_CORREO_DOC_RRAA       OUT VARCHAR2,
    P_CORREO_BU             OUT VARCHAR2,
    P_CORREO_ADM            OUT VARCHAR2,
    P_ROL_CAJA              OUT VARCHAR2,
    P_CORREO_CAJA           OUT VARCHAR2,
    P_CORREO_CAU            OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_MATRICULA (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_MAT      OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_ELIMINAR_NRC (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VALIDAR_CONVA (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_CONVA      OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_PAGOS_CTA_ADM(
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE_NEW		IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_CTA_ADM    OUT VARCHAR2,
    P_CTA_DESC_ADM  	OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_ACTIVAR_INGRESANTE (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE      	IN STVDEPT.STVDEPT_CODE%TYPE,
    P_DEPT_CODE_NEW 	IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_DEST    IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_PAGOS_CTA (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE_NEW     IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_CTA        OUT VARCHAR2,
    P_CTA_DESC			OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_BENEFICIO_PREVIO (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE_NEW     IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_BEN        OUT VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_REGISTRAR_CAMBIO_ADM (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_DEPT_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_DEPT_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_MOD_ADM           IN VARCHAR2,
    P_MOD_ADM_NEW       IN VARCHAR2,
    P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MESSAGE			OUT VARCHAR2
);

END BWZKSDOD;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSDOD;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSDOD FOR BANINST1.BWZKSDOD;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSDOD
--  START gurgrth BWZKSDOD
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/