/*
SET SERVEROUTPUT ON
DECLARE P_CAMBIO_ADM varchar2(4000);
 P_DEPT_CODE_NEW varchar2(4000);
 P_CAMP_CODE_DEST varchar2(4000);
 P_CAMP_CODE_NEW varchar2(4000);
 P_CAMP_NEW_DESC varchar2(4000);
 P_MOD_ADM_NEW VARCHAR2(4000);
 P_MOD_ADM_NEW_DESC VARCHAR2(4000);
 P_PROGRAM_NEW VARCHAR2(4000);
 P_PROGRAM_NEW_DESC VARCHAR2(4000);
 P_MESSAGE VARCHAR2(4000);
BEGIN
    P_GET_CAMBIO_ADM('73632340', '201910', 'UREG', 'S01','HYO','108', '79', 'Previa', P_CAMBIO_ADM, P_DEPT_CODE_NEW, P_CAMP_CODE_DEST, P_CAMP_CODE_NEW, P_CAMP_NEW_DESC, P_MOD_ADM_NEW, P_MOD_ADM_NEW_DESC, P_PROGRAM_NEW, P_PROGRAM_NEW_DESC, P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_CAMBIO_ADM);
    DBMS_OUTPUT.PUT_LINE(P_DEPT_CODE_NEW);
    DBMS_OUTPUT.PUT_LINE(P_CAMP_CODE_DEST);
    DBMS_OUTPUT.PUT_LINE(P_CAMP_CODE_NEW);
    DBMS_OUTPUT.PUT_LINE(P_CAMP_NEW_DESC);
    DBMS_OUTPUT.PUT_LINE(P_MOD_ADM_NEW);
    DBMS_OUTPUT.PUT_LINE(P_MOD_ADM_NEW_DESC);
    DBMS_OUTPUT.PUT_LINE(P_PROGRAM_NEW);
    DBMS_OUTPUT.PUT_LINE(P_PROGRAM_NEW_DESC);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_GET_CAMBIO_ADM (
    P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE	            IN STVTERM.STVTERM_CODE%TYPE,
    P_DEPT_CODE			    IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC             IN STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM               IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MOD_ADM               IN VARCHAR2,
    P_MOD_ADM_DESC          IN VARCHAR2,
    P_CAMBIO_ADM            OUT VARCHAR2,
    P_DEPT_CODE_NEW		    OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE_DEST        OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW		    OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_NEW_DESC         OUT VARCHAR2,
    P_MOD_ADM_NEW           OUT VARCHAR2,
    P_MOD_ADM_NEW_DESC      OUT VARCHAR2,
    P_PROGRAM_NEW           OUT VARCHAR2,
    P_PROGRAM_NEW_DESC      OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
AS
	V_CONTADOR_DEPT     NUMBER := 0;
    V_CONTADOR_CAMP     NUMBER := 0;
    V_CONTADOR_ME       NUMBER := 0;
	V_APEC_TERM         VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);
    V_APEC_CAMP         VARCHAR2(10);
    V_APEC_DEPT_NEW     VARCHAR2(10);
    V_APEC_ME_NEW       VARCHAR2(10);
    V_APEC_CAMP_NEW     VARCHAR2(10);
    V_APEC_MOD_ADM      VARCHAR(10);
    V_APEC_MOD_ADM_NEW  VARCHAR(10);
    V_PROGRAM_DESC      VARCHAR2(100);
BEGIN
    
    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
    INTO V_APEC_DEPT
    FROM CZRDEPT 
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- CAMPUS
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE;

    -- MODALIDAD DE ADMISION
    SELECT "IDModalidadPostu"
        INTO V_APEC_MOD_ADM
    FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDModRecruiter" = P_MOD_ADM
    AND "departament" = V_APEC_DEPT;
    
    --INDICADOR QUE EXISTE EL CAMBIO DEPT
    SELECT COUNT(*)
        INTO V_CONTADOR_DEPT
    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
    WHERE TRIM("IDDependencia") = 'UCCI'
    AND TRIM("IDPerAcad") = V_APEC_TERM
    AND TRIM("IDEscuelaADM") <> V_APEC_DEPT
    AND "IDAlumno" = P_ID
    AND TRIM("Ingresante") = '0'
    AND TRIM("Renuncia") = '0';
   
    IF V_CONTADOR_DEPT > 0 THEN
        SELECT DISTINCT TRIM("IDEscuela"), "IDModalidadPostu", TRIM("IDEscuelaADM")
            INTO P_PROGRAM_NEW, V_APEC_MOD_ADM_NEW, V_APEC_DEPT_NEW
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE TRIM("IDDependencia") = 'UCCI'
        AND TRIM("IDPerAcad") = V_APEC_TERM
        AND TRIM("IDEscuelaADM") <> V_APEC_DEPT
        AND "IDAlumno" = P_ID
        AND TRIM("Ingresante") = '0'
        AND TRIM("Renuncia") = '0';

        -- DEPARTAMENTO
        SELECT CZRDEPT_CODE
            INTO P_DEPT_CODE_NEW
        FROM CZRDEPT 
        WHERE CZRDEPT_DIVS_CODE = 'UCCI'
        AND CZRDEPT_DEPT_BDUCCI = V_APEC_DEPT_NEW;

        IF P_PROGRAM <> P_PROGRAM_NEW THEN
            -- >> DESC PROGRAM NEW
            SELECT SMRPRLE_PROGRAM_DESC
                INTO P_PROGRAM_NEW_DESC
            FROM SMRPRLE
            WHERE SMRPRLE_PROGRAM = P_PROGRAM_NEW;
            
            ELSE
                -- >> DESC PROGRAM
                SELECT SMRPRLE_PROGRAM_DESC
                    INTO P_PROGRAM_NEW_DESC
                FROM SMRPRLE
                WHERE SMRPRLE_PROGRAM = P_PROGRAM;
        END IF;

        IF V_APEC_MOD_ADM_NEW <> V_APEC_MOD_ADM THEN
            -- >> MODALIDAD DE ADMISION NUEVO
            SELECT "IDModRecruiter"
                INTO P_MOD_ADM_NEW
            FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDModalidadPostu" = V_APEC_MOD_ADM_NEW
            AND TRIM("departament") = V_APEC_DEPT_NEW;
            
            -- >> DESC MODALIDAD DE ADMISION
            SELECT STVADMT_DESC
                INTO P_MOD_ADM_NEW_DESC
            FROM STVADMT
            WHERE STVADMT_CODE = P_MOD_ADM_NEW;
            
            ELSE
                P_MOD_ADM_NEW := P_MOD_ADM;
                P_MOD_ADM_NEW_DESC := P_MOD_ADM_DESC;
        END IF;

        --INDICADOR QUE EXISTE EL CAMBIO CAMP
        SELECT COUNT(*)
        INTO V_CONTADOR_ME
        FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
        INNER JOIN CZRDEPT DE ON
            AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
            AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
        WHERE AE."IDAlumno" = P_ID
        AND AE."IDEscuela" <> V_APEC_DEPT
        ORDER BY AE."FechaInscripcion" DESC;
        
        IF V_CONTADOR_ME > 0 THEN
            SELECT IDEscuelaADM, IDSede
            INTO V_APEC_ME_NEW, V_APEC_CAMP_NEW
            FROM (
                SELECT TRIM(AE."IDEscuela") IDEscuelaADM, TRIM(AE."IDSede") IDSede
                FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
                INNER JOIN CZRDEPT DE ON
                    AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
                    AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
                WHERE AE."IDAlumno" = P_ID
                AND AE."IDEscuela" <> V_APEC_DEPT
                ORDER BY AE."FechaInscripcion" DESC
                ) WHERE ROWNUM = 1;
            
            -- NEW CAMPUS
            SELECT CZRCAMP_CODE
                INTO P_CAMP_CODE_DEST
            FROM CZRCAMP 
            WHERE CZRCAMP_CAMP_BDUCCI = V_APEC_CAMP_NEW;
            
            IF P_CAMP_CODE <> P_CAMP_CODE_DEST THEN
                IF V_APEC_ME_NEW = 'ADV' THEN
                    P_CAMP_CODE_NEW := 'V00';
                ELSE
                    P_CAMP_CODE_NEW := P_CAMP_CODE_DEST;
                END IF;
                V_CONTADOR_CAMP := 1;
            ELSE
                IF V_APEC_ME_NEW = 'ADV' THEN
                    P_CAMP_CODE_NEW := 'V00';
                END IF;
            END IF;

            SELECT STVCAMP_DESC
                INTO P_CAMP_NEW_DESC
            FROM STVCAMP
            WHERE STVCAMP_CODE = P_CAMP_CODE_NEW;

        ELSE

            SELECT IDEscuelaADM, IDSede
            INTO V_APEC_ME_NEW, V_APEC_CAMP_NEW
            FROM (
                SELECT TRIM(AE."IDEscuela") IDEscuelaADM, TRIM(AE."IDSede") IDSede
                FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
                INNER JOIN CZRDEPT DE ON
                    AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
                    AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
                WHERE AE."IDAlumno" = P_ID
                AND AE."IDEscuela" = V_APEC_DEPT
                ORDER BY AE."FechaInscripcion" DESC
                ) WHERE ROWNUM = 1;
            
            -- NEW CAMPUS
            SELECT CZRCAMP_CODE
                INTO P_CAMP_CODE_DEST
            FROM CZRCAMP 
            WHERE CZRCAMP_CAMP_BDUCCI = V_APEC_CAMP_NEW;

            IF P_CAMP_CODE <> P_CAMP_CODE_DEST THEN
                IF V_APEC_ME_NEW = 'ADV' THEN
                    P_CAMP_CODE_NEW := 'V00';
                ELSE
                    P_CAMP_CODE_NEW := P_CAMP_CODE_DEST;
                END IF;
                V_CONTADOR_CAMP := 1;
            END IF;

            SELECT STVCAMP_DESC
                INTO P_CAMP_NEW_DESC
            FROM STVCAMP
            WHERE STVCAMP_CODE = P_CAMP_CODE_NEW;

        END IF;
        
    ELSE

        IF V_APEC_DEPT <> 'ADV' THEN
            --INDICADOR QUE EXISTE EL CAMBIO CAMP
            SELECT COUNT(*)
                INTO V_CONTADOR_CAMP
            FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
            INNER JOIN CZRDEPT DE ON
                AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
                AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
            WHERE AE."IDAlumno" = P_ID
            AND(AE."IDSede") <> V_APEC_CAMP
            AND AE."IDEscuela" = V_APEC_DEPT
            ORDER BY AE."FechaInscripcion" DESC;

            IF V_CONTADOR_CAMP > 0 THEN
                SELECT *
                INTO V_APEC_CAMP_NEW
                FROM (
                    SELECT TRIM(AE."IDSede")
                    FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
                    INNER JOIN CZRDEPT DE ON
                        AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
                        AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
                    WHERE AE."IDAlumno" = P_ID
                    AND(AE."IDSede") <> V_APEC_CAMP
                    AND AE."IDEscuela" = V_APEC_DEPT
                    ORDER BY AE."FechaInscripcion" DESC)
                WHERE ROWNUM = 1;
                
                SELECT DISTINCT TRIM("IDEscuela"), "IDModalidadPostu", TRIM("IDEscuelaADM")
                    INTO P_PROGRAM_NEW, V_APEC_MOD_ADM_NEW, V_APEC_DEPT_NEW
                FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                WHERE TRIM("IDDependencia") = 'UCCI'
                AND TRIM("IDPerAcad") = V_APEC_TERM
                AND TRIM("IDEscuelaADM") = V_APEC_DEPT
                AND "IDAlumno" = P_ID
                AND TRIM("Renuncia") = '0';

                -- DEPARTAMENTO
                SELECT CZRDEPT_CODE
                    INTO P_DEPT_CODE_NEW
                FROM CZRDEPT 
                WHERE CZRDEPT_DIVS_CODE = 'UCCI'
                AND CZRDEPT_DEPT_BDUCCI = V_APEC_DEPT_NEW;

                -- NEW CAMPUS
                SELECT CZRCAMP_CODE
                    INTO P_CAMP_CODE_DEST
                FROM CZRCAMP 
                WHERE CZRCAMP_CAMP_BDUCCI = V_APEC_CAMP_NEW;

                IF V_APEC_DEPT = 'ADV' THEN
                    P_CAMP_CODE_NEW := 'V00';
                ELSE
                    P_CAMP_CODE_NEW := P_CAMP_CODE_DEST;
                END IF;
            ELSE
                P_CAMP_CODE_DEST := P_CAMP_CODE;
                P_CAMP_CODE_NEW := P_CAMP_CODE;
            END IF;
            
            -- NEW CAMPUS DESC
            SELECT STVCAMP_DESC
                INTO P_CAMP_NEW_DESC
            FROM STVCAMP
            WHERE STVCAMP_CODE = P_CAMP_CODE_NEW;

            IF P_PROGRAM <> P_PROGRAM_NEW THEN
            -- >> DESC PROGRAM NEW
            SELECT SMRPRLE_PROGRAM_DESC
                INTO P_PROGRAM_NEW_DESC
            FROM SMRPRLE
            WHERE SMRPRLE_PROGRAM = P_PROGRAM_NEW;
            
            ELSE
                -- >> DESC PROGRAM
                SELECT SMRPRLE_PROGRAM_DESC
                    INTO P_PROGRAM_NEW_DESC
                FROM SMRPRLE
                WHERE SMRPRLE_PROGRAM = P_PROGRAM;
            END IF;
            
        IF V_APEC_MOD_ADM_NEW <> V_APEC_MOD_ADM THEN
            -- >> MODALIDAD DE ADMISION NUEVO
            SELECT "IDModRecruiter"
                INTO P_MOD_ADM_NEW
            FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDModalidadPostu" = V_APEC_MOD_ADM_NEW
            AND TRIM("departament") = V_APEC_DEPT_NEW;
            
            -- >> DESC MODALIDAD DE ADMISION
            SELECT STVADMT_DESC
                INTO P_MOD_ADM_NEW_DESC
            FROM STVADMT
            WHERE STVADMT_CODE = P_MOD_ADM_NEW;
            
            ELSE
                P_MOD_ADM_NEW := P_MOD_ADM;
                P_MOD_ADM_NEW_DESC := P_MOD_ADM_DESC;
        END IF;

        ELSE
            V_CONTADOR_CAMP := 0;
        END IF;
    END IF;
    
    IF P_PROGRAM <> '502' AND P_PROGRAM_NEW = '502' THEN
        P_CAMBIO_ADM := 'NO';
        P_MESSAGE := 'ERROR! El estudiante no cambiarse a la carrera de Medicina Humana. Por favor, verificar el cambio en RECRUITER.';
        P_DEPT_CODE_NEW := '-';
        P_CAMP_CODE_DEST := '-';
        P_CAMP_CODE_NEW	:= '-';
        P_CAMP_NEW_DESC := '-';
        P_MOD_ADM_NEW := '-';
        P_MOD_ADM_NEW_DESC := '-';
        P_PROGRAM_NEW := '-';
        P_PROGRAM_NEW_DESC := '-';
    ELSE
        IF V_CONTADOR_CAMP > 0 AND V_CONTADOR_DEPT > 0 THEN
            P_CAMBIO_ADM := 'MS';
            P_MESSAGE := 'Cambio de modalidad de estudios y sede';
        ELSIF V_CONTADOR_CAMP > 0 AND V_CONTADOR_DEPT = 0 THEN
            P_CAMBIO_ADM := 'SE';
            P_MESSAGE := 'Cambio de sede';
        ELSIF V_CONTADOR_CAMP = 0 AND V_CONTADOR_DEPT > 0 THEN
            P_CAMBIO_ADM := 'ME';
            P_MESSAGE := 'Cambio de modalidad de estudios';
        ELSE
            P_CAMBIO_ADM := 'NO';
            P_MESSAGE := 'ERROR! El estudiante no tiene registrado su cambio de modalidad de estudios y/o campus. Por favor, verificar el cambio en RECRUITER.';
            P_DEPT_CODE_NEW := '-';
            P_CAMP_CODE_DEST := '-';
            P_CAMP_CODE_NEW	:= '-';
            P_CAMP_NEW_DESC := '-';
            P_MOD_ADM_NEW := '-';
            P_MOD_ADM_NEW_DESC := '-';
            P_PROGRAM_NEW := '-';
            P_PROGRAM_NEW_DESC := '-';
        END IF;
    END IF;
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_CAMBIO_ADM;