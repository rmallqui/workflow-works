/******************************************************************************/
/* BWZKSDOD.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para generar el Paquete relacionado a los        */
/*                    procedimientos de Cambio de modalidad de estudios y/o   */
/*                    campus en Admisión.                                     */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del código.                                    BFV 10/ENE/2019 */
/*    Creación del paquete de Permanencia de Plan de Estudios                 */
/*                                                                            */
/*    Procedure P_VALID_ID: Verifica si el DNI ingresado existe en la DB      */
/*    Procedure P_GET_INFO_INGRESANTE: El objetivo es que se obtenga los      */
/*      datos del estudiante.                                                 */
/*    Procedure P_VERIFICAR_APTO: El objetivo es que se saber si el           */
/*      estudiante cumple con las verificaciones necesarias para acceder al   */
/*      servicio del Workflow.                                                */
/*    Procedure P_VERIFICAR_REQUISITOS_PREVIOS: Se verifica si el estudiante  */
/*      ha traido sus requisitos de admision                                  */
/*    Procedure P_GET_CAMBIO_ADM: Obtiene el cambio realizado en RECRUITER    */
/*    Procedure P_GET_USUARIOS_ADMISION: Obtienes el correo de los usuarios   */
/*      de las oficinas involucradas                                          */
/*    Procedure P_VERIFICAR_MATRICULA: Verifica si tiene matricula activa.    */
/*    Procedure P_ELIMINAR_NRC: Elimina la matricula del estudiante en el     */
/*      periodo solicitado.                                                   */
/*    Procedure P_VALIDAR_CONVA: El objetivo es verificar si tiene registrado */
/*      una convalidacion                                                     */
/*    Procedure P_VERIFICAR_PAGOS_ADM: Se verifica si el estudiante tiene     */
/*      pagos en su cuenta anterior de admisión                               */
/*    Procedure P_ACTIVAR_INGRESANTE:  Se registra el ingreso de la nueva     */
/*      postulacion                                                           */
/*    Procedure P_VERIFICAR_PAGOS_CTA: Se verifica si el estudiante tiene     */
/*      pagos en su cuenta anterior                                           */
/*    Procedure P_VERIFICAR_BENEFICIO_PREVIO: Elimina la cuenta anterior      */
/*      del estudiante                                                        */
/*    Procedure P_REGISTRAR_CAMBIO_ADM: Registra en la BD el cambio del       */
/*      estudiante solicitante                                                */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/**********************************************************************/
-- Creación BANNIS  Y PERMISOS
/**********************************************************************/
--     CREATE OR REPLACE PUBLIC SYNONYM "BWZKSDOD" FOR "BANINST1"."BWZKSDOD";
--     GRANT EXECUTE ON BANINST1.BWZKSDOD TO SATURN;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY BANINST1.BWZKSDOD AS
/*
  BWZKSDOD:
       Paquete Web _ Desarrollo Propio _ Paquete _ Cambio Sede Departamento On Demand ADM
*/
-- FILE NAME..: BWZKSDOD.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSDOD
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2019
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_VALID_ID (
    P_ID            IN  SPRIDEN.SPRIDEN_ID%TYPE,
    P_DNI_VALID     OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VALID_ID (P_ID, P_DNI_VALID);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VALID_ID;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_INFO_INGRESANTE (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
    P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_DESC      OUT SMRPRLE.SMRPRLE_PROGRAM_DESC%TYPE,
    P_MOD_ADM           OUT VARCHAR2,
    P_MOD_ADM_DESC      OUT VARCHAR2,
    P_CATALOGO          OUT VARCHAR2,
    P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
    P_NAME              OUT VARCHAR2,
    P_FECHA_SOL         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_GET_INFO_INGRESANTE (P_ID, P_PIDM, P_TERM_CODE, P_DEPT_CODE, P_CAMP_CODE, P_CAMP_DESC, P_PROGRAM, P_PROGRAM_DESC, P_MOD_ADM, P_MOD_ADM_DESC, P_CATALOGO, P_EMAIL, P_NAME, P_FECHA_SOL);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_INFO_INGRESANTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_APTO (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_APTO     OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
)
AS
    V_SOLTRA            INTEGER := 0; --VERIFICA SOL. TRASLADO INTERNO
    V_SOLPER            INTEGER := 0; --VERIFICA SOL. PERMANENCIA DE PLAN
    V_SOLRES            INTEGER := 0; --VERIFICA SOL. RESERVA
    V_SOLREC            INTEGER := 0; --VERIFICA SOL. RECTIFICACION DE MATRICULA
    V_SOLCAM            INTEGER := 0; --VERIFICA SOL. CAMBIO DE PLAN
    V_SOLCUO            INTEGER := 0; --VERIFICA SOL. CAMBIO DE CUOTA INICIAL
    V_SOLDIR            INTEGER := 0; --VERIFICA SOL. ASIGNTURA DIRIGIDA
    V_SOLCON            INTEGER := 0; --VERIFICA SOL. DESCUENTO POR CONVENIO
    V_FEC_ADM           INTEGER := 0; --VERIFICA SI ESTA VIGENTE EL PERIODO DE ADMISION DEL ESTUDIANTE PARA EL CAMBIO
    V_PROC_DUP          INTEGER := 0; --VERIFICA SI EL ESTUDIANTE YA TIENE UN PROCESO DE CAMBIO FINALIZADO
    V_RESPUESTA         VARCHAR2(4000) := NULL; --RESPUESTA ANIDADA DE LAS VALIDACIONES
    V_FLAG              INTEGER := 0; --BANDERA DE VALIDACIÓN
      
    V_CODE_DEPT         STVDEPT.STVDEPT_CODE%TYPE;
    V_CODE_CAMP         STVCAMP.STVCAMP_CODE%TYPE;
    V_PROGRAM           SORLCUR.SORLCUR_PROGRAM%TYPE;

    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS ON
                SORLCUR_PIDM = SORLFOS_PIDM 
                AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM = P_PIDM 
                AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;

BEGIN
    ---- P_VERIFICAR_APTO PARA SOLICITUD DE CAMBIO DE CARRERA (ADMISION) ON DEMAND
    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
    CLOSE C_SORLCUR;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');

    IF V_SOLREC > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_SOLTRA
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_SOLCAM
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE CUOTA INICIAL ACTIVO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_SOLCUO
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL007'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCUO > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DESCUENTO POR CONVENIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_SOLCON
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL019'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLCON > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Descuento por convenio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Descuento pot convenio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE FECHAS DE ADMISIÓN
    --################################################################################################
    SELECT COUNT(*)
        INTO V_FEC_ADM
    FROM ADM.tblFechaAdmisionWF@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "term" = P_TERM_CODE
    AND "campus" = V_CODE_CAMP
    AND "dept" = V_CODE_DEPT
    AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE("FechaInicio",'dd/mm/yy')
    AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE("FechaFin",'dd/mm/yy');

    IF V_FEC_ADM = 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No esta dentro de las fechas de admisión vigentes al periodo '|| P_TERM_CODE || '. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No esta dentro de las fechas de admisión vigentes al periodo '|| P_TERM_CODE || '. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE PERIODO DE INGRESO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_PROC_DUP
    FROM ADM.tblCambiosADM@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "pidm" = P_PIDM
    AND "term" = P_TERM_CODE;

    IF V_PROC_DUP > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El estudiante ya tiene un cambio de admisión. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El estudiante ya tiene un cambio de admisión. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_MESSAGE := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_MESSAGE := 'OK';
    END IF;

EXCEPTION 
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_APTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_REQUISITOS_PREVIOS (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ARCHIVADOR_APEC   OUT VARCHAR2,
    P_REQUISITOS_APEC   OUT VARCHAR2,
    P_ARCHIVADOR_BANNER OUT VARCHAR2,
    P_REQUISITOS_BANNER OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
    V_APEC_TERM         VARCHAR2(10);
    V_FLAG_ARCH_APEC    BOOLEAN := FALSE;
    V_FLAG_ARCH_BANNER  BOOLEAN := FALSE;
    V_FLAG_REQ_APEC     BOOLEAN := FALSE;
    V_FLAG_REQ_BANNER   BOOLEAN := FALSE;
    
    V_ARCH_APEC         VARCHAR2(1000) := '- Sin Registros';
    V_ARCH_BANNER       VARCHAR2(1000) := '- Sin Registros';
    V_REQ_APEC          VARCHAR2(1000) := 'Sin registro de requisitos previos';
    V_REQ_BANNER        VARCHAR2(1000) := 'Sin registro de requisitos previos';

BEGIN

    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM 
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- OBTENER LOS REGISTROS DE ARCHIVADOR DE APEC
    WITH CTE AS(
        SELECT "Archivador" ARCHIVADORAPEC, ROWNUM AS N
        FROM tblPostulanteRequisitos@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDAlumno" = P_ID
            AND ("Archivador" IS NOT NULL OR "Archivador" <> '')
    ),
    CTE2(ARCHIVADORAPEC, N, ARCHIVADORAPEC_ULT) AS
    (
        SELECT ARCHIVADORAPEC, N, ARCHIVADORAPEC
        FROM CTE
        WHERE N=1
        
        UNION ALL
        
        SELECT a.ARCHIVADORAPEC, a.N, B.ARCHIVADORAPEC_ULT || '<br />' || a.ARCHIVADORAPEC
        FROM CTE a
        INNER JOIN CTE2 b ON 
            a.N = b.N + 1
    )
    SELECT NVL(MAX(ARCHIVADORAPEC_ULT),'- Sin Registro')
        INTO P_ARCHIVADOR_APEC
    FROM CTE2
    ;

    -- OBTENER LOS REGISTROS DE ARCHIVADOR DE BANNER
    WITH cte AS(
        SELECT SARCHKL_CODE_VALUE ArchivadorBANNER, rownum as n
        FROM SARCHKL
        WHERE SARCHKL_TERM_CODE_ENTRY = P_TERM_CODE
            AND (SARCHKL_CODE_VALUE IS NOT NULL OR SARCHKL_CODE_VALUE <> '')
            AND SARCHKL_PIDM = P_PIDM
    ),
    cte2(ArchivadorBANNER, n, ArchivadorBANNER_Ult) AS
    (
        SELECT ArchivadorBANNER, n, ArchivadorBANNER
        FROM cte
        WHERE  n=1
        
        UNION ALL
        
        SELECT a.ArchivadorBANNER, a.n, b.ArchivadorBANNER_Ult || '<br />' || a.ArchivadorBANNER
        FROM cte a
        INNER JOIN cte2 b
            ON a.n = b.n + 1
    )
    SELECT NVL(MAX(ArchivadorBANNER_Ult),'- Sin Registro')
        INTO P_ARCHIVADOR_BANNER
    FROM cte2
    ;

    -- OBTENER LOS REQUISITOS DE APEC
     WITH CTE AS (
        SELECT CASE 
            WHEN pr."Observacion" IS NOT NULL THEN '- ' || re."Nombre" || ' - Obs: ' || pr."Observacion"
            WHEN pr."Observacion" <> '' THEN '- ' || re."Nombre" || ' - Obs: ' || pr."Observacion"
            ELSE '- ' || re."Nombre"
            END RequisitoAPEC, rownum as n
        FROM tblPostulanteRequisitos@BDUCCI.CONTINENTAL.EDU.PE pr
        INNER JOIN tblRequisitos@BDUCCI.CONTINENTAL.EDU.PE re ON
            pr."IDRequisito" = re."IDRequisito"
        WHERE pr."IDDependencia" = 'UCCI'
            AND pr."IDPerAcad" = V_APEC_TERM
            AND pr."IDAlumno" = P_ID
            AND pr."CantEntregada" > 0
        ORDER BY re."Nombre"
    ),
    CTE2(RequisitoAPEC, n, RequisitoAPEC_ULT) AS (
        SELECT RequisitoAPEC, n, RequisitoAPEC
        FROM CTE
        WHERE n=1
        
        UNION ALL
        
        SELECT a.RequisitoAPEC, a.n, b.RequisitoAPEC_ULT || '<br />' || a.RequisitoAPEC
        FROM CTE a
        INNER JOIN cte2 b ON
            a.n = b.n + 1
    )
    SELECT NVL(MAX(RequisitoAPEC_ULT),'- Sin registro de requisitos previos')
        INTO P_REQUISITOS_APEC
    FROM CTE2
    ;

    -- OBTENER LOS REQUISITOS DE BANNER
    with cte as(
        SELECT CASE 
            WHEN SARCHKL_COMMENT IS NOT NULL THEN '- ' || STVADMR_DESC || ' - Obs: ' || SARCHKL_COMMENT 
            WHEN SARCHKL_COMMENT <> '' THEN '- ' || STVADMR_DESC || ' - Obs: ' || SARCHKL_COMMENT 
            ELSE '- ' || STVADMR_DESC
            END RequisitoBANNER, rownum as n
        FROM SARCHKL
        INNER JOIN STVADMR ON
            SARCHKL_ADMR_CODE = STVADMR_CODE
        WHERE SARCHKL_TERM_CODE_ENTRY = P_TERM_CODE
        AND SARCHKL_PIDM = P_PIDM
        AND SARCHKL_CKST_CODE = 'ENTREGADO'
    ),
    cte2(RequisitoBANNER, n, RequisitoBANNER_Ult) as
    (
        SELECT RequisitoBANNER, n, RequisitoBANNER
        FROM cte
        WHERE n=1
        UNION ALL
        SELECT a.RequisitoBANNER, a.n, b.RequisitoBANNER_Ult || '<br />' || a.RequisitoBANNER
        FROM cte a
        INNER JOIN cte2 b ON
            a.n = b.n + 1
    )
    SELECT NVL(MAX(RequisitoBANNER_Ult),'- Sin registro de requisitos previos')
        INTO P_REQUISITOS_BANNER
    FROM cte2
    ;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_REQUISITOS_PREVIOS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_CAMBIO_ADM (
    P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE	            IN STVTERM.STVTERM_CODE%TYPE,
    P_DEPT_CODE			    IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC             IN STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM               IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MOD_ADM               IN VARCHAR2,
    P_MOD_ADM_DESC          IN VARCHAR2,
    P_CAMBIO_ADM            OUT VARCHAR2,
    P_DEPT_CODE_NEW		    OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE_DEST        OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW		    OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_NEW_DESC         OUT VARCHAR2,
    P_MOD_ADM_NEW           OUT VARCHAR2,
    P_MOD_ADM_NEW_DESC      OUT VARCHAR2,
    P_PROGRAM_NEW           OUT VARCHAR2,
    P_PROGRAM_NEW_DESC      OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
AS
	V_CONTADOR_DEPT     NUMBER := 0;
    V_CONTADOR_CAMP     NUMBER := 0;
    V_CONTADOR_ME       NUMBER := 0;
	V_APEC_TERM         VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);
    V_APEC_CAMP         VARCHAR2(10);
    V_APEC_DEPT_NEW     VARCHAR2(10);
    V_APEC_ME_NEW       VARCHAR2(10);
    V_APEC_CAMP_NEW     VARCHAR2(10);
    V_APEC_MOD_ADM      VARCHAR(10);
    V_APEC_MOD_ADM_NEW  VARCHAR(10);
    V_PROGRAM_DESC      VARCHAR2(100);
BEGIN
    
    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT 
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- CAMPUS
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE;

    -- MODALIDAD DE ADMISION
    SELECT "IDModalidadPostu"
        INTO V_APEC_MOD_ADM
    FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDModRecruiter" = P_MOD_ADM
    AND "departament" = V_APEC_DEPT;
    
    --INDICADOR QUE EXISTE EL CAMBIO DEPT
    SELECT COUNT(*)
        INTO V_CONTADOR_DEPT
    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
    WHERE TRIM("IDDependencia") = 'UCCI'
    AND TRIM("IDPerAcad") = V_APEC_TERM
    AND TRIM("IDEscuelaADM") <> V_APEC_DEPT
    AND "IDAlumno" = P_ID
    AND TRIM("Ingresante") = '0'
    AND TRIM("Renuncia") = '0';
   
    IF V_CONTADOR_DEPT > 0 THEN
        SELECT DISTINCT TRIM("IDEscuela"), "IDModalidadPostu", TRIM("IDEscuelaADM")
            INTO P_PROGRAM_NEW, V_APEC_MOD_ADM_NEW, V_APEC_DEPT_NEW
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE TRIM("IDDependencia") = 'UCCI'
        AND TRIM("IDPerAcad") = V_APEC_TERM
        AND TRIM("IDEscuelaADM") <> V_APEC_DEPT
        AND "IDAlumno" = P_ID
        AND TRIM("Ingresante") = '0'
        AND TRIM("Renuncia") = '0';

        -- DEPARTAMENTO
        SELECT CZRDEPT_CODE
            INTO P_DEPT_CODE_NEW
        FROM CZRDEPT 
        WHERE CZRDEPT_DIVS_CODE = 'UCCI'
        AND CZRDEPT_DEPT_BDUCCI = V_APEC_DEPT_NEW;

        IF P_PROGRAM <> P_PROGRAM_NEW THEN
            -- >> DESC PROGRAM NEW
            SELECT SMRPRLE_PROGRAM_DESC
                INTO P_PROGRAM_NEW_DESC
            FROM SMRPRLE
            WHERE SMRPRLE_PROGRAM = P_PROGRAM_NEW;
            
            ELSE
                -- >> DESC PROGRAM
                SELECT SMRPRLE_PROGRAM_DESC
                    INTO P_PROGRAM_NEW_DESC
                FROM SMRPRLE
                WHERE SMRPRLE_PROGRAM = P_PROGRAM;
        END IF;

        IF V_APEC_MOD_ADM_NEW <> V_APEC_MOD_ADM THEN
            -- >> MODALIDAD DE ADMISION NUEVO
            SELECT "IDModRecruiter"
                INTO P_MOD_ADM_NEW
            FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDModalidadPostu" = V_APEC_MOD_ADM_NEW
            AND TRIM("departament") = V_APEC_DEPT_NEW;
            
            -- >> DESC MODALIDAD DE ADMISION
            SELECT STVADMT_DESC
                INTO P_MOD_ADM_NEW_DESC
            FROM STVADMT
            WHERE STVADMT_CODE = P_MOD_ADM_NEW;
            
            ELSE
                P_MOD_ADM_NEW := P_MOD_ADM;
                P_MOD_ADM_NEW_DESC := P_MOD_ADM_DESC;
        END IF;

        --INDICADOR QUE EXISTE EL CAMBIO CAMP
        SELECT COUNT(*)
            INTO V_CONTADOR_ME
        FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
        INNER JOIN CZRDEPT DE ON
            AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
            AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
        WHERE AE."IDAlumno" = P_ID
        AND AE."IDEscuela" <> V_APEC_DEPT
        ORDER BY AE."FechaInscripcion" DESC;
        
        IF V_CONTADOR_ME > 0 THEN
            SELECT IDEscuelaADM, IDSede
                INTO V_APEC_ME_NEW, V_APEC_CAMP_NEW
            FROM (
                SELECT TRIM(AE."IDEscuela") IDEscuelaADM, TRIM(AE."IDSede") IDSede
                FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
                INNER JOIN CZRDEPT DE ON
                    AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
                    AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
                WHERE AE."IDAlumno" = P_ID
                AND AE."IDEscuela" <> V_APEC_DEPT
                ORDER BY AE."FechaInscripcion" DESC
                ) WHERE ROWNUM = 1;
            
            -- NEW CAMPUS
            SELECT CZRCAMP_CODE
                INTO P_CAMP_CODE_DEST
            FROM CZRCAMP 
            WHERE CZRCAMP_CAMP_BDUCCI = V_APEC_CAMP_NEW;
            
            IF P_CAMP_CODE <> P_CAMP_CODE_DEST THEN
                IF V_APEC_ME_NEW = 'ADV' THEN
                    P_CAMP_CODE_NEW := 'V00';
                ELSE
                    P_CAMP_CODE_NEW := P_CAMP_CODE_DEST;
                END IF;
                V_CONTADOR_CAMP := 1;
            ELSE
                IF V_APEC_ME_NEW = 'ADV' THEN
                    P_CAMP_CODE_NEW := 'V00';
                END IF;
            END IF;

            SELECT STVCAMP_DESC
                INTO P_CAMP_NEW_DESC
            FROM STVCAMP
            WHERE STVCAMP_CODE = P_CAMP_CODE_NEW;

        ELSE

            SELECT IDEscuelaADM, IDSede
                INTO V_APEC_ME_NEW, V_APEC_CAMP_NEW
            FROM (
                SELECT TRIM(AE."IDEscuela") IDEscuelaADM, TRIM(AE."IDSede") IDSede
                FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
                INNER JOIN CZRDEPT DE ON
                    AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
                    AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
                WHERE AE."IDAlumno" = P_ID
                AND AE."IDEscuela" = V_APEC_DEPT
                ORDER BY AE."FechaInscripcion" DESC
                ) WHERE ROWNUM = 1;
            
            -- NEW CAMPUS
            SELECT CZRCAMP_CODE
                INTO P_CAMP_CODE_DEST
            FROM CZRCAMP 
            WHERE CZRCAMP_CAMP_BDUCCI = V_APEC_CAMP_NEW;

            IF P_CAMP_CODE <> P_CAMP_CODE_DEST THEN
                IF V_APEC_ME_NEW = 'ADV' THEN
                    P_CAMP_CODE_NEW := 'V00';
                ELSE
                    P_CAMP_CODE_NEW := P_CAMP_CODE_DEST;
                END IF;
                V_CONTADOR_CAMP := 1;
            END IF;

            SELECT STVCAMP_DESC
                INTO P_CAMP_NEW_DESC
            FROM STVCAMP
            WHERE STVCAMP_CODE = P_CAMP_CODE_NEW;

        END IF;
        
    ELSE

        IF V_APEC_DEPT <> 'ADV' THEN
            --INDICADOR QUE EXISTE EL CAMBIO CAMP
            SELECT COUNT(*)
                INTO V_CONTADOR_CAMP
            FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
            INNER JOIN CZRDEPT DE ON
                AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
                AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
            WHERE AE."IDAlumno" = P_ID
            AND(AE."IDSede") <> V_APEC_CAMP
            AND AE."IDEscuela" = V_APEC_DEPT
            ORDER BY AE."FechaInscripcion" DESC;

            IF V_CONTADOR_CAMP > 0 THEN
                SELECT *
                    INTO V_APEC_CAMP_NEW
                FROM (
                    SELECT TRIM(AE."IDSede")
                    FROM tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE AE
                    INNER JOIN CZRDEPT DE ON
                        AE."IDEscuela" = DE.CZRDEPT_DEPT_BDUCCI
                        AND TRIM(AE."IDDependencia") = DE.CZRDEPT_DIVS_CODE
                    WHERE AE."IDAlumno" = P_ID
                    AND(AE."IDSede") <> V_APEC_CAMP
                    AND AE."IDEscuela" = V_APEC_DEPT
                    ORDER BY AE."FechaInscripcion" DESC)
                WHERE ROWNUM = 1;
                
                SELECT DISTINCT TRIM("IDEscuela"), "IDModalidadPostu", TRIM("IDEscuelaADM")
                    INTO P_PROGRAM_NEW, V_APEC_MOD_ADM_NEW, V_APEC_DEPT_NEW
                FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                WHERE TRIM("IDDependencia") = 'UCCI'
                AND TRIM("IDPerAcad") = V_APEC_TERM
                AND TRIM("IDEscuelaADM") = V_APEC_DEPT
                AND "IDAlumno" = P_ID
                AND TRIM("Renuncia") = '0';

                -- DEPARTAMENTO
                SELECT CZRDEPT_CODE
                    INTO P_DEPT_CODE_NEW
                FROM CZRDEPT 
                WHERE CZRDEPT_DIVS_CODE = 'UCCI'
                AND CZRDEPT_DEPT_BDUCCI = V_APEC_DEPT_NEW;

                -- NEW CAMPUS
                SELECT CZRCAMP_CODE
                    INTO P_CAMP_CODE_DEST
                FROM CZRCAMP 
                WHERE CZRCAMP_CAMP_BDUCCI = V_APEC_CAMP_NEW;

                IF V_APEC_DEPT = 'ADV' THEN
                    P_CAMP_CODE_NEW := 'V00';
                ELSE
                    P_CAMP_CODE_NEW := P_CAMP_CODE_DEST;
                END IF;
            ELSE
                P_CAMP_CODE_DEST := P_CAMP_CODE;
                P_CAMP_CODE_NEW := P_CAMP_CODE;
            END IF;
            
            -- NEW CAMPUS DESC
            SELECT STVCAMP_DESC
                INTO P_CAMP_NEW_DESC
            FROM STVCAMP
            WHERE STVCAMP_CODE = P_CAMP_CODE_NEW;

            IF P_PROGRAM <> P_PROGRAM_NEW THEN
            -- >> DESC PROGRAM NEW
            SELECT SMRPRLE_PROGRAM_DESC
                INTO P_PROGRAM_NEW_DESC
            FROM SMRPRLE
            WHERE SMRPRLE_PROGRAM = P_PROGRAM_NEW;
            
            ELSE
                -- >> DESC PROGRAM
                SELECT SMRPRLE_PROGRAM_DESC
                    INTO P_PROGRAM_NEW_DESC
                FROM SMRPRLE
                WHERE SMRPRLE_PROGRAM = P_PROGRAM;
            END IF;
            
        IF V_APEC_MOD_ADM_NEW <> V_APEC_MOD_ADM THEN
            -- >> MODALIDAD DE ADMISION NUEVO
            SELECT "IDModRecruiter"
                INTO P_MOD_ADM_NEW
            FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDModalidadPostu" = V_APEC_MOD_ADM_NEW
            AND TRIM("departament") = V_APEC_DEPT_NEW;
            
            -- >> DESC MODALIDAD DE ADMISION
            SELECT STVADMT_DESC
                INTO P_MOD_ADM_NEW_DESC
            FROM STVADMT
            WHERE STVADMT_CODE = P_MOD_ADM_NEW;
            
            ELSE
                P_MOD_ADM_NEW := P_MOD_ADM;
                P_MOD_ADM_NEW_DESC := P_MOD_ADM_DESC;
        END IF;

        ELSE
            V_CONTADOR_CAMP := 0;
        END IF;
    END IF;
    
    IF P_PROGRAM <> '502' AND P_PROGRAM_NEW = '502' THEN
        P_CAMBIO_ADM := 'NO';
        P_MESSAGE := 'ERROR! El estudiante no cambiarse a la carrera de Medicina Humana. Por favor, verificar el cambio en RECRUITER.';
        P_DEPT_CODE_NEW := '-';
        P_CAMP_CODE_DEST := '-';
        P_CAMP_CODE_NEW	:= '-';
        P_CAMP_NEW_DESC := '-';
        P_MOD_ADM_NEW := '-';
        P_MOD_ADM_NEW_DESC := '-';
        P_PROGRAM_NEW := '-';
        P_PROGRAM_NEW_DESC := '-';
    ELSE
        IF V_CONTADOR_CAMP > 0 AND V_CONTADOR_DEPT > 0 THEN
            P_CAMBIO_ADM := 'MS';
            P_MESSAGE := 'Cambio de modalidad de estudios y sede';
        ELSIF V_CONTADOR_CAMP > 0 AND V_CONTADOR_DEPT = 0 THEN
            P_CAMBIO_ADM := 'SE';
            P_MESSAGE := 'Cambio de sede';
        ELSIF V_CONTADOR_CAMP = 0 AND V_CONTADOR_DEPT > 0 THEN
            P_CAMBIO_ADM := 'ME';
            P_MESSAGE := 'Cambio de modalidad de estudios';
        ELSE
            P_CAMBIO_ADM := 'NO';
            P_MESSAGE := 'ERROR! El estudiante no tiene registrado su cambio de modalidad de estudios y/o campus. Por favor, verificar el cambio en RECRUITER.';
            P_DEPT_CODE_NEW := '-';
            P_CAMP_CODE_DEST := '-';
            P_CAMP_CODE_NEW	:= '-';
            P_CAMP_NEW_DESC := '-';
            P_MOD_ADM_NEW := '-';
            P_MOD_ADM_NEW_DESC := '-';
            P_PROGRAM_NEW := '-';
            P_PROGRAM_NEW_DESC := '-';
        END IF;
    END IF;
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_CAMBIO_ADM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_USUARIOS_ADMISION (
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CORREO_RRAA           OUT VARCHAR2,
    P_ROL_DOC_RRAA          OUT VARCHAR2,
    P_CORREO_DOC_RRAA       OUT VARCHAR2,
    P_CORREO_BU             OUT VARCHAR2,
    P_CORREO_ADM            OUT VARCHAR2,
    P_ROL_CAJA              OUT VARCHAR2,
    P_CORREO_CAJA           OUT VARCHAR2,
    P_CORREO_CAU            OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Academicos de esa sede.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    V_ROLE_ID                   NUMBER;
    V_ROLE_ID_NEW               NUMBER;
    V_ORG_ID                    NUMBER;
    V_Email_Address             VARCHAR2(100);
    V_ROL_SEDE                  VARCHAR2(100);
    V_ROL_SEDE_NEW              VARCHAR2(100);
    V_ROL                       VARCHAR2(100);
    V_CORREO                    VARCHAR2(4000);
    
    V_CAMP_CODE                 VARCHAR2(4000);
    V_CAMP_CODE_NEW             VARCHAR2(4000);
    
    V_SECCION_EXCEPT            VARCHAR2(50);
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID
        AND ROLE_ID in (V_ROLE_ID,V_ROLE_ID_NEW);
        
    CURSOR C_OFFICE IS
        SELECT 'ADMbienestar' FROM DUAL
        UNION
        SELECT 'ADMregistro' FROM DUAL
        UNION
        SELECT 'cau' FROM DUAL
        UNION
        SELECT 'ADMcaja' FROM DUAL
        UNION
        SELECT 'requisito' FROM DUAL
        UNION
        SELECT 'admision' FROM DUAL
        ;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
    OPEN C_OFFICE;
    LOOP
        FETCH C_OFFICE INTO V_ROL;
        EXIT WHEN C_OFFICE%NOTFOUND;
            
            V_CAMP_CODE := P_CAMP_CODE;
            V_CAMP_CODE_NEW := P_CAMP_CODE_NEW;
            
            IF V_ROL = 'ADMcaja' THEN
                V_CAMP_CODE := 'S01';
                V_CAMP_CODE_NEW := 'S01';
            END IF;

            V_CORREO := NULL;
            V_Email_Address := NULL;
            
            V_ROL_SEDE := V_ROL || V_CAMP_CODE;
            V_ROL_SEDE_NEW := V_ROL || V_CAMP_CODE_NEW;
            -- Obtener el ROL_ID 
            V_SECCION_EXCEPT := 'ROLES';
            
            IF V_ROL IN ('cau', 'requisito', 'admision') THEN
                SELECT DISTINCT ID 
                    INTO V_ROLE_ID
                FROM WORKFLOW.ROLE
                WHERE NAME = V_ROL_SEDE;
                
                SELECT DISTINCT ID 
                    INTO V_ROLE_ID_NEW
                FROM WORKFLOW.ROLE
                WHERE NAME = V_ROL_SEDE_NEW;
            ELSE
                SELECT DISTINCT ID 
                    INTO V_ROLE_ID
                FROM WORKFLOW.ROLE
                WHERE NAME = V_ROL_SEDE_NEW;
                
                V_ROLE_ID_NEW := '';
            END IF;
            
            V_SECCION_EXCEPT := '';
            
            -- Obtener el ORG_ID 
            V_SECCION_EXCEPT := 'ORGRANIZACION';
            
            SELECT ID
                INTO V_ORG_ID
            FROM WORKFLOW.ORGANIZATION
            WHERE NAME = 'Root';
            
            V_SECCION_EXCEPT := '';
            
            -- Obtener los datos de usuarios que relaciona rol y usuario
            V_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
            -- #######################################################################
            OPEN C_ROLE_ASSIGNMENT;
            LOOP
                FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
                EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                    -- Obtener Datos Usuario
                    SELECT Email_Address
                        INTO V_Email_Address
                    FROM WORKFLOW.WFUSER
                    WHERE ID IN (V_ROLE_ASSIGNMENT_REC.USER_ID) ;
                    
                    IF V_Email_Address IS NOT NULL OR V_Email_Address <> '' THEN
                        IF V_CORREO IS NULL OR V_CORREO = '' THEN
                            V_CORREO := V_Email_Address;
                        ELSE
                            V_CORREO := V_CORREO || ',' || V_Email_Address;
                        END IF;
                    END IF;
            
                    IF  V_ROL = 'ADMbienestar' THEN
                        P_CORREO_BU := V_CORREO;
                    ELSIF V_ROL = 'ADMregistro' THEN
                        P_CORREO_RRAA := V_CORREO;
                    ELSIF V_ROL = 'requisito' THEN
                        P_CORREO_DOC_RRAA := V_CORREO;
                        P_ROL_DOC_RRAA := V_ROL_SEDE;
                    ELSIF V_ROL = 'cau' THEN
                        P_CORREO_CAU := V_CORREO;
                    ELSIF V_ROL = 'admision' THEN
                        P_CORREO_ADM := V_CORREO;
                    ELSIF V_ROL = 'ADMcaja' THEN
                        P_CORREO_CAJA := V_CORREO;
                        P_ROL_CAJA := V_ROL_SEDE;
                    END IF;
                    
            END LOOP;
            CLOSE C_ROLE_ASSIGNMENT;
            V_SECCION_EXCEPT := '';

    END LOOP;
    CLOSE C_OFFICE;
          
EXCEPTION
WHEN TOO_MANY_ROWS THEN 
    IF (V_SECCION_EXCEPT = 'ROLES') THEN
        P_MESSAGE := '- Se encontraron mas de un ROL con el mismo nombre: ' || V_ROL || P_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
        P_MESSAGE := '- Se encontraron mas de una ORGANIZACIÓN con el mismo nombre.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
        P_MESSAGE := '- Se encontraron mas de un usuario con el mismo ROL.';
    ELSE
        P_MESSAGE := SQLERRM;
    END IF;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
    
WHEN NO_DATA_FOUND THEN
    IF (V_SECCION_EXCEPT = 'ROLES') THEN
        P_MESSAGE := '- NO se encontró el nombre del ROL: ' || V_ROL || P_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
        P_MESSAGE := '- NO se encontró el nombre de la ORGANIZACION.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
        P_MESSAGE := '- NO  se encontró ningun usuario con esas caracteristicas.';
    ELSE
        P_MESSAGE := SQLERRM;
    END IF; 
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
    
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIOS_ADMISION;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_MATRICULA (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_MAT      OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VERIFICAR_MATRICULA (P_PIDM, P_TERM_CODE, P_STATUS_MAT, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_MATRICULA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_ELIMINAR_NRC (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_ELIMINAR_NRC (P_PIDM, P_TERM_CODE, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_ELIMINAR_NRC;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VALIDAR_CONVA (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_CONVA      OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VALIDAR_CONVA (P_PIDM, P_TERM_CODE, P_STATUS_CONVA, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VALIDAR_CONVA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_PAGOS_CTA_ADM(
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE_NEW		IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_CTA_ADM    OUT VARCHAR2,
    P_CTA_DESC_ADM  	OUT VARCHAR2
)
AS
    V_ERROR             EXCEPTION;
    V_APEC_TERM         VARCHAR2(10);
    V_APEC_CAMP	        VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);
    
    P_RESULT            INTEGER;

BEGIN

    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM 
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;
    
    -- CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE_NEW;
    
    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE_NEW;

    -- VERIFICAR SI TIENE CUENTA CORRIENTE DUPLICADA O NO
    P_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
              'dbo.sp_VerificarCtaAdmisionDup "'
              || 'UCCI' ||'" , "'|| P_ID ||'" , "'|| V_APEC_TERM ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_APEC_DEPT ||'" , "'|| P_CAMBIO_ADM ||'"' 
        );
    COMMIT;
    
    -- OBTENER RESULTADOS
    SELECT "status", "observacion"
        INTO P_STATUS_CTA_ADM, P_CTA_DESC_ADM
    FROM ADM.tblRptaValidacionCtaCteADM@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "pidm" = P_PIDM
        AND "term" = P_TERM_CODE
        AND "idtipoCambioADM" = P_CAMBIO_ADM;
    
    IF P_STATUS_CTA_ADM = '1' THEN
        P_STATUS_CTA_ADM := 'TRUE';
    ELSE
        P_STATUS_CTA_ADM := 'FALSE';
    END IF;
   
EXCEPTION
WHEN V_ERROR THEN 
    P_CTA_DESC_ADM := '- No se ejecuto correctamente el procedimiento correspondiente de verificación de cuentas corrientes de admisión duplicadas.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_CTA_DESC_ADM );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_PAGOS_CTA_ADM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_ACTIVAR_INGRESANTE (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE      	IN STVDEPT.STVDEPT_CODE%TYPE,
    P_DEPT_CODE_NEW 	IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_DEST    IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
)
AS
    V_APEC_TERM         VARCHAR2(10);
    V_APEC_CAMP	        VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);
    
    V_APEC_CAMP_NEW     VARCHAR2(10);
    V_APEC_DEPT_NEW     VARCHAR2(10);
    V_APEC_CAMP_DEST    VARCHAR2(10);

    V_RESULT            INTEGER;

    V_CONTADOR_ING_ANT  INTEGER;
    V_CONTADOR_ING_NEW  INTEGER;

    V_SARADAP_APPL_NO   SARADAP.SARADAP_APPL_NO%TYPE;

    V_N1                DECIMAL(18,2); --NOTA N1 DE GQT Y VIR
    V_N2                DECIMAL(18,2); --NOTA N2 DE GQT Y VIR
    V_N3                DECIMAL(18,2); --NOTA N3 DE GQT Y VIR
    V_N4                DECIMAL(18,2); --NOTA N4 DE GQT Y VIR
    V_N5                DECIMAL(18,2); --NOTA N5 DE GQT Y VIR
    V_N6                DECIMAL(18,2); --NOTA N6 DE GQT Y VIR
    V_N7                DECIMAL(18,2); --NOTA N7 DE GQT Y VIR
    V_RM                DECIMAL(18,2); --NOTA RM DE CR
    V_RV                DECIMAL(18,2); --NOTA RV DE CR
    V_CO                DECIMAL(18,2); --NOTA CO DE CR
    V_PUNTAJE           DECIMAL(18,2); --NOTA DEL PUNTAJE FINAL
    V_FEC_EXAMEN        TIMESTAMP;
    
BEGIN
    --dbms_session.set_nls('nls_date_format', 'mm/dd/yyyy');
    
    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM 
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;
    
    -- CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE;

    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- NEW CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP_NEW
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE_NEW;

    -- NEW CAMPUS DEST (CASOS DE VIRTUAL)
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP_DEST
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE_DEST;

    -- NEW DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT_NEW
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE_NEW;

    IF P_CAMBIO_ADM = 'SE' THEN
        --INDICADOR SI ESTA VIGENTE EL CAMBIO ANTERIOR
        SELECT COUNT(*)
            INTO V_CONTADOR_ING_ANT
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '1'
        AND "Renuncia" = '0';
        
        IF V_CONTADOR_ING_ANT > 0 THEN
            -- DESACTIVAR EL INGRESO EN LA MODALIDAD PARA VOLVER A ACTIVAR EL REGISTRO
            UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                SET "Ingresante" = 0
            WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT
            AND "IDAlumno" = P_ID
            AND "Ingresante" = '1'
            AND "Renuncia" = '0';
            
            COMMIT;
            
        END IF;
              
    ELSE
        
        SELECT COUNT(*)
            INTO V_CONTADOR_ING_ANT
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT
            AND "Ingresante" = '1'
            AND "Renuncia" = '0'
            AND "IDAlumno" = P_ID;

        IF V_CONTADOR_ING_ANT > 0 THEN
            
            SELECT COUNT(*)
                INTO V_CONTADOR_ING_NEW
            FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT_NEW
            AND "IDAlumno" = P_ID;
            --AND "Ingresante" = 0;
            
            IF V_CONTADOR_ING_NEW > 0 THEN
            
                IF V_APEC_DEPT = 'ADM' THEN
                
                    SELECT "RM", "RV", "CO", "Puntaje"
                        INTO V_RM, V_RV, V_CO, V_PUNTAJE
                    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT
                        AND "Ingresante" = '1'
                        AND "IDAlumno" = P_ID;
                    
                    UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                        SET "N1" = V_CO, "N2" = V_CO, "N3" = V_CO, "N4" = V_CO, "N5" = V_CO, "N6" = V_RM, "N7" = V_RV, "Puntaje" = V_PUNTAJE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
                        AND "Ingresante" = '0'
                        AND "IDAlumno" = P_ID;
                    
                    COMMIT;
                    
                ELSIF V_APEC_DEPT_NEW = 'ADM' THEN
                
                    SELECT "N1", "N2", "N3", "N4", "N5", "N6", "N7", "Puntaje"
                        INTO V_N1, V_N2, V_N3, V_N4, V_N5, V_N6, V_N7, V_PUNTAJE
                    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT
                        AND "Ingresante" = '1'
                        AND "IDAlumno" = P_ID;
                    
                    UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                        SET "RM" = V_N6, "RV" = V_N7, "CO" = (V_N1 + V_N2 + V_N3 + V_N4 + V_N5)/5, "Puntaje" = V_PUNTAJE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
                        AND "IDAlumno" = P_ID;
                    COMMIT;
                    
                ELSE
                
                    SELECT "N1", "N2", "N3", "N4", "N5", "N6", "N7", "Puntaje"
                        INTO V_N1, V_N2, V_N3, V_N4, V_N5, V_N6, V_N7, V_PUNTAJE
                    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT
                        AND "Ingresante" = '1'
                        AND "IDAlumno" = P_ID;
                    
                    UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                        SET "N1" = V_N1, "N2" = V_N2, "N3" = V_N3, "N4" = V_N4, "N5" = V_N5, "N6" = V_N6, "N7" = V_N7, "Puntaje" = V_PUNTAJE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
                        AND "IDAlumno" = P_ID;
                    COMMIT;
                    
                END IF;
                
            END IF;

            UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                SET "Renuncia" = 1
            WHERE "IDDependencia" = 'UCCI'
                AND "IDPerAcad" = V_APEC_TERM
                AND "IDEscuelaADM" = V_APEC_DEPT
                AND "Ingresante" = '1'
                AND "Renuncia" = '0'
                AND "IDAlumno" = P_ID;
            COMMIT;

        END IF;
        
    END IF;

    -- Obtener registro maximo del SARADAP
    SELECT MAX(SARADAP_APPL_NO)
        INTO V_SARADAP_APPL_NO
    FROM SARADAP
    WHERE SARADAP_PIDM = P_PIDM
    AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
    AND SARADAP_LEVL_CODE = 'PG';

    -- Actualización del SARADAP
    UPDATE SARADAP
        SET SARADAP_APPL_DATE = SARADAP_APPL_DATE-1
    WHERE SARADAP_PIDM = P_PIDM
    AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
    AND SARADAP_LEVL_CODE = 'PG'
    AND SARADAP_APPL_NO = V_SARADAP_APPL_NO;
    
    COMMIT;

    IF V_APEC_DEPT_NEW = 'ADM' THEN

        SELECT "RM", "RV", "CO", "Puntaje"
            INTO V_RM, V_RV, V_CO, V_PUNTAJE
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '0'
        AND "Renuncia" = '0';

        SELECT TO_DATE("IDExamen",'dd/mm/yy hh24:mi:ss')
            INTO V_FEC_EXAMEN
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '0'
        AND "Renuncia" = '0';
        
        V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
        'ADM.sp_ActivarIngresante "'
        || 'UCCI" , "'|| V_APEC_CAMP_NEW || '" , "' || V_APEC_TERM || '" , "' || TO_CHAR(V_FEC_EXAMEN, 'mm/dd/yyyy') ||'" , "'|| P_ID ||'" , "'
        ||  REPLACE(V_RM, ',', '.') ||'" , "'|| REPLACE(V_RV, ',', '.') || '" , "'|| REPLACE(V_CO, ',', '.') || '" , "'|| REPLACE(V_PUNTAJE, ',', '.') || '" , "'|| 1 || '" , "'|| 1 || '" '
        );
        COMMIT;
  
    ELSIF V_APEC_DEPT_NEW = 'ADG' THEN
   
        SELECT "N1", "N2", "N3", "N4", "N5", "N6", "N7", "Puntaje"
            INTO V_N1, V_N2, V_N3, V_N4, V_N5, V_N6, V_N7, V_PUNTAJE
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT_NEW
            AND "IDAlumno" = P_ID
            AND "Ingresante" = '0'
            AND "Renuncia" = '0';

        SELECT TO_DATE("IDExamen",'dd/mm/yy hh24:mi:ss')
            INTO V_FEC_EXAMEN
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '0'
        AND "Renuncia" = '0';
        
        V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
        'ADM.sp_ActivarIngresante_gqt "UCCI" , "'|| V_APEC_CAMP_NEW || '" , "' || V_APEC_TERM || '" , "' || TO_CHAR(V_FEC_EXAMEN, 'mm/dd/yyyy') ||'" , "'|| P_ID ||'" , "'
        ||  REPLACE(V_N1, ',', '.') ||'" , "'|| REPLACE(V_N2, ',', '.') || '" , "'|| REPLACE(V_N3, ',', '.') || '" , "'|| REPLACE(V_N4, ',', '.') ||
        '" , "'|| REPLACE(V_N5, ',', '.') ||'" , "'|| REPLACE(V_N6, ',', '.') || '" , "'|| REPLACE(V_N7, ',', '.') || '" , "'|| REPLACE(V_PUNTAJE, ',', '.') || '" , "'|| 1 || '" , "'|| 1 || '" '
        );
        COMMIT;

    ELSE

        SELECT NVL("N1",0), NVL("N2",0), NVL("N3",0), NVL("N4",0), NVL("N5",0), NVL("N6",0), NVL("N7",0), NVL("Puntaje",0)
            INTO V_N1, V_N2, V_N3, V_N4, V_N5, V_N6, V_N7, V_PUNTAJE
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT_NEW
            AND "IDAlumno" = P_ID;

        SELECT TO_DATE("IDExamen",'dd/mm/yy hh24:mi:ss')
            INTO V_FEC_EXAMEN
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '0'
        AND "Renuncia" = '0';

        V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
        'ADM.sp_ActivarIngresante_Vir "'
        || 'UCCI" , "'|| V_APEC_CAMP_NEW || '" , "'|| V_APEC_CAMP_DEST || '" , "' || V_APEC_TERM || '" , "' || TO_CHAR(V_FEC_EXAMEN, 'mm/dd/yyyy') ||'" , "'|| P_ID ||'" , "'
        ||  REPLACE(V_N1, ',', '.') ||'" , "'|| REPLACE(V_N2, ',', '.') || '" , "'|| REPLACE(V_N3, ',', '.') || '" , "'|| REPLACE(V_N4, ',', '.') ||
        '" , "'|| REPLACE(V_N5, ',', '.') ||'" , "'|| REPLACE(V_N6, ',', '.') || '" , "'|| REPLACE(V_N7, ',', '.') || '" , "'|| REPLACE(V_PUNTAJE, ',', '.') || '" , "'|| 1 || '" , "'|| 1 || '" '
        );
        COMMIT;

    END IF;

    -- Actualización del SARADAP
    UPDATE SARADAP
        SET SARADAP_APPL_DATE = SARADAP_APPL_DATE+1
    WHERE SARADAP_PIDM = P_PIDM
    AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
    AND SARADAP_LEVL_CODE = 'PG'
    AND SARADAP_APPL_NO = V_SARADAP_APPL_NO;

    COMMIT;

EXCEPTION
WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_ACTIVAR_INGRESANTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_PAGOS_CTA (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE_NEW		IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_CTA        OUT VARCHAR2,
    P_CTA_DESC			OUT VARCHAR2
)
AS
    V_ERROR             EXCEPTION;
    V_CONTADOR          NUMBER;
    V_ATTS_CODE         SGRSATT.SGRSATT_ATTS_CODE%TYPE;
    V_PERCAT            SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE;
    V_NEW_PLAN          VARCHAR2(10);

BEGIN

    -- ATRIBUTO DE PLAN
    SELECT SGRSATT_ATTS_CODE
        INTO V_ATTS_CODE
    FROM (
        SELECT SGRSATT_ATTS_CODE 
        FROM SGRSATT
        WHERE SGRSATT_PIDM = P_PIDM
            AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
            ORDER BY SGRSATT_TERM_CODE_EFF DESC
    )WHERE ROWNUM = 1;

    -- PERIODO DE CATALOGO DE ESTUDIOS
    SELECT SORLCUR_TERM_CODE_CTLG 
        INTO V_PERCAT
    FROM( 
        SELECT SORLCUR_TERM_CODE_CTLG 
        FROM SORLCUR
        WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_ROLL_IND = 'Y'
            AND SORLCUR_CACT_CODE = 'ACTIVE'
            AND SORLCUR_TERM_CODE_END IS NULL
        ORDER BY SORLCUR_SEQNO DESC
    ) WHERE ROWNUM = 1;
    
    --VALIDACION DE PERIODO DE CATALOGO CON EL ATRIBUTO DE PLAN
    IF P_CAMP_CODE_NEW = 'F02' THEN
        V_NEW_PLAN := 'TRUE';
    ELSIF V_ATTS_CODE = 'P018' AND V_PERCAT > '201800' THEN
        V_NEW_PLAN := 'TRUE';
    ELSIF V_ATTS_CODE = 'P015' AND V_PERCAT > '201500' AND V_PERCAT < '201810' THEN
        V_NEW_PLAN := 'FALSE';
    ELSIF V_ATTS_CODE = 'P002' AND V_PERCAT < '201510' THEN
        V_NEW_PLAN := 'FALSE';
    ELSIF V_ATTS_CODE = 'P003' AND V_PERCAT < '201400' AND P_DEPT_CODE_NEW = 'UPGT' THEN
        V_NEW_PLAN := 'FALSE';
    ELSE
        RAISE V_ERROR;
    END IF;
    
    --LLAMA AL SP DEL PAQUETE GENERICO
    BWZKPSPG.P_VERIFICAR_PAGOS_CTA_DUP (P_PIDM, P_ID, P_DEPT_CODE_NEW, P_CAMP_CODE_NEW, P_TERM_CODE, P_PROGRAM_NEW, V_NEW_PLAN, P_CAMBIO_ADM, P_STATUS_CTA, P_CTA_DESC);

EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'El periodo de catálogo no coincide con el atributo de plan de estudios');
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_PAGOS_CTA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_BENEFICIO_PREVIO (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE_NEW		IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_BEN        OUT VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
)
AS
BEGIN

    --LLAMA AL SP DEL PAQUETE GENERICO
    BWZKPSPG.P_VERIFICAR_BENEFICIO_PREVIO (P_PIDM, P_ID, P_DEPT_CODE_NEW, P_CAMP_CODE_NEW, P_TERM_CODE, P_CAMBIO_ADM, P_STATUS_BEN, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_BENEFICIO_PREVIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_REGISTRAR_CAMBIO_ADM (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_DEPT_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_DEPT_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_MOD_ADM           IN VARCHAR2,
    P_MOD_ADM_NEW       IN VARCHAR2,
    P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MESSAGE			OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_REGISTRAR_CAMBIO_ADM (P_PIDM, P_TERM_CODE, P_CAMBIO_ADM, P_DEPT_CODE, P_DEPT_CODE_NEW, P_CAMP_CODE, P_CAMP_CODE_NEW, P_MOD_ADM, P_MOD_ADM_NEW, P_PROGRAM, P_PROGRAM_NEW, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_REGISTRAR_CAMBIO_ADM;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--        


END BWZKSDOD;

/**********************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**********************************************************************************************/