-- 70109442 ---> 435432
/*
SET SERVEROUTPUT ON
DECLARE BOOL VARCHAR2(1);
BEGIN
        BOOL :=  F_RSS_SPGA('435432','0','0');
        DBMS_OUTPUT.PUT_LINE(BOOL);
END;
*/

CREATE OR REPLACE FUNCTION F_RSS_SPGA (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_UA_RSS_SPGA
              "Regla Solicitud de Servicio de SOLICITUD DE PRORROGA GASTOS ADMINISTRATIVOS "
  FECHA     : 06/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : La solicitud disponible solo:
              - Disponible desde el 20 de cada mes hasta el 7 del siguiente mes.
  =================================================================================================================== */
IS
      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL013';
      V_RETUR               BOOLEAN := FALSE;
      V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE; 
      V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;
      V_PROGRAM             SORLCUR.SORLCUR_PROGRAM%TYPE;
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM                SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
      V_NRC_N               INTEGER;

      V_FEC_PRORRG_INI      DATE;
      V_FEC_PRORRG_FIN      DATE;
      
      V_APEC_CAMP           VARCHAR2(9);
      V_APEC_TERM           VARCHAR2(9);
      V_ROWNUM              NUMBER;
      V_IDSECCIONC          VARCHAR2(18);
      V_CONCEPTO            VARCHAR2(9);
      V_FECCARGO            DATE;
      V_PRORROGA            VARCHAR2(50);

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR GET_DATACURR_C IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                SORLCUR_PROGRAM
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM 
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM --> SFRRSTS)
      CURSOR GET_PTRM_C IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;


      -- OBTENER PERIODO ACTIVO (SOATERM) inicio a fin
      CURSOR SOBPTRM_C IS
      SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE FROM
      (
        SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, MIN(SOBPTRM_START_DATE) SOBPTRM_START_DATE, MAX(SOBPTRM_END_DATE) SOBPTRM_END_DATE
        FROM SOBPTRM
        WHERE SOBPTRM_PTRM_CODE IN (V_SUB_PTRM||'1' , (V_SUB_PTRM || CASE WHEN (V_CODE_DEPT ='UVIR' OR V_CODE_DEPT ='UPGT') THEN '2' ELSE '-' END) ) 
        AND SOBPTRM_PTRM_CODE NOT LIKE '%0'
        GROUP BY SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
      ) WHERE ROWNUM = 1
      AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SOBPTRM_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SOBPTRM_END_DATE
      ORDER BY SOBPTRM_TERM_CODE DESC;  


      -- Validar si tiene NRC activo en un determinado periodo (***Se esta filtrando parte de periodo para los casos de modulos de UVIR Y UPGT)
      CURSOR SFRSTCR_N_C IS
      SELECT COUNT(*) NRC_ON FROM SFRSTCR
      WHERE SFRSTCR_TERM_CODE = V_CODE_TERM
            AND SFRSTCR_PIDM = P_PIDM
            AND SFRSTCR_PTRM_CODE = V_PTRM
            AND SFRSTCR_RSTS_CODE IN ('RW','RE');

      -- GET FECHAS DE cARGO Y los conceptos validos para las ProrrogaS
        -- UREG : C02,C03,C04
        -- UPGT : C02,C03
      CURSOR GET_FCARGO_C IS
      SELECT rownum, IDSeccionC, IDConcepto, FecCargo, Prorroga
      FROM (
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = V_APEC_CAMP
                      AND "IDPerAcad" = V_APEC_TERM
                      AND "IDEscuela" = V_PROGRAM
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                      AND "IDSeccionC" <> '15NEX1A'
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO           
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE = V_PTRM
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM
                      )
              )
          SELECT "IDSeccionC" IDSeccionC,"IDConcepto" IDConcepto, "FecCargo" FecCargo, "Prorroga" Prorroga 
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = V_APEC_CAMP
          AND "IDPerAcad"   = V_APEC_TERM
          AND "IDEscuela"   = V_PROGRAM
          AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
          AND "Prorroga"    = '0'
          AND "IDConcepto"  IN ('C02','C03', CASE WHEN V_CODE_DEPT = 'UREG' THEN 'C04' ELSE '-' END) 
      );

BEGIN  
--    
    -- >> GET DEPARTAMENTO, CAMPUS y PROGRAM --
    OPEN GET_DATACURR_C;
    LOOP
        FETCH GET_DATACURR_C INTO V_CODE_CAMP,V_CODE_DEPT,V_PROGRAM ;
        EXIT WHEN GET_DATACURR_C%NOTFOUND;
    END LOOP;
    CLOSE GET_DATACURR_C;

    -- GET DATOS ALUMNO
    -- P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);


    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN GET_PTRM_C;
    LOOP
        FETCH GET_PTRM_C INTO V_SUB_PTRM;
        EXIT WHEN GET_PTRM_C%NOTFOUND;
    END LOOP;
    CLOSE GET_PTRM_C;


    ------------------------------------------------------------
    -- >> Obteniendo PERIODO ACTIVO Y LA PARTE PERIODO  --
    OPEN SOBPTRM_C;
      LOOP
        FETCH SOBPTRM_C INTO P_CODE_TERM, V_PTRM;
        IF SOBPTRM_C%FOUND THEN
            V_CODE_TERM := P_CODE_TERM;
        ELSE EXIT;
      END IF;
      END LOOP;
    CLOSE SOBPTRM_C;


    ---------------------------------------------------------------------------------------------
    -- >> Validar si el estudiante esta estudiando en el periodo actual activo (TAMBIEN VALIDA por modulo PARTE PERIODO)
    OPEN SFRSTCR_N_C;
    LOOP
      FETCH SFRSTCR_N_C INTO V_NRC_N;
      EXIT WHEN SFRSTCR_N_C%NOTFOUND;
    END LOOP;
    CLOSE SFRSTCR_N_C;   
  
    IF(V_NRC_N = 0) THEN RETURN 'N'; END IF;


    ----------------------------------------------------------------------------------
    -- GET FECHAS DE cARGO Y los conceptos validos para las ProrrogaS
        -- UREG : C02,C03,C04
        -- UPGT : C02,C03

        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = V_CODE_CAMP;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = V_CODE_TERM;-- PERIODO
        DBMS_OUTPUT.PUT_LINE(SYSDATE-30);
        OPEN GET_FCARGO_C;
            LOOP 
                FETCH GET_FCARGO_C INTO V_ROWNUM, V_IDSECCIONC, V_CONCEPTO, V_FECCARGO, V_PRORROGA;
                IF GET_FCARGO_C%FOUND THEN
                    ------------------ LAS REGLAS PARA LOS DEPARTAMENTOS YA SE FILTRARON EN LA CONSULTA -----------------
                    -- En la regla de caja y bienestar la fecha de vencimiento de pago es el 4 de cada mes a una pension.
                    V_FEC_PRORRG_INI := TO_DATE(('20/' || TO_CHAR(ADD_MONTHS(V_FECCARGO, -1), 'mm/yyyy')),'dd/mm/yyyy');
                    V_FEC_PRORRG_FIN := TO_DATE(('07/' || TO_CHAR(V_FECCARGO,'mm/yyyy')),'dd/mm/yyyy') + 1;
                    IF (V_FEC_PRORRG_INI <= (SYSDATE) AND (SYSDATE) < V_FEC_PRORRG_FIN) THEN
                        RETURN 'Y';
                    END IF;
                    ------------------------------------------------------------
                ELSE EXIT;
            END IF;
            END LOOP;
        CLOSE GET_FCARGO_C;

    RETURN 'N';
--
END F_RSS_SPGA;