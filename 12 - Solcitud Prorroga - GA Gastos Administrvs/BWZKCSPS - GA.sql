/**********************************************************************************************/
/* BWZKCSPS.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripcion corta: Script para generar el Paquete de automatizacion del proceso de         */
/*                    Solicitud de Prorroga de pagos.                                         */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creacion del Codigo.                                        RML             12/JUL/2017 */
/*    --------------------                                                                    */
/*    Creacion del paquete de Reserva de Matricula On Demand                                  */
/*    --Paquete generico                                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX).         */
/*    Procedure P_OBTENER_COMENTARIO_ALUMNO: Obtiene EL COMENTARIO como unico dato ingresado  */
/*              por el estudiante.                                                            */
/*    Procedure P_GETDATAX_CARGO: Obtiene los datos de la CTA alumno (conceptos) PARA realizar*/
/*              la prorroga.                                                                  */
/*    Procedure P_SETPRORROGA_GA: Registra la prorroga para la cuota del alumno validando que */
/*              no exista una anterior.Retira la penalidad de Gasto Administrativo en caso se */
/*              haya generado.                                                                */
/*    Procedure P_SIDEUDA_CUOTA: Verifica la existencia de deuda para un APEC_IDAlumno Y      */
/*              CONCEPTO DETERMINADO.                                                         */    
/*    Procedure P_CALC_DIAS: Calcula los dias restante a una fecha. Devuelve                  */ 
/*                           0- Si estamos a mas de 2 de la fecha limite de prorroga.         */
/*                           1- Si estamos a 1 o a cero dias de la fecha limite de prorroga.  */  
/*                           2- Si ya estamos en la fecha o posterior a la fecha limite de    */
/*                              prorroga                                                      */
/*                                                                                            */
/* 2. Actualizacion P_GETDATAX_CARGO                            LAM               10/APR/2018 */
/*    Se cambia la validacion de la seccion de caja por length(IDSeccionC)=5.                 */
/* 3. Actualizacion P_SETPRORROGA_GA                            BFV               12/JUN/2018 */
/*    Se cambia la forma de busqueda para el concepto de Gasto Administrativo (GA_ por G__).  */ 
/* 4. Actualizacion Paquete generico                            BFV               15/OCT/2018 */
/*    Se cambia el SP P_CAMBIAR_ESTADO usando un paquete generico de donde consumira la info. */
/*    Actualizacion P_GETDATAX_CARGO                                                          */
/*    Se restringe la regla para que solo se aplique a estudiantes con estado M.              */
/*    Actualizacion Consultas de fecha                          BFV               15/OCT/2018 */
/*    Se actualiza consultas de fechas para que sea mas agil y rapido.                        */
/* 5. Actualizacion Consulta                                    BFV               01/FEB/2019 */
/*    Se actualiza la consulta para obtener el campo PARTE PERIODO de todo el paquete.        */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/

-- PERMISOS DE EJECUCION
/**********************************************************************************************/
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/
CREATE OR REPLACE PACKAGE BWZKCSPS AS
    
PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
            P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
            P_AN                  OUT NUMBER,
            P_ERROR               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
            P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_COMENTARIO          OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_GETDATAX_CARGO (
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
            P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
            P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_APEC_IDALUMNO       OUT VARCHAR2,
            P_PROGRAM             OUT SORLCUR.SORLCUR_PROGRAM%TYPE,
            P_PRORR_CONCEPTO      OUT VARCHAR2,
            P_PRORR_SECCIONC      OUT VARCHAR2,
            P_FECCARGO            OUT VARCHAR2,
            P_FECINIC             OUT VARCHAR2,
            P_FEC_PRORRG_CARGO    OUT VARCHAR2 -- REGLA: LA FECHA LIMITE DE LA PRORROGA PARA EL PAGO DE LA CUOTA ES EL 15/MM/YYYY
      );

--------------------------------------------------------------------------------

PROCEDURE P_SETPRORROGA_GA (
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
            P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
            P_APEC_IDALUMNO       IN VARCHAR2,
            P_PROGRAM             IN SORLCUR.SORLCUR_PROGRAM%TYPE,
            P_PRORR_CONCEPTO      IN VARCHAR2,
            P_PRORR_SECCIONC      IN VARCHAR2,
            P_FECINIC             IN VARCHAR2,
            P_FEC_PRORRG_CARGO    IN VARCHAR2, -- REGLA: LA FECHA LIMITE DE LA PRORROGA PARA EL PAGO DE LA CUOTA ES EL 15/MM/YYYY
            P_COMENTARIO          IN VARCHAR2, -- EN APEC SOLO PERMITE 200 CARACTERES
            P_MESSAGE             OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_SIDEUDA_CUOTA (
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
            P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
            P_APEC_IDALUMNO       IN VARCHAR2,
            P_PROGRAM             IN SORLCUR.SORLCUR_PROGRAM%TYPE,
            P_PRORR_CONCEPTO      IN VARCHAR2, -- APEC_CONCEPTO
            P_PRORR_SECCIONC      IN VARCHAR2, -- APEC_SECCIONC
            P_FECINIC             IN VARCHAR2, -- APEC_CTA_FECINI 
            P_DEUDA               OUT VARCHAR2,
            P_MESSAGE             OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_CALC_DIAS (
            P_FEC_PRORRG_CARGO    IN VARCHAR2, -- FECHA a cual se calcula los dias restantes(SYSDATE).
            P_CODIGO              OUT NUMBER
      );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END BWZKCSPS;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKCSPS;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKCSPS FOR BANINST1.BWZKCSPS;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKCSPS
--  START gurgrth BWZKCSPS
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/