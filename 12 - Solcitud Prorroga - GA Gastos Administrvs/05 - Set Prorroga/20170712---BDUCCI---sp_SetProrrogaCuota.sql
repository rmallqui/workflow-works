
/**************************************************************
    PERMISOS    SP de BDUCCI ---------------> AOPEC
**************************************************************/

--  grant execute on [dbo].[sp_SetProrrogaCuota]  to [SSLSCDataCBanner]

/**************************************************************/


USE [BDUCCI]
GO
  /* ===================================================================================================================
  NOMBRE    : [dbo].[sp_SetProrrogaCuota] 
  FECHA     : 10/07/2017
  AUTOR     : Richard Mallqui Lopez
  OBJETIVO  : Registra una prorroga y Retira el Cargo Administrativo validando y solo en caso que ya venció su cuota.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
ALTER PROCEDURE [dbo].[sp_SetProrrogaCuota] 
    @c_IDAlumno     varchar(15),
    @c_IDSede       varchar(9),
    @c_IDDependencia    varchar(9),
    @c_IDPerAcad      varchar(6),
    @c_IDSccionc      varchar(18),
    @c_IDConcepto     varchar(5),
    @c_IDEscuela      varchar(5),
    @c_GAConcepto     varchar(5),
    @c_GACosto      varchar(10),
    @c_Modalidad      varchar(6),
    @c_FecInic      varchar(15),
    @c_FecProrrogCargo  varchar(15),
    @c_IDUsuario      varchar(50),
    @c_Comentario     varchar(200)
AS
DECLARE 
  @c_prorroga         Char(1),
  @c_DATE_FecInic       datetime,
  @c_DATE_FecProrrogCargo   datetime
BEGIN
------ 
  SET @c_prorroga       = '1'
  SET @c_DATE_FecInic     = CONVERT(DATETIME, @c_FecInic,103)
  SET @c_DATE_FecProrrogCargo = CONVERT(DATETIME, @c_FecProrrogCargo,103)

  --variables de procedimiento
  SET NOCOUNT ON;
  SET FMTONLY OFF
  BEGIN TRY
    BEGIN TRANSACTION
      
    IF EXISTS( -- Verificar que no tenga prorroga para dicho concepto.
    SELECT * FROM tblctacorriente WHERE IDAlumno = @c_IDAlumno AND IDPerAcad = @c_IDPerAcad and iddependencia = @c_IDDependencia AND
    IDSede = @c_IDSede AND IDEscuela = @c_IDEscuela AND IDSeccionC = @c_IDSccionc AND FecInic = @c_DATE_FecInic AND fecprorroga  IS NULL 
    AND IDConcepto = @c_IDConcepto AND Prorroga = '0')
    BEGIN 
        
        -- Actualizar la CUOTA en CTA Corriente (prorroga y la fecha prorroga)
        UPDATE tblctacorriente
        SET prorroga = @c_prorroga, fecprorroga = @c_DATE_FecProrrogCargo
        WHERE idsede    = @c_IDSede
        and iddependencia = @c_IDDependencia
        and idalumno    = @c_IDAlumno
        and idperacad   = @c_IDPerAcad
        and idseccionc    = @c_IDSccionc
        and fecinic     = @c_DATE_FecInic
        and idconcepto    = @c_IDConcepto
        and idescuela   = @c_IDEscuela


        -- Descontar el Gasto Administrativo en caso ya tenga deuda (25)
        UPDATE tblctacorriente
        SET Cargo = Cargo -  CAST(@c_GACosto AS INT) 
        WHERE idsede    = @c_IDSede
        and iddependencia = @c_IDDependencia
        and idalumno    = @c_IDAlumno
        and idperacad   = @c_IDPerAcad
        and idseccionc    = @c_IDSccionc
        and fecinic     = @c_DATE_FecInic
        and idconcepto    = @c_GAConcepto
        and idescuela   = @c_IDEscuela
        and FecCargo    < GETDATE()
        and Deuda >=  CAST(@c_GACosto AS INT) 

        -- Insertar el Registro de de la PRORROGA
        INSERT INTO tblAlumnoProrroga (IDAlumno,IDPerAcad,IDDependencia,IDSeccionC,FecInic,IDConcepto,IDFecProrroga,Motivo,IDUsuario,Valido,FechaRegistro)
        VALUES (@c_IDAlumno,@c_IDPerAcad,@c_IDDependencia,@c_IDSccionc,@c_DATE_FecInic,@c_IDConcepto,@c_DATE_FecProrrogCargo,@c_Comentario,@c_IDUsuario,'1',GETDATE())
    END
    ELSE
        RAISERROR (15600,-1,-1,'Inconsistencia detectada: La prorroga ya fue generada anteriormente.'); 

    COMMIT TRANSACTION
  END TRY
  BEGIN CATCH

        --INSTRUCCIONES EN CASO DE ERRORES
        DECLARE @ErrorMessage NVARCHAR(4000);  
        DECLARE @ErrorSeverity INT;  
        DECLARE @ErrorState INT;  

        SELECT   
            @ErrorMessage = ERROR_MESSAGE(),  
            @ErrorSeverity = ERROR_SEVERITY(),  
            @ErrorState = ERROR_STATE();  

        -- Use RAISERROR inside the CATCH block to return error  
        -- information about the original error that caused  
        -- execution to jump to the CATCH block.  
        RAISERROR (
            @ErrorMessage, -- Message text.  
            @ErrorSeverity, -- Severity.  
            @ErrorState -- State.  
            );  
    
    ROLLBACK TRANSACTION
  END CATCH
END

GO 

grant execute on [dbo].[sp_SetProrrogaCuota]  to [SSLSCDataCBanner]