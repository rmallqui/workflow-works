


/**************************************************************
        SP de BDUCCI ---------------> AOPEC
**************************************************************/
PERMISOS grant execute on [dbo].[sp_SetProrrogaCuota]  to [SSLSCDataCBanner]
/**************************************************************/




USE [BDUCCI]
GO
  /* ===================================================================================================================
  NOMBRE    : [dbo].[sp_SetProrrogaCuota] 
  FECHA   : 10/07/2017
  AUTOR   : Richard Mallqui Lopez
  OBJETIVO  : Registra una prorroga y Retira el Cargo Administrativo validando y solo en caso que ya venció su cuota.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
create PROCEDURE [dbo].[sp_SetProrrogaCuota] 
  @c_IDAlumno     varchar(15),
  @c_IDSede     varchar(9),
  @c_IDDependencia  varchar(9),
  @c_IDPerAcad    varchar(6),
  @c_IDSccionc    varchar(18),
  @c_IDConcepto   varchar(5),
  @c_IDEscuela    varchar(5),
  @c_cargoAdm     varchar(5),
  @c_Modalidad    varchar(6),
  @c_FecInic      datetime,
  @c_FecCargo     datetime,
  @c_FecProrrogCargo  datetime,
  @c_IDUsuario    varchar(50),
  @c_Comentario   varchar(200),
  @c_Respuesta    char(1) out
AS
DECLARE 
  @c_prorroga     char(1),
  @c_IDConcpetoGA   VARCHAR(5)
BEGIN
  
  SET @c_prorroga   = '1'
  SET @c_IDConcpetoGA = 'GA' + RIGHT(RTRIM(@c_IDConcepto), 1)

  --variables de procedimiento
  SET NOCOUNT ON;
  SET FMTONLY OFF
  BEGIN TRY
    BEGIN TRANSACTION
      
      -- Actualizar la CUOTA en CTA Corriente (prorroga y la fecha prorroga)
      UPDATE tblctacorriente
      SET prorroga = @c_prorroga, fecprorroga = @c_FecProrrogCargo
      WHERE idsede    = @c_IDSede
      and iddependencia = @c_IDDependencia
      and idalumno    = @c_IDAlumno
      and idperacad   = @c_IDPerAcad
      and idseccionc    = @c_IDSccionc
      and fecinic     = @c_FecInic
      and idconcepto    = @c_IDConcepto
      and idescuela   = @c_IDEscuela


      -- Descontar el Gasto Administrativo en caso ya tenga deuda (25)
      UPDATE tblctacorriente
      SET Cargo = Cargo - 25
      WHERE idsede    = @c_IDSede
      and iddependencia = @c_IDDependencia
      and idalumno    = @c_IDAlumno
      and idperacad   = @c_IDPerAcad
      and idseccionc    = @c_IDSccionc
      and fecinic     = @c_FecInic
      and idconcepto    = @c_IDConcpetoGA
      and idescuela   = @c_IDEscuela
      and FecCargo    < GETDATE()
      and Deuda >= 25


      -- Insertar el Registro de de la PRORROGA
      INSERT INTO tblAlumnoProrroga (IDAlumno,IDPerAcad,IDDependencia,IDSeccionC,FecInic,IDConcepto,IDFecProrroga,Motivo,IDUsuario,Valido,FechaRegistro)
      VALUES (@c_IDAlumno,@c_IDPerAcad,@c_IDDependencia,@c_IDSccionc,@c_FecInic,@c_IDConcepto,@c_FecProrrogCargo,@c_Comentario,@c_IDUsuario,'1',GETDATE())

      set @c_Respuesta = '1'

    COMMIT TRANSACTION
  END TRY
  BEGIN CATCH
      --INSTRUCCIONES EN CASO DE ERRORES
      set @c_Respuesta = '0'
    ROLLBACK TRANSACTION
  END CATCH
END