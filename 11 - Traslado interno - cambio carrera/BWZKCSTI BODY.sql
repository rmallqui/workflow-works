/**********************************************************************************************/
/* BWZKCSTI.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Traslado Interno.                                          */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        RML             04/ENE/2017 */
/*    --------------------                                                                    */
/*    Creación del paquete de Traslado Interno                                                */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante cumpla las condiciones básicas   */
/*              para acceder para el WF.                                                      */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el procedimiento LOS CORREOS del  */
/*              responsable(s)de Registros Academicos de esa sede.                            */          
/*    Procedure P_GETUSER_COORDINADOR: Obtiene el usuario WF del cordinador,.                 */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Valida si el estudiante anulo la solicitud.     */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: cambia el estado de la solicitud (XXXXXX).         */
/*    Procedure P_SET_CODDETALLE: genera una deuda por CODIGO DETALLE de ls SeccionC "DTA".   */
/*    Procedure P_GET_SIDEUDA_DETLS: Verificar en base a un PIDM y el Num. Transaccion        */
/*              verifique la existencia de deuda antes a la fecha actual.                     */
/*    Procedure P_SET_ATRIBUTO_PLANS : SET ATRIBUTO de PLAN ESTUDIOS a un periodo determinado.*/
/*    Procedure P_DEL_CODDETALLE: genera una deuda por CODIGO DETALLE.                        */
/*    Procedure F_VALIDAR_FECHA_DISPONIBLE: Valida que la fecha ENVIADA este dentro de las 48 */
/*              horas para su atencion.                                                       */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene EL PROGRAMA, PROGRAM_DESC, COMENTARIO Y TELEFONO*/ 
/*              de una solicitud especifica.                                                  */
/*    Procedure P_GET_CORDINADOR_PROGRAM: A traves de un codigo de carrera Ejm "110" se       */
/*              obtiene los datos del coordinador.                                            */
/*    Procedure P_SET_CAMBIAR_CARRERA: Cambia la carrera al estudiante.                       */
/*    Procedure P_CREATE_CTACORRIENTE: Asigna escala y crea la CTA Corriente de un alumno.    */
/*    Procedure P_SET_CARGO_CONCEPTO: APEC-BDUCCI: Crea un registro al estudiante con el      */  
/*              CONCEPTO.                                                                     */
/*    Procedure P_GET_ABONO_CTA: Obtiene el abono en una cta corriente especifica.            */
/*    Procedure P_REMOVE_CTA: Desabilita la cta corriente especifica por PIDM.                */
/*    Procedure P_SET_CURSO_INTROD: Asigna los cursos introducctorios de RM y RV con nota 11  */
/*              solo en caso no los tubiera alguno.                                           */    
/*    Procedure P_VERIF_CTLG_CAMBIO: Verifica el si el plan de estudio se mantiene o cambia.  */
/*    Procedure P_STI_CONV_ASIGN: Convalida asignaturas del plan 2007 al 2015.                */
/*    Procedure P_STI_EXECCAPP: Ejecucion de CAPP.                                            */
/*    Procedure P_Sti_Execproy: Ejecucion de PROYECCION.                                      */
/*    Procedure P_GET_ASIG_RECONOCIDAS: Obtiene la lista de asignaturas reconocidas.          */
/*    Procedure P_SET_ATRIBUTO_PLANS: SET ATRIBUTO de PLAN ESTUDIOS a un periodo determinado. */
/*                                                                                            */
/* 2. Actualización P_STI_CONV_ASIGN                           LAM/HGH            02/MAR/2018 */
/*    Se agrega este paquete ya que ahora el cambio de plan de estudio sera una convalidación */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
/**********************************************************************************************/
-- PERMISOS DE EJECUCION
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/
CREATE OR REPLACE PACKAGE BODY BWZKCSTI AS

PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0; --VALIDA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VALIDA RETENCIÓN 02
        V_SOLPER            INTEGER := 0; --VALIDA SOLICITUD PERMANENCIA DE PLAN
        V_SOLRES            INTEGER := 0; --VALIDA SOLICITUD RESERVA
        V_SOLREC            INTEGER := 0; --VALIDA SOLICITUD RECTIFICACION
        V_SOLCAM            INTEGER := 0; --VALIDA SOLICITUD CAMBIO DE PLAN
        V_SOLTRA            INTEGER := 0; --VALIDA SOLICITUD TRASLADO INTERNO FINALIZADO
        V_SOLDIR            INTEGER := 0; --VALIDA SOLICITUD ASIGNATURA DIRIGIDA
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
        V_BECA18            INTEGER := 0; --VERIFICA SI ES BECA 18
        P_APEC_TERM         VARCHAR2(4000);
        
        P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL013';

        V_CODE_DEPT         STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_TERM         STVTERM.STVTERM_CODE%TYPE;
        V_CODE_CAMP         STVCAMP.STVCAMP_CODE%TYPE;
        V_PROGRAM           SORLCUR.SORLCUR_PROGRAM%TYPE;
        V_TERMCTLG          SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';

        P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
        V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        V_PTRM                SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
        V_CREDITOS            INTEGER;
        V_CICLO               INTEGER;
        V_NRC_N               INTEGER;
        V_PRIORIDAD           NUMBER;
        V_INDICADOR           NUMBER;

    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS ON 
                SORLCUR_PIDM = SORLFOS_PIDM 
                AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM = P_PIDM 
                AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;

    --################################################################################################
    -- Calculando parte PERIODO (sub PTRM)
    --################################################################################################
    CURSOR C_SFRRSTS_PTRM IS
        SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2) SUBPTRM
        FROM CZRPTRM 
        WHERE CZRPTRM_DEPT = V_CODE_DEPT
        AND CZRPTRM_CAMP_CODE = V_CODE_CAMP;

    --################################################################################################
    /*   GET TERM (PERIODO - Solo usando parte de periodo 1) 
         A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
         y UNA SEMANA despues DE INICIO DE CLASES(SOBPTRM_START_DATE+7)
    */
    --################################################################################################
    CURSOR C_SOBPTRM IS
        SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
        FROM (
          -- Forma SOATERM - SFARSTS ---> fechas para las partes de periodo
            SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
            FROM SOBPTRM
            INNER JOIN SFRRSTS ON
                SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
            WHERE SFRRSTS_RSTS_CODE = 'RW'
                AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
                AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
                AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (ADD_MONTHS(SOBPTRM_START_DATE,1))
            ORDER BY SOBPTRM_TERM_CODE DESC
        ) WHERE ROWNUM <= 1;
        
    -- Validar si tiene NRC activo en un determinado periodo (*** No se esta filtrando parte de periodo para los casos de modulos de UVIR Y UPGT)
    CURSOR C_SFRSTCR_N IS
        SELECT COUNT(*) NRC_ON 
        FROM SFRSTCR
        WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
            AND SFRSTCR_PIDM = P_PIDM
            AND SFRSTCR_RSTS_CODE IN ('RW','RE');
      
BEGIN
    -- APEC TERM
    SELECT CZRTERM_TERM_BDUCCI
        INTO P_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE ;-- PERIODO
    
    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
    END LOOP;
    CLOSE C_SORLCUR;
    
    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
        INTO V_RET01
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SPRHOLD_TO_DATE;
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SPRHOLD_TO_DATE;
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    --- VALIDACIÓN DE ESTUDIANTES DE PROGRAMA 309 (Administración - MKT y Neg. Internacionales)
    --################################################################################################
    IF V_PROGRAM = '309' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. No esta habilitado para este flujo. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. No esta habilitado para este flujo. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLPER > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permamencia de Plan de Estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permamencia de Plan de Estudios activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    -- >> VALIDACION: SOLO se puede un solo TRASLADO INTERNO por PERIODO.
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR 
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE = 'CO'; -- Estado 'CO'-Completado
        
    IF V_SOLTRA > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno ya finalizada. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno ya finalizada. ';
        END IF;
        V_FLAG := 1;
    END IF;

    -- DATOS ALUMNO
    BWZKWFFN.P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);
    ------------------------------------------------------------
--    -- >> calculando SUB PARTE PERIODO  --
--    OPEN C_SFRRSTS_PTRM;
--    LOOP
--      FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
--      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
--    END LOOP;
--    CLOSE C_SFRRSTS_PTRM;
--
--    -- PARTE DE PERIODOS
--    V_SUB_PTRM_1 := V_SUB_PTRM || '1';
--    V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (V_CODE_DEPT ='UVIR' OR V_CODE_DEPT ='UPGT') THEN '2' ELSE '-' END;
--
--    ------------------------------------------------------------
--    -- >> Obteniendo PERIODO ACTIVO Y LA PARTE PERIODO  --
--    V_CODE_TERM := NULL;
--    OPEN C_SOBPTRM;
--      LOOP
--            FETCH C_SOBPTRM INTO V_CODE_TERM, V_PTRM;
--            IF C_SOBPTRM%FOUND THEN
--                V_CODE_TERM := P_TERM_CODE;
--            ELSE EXIT;
--      END IF;
--      END LOOP;
--    CLOSE C_SOBPTRM;
--    
--    IF V_CODE_TERM IS NULL THEN
--        P_STATUS_APTO := 'FALSE';
--        P_REASON := 'Estudiante no esta dentro de las fechas de inicio de matricula y la primera semana de clases.';
--        RETURN;
--    END IF;
--
--
--    -- VALIDAR MODALIDAD Y PRIORIDAD
--    BWZKWFFN.P_CHECK_DEPT_PRIORIDAD(P_PIDM, P_CODIGO_SOL, V_CODE_DEPT,NULL,V_CODE_CAMP, V_PRIORIDAD);
--    
--    IF (V_PRIORIDAD <> 3 AND V_PRIORIDAD <> 5 AND V_PRIORIDAD <> 8) THEN
--        P_STATUS_APTO := 'FALSE';
--        P_REASON := 'Estudiante no esta dentro de las fechas de solicitud';
--        RETURN;
--    END IF;

    --################################################################################################
    -- >> Validar si el estudiante esta estudiando en el periodo actual activo (TAMBIEN VALIDA por modulo PARTE PERIODO)
    --################################################################################################
    OPEN C_SFRSTCR_N;
    LOOP
      FETCH C_SFRSTCR_N INTO V_NRC_N;
      EXIT WHEN C_SFRSTCR_N%NOTFOUND;
    END LOOP;
    CLOSE C_SFRSTCR_N;    

    IF V_NRC_N > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene matricula activa en el periodo solicitado. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene matricula activa en el periodo solicitado. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    --OBTENER CREDITOS Y CICLO
    --################################################################################################

        BWZKWFFN.P_CREDITOS_CICLO(P_PIDM,V_CREDITOS,V_CICLO);

    --################################################################################################    
    ---MINIMO 72 CREDITOS PARA ACCEDER
    --################################################################################################
    IF V_CREDITOS < 72 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No tiene los créditos solicitados (mínimo 72 créditos aprobados). ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No tiene los créditos solicitados (mínimo 72 créditos aprobados). ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE BECA 18
    --################################################################################################
        WITH
        CTE_tblPersonaAlumno AS (
            SELECT "IDAlumno" IDAlumno 
            FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDPersona" IN ( 
                SELECT "IDPersona"
                FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE
                WHERE "IDPersonaN" = P_PIDM
                )
        )
        SELECT COUNT(*)
            INTO V_BECA18
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno" IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
            AND "IDPerAcad" = P_APEC_TERM
            AND LENGTH("IDSeccionC") = 5
            AND SUBSTRB("IDSeccionC",-2,2) = '20'
            AND "Estado" = 'M'
            AND "IDConcepto" in ('C01','C02','C03','C04','C05'); -- CUALQUIER CONCEPTO seguro SOLO PARA OBTENER LOS DATOS IDALUMNO Y SECCIONC

    IF V_BECA18 > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Es estudiante de Beca 18.';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Es estudiante de Beca 18.';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_APTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_USUARIO (
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
    P_CORREO              OUT VARCHAR2,
    P_ROL_SEDE            OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Academicos de esa sede.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    V_ROLE_ID                 NUMBER;
    V_ORG_ID                  NUMBER;
    V_Email_Address           VARCHAR2(100);
    
    V_SECCION_EXCEPT          VARCHAR2(50);
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT *
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID
        AND ROLE_ID = V_ROLE_ID;
    
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
    P_ROL_SEDE := P_ROL || P_CAMP_CODE;
    
    -- Obtener el ROL_ID 
    V_SECCION_EXCEPT := 'ROLES';
    
    SELECT ID 
        INTO V_ROLE_ID
    FROM WORKFLOW.ROLE
    WHERE NAME = P_ROL_SEDE;
    
    V_SECCION_EXCEPT := '';
    
    -- Obtener el ORG_ID 
    V_SECCION_EXCEPT := 'ORGRANIZACION';
    
    SELECT ID
        INTO V_ORG_ID 
    FROM WORKFLOW.ORGANIZATION
    WHERE NAME = 'Root';
    
    V_SECCION_EXCEPT := '';
    
    -- Obtener los datos de usuarios que relaciona rol y usuario
    V_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
    
    -- #######################################################################
    OPEN C_ROLE_ASSIGNMENT;
    LOOP
        FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
        EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
            -- Obtener Datos Usuario
            SELECT Email_Address
                INTO V_Email_Address
            FROM WORKFLOW.WFUSER
            WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
        
            P_CORREO := P_CORREO || V_Email_Address || ',';
    END LOOP;
    CLOSE C_ROLE_ASSIGNMENT;
    V_SECCION_EXCEPT := '';
    
    -- Extraer el ultimo digito en caso sea un "coma"(,)
    SELECT SUBSTR(P_CORREO,1,LENGTH(P_CORREO) -1)
        INTO P_CORREO
    FROM DUAL
    WHERE SUBSTR(P_CORREO,-1,1) = ',';
      
EXCEPTION
WHEN TOO_MANY_ROWS THEN 
    IF ( V_SECCION_EXCEPT = 'ROLES') THEN
        P_ERROR := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
        P_ERROR := '- Se encontraron mas de una ORGANIZACIÓN con el mismo nombre.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
        P_ERROR := '- Se encontraron mas de un usuario con el mismo ROL.';
    ELSE P_ERROR := SQLERRM;
    END IF; 
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN NO_DATA_FOUND THEN
    IF ( V_SECCION_EXCEPT = 'ROLES') THEN
        P_ERROR := '- NO se encontró el nombre del ROL: ' || P_ROL || P_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
        P_ERROR := '- NO se encontró el nombre de la ORGANIZACION.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
        P_ERROR := '- NO  se encontró ningun usuario con esas caracteristicas.';
    ELSE  P_ERROR := SQLERRM;
    END IF; 
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GETUSER_COORDINADOR (
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sencible mayuscula 'S01'
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE, -- sencible a mayusculas 'coordinador'
      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_ROL_COORDINADOR     OUT VARCHAR2,
      P_ROL_MAILS           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GETUSER_COORDINADOR
  FECHA     : 22/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el usuario WF del cordinador,
              Se establecio que el usuario se compone por "COORDINADOR + PROGRAM_CODE + CAMPUS"
                  - Ejm: COORDINADOR110S01 

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   26/07/2017    RMALLQUI      Se agrego la obtencion de correos de usuarios que pertenecen a dicho rol. 
  =================================================================================================================== */
AS
        -- @PARAMETERS
    V_ROLE_ID                 NUMBER;
    V_ORG_ID                  NUMBER;
    V_INDICADOR               NUMBER;
    V_Email_Address           VARCHAR2(500);
    V_EXEPTION                EXCEPTION;
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT *
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID
        AND ROLE_ID = V_ROLE_ID;
    
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
---.
    P_ROL_COORDINADOR := P_ROL ||  P_PROGRAM_NEW || P_CAMP_CODE;
    
    -- Obtener el ROL_ID 
    SELECT ID
        INTO V_ROLE_ID
    FROM WORKFLOW.ROLE
    WHERE NAME = P_ROL_COORDINADOR;
    
    -- Obtener el ORG_ID 
    SELECT ID
        INTO V_ORG_ID
    FROM WORKFLOW.ORGANIZATION
    WHERE NAME = 'Root';
    
    --Validar que exista el ROL
    SELECT COUNT(*)
        INTO V_INDICADOR
    FROM WORKFLOW.ROLE_ASSIGNMENT 
    WHERE ORG_ID = V_ORG_ID
    AND ROLE_ID = V_ROLE_ID;
    
    IF V_INDICADOR <> 1 THEN
        RAISE V_EXEPTION;
    END if;
    
    -- #######################################################################
    OPEN C_ROLE_ASSIGNMENT;
    LOOP
        FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
        EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
            -- Obtener Datos Usuario
            SELECT Email_Address
                INTO V_Email_Address
            FROM WORKFLOW.WFUSER
            WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
        
            P_ROL_MAILS := P_ROL_MAILS || V_Email_Address || ',';
            
    END LOOP;
    CLOSE C_ROLE_ASSIGNMENT;
    
    -- Extraer el ultimo digito en caso sea un "coma"(,)
    SELECT SUBSTR(P_ROL_MAILS,1,LENGTH(P_ROL_MAILS) -1)
        INTO P_ROL_MAILS
    FROM DUAL
    WHERE SUBSTR(P_ROL_MAILS,-1,1) = ',';

EXCEPTION
WHEN V_EXEPTION THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No se encontro el ROL.' || P_ROL_COORDINADOR);
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GETUSER_COORDINADOR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STATUS_SOL          OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VERIFICAR_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_STATUS_SOL, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_ESTADO, P_AN, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CODDETALLE ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
    P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
    P_DATE_CARGO            OUT VARCHAR2,
    P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CODDETALLE
  FECHA     : 22/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE de ls SeccionC "DTA"
              APEC-BDUCCI: Crea un registro al estudiante con el CONCEPTO.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_NOM_SERVICIO            VARCHAR2(30);
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    V_DATENOW                 DATE := SYSDATE;
    
    V_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;          
    V_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
    V_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
    V_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
    V_ALUM_TERM_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
    V_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP 
    V_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE

    V_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
    V_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
    
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORLCUR_FOS IS
        SELECT    SORLCUR_LEVL_CODE,        
                  SORLCUR_COLL_CODE,
                  SORLCUR_DEGC_CODE,  
                  SORLCUR_PROGRAM,      
                  SORLCUR_TERM_CODE_ADMIT,  
                  SORLCUR_STYP_CODE,  
                  SORLCUR_RATE_CODE  
        FROM (
                SELECT    SORLCUR_LEVL_CODE,    SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,  
                          SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  SORLCUR_STYP_CODE,  
                          SORLCUR_RATE_CODE
                FROM SORLCUR
                INNER JOIN SORLFOS ON
                    SORLCUR_PIDM = SORLFOS_PIDM 
                    AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
                WHERE SORLCUR_PIDM = P_PIDM
                    AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                    AND SORLCUR_CACT_CODE = 'ACTIVE' 
                    AND SORLCUR_CURRENT_CDE = 'Y'
                    AND SORLCUR_TERM_CODE_END IS NULL
                ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM <= 1;
    
    -- APEC PARAMS
    V_APEC_SECC_SEDE        VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    V_APEC_IDESCUELA        VARCHAR2(9);
    V_APEC_IDCUENTA         VARCHAR2(10);
    V_APEC_MONEDA           VARCHAR2(10);
    V_APEC_MOUNT            NUMBER;
    V_APEC_IDSECCIONC_INIC     DATE;
    V_APEC_IDSECCIONC_VENC     DATE;
    
BEGIN

    -- #######################################################################
    -- >> GET DATOS --
    OPEN C_SORLCUR_FOS;
    LOOP
    FETCH C_SORLCUR_FOS INTO  V_ALUM_NIVEL,     V_ALUM_ESCUELA,   V_ALUM_GRADO,
                              V_ALUM_PROGRAMA,  V_ALUM_TERM_ADM,  V_ALUM_TIPO_ALUM, V_ALUM_TARIFA;
    EXIT WHEN C_SORLCUR_FOS%NOTFOUND;
    END LOOP;
    CLOSE C_SORLCUR_FOS;
      
      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)

            SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO

            /*******
                Para la SEDE(CAMPUS) se trabaja con el de la SECCIONC ESPECIAL.
                SeccionC : DTA --> sede: HYO
            ********/
            SELECT  t1."IDSeccionC",  
                    t2."IDsede",
                    t1."IDEscuela",
                    t1."FecInic",           
                    "FecVenc",              
                    "Monto",      
                    "IDCuenta",      
                    t1."Moneda"
            INTO    V_APEC_IDSECCIONC, 
                    V_APEC_SECC_SEDE,
                    V_APEC_IDESCUELA,
                    V_APEC_IDSECCIONC_INIC, 
                    V_APEC_IDSECCIONC_VENC, 
                    V_APEC_MOUNT, 
                    V_APEC_IDCUENTA, 
                    V_APEC_MONEDA
            FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
            INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
              ON t1."IDSede"          = t2."IDsede"
              AND t1."IDPerAcad"      = t2."IDPerAcad" 
              AND t1."IDDependencia"  = t2."IDDependencia" 
              AND t1."IDSeccionC"     = t2."IDSeccionC" 
              AND t1."FecInic"        = t2."FecInic"
            WHERE t2."IDDependencia" = 'UCCI'
              AND t2."IDSeccionC"    = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
              --AND LENGTH(t2."IDSeccionC") = 5
              AND t2."IDPerAcad"     = V_APEC_TERM
              AND T1."IDPerAcad"     = V_APEC_TERM
              AND t1."IDConcepto"    = P_DETL_CODE;    

            --**********************************************************************++***********************
            -- INSERT ALUMNO ESTADO en caso no exista el registro.
            SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDSede"      = V_APEC_SECC_SEDE 
            AND "IDAlumno"      = P_ID_ALUMNO 
            AND "IDPerAcad"     = V_APEC_TERM
            AND "IDDependencia" = 'UCCI' 
            AND "IDSeccionC"    = V_APEC_IDSECCIONC 
            AND "FecInic"       = V_APEC_IDSECCIONC_INIC;
            ----- INSERT
            IF (V_INDICADOR = 0) THEN

                  INSERT INTO dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE 
                          ("IDSede",                      "IDAlumno", 
                          "IDPerAcad",                    "IDDependencia", 
                          "IDSeccionC",                   "FecInic", 
                          "IDPersonal",                   "IDSeccion", 
                          "Ciclo",                        "IDEscuela", 
                          "IDEscala",                     "IDInstitucion", 
                          "FecMatricula",                 "Beca",  
                          "Estado",                       "FechaEstado", 
                          "MatriculaTipo",                "Carnet", 
                          "PlanPago")
                  VALUES(
                          V_APEC_SECC_SEDE,               P_ID_ALUMNO, 
                          V_APEC_TERM,                    'UCCI', 
                          V_APEC_IDSECCIONC,              V_APEC_IDSECCIONC_INIC, 
                          'Banner', /*idpersonal*/        V_APEC_IDSECCIONC, 
                          0, /*ciclo*/                    V_APEC_IDESCUELA, /*idescuela*/
                          '1', /*idescala*/               '00000000000' , /*institucion*/ 
                          V_DATENOW,                      0, /*beca*/ 
                          'M', /*estado*/                 V_DATENOW, /*fechaestado*/ 
                          '2', /*matriculatipo*/          0, /*carnet*/ 
                          5 /*planpago*/ 
                  );
            END IF;

            -- *********************************************************************************************
            -- ASINGANDO el cargo a CTACORRIENTE
                  -->SECCION ESPECIAL DTA
                    -- CONCETPSO : DTA - C01 -> S/  3.0
                    -- CONCETPSO : DTI - C02 -> S/100.0                
            SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI' 
            AND "IDSede"      = V_APEC_SECC_SEDE 
            AND "IDAlumno"    = P_ID_ALUMNO
            AND "IDPerAcad"   = V_APEC_TERM 
            AND "IDSeccionC"  = V_APEC_IDSECCIONC
            AND "FecInic"     = V_APEC_IDSECCIONC_INIC
            AND "IDConcepto"  = P_DETL_CODE;

            IF (V_INDICADOR = 0) THEN
                  
                INSERT INTO dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                    ("IDSede",              "IDAlumno",               "IDPerAcad", 
                    "IDDependencia",        "IDSeccionC",             "FecInic", 
                    "IDConcepto",           "FecCargo",               "Prorroga", 
                    "Cargo",                "DescMora",               "IDEscuela", 
                    "IDCuenta",             "IDInstitucion",          "Moneda", 
                    "IDEscala",             "Beca",                   "Estado", 
                    "Abono" )
                VALUES
                    (V_APEC_SECC_SEDE,      P_ID_ALUMNO,              V_APEC_TERM, 
                    'UCCI',                 V_APEC_IDSECCIONC,        V_APEC_IDSECCIONC_INIC,
                    P_DETL_CODE,            V_APEC_IDSECCIONC_INIC,   '0',
                    V_APEC_MOUNT,           0,                        V_APEC_IDESCUELA,
                    V_APEC_IDCUENTA,        '00000000000',            V_APEC_MONEDA,
                    '1',                    0,                        'M',
                    0);
            
            ELSIF(V_INDICADOR = 1) THEN
            
                UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                    SET "Cargo" = "Cargo" + V_APEC_MOUNT  
                WHERE "IDDependencia" = 'UCCI' 
                AND "IDSede"      = V_APEC_SECC_SEDE 
                AND "IDAlumno"    = P_ID_ALUMNO
                AND "IDPerAcad"   = V_APEC_TERM 
                AND "IDSeccionC"  = V_APEC_IDSECCIONC
                AND "FecInic"     = V_APEC_IDSECCIONC_INIC
                AND "IDConcepto"  = P_DETL_CODE;

            ELSE
                RAISE V_EXCEP_NOTFOUND_CARGO;                  
            END IF;
            
              P_TRAN_NUMBER := 0;

      ELSE -- 1 ---> BANNER

            -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
            SELECT SFRRGFE_MIN_CHARGE
                INTO V_CARGO_MINIMO
            FROM SFRRGFE 
            WHERE SFRRGFE_TERM_CODE = P_TERM_CODE AND SFRRGFE_DETL_CODE = P_DETL_CODE AND SFRRGFE_TYPE = 'STUDENT'
            AND NVL(NVL(SFRRGFE_LEVL_CODE, v_alum_nivel),'-')           = NVL(NVL(v_alum_nivel, SFRRGFE_LEVL_CODE),'-')--------------------- Nivel
            AND NVL(NVL(SFRRGFE_CAMP_CODE, P_CAMP_CODE),'-')          = NVL(NVL(P_CAMP_CODE, SFRRGFE_CAMP_CODE),'-')  ----------------- Campus (sede)
            AND NVL(NVL(SFRRGFE_COLL_CODE, v_alum_escuela),'-')         = NVL(NVL(v_alum_escuela, SFRRGFE_COLL_CODE),'-') --------------- Escuela 
            AND NVL(NVL(SFRRGFE_DEGC_CODE, v_alum_grado),'-')           = NVL(NVL(v_alum_grado, SFRRGFE_DEGC_CODE),'-') ----------- Grado
            AND NVL(NVL(SFRRGFE_PROGRAM, v_alum_programa),'-')          = NVL(NVL(v_alum_programa, SFRRGFE_PROGRAM),'-') ---------- Programa
            AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, V_ALUM_TERM_ADM),'-')  = NVL(NVL(V_ALUM_TERM_ADM, SFRRGFE_TERM_CODE_ADMIT),'-') --------- Periodo Admicion
            -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
            -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
            -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
            AND NVL(NVL(SFRRGFE_DEPT_CODE, P_DEPT_CODE),'-')    = NVL(NVL(P_DEPT_CODE, SFRRGFE_DEPT_CODE),'-') ------------ Departamento
            -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
            AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, v_alum_tipo_alum),'-') = NVL(NVL(v_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') ------ Tipo Alumno Curriculum
            AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, v_alum_tarifa),'-')   = NVL(NVL(v_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------- Trf Curriculum (Escala)

            -- GET - Descripcion de CODIGO DETALLE  
            SELECT TBBDETC_DESC
                INTO V_NOM_SERVICIO
            FROM TBBDETC
            WHERE TBBDETC_DETAIL_CODE = P_DETL_CODE;
          
            -- #######################################################################
            -- GENERAR DEUDA
            TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM,        -- PIDEM ALUMNO
                                     p_term_code            =>  P_TERM_CODE,          -- DETALLE
                                     p_detail_code          =>  P_DETL_CODE,      -- CODIGO DETALLE 
                                     p_user                 =>  USER,               -- USUARIO
                                     p_entry_date           =>  SYSDATE,     
                                     p_amount               =>  V_CARGO_MINIMO,
                                     p_effective_date       =>  SYSDATE,
                                     p_bill_date            =>  NULL,    
                                     p_due_date             =>  NULL,    
                                     p_desc                 =>  V_NOM_SERVICIO,    
                                     p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                     p_tran_number_paid     =>  NULL,     -- numero de transaccion que sera pagara
                                     p_crossref_pidm        =>  NULL,    
                                     p_crossref_number      =>  NULL,    
                                     p_crossref_detail_code =>  NULL,     
                                     p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                     p_acct_feed_ind        =>  'Y',
                                     p_session_number       =>  0,    
                                     p_cshr_end_date        =>  NULL,    
                                     p_crn                  =>  NULL,
                                     p_crossref_srce_code   =>  NULL,
                                     p_loc_mdt              =>  NULL,
                                     p_loc_mdt_seq          =>  NULL,    
                                     p_rate                 =>  NULL,    
                                     p_units                =>  NULL,     
                                     p_document_number      =>  NULL,    
                                     p_trans_date           =>  NULL,    
                                     p_payment_id           =>  NULL,    
                                     p_invoice_number       =>  NULL,    
                                     p_statement_date       =>  NULL,    
                                     p_inv_number_paid      =>  NULL,    
                                     p_curr_code            =>  NULL,    
                                     p_exchange_diff        =>  NULL,    
                                     p_foreign_amount       =>  NULL,    
                                     p_late_dcat_code       =>  NULL,    
                                     p_atyp_code            =>  NULL,    
                                     p_atyp_seqno           =>  NULL,    
                                     p_card_type_vr         =>  NULL,    
                                     p_card_exp_date_vr     =>  NULL,     
                                     p_card_auth_number_vr  =>  NULL,    
                                     p_crossref_dcat_code   =>  NULL,    
                                     p_orig_chg_ind         =>  NULL,    
                                     p_ccrd_code            =>  NULL,    
                                     p_merchant_id          =>  NULL,    
                                     p_data_origin          =>  'WorkFlow',    
                                     p_override_hold        =>  'N',     
                                     p_tran_number_out      =>  P_TRAN_NUMBER, 
                                     p_rowid_out            =>  V_ROWID);
      
      END IF;
      
      SELECT TO_CHAR(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') INTO P_DATE_CARGO FROM DUAL;
      
      COMMIT;
EXCEPTION
WHEN V_EXCEP_NOTFOUND_CARGO THEN
    P_ERROR := '- No se encontro data para generar cargo al estudiante.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CODDETALLE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_SIDEUDA_DETLS (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE, --------------- APEC
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, ------------- APEC
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE, ------------- APEC
    P_DETL_1CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
    P_DETL_2CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
    P_DEUDA_CODE          OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Verificar en base a un PIDM y el Num. Transaccion verifique la existencia de deuda. divisa PEN.
              - APEC verificar si el PIDM tiene deuda en algun concepto especifico para la seccion "DTA".

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */              
AS
    PRAGMA AUTONOMOUS_TRANSACTION;
    V_DEUDA_INDICADOR       NUMBER;
    -- APEC PARAMS
    V_APEC_SECC_SEDE        VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    V_APEC_IDSECCIONC_INIC  DATE;
    V_APEC_MOUNT1           NUMBER;      
    V_APEC_MOUNT2           NUMBER;
BEGIN
--
    --#################################----- APEC -----#######################################
    --########################################################################################
    -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
    IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
        SELECT CZRTERM_TERM_BDUCCI
            INTO V_APEC_TERM
        FROM CZRTERM
        WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        
        --**********************************************************************************************
        -- GET SECCIONC
        SELECT  "IDsede", "IDSeccionC" , "FecInic"
            INTO V_APEC_SECC_SEDE, V_APEC_IDSECCIONC, V_APEC_IDSECCIONC_INIC
        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia"='UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDSeccionC" = 'DTA'; -- 'DTA' SeccionC Especial para traslado interno
        --AND LENGTH("IDSeccionC") = 5;
        
        -- GET 1ra DEUDA(s)
        SELECT "Deuda"
            INTO V_APEC_MOUNT1
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno"    = P_ID_ALUMNO
        AND "IDSede"      = V_APEC_SECC_SEDE
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDSeccionC"  = V_APEC_IDSECCIONC
        AND "FecInic"     = V_APEC_IDSECCIONC_INIC
        AND "IDConcepto"  = P_DETL_1CODE;
        
        -- GET 2da DEUDA(s) 
        SELECT "Deuda"
            INTO V_APEC_MOUNT2
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno"    = P_ID_ALUMNO
        AND "IDSede"      = V_APEC_SECC_SEDE
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDSeccionC"  = V_APEC_IDSECCIONC
        AND "FecInic"     = V_APEC_IDSECCIONC_INIC
        AND "IDConcepto"  = P_DETL_2CODE;
        
        COMMIT;
    END IF;
    
    /*
      - CODIGOS DEUDA
        3 : Tiene deuda en los dos conceptos
        1 : Tiene deuda solo en el primer concepto
        2 : Tiene deuda solo en el segundo concepto
    */
    P_DEUDA_CODE := CASE  WHEN V_APEC_MOUNT1 > 0 AND V_APEC_MOUNT2 > 0 THEN 3
                          WHEN V_APEC_MOUNT1 > 0 THEN 1 
                          WHEN V_APEC_MOUNT2 > 0 THEN 2 ELSE 0 END;
                    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_SIDEUDA_DETLS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_DEL_CODDETALLE ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, 
    P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
    P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_DEL_CODDETALLE
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE.
              APEC-BDUCCI: Amortiza un cargo al estudiante con mismo costo del CONCEPTO. 

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_NOM_SERVICIO            VARCHAR2(30);
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    V_MESSAGE                 EXCEPTION;
    V_TRAN_NUMBER_OUT         TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
    V_ROWID                   GB_COMMON.INTERNAL_RECORD_ID_TYPE;
    
    -- APEC PARAMS
    V_APEC_SECC_SEDE        VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    V_APEC_IDSECCIONC_INIC  DATE;
    V_APEC_MOUNT            NUMBER;
    
    V_FOUND_AMOUNT          TBRACCD.TBRACCD_AMOUNT%TYPE;
    V_COD_DETALLE           TBRACCD.TBRACCD_DETAIL_CODE%TYPE;
    V_TERM_CODE             STVTERM.STVTERM_CODE%TYPE;
BEGIN
      
      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)

            SELECT CZRTERM_TERM_BDUCCI
                INTO V_APEC_TERM
            FROM CZRTERM
            WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
            
            --**********************************************************************************
            -- GET datos CUENTA APEC
            SELECT t1."IDSeccionC", t2."IDsede", t1."FecInic", t1."Monto" INTO V_APEC_IDSECCIONC, V_APEC_SECC_SEDE, V_APEC_IDSECCIONC_INIC, V_APEC_MOUNT
              FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
              INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t2 ON 
                t1."IDSede"          = t2."IDsede"
                AND t1."IDPerAcad"      = t2."IDPerAcad" 
                AND t1."IDDependencia"  = t2."IDDependencia" 
                AND t1."IDSeccionC"     = t2."IDSeccionC" 
                AND t1."FecInic"        = t2."FecInic"
              WHERE t2."IDDependencia"  = 'UCCI'
                AND t2."IDSeccionC"     = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
                AND LENGTH(t2."IDSeccionC") = 3
                AND t2."IDPerAcad"      = V_APEC_TERM
                AND T1."IDPerAcad"      = V_APEC_TERM
                AND t1."IDConcepto"     = P_DETL_CODE;
            
            --******************************************************************************
            -- Validar si existe la cuenta del estudiante  para traslado interno
            SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDSede" = V_APEC_SECC_SEDE
            AND "IDAlumno" = P_ID_ALUMNO
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDDependencia" = 'UCCI'
            AND "IDSeccionC" = V_APEC_IDSECCIONC
            AND "FecInic" = V_APEC_IDSECCIONC_INIC
            AND "IDConcepto" = P_DETL_CODE;
            
            IF (V_INDICADOR = 1) THEN
                  -- UPDATE MONTO(s)
                  UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                  SET "Cargo" = "Cargo" - V_APEC_MOUNT
                  WHERE "IDDependencia" = 'UCCI'
                  AND "IDAlumno"    = P_ID_ALUMNO
                  AND "IDSede"      = V_APEC_SECC_SEDE
                  AND "IDPerAcad"   = V_APEC_TERM
                  AND "IDSeccionC"  = V_APEC_IDSECCIONC
                  AND "FecInic" = V_APEC_IDSECCIONC_INIC
                  AND "IDConcepto"  = P_DETL_CODE;
            ELSE
                  RAISE V_EXCEP_NOTFOUND_CARGO;
            END IF;

            P_TRAN_NUMBER := 0;

      ELSE -- 1 ---> BANNER

             -- Validar si la transaccion (DEUDA) NO tubo algun movimiento.
            SELECT COUNT(*) INTO V_INDICADOR
            FROM (
                    -- : listar LOS CODIGOS QUE YA TIENEN ALGUN MOVIMIENTO. : TRAN , CHG 
                    SELECT TBRAPPL_PIDM TEMP_PIDM ,TBRAPPL_PAY_TRAN_NUMBER TEMP_PAY_TRAN_NUMBER
                    FROM TBRAPPL
                    WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM
                    UNION 
                    SELECT TBRAPPL_PIDM, TBRAPPL_CHG_TRAN_NUMBER
                    FROM TBRAPPL
                    WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM
            ) WHERE P_TRAN_NUMBER = TEMP_PAY_TRAN_NUMBER;
            
            IF V_INDICADOR = 1 THEN
                  RAISE V_MESSAGE;
            END IF;              
                
            -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
            TZKCDAA.p_calc_deuda_alumno(P_PIDM,'PEN');
            COMMIT;
                
            -- GET , COD_DETALLE , AMOUNT(NEGATIVO) de la transaccion a cancelar  
            SELECT  (TZRCDAB_AMOUNT * (-1)), 
                    TZRCDAB_DETAIL_CODE, 
                    TZRCDAB_TERM_CODE
            INTO    V_FOUND_AMOUNT,
                    V_COD_DETALLE, 
                    V_TERM_CODE
            FROM   TZRCDAB
            WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
            AND TZRCDAB_PIDM = P_PIDM 
            AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
            
            -- GET DESCRIPCION del cod_detalle
            SELECT TBBDETC_DESC INTO V_NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = V_COD_DETALLE;
        
            -- #######################################################################
            -- GENERAR DEUDA
            TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM,        -- PIDEM ALUMNO
                                     p_term_code            =>  V_TERM_CODE,          -- DETALLE
                                     p_detail_code          =>  V_COD_DETALLE,      -- CODIGO DETALLE  -- mismo codigo detalle
                                     p_user                 =>  USER,               -- USUARIO
                                     p_entry_date           =>  SYSDATE,     
                                     p_amount               =>  V_FOUND_AMOUNT,
                                     p_effective_date       =>  SYSDATE,
                                     p_bill_date            =>  NULL,    
                                     p_due_date             =>  NULL,    
                                     p_desc                 =>  V_NOM_SERVICIO,       -- referencia por consultar 
                                     p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                     p_tran_number_paid     =>  P_TRAN_NUMBER,      -- numero de transaccion que sera pagara
                                     p_crossref_pidm        =>  NULL,    
                                     p_crossref_number      =>  NULL,    
                                     p_crossref_detail_code =>  NULL,     
                                     p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                     p_acct_feed_ind        =>  'Y',
                                     p_session_number       =>  0,    
                                     p_cshr_end_date        =>  NULL,    
                                     p_crn                  =>  NULL,
                                     p_crossref_srce_code   =>  NULL,
                                     p_loc_mdt              =>  NULL,
                                     p_loc_mdt_seq          =>  NULL,    
                                     p_rate                 =>  NULL,    
                                     p_units                =>  NULL,     
                                     p_document_number      =>  NULL,    
                                     p_trans_date           =>  NULL,    
                                     p_payment_id           =>  NULL,    
                                     p_invoice_number       =>  NULL,    
                                     p_statement_date       =>  NULL,    
                                     p_inv_number_paid      =>  NULL,    
                                     p_curr_code            =>  NULL,    
                                     p_exchange_diff        =>  NULL,    
                                     p_foreign_amount       =>  NULL,    
                                     p_late_dcat_code       =>  NULL,    
                                     p_atyp_code            =>  NULL,    
                                     p_atyp_seqno           =>  NULL,    
                                     p_card_type_vr         =>  NULL,    
                                     p_card_exp_date_vr     =>  NULL,     
                                     p_card_auth_number_vr  =>  NULL,    
                                     p_crossref_dcat_code   =>  NULL,    
                                     p_orig_chg_ind         =>  NULL,    
                                     p_ccrd_code            =>  NULL,    
                                     p_merchant_id          =>  NULL,    
                                     p_data_origin          =>  'WORKFLOW',    
                                     p_override_hold        =>  'N',     
                                     p_tran_number_out      =>  V_TRAN_NUMBER_OUT, 
                                     p_rowid_out            =>  V_ROWID);
              ------------------------------------------------------------------------------  
                  -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
                  TZJAPOL.p_run_proc_tvrappl(P_ID_ALUMNO);
              ------------------------------------------------------------------------------  
      END IF;
      COMMIT;
EXCEPTION
WHEN V_MESSAGE THEN
    P_ERROR  := '- La deuda tubo movimientos por lo que no se puede cancelar directamente.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN V_EXCEP_NOTFOUND_CARGO THEN
    P_ERROR := '- No se encontro el concepto en la cuenta corriente.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_DEL_CODDETALLE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_VALIDAR_FECHA_DISPONIBLE (
    P_DATE   IN VARCHAR2
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_VALIDAR_FECHA_DISPONIBLE
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida que la fecha ENVIADA este dentro de las 48 horas para su atencion.
              se considdera la atencion hasta antes de finalizar el dia de finalizada las 48 horas

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS    
    V_RECEPTION_DATE            DATE;
    V_DIAS                      NUMBER;
BEGIN
--
--      -- CONVERTIR LA FECHA A UN FORMATO VALIDO
--      --SELECT TO_DATE('25/05/2017 11:02:47','dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = American') 
--      SELECT TO_DATE(P_DATE,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = American') 
--      INTO V_RECEPTION_DATE
--      FROM DUAL;
--
--    -- Valida que la fecha este SEA MENOR A 3 DIAS DE TOLERANCIA.
--    IF(SYSDATE < TO_DATE(TO_CHAR(V_RECEPTION_DATE + 3,'dd/mm/yyyy'),'dd/mm/yyyy')) THEN
--        RETURN 'TRUE';
--    ELSE
--        RETURN 'FALSE';
--    END IF;
--
    SELECT TO_DATE(P_DATE, 'DD/MM/YYYY hh24:mi:ss')
        INTO V_RECEPTION_DATE
    FROM DUAL;
    
    SELECT SYSDATE - V_RECEPTION_DATE
        INTO V_DIAS
    FROM DUAL;
    
    -- Valida que la fecha este dentro de los 2 DIAS DE TOLERANCIA.
    IF V_DIAS <= 2 THEN
        RETURN 'TRUE';
    ELSE
        RETURN 'FALSE';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END F_VALIDAR_FECHA_DISPONIBLE;  


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_STUDENT_INPUTS (
    P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_PROGRAM_OLD           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_OLD_DESC      OUT VARCHAR2,
    P_PROGRAM_NEW           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_NEW_DESC      OUT VARCHAR2,
    P_COMENTARIO_ALUMNO     OUT VARCHAR2,
    P_TELEFONO_ALUMNO       OUT VARCHAR2,
    P_OPT_CONSEJERIA        OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 11/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Para Solicitudes 'SOL013' : 
              Obtiene EL PROGRAMA, PROGRAM_DESC, COMENTARIO Y TELEFONO de una solicitud especifica. 
              Se establecio que ultimo dato es el TELEFONO y el penultimo el COMENTARIO.

  NRO     FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
AS
    V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
    V_COMENTARIO              VARCHAR2(4000);
    V_CLAVE                   VARCHAR2(4000);
    V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
    V_PIDM                    SPRIDEN.SPRIDEN_PIDM%TYPE;
    V_ERROR                   EXCEPTION; 
    
    -- OBTENER PROGRAMA ACTUAL(Carrera) y  descriocion
    CURSOR C_PROGRAM_OLD IS
        SELECT SORLCUR_PROGRAM, SZVMAJR_DESCRIPTION
        FROM (
            SELECT  SORLCUR_PROGRAM,SZVMAJR_DESCRIPTION
            FROM SORLCUR        
            INNER JOIN SORLFOS ON
                SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
            INNER JOIN SZVMAJR ON
                SORLCUR_PROGRAM = SZVMAJR_CODE 
            WHERE SORLCUR_PIDM = V_PIDM 
                AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM = 1;
    
    /*
    OBTENER CARRERA Y SU DESCRIPCION(PROGRAM OLD),COMENTARIO ,EL TELEFONO Y LA CONFIRMACION DE CONSEJERIA
    DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
    */
    CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD ON--------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
      
BEGIN
      
    -- GET CODIGO SOLICITUD
    SELECT SVRSVPR_SRVC_CODE, SVRSVPR_PIDM
        INTO V_CODIGO_SOLICITUD, V_PIDM
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    -- OBTENER PROGRAMA ACTUAL(Carrera) y  descriocion
    OPEN C_PROGRAM_OLD;
    LOOP
        FETCH C_PROGRAM_OLD INTO P_PROGRAM_OLD, P_PROGRAM_OLD_DESC;
        EXIT WHEN C_PROGRAM_OLD%NOTFOUND;
    END LOOP;
    CLOSE C_PROGRAM_OLD;

    -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
    OPEN C_SVRSVAD;
    LOOP
        FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
        IF C_SVRSVAD%FOUND THEN
            IF V_ADDL_DATA_SEQ = 1 THEN
                -- OBTENER NUEVO PROGRAMA (Carrera) y su descriocion
                P_PROGRAM_NEW := V_CLAVE;
                P_PROGRAM_NEW_DESC := V_COMENTARIO;
            ELSIF V_ADDL_DATA_SEQ = 2 THEN
                -- OBTENER COMENTARIO DE LA SOLICITUD
                P_COMENTARIO_ALUMNO := V_COMENTARIO;
            ELSIF V_ADDL_DATA_SEQ = 3 THEN
                -- OBTENER EL TELEFONO DE LA SOLICITUD 
                P_TELEFONO_ALUMNO := V_COMENTARIO;
            ELSIF V_ADDL_DATA_SEQ = 4 THEN
                -- GET OPCION DE CONFIRMACION DE CONSEJERIA
                P_OPT_CONSEJERIA := V_CLAVE;
            END IF;
        ELSE EXIT;
        END IF;
    END LOOP;
    CLOSE C_SVRSVAD;
    
    IF (P_TELEFONO_ALUMNO IS NULL OR P_COMENTARIO_ALUMNO IS NULL OR P_PROGRAM_NEW IS NULL) THEN
      RAISE V_ERROR;
    END IF;
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

-- ## Se descarto su uso para esta ocasion 201720 26/07/2017
PROCEDURE P_GET_CORDINADOR_PROGRAM (
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_ID                OUT SPRIDEN.SPRIDEN_ID%TYPE,
    P_NOMBREC           OUT VARCHAR2,
    P_CORREO            OUT VARCHAR2,
    P_PHONE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_CORDINADOR_PROGRAM
  FECHA     : 19/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : A traves de un codigo de carrera Ejm "110" se obtiene los datos del coordinador
  =================================================================================================================== */
AS
    V_PIDM              SPRIDEN.SPRIDEN_PIDM%TYPE;
    V_NOM_APP           VARCHAR2(200);
    
    ------- GET MAIL
    CURSOR C_CORREO IS
    SELECT GOREMAL_EMAIL_ADDRESS FROM (
        SELECT ORDEN, GOREMAL_EMAIL_ADDRESS FROM (
            SELECT 1 ORDEN, GOREMAL_EMAIL_ADDRESS 
            FROM GOREMAL 
            WHERE GOREMAL_PIDM = V_PIDM 
                AND GOREMAL_STATUS_IND = 'A' 
                AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
            UNION
            SELECT 2 ORDEN, GOREMAL_EMAIL_ADDRESS 
            FROM GOREMAL
            WHERE GOREMAL_PIDM = V_PIDM
                AND GOREMAL_STATUS_IND = 'A' 
                AND GOREMAL_PREFERRED_IND = 'Y' 
                AND GOREMAL_EMAIL_ADDRESS NOT LIKE ('%@continental.edu.pe')
            UNION
            SELECT 3 ORDEN, GOREMAL_EMAIL_ADDRESS
            FROM GOREMAL
            WHERE GOREMAL_PIDM = V_PIDM
                AND GOREMAL_STATUS_IND = 'A' 
                AND GOREMAL_PREFERRED_IND <> 'Y' 
                AND GOREMAL_EMAIL_ADDRESS NOT LIKE ('%@continental.edu.pe')
        ) ORDER BY ORDEN
    ) WHERE ROWNUM = 1;
    
    ------- GET PHONE
    CURSOR C_PHONE IS
        SELECT PHONE
        FROM (
            -- ORDEN para obtener el telefono prefrencial, caso contrario un telefono no preferencial.
            SELECT PHONE 
            FROM (
                SELECT ORDEN, PHONE FROM (
                -- Telefono preferencial
                    SELECT 1 ORDEN, TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PHONE
                    FROM SPRTELE
                    LEFT JOIN STVTELE ON 
                        SPRTELE_TELE_CODE = STVTELE_CODE
                    WHERE SPRTELE_PIDM = V_PIDM
                    AND SPRTELE_PRIMARY_IND = 'Y'
                    AND SPRTELE_STATUS_IND IS NULL
                    AND (TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%<%' 
                        OR TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%>%')
                ORDER BY SPRTELE_SEQNO)
            UNION
            SELECT ORDEN, PHONE
            FROM (
            -- Telefono no preferencial
                SELECT 2 ORDEN, TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PHONE
                FROM SPRTELE
                LEFT JOIN STVTELE ON 
                    SPRTELE_TELE_CODE = STVTELE_CODE
                WHERE SPRTELE_PIDM = V_PIDM
                    AND SPRTELE_PRIMARY_IND IS NULL
                    AND SPRTELE_STATUS_IND IS NULL
                    AND (TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%<%' 
                        OR TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%>%')
                ORDER BY SPRTELE_SEQNO)
            ) ORDER BY ORDEN
        ) WHERE ROWNUM = 1;
BEGIN 
--     
    P_NOMBREC := '-';
    P_CORREO  := '-';
    P_PHONE   := '-';
    
    -- GET ID, PIDM , NOMBRES
    SELECT DNI, SPRIDEN_PIDM, (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME) NAMES 
        INTO P_ID, V_PIDM, P_NOMBREC
    FROM ADVISOR 
    INNER JOIN SPRIDEN ON 
        SPRIDEN_ID = DNI
    WHERE TIPO_ASESOR = 'COR' 
        AND CARRERA = P_PROGRAM_NEW 
        AND NIVEL = 'PG';
    
    ------- GET MAIL
    OPEN C_CORREO;
    LOOP
        FETCH C_CORREO INTO P_CORREO;
        EXIT WHEN C_CORREO%NOTFOUND;
    END LOOP;
    CLOSE C_CORREO;
    
    ------- GET PHONE
    OPEN C_PHONE;
    LOOP
        FETCH C_PHONE INTO P_PHONE;
        EXIT WHEN C_PHONE%NOTFOUND;
    END LOOP;
    CLOSE C_PHONE;
      
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_CORDINADOR_PROGRAM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CAMBIAR_CARRERA ( 
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PER_CATALOGO        IN VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_CAMBIAR_ESTATUS
  FECHA     : 18/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cambia la carrera al estudiante.

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   17/08/2017    rmallqui      Se esta llamando al SP para insertar atributo de plan en caso le corresponda.
  =================================================================================================================== */
AS
        V_TERM_CODE_LAST          SFBETRM.SFBETRM_TERM_CODE%TYPE;
        V_CURR_RULE               SOBCURR.SOBCURR_CURR_RULE%TYPE;
        V_COLL_CODE               SOBCURR.SOBCURR_COLL_CODE%TYPE;
        V_DEGC_CODE               SOBCURR.SOBCURR_DEGC_CODE%TYPE;
        V_CMJR_RULE               SORCMJR.SORCMJR_CMJR_RULE%TYPE;
        V_PER_CATALOGO            VARCHAR2(6);
        V_INDICADOR               NUMBER;
        V_MESSAGE                 EXCEPTION;

        V_ATTS_CODE               STVATTS.STVATTS_CODE%TYPE;      -------- COD atributos
        
        /*----------------------------------------------------------------------------------
        -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
              - Nuevo Registro como Evidencia que alumno realizo TRAMITE
        */
        CURSOR C_SGRSTSP IS
          SELECT * FROM (
              --Ultimo registro cercano al P_TERM_CODE
              SELECT * FROM SGRSTSP 
              WHERE SGRSTSP_PIDM = P_PIDM
              AND SGRSTSP_TERM_CODE_EFF <= P_TERM_CODE
              ORDER BY SGRSTSP_KEY_SEQNO DESC
          ) WHERE ROWNUM = 1;
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SGBSTDN IS
        SELECT * FROM (
            SELECT *
            FROM SGBSTDN
            WHERE SGBSTDN_PIDM = P_PIDM
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN
            WHERE SGBSTDN_PIDM = P_PIDM
            AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE
         );
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SORLCUR IS
        SELECT * FROM (
            SELECT * FROM SORLCUR 
            WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        )WHERE ROWNUM = 1;
        
        V_SGRSTSP_KEY_SEQNO     SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
        V_SGRSTSP_REC           SGRSTSP%ROWTYPE;
        V_SGBSTDN_REC           SGBSTDN%ROWTYPE;
        V_SORLCUR_REC           SORLCUR%ROWTYPE;
        V_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE;
        V_SORLCUR_SEQNO_INC     SORLCUR.SORLCUR_SEQNO%TYPE;
        V_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE;
        
        V_INDICADOR             NUMBER;
BEGIN
--        
          -- Calculando periodo CATALOGO
          V_PER_CATALOGO := CASE P_PER_CATALOGO WHEN '2015' THEN '201800'
                                                WHEN '2007' THEN '201420'
                                                ELSE NULL END;
          IF V_PER_CATALOGO IS NULL THEN
                  RAISE V_MESSAGE;
          END IF;
          
          
          /* #######################################################################
          -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
              - Nuevo Registro como Evidencia que alumno realizo TRAMITE
          */
          OPEN C_SGRSTSP;
          LOOP
              FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
              EXIT WHEN C_SGRSTSP%NOTFOUND;
              
              -- GET SGRSTSP_KEY_SEQNO NUEVO
              SELECT SGRSTSP_KEY_SEQNO + 1 INTO V_SGRSTSP_KEY_SEQNO FROM (
                  SELECT SGRSTSP_KEY_SEQNO  FROM SGRSTSP 
                  WHERE SGRSTSP_PIDM = P_PIDM
                  ORDER BY SGRSTSP_KEY_SEQNO DESC
              ) WHERE ROWNUM = 1;
              
              INSERT INTO SGRSTSP (
                      SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                      SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                      SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                      SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                      SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                      SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                      SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                      SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                      SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
              SELECT 
                      V_SGRSTSP_REC.SGRSTSP_PIDM,       P_TERM_CODE,                          V_SGRSTSP_KEY_SEQNO,
                      'AS',                             SYSDATE,                              'WorkFlow',
                      USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                      V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                      V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                      V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                      V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
              FROM DUAL;              
          END LOOP;
          CLOSE C_SGRSTSP;
          
          -- #######################################################################
          -- Get Reglas de Curriculum (CURR_RULE)
          SELECT  SOBCURR_CURR_RULE,  SOBCURR_COLL_CODE,  SOBCURR_DEGC_CODE,  SORCMJR_CMJR_RULE
          INTO    V_CURR_RULE,        V_COLL_CODE,        V_DEGC_CODE,        V_CMJR_RULE
          FROM (
                SELECT * FROM SOBCURR
                INNER JOIN (
                    SELECT  SOBCURR_CURR_RULE CURR_RULE, SOBCURR_CAMP_CODE CAMP_CODE, SOBCURR_COLL_CODE COLL_CODE, 
                            SOBCURR_DEGC_CODE DEGC_CODE, SOBCURR_PROGRAM CPROGRAM, MAX(SOBCURR_TERM_CODE_INIT) TERM_CODE_INIT
                    FROM SOBCURR 
                    WHERE SOBCURR_LEVL_CODE = 'PG' 
                    GROUP BY SOBCURR_CURR_RULE, SOBCURR_CAMP_CODE, SOBCURR_COLL_CODE, SOBCURR_DEGC_CODE, SOBCURR_PROGRAM
                )ON SOBCURR_CURR_RULE = CURR_RULE
                AND SOBCURR_CAMP_CODE = CAMP_CODE
                AND SOBCURR_COLL_CODE = COLL_CODE
                AND SOBCURR_DEGC_CODE = DEGC_CODE
                AND SOBCURR_PROGRAM = CPROGRAM
                AND SOBCURR_TERM_CODE_INIT = TERM_CODE_INIT
          ) 
          INNER JOIN (
                SELECT * FROM SORCMJR
                INNER JOIN (
                    SELECT SORCMJR_CURR_RULE CURR_RULE, SORCMJR_MAJR_CODE MAJR_CODE, SORCMJR_DEPT_CODE DEPT_CODE, MAX(SORCMJR_TERM_CODE_EFF) TERM_CODE_EFF
                    FROM SORCMJR 
                    GROUP BY SORCMJR_CURR_RULE, SORCMJR_MAJR_CODE, SORCMJR_DEPT_CODE
                )ON SORCMJR_CURR_RULE = CURR_RULE
                AND SORCMJR_MAJR_CODE = MAJR_CODE
                AND SORCMJR_DEPT_CODE = DEPT_CODE
                AND SORCMJR_TERM_CODE_EFF = TERM_CODE_EFF
          )
          ON SOBCURR_CURR_RULE = SORCMJR_CURR_RULE
          WHERE SOBCURR_CAMP_CODE = P_CAMP_CODE
          AND SOBCURR_LEVL_CODE = 'PG'
          AND SORCMJR_DEPT_CODE = P_DEPT_CODE
          AND SOBCURR_PROGRAM = P_PROGRAM_NEW;

          
          -- UPDATE SGASTDN
          UPDATE SGBSTDN
          SET  SGBSTDN_STST_CODE = 'AS'  ------------------ ESTADOS STVSTST : 'AS' - Activo
              ,SGBSTDN_COLL_CODE_1 = V_COLL_CODE
              ,SGBSTDN_DEGC_CODE_1 = V_DEGC_CODE
              ,SGBSTDN_MAJR_CODE_1 = P_PROGRAM_NEW
              ,SGBSTDN_ACTIVITY_DATE = SYSDATE
              ,SGBSTDN_PROGRAM_1 = P_PROGRAM_NEW
              ,SGBSTDN_TERM_CODE_CTLG_1 = V_PER_CATALOGO
              ,SGBSTDN_CURR_RULE_1 = V_CURR_RULE
              ,SGBSTDN_CMJR_RULE_1_1 = V_CMJR_RULE
              ,SGBSTDN_DATA_ORIGIN = 'WorkFlow'
              ,SGBSTDN_USER_ID = USER
          WHERE SGBSTDN_PIDM = P_PIDM
          AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
          
          -- #######################################################################
          -- INSERT  ------> SGBSTDN -
          OPEN C_SGBSTDN;
          LOOP
              FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
              EXIT WHEN C_SGBSTDN%NOTFOUND;

                INSERT INTO SGBSTDN (
                          SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                          SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                          SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                          SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                          SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                          SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                          SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                          SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                          SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                          SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                          SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                          SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                          SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                          SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                          SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                          SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                          SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                          SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                          SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                          SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                          SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                          SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                          SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                          SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                          SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                          SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                          SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                          SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                          SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                          SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                          SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                          SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                          SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                          SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                          SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                          SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                          SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
                VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_TERM_CODE,                                 'AS', /*AS-Activo*/ 
                          V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            V_SGBSTDN_REC.SGBSTDN_STYP_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                          V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                          V_COLL_CODE,                                V_DEGC_CODE,                                  P_PROGRAM_NEW,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                          V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                          V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                          V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                          SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                          V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                          V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                          V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                          V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                          V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                          V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        P_PROGRAM_NEW,                                V_PER_CATALOGO,
                          V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                          V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                          V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                          V_CURR_RULE,                                V_CMJR_RULE,                                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                          V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                          V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                          V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                          USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
          END LOOP;
          CLOSE C_SGBSTDN;
          
          -- #######################################################################
          -- INSERT  ------> SORLCUR -
          OPEN C_SORLCUR;
          LOOP
              FETCH C_SORLCUR INTO V_SORLCUR_REC;
              EXIT WHEN C_SORLCUR%NOTFOUND;    
              
              -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
              SELECT MAX(SORLCUR_SEQNO + 1)
                INTO V_SORLCUR_SEQNO_INC
              FROM SORLCUR
              WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
              
              -- GET SORLCUR_SEQNO NUEVO 
              SELECT MAX(SORLCUR_SEQNO + 2)
                INTO V_SORLCUR_SEQNO_NEW
              FROM SORLCUR
              WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
              
              -- GET SORLCUR_SEQNO ANTERIOR 
              V_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;

              -- INACTIVE --- SORLCUR - (1 DE 2)
              INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                          V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          NULL /*SORLCUR_CURRENT_CDE*/ );
              
              -- ACTIVE --- SORLCUR - (2 DE 2)
              INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SGRSTSP_KEY_SEQNO,/*SGRSTSP*/             V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                          'WorkFlow',                             SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_COLL_CODE,                            V_DEGC_CODE,                                V_PER_CATALOGO /*SORLCUR_TERM_CODE_CTLG*/,
                          NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            P_PROGRAM_NEW,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          V_SORLCUR_REC.SORLCUR_CURRENT_CDE );
  
              
              -- UPDATE TERM_CODE_END (vigencia curriculum del Registro ANTERIOR) 
              UPDATE SORLCUR 
              SET   SORLCUR_TERM_CODE_END = P_TERM_CODE, 
                    SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
              WHERE   SORLCUR_PIDM = P_PIDM 
              AND     SORLCUR_LMOD_CODE = 'LEARNER'
              AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
              
              -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo)  -- Solo 1 curricula activa por PERIODO
              UPDATE SORLCUR 
              SET   SORLCUR_CURRENT_CDE = NULL
              WHERE   SORLCUR_PIDM = P_PIDM 
              AND     SORLCUR_LMOD_CODE = 'LEARNER'
              AND     SORLCUR_TERM_CODE_END = P_TERM_CODE -- En caso el registro sea del mismo Periodo
              AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;    
              
          END LOOP;
          CLOSE C_SORLCUR;
          
          -- #######################################################################
          -- INSERT --- SORLFOS - (1 DE 2) 
          INSERT INTO SORLFOS (
                SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                SORLFOS_CURRENT_CDE) 
          SELECT 
                V_SORLFOS_REC.SORLFOS_PIDM,             V_SORLCUR_SEQNO_INC,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                'CHANGED'/*SORLFOS_CSTS_CODE*/,         'INACTIVE'/*SORLFOS_CACT_CODE*/,          V_SORLFOS_REC.SORLFOS_DATA_ORIGIN,
                USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
                V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
                NULL /*SORLFOS_CURRENT_CDE*/
          FROM SORLFOS V_SORLFOS_REC
          WHERE SORLFOS_PIDM = P_PIDM
          AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
          AND ROWNUM = 1;
          
          -- INSERT --- SORLFOS - (2 DE 2)
          INSERT INTO SORLFOS (
                SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                SORLFOS_CURRENT_CDE) 
          SELECT 
                V_SORLFOS_REC.SORLFOS_PIDM,                 V_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                V_SORLFOS_REC.SORLFOS_LFST_CODE,            P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                V_SORLFOS_REC.SORLFOS_CSTS_CODE,            V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
                USER,                                       SYSDATE,                                  P_PROGRAM_NEW /*SORLFOS_MAJR_CODE*/,
                V_PER_CATALOGO /*SORLFOS_TERM_CODE_CTLG*/,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,     V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH,     V_CMJR_RULE /*SORLFOS_LFOS_RULE*/,        V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                V_SORLFOS_REC.SORLFOS_START_DATE,           V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,         USER,                                     SYSDATE,
                'Y'
          FROM SORLFOS V_SORLFOS_REC
          WHERE SORLFOS_PIDM = P_PIDM
          AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
          AND ROWNUM = 1;
           
          -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
          UPDATE SORLFOS 
          SET   SORLFOS_CURRENT_CDE = NULL
          WHERE   SORLFOS_PIDM = P_PIDM
          AND     SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
          AND     SORLFOS_CSTS_CODE IN ('INPROGRESS','STUDYPATH');
        
        ------------------------------------------------------------------------------------
        ------------------------------------------------------------------------------------
        -- ## Asugnacion de ATRIBUTO del plan 2015 en caso el cordinador(P_PER_CATALOGO) lo  haya asignado
        
            V_ATTS_CODE := 'P015'; --'P015' CODIGO de atributo del plan 2015
            IF(P_PER_CATALOGO = '2015') THEN
                  P_SET_ATRIBUTO_PLANS(P_PIDM, P_TERM_CODE, V_ATTS_CODE, P_ERROR);
            END IF;
        
        --
        COMMIT;
EXCEPTION
    WHEN V_MESSAGE THEN
          P_ERROR := '- El periodo de CATALOGO es obligatorio.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
    WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CAMBIAR_CARRERA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CREATE_CTACORRIENTE (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_CREATE_CTACORRIENTE
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asigna escala y crea la CTA Corriente de un alumno

MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   17/08/2017    RMALLQUI      Se cambio el sp_ActualizarMontoPagarBanner por sp_AsignarEscala_CtaCorriente que incluye
                                    la asignacion de escala y luego crea la cta corriente.
  002   23/08/2017    RMALLQUI      Se modifico la asignacion de escala y se agrego la asignacion de cargo por carnet
  003   06/11/2017    KARANA        Se agrego el filtro de "NuevoPlan=0" (P018)
  =================================================================================================================== */
AS
    -- @PARAMETERS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_SECCIONC         VARCHAR2(20);
    V_NUM_CTACORRIENTE      NUMBER;
    V_PART_PERIODO          VARCHAR2(9);
    V_RESULT                INTEGER;
    
    V_ESCALA                VARCHAR2(9);
    V_CRONOGRAMA            VARCHAR2(9);
    V_OBSERVACION           VARCHAR(255) := 'Asignacion de escala - Traslado Interno WORKFLOW';
BEGIN 
--
    --**************************************************************************************************************
    -- GET CRONOGRAMA SECCIONC
    SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
        INTO V_PART_PERIODO
    FROM CZRPTRM 
    WHERE CZRPTRM_DEPT = P_DEPT_CODE
    AND CZRPTRM_CAMP_CODE = P_CAMP_CODE;
    
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP 
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    -- Validar si el estudiante tiene CTA CORRIENTE con C00(CONCEPTO MATRICULA)
    WITH 
    CTE_tblSeccionC AS (
        -- GET SECCIONC
        SELECT  "IDSeccionC" IDSeccionC,
                "FecInic" FecInic
        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia"='UCCI'
        AND "IDsede"    = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuela" = P_PROGRAM_NEW
        AND LENGTH("IDSeccionC") = 5
        AND SUBSTRB("IDSeccionC",-2,2) IN (
            -- PARTE PERIODO           
            SELECT CZRPTRM_PTRM_BDUCCI
            FROM CZRPTRM
            WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
        )
    )
    SELECT COUNT("IDAlumno")
        INTO V_NUM_CTACORRIENTE
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno"    = P_ID_ALUMNO
    AND "IDSede"      = V_APEC_CAMP
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDEscuela"   = P_PROGRAM_NEW
    AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
    AND "IDConcepto" = 'C00'; -- C00 Concepto Matricula
    
    -- Get ESCALA y CRONOGRAMA
    SELECT "Cronograma", "Traslado" TRASLADO
        INTO V_CRONOGRAMA, V_ESCALA
    FROM tblCronogramas@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDepartamento" = P_DEPT_CODE
    AND "IDCampus" = P_CAMP_CODE
    AND "Activo" = 1
    AND "Pronabec" <> 1
    AND "NuevoPlan"= 0;

    -----------------------------------------------------------------------------------------------
    --- Asignacion de escala y Creacion CTA CORRIENTE.
    IF(V_NUM_CTACORRIENTE = 0) THEN
          ---------------------------------------
          /* OBSERVACION: Para pruebas acceder al SCRIPT y comentar y descomentar la conexcion de "AT BANNER" --> "AT MIGR"
                dentro del SP : sp_ActualizarMontoPagarBanner(YA INCLUIDO en el SP dbo.sp_AsignarEscala_CtaCorriente)
          */
          ---------------------------------------
          V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
                'dbo.sp_AsignarEscala_CtaCorriente "'
                || 'UCCI' ||'" , "'|| P_ID_ALUMNO ||'" , "'|| V_APEC_TERM ||'" , "'|| V_APEC_DEPT ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_ESCALA ||'" , "'|| V_CRONOGRAMA ||'" , "'|| V_OBSERVACION ||'" , "'|| USER ||'"' 
          );
    END IF;
    
    COMMIT;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CREATE_CTACORRIENTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

------------ APEC -----------------
PROCEDURE P_SET_CARGO_CONCEPTO ( 
        P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_APEC_CONCEPTO         IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
        P_PROGRAM_NEW           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
        P_MESSAGE               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CARGO_CONCEPTO
  FECHA     : 23/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : APEC-BDUCCI: Crea un registro al estudiante con el CONCEPTO.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
          -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_APEC_SECCIONC         VARCHAR2(20);
      V_PART_PERIODO          VARCHAR2(9);
      V_CARGO_CARNET          NUMBER := 0;
      V_MONTO_CAR             NUMBER := 0;
    
BEGIN
--    
    --**************************************************************************************************************
    -- GET CRONOGRAMA SECCIONC
    SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
        INTO V_PART_PERIODO
    FROM CZRPTRM 
    WHERE CZRPTRM_DEPT = P_DEPT_CODE
    AND CZRPTRM_CAMP_CODE = P_CAMP_CODE;
    
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    --Get SECCIONC
    WITH 
    CTE_tblSeccionC AS (
        -- GET SECCIONC
        SELECT  "IDSeccionC" IDSeccionC,
              "FecInic" FecInic
        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia"='UCCI'
        AND "IDsede"    = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuela" = P_PROGRAM_NEW
        AND LENGTH("IDSeccionC") = 5
        AND SUBSTRB("IDSeccionC",-2,2) IN (
            -- PARTE PERIODO           
            SELECT CZRPTRM_PTRM_BDUCCI
            FROM CZRPTRM
            WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
        )
    )
    SELECT "IDSeccionC"
        INTO V_APEC_SECCIONC
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno"    = P_ID
    AND "IDSede"      = V_APEC_CAMP
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDEscuela"   = P_PROGRAM_NEW
    AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
    AND "IDConcepto" = 'C00'; -- C00 Concepto Matricula
    
    /*******
     -- Get CARGO de CARNET "CAR"
    ********/
    SELECT  "Monto"
        INTO    V_CARGO_CARNET
    FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
    INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t2 ON 
        t1."IDSede" = t2."IDsede"
        AND t1."IDPerAcad" = t2."IDPerAcad" 
        AND t1."IDDependencia" = t2."IDDependencia" 
        AND t1."IDSeccionC" = t2."IDSeccionC" 
        AND t1."FecInic" = t2."FecInic"
    WHERE t2."IDDependencia" = 'UCCI'
    AND t2."IDSeccionC" = V_APEC_SECCIONC
    AND LENGTH(t2."IDSeccionC") = 5
    AND t2."IDPerAcad" = V_APEC_TERM
    AND T1."IDPerAcad" = V_APEC_TERM
    AND t1."IDConcepto" = P_APEC_CONCEPTO;

    SELECT "Cargo"
        INTO V_MONTO_CAR
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDDependencia" = 'UCCI' 
    AND "IDAlumno"    = P_ID
    AND "IDSede"      = V_APEC_CAMP 
    AND "IDPerAcad"   = V_APEC_TERM 
    AND "IDEscuela"   = P_PROGRAM_NEW
    AND "IDSeccionC"  = V_APEC_SECCIONC
    AND "IDConcepto"  = P_APEC_CONCEPTO;
    
    IF V_MONTO_CAR = 0 THEN
        /**********************************************************************************************
        -- ASINGANDO el cargo CARNET a CTACORRIENTE
        ********/
        UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
           SET "Cargo" = "Cargo" + V_CARGO_CARNET  
        WHERE "IDDependencia" = 'UCCI' 
        AND "IDAlumno"    = P_ID
        AND "IDSede"      = V_APEC_CAMP 
        AND "IDPerAcad"   = V_APEC_TERM 
        AND "IDEscuela"   = P_PROGRAM_NEW
        AND "IDSeccionC"  = V_APEC_SECCIONC
        AND "IDConcepto"  = P_APEC_CONCEPTO; 
        
        COMMIT;
    END IF;
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CARGO_CONCEPTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_ABONO_CTA (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_ABONO_OLD           OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_GET_ABONO_CTA
  FECHA     : 26/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el abono en una cta corriente especifica.
              
  MODIFICACIONES
  NRO       FECHA       USUARIO         MODIFICACION
  =================================================================================================================== */
AS
    -- @PARAMETERS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_PART_PERIODO          VARCHAR2(9);
    V_ABONO_OLD             NUMBER := 0;
BEGIN 
--
    --**************************************************************************************************************
    -- GET CRONOGRAMA SECCIONC
    SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
        INTO V_PART_PERIODO
    FROM CZRPTRM 
    WHERE CZRPTRM_DEPT = P_DEPT_CODE
    AND CZRPTRM_CAMP_CODE = P_CAMP_CODE;
    
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    -- Validar si el estuidnate tiene CTA CORRIENTE. como C00(CONCPETO MATRICULA)
    WITH 
    CTE_tblSeccionC AS (
        -- GET SECCIONC
        SELECT  "IDSeccionC" IDSeccionC,
                "FecInic" FecInic
        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia"='UCCI'
        AND "IDsede"    = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuela" = P_PROGRAM_OLD
        AND LENGTH("IDSeccionC") = 5
        AND SUBSTRB("IDSeccionC",-2,2) IN (
            -- PARTE PERIODO           
            SELECT CZRPTRM_PTRM_BDUCCI
            FROM CZRPTRM
            WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
        )
    ),
    CTE_tblPersonaAlumno AS (
        SELECT "IDAlumno" IDAlumno 
        FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDPersona" IN ( 
            SELECT "IDPersona" 
            FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDPersonaN" = P_PIDM
        )
    )
    SELECT SUM("Abono")
        INTO V_ABONO_OLD
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
    AND "IDSede"      = V_APEC_CAMP
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDEscuela"   = P_PROGRAM_OLD
    AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC);
    
    P_ABONO_OLD := CASE WHEN V_ABONO_OLD IS NULL THEN 0 ELSE V_ABONO_OLD END;
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ABONO_CTA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_REMOVE_CTA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_REMOVE_CTA
  FECHA     : 29/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Desabilita la cta corriente especifica por PIDM.
              
  MODIFICACIONES
  NRO       FECHA         USUARIO         MODIFICACION
  001       01/08/2017    rmallquo        Se agrego la eliminacion de la cuenta
  =================================================================================================================== */
AS
    -- @PARAMETERS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_PART_PERIODO          VARCHAR2(9);
    V_ABONO_OLD             NUMBER := 0;
    
    V_RESULT                INTEGER;
    V_FECINIC               DATE;
    V_APEC_ALUMNO           VARCHAR2(15);
    V_IDSECCIONC            VARCHAR2(10);
BEGIN 
--
    --**************************************************************************************************************
    -- GET CRONOGRAMA SECCIONC
    SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2)
        INTO V_PART_PERIODO
    FROM CZRPTRM 
    WHERE CZRPTRM_DEPT = P_DEPT_CODE
    AND CZRPTRM_CAMP_CODE = P_CAMP_CODE;

    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    -- GET  IDSECCIONC Y IDALUMNO CON SU CTA CORRIENTE
    WITH 
    CTE_tblSeccionC AS (
        -- GET SECCIONC
        SELECT  "IDSeccionC" IDSeccionC,
                "FecInic" FecInic
        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia"='UCCI'
        AND "IDsede"    = V_APEC_CAMP
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuela" = P_PROGRAM_OLD
        AND LENGTH("IDSeccionC") = 5
        AND SUBSTRB("IDSeccionC",-2,2) IN (
            -- PARTE PERIODO           
            SELECT CZRPTRM_PTRM_BDUCCI
            FROM CZRPTRM
            WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
        )
    ),
    CTE_tblPersonaAlumno AS (
        SELECT "IDAlumno" IDAlumno 
        FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDPersona" IN ( 
            SELECT "IDPersona"
            FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDPersonaN" = P_PIDM
        )
    )
    SELECT SUM("Abono") Abono, "IDSeccionC", "FecInic", "IDAlumno"
        INTO V_ABONO_OLD, V_IDSECCIONC, V_FECINIC, V_APEC_ALUMNO
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
    AND "IDSede"      = V_APEC_CAMP
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDEscuela"   = P_PROGRAM_OLD
    AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
    GROUP BY "IDSeccionC", "FecInic", "IDAlumno";
    
    -- Validar y DESABILITAR la cta corriente
    IF (V_ABONO_OLD IS NULL) THEN
        P_MESSAGE := ' No Se encontro la Cuenta Corriente';
    ELSIF(V_ABONO_OLD > 0) THEN
        P_MESSAGE := ' Se detecto ABONOS en la Cuenta Corriente.';
    ELSE 
         -- Eliminar la CTA CORRIENTE.(en caso no tenga abonos)
        ---------------------------------------    
        V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
              'dbo.sp_EliminarCtaCorriente "'
              || 'UCCI" , "'|| V_APEC_ALUMNO || '" , "' || V_APEC_TERM || '" , "' || V_APEC_CAMP ||'" , "'
              || V_IDSECCIONC ||'" , "'|| TO_CHAR(V_FECINIC, 'dd/mm/yyyy')  ||'" '
        );
        COMMIT;
    END IF;
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_REMOVE_CTA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CURSO_INTROD (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_MESSAGE             OUT VARCHAR2
)
AS
    V_STATUS_DATE_AP      SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
BEGIN

    -- Get la fecha de aprovación de la solicitud
    SELECT SVRSVPR_STATUS_DATE
        INTO V_STATUS_DATE_AP
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD 
        AND SVRSVPR_SRVS_CODE = 'AP';

    BWZKPSPG.P_SET_CURSO_INTROD (P_PIDM, P_TERM_CODE, V_STATUS_DATE_AP, P_DEPT_CODE, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CURSO_INTROD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIF_CTLG_CAMBIO(
     P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
     P_PER_CATALOGO    IN VARCHAR2,
     P_MESSAGE         OUT VARCHAR2,
     P_DESCRIP         OUT VARCHAR2
)
AS
     V_PER_CATALOGO    STVTERM.STVTERM_CODE%TYPE;
     V_PLAN            VARCHAR2(4);
BEGIN

    SELECT SORLCUR_TERM_CODE_CTLG
        INTO V_PER_CATALOGO
    FROM (
        SELECT SORLCUR_TERM_CODE_CTLG
        FROM SORLCUR
        INNER JOIN SORLFOS ON
            SORLCUR_PIDM = SORLFOS_PIDM 
            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_PIDM 
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y'
            AND SORLCUR_TERM_CODE_END IS NULL
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM = 1;
     
    IF (V_PER_CATALOGO >= '201510' AND V_PER_CATALOGO < '201810') THEN
        V_PLAN := '2015';
    ELSIF (V_PER_CATALOGO >= '200710' AND V_PER_CATALOGO < '201510') THEN
        V_PLAN := '2007';
    END IF;
     
    IF P_PER_CATALOGO = V_PLAN THEN
        P_MESSAGE := 'TRUE';
        P_DESCRIP := 'MANTIENE SU PLAN DE ESTUDIOS';
    ELSE
        P_MESSAGE := 'FALSE';
        P_DESCRIP := 'CAMBIA DE PLAN DE ESTUDIOS';
    END IF;
     
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIF_CTLG_CAMBIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_STI_CONV_ASIGN(
    P_TERM_CODE    IN STVTERM.STVTERM_CODE%TYPE,
    P_PIDM         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_MESSAGE      OUT VARCHAR2
)
AS   
    V_PROGRAM     SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_CATALOGO    STVTERM.STVTERM_CODE%TYPE:='201800';

BEGIN

    SELECT SORLCUR_PROGRAM
    INTO V_PROGRAM
    FROM (
        SELECT SORLCUR_PROGRAM
        FROM SORLCUR
        INNER JOIN SORLFOS ON
            SORLCUR_PIDM = SORLFOS_PIDM 
            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_PIDM 
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y'
            AND SORLCUR_TERM_CODE_END IS NULL
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM = 1;

    -- PACKAGE CONVALIDACION
    SZKCAEE.P_CONV_ASIGN_EQUIV_WF(P_TERM_CODE,V_CATALOGO,P_PIDM,V_PROGRAM,P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_STI_CONV_ASIGN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_STI_EXECCAPP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SZKECAP.P_EXEC_CAPP(P_PIDM,P_PROGRAM_NEW,P_TERM_CODE,LV_OUT);
    
    IF (LV_OUT <> '0') THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
WHEN P_EXCEPTION THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_STI_EXECCAPP;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_STI_EXECPROY (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_STI_EXECPROY
  FECHA     : 08/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Ejecucion de PROYECCION.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SZKECAP.P_EXEC_PROY(P_PIDM,P_PROGRAM_NEW,P_TERM_CODE,LV_OUT);
    
    IF (LV_OUT <> '0') THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
WHEN P_EXCEPTION THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_STI_EXECPROY;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_ASIG_RECONOCIDAS (
    P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_HTML1         OUT VARCHAR2,
    P_HTML2         OUT VARCHAR2,
    P_HTML3         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_GET_ASIG_RECONOCIDAS (P_PIDM, P_HTML1, P_HTML2, P_HTML3);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ASIG_RECONOCIDAS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

------- #### CREADO para el SP  P_SET_CAMBIAR_CARRERA  ### -----------
PROCEDURE P_SET_ATRIBUTO_PLANS (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,      -------- COD atributos
    P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_SET_ATRIBUTO_PLAN (P_PIDM, P_TERM_CODE, P_ATTS_CODE, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_ATRIBUTO_PLANS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKCSTI;

/**********************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**********************************************************************************************/