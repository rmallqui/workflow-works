/**************************************************************
        SP de BDUCCI ---------------> APEC
**************************************************************/

/* PERMISOS - IMPORTANTE */ 
--				grant execute on [dbo].[sp_EliminarCtaCorriente]  to [SSLSCDataCBanner]

/**************************************************************/

USE [BDUCCI]
GO
/* ===================================================================================================================
NOMBRE		: [dbo].[sp_EliminarCtaCorriente]
FECHA		: 01/08/2017
AUTOR		: Richard Mallqui Lopez 
OBJETIVO	: Eliminar LA CTACORRIENTE de un alumno validando que no tenga abonos.

MODIFICACIONES
NRO 	FECHA		USUARIO		MODIFICACION
=================================================================================================================== */
CREATE PROCEDURE [dbo].[sp_EliminarCtaCorriente]
	@c_IDDependencia	VARCHAR(100),
	@c_IDAlumno			VARCHAR(100),
	@c_IDPerAcad		VARCHAR(100),
	@c_IDSede			VARCHAR(100),
	@c_IDSeccionC		VARCHAR(100),
	@c_FecInic			VARCHAR(15)
AS
	DECLARE 
	   @c_Abono				decimal(9,2),
	   @c_DATE_FecInic      datetime

BEGIN	
-----	
	SET @c_DATE_FecInic     = CONVERT(DATETIME, @c_FecInic,103)

	--GET si se realizo abonos
	SELECT	@c_Abono = SUM(Abono) 
	FROM tblCtaCorriente 
	WHERE IDAlumno			= @c_IDAlumno 
		AND IDPerAcad		= @c_IDPerAcad
		AND IDDependencia	= @c_IDDependencia 
		AND IDSede			= @c_IDSede
		AND FecInic			= @c_DATE_FecInic 
		AND IDSeccionC		= @c_IDSeccionC
	
	
	SET NOCOUNT ON;
	SET FMTONLY OFF
	BEGIN TRY
		BEGIN TRANSACTION
			
			IF @c_Abono > 0 BEGIN
				RAISERROR (15600,-1,-1,'Inconsistencia detectada: No se puede eliminar la cuenta porque se realizaron abonos.'); 
			END;

			DELETE FROM tblAlumnoEstado
			WHERE IDAlumno			= @c_IDAlumno
				AND IDPerAcad		= @c_IDPerAcad
				AND IDSede			= @c_IDSede
				AND IDDependencia	= @c_IDDependencia
				AND FecInic			= @c_DATE_FecInic
				AND IDSeccionC		= @c_IDSeccionC

			DELETE FROM tblctacorriente
			WHERE IDAlumno			= @c_IDAlumno
				AND IDPerAcad		= @c_IDPerAcad
				AND IDSede			= @c_IDSede
				AND IDDependencia	= @c_IDDependencia
				AND FecInic			= @c_DATE_FecInic
				AND IDSeccionC		= @c_IDSeccionC

			COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

			--INSTRUCCIONES EN CASO DE ERRORES
			DECLARE @ErrorMessage NVARCHAR(4000);  
			DECLARE @ErrorSeverity INT;  
			DECLARE @ErrorState INT;  

			SELECT   
				@ErrorMessage = ERROR_MESSAGE(),  
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE();  

			-- Use RAISERROR inside the CATCH block to return error  
			-- information about the original error that caused  
			-- execution to jump to the CATCH block.  
			RAISERROR (
				@ErrorMessage, -- Message text.  
				@ErrorSeverity, -- Severity.  
				@ErrorState -- State.  
				);  
    
			ROLLBACK TRANSACTION

	END CATCH
END

GO 

grant execute on [dbo].[sp_EliminarCtaCorriente]  to [SSLSCDataCBanner]