/*
SET SERVEROUTPUT ON
DECLARE P_ABONO_OLD NUMBER;
BEGIN
    P_GET_ABONO_CTA(326495,'43317482','UPGT','S01','201710','105',P_ABONO_OLD);
    DBMS_OUTPUT.PUT_LINE(P_ABONO_OLD);
    
    P_GET_ABONO_CTA(127673,'41453900','UREG','S01','201710','315',P_ABONO_OLD);
    DBMS_OUTPUT.PUT_LINE(P_ABONO_OLD);
END;
*/

CREATE OR REPLACE PROCEDURE P_GET_ABONO_CTA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_ABONO_OLD           OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_GET_ABONO_CTA
  FECHA     : 26/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el abono en una cta corriente especifica.
              
  MODIFICACIONES
  NRO       FECHA       USUARIO         MODIFICACION
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_PART_PERIODO          VARCHAR2(9);
      V_ABONO_OLD             NUMBER := 0;
BEGIN 
--
        --**************************************************************************************************************
        -- GET CRONOGRAMA SECCIONC
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                  WHEN 'UPGT' THEN 'W' 
                                  WHEN 'UREG' THEN 'R' 
                                  WHEN 'UPOS' THEN '-' 
                                  WHEN 'ITEC' THEN '-' 
                                  WHEN 'UCIC' THEN '-' 
                                  WHEN 'UCEC' THEN '-' 
                                  WHEN 'ICEC' THEN '-' 
                                  ELSE '1' END ||
                CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                  WHEN 'F01' THEN 'A' 
                                  WHEN 'F02' THEN 'L' 
                                  WHEN 'F03' THEN 'C' 
                                  WHEN 'V00' THEN 'V' 
                                  ELSE '9' END
        INTO V_PART_PERIODO
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
        
        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
        
        -- Validar si el estuidnate tiene CTA CORRIENTE. como C00(CONCPETO MATRICULA)
        WITH 
            CTE_tblSeccionC AS (
                    -- GET SECCIONC
                    SELECT  "IDSeccionC" IDSeccionC,
                            "FecInic" FecInic
                    FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDDependencia"='UCCI'
                    AND "IDsede"    = V_APEC_CAMP
                    AND "IDPerAcad" = V_APEC_TERM
                    AND "IDEscuela" = P_PROGRAM_OLD
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                    AND "IDSeccionC" <> '15NEX1A'
                    AND SUBSTRB("IDSeccionC",-2,2) IN (
                        -- PARTE PERIODO           
                        SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                    )
            ),
            CTE_tblPersonaAlumno AS (
                    SELECT "IDAlumno" IDAlumno 
                    FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDPersona" IN ( 
                        SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM
                    )
            )
        SELECT SUM("Abono") INTO V_ABONO_OLD
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
        AND "IDSede"      = V_APEC_CAMP
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDEscuela"   = P_PROGRAM_OLD
        AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC);
        
        P_ABONO_OLD := CASE WHEN V_ABONO_OLD IS NULL THEN 0 ELSE V_ABONO_OLD END;
        
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ABONO_CTA;