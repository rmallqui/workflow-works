APUNTES
=================================================================================
ENALCE COMPARTIDO: https://drive.google.com/open?id=0B6g9PRwmQiqoVUswS3hSOXNCX2M

Se creo un paquete en la tarea 2: contendra en orden todas las funciones, procedimeintos creados para este fin.

	######## SOLICITUD DE REINCORPORACIÒN ########
		
			- Nombre del paquete : 			BWZKCSTI
			- Cuerpo del pakquete :			BWZKCSTI BODY


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 00 --> carpeta 0 : TABLA INFORMATIVA - CONFIGURACION SOLICITU DE SERVICIO
---------------------------------------------------------------------------------------------------------------------------------
	
	- VISTA: Para el listado de carrera del alumno  .............. "SWVSTIP"
	- VISTA: Opiones para el usuario responsable ................. "SWVSTIC"
	

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 1 --> carpeta 01 : TRIGGER - FUNCIONE 
---------------------------------------------------------------------------------------------------------------------------------
	- Se elaboro triguer, Trigguer-ST_SVRSVPR_POST_INSERT_ROW --  ('WORKFLOW','WFSOL013');

				- se modificò el orden de los parametros
								P_PIDM_ALUMNO	
								P_ID_ALUMNO
								P_PERIODO
								P_CORREO_ALUMNO
								P_NOMBRE_ALUMNO
								P_FOLIO_SOLICITUD
								P_FECHA_SOLICITUD
								P_COMENTARIO_ALUMNO
								P_MODALIDAD_ALUMNO
								P_COD_SEDE
								P_NOMBRE_SEDE

	-- FUNCION: Adicional a las reglas de servicio (EN LA CARPETA 00-WF-FUNCIONES)

				WFK_OAFORM.FUNCTION F_RSS_STI (
				    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
				    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
				    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
				 );


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 2 --> carpeta 02 : Obtener usuario 
---------------------------------------------------------------------------------------------------------------------------------
				PROCEDURE P_GET_USUARIO (
			            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
			            P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
			            P_CORREO              OUT VARCHAR2,
			            P_ROL_SEDE            OUT VARCHAR2,
			            P_ERROR               OUT VARCHAR2
			      );



---------------------------------------------------------------------------------------------------------------------------------
TAREAS 3 --> carpeta 03 : Obtener rol cordinador
			---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_GETUSER_COORDINADOR (
			            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sencible mayuscula 'S01'
			            P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE, -- sencible a mayusculas 'coordinador'
			            P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			            P_ROL_COORDINADOR     OUT VARCHAR2,
			            P_ROL_MAILS           OUT VARCHAR2
			      );


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 3 --> carpeta 04 : cambio de estado solicitud
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
			          P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
			          P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
			          P_AN                  OUT NUMBER,
			          P_ERROR               OUT VARCHAR2
		      );


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 5 --> carpeta 05 - SET CODIGO DETALLE (DEUDA)
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_SET_CODDETALLE (  
			        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
			        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
			        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
			        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
			        P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
			        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
			        P_DATE_CARGO            OUT VARCHAR2,
			        P_ERROR                 OUT VARCHAR2
			)


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 6 --> carpeta 06 - GET SI DEDUDA DTA
---------------------------------------------------------------------------------------------------------------------------------
				--------- SOLO APEC - VALIDA DOS CONCEPTOS
				PROCEDURE P_GET_SIDEUDA_DETLS (
				      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
				      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE, --------------- APEC
				      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, ------------- APEC
				      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE, ------------- APEC
				      P_DETL_1CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
				      P_DETL_2CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
				      P_DEUDA_CODE          OUT NUMBER
				)        


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 7 --> carpeta 07 -  DEL CODIGO DETALLE
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_DEL_CODDETALLE ( 
			        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
			        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
			        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
			        P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, 
			        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
			        P_ERROR                 OUT VARCHAR2
			)


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 8 --> carpeta 08 - VALIDAR FECHA DISPONIBLE
---------------------------------------------------------------------------------------------------------------------------------
			FUNCTION F_VALIDAR_FECHA_DISPONIBLE (
			        P_DATE   IN VARCHAR2
			) RETURN VARCHAR2



---------------------------------------------------------------------------------------------------------------------------------
TAREAS 9 --> carpeta 09 - P_GET_STUDENT_INPUTS
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_GET_STUDENT_INPUTS (
			      P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
			      P_PROGRAM_OLD           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
			      P_PROGRAM_OLD_DESC      OUT VARCHAR2,
			      P_PROGRAM_NEW           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
			      P_PROGRAM_NEW_DESC      OUT VARCHAR2,
			      P_COMENTARIO_ALUMNO     OUT VARCHAR2,
			      P_TELEFONO_ALUMNO       OUT VARCHAR2,
			      P_OPT_CONSEJERIA        OUT VARCHAR2
			)



---------------------------------------------------------------------------------------------------------------------------------
TAREAS 10 --> carpeta 10 -  GET DIRECTOR ESCUELA
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_GET_CORDINADOR_PROGRAM (
			      P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			      P_ID                OUT SPRIDEN.SPRIDEN_ID%TYPE,
			      P_NOMBREC           OUT VARCHAR2,
			      P_CORREO            OUT VARCHAR2,
			      P_PHONE             OUT VARCHAR2
			)


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 11 --> carpeta 11 -  Set Nueva Carrera
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_SET_CAMBIAR_CARRERA ( 
			      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
			      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
			      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
			      P_PER_CATALOGO        IN VARCHAR2,
			      P_ERROR               OUT VARCHAR2
			)



---------------------------------------------------------------------------------------------------------------------------------
TAREAS 12 --> carpeta 12 -  CREATE NUEVA CTA CORRIENTE
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_CREATE_CTACORRIENTE (
			      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
			      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
			      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
			      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
			      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE
			)

		--  Se elaboro un SP "sp_AsignarEscala_CtaCorriente" en BDUCCI(APEC) 
			para la CREACION de la cta-corriente

			20170817___APEC---dbo,sp_AsignarEscala_CtaCorriente.sql


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 13 --> carpeta 13 -  SET CARGO CONCEPTO
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_SET_CARGO_CONCEPTO ( 
			        P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
			        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
			        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
			        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
			        P_APEC_CONCEPTO         IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
			        P_PROGRAM_NEW           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			        P_MESSAGE               OUT VARCHAR2
			)

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 14 --> carpeta 14 -  GET ABONO CTA CORRIENTE 
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_GET_ABONO_CTA (
			      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
			      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
			      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
			      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
			      P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			      P_ABONO_OLD           OUT NUMBER
			)



---------------------------------------------------------------------------------------------------------------------------------
TAREAS 15 --> carpeta 15 -  GET ABONO CTA CORRIENTE 
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_REMOVE_CTA (
			      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
			      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
			      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
			      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
			      P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			      P_MESSAGE             OUT VARCHAR2
			)

		--  Se elaboro un SP "sp_EliminarCtaCorriente" en BDUCCI(APEC)  para la eliminación de la cta-corriente

			20170801---BDUCCI---sp_EliminarCtaCorriente.sql


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 16 --> carpeta 16 -  SET CURSOS INTRODUCTORIOS
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_SET_CURSO_INTROD (
			      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
			      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
			      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
			      P_MESSAGE             OUT VARCHAR2
			)


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 17 --> carpeta 17 -  EJECUCION CAPP
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_STI_EXECCAPP (
			            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			            P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
			            P_MESSAGE           OUT VARCHAR2
			      );


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 18 --> carpeta 18 -  EJECUCION PROYECCION
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_STI_EXECPROY (
			            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			            P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
			            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
			            P_MESSAGE           OUT VARCHAR2
			      );


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 19 --> carpeta 19 -  GET ASIGNATIRUAS RECONOCIDAS
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_GET_ASIG_RECONOCIDAS (
			      P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			      P_HTML1         OUT VARCHAR2,
			      P_HTML2         OUT VARCHAR2,
			      P_HTML3         OUT VARCHAR2
			)

	- Se elaboro unatabla HTML para incluir en el correo de respuesta 

			Tabla-report-alum-20170323.html


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 20 --> carpeta 20 - Set Atributo - Al Crear Nueva Carrera-11-
---------------------------------------------------------------------------------------------------------------------------------
			------- #### CREADO para el SP  P_SET_CAMBIAR_CARRERA  ### -----------
			PROCEDURE P_SET_ATRIBUTO_PLANS (
			      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
			      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
			      P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE      -------- COD atributos
			)



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

EVENTOS: