/*
SET SERVEROUTPUT ON
DECLARE P_FECHA_VALIDA VARCHAR2(10);
BEGIN
    P_CREATE_CTACORRIENTE(326495,'43317482','UPGT','S01','201710','105');
END;
*/


CREATE OR REPLACE PROCEDURE P_CREATE_CTACORRIENTE (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_NUM_CTACORRIENTE      NUMBER;
      V_PART_PERIODO          VARCHAR2(9);
      V_RESULT                INTEGER;
BEGIN 
--
        --**************************************************************************************************************
        -- GET CRONOGRAMA SECCIONC
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                  WHEN 'UPGT' THEN 'W' 
                                  WHEN 'UREG' THEN 'R' 
                                  WHEN 'UPOS' THEN '-' 
                                  WHEN 'ITEC' THEN '-' 
                                  WHEN 'UCIC' THEN '-' 
                                  WHEN 'UCEC' THEN '-' 
                                  WHEN 'ICEC' THEN '-' 
                                  ELSE '1' END ||
                CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                  WHEN 'F01' THEN 'A' 
                                  WHEN 'F02' THEN 'L' 
                                  WHEN 'F03' THEN 'C' 
                                  WHEN 'V00' THEN 'V' 
                                  ELSE '9' END
        INTO V_PART_PERIODO
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
        
        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
        
        -- Validar si el estuidnate tiene CTA CORRIENTE. como C00(CONCPETO MATRICULA)
        WITH 
            CTE_tblSeccionC AS (
                    -- GET SECCIONC
                    SELECT  "IDSeccionC" IDSeccionC,
                            "FecInic" FecInic
                    FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDDependencia"='UCCI'
                    AND "IDsede"    = V_APEC_CAMP
                    AND "IDPerAcad" = V_APEC_TERM
                    AND "IDEscuela" = P_PROGRAM_NEW
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                    AND "IDSeccionC" <> '15NEX1A'
                    AND SUBSTRB("IDSeccionC",-2,2) IN (
                        -- PARTE PERIODO           
                        SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                    )
            )
        SELECT COUNT("IDAlumno") INTO V_NUM_CTACORRIENTE
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno"    = P_ID_ALUMNO
        AND "IDSede"      = V_APEC_CAMP
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDEscuela"   = P_PROGRAM_NEW
        AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
        AND "IDConcepto" = 'C00'; -- C00 Concepto Matricula

        --- Creacion CTA CORRIENTE.
        IF(V_NUM_CTACORRIENTE = 0) THEN
              -- ACTUALIZAR MONTO PAGAR (CREDITOS 10)
              ---------------------------------------
              -- OBSERVACIÒN: Para pruebas acceder al SCRIPT y comentar y descomentar la conexcion de "AT BANNER" --> "AT MIGR"
              ---------------------------------------
              V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
                    'dbo.sp_ActualizarMontoPagarBanner "'
                    || 'UCCI' ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_APEC_TERM ||'" , "'|| P_ID_ALUMNO ||'"' 
              );
        END IF;
        
        COMMIT;
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CREATE_CTACORRIENTE;