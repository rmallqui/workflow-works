/*
select dbms_metadata.get_ddl('PROCEDURE','P_CAMBIAR_CARRERA') from dual

SET SERVEROUTPUT ON
DECLARE P_ERROR                   VARCHAR2(100);
BEGIN
    P_SET_CAMBIAR_CARRERA(326495,'110','UREG','S01','201710','2015',P_ERROR); -- 43317482
    DBMS_OUTPUT.PUT_LINE('ERROR :' || P_ERROR);
END;
*/
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

CREATE OR REPLACE PROCEDURE P_SET_CAMBIAR_CARRERA ( 
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PER_CATALOGO        IN VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_CAMBIAR_ESTATUS
  FECHA     : 18/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cambia la carrera al estudiante.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        V_TERM_CODE_LAST          SFBETRM.SFBETRM_TERM_CODE%TYPE;
        V_CURR_RULE               SOBCURR.SOBCURR_CURR_RULE%TYPE;
        V_COLL_CODE               SOBCURR.SOBCURR_COLL_CODE%TYPE;
        V_DEGC_CODE               SOBCURR.SOBCURR_DEGC_CODE%TYPE;
        V_CMJR_RULE               SORCMJR.SORCMJR_CMJR_RULE%TYPE;
        V_PER_CATALOGO            VARCHAR2(6);
        V_MESSAGE                 EXCEPTION;
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SGRSTSP IS
          SELECT * FROM (
              SELECT *  FROM SGRSTSP 
              WHERE SGRSTSP_PIDM = P_PIDM
              AND SGRSTSP_TERM_CODE_EFF = P_TERM_CODE
              ORDER BY SGRSTSP_KEY_SEQNO DESC
          ) WHERE ROWNUM = 1;
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SGBSTDN IS
        SELECT * FROM (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM
            AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE
         );
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SORLCUR IS
        SELECT * FROM (
            SELECT * FROM SORLCUR 
            WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        )WHERE ROWNUM = 1;
        
        V_SGRSTSP_KEY_SEQNO     SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
        V_SGRSTSP_REC           SGRSTSP%ROWTYPE;
        V_SGBSTDN_REC           SGBSTDN%ROWTYPE;
        V_SORLCUR_REC           SORLCUR%ROWTYPE;
        V_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE;
        V_SORLCUR_SEQNO_INC     SORLCUR.SORLCUR_SEQNO%TYPE;
        V_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE;
        
        V_INDICADOR             NUMBER;
BEGIN
--        
          -- Calculando periodo CATALOGO
          V_PER_CATALOGO := CASE P_PER_CATALOGO WHEN '2015' THEN '201710'
                                                WHEN '2007' THEN '201420'
                                                ELSE NULL END;
          IF V_PER_CATALOGO IS NULL THEN
                  RAISE V_MESSAGE;
          END IF;
          
          -- #######################################################################
          -- INSERT  ------> PLAN ESTUDIO -
          OPEN C_SGRSTSP;
          LOOP
              FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
              EXIT WHEN C_SGRSTSP%NOTFOUND;
              
              -- GET SGRSTSP_KEY_SEQNO NUEVO
              V_SGRSTSP_KEY_SEQNO := V_SGRSTSP_REC.SGRSTSP_KEY_SEQNO + 1;
              
              INSERT INTO SGRSTSP (
                      SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                      SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                      SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                      SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                      SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                      SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                      SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                      SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                      SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
              SELECT 
                      V_SGRSTSP_REC.SGRSTSP_PIDM,       P_TERM_CODE,                          V_SGRSTSP_KEY_SEQNO,
                      'AS',                             SYSDATE,                              'WorkFlow',
                      USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                      V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                      V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                      V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                      V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
              FROM DUAL;
              
          END LOOP;
          CLOSE C_SGRSTSP;
          
          -- #######################################################################
          -- Get Reglas de Curriculum (CURR_RULE)
          SELECT  SOBCURR_CURR_RULE,  SOBCURR_COLL_CODE,  SOBCURR_DEGC_CODE,  SORCMJR_CMJR_RULE
          INTO    V_CURR_RULE,        V_COLL_CODE,        V_DEGC_CODE,        V_CMJR_RULE
          FROM (
                SELECT * FROM SOBCURR
                INNER JOIN (
                    SELECT  SOBCURR_CURR_RULE CURR_RULE, SOBCURR_CAMP_CODE CAMP_CODE, SOBCURR_COLL_CODE COLL_CODE, 
                            SOBCURR_DEGC_CODE DEGC_CODE, SOBCURR_PROGRAM CPROGRAM, MAX(SOBCURR_TERM_CODE_INIT) TERM_CODE_INIT
                    FROM SOBCURR 
                    WHERE SOBCURR_LEVL_CODE = 'PG' 
                    GROUP BY SOBCURR_CURR_RULE, SOBCURR_CAMP_CODE, SOBCURR_COLL_CODE, SOBCURR_DEGC_CODE, SOBCURR_PROGRAM
                )ON SOBCURR_CURR_RULE = CURR_RULE
                AND SOBCURR_CAMP_CODE = CAMP_CODE
                AND SOBCURR_COLL_CODE = COLL_CODE
                AND SOBCURR_DEGC_CODE = DEGC_CODE
                AND SOBCURR_PROGRAM = CPROGRAM
                AND SOBCURR_TERM_CODE_INIT = TERM_CODE_INIT
          ) 
          INNER JOIN (
                SELECT * FROM SORCMJR
                INNER JOIN (
                    SELECT SORCMJR_CURR_RULE CURR_RULE, SORCMJR_MAJR_CODE MAJR_CODE, SORCMJR_DEPT_CODE DEPT_CODE, MAX(SORCMJR_TERM_CODE_EFF) TERM_CODE_EFF
                    FROM SORCMJR 
                    GROUP BY SORCMJR_CURR_RULE, SORCMJR_MAJR_CODE, SORCMJR_DEPT_CODE
                )ON SORCMJR_CURR_RULE = CURR_RULE
                AND SORCMJR_MAJR_CODE = MAJR_CODE
                AND SORCMJR_DEPT_CODE = DEPT_CODE
                AND SORCMJR_TERM_CODE_EFF = TERM_CODE_EFF
          )
          ON SOBCURR_CURR_RULE = SORCMJR_CURR_RULE
          WHERE SOBCURR_CAMP_CODE = P_CAMP_CODE
          AND SOBCURR_LEVL_CODE = 'PG'
          AND SORCMJR_DEPT_CODE = P_DEPT_CODE
          AND SOBCURR_PROGRAM = P_PROGRAM_NEW;

          
          -- UPDATE SGASTDN
          UPDATE SGBSTDN
          SET  SGBSTDN_STST_CODE = 'AS'  ------------------ ESTADOS STVSTST : 'AS' - Activo
              ,SGBSTDN_STYP_CODE = 'C'   ------------------ ESTADOS STVSTYP : 'C' - Regular
              ,SGBSTDN_COLL_CODE_1 = V_COLL_CODE
              ,SGBSTDN_DEGC_CODE_1 = V_DEGC_CODE
              ,SGBSTDN_MAJR_CODE_1 = P_PROGRAM_NEW
              ,SGBSTDN_ACTIVITY_DATE = SYSDATE
              ,SGBSTDN_PROGRAM_1 = P_PROGRAM_NEW
              ,SGBSTDN_TERM_CODE_CTLG_1 = V_PER_CATALOGO
              ,SGBSTDN_CURR_RULE_1 = V_CURR_RULE
              ,SGBSTDN_CMJR_RULE_1_1 = V_CMJR_RULE
              ,SGBSTDN_DATA_ORIGIN = 'WorkFlow'
              ,SGBSTDN_USER_ID = USER
          WHERE SGBSTDN_PIDM = P_PIDM
          AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
          
          -- #######################################################################
          -- INSERT  ------> SGBSTDN -
          OPEN C_SGBSTDN;
          LOOP
              FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
              EXIT WHEN C_SGBSTDN%NOTFOUND;

                INSERT INTO SGBSTDN (
                          SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                          SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                          SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                          SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                          SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                          SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                          SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                          SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                          SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                          SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                          SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                          SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                          SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                          SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                          SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                          SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                          SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                          SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                          SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                          SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                          SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                          SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                          SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                          SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                          SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                          SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                          SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                          SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                          SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                          SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                          SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                          SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                          SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                          SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                          SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                          SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                          SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
                VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_TERM_CODE,                                 'AS', /*AS-Activo*/ 
                          V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            'C',/*C-Regilar*/                             V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                          V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                          V_COLL_CODE,                                V_DEGC_CODE,                                  P_PROGRAM_NEW,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                          V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                          V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                          V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                          SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                          V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                          V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                          V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                          V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                          V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                          V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        P_PROGRAM_NEW,                                V_PER_CATALOGO,
                          V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                          V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                          V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                          V_CURR_RULE,                                V_CMJR_RULE,                                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                          V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                          V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                          V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                          USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
          END LOOP;
          CLOSE C_SGBSTDN;
          
          -- #######################################################################
          -- INSERT  ------> SORLCUR -
          OPEN C_SORLCUR;
          LOOP
              FETCH C_SORLCUR INTO V_SORLCUR_REC;
              EXIT WHEN C_SORLCUR%NOTFOUND;    
              
              -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
              V_SORLCUR_SEQNO_INC := V_SORLCUR_REC.SORLCUR_SEQNO + 1;
              -- GET SORLCUR_SEQNO NUEVO 
              V_SORLCUR_SEQNO_NEW := V_SORLCUR_REC.SORLCUR_SEQNO + 2;
              -- GET SORLCUR_SEQNO ANTERIOR 
              V_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;
              
              -- INACTIVE --- SORLCUR - (1 DE 2)
              INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SGRSTSP_KEY_SEQNO,/*SGRSTSP*/             V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                          V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          NULL /*SORLCUR_CURRENT_CDE*/ );
              
              -- ACTIVE --- SORLCUR - (2 DE 2)
              INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SGRSTSP_KEY_SEQNO,/*SGRSTSP*/             V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                          V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_COLL_CODE,                            V_DEGC_CODE,                                V_PER_CATALOGO /*SORLCUR_TERM_CODE_CTLG*/,
                          NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            P_PROGRAM_NEW,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          V_SORLCUR_REC.SORLCUR_CURRENT_CDE );
  
              
              -- UPDATE TERM_CODE_END (vigencia curriculum del Registro ANTERIOR) 
              UPDATE SORLCUR 
              SET   SORLCUR_TERM_CODE_END = P_TERM_CODE, 
                    SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
              WHERE   SORLCUR_PIDM = P_PIDM 
              AND     SORLCUR_LMOD_CODE = 'LEARNER'
              AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
              
              -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo)  PARA registros del mismo PERIODO.
              UPDATE SORLCUR 
              SET   SORLCUR_CURRENT_CDE = NULL
              WHERE   SORLCUR_PIDM = P_PIDM 
              AND     SORLCUR_LMOD_CODE = 'LEARNER'
              AND     SORLCUR_TERM_CODE_END = P_TERM_CODE
              AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;    
              
          END LOOP;
          CLOSE C_SORLCUR;
          
          -- #######################################################################
          -- INSERT --- SORLFOS - (1 DE 2) 
          INSERT INTO SORLFOS (
                SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                SORLFOS_CURRENT_CDE) 
          SELECT 
                V_SORLFOS_REC.SORLFOS_PIDM,             V_SORLCUR_SEQNO_INC,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                'CHANGED'/*SORLFOS_CSTS_CODE*/,         'INACTIVE'/*SORLFOS_CACT_CODE*/,          V_SORLFOS_REC.SORLFOS_DATA_ORIGIN,
                USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
                V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
                NULL /*SORLFOS_CURRENT_CDE*/
          FROM SORLFOS V_SORLFOS_REC
          WHERE SORLFOS_PIDM = P_PIDM
          AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
          
          -- INSERT --- SORLFOS - (2 DE 2)
          INSERT INTO SORLFOS (
                SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                SORLFOS_CURRENT_CDE) 
          SELECT 
                V_SORLFOS_REC.SORLFOS_PIDM,                 V_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                V_SORLFOS_REC.SORLFOS_LFST_CODE,            P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                V_SORLFOS_REC.SORLFOS_CSTS_CODE,            V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
                USER,                                       SYSDATE,                                  P_PROGRAM_NEW /*SORLFOS_MAJR_CODE*/,
                V_PER_CATALOGO /*SORLFOS_TERM_CODE_CTLG*/,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,     V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH,     V_CMJR_RULE /*SORLFOS_LFOS_RULE*/,        V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                V_SORLFOS_REC.SORLFOS_START_DATE,           V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,         USER,                                     SYSDATE,
                'Y'
          FROM SORLFOS V_SORLFOS_REC
          WHERE SORLFOS_PIDM = P_PIDM
          AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
           
          -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
          UPDATE SORLFOS 
          SET   SORLFOS_CURRENT_CDE = NULL
          WHERE   SORLFOS_PIDM = P_PIDM
          AND     SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
          AND     SORLFOS_CSTS_CODE = 'INPROGRESS';
        
        ------------------------------------------------------------------------------------
        
        --
        COMMIT;
EXCEPTION
    WHEN V_MESSAGE THEN
          P_ERROR := '- El periodo de CATALOGO es obligatorio.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
    WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CAMBIAR_CARRERA;   
      
