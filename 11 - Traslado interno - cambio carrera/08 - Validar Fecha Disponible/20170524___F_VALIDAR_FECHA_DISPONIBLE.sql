/*
SET SERVEROUTPUT ON
DECLARE P_FECHA_VALIDA VARCHAR2(30);
BEGIN
    SELECT F_VALIDAR_FECHA_DISPONIBLE('25/05/2017 11:42:37') INTO P_FECHA_VALIDA FROM DUAL;
    DBMS_OUTPUT.PUT_LINE(P_FECHA_VALIDA);
END;
*/

create or replace FUNCTION F_VALIDAR_FECHA_DISPONIBLE (
        P_DATE   IN VARCHAR2
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_VALIDAR_FECHA_DISPONIBLE
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida que la fecha ENVIADA este dentro de las 48 horas para su atencion.
              se considdera la atencion hasta antes de finalizar el dia de finalizada las 48 horas

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS    
    V_RECEPTION_DATE          DATE;
BEGIN
--
      -- CONVERTIR LA FECHA A UN FORMATO VALIDO
      --SELECT TO_DATE('25/05/2017 11:02:47','dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = American') 
      SELECT TO_DATE(P_DATE,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = American') 
      INTO V_RECEPTION_DATE
      FROM DUAL;

    -- Valida que la fecha este SEA MENOR A 3 DIAS DE TOLERANCIA.
    IF(SYSDATE < TO_DATE(TO_CHAR(V_RECEPTION_DATE + 3,'dd/mm/yyyy'),'dd/mm/yyyy')) THEN
        RETURN 'TRUE';
    ELSE
        RETURN 'FALSE';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END F_VALIDAR_FECHA_DISPONIBLE;  