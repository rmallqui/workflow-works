/*
-- buscar objecto
select dbms_metadata.get_ddl('PROCEDURE','P_SET_CODDETALLE') from dual
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

SET SERVEROUTPUT ON
DECLARE   P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
          P_ERROR                 VARCHAR2(4000);
          p_date                  DATE;
BEGIN
    P_SET_CODDETALLE(241106,'74288044','UREG','S01','201710','C01',P_TRAN_NUMBER,p_date,P_ERROR);
    DBMS_OUTPUT.PUT_LINE(p_date  || '--' || P_ERROR);
END;
*/


CREATE OR REPLACE PROCEDURE P_SET_CODDETALLE ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_DATE_CARGO            OUT VARCHAR2,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CODDETALLE
  FECHA     : 22/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE.
              APEC-BDUCCI: Crea un registro al estudiante con el CONCEPTO.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_NOM_SERVICIO            VARCHAR2(30);
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    V_DATENOW                 DATE := SYSDATE;
    
    V_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;          
    V_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
    V_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
    V_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
    V_ALUM_TERM_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
    V_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP 
    V_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE

    V_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
    V_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
    
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORLCUR_FOS IS
    SELECT    SORLCUR_LEVL_CODE,        
              SORLCUR_COLL_CODE,
              SORLCUR_DEGC_CODE,  
              SORLCUR_PROGRAM,      
              SORLCUR_TERM_CODE_ADMIT,  
              SORLCUR_STYP_CODE,  
              SORLCUR_RATE_CODE  
    FROM (
            SELECT    SORLCUR_LEVL_CODE,    SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,  
                      SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  SORLCUR_STYP_CODE,  
                      SORLCUR_RATE_CODE
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM        =   P_PIDM
                AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;
    
    -- APEC PARAMS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    V_APEC_IDCUENTA         VARCHAR2(10);
    V_APEC_MONEDA           VARCHAR2(10);
    V_APEC_MOUNT            NUMBER;
    V_APEC_IDSECCIONC_INIC     DATE;
    V_APEC_IDSECCIONC_VENC     DATE;
    
BEGIN

      -- #######################################################################
      -- >> GET DATOS --
      OPEN C_SORLCUR_FOS;
      LOOP
        FETCH C_SORLCUR_FOS INTO  V_ALUM_NIVEL,     V_ALUM_ESCUELA,   V_ALUM_GRADO,
                                  V_ALUM_PROGRAMA,  V_ALUM_TERM_ADM,  V_ALUM_TIPO_ALUM, V_ALUM_TARIFA;
        EXIT WHEN C_SORLCUR_FOS%NOTFOUND;
      END LOOP;
      CLOSE C_SORLCUR_FOS;
      

      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)

            SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
            SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO

            
            SELECT  t1."IDSeccionC",   
                    t1."FecInic",           
                    "FecVenc",              
                    "Monto",      
                    "IDCuenta",      
                    t1."Moneda"
            INTO    V_APEC_IDSECCIONC,  
                    V_APEC_IDSECCIONC_INIC, 
                    V_APEC_IDSECCIONC_VENC, 
                    V_APEC_MOUNT, 
                    V_APEC_IDCUENTA, 
                    V_APEC_MONEDA
            FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
            INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
              ON t1."IDSede"          = t2."IDsede"
              AND t1."IDPerAcad"      = t2."IDPerAcad" 
              AND t1."IDDependencia"  = t2."IDDependencia" 
              AND t1."IDSeccionC"     = t2."IDSeccionC" 
              AND t1."FecInic"        = t2."FecInic"
            WHERE t2."IDDependencia" = 'UCCI'
              -- AND t2."IDsede"        = V_APEC_CAMP -- Para la SeccionC especial es indistinto la SEDE
              AND t2."IDSeccionC"    = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
              AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
              AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
              AND t2."IDSeccionC"     <> '15NEX1A'
              AND t2."IDPerAcad"     = V_APEC_TERM
              AND T1."IDPerAcad"     = V_APEC_TERM
              AND t1."IDConcepto"    = P_DETL_CODE;    
            
            
            --**********************************************************************++***********************
            -- INSERT ALUMNO ESTADO en caso no exista el registro.
            SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDSede" = V_APEC_CAMP 
            AND "IDAlumno" = P_ID_ALUMNO 
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDDependencia" = 'UCCI' 
            AND "IDSeccionC" = V_APEC_IDSECCIONC 
            AND "FecInic" = V_APEC_IDSECCIONC_INIC;
            ----- INSERT
            IF (V_INDICADOR = 0) THEN
                  INSERT INTO dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE 
                          ("IDSede",                      "IDAlumno", 
                          "IDPerAcad",                    "IDDependencia", 
                          "IDSeccionC",                   "FecInic", 
                          "IDPersonal",                   "IDSeccion", 
                          "Ciclo",                        "IDEscuela", 
                          "IDEscala",                     "IDInstitucion", 
                          "FecMatricula",                 "Beca",  
                          "Estado",                       "FechaEstado", 
                          "MatriculaTipo",                "Carnet", 
                          "PlanPago")
                  VALUES(
                          V_APEC_CAMP,                    P_ID_ALUMNO, 
                          V_APEC_TERM,                    'UCCI', 
                          V_APEC_IDSECCIONC,              V_APEC_IDSECCIONC_INIC, 
                          'Banner', /*idpersonal*/        V_APEC_IDSECCIONC, 
                          0, /*ciclo*/                    '1', /*idescuela*/
                          '1', /*idescala*/               '00000000000' , /*institucion*/ 
                          V_DATENOW,                      0, /*beca*/ 
                          'M', /*estado*/                 V_DATENOW, /*fechaestado*/ 
                          '2', /*matriculatipo*/          0, /*carnet*/ 
                          5 /*planpago*/ 
                  );
              END IF;

            -- *********************************************************************************************
            -- ASINGANDO el cargo a CTACORRIENTE
                  -->SECCION ESPECIAL DTA
                    -- CONCETPSO : DTA - C01 -> S/  3.0
                    -- CONCETPSO : DTI - C02 -> S/100.0                
            SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI' 
            AND "IDSede"      = V_APEC_CAMP 
            AND "IDAlumno"    = P_ID_ALUMNO
            AND "IDPerAcad"   = V_APEC_TERM 
            AND "IDSeccionC"  = V_APEC_IDSECCIONC
            AND "FecInic"     = V_APEC_IDSECCIONC_INIC
            AND "IDConcepto"  = P_DETL_CODE;
            
            IF (V_INDICADOR = 0) THEN
                  
                INSERT INTO dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                    ("IDSede",              "IDAlumno",               "IDPerAcad", 
                    "IDDependencia",        "IDSeccionC",             "FecInic", 
                    "IDConcepto",           "FecCargo",               "Prorroga", 
                    "Cargo",                "DescMora",               "IDEscuela", 
                    "IDCuenta",             "IDInstitucion",          "Moneda", 
                    "IDEscala",             "Beca",                   "Estado", 
                    "Abono" )
                VALUES
                    (V_APEC_CAMP,           P_ID_ALUMNO,              V_APEC_TERM, 
                    'UCCI',                 V_APEC_IDSECCIONC,        V_APEC_IDSECCIONC_INIC,
                    P_DETL_CODE,            V_APEC_IDSECCIONC_INIC,   '0',
                    V_APEC_MOUNT,           0,                        '1',
                    V_APEC_IDCUENTA,        '00000000000',            V_APEC_MONEDA,
                    '1',                    0,                        'M',
                    0);
            
            ELSIF(V_INDICADOR = 1) THEN
            
                UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                    SET "Cargo" = "Cargo" + V_APEC_MOUNT  
                WHERE "IDDependencia" = 'UCCI' 
                AND "IDSede"      = V_APEC_CAMP 
                AND "IDAlumno"    = P_ID_ALUMNO
                AND "IDPerAcad"   = V_APEC_TERM 
                AND "IDSeccionC"  = V_APEC_IDSECCIONC
                AND "FecInic"     = V_APEC_IDSECCIONC_INIC
                AND "IDConcepto"  = P_DETL_CODE;

            ELSE
                RAISE V_EXCEP_NOTFOUND_CARGO;                  
            END IF;
            
              P_TRAN_NUMBER := 0;

      ELSE -- 1 ---> BANNER

            -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
            SELECT SFRRGFE_MIN_CHARGE INTO V_CARGO_MINIMO
            FROM SFRRGFE 
            WHERE SFRRGFE_TERM_CODE = P_TERM_CODE AND SFRRGFE_DETL_CODE = P_DETL_CODE AND SFRRGFE_TYPE = 'STUDENT'
            AND NVL(NVL(SFRRGFE_LEVL_CODE, v_alum_nivel),'-')           = NVL(NVL(v_alum_nivel, SFRRGFE_LEVL_CODE),'-')--------------------- Nivel
            AND NVL(NVL(SFRRGFE_CAMP_CODE, P_CAMP_CODE),'-')          = NVL(NVL(P_CAMP_CODE, SFRRGFE_CAMP_CODE),'-')  ----------------- Campus (sede)
            AND NVL(NVL(SFRRGFE_COLL_CODE, v_alum_escuela),'-')         = NVL(NVL(v_alum_escuela, SFRRGFE_COLL_CODE),'-') --------------- Escuela 
            AND NVL(NVL(SFRRGFE_DEGC_CODE, v_alum_grado),'-')           = NVL(NVL(v_alum_grado, SFRRGFE_DEGC_CODE),'-') ----------- Grado
            AND NVL(NVL(SFRRGFE_PROGRAM, v_alum_programa),'-')          = NVL(NVL(v_alum_programa, SFRRGFE_PROGRAM),'-') ---------- Programa
            AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, V_ALUM_TERM_ADM),'-')  = NVL(NVL(V_ALUM_TERM_ADM, SFRRGFE_TERM_CODE_ADMIT),'-') --------- Periodo Admicion
            -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
            -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
            -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
            AND NVL(NVL(SFRRGFE_DEPT_CODE, P_DEPT_CODE),'-')    = NVL(NVL(P_DEPT_CODE, SFRRGFE_DEPT_CODE),'-') ------------ Departamento
            -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
            AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, v_alum_tipo_alum),'-') = NVL(NVL(v_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') ------ Tipo Alumno Curriculum
            AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, v_alum_tarifa),'-')   = NVL(NVL(v_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------- Trf Curriculum (Escala)

            -- GET - Descripcion de CODIGO DETALLE  
            SELECT TBBDETC_DESC INTO V_NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_DETL_CODE;
          
            -- #######################################################################
            -- GENERAR DEUDA
            TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM,        -- PIDEM ALUMNO
                                     p_term_code            =>  P_TERM_CODE,          -- DETALLE
                                     p_detail_code          =>  P_DETL_CODE,      -- CODIGO DETALLE 
                                     p_user                 =>  USER,               -- USUARIO
                                     p_entry_date           =>  SYSDATE,     
                                     p_amount               =>  V_CARGO_MINIMO,
                                     p_effective_date       =>  SYSDATE,
                                     p_bill_date            =>  NULL,    
                                     p_due_date             =>  NULL,    
                                     p_desc                 =>  V_NOM_SERVICIO,    
                                     p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                     p_tran_number_paid     =>  NULL,     -- numero de transaccion que ser� pagara
                                     p_crossref_pidm        =>  NULL,    
                                     p_crossref_number      =>  NULL,    
                                     p_crossref_detail_code =>  NULL,     
                                     p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                     p_acct_feed_ind        =>  'Y',
                                     p_session_number       =>  0,    
                                     p_cshr_end_date        =>  NULL,    
                                     p_crn                  =>  NULL,
                                     p_crossref_srce_code   =>  NULL,
                                     p_loc_mdt              =>  NULL,
                                     p_loc_mdt_seq          =>  NULL,    
                                     p_rate                 =>  NULL,    
                                     p_units                =>  NULL,     
                                     p_document_number      =>  NULL,    
                                     p_trans_date           =>  NULL,    
                                     p_payment_id           =>  NULL,    
                                     p_invoice_number       =>  NULL,    
                                     p_statement_date       =>  NULL,    
                                     p_inv_number_paid      =>  NULL,    
                                     p_curr_code            =>  NULL,    
                                     p_exchange_diff        =>  NULL,    
                                     p_foreign_amount       =>  NULL,    
                                     p_late_dcat_code       =>  NULL,    
                                     p_atyp_code            =>  NULL,    
                                     p_atyp_seqno           =>  NULL,    
                                     p_card_type_vr         =>  NULL,    
                                     p_card_exp_date_vr     =>  NULL,     
                                     p_card_auth_number_vr  =>  NULL,    
                                     p_crossref_dcat_code   =>  NULL,    
                                     p_orig_chg_ind         =>  NULL,    
                                     p_ccrd_code            =>  NULL,    
                                     p_merchant_id          =>  NULL,    
                                     p_data_origin          =>  'WorkFlow',    
                                     p_override_hold        =>  'N',     
                                     p_tran_number_out      =>  P_TRAN_NUMBER, 
                                     p_rowid_out            =>  V_ROWID);
      
      END IF;
      
      SELECT TO_CHAR(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') INTO P_DATE_CARGO FROM DUAL;

      COMMIT;
EXCEPTION
  WHEN V_EXCEP_NOTFOUND_CARGO THEN
        P_ERROR := '- No se encontro data para generar cargo al estudiante.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CODDETALLE;