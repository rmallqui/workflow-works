/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_REASON VARCHAR2(4000);
BEGIN
    P_VERIFICAR_APTO(289039, '201820', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0; --VALIDA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VALIDA RETENCIÓN 02
        V_SOLPER            INTEGER := 0; --VALIDA SOLICITUD PERMANENCIA DE PLAN
        V_SOLRES            INTEGER := 0; --VALIDA SOLICITUD RESERVA
        V_SOLREC            INTEGER := 0; --VALIDA SOLICITUD RECTIFICACION
        V_SOLCAM            INTEGER := 0; --VALIDA SOLICITUD CAMBIO DE PLAN
        V_SOLTRA            INTEGER := 0; --VALIDA SOLICITUD TRASLADO INTERNO FINALIZADO
        V_SOLDIR            INTEGER := 0; --VALIDA SOLICITUD ASIGNATURA DIRIGIDA
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
        
        P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL013';

        V_CODE_DEPT         STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_TERM         STVTERM.STVTERM_CODE%TYPE;
        V_CODE_CAMP         STVCAMP.STVCAMP_CODE%TYPE;
        V_PROGRAM           SORLCUR.SORLCUR_PROGRAM%TYPE;
        V_TERMCTLG          SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';

        P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
        V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        V_PTRM                SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
        V_CREDITOS            INTEGER;
        V_CICLO               INTEGER;
        V_NRC_N               INTEGER;
        V_PRIORIDAD           NUMBER;
        V_INDICADOR           NUMBER;

    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;

    --################################################################################################
    -- Calculando parte PERIODO (sub PTRM)
    --################################################################################################
    CURSOR C_SFRRSTS_PTRM IS
    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                            WHEN 'UPGT' THEN 'W' 
                            WHEN 'UREG' THEN 'R' 
                            WHEN 'UPOS' THEN '-' 
                            WHEN 'ITEC' THEN '-' 
                            WHEN 'UCIC' THEN '-' 
                            WHEN 'UCEC' THEN '-' 
                            WHEN 'ICEC' THEN '-' 
                            ELSE '1' END ||
          CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                            WHEN 'F01' THEN 'A' 
                            WHEN 'F02' THEN 'L' 
                            WHEN 'F03' THEN 'C' 
                            WHEN 'V00' THEN 'V' 
                            ELSE '9' END SUBPTRM
    FROM STVCAMP,STVDEPT 
    WHERE STVDEPT_CODE = V_CODE_DEPT
    AND STVCAMP_CODE = V_CODE_CAMP;

    --################################################################################################
    /*   GET TERM (PERIODO - Solo usando parte de periodo 1) 
         A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
         y UNA SEMANA despues DE INICIO DE CLASES(SOBPTRM_START_DATE+7)
    */
    --################################################################################################
        CURSOR C_SOBPTRM IS
        SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE FROM (
          -- Forma SOATERM - SFARSTS ---> fechas para las partes de periodo
            SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
            FROM SOBPTRM
            INNER JOIN SFRRSTS
                ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
            WHERE SFRRSTS_RSTS_CODE = 'RW'
                AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
                AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
                AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (ADD_MONTHS(SOBPTRM_START_DATE,1))
            ORDER BY SOBPTRM_TERM_CODE DESC
        ) WHERE ROWNUM <= 1;
        
        -- Validar si tiene NRC activo en un determinado periodo (*** No se esta filtrando parte de periodo para los casos de modulos de UVIR Y UPGT)
        CURSOR C_SFRSTCR_N IS
        SELECT COUNT(*) NRC_ON 
        FROM SFRSTCR
        WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
            AND SFRSTCR_PIDM = P_PIDM
            AND SFRSTCR_RSTS_CODE IN ('RW','RE');
      
BEGIN

    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
    CLOSE C_SORLCUR;
    
    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SPRHOLD_TO_DATE;
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SPRHOLD_TO_DATE;
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    --- VALIDACIÓN DE ESTUDIANTES DE PROGRAMA 309 (Administración - MKT y Neg. Internacionales)
    --################################################################################################
    IF V_PROGRAM = '309' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. No esta habilitado para este flujo. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. No esta habilitado para este flujo. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLPER > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permamencia de Plan de Estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permamencia de Plan de Estudios activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    -- >> VALIDACION: SOLO se puede un solo TRASLADO INTERNO por PERIODO.
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR 
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE = 'CO'; -- Estado 'CO'-Completado
        
    IF V_SOLTRA > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno ya finalizada. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno ya finalizada. ';
        END IF;
        V_FLAG := 1;
    END IF;

    -- DATOS ALUMNO
    BWZKWFFN.P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);
    ------------------------------------------------------------
--    -- >> calculando SUB PARTE PERIODO  --
--    OPEN C_SFRRSTS_PTRM;
--    LOOP
--      FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
--      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
--    END LOOP;
--    CLOSE C_SFRRSTS_PTRM;
--
--    -- PARTE DE PERIODOS
--    V_SUB_PTRM_1 := V_SUB_PTRM || '1';
--    V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (V_CODE_DEPT ='UVIR' OR V_CODE_DEPT ='UPGT') THEN '2' ELSE '-' END;
--
--    ------------------------------------------------------------
--    -- >> Obteniendo PERIODO ACTIVO Y LA PARTE PERIODO  --
--    V_CODE_TERM := NULL;
--    OPEN C_SOBPTRM;
--      LOOP
--            FETCH C_SOBPTRM INTO V_CODE_TERM, V_PTRM;
--            IF C_SOBPTRM%FOUND THEN
--                V_CODE_TERM := P_TERM_CODE;
--            ELSE EXIT;
--      END IF;
--      END LOOP;
--    CLOSE C_SOBPTRM;
--    
--    IF V_CODE_TERM IS NULL THEN
--        P_STATUS_APTO := 'FALSE';
--        P_REASON := 'Estudiante no esta dentro de las fechas de inicio de matricula y la primera semana de clases.';
--        RETURN;
--    END IF;
--
--
--    -- VALIDAR MODALIDAD Y PRIORIDAD
--    BWZKWFFN.P_CHECK_DEPT_PRIORIDAD(P_PIDM, P_CODIGO_SOL, V_CODE_DEPT,NULL,V_CODE_CAMP, V_PRIORIDAD);
--    
--    IF (V_PRIORIDAD <> 3 AND V_PRIORIDAD <> 5 AND V_PRIORIDAD <> 8) THEN
--        P_STATUS_APTO := 'FALSE';
--        P_REASON := 'Estudiante no esta dentro de las fechas de solicitud';
--        RETURN;
--    END IF;

    --################################################################################################
    -- >> Validar si el estudiante esta estudiando en el periodo actual activo (TAMBIEN VALIDA por modulo PARTE PERIODO)
    --################################################################################################
    OPEN C_SFRSTCR_N;
    LOOP
      FETCH C_SFRSTCR_N INTO V_NRC_N;
      EXIT WHEN C_SFRSTCR_N%NOTFOUND;
    END LOOP;
    CLOSE C_SFRSTCR_N;    

    IF V_NRC_N > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene matricula activa en el periodo solicitado. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene matricula activa en el periodo solicitado. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    --OBTENER CREDITOS Y CICLO
    --################################################################################################

        BWZKWFFN.P_CREDITOS_CICLO(P_PIDM,V_CREDITOS,V_CICLO);

    --################################################################################################    
    ---MINIMO 72 CREDITOS PARA ACCEDER
    --################################################################################################
    IF V_CREDITOS < 72 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No tiene los créditos solicitados (mínimo 72 créditos aprobados). ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No tiene los créditos solicitados (mínimo 72 créditos aprobados). ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;