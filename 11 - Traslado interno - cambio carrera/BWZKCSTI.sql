/**********************************************************************************************/
/* BWZKCSTI.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Traslado Interno.                                          */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        RML             04/ENE/2017 */
/*    --------------------                                                                    */
/*    Creación del paquete de Traslado Interno                                                */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante cumpla las condiciones básicas   */
/*              para acceder para el WF.                                                      */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el procedimiento LOS CORREOS del  */
/*              responsable(s)de Registros Academicos de esa sede.                            */          
/*    Procedure P_GETUSER_COORDINADOR: Obtiene el usuario WF del cordinador,.                 */
/*    --Paquete generico                                                                      */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Valida si el estudiante anulo la solicitud.     */
/*    --Paquete generico                                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: cambia el estado de la solicitud (XXXXXX).         */
/*    Procedure P_SET_CODDETALLE: genera una deuda por CODIGO DETALLE de ls SeccionC "DTA".   */
/*    Procedure P_GET_SIDEUDA_DETLS: Verificar en base a un PIDM y el Num. Transaccion        */
/*              verifique la existencia de deuda antes a la fecha actual.                     */
/*    Procedure P_SET_ATRIBUTO_PLANS : SET ATRIBUTO de PLAN ESTUDIOS a un periodo determinado.*/
/*    Procedure P_DEL_CODDETALLE: genera una deuda por CODIGO DETALLE.                        */
/*    Procedure F_VALIDAR_FECHA_DISPONIBLE: Valida que la fecha ENVIADA este dentro de las 48 */
/*              horas para su atencion.                                                       */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene EL PROGRAMA, PROGRAM_DESC, COMENTARIO Y TELEFONO*/ 
/*              de una solicitud especifica.                                                  */
/*    Procedure P_GET_CORDINADOR_PROGRAM: A traves de un codigo de carrera Ejm "110" se       */
/*              obtiene los datos del coordinador.                                            */
/*    Procedure P_SET_CAMBIAR_CARRERA: Cambia la carrera al estudiante.                       */
/*    Procedure P_CREATE_CTACORRIENTE: Asigna escala y crea la CTA Corriente de un alumno.    */
/*    Procedure P_SET_CARGO_CONCEPTO: APEC-BDUCCI: Crea un registro al estudiante con el      */  
/*              CONCEPTO.                                                                     */
/*    Procedure P_GET_ABONO_CTA: Obtiene el abono en una cta corriente especifica.            */
/*    Procedure P_REMOVE_CTA: Desabilita la cta corriente especifica por PIDM.                */
/*    Procedure P_SET_CURSO_INTROD: Asigna los cursos introducctorios de RM y RV con nota 11  */
/*              solo en caso no los tubiera alguno.                                           */    
/*    Procedure P_VERIF_CTLG_CAMBIO: Verifica el si el plan de estudio se mantiene o cambia.  */
/*    Procedure P_STI_CONV_ASIGN: Convalida asignaturas del plan 2007 al 2015.                */
/*    Procedure P_STI_EXECCAPP: Ejecucion de CAPP.                                            */
/*    Procedure P_STI_Execproy: Ejecucion de PROYECCION.                                      */
/*    --Paquete generico                                                                      */
/*    Procedure P_GET_ASIG_RECONOCIDAS: Obtiene la lista de asignaturas reconocidas.          */
/*    --Paquete generico                                                                      */
/*    Procedure P_SET_ATRIBUTO_PLANS: SET ATRIBUTO de PLAN ESTUDIOS a un periodo determinado. */
/*                                                                                            */
/* 2. Actualización P_STI_CONV_ASIGN                           LAM/HGH            02/MAR/2018 */
/*    Se agrega este paquete ya que ahora el cambio de plan de estudio será una convalidación */
/* 3. Actualización P_SET_CARGO_CONCEPTO                       LAM                06/ABR/2018 */
/*    Se realiza la validación si en la nueva cta cte tiene asignado el monto de por concepto */
/*    CAR, si no lo tiene inserta el monto creado en el sección de caja asociado al concepto. */
/* 4. Actualización P_GET_ASIG_RECONOCIDAS                     BFV                06/JUL/2018 */
/*    Se realiza la actualización en la obtención del número de secuencia para obtener el     */
/*    reporte de asignaturas reconocidas.                                                     */
/*    Aumento SP_VERIFICAR_APTO                                                               */
/*    Se aumenta el SP que verifica las condiciones básicas que debe tener un estudiante para */
/*    acceder al WF.                                                                          */
/*    Creacion P_VERIFICAR_ESTADO_SOLICITUD                                                   */
/*    Se aumenta el SP para que valide si el estudiante anulo o no su solicitud.              */
/* 5. Actualización SP_VERIFICAR_APTO                                                         */
/*    Se aumenta la validación de la carrera 309 para que se les deniegue el acceso al flujo. */
/* 6. Actualizacion Paquete generico                           BFV                04/OCT/2018 */
/*    Se actualiza el SP para que use SP de un paquete generico.                              */
/*    Actualizacion P_VERIFICAR_APTO                                                          */
/*    Se aumenta la restricción de Beca 18 para que no continuen con el flujo.                */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/

-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/
CREATE OR REPLACE PACKAGE BWZKCSTI AS

PROCEDURE P_VERIFICAR_APTO (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_APTO     OUT VARCHAR2,
    P_REASON          OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO (
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
    P_CORREO              OUT VARCHAR2,
    P_ROL_SEDE            OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GETUSER_COORDINADOR (
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sencible mayuscula 'S01'
    P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE, -- sencible a mayusculas 'coordinador'
    P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_ROL_COORDINADOR     OUT VARCHAR2,
    P_ROL_MAILS           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STATUS_SOL          OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
);


--------------------------------------------------------------------------------

PROCEDURE P_SET_CODDETALLE ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
    P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
    P_DATE_CARGO            OUT VARCHAR2,
    P_ERROR                 OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_SIDEUDA_DETLS (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE, --------------- APEC
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, ------------- APEC
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE, ------------- APEC
    P_DETL_1CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
    P_DETL_2CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
    P_DEUDA_CODE          OUT NUMBER
);

--------------------------------------------------------------------------------

PROCEDURE P_DEL_CODDETALLE ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, 
    P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
    P_ERROR                 OUT VARCHAR2
);

-------------------------------------------------------------------------------

FUNCTION F_VALIDAR_FECHA_DISPONIBLE (
    P_DATE   IN VARCHAR2
) RETURN VARCHAR2;

-------------------------------------------------------------------------------

PROCEDURE P_GET_STUDENT_INPUTS (
    P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_PROGRAM_OLD           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_OLD_DESC      OUT VARCHAR2,
    P_PROGRAM_NEW           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_NEW_DESC      OUT VARCHAR2,
    P_COMENTARIO_ALUMNO     OUT VARCHAR2,
    P_TELEFONO_ALUMNO       OUT VARCHAR2,
    P_OPT_CONSEJERIA        OUT VARCHAR2
);

-------------------------------------------------------------------------------

-- ## Se descarto su uso para esta ocasion 201720 26/07/2017
PROCEDURE P_GET_CORDINADOR_PROGRAM (
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_ID                OUT SPRIDEN.SPRIDEN_ID%TYPE,
    P_NOMBREC           OUT VARCHAR2,
    P_CORREO            OUT VARCHAR2,
    P_PHONE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_SET_CAMBIAR_CARRERA ( 
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_PER_CATALOGO        IN VARCHAR2,
    P_ERROR               OUT VARCHAR2
);
 
-------------------------------------------------------------------------------

PROCEDURE P_CREATE_CTACORRIENTE (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE
);

-------------------------------------------------------------------------------

------------ APEC -----------------
PROCEDURE P_SET_CARGO_CONCEPTO ( 
    P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_APEC_CONCEPTO         IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
    P_PROGRAM_NEW           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MESSAGE               OUT VARCHAR2
);

-------------------------------------------------------------------------------

PROCEDURE P_GET_ABONO_CTA (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_ABONO_OLD           OUT NUMBER
);

-------------------------------------------------------------------------------

PROCEDURE P_REMOVE_CTA (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MESSAGE             OUT VARCHAR2
);

-------------------------------------------------------------------------------

PROCEDURE P_SET_CURSO_INTROD (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
);

-------------------------------------------------------------------------------

PROCEDURE P_VERIF_CTLG_CAMBIO(
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PER_CATALOGO    IN VARCHAR2,
    P_MESSAGE         OUT VARCHAR2,
    P_DESCRIP         OUT VARCHAR2
);

-------------------------------------------------------------------------------

PROCEDURE P_STI_CONV_ASIGN(
    P_TERM_CODE    IN STVTERM.STVTERM_CODE%TYPE,
    P_PIDM         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_MESSAGE      OUT VARCHAR2
);

-------------------------------------------------------------------------------

PROCEDURE P_STI_EXECCAPP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
);

-------------------------------------------------------------------------------

PROCEDURE P_STI_EXECPROY (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
);

-------------------------------------------------------------------------------

PROCEDURE P_GET_ASIG_RECONOCIDAS (
    P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_HTML1         OUT VARCHAR2,
    P_HTML2         OUT VARCHAR2,
    P_HTML3         OUT VARCHAR2
);

-------------------------------------------------------------------------------

------- #### CREADO para el SP  P_SET_CAMBIAR_CARRERA  ### -----------
PROCEDURE P_SET_ATRIBUTO_PLANS (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,      -------- COD atributos
    P_MESSAGE           OUT VARCHAR2
);

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END BWZKCSTI;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKCSTI;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKCSTI FOR BANINST1.BWZKCSTI;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKCSTI
--  START gurgrth BWZKCSTI
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/