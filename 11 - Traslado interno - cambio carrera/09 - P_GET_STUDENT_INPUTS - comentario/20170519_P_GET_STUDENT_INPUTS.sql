  /*
  SET SERVEROUTPUT ON
  declare P_PROGRAMA VARCHAR2(100);
   P_PROGRAMA_DESC varchar2(100);
   P_COMENT varchar2(4000);
   P_PRONE varchar2(50);
  begin
      P_GET_STUDENT_INPUTS(4025,P_PROGRAMA,P_PROGRAMA_DESC,P_COMENT,P_PRONE);
      DBMS_OUTPUT.PUT_LINE(P_PROGRAMA);
      DBMS_OUTPUT.PUT_LINE(P_PROGRAMA_DESC  || ' **** ' || P_COMENT || ' **** ' || P_PRONE);
  end;
  */

  CREATE or replace PROCEDURE P_GET_STUDENT_INPUTS (
        P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_PROGRAM               OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
        P_PROGRAM_DESC          OUT VARCHAR2,
        P_COMENTARIO_ALUMNO     OUT VARCHAR2,
        P_TELEFONO_ALUMNO       OUT VARCHAR2
  )
  /* ===================================================================================================================
    NOMBRE    : P_GET_STUDENT_INPUTS
    FECHA     : 11/05/2017
    AUTOR     : Mallqui Lopez, Richard Alfonso
    OBJETIVO  : Para Solicitudes 'SOL013' : 
                Obtiene EL PROGRAMA, PROGRAM_DESC, COMENTARIO Y TELEFONO de una solicitud especifica. 
                Se establecio que ultimo dato es el TELEFONO y el penultimo el COMENTARIO.

    NRO     FECHA         USUARIO       MODIFICACION
    =================================================================================================================== */
  AS
        V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
        V_CONTADOR                INTEGER := 0;
        V_COMENTARIO_ALUMNO       VARCHAR2(4000);
        V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
        V_ERROR                   EXCEPTION; 
        
        -- OBTENER NUEVO PROGRAMA (Carrera) y su descriocion
        CURSOR C_CPROGRAM IS
          SELECT SVRSVAD_ADDL_DATA_CDE,SVRSVAD_ADDL_DATA_DESC
          FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
          INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
              ON SVRSVAD_ADDL_DATA_SEQ    = SVRSRAD_ADDL_DATA_SEQ
          WHERE   SVRSVAD_ADDL_DATA_SEQ   = 1 -- Se configuro como primer dato  CARRERA
            AND SVRSRAD_SRVC_CODE         = V_CODIGO_SOLICITUD
            AND SVRSVAD_PROTOCOL_SEQ_NO   = P_FOLIO_SOLICITUD;
            
        
        -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
        CURSOR C_SVRSVAD IS
          SELECT SVRSVAD_ADDL_DATA_SEQ,SVRSVAD_ADDL_DATA_DESC
          FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
          INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
          ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
          WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
          AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
          AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
          ORDER BY SVRSVAD_ADDL_DATA_SEQ;
        
  BEGIN
        
        -- GET CODIGO SOLICITUD
        SELECT SVRSVPR_SRVC_CODE 
        INTO V_CODIGO_SOLICITUD 
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
        
        OPEN C_CPROGRAM;
          LOOP
            FETCH C_CPROGRAM INTO P_PROGRAM, P_PROGRAM_DESC;
            EXIT WHEN C_CPROGRAM%NOTFOUND;
          END LOOP;
        CLOSE C_CPROGRAM;
        
        OPEN C_SVRSVAD;
          LOOP
            FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_COMENTARIO_ALUMNO;
            IF C_SVRSVAD%FOUND THEN
                IF V_ADDL_DATA_SEQ = 2 THEN
                    P_COMENTARIO_ALUMNO := V_COMENTARIO_ALUMNO;
                ELSIF V_ADDL_DATA_SEQ = 3 THEN
                    P_TELEFONO_ALUMNO := V_COMENTARIO_ALUMNO;
                END IF;
            ELSE EXIT;
          END IF;
          END LOOP;
        CLOSE C_SVRSVAD;
        
        IF (P_TELEFONO_ALUMNO IS NULL OR P_COMENTARIO_ALUMNO IS NULL OR P_PROGRAM IS NULL) THEN
            RAISE V_ERROR;
        END IF;
  EXCEPTION
    WHEN V_ERROR THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
    WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
  END P_GET_STUDENT_INPUTS;