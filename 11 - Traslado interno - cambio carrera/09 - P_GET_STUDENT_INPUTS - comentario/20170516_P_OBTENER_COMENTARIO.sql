/*
SET SERVEROUTPUT ON
declare comentario varchar2(4000);
 comentario2 varchar2(200);
begin
    P_OBTENER_COMENTARIO_TELEFONO(4021,comentario,comentario2);
    DBMS_OUTPUT.PUT_LINE(comentario || ' **** ' || comentario2);
end;
*/

CREATE or replace PROCEDURE P_OBTENER_COMENTARIO_TELEFONO (
      P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO_ALUMNO     OUT VARCHAR2,
      P_TELEFONO_ALUMNO       OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_COMENTARIO_TELEFONO
  FECHA     : 11/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Para Solicitudes 'SOL013' : 
              Obtiene el COMENTARIO y TELEFONO de una solicitud especifica. 
              Se establecio que ultimo dato es el TELEFONO y el penultimo el COMENTARIO.

  NRO     FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
AS
      V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_CONTADOR                INTEGER := 0;
      V_COMENTARIO_ALUMNO       VARCHAR2(4000);
      V_ROWNUM                  NUMBER;
      V_ERROR                   EXCEPTION; 
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      CURSOR C_SVRSVAD IS
      SELECT ROWNUM, SVRSVAD_ADDL_DATA_DESC 
      FROM (
            SELECT SVRSVAD_ADDL_DATA_DESC
            FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
            WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
            AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
            ORDER BY SVRSVAD_ADDL_DATA_SEQ DESC
      ) WHERE ROWNUM < 3;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO V_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ROWNUM, V_COMENTARIO_ALUMNO;
          IF C_SVRSVAD%FOUND THEN
              IF V_ROWNUM = 1 THEN
                  P_TELEFONO_ALUMNO := V_COMENTARIO_ALUMNO;
              ELSE 
                  P_COMENTARIO_ALUMNO := V_COMENTARIO_ALUMNO;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;
      
      IF (P_TELEFONO_ALUMNO IS NULL OR P_COMENTARIO_ALUMNO IS NULL) THEN
          RAISE V_ERROR;
      END IF;
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_OBTENER_COMENTARIO_TELEFONO;