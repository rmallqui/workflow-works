create or replace PACKAGE WFK_CONTISTI AS
/*
 WFK_CONTISTI:
       Conti Solicitud de Traslado Interno
*/
-- FILE NAME..: WFK_CONTISTI.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISTI
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              


PROCEDURE P_GET_USUARIO (
            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
            P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
            P_CORREO              OUT VARCHAR2,
            P_ROL_SEDE            OUT VARCHAR2,
            P_ERROR               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_GETUSER_COORDINADOR (
            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
            P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
            P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
            P_ROL_COORDINADOR     OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
          P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
          P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
          P_AN                  OUT NUMBER,
          P_ERROR               OUT VARCHAR2
      );


--------------------------------------------------------------------------------

PROCEDURE P_SET_CODDETALLE ( 
          P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
          P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
          P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
          P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
          P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
          P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
          P_DATE_CARGO            OUT VARCHAR2,
          P_ERROR                 OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_GET_SIDEUDA (
          P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE, --------------- APEC
          P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, ------------- APEC
          P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE, ------------- APEC
          P_DETL_CODE           IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
          P_TRAN_NUMBER         IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
          P_DEUDA               OUT VARCHAR2
      );

PROCEDURE P_GET_SIDEUDA_DETLS (
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE, --------------- APEC
            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, ------------- APEC
            P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE, ------------- APEC
            P_DETL_1CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
            P_DETL_2CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
            P_DEUDA_CODE          OUT NUMBER
      );

--------------------------------------------------------------------------------

PROCEDURE P_DEL_CODDETALLE ( 
          P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
          P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
          P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
          P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, 
          P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
          P_ERROR                 OUT VARCHAR2
    );
   
-------------------------------------------------------------------------------

FUNCTION F_VALIDAR_FECHA_DISPONIBLE (
            P_DATE   IN VARCHAR2
    ) RETURN VARCHAR2;

-------------------------------------------------------------------------------

PROCEDURE P_GET_STUDENT_INPUTS (
          P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
          P_PROGRAM_OLD           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
          P_PROGRAM_OLD_DESC      OUT VARCHAR2,
          P_PROGRAM_NEW           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
          P_PROGRAM_NEW_DESC      OUT VARCHAR2,
          P_COMENTARIO_ALUMNO     OUT VARCHAR2,
          P_TELEFONO_ALUMNO       OUT VARCHAR2,
          P_OPT_CONSEJERIA        OUT VARCHAR2
    );

-------------------------------------------------------------------------------

PROCEDURE P_GET_CORDINADOR_PROGRAM (
          P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
          P_ID                OUT SPRIDEN.SPRIDEN_ID%TYPE,
          P_NOMBREC           OUT VARCHAR2,
          P_CORREO            OUT VARCHAR2,
          P_PHONE             OUT VARCHAR2
    );

--------------------------------------------------------------------------------

PROCEDURE P_SET_CAMBIAR_CARRERA ( 
          P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
          P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
          P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
          P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
          P_PER_CATALOGO        IN VARCHAR2,
          P_ERROR               OUT VARCHAR2
    );
 
-------------------------------------------------------------------------------

PROCEDURE P_CREATE_CTACORRIENTE (
          P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
          P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
          P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
          P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
          P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE
    );

-------------------------------------------------------------------------------

PROCEDURE P_GET_ABONO_CTA (
          P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
          P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
          P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
          P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
          P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
          P_ABONO_OLD           OUT NUMBER
    );

-------------------------------------------------------------------------------

PROCEDURE P_REMOVE_CTA (
          P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
          P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
          P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
          P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
          P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
          P_MESSAGE             OUT VARCHAR2
    );

-------------------------------------------------------------------------------

PROCEDURE P_GET_ASIG_RECONOCIDAS (
            P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_HTML1         OUT VARCHAR2,
            P_HTML2         OUT VARCHAR2,
            P_HTML3         OUT VARCHAR2
      );

-------------------------------------------------------------------------------

PROCEDURE P_STI_EXECCAPP (
            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
            P_MESSAGE           OUT VARCHAR2
      );

-------------------------------------------------------------------------------

PROCEDURE P_STI_EXECPROY (
            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
            P_MESSAGE           OUT VARCHAR2
      );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END WFK_CONTISTI;