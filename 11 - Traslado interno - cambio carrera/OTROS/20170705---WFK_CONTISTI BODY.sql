create or replace PACKAGE BODY WFK_CONTISTI AS
/*
 WFK_CONTISTI:
       Conti Solicitud de Traslado Interno
*/
-- FILE NAME..: WFK_CONTISTI.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISTI
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              


PROCEDURE P_GET_USUARIO (
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO              OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_ROLE_ID                 NUMBER;
      V_ORG_ID                  NUMBER;
      V_Email_Address           VARCHAR2(100);
    
      V_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID AND ROLE_ID = V_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_CAMP_CODE;
      
      -- Obtener el ROL_ID 
      V_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO V_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      V_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      V_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO V_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      V_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      V_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO V_Email_Address FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_CORREO := P_CORREO || V_Email_Address || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      V_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_CORREO,1,LENGTH(P_CORREO) -1) INTO P_CORREO
      FROM DUAL
      WHERE SUBSTR(P_CORREO,-1,1) = ',';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( V_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_CAMP_CODE;
          ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := '- Se encontraron mas de una ORGANIZACIóN con el mismo nombre.';
          ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := '- Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN NO_DATA_FOUND THEN
          IF ( V_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := '- NO se encontrò el nombre del ROL: ' || P_ROL || P_CAMP_CODE;
          ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := '- NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := '- NO  se encontrÓ ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_GETUSER_COORDINADOR (
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_ROL_COORDINADOR     OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GETUSER_COORDINADOR
  FECHA     : 22/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el usuario WF del cordinador,
              Se establecio que el usuario se compone por "COORDINADOR + PROGRAM_CODE + CAMPUS"
                  - Ejm: COORDINADOR110S01 
  =================================================================================================================== */
AS
            -- @PARAMETERS
      V_ROLE_ID                 NUMBER;
      V_ORG_ID                  NUMBER;
      V_INDICADOR               NUMBER;
      V_EXEPTION                EXCEPTION;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
---.
      P_ROL_COORDINADOR := P_ROL ||  P_PROGRAM_NEW || P_CAMP_CODE;
      
      -- Obtener el ROL_ID 
      SELECT ID INTO V_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_COORDINADOR;
      
      -- Obtener el ORG_ID 
      SELECT ID INTO V_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      
      --Validar que exista el ROL
      SELECT COUNT(*) INTO V_INDICADOR FROM WORKFLOW.ROLE_ASSIGNMENT 
      WHERE ORG_ID = V_ORG_ID AND ROLE_ID = V_ROLE_ID;
      IF V_INDICADOR <> 1 THEN
          RAISE V_EXEPTION;
      END if;
      
EXCEPTION
  WHEN V_EXEPTION THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No se encontro el ROL.' || P_ROL_COORDINADOR);
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GETUSER_COORDINADOR;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_AN                  OUT NUMBER,
  P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 12/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : cambia el estado de la solicitud (XXXXXX)

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   10/03/2016    RMALLQUI      Se agrego el parametro P_AN paravalidar si la solicitud este ANULADO.
  002   27/04/2016    RMALLQUI      Se comentó el filtro de fechas para cambir el estado de la solicitud.
                                    Se modifico el param. P_ERROR en el EXCEPTION, de tal meanera que el WF detenga el flujo en cualquier caso de error.
  =================================================================================================================== */
AS
            P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
            P_ESTADO_PREVIOUS         SVVSRVS.SVVSRVS_CODE%TYPE;
            P_FECHA_SOL               SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
            P_INDICADOR               NUMBER;
            E_INVALID_ESTADO          EXCEPTION;
            V_PRIORIDAD               SVRSVPR.SVRSVPR_RSRV_SEQ_NO%TYPE;
BEGIN
--
  ----------------------------------------------------------------------------
        
        -- GET tipo de solicitud por ejemplo "SOL003" 
        SELECT  SVRSVPR_SRVC_CODE, 
                SVRSVPR_SRVS_CODE,
                SVRSVPR_RECEPTION_DATE,
                SVRSVPR_RSRV_SEQ_NO
        INTO    P_CODIGO_SOLICITUD,
                P_ESTADO_PREVIOUS,
                P_FECHA_SOL,
                V_PRIORIDAD
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
        
        -- SI EL ESTADO ES ANULADO (AN)
        P_AN := 0;
        IF P_ESTADO_PREVIOUS = 'AN' THEN
              P_AN := 1;
        END IF;
        
        -- VALIDAR que :
            -- El ESTADO actual sea modificable segun la configuracion.
            -- El ETSADO enviado pertenesca entre los asignados al tipo de SOLICTUD.
        SELECT COUNT(*) INTO P_INDICADOR
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        AND SVRSVPR_SRVS_CODE IN (
              -- Estados de solic. MODIFICABLES segun las reglas y configuracion
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        )
        AND P_ESTADO IN (
              -- Estados de solic. configurados
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        );
        
        
        IF P_INDICADOR = 0 THEN
              RAISE E_INVALID_ESTADO;
        ELSIF (P_ESTADO_PREVIOUS <> P_ESTADO) THEN
              -- UPDATE SOLICITUD
              UPDATE SVRSVPR
                SET SVRSVPR_SRVS_CODE = P_ESTADO,  --AN , AP , RE entre otros
                SVRSVPR_ACTIVITY_DATE = SYSDATE,
                SVRSVPR_DATA_ORIGIN = 'WorkFlow'
              WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
              AND SVRSVPR_SRVS_CODE IN (
                        -- Estados de solic. MODIFICABLES segun las reglas y configuracion
                        SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                        FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                        INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                        ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                        AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                        INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                        ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                        WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                        AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                        AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
                        AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                        -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
                )
              AND P_ESTADO IN (
                      -- Estados de solic. configurados
                      SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                      FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                      INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                      ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                      AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                      INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                      ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                      WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                      AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                      AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                      -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
              );
              
              COMMIT;
        END IF;
        
EXCEPTION
  WHEN E_INVALID_ESTADO THEN
          P_ERROR  := '- El "ESTADO" actual no es modificable ó no se encuentra entre los ESTADOS disponibles para la solicitud.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_SET_CODDETALLE ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_DATE_CARGO            OUT VARCHAR2,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CODDETALLE
  FECHA     : 22/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE.
              APEC-BDUCCI: Crea un registro al estudiante con el CONCEPTO.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_NOM_SERVICIO            VARCHAR2(30);
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    V_DATENOW                 DATE := SYSDATE;
    
    V_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;          
    V_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
    V_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
    V_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
    V_ALUM_TERM_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
    V_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP 
    V_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE

    V_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
    V_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
    
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORLCUR_FOS IS
    SELECT    SORLCUR_LEVL_CODE,        
              SORLCUR_COLL_CODE,
              SORLCUR_DEGC_CODE,  
              SORLCUR_PROGRAM,      
              SORLCUR_TERM_CODE_ADMIT,  
              SORLCUR_STYP_CODE,  
              SORLCUR_RATE_CODE  
    FROM (
            SELECT    SORLCUR_LEVL_CODE,    SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,  
                      SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  SORLCUR_STYP_CODE,  
                      SORLCUR_RATE_CODE
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM        =   P_PIDM
                AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;
    
    -- APEC PARAMS
    V_APEC_SECC_SEDE        VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    V_APEC_IDCUENTA         VARCHAR2(10);
    V_APEC_MONEDA           VARCHAR2(10);
    V_APEC_MOUNT            NUMBER;
    V_APEC_IDSECCIONC_INIC     DATE;
    V_APEC_IDSECCIONC_VENC     DATE;
    
BEGIN

      -- #######################################################################
      -- >> GET DATOS --
      OPEN C_SORLCUR_FOS;
      LOOP
        FETCH C_SORLCUR_FOS INTO  V_ALUM_NIVEL,     V_ALUM_ESCUELA,   V_ALUM_GRADO,
                                  V_ALUM_PROGRAMA,  V_ALUM_TERM_ADM,  V_ALUM_TIPO_ALUM, V_ALUM_TARIFA;
        EXIT WHEN C_SORLCUR_FOS%NOTFOUND;
      END LOOP;
      CLOSE C_SORLCUR_FOS;
      
      
      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)

            SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO

            
            /*******
                Para la SEDE(CAMPUS) se trabaja con el de la SECCIONC ESPECIAL.
                SeccionC : DTA --> sede: HYO
            ********/
            SELECT  t1."IDSeccionC",  
                    t2."IDsede",
                    t1."FecInic",           
                    "FecVenc",              
                    "Monto",      
                    "IDCuenta",      
                    t1."Moneda"
            INTO    V_APEC_IDSECCIONC, 
                    V_APEC_SECC_SEDE,
                    V_APEC_IDSECCIONC_INIC, 
                    V_APEC_IDSECCIONC_VENC, 
                    V_APEC_MOUNT, 
                    V_APEC_IDCUENTA, 
                    V_APEC_MONEDA
            FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
            INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
              ON t1."IDSede"          = t2."IDsede"
              AND t1."IDPerAcad"      = t2."IDPerAcad" 
              AND t1."IDDependencia"  = t2."IDDependencia" 
              AND t1."IDSeccionC"     = t2."IDSeccionC" 
              AND t1."FecInic"        = t2."FecInic"
            WHERE t2."IDDependencia" = 'UCCI'
              AND t2."IDSeccionC"    = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
              AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
              AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
              AND t2."IDSeccionC"     <> '15NEX1A'
              AND t2."IDPerAcad"     = V_APEC_TERM
              AND T1."IDPerAcad"     = V_APEC_TERM
              AND t1."IDConcepto"    = P_DETL_CODE;    

            
            --**********************************************************************++***********************
            -- INSERT ALUMNO ESTADO en caso no exista el registro.
            SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDSede"      = V_APEC_SECC_SEDE 
            AND "IDAlumno"      = P_ID_ALUMNO 
            AND "IDPerAcad"     = V_APEC_TERM
            AND "IDDependencia" = 'UCCI' 
            AND "IDSeccionC"    = V_APEC_IDSECCIONC 
            AND "FecInic"       = V_APEC_IDSECCIONC_INIC;
            ----- INSERT
            IF (V_INDICADOR = 0) THEN

                  INSERT INTO dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE 
                          ("IDSede",                      "IDAlumno", 
                          "IDPerAcad",                    "IDDependencia", 
                          "IDSeccionC",                   "FecInic", 
                          "IDPersonal",                   "IDSeccion", 
                          "Ciclo",                        "IDEscuela", 
                          "IDEscala",                     "IDInstitucion", 
                          "FecMatricula",                 "Beca",  
                          "Estado",                       "FechaEstado", 
                          "MatriculaTipo",                "Carnet", 
                          "PlanPago")
                  VALUES(
                          V_APEC_SECC_SEDE,               P_ID_ALUMNO, 
                          V_APEC_TERM,                    'UCCI', 
                          V_APEC_IDSECCIONC,              V_APEC_IDSECCIONC_INIC, 
                          'Banner', /*idpersonal*/        V_APEC_IDSECCIONC, 
                          0, /*ciclo*/                    '1', /*idescuela*/
                          '1', /*idescala*/               '00000000000' , /*institucion*/ 
                          V_DATENOW,                      0, /*beca*/ 
                          'M', /*estado*/                 V_DATENOW, /*fechaestado*/ 
                          '2', /*matriculatipo*/          0, /*carnet*/ 
                          5 /*planpago*/ 
                  );
            END IF;

    
            -- *********************************************************************************************
            -- ASINGANDO el cargo a CTACORRIENTE
                  -->SECCION ESPECIAL DTA
                    -- CONCETPSO : DTA - C01 -> S/  3.0
                    -- CONCETPSO : DTI - C02 -> S/100.0                
            SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI' 
            AND "IDSede"      = V_APEC_SECC_SEDE 
            AND "IDAlumno"    = P_ID_ALUMNO
            AND "IDPerAcad"   = V_APEC_TERM 
            AND "IDSeccionC"  = V_APEC_IDSECCIONC
            AND "FecInic"     = V_APEC_IDSECCIONC_INIC
            AND "IDConcepto"  = P_DETL_CODE;
         
         
            IF (V_INDICADOR = 0) THEN
                  
                INSERT INTO dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                    ("IDSede",              "IDAlumno",               "IDPerAcad", 
                    "IDDependencia",        "IDSeccionC",             "FecInic", 
                    "IDConcepto",           "FecCargo",               "Prorroga", 
                    "Cargo",                "DescMora",               "IDEscuela", 
                    "IDCuenta",             "IDInstitucion",          "Moneda", 
                    "IDEscala",             "Beca",                   "Estado", 
                    "Abono" )
                VALUES
                    (V_APEC_SECC_SEDE,      P_ID_ALUMNO,              V_APEC_TERM, 
                    'UCCI',                 V_APEC_IDSECCIONC,        V_APEC_IDSECCIONC_INIC,
                    P_DETL_CODE,            V_APEC_IDSECCIONC_INIC,   '0',
                    V_APEC_MOUNT,           0,                        '1',
                    V_APEC_IDCUENTA,        '00000000000',            V_APEC_MONEDA,
                    '1',                    0,                        'M',
                    0);
            
            ELSIF(V_INDICADOR = 1) THEN
            
                UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                    SET "Cargo" = "Cargo" + V_APEC_MOUNT  
                WHERE "IDDependencia" = 'UCCI' 
                AND "IDSede"      = V_APEC_SECC_SEDE 
                AND "IDAlumno"    = P_ID_ALUMNO
                AND "IDPerAcad"   = V_APEC_TERM 
                AND "IDSeccionC"  = V_APEC_IDSECCIONC
                AND "FecInic"     = V_APEC_IDSECCIONC_INIC
                AND "IDConcepto"  = P_DETL_CODE;

            ELSE
                RAISE V_EXCEP_NOTFOUND_CARGO;                  
            END IF;
            
              P_TRAN_NUMBER := 0;

      ELSE -- 1 ---> BANNER

            -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
            SELECT SFRRGFE_MIN_CHARGE INTO V_CARGO_MINIMO
            FROM SFRRGFE 
            WHERE SFRRGFE_TERM_CODE = P_TERM_CODE AND SFRRGFE_DETL_CODE = P_DETL_CODE AND SFRRGFE_TYPE = 'STUDENT'
            AND NVL(NVL(SFRRGFE_LEVL_CODE, v_alum_nivel),'-')           = NVL(NVL(v_alum_nivel, SFRRGFE_LEVL_CODE),'-')--------------------- Nivel
            AND NVL(NVL(SFRRGFE_CAMP_CODE, P_CAMP_CODE),'-')          = NVL(NVL(P_CAMP_CODE, SFRRGFE_CAMP_CODE),'-')  ----------------- Campus (sede)
            AND NVL(NVL(SFRRGFE_COLL_CODE, v_alum_escuela),'-')         = NVL(NVL(v_alum_escuela, SFRRGFE_COLL_CODE),'-') --------------- Escuela 
            AND NVL(NVL(SFRRGFE_DEGC_CODE, v_alum_grado),'-')           = NVL(NVL(v_alum_grado, SFRRGFE_DEGC_CODE),'-') ----------- Grado
            AND NVL(NVL(SFRRGFE_PROGRAM, v_alum_programa),'-')          = NVL(NVL(v_alum_programa, SFRRGFE_PROGRAM),'-') ---------- Programa
            AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, V_ALUM_TERM_ADM),'-')  = NVL(NVL(V_ALUM_TERM_ADM, SFRRGFE_TERM_CODE_ADMIT),'-') --------- Periodo Admicion
            -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
            -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
            -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
            AND NVL(NVL(SFRRGFE_DEPT_CODE, P_DEPT_CODE),'-')    = NVL(NVL(P_DEPT_CODE, SFRRGFE_DEPT_CODE),'-') ------------ Departamento
            -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
            AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, v_alum_tipo_alum),'-') = NVL(NVL(v_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') ------ Tipo Alumno Curriculum
            AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, v_alum_tarifa),'-')   = NVL(NVL(v_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------- Trf Curriculum (Escala)

            -- GET - Descripcion de CODIGO DETALLE  
            SELECT TBBDETC_DESC INTO V_NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_DETL_CODE;
          
            -- #######################################################################
            -- GENERAR DEUDA
            TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM,        -- PIDEM ALUMNO
                                     p_term_code            =>  P_TERM_CODE,          -- DETALLE
                                     p_detail_code          =>  P_DETL_CODE,      -- CODIGO DETALLE 
                                     p_user                 =>  USER,               -- USUARIO
                                     p_entry_date           =>  SYSDATE,     
                                     p_amount               =>  V_CARGO_MINIMO,
                                     p_effective_date       =>  SYSDATE,
                                     p_bill_date            =>  NULL,    
                                     p_due_date             =>  NULL,    
                                     p_desc                 =>  V_NOM_SERVICIO,    
                                     p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                     p_tran_number_paid     =>  NULL,     -- numero de transaccion que será pagara
                                     p_crossref_pidm        =>  NULL,    
                                     p_crossref_number      =>  NULL,    
                                     p_crossref_detail_code =>  NULL,     
                                     p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                     p_acct_feed_ind        =>  'Y',
                                     p_session_number       =>  0,    
                                     p_cshr_end_date        =>  NULL,    
                                     p_crn                  =>  NULL,
                                     p_crossref_srce_code   =>  NULL,
                                     p_loc_mdt              =>  NULL,
                                     p_loc_mdt_seq          =>  NULL,    
                                     p_rate                 =>  NULL,    
                                     p_units                =>  NULL,     
                                     p_document_number      =>  NULL,    
                                     p_trans_date           =>  NULL,    
                                     p_payment_id           =>  NULL,    
                                     p_invoice_number       =>  NULL,    
                                     p_statement_date       =>  NULL,    
                                     p_inv_number_paid      =>  NULL,    
                                     p_curr_code            =>  NULL,    
                                     p_exchange_diff        =>  NULL,    
                                     p_foreign_amount       =>  NULL,    
                                     p_late_dcat_code       =>  NULL,    
                                     p_atyp_code            =>  NULL,    
                                     p_atyp_seqno           =>  NULL,    
                                     p_card_type_vr         =>  NULL,    
                                     p_card_exp_date_vr     =>  NULL,     
                                     p_card_auth_number_vr  =>  NULL,    
                                     p_crossref_dcat_code   =>  NULL,    
                                     p_orig_chg_ind         =>  NULL,    
                                     p_ccrd_code            =>  NULL,    
                                     p_merchant_id          =>  NULL,    
                                     p_data_origin          =>  'WorkFlow',    
                                     p_override_hold        =>  'N',     
                                     p_tran_number_out      =>  P_TRAN_NUMBER, 
                                     p_rowid_out            =>  V_ROWID);
      
      END IF;
      
      SELECT TO_CHAR(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') INTO P_DATE_CARGO FROM DUAL;
      
      COMMIT;
EXCEPTION
  WHEN V_EXCEP_NOTFOUND_CARGO THEN
        P_ERROR := '- No se encontro data para generar cargo al estudiante.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CODDETALLE;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_GET_SIDEUDA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE, --------------- APEC
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, ------------- APEC
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE, ------------- APEC
      P_DETL_CODE           IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
      P_TRAN_NUMBER         IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
      P_DEUDA               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Verificar en base a un PIDM y el Num. Transaccion verifique la existencia de deuda. divisa PEN.
              - APEC verificar si el PIDM tiene deuda en algun concepto especifico para la seccion "DTA".

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      V_DEUDA_INDICADOR       NUMBER;
      -- APEC PARAMS
      V_APEC_SECC_SEDE        VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(10);
      V_APEC_IDSECCIONC       VARCHAR2(15);
      V_APEC_IDSECCIONC_INIC  DATE;
      V_APEC_MOUNT            NUMBER;      
      
BEGIN
--
    
    --#################################----- APEC -----#######################################
    --########################################################################################
    -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
    IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
          
          --**********************************************************************************************
          -- GET SECCIONC
          SELECT  "IDsede", "IDSeccionC" , "FecInic" INTO V_APEC_SECC_SEDE, V_APEC_IDSECCIONC, V_APEC_IDSECCIONC_INIC
          FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia"='UCCI'
          AND "IDPerAcad" = V_APEC_TERM
          AND "IDSeccionC" = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
          AND "IDSeccionC" <> '15NEX1A';

          -- GET DEUDA(s)
          SELECT "Deuda" INTO V_APEC_MOUNT FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    = P_ID_ALUMNO
          AND "IDSede"      = V_APEC_SECC_SEDE
          AND "IDPerAcad"   = V_APEC_TERM
          AND "IDSeccionC"  = V_APEC_IDSECCIONC
          AND "FecInic"     = V_APEC_IDSECCIONC_INIC
          AND "IDConcepto"  = P_DETL_CODE;
          
          V_DEUDA_INDICADOR := V_APEC_MOUNT;
          
          COMMIT;
    ELSE --**************************** 1 ---> BANNER ***************************
          
          -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
          TZKCDAA.p_calc_deuda_alumno(P_PIDM,'PEN');
          COMMIT;
              
          -- GET deuda
          SELECT COUNT(TZRCDAB_AMOUNT)
          INTO   V_DEUDA_INDICADOR
          FROM   TZRCDAB
          WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
          AND TZRCDAB_PIDM = P_PIDM
          --AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE
          AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
          
    END IF;        
   
    IF V_DEUDA_INDICADOR > 0 THEN
      P_DEUDA   := 'TRUE';
    ELSE
      P_DEUDA   :='FALSE';
    END IF;

END P_GET_SIDEUDA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_GET_SIDEUDA_DETLS (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE, --------------- APEC
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, ------------- APEC
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE, ------------- APEC
      P_DETL_1CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
      P_DETL_2CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
      P_DEUDA_CODE          OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Verificar en base a un PIDM y el Num. Transaccion verifique la existencia de deuda. divisa PEN.
              - APEC verificar si el PIDM tiene deuda en algun concepto especifico para la seccion "DTA".

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      V_DEUDA_INDICADOR       NUMBER;
      -- APEC PARAMS
      V_APEC_SECC_SEDE        VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(10);
      V_APEC_IDSECCIONC       VARCHAR2(15);
      V_APEC_IDSECCIONC_INIC  DATE;
      V_APEC_MOUNT1           NUMBER;      
      V_APEC_MOUNT2           NUMBER;
      
BEGIN
--
    
    --#################################----- APEC -----#######################################
    --########################################################################################
    -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
    IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
          
          --**********************************************************************************************
          -- GET SECCIONC
          SELECT  "IDsede", "IDSeccionC" , "FecInic" INTO V_APEC_SECC_SEDE, V_APEC_IDSECCIONC, V_APEC_IDSECCIONC_INIC
          FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia"='UCCI'
          AND "IDPerAcad" = V_APEC_TERM
          AND "IDSeccionC" = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
          AND "IDSeccionC" <> '15NEX1A';

          -- GET 1° DEUDA(s)
          SELECT "Deuda" INTO V_APEC_MOUNT1 FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    = P_ID_ALUMNO
          AND "IDSede"      = V_APEC_SECC_SEDE
          AND "IDPerAcad"   = V_APEC_TERM
          AND "IDSeccionC"  = V_APEC_IDSECCIONC
          AND "FecInic"     = V_APEC_IDSECCIONC_INIC
          AND "IDConcepto"  = P_DETL_1CODE;
          
          -- GET 2° DEUDA(s) 
          SELECT "Deuda" INTO V_APEC_MOUNT2 FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    = P_ID_ALUMNO
          AND "IDSede"      = V_APEC_SECC_SEDE
          AND "IDPerAcad"   = V_APEC_TERM
          AND "IDSeccionC"  = V_APEC_IDSECCIONC
          AND "FecInic"     = V_APEC_IDSECCIONC_INIC
          AND "IDConcepto"  = P_DETL_2CODE;
            
          COMMIT;
    END IF;
    
    /*
      - CODIGOS DEUDA
        3 : Tiene deuda en los dos conceptos
        1 : Tiene deuda solo en el primer concepto
        2 : Tiene deuda solo en el segundo concepto
    */
    P_DEUDA_CODE := CASE  WHEN V_APEC_MOUNT1 > 0 AND V_APEC_MOUNT2 > 0 THEN 3
                          WHEN V_APEC_MOUNT1 > 0 THEN 1 
                          WHEN V_APEC_MOUNT2 > 0 THEN 2 ELSE 0 END;
                    
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_SIDEUDA_DETLS;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_DEL_CODDETALLE ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, 
        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_DEL_CODDETALLE
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE.
              APEC-BDUCCI: Amortiza un cargo al estudiante con mismo costo del CONCEPTO. 

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_NOM_SERVICIO            VARCHAR2(30);
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    V_MESSAGE                 EXCEPTION;
    V_TRAN_NUMBER_OUT         TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
    V_ROWID                   GB_COMMON.INTERNAL_RECORD_ID_TYPE;
    
    -- APEC PARAMS
    V_APEC_SECC_SEDE        VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    V_APEC_IDSECCIONC_INIC  DATE;
    V_APEC_MOUNT            NUMBER;
    
    V_FOUND_AMOUNT          TBRACCD.TBRACCD_AMOUNT%TYPE;
    V_COD_DETALLE           TBRACCD.TBRACCD_DETAIL_CODE%TYPE;
    V_TERM_CODE             STVTERM.STVTERM_CODE%TYPE;
BEGIN
      

      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)

            SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
            
            
            --**********************************************************************************
            -- GET datos CUENTA APEC
            SELECT t1."IDSeccionC", t2."IDsede", t1."FecInic", t1."Monto" INTO V_APEC_IDSECCIONC, V_APEC_SECC_SEDE, V_APEC_IDSECCIONC_INIC, V_APEC_MOUNT
              FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
              INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
                ON t1."IDSede"          = t2."IDsede"
                AND t1."IDPerAcad"      = t2."IDPerAcad" 
                AND t1."IDDependencia"  = t2."IDDependencia" 
                AND t1."IDSeccionC"     = t2."IDSeccionC" 
                AND t1."FecInic"        = t2."FecInic"
              WHERE t2."IDDependencia"  = 'UCCI'
                AND t2."IDSeccionC"     = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
                AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
                AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
                AND t2."IDSeccionC"     <> '15NEX1A'
                AND t2."IDPerAcad"      = V_APEC_TERM
                AND T1."IDPerAcad"      = V_APEC_TERM
                AND t1."IDConcepto"     = P_DETL_CODE;
            
            --******************************************************************************
            -- Validar si existe la cuenta del estudiante  para traslado interno
            SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDSede" = V_APEC_SECC_SEDE
            AND "IDAlumno" = P_ID_ALUMNO
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDDependencia" = 'UCCI'
            AND "IDSeccionC" = V_APEC_IDSECCIONC
            AND "FecInic" = V_APEC_IDSECCIONC_INIC
            AND "IDConcepto" = P_DETL_CODE;
            
            IF (V_INDICADOR = 1) THEN
                  -- UPDATE MONTO(s)
                  UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                  SET "Cargo" = "Cargo" - V_APEC_MOUNT
                  WHERE "IDDependencia" = 'UCCI'
                  AND "IDAlumno"    = P_ID_ALUMNO
                  AND "IDSede"      = V_APEC_SECC_SEDE
                  AND "IDPerAcad"   = V_APEC_TERM
                  AND "IDSeccionC"  = V_APEC_IDSECCIONC
                  AND "FecInic" = V_APEC_IDSECCIONC_INIC
                  AND "IDConcepto"  = P_DETL_CODE;
            ELSE
                  RAISE V_EXCEP_NOTFOUND_CARGO;
            END IF;
            
            
            P_TRAN_NUMBER := 0;

      ELSE -- 1 ---> BANNER

             -- Validar si la transaccion (DEUDA) NO tubo algun movimiento.
            SELECT COUNT(*) INTO V_INDICADOR
            FROM (
                    -- : listar LOS CODIGOS QUE YA TIENEN ALGUN MOVIMIENTO. : TRAN , CHG 
                    SELECT TBRAPPL_PIDM TEMP_PIDM ,TBRAPPL_PAY_TRAN_NUMBER TEMP_PAY_TRAN_NUMBER
                    FROM TBRAPPL
                    WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM
                    UNION 
                    SELECT TBRAPPL_PIDM, TBRAPPL_CHG_TRAN_NUMBER
                    FROM TBRAPPL
                    WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM
            ) WHERE P_TRAN_NUMBER = TEMP_PAY_TRAN_NUMBER;
            
            IF V_INDICADOR = 1 THEN
                  RAISE V_MESSAGE;
            END IF;              
                
            -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
            TZKCDAA.p_calc_deuda_alumno(P_PIDM,'PEN');
            COMMIT;
                
            -- GET , COD_DETALLE , AMOUNT(NEGATIVO) de la transaccion a cancelar  
            SELECT  (TZRCDAB_AMOUNT * (-1)), 
                    TZRCDAB_DETAIL_CODE, 
                    TZRCDAB_TERM_CODE
            INTO    V_FOUND_AMOUNT,
                    V_COD_DETALLE, 
                    V_TERM_CODE
            FROM   TZRCDAB
            WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
            AND TZRCDAB_PIDM = P_PIDM 
            AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
            
            -- GET DESCRIPCION del cod_detalle
            SELECT TBBDETC_DESC INTO V_NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = V_COD_DETALLE;
        
            -- #######################################################################
            -- GENERAR DEUDA
            TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM,        -- PIDEM ALUMNO
                                     p_term_code            =>  V_TERM_CODE,          -- DETALLE
                                     p_detail_code          =>  V_COD_DETALLE,      -- CODIGO DETALLE  -- mismo codigo detalle
                                     p_user                 =>  USER,               -- USUARIO
                                     p_entry_date           =>  SYSDATE,     
                                     p_amount               =>  V_FOUND_AMOUNT,
                                     p_effective_date       =>  SYSDATE,
                                     p_bill_date            =>  NULL,    
                                     p_due_date             =>  NULL,    
                                     p_desc                 =>  V_NOM_SERVICIO,       -- referencia por consultar 
                                     p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                     p_tran_number_paid     =>  P_TRAN_NUMBER,      -- numero de transaccion que será pagara
                                     p_crossref_pidm        =>  NULL,    
                                     p_crossref_number      =>  NULL,    
                                     p_crossref_detail_code =>  NULL,     
                                     p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                     p_acct_feed_ind        =>  'Y',
                                     p_session_number       =>  0,    
                                     p_cshr_end_date        =>  NULL,    
                                     p_crn                  =>  NULL,
                                     p_crossref_srce_code   =>  NULL,
                                     p_loc_mdt              =>  NULL,
                                     p_loc_mdt_seq          =>  NULL,    
                                     p_rate                 =>  NULL,    
                                     p_units                =>  NULL,     
                                     p_document_number      =>  NULL,    
                                     p_trans_date           =>  NULL,    
                                     p_payment_id           =>  NULL,    
                                     p_invoice_number       =>  NULL,    
                                     p_statement_date       =>  NULL,    
                                     p_inv_number_paid      =>  NULL,    
                                     p_curr_code            =>  NULL,    
                                     p_exchange_diff        =>  NULL,    
                                     p_foreign_amount       =>  NULL,    
                                     p_late_dcat_code       =>  NULL,    
                                     p_atyp_code            =>  NULL,    
                                     p_atyp_seqno           =>  NULL,    
                                     p_card_type_vr         =>  NULL,    
                                     p_card_exp_date_vr     =>  NULL,     
                                     p_card_auth_number_vr  =>  NULL,    
                                     p_crossref_dcat_code   =>  NULL,    
                                     p_orig_chg_ind         =>  NULL,    
                                     p_ccrd_code            =>  NULL,    
                                     p_merchant_id          =>  NULL,    
                                     p_data_origin          =>  'WORKFLOW',    
                                     p_override_hold        =>  'N',     
                                     p_tran_number_out      =>  V_TRAN_NUMBER_OUT, 
                                     p_rowid_out            =>  V_ROWID);
        
              ------------------------------------------------------------------------------  
                
                  -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
                  TZJAPOL.p_run_proc_tvrappl(P_ID_ALUMNO);
              
              ------------------------------------------------------------------------------  
      END IF;
      
      COMMIT;
EXCEPTION
  WHEN V_MESSAGE THEN
        P_ERROR  := '- La deuda tubo movimientos por lo que no se puede cancelar directamente.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN V_EXCEP_NOTFOUND_CARGO THEN
        P_ERROR := '- No se encontro el concepto en la cuenta corriente.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_DEL_CODDETALLE;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


FUNCTION F_VALIDAR_FECHA_DISPONIBLE (
        P_DATE   IN VARCHAR2
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_VALIDAR_FECHA_DISPONIBLE
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida que la fecha ENVIADA este dentro de las 48 horas para su atencion.
              se considdera la atencion hasta antes de finalizar el dia de finalizada las 48 horas

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS    
    V_RECEPTION_DATE          DATE;

BEGIN
--
      -- CONVERTIR LA FECHA A UN FORMATO VALIDO
      --SELECT TO_DATE('25/05/2017 11:02:47','dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = American') 
      SELECT TO_DATE(P_DATE,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = American') 
      INTO V_RECEPTION_DATE
      FROM DUAL;

    -- Valida que la fecha este SEA MENOR A 3 DIAS DE TOLERANCIA.
    IF(SYSDATE < TO_DATE(TO_CHAR(V_RECEPTION_DATE + 3,'dd/mm/yyyy'),'dd/mm/yyyy')) THEN
        RETURN 'TRUE';
    ELSE
        RETURN 'FALSE';
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END F_VALIDAR_FECHA_DISPONIBLE;  


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_GET_STUDENT_INPUTS (
      P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_PROGRAM_OLD           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_PROGRAM_OLD_DESC      OUT VARCHAR2,
      P_PROGRAM_NEW           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_PROGRAM_NEW_DESC      OUT VARCHAR2,
      P_COMENTARIO_ALUMNO     OUT VARCHAR2,
      P_TELEFONO_ALUMNO       OUT VARCHAR2,
      P_OPT_CONSEJERIA        OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 11/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Para Solicitudes 'SOL013' : 
              Obtiene EL PROGRAMA, PROGRAM_DESC, COMENTARIO Y TELEFONO de una solicitud especifica. 
              Se establecio que ultimo dato es el TELEFONO y el penultimo el COMENTARIO.

  NRO     FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
AS
      V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_COMENTARIO              VARCHAR2(4000);
      V_CLAVE                   VARCHAR2(4000);
      V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
      V_PIDM                    SPRIDEN.SPRIDEN_PIDM%TYPE;
      V_ERROR                   EXCEPTION; 
      
      -- OBTENER PROGRAMA ACTUAL(Carrera) y  descriocion
      CURSOR C_PROGRAM_OLD IS
        SELECT SORLCUR_PROGRAM, SZVMAJR_DESCRIPTION FROM (
                SELECT  SORLCUR_PROGRAM,SZVMAJR_DESCRIPTION
                FROM SORLCUR        
                INNER JOIN SORLFOS
                    ON SORLCUR_PIDM         = SORLFOS_PIDM 
                    AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                INNER JOIN SZVMAJR 
                    ON SORLCUR_PROGRAM = SZVMAJR_CODE 
                WHERE SORLCUR_PIDM          = V_PIDM 
                    AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                    AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                    AND SORLCUR_CURRENT_CDE = 'Y'
                ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM = 1;
      

      /*
      OBTENER CARRERA Y SU DESCRIPCION(PROGRAM OLD),COMENTARIO ,EL TELEFONO Y LA CONFIRMACION DE CONSEJERIA
      DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      */
      CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
      
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE, SVRSVPR_PIDM
      INTO V_CODIGO_SOLICITUD, V_PIDM
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- OBTENER PROGRAMA ACTUAL(Carrera) y  descriocion
      OPEN C_PROGRAM_OLD;
        LOOP
          FETCH C_PROGRAM_OLD INTO P_PROGRAM_OLD, P_PROGRAM_OLD_DESC;
          EXIT WHEN C_PROGRAM_OLD%NOTFOUND;
        END LOOP;
      CLOSE C_PROGRAM_OLD;
      
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 1 THEN
                  -- OBTENER NUEVO PROGRAMA (Carrera) y su descriocion
                  P_PROGRAM_NEW := V_CLAVE;
                  P_PROGRAM_NEW_DESC := V_COMENTARIO;
              ELSIF V_ADDL_DATA_SEQ = 2 THEN
                  -- OBTENER COMENTARIO DE LA SOLICITUD
                  P_COMENTARIO_ALUMNO := V_COMENTARIO;
              ELSIF V_ADDL_DATA_SEQ = 3 THEN
                  -- OBTENER EL TELEFONO DE LA SOLICITUD 
                  P_TELEFONO_ALUMNO := V_COMENTARIO;
              ELSIF V_ADDL_DATA_SEQ = 4 THEN
                  -- GET OPCION DE CONFIRMACION DE CONSEJERIA
                  P_OPT_CONSEJERIA := V_CLAVE;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;
      
      IF (P_TELEFONO_ALUMNO IS NULL OR P_COMENTARIO_ALUMNO IS NULL OR P_PROGRAM_NEW IS NULL) THEN
          RAISE V_ERROR;
      END IF;
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_GET_CORDINADOR_PROGRAM (
      P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_ID                OUT SPRIDEN.SPRIDEN_ID%TYPE,
      P_NOMBREC           OUT VARCHAR2,
      P_CORREO            OUT VARCHAR2,
      P_PHONE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_CORDINADOR_PROGRAM
  FECHA     : 19/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : A traves de un codigo de carrera Ejm "110" se obtiene los datos del coordinador
  =================================================================================================================== */
AS
      V_PIDM              SPRIDEN.SPRIDEN_PIDM%TYPE;
      V_NOM_APP           VARCHAR2(200);
      
      ------- GET MAIL
      CURSOR C_CORREO IS
        SELECT GOREMAL_EMAIL_ADDRESS FROM (
              SELECT ORDEN, GOREMAL_EMAIL_ADDRESS FROM (
                  SELECT 1 ORDEN, GOREMAL_EMAIL_ADDRESS FROM GOREMAL 
                  WHERE GOREMAL_PIDM = V_PIDM and GOREMAL_STATUS_IND = 'A' AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                  UNION
                  SELECT 2 ORDEN, GOREMAL_EMAIL_ADDRESS FROM GOREMAL
                  WHERE GOREMAL_PIDM = V_PIDM and GOREMAL_STATUS_IND = 'A' AND GOREMAL_PREFERRED_IND = 'Y' AND GOREMAL_EMAIL_ADDRESS NOT LIKE ('%@continental.edu.pe')
                  UNION
                  SELECT 3 ORDEN, GOREMAL_EMAIL_ADDRESS FROM GOREMAL
                  WHERE GOREMAL_PIDM = V_PIDM and GOREMAL_STATUS_IND = 'A' AND GOREMAL_PREFERRED_IND <> 'Y' AND GOREMAL_EMAIL_ADDRESS NOT LIKE ('%@continental.edu.pe')
              ) ORDER BY ORDEN
        ) WHERE ROWNUM = 1;
      
        ------- GET PHONE
        CURSOR C_PHONE IS
          SELECT PHONE FROM (
              -- ORDEN para obtener el telefono prefrencial, caso contrario un telefono no preferencial.
              SELECT PHONE FROM (
                  SELECT ORDEN, PHONE FROM (
                      -- Telefono preferencial
                      SELECT 1 ORDEN, TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PHONE
                      FROM SPRTELE LEFT JOIN STVTELE ON SPRTELE_TELE_CODE = STVTELE_CODE
                      WHERE SPRTELE_PIDM = V_PIDM AND SPRTELE_PRIMARY_IND = 'Y' AND SPRTELE_STATUS_IND IS NULL
                      AND (TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%<%' 
                            OR TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%>%')
                      ORDER BY SPRTELE_SEQNO
                  )
                  UNION
                  SELECT ORDEN, PHONE FROM (
                      -- Telefono no preferencial
                      SELECT 2 ORDEN, TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PHONE
                      FROM SPRTELE LEFT JOIN STVTELE ON SPRTELE_TELE_CODE = STVTELE_CODE
                      WHERE SPRTELE_PIDM = V_PIDM AND SPRTELE_PRIMARY_IND IS NULL AND SPRTELE_STATUS_IND IS NULL
                      AND (TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%<%' 
                            OR TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%>%')
                      ORDER BY SPRTELE_SEQNO
                  )
              ) ORDER BY ORDEN
          ) WHERE ROWNUM = 1;
BEGIN 
--     
      P_NOMBREC := '-';
      P_CORREO  := '-';
      P_PHONE   := '-';
      
      -- GET ID, PIDM , NOMBRES
      SELECT DNI, SPRIDEN_PIDM, (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME) NAMES 
      INTO P_ID, V_PIDM, P_NOMBREC
      FROM ADVISOR 
      INNER JOIN SPRIDEN
      ON SPRIDEN_ID = DNI
      WHERE TIPO_ASESOR = 'COR' 
      AND CARRERA = P_PROGRAM_NEW 
      AND NIVEL = 'PG';
      
      ------- GET MAIL
      OPEN C_CORREO;
      LOOP
          FETCH C_CORREO INTO P_CORREO;
          EXIT WHEN C_CORREO%NOTFOUND;
      END LOOP;
      CLOSE C_CORREO;
      
      ------- GET PHONE
      OPEN C_PHONE;
      LOOP
          FETCH C_PHONE INTO P_PHONE;
          EXIT WHEN C_PHONE%NOTFOUND;
      END LOOP;
      CLOSE C_PHONE;
      
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_CORDINADOR_PROGRAM;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_SET_CAMBIAR_CARRERA ( 
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PER_CATALOGO        IN VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_CAMBIAR_ESTATUS
  FECHA     : 18/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cambia la carrera al estudiante.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        V_TERM_CODE_LAST          SFBETRM.SFBETRM_TERM_CODE%TYPE;
        V_CURR_RULE               SOBCURR.SOBCURR_CURR_RULE%TYPE;
        V_COLL_CODE               SOBCURR.SOBCURR_COLL_CODE%TYPE;
        V_DEGC_CODE               SOBCURR.SOBCURR_DEGC_CODE%TYPE;
        V_CMJR_RULE               SORCMJR.SORCMJR_CMJR_RULE%TYPE;
        V_PER_CATALOGO            VARCHAR2(6);
        V_INDICADOR               NUMBER;
        V_MESSAGE                 EXCEPTION;
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SGRSTSP IS
          SELECT * FROM (
              SELECT *  FROM SGRSTSP 
              WHERE SGRSTSP_PIDM = P_PIDM
              AND SGRSTSP_TERM_CODE_EFF <= P_TERM_CODE
              ORDER BY SGRSTSP_KEY_SEQNO DESC
          ) WHERE ROWNUM = 1;
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SGBSTDN IS
        SELECT * FROM (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM
            AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE
         );
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SORLCUR IS
        SELECT * FROM (
            SELECT * FROM SORLCUR 
            WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        )WHERE ROWNUM = 1;
        
        V_SGRSTSP_KEY_SEQNO     SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
        V_SGRSTSP_REC           SGRSTSP%ROWTYPE;
        V_SGBSTDN_REC           SGBSTDN%ROWTYPE;
        V_SORLCUR_REC           SORLCUR%ROWTYPE;
        V_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE;
        V_SORLCUR_SEQNO_INC     SORLCUR.SORLCUR_SEQNO%TYPE;
        V_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE;
        
        V_INDICADOR             NUMBER;
BEGIN
--        
          -- Calculando periodo CATALOGO
          V_PER_CATALOGO := CASE P_PER_CATALOGO WHEN '2015' THEN '201710'
                                                WHEN '2007' THEN '201420'
                                                ELSE NULL END;
          IF V_PER_CATALOGO IS NULL THEN
                  RAISE V_MESSAGE;
          END IF;
          
          
          -- #######################################################################
          -- INSERT  ------> PLAN ESTUDIO -
          OPEN C_SGRSTSP;
          LOOP
              FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
              EXIT WHEN C_SGRSTSP%NOTFOUND;
              
              -- GET SGRSTSP_KEY_SEQNO NUEVO
              SELECT SGRSTSP_KEY_SEQNO + 1 INTO V_SGRSTSP_KEY_SEQNO FROM (
                  SELECT SGRSTSP_KEY_SEQNO  FROM SGRSTSP 
                  WHERE SGRSTSP_PIDM = P_PIDM
                  ORDER BY SGRSTSP_KEY_SEQNO DESC
              ) WHERE ROWNUM = 1;

              
              INSERT INTO SGRSTSP (
                      SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                      SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                      SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                      SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                      SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                      SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                      SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                      SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                      SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
              SELECT 
                      V_SGRSTSP_REC.SGRSTSP_PIDM,       P_TERM_CODE,                          V_SGRSTSP_KEY_SEQNO,
                      'AS',                             SYSDATE,                              'WorkFlow',
                      USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                      V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                      V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                      V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                      V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
              FROM DUAL;              
          END LOOP;
          CLOSE C_SGRSTSP;
          
          -- #######################################################################
          -- Get Reglas de Curriculum (CURR_RULE)
          SELECT  SOBCURR_CURR_RULE,  SOBCURR_COLL_CODE,  SOBCURR_DEGC_CODE,  SORCMJR_CMJR_RULE
          INTO    V_CURR_RULE,        V_COLL_CODE,        V_DEGC_CODE,        V_CMJR_RULE
          FROM (
                SELECT * FROM SOBCURR
                INNER JOIN (
                    SELECT  SOBCURR_CURR_RULE CURR_RULE, SOBCURR_CAMP_CODE CAMP_CODE, SOBCURR_COLL_CODE COLL_CODE, 
                            SOBCURR_DEGC_CODE DEGC_CODE, SOBCURR_PROGRAM CPROGRAM, MAX(SOBCURR_TERM_CODE_INIT) TERM_CODE_INIT
                    FROM SOBCURR 
                    WHERE SOBCURR_LEVL_CODE = 'PG' 
                    GROUP BY SOBCURR_CURR_RULE, SOBCURR_CAMP_CODE, SOBCURR_COLL_CODE, SOBCURR_DEGC_CODE, SOBCURR_PROGRAM
                )ON SOBCURR_CURR_RULE = CURR_RULE
                AND SOBCURR_CAMP_CODE = CAMP_CODE
                AND SOBCURR_COLL_CODE = COLL_CODE
                AND SOBCURR_DEGC_CODE = DEGC_CODE
                AND SOBCURR_PROGRAM = CPROGRAM
                AND SOBCURR_TERM_CODE_INIT = TERM_CODE_INIT
          ) 
          INNER JOIN (
                SELECT * FROM SORCMJR
                INNER JOIN (
                    SELECT SORCMJR_CURR_RULE CURR_RULE, SORCMJR_MAJR_CODE MAJR_CODE, SORCMJR_DEPT_CODE DEPT_CODE, MAX(SORCMJR_TERM_CODE_EFF) TERM_CODE_EFF
                    FROM SORCMJR 
                    GROUP BY SORCMJR_CURR_RULE, SORCMJR_MAJR_CODE, SORCMJR_DEPT_CODE
                )ON SORCMJR_CURR_RULE = CURR_RULE
                AND SORCMJR_MAJR_CODE = MAJR_CODE
                AND SORCMJR_DEPT_CODE = DEPT_CODE
                AND SORCMJR_TERM_CODE_EFF = TERM_CODE_EFF
          )
          ON SOBCURR_CURR_RULE = SORCMJR_CURR_RULE
          WHERE SOBCURR_CAMP_CODE = P_CAMP_CODE
          AND SOBCURR_LEVL_CODE = 'PG'
          AND SORCMJR_DEPT_CODE = P_DEPT_CODE
          AND SOBCURR_PROGRAM = P_PROGRAM_NEW;

          
          -- UPDATE SGASTDN
          UPDATE SGBSTDN
          SET  SGBSTDN_STST_CODE = 'AS'  ------------------ ESTADOS STVSTST : 'AS' - Activo
              ,SGBSTDN_STYP_CODE = 'C'   ------------------ ESTADOS STVSTYP : 'C' - Regular
              ,SGBSTDN_COLL_CODE_1 = V_COLL_CODE
              ,SGBSTDN_DEGC_CODE_1 = V_DEGC_CODE
              ,SGBSTDN_MAJR_CODE_1 = P_PROGRAM_NEW
              ,SGBSTDN_ACTIVITY_DATE = SYSDATE
              ,SGBSTDN_PROGRAM_1 = P_PROGRAM_NEW
              ,SGBSTDN_TERM_CODE_CTLG_1 = V_PER_CATALOGO
              ,SGBSTDN_CURR_RULE_1 = V_CURR_RULE
              ,SGBSTDN_CMJR_RULE_1_1 = V_CMJR_RULE
              ,SGBSTDN_DATA_ORIGIN = 'WorkFlow'
              ,SGBSTDN_USER_ID = USER
          WHERE SGBSTDN_PIDM = P_PIDM
          AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
          
          -- #######################################################################
          -- INSERT  ------> SGBSTDN -
          OPEN C_SGBSTDN;
          LOOP
              FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
              EXIT WHEN C_SGBSTDN%NOTFOUND;

                INSERT INTO SGBSTDN (
                          SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                          SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                          SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                          SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                          SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                          SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                          SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                          SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                          SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                          SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                          SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                          SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                          SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                          SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                          SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                          SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                          SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                          SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                          SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                          SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                          SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                          SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                          SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                          SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                          SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                          SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                          SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                          SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                          SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                          SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                          SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                          SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                          SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                          SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                          SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                          SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                          SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
                VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_TERM_CODE,                                 'AS', /*AS-Activo*/ 
                          V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            'C',/*C-Regilar*/                             V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                          V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                          V_COLL_CODE,                                V_DEGC_CODE,                                  P_PROGRAM_NEW,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                          V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                          V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                          V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                          SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                          V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                          V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                          V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                          V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                          V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                          V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        P_PROGRAM_NEW,                                V_PER_CATALOGO,
                          V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                          V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                          V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                          V_CURR_RULE,                                V_CMJR_RULE,                                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                          V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                          V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                          V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                          USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
          END LOOP;
          CLOSE C_SGBSTDN;
          
          -- #######################################################################
          -- INSERT  ------> SORLCUR -
          OPEN C_SORLCUR;
          LOOP
              FETCH C_SORLCUR INTO V_SORLCUR_REC;
              EXIT WHEN C_SORLCUR%NOTFOUND;    
              
              -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
              V_SORLCUR_SEQNO_INC := V_SORLCUR_REC.SORLCUR_SEQNO + 1;
              -- GET SORLCUR_SEQNO NUEVO 
              V_SORLCUR_SEQNO_NEW := V_SORLCUR_REC.SORLCUR_SEQNO + 2;
              -- GET SORLCUR_SEQNO ANTERIOR 
              V_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;

              
              -- INACTIVE --- SORLCUR - (1 DE 2)
              INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SGRSTSP_KEY_SEQNO,/*SGRSTSP*/             V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                          V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          NULL /*SORLCUR_CURRENT_CDE*/ );
              
              -- ACTIVE --- SORLCUR - (2 DE 2)
              INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SGRSTSP_KEY_SEQNO,/*SGRSTSP*/             V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                          V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_COLL_CODE,                            V_DEGC_CODE,                                V_PER_CATALOGO /*SORLCUR_TERM_CODE_CTLG*/,
                          NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            P_PROGRAM_NEW,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          V_SORLCUR_REC.SORLCUR_CURRENT_CDE );
  
              
              -- UPDATE TERM_CODE_END (vigencia curriculum del Registro ANTERIOR) 
              UPDATE SORLCUR 
              SET   SORLCUR_TERM_CODE_END = P_TERM_CODE, 
                    SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
              WHERE   SORLCUR_PIDM = P_PIDM 
              AND     SORLCUR_LMOD_CODE = 'LEARNER'
              AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
              
              -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo)  PARA registros del mismo PERIODO.
              UPDATE SORLCUR 
              SET   SORLCUR_CURRENT_CDE = NULL
              WHERE   SORLCUR_PIDM = P_PIDM 
              AND     SORLCUR_LMOD_CODE = 'LEARNER'
              AND     SORLCUR_TERM_CODE_END = P_TERM_CODE
              AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;    
              
          END LOOP;
          CLOSE C_SORLCUR;
          
          -- #######################################################################
          -- INSERT --- SORLFOS - (1 DE 2) 
          INSERT INTO SORLFOS (
                SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                SORLFOS_CURRENT_CDE) 
          SELECT 
                V_SORLFOS_REC.SORLFOS_PIDM,             V_SORLCUR_SEQNO_INC,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                'CHANGED'/*SORLFOS_CSTS_CODE*/,         'INACTIVE'/*SORLFOS_CACT_CODE*/,          V_SORLFOS_REC.SORLFOS_DATA_ORIGIN,
                USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
                V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
                NULL /*SORLFOS_CURRENT_CDE*/
          FROM SORLFOS V_SORLFOS_REC
          WHERE SORLFOS_PIDM = P_PIDM
          AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
          
          -- INSERT --- SORLFOS - (2 DE 2)
          INSERT INTO SORLFOS (
                SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                SORLFOS_CURRENT_CDE) 
          SELECT 
                V_SORLFOS_REC.SORLFOS_PIDM,                 V_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                V_SORLFOS_REC.SORLFOS_LFST_CODE,            P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                V_SORLFOS_REC.SORLFOS_CSTS_CODE,            V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
                USER,                                       SYSDATE,                                  P_PROGRAM_NEW /*SORLFOS_MAJR_CODE*/,
                V_PER_CATALOGO /*SORLFOS_TERM_CODE_CTLG*/,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,     V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH,     V_CMJR_RULE /*SORLFOS_LFOS_RULE*/,        V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                V_SORLFOS_REC.SORLFOS_START_DATE,           V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,         USER,                                     SYSDATE,
                'Y'
          FROM SORLFOS V_SORLFOS_REC
          WHERE SORLFOS_PIDM = P_PIDM
          AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
           
          -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
          UPDATE SORLFOS 
          SET   SORLFOS_CURRENT_CDE = NULL
          WHERE   SORLFOS_PIDM = P_PIDM
          AND     SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
          AND     SORLFOS_CSTS_CODE = 'INPROGRESS';
        
        ------------------------------------------------------------------------------------
        
        --
        COMMIT;
EXCEPTION
    WHEN V_MESSAGE THEN
          P_ERROR := '- El periodo de CATALOGO es obligatorio.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
    WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CAMBIAR_CARRERA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CREATE_CTACORRIENTE (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_NUM_CTACORRIENTE      NUMBER;
      V_PART_PERIODO          VARCHAR2(9);
      V_RESULT                INTEGER;
BEGIN 
--
        --**************************************************************************************************************
        -- GET CRONOGRAMA SECCIONC
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                  WHEN 'UPGT' THEN 'W' 
                                  WHEN 'UREG' THEN 'R' 
                                  WHEN 'UPOS' THEN '-' 
                                  WHEN 'ITEC' THEN '-' 
                                  WHEN 'UCIC' THEN '-' 
                                  WHEN 'UCEC' THEN '-' 
                                  WHEN 'ICEC' THEN '-' 
                                  ELSE '1' END ||
                CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                  WHEN 'F01' THEN 'A' 
                                  WHEN 'F02' THEN 'L' 
                                  WHEN 'F03' THEN 'C' 
                                  WHEN 'V00' THEN 'V' 
                                  ELSE '9' END
        INTO V_PART_PERIODO
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
        
        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
        
        -- Validar si el estuidnate tiene CTA CORRIENTE. como C00(CONCPETO MATRICULA)
        WITH 
            CTE_tblSeccionC AS (
                    -- GET SECCIONC
                    SELECT  "IDSeccionC" IDSeccionC,
                            "FecInic" FecInic
                    FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDDependencia"='UCCI'
                    AND "IDsede"    = V_APEC_CAMP
                    AND "IDPerAcad" = V_APEC_TERM
                    AND "IDEscuela" = P_PROGRAM_NEW
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                    AND "IDSeccionC" <> '15NEX1A'
                    AND SUBSTRB("IDSeccionC",-2,2) IN (
                        -- PARTE PERIODO           
                        SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                    )
            )
        SELECT COUNT("IDAlumno") INTO V_NUM_CTACORRIENTE
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno"    = P_ID_ALUMNO
        AND "IDSede"      = V_APEC_CAMP
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDEscuela"   = P_PROGRAM_NEW
        AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
        AND "IDConcepto" = 'C00'; -- C00 Concepto Matricula

        --- Creacion CTA CORRIENTE.
        IF(V_NUM_CTACORRIENTE = 0) THEN
              -- ACTUALIZAR MONTO PAGAR (CREDITOS 10)
              ---------------------------------------
              -- OBSERVACIÒN: Para pruebas acceder al SCRIPT y comentar y descomentar la conexcion de "AT BANNER" --> "AT MIGR"
              ---------------------------------------
              V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
                    'dbo.sp_ActualizarMontoPagarBanner "'
                    || 'UCCI' ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_APEC_TERM ||'" , "'|| P_ID_ALUMNO ||'"' 
              );
        END IF;
        
        COMMIT;
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CREATE_CTACORRIENTE;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_GET_ABONO_CTA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_ABONO_OLD           OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_GET_ABONO_CTA
  FECHA     : 26/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el abono en una cta corriente especifica.
              
  MODIFICACIONES
  NRO       FECHA       USUARIO         MODIFICACION
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_PART_PERIODO          VARCHAR2(9);
      V_ABONO_OLD             NUMBER := 0;
BEGIN 
--
        --**************************************************************************************************************
        -- GET CRONOGRAMA SECCIONC
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                  WHEN 'UPGT' THEN 'W' 
                                  WHEN 'UREG' THEN 'R' 
                                  WHEN 'UPOS' THEN '-' 
                                  WHEN 'ITEC' THEN '-' 
                                  WHEN 'UCIC' THEN '-' 
                                  WHEN 'UCEC' THEN '-' 
                                  WHEN 'ICEC' THEN '-' 
                                  ELSE '1' END ||
                CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                  WHEN 'F01' THEN 'A' 
                                  WHEN 'F02' THEN 'L' 
                                  WHEN 'F03' THEN 'C' 
                                  WHEN 'V00' THEN 'V' 
                                  ELSE '9' END
        INTO V_PART_PERIODO
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
        
        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
        
        -- Validar si el estuidnate tiene CTA CORRIENTE. como C00(CONCPETO MATRICULA)
        WITH 
            CTE_tblSeccionC AS (
                    -- GET SECCIONC
                    SELECT  "IDSeccionC" IDSeccionC,
                            "FecInic" FecInic
                    FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDDependencia"='UCCI'
                    AND "IDsede"    = V_APEC_CAMP
                    AND "IDPerAcad" = V_APEC_TERM
                    AND "IDEscuela" = P_PROGRAM_OLD
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                    AND "IDSeccionC" <> '15NEX1A'
                    AND SUBSTRB("IDSeccionC",-2,2) IN (
                        -- PARTE PERIODO           
                        SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                    )
            ),
            CTE_tblPersonaAlumno AS (
                    SELECT "IDAlumno" IDAlumno 
                    FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDPersona" IN ( 
                        SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM
                    )
            )
        SELECT SUM("Abono") INTO V_ABONO_OLD
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
        AND "IDSede"      = V_APEC_CAMP
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDEscuela"   = P_PROGRAM_OLD
        AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC);
        
        P_ABONO_OLD := CASE WHEN V_ABONO_OLD IS NULL THEN 0 ELSE V_ABONO_OLD END;
        
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ABONO_CTA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_REMOVE_CTA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_REMOVE_CTA
  FECHA     : 29/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Desabilita la cta corriente especifica por PIDM.
              
  MODIFICACIONES
  NRO       FECHA       USUARIO         MODIFICACION
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_PART_PERIODO          VARCHAR2(9);
      V_ABONO_OLD             NUMBER := 0;
      
      V_ROWNUM                NUMBER := 0;
      V_APEC_ALUMNO           VARCHAR2(15);
      V_IDSECCIONC            VARCHAR2(10);
      V_MESSAGE_EXCEP         EXCEPTION;
      
      -- GET IDSECCION Y APEC_IDALUMNO
      CURSOR C_APEC_CTA IS
      SELECT rownum, IDSeccionC, IDAlumno
      FROM (
              -- GET  IDSECCIONC Y IDALUMNO CON SU CTA CORRIENTE
              WITH 
                  CTE_tblSeccionC AS (
                          -- GET SECCIONC
                          SELECT  "IDSeccionC" IDSeccionC,
                                  "FecInic" FecInic
                          FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                          WHERE "IDDependencia"='UCCI'
                          AND "IDsede"    = V_APEC_CAMP
                          AND "IDPerAcad" = V_APEC_TERM
                          AND "IDEscuela" = P_PROGRAM_OLD
                          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                          AND "IDSeccionC" <> '15NEX1A'
                          AND SUBSTRB("IDSeccionC",-2,2) IN (
                              -- PARTE PERIODO           
                              SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                          )
                  ),
                  CTE_tblPersonaAlumno AS (
                          SELECT "IDAlumno" IDAlumno 
                          FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                          WHERE "IDPersona" IN ( 
                              SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM
                          )
                  )
              SELECT "IDSeccionC" IDSeccionC, "IDAlumno" IDAlumno
              FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
              WHERE "IDDependencia" = 'UCCI'
              AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
              AND "IDSede"      = V_APEC_CAMP
              AND "IDPerAcad"   = V_APEC_TERM
              AND "IDEscuela"   = P_PROGRAM_OLD
              AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
              GROUP BY "IDSeccionC", "IDAlumno"
        );
            
BEGIN 
--
        --**************************************************************************************************************
        -- GET CRONOGRAMA SECCIONC
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                  WHEN 'UPGT' THEN 'W' 
                                  WHEN 'UREG' THEN 'R' 
                                  WHEN 'UPOS' THEN '-' 
                                  WHEN 'ITEC' THEN '-' 
                                  WHEN 'UCIC' THEN '-' 
                                  WHEN 'UCEC' THEN '-' 
                                  WHEN 'ICEC' THEN '-' 
                                  ELSE '1' END ||
                CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                  WHEN 'F01' THEN 'A' 
                                  WHEN 'F02' THEN 'L' 
                                  WHEN 'F03' THEN 'C' 
                                  WHEN 'V00' THEN 'V' 
                                  ELSE '9' END
        INTO V_PART_PERIODO
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
        
        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
        
        
        -- GET ABONO A LA CUENTA ANTERIOR
        P_GET_ABONO_CTA(P_PIDM,P_ID_ALUMNO,P_DEPT_CODE,P_CAMP_CODE,P_TERM_CODE,P_PROGRAM_OLD,V_ABONO_OLD);


        -- GET IDSECCIONC Y APEC_IDALUMNO
        OPEN C_APEC_CTA;
        LOOP
            FETCH C_APEC_CTA INTO V_ROWNUM, V_IDSECCIONC, V_APEC_ALUMNO;
            EXIT WHEN C_APEC_CTA%NOTFOUND;                
        END LOOP;
        CLOSE C_APEC_CTA;

        
        /*****************************************************************************/        
        -- VALIDANDO
        IF(V_ABONO_OLD > 0) THEN
        
            --**QUE NO TENGA ABONOS
            P_MESSAGE := '- Se detecto ABONOS en la Cuenta Corriente.';
            RAISE V_MESSAGE_EXCEP;
            
        ELSIF V_ROWNUM > 1 THEN
            
            P_MESSAGE := '- Se detecto mas de una CTA Corriente al alumnestudiante.';
            RAISE V_MESSAGE_EXCEP;
            
        ELSIF V_ROWNUM = 1 THEN
                
            -- DESABILITAR cta corriente
            UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                SET "Estado" = 'E'
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    = V_APEC_ALUMNO
            AND "IDSede"      = V_APEC_CAMP
            AND "IDPerAcad"   = V_APEC_TERM
            AND "IDEscuela"   = P_PROGRAM_OLD
            AND "IDSeccionC"  = V_IDSECCIONC;
            
            COMMIT;
        END IF;
        
EXCEPTION
  WHEN V_MESSAGE_EXCEP THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_REMOVE_CTA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_GET_ASIG_RECONOCIDAS (
      P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_HTML1         OUT VARCHAR2,
      P_HTML2         OUT VARCHAR2,
      P_HTML3         OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_ASIG_RECONOCIDAS
  FECHA     : 30/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene la lista de asignaturas reconocidas.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    
    V_MESSAGE             EXCEPTION;
    V_HTML                VARCHAR2(4000);
    V_ORIGEN              VARCHAR2(30);
    V_NHTML               NUMBER := 0;
    V_CREDIT_SUM          NUMBER := 0;
    V_CREDIT_SUM_HTML     VARCHAR2(200); -- PARA MOSTRAR EN EL RPEORTE html LA SUMA DE CREDITOS RECONOCIDOS
    
    V_REQUEST_NO          SMRRQCM.SMRRQCM_REQUEST_NO%TYPE;
    V_CRSE_NUMB           SMRDOUS.SMRDOUS_CRSE_NUMB%TYPE;
    V_CRN                 SMRDOUS.SMRDOUS_CRN%TYPE;
    V_CRN_VAR             VARCHAR2(15);
    V_TITLE               SMRDOUS.SMRDOUS_TITLE%TYPE;
    V_CRSE_SOURCE         SMRDOUS.SMRDOUS_CRSE_SOURCE%TYPE; 
    V_CREDIT_HOURS        SMRDOUS.SMRDOUS_CREDIT_HOURS%TYPE;
    V_GRDE_CODE           SMRDOUS.SMRDOUS_GRDE_CODE%TYPE;
    
    
    -- CURSOR GET ultima ejecucion de CAP
    CURSOR C_REQUEST_NO IS
    SELECT SMRRQCM_REQUEST_NO FROM (
        SELECT SMRRQCM_REQUEST_NO FROM SMRRQCM WHERE SMRRQCM_PIDM = P_PIDM 
        ORDER BY SMRRQCM_REQUEST_NO DESC
      )WHERE ROWNUM = 1;
    
    
    /* GET LIST ASIGNATURAS RECONOCIDAS.
            SMRDOUS_CRSE_SOURCE:   H - History, T - Transfer, R - In-Progress, P - Planned
    */
    CURSOR C_ASIG_CAP IS
    SELECT SMRDOUS_CRSE_NUMB,SMRDOUS_CRN,SMRDOUS_TITLE, SMRDOUS_CRSE_SOURCE, SMRDOUS_CREDIT_HOURS, SMRDOUS_GRDE_CODE
    FROM (
        SELECT SMRDOUS_CRSE_NUMB,SMRDOUS_CRN,SMRDOUS_TITLE, SMRDOUS_CRSE_SOURCE, SMRDOUS_CREDIT_HOURS, SMRDOUS_GRDE_CODE
              FROM SMRDOUS 
              WHERE SMRDOUS_PIDM = P_PIDM
              AND SMRDOUS_REQUEST_NO = V_REQUEST_NO
              AND SMRDOUS_CRSE_SOURCE IN ('H','T')
              ORDER BY SMRDOUS_COMPLIANCE_ORDER
    ) WHERE ROWNUM < 80;
    
BEGIN
    
    P_HTML1 := '<tr></tr>';
    P_HTML2 := '<tr></tr>';
    P_HTML3 := '<tr></tr>';
    
    -- -- CURSOR GET ID ultima ejecucion de CAP   
    OPEN C_REQUEST_NO;
    LOOP
      FETCH C_REQUEST_NO INTO V_REQUEST_NO;
      EXIT WHEN C_REQUEST_NO%NOTFOUND;
    END LOOP;
    CLOSE C_REQUEST_NO;
 
    
    -- GET LIST ASIGNATURAS RECONOCIDAS.
    OPEN C_ASIG_CAP;
    LOOP
        FETCH C_ASIG_CAP INTO V_CRSE_NUMB, V_CRN, V_TITLE, V_CRSE_SOURCE, V_CREDIT_HOURS, V_GRDE_CODE;
        IF C_ASIG_CAP%FOUND THEN
            
            V_CREDIT_SUM := V_CREDIT_SUM + V_CREDIT_HOURS;
            
            V_HTML := '';
            /* ORIGEN:   H - History, T - Transfer, R - In-Progress, P - Planned*/
            V_ORIGEN := (CASE V_CRSE_SOURCE WHEN 'H' THEN 'Historial' WHEN 'T' THEN 'Transfer' ELSE '-' END);
            
            V_CRN_VAR := CASE WHEN NVL(TRIM(V_CRN),'-')='-' THEN '-' ELSE V_CRN END;
             
            /***********************************************************************************************************
            -- Exportar reporte de asignaturas reconocidas
            ***********************************************************************************************************/
            V_HTML := '<tr><td>' || V_CRSE_NUMB || '</td><td>' || V_CRN_VAR || '</td><td>' || V_TITLE || '</td><td>' || V_ORIGEN || '</td><td>' || V_CREDIT_HOURS || '</td><td>' || V_GRDE_CODE || '</td></tr>';
            
            IF    (LENGTH(P_HTML1) + LENGTH(V_HTML) < 4000) AND V_NHTML < 1 THEN
                      IF(P_HTML1='<tr></tr>')THEN P_HTML1:=''; END IF;
                      P_HTML1  := P_HTML1 || V_HTML;
            ELSIF (LENGTH(P_HTML2) + LENGTH(V_HTML) < 4000) AND V_NHTML < 2 THEN 
                      IF(P_HTML2='<tr></tr>')THEN P_HTML2:=''; END IF;
                      P_HTML2 := P_HTML2 || V_HTML;
                      V_NHTML  := 1;
            ELSIF (LENGTH(P_HTML3) + LENGTH(V_HTML) < 4000) AND V_NHTML < 3 THEN 
                      IF(P_HTML3='<tr></tr>')THEN P_HTML3:=''; END IF;
                      P_HTML3 := P_HTML3 || V_HTML;
                      V_NHTML  := 2;
            ELSE
                  RAISE V_MESSAGE;
            END IF;
            
        ELSE
            EXIT;
        END IF;
    END LOOP;
    CLOSE C_ASIG_CAP;

    
    V_CREDIT_SUM_HTML := '<tr style="border-top: 1px solid #202629;"><td style="" colspan="4"></td><td style="background-color: #a3a3a3;color: #202629;">'|| V_CREDIT_SUM ||'</td><td></td></tr>';
    --*************************************************************************************************************
    --  SUMA DE CREDITOS 
    IF(LENGTH(P_HTML3) + LENGTH(V_CREDIT_SUM_HTML)) < 4000 THEN 
        P_HTML3 := P_HTML3 || V_CREDIT_SUM_HTML;
    ELSE
        RAISE V_MESSAGE;
    END IF;
    
EXCEPTION
  WHEN V_MESSAGE THEN 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| '- La cantidad de registros supera la capacidad del los parametros retornados.' );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ASIG_RECONOCIDAS;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_STI_EXECCAPP (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_STI_EXECCAPP
  FECHA     : 08/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Ejecución de CAPP.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SZKECAP.P_EXEC_CAPP(P_PIDM,P_PROGRAM_NEW,P_TERM_CODE,LV_OUT);
    IF(LV_OUT <> '0')THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
  WHEN P_EXCEPTION THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_STI_EXECCAPP;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_STI_EXECPROY (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_STI_EXECPROY
  FECHA     : 08/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Ejecución de PROYECCION.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SZKECAP.P_EXEC_PROY(P_PIDM,P_PROGRAM_NEW,P_TERM_CODE,LV_OUT);
    IF(LV_OUT <> '0')THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
  WHEN P_EXCEPTION THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_STI_EXECPROY;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTISTI;