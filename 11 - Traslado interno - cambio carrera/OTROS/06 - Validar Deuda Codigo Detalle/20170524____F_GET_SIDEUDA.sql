/*
SET SERVEROUTPUT ON
DECLARE P_DEUDA VARCHAR2(10);
BEGIN
        P_GET_SIDEUDA(241106,'74288044','S01','201710','C01','0',P_DEUDA);
        DBMS_OUTPUT.PUT_LINE(P_DEUDA);
END;
*/


create or replace PROCEDURE P_GET_SIDEUDA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE, --------------- APEC
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, ------------- APEC
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE, ------------- APEC
      P_DETL_CODE           IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
      P_TRAN_NUMBER         IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
      P_DEUDA               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Verificar en base a un PIDM y el Num. Transaccion verifique la existencia de deuda. divisa PEN.
              - APEC verificar si el PIDM tiene deuda en algun concepto especifico para la seccion "DTA".

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      V_DEUDA_INDICADOR       NUMBER;
      -- APEC PARAMS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(10);
      V_APEC_IDSECCIONC       VARCHAR2(15);
      V_APEC_IDSECCIONC_INIC  DATE;
      V_APEC_MOUNT            NUMBER;      
      
BEGIN
--
    
    --#################################----- APEC -----#######################################
    --########################################################################################
    -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
    IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
          SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
          
          --**********************************************************************************************
          -- GET SECCIONC
          SELECT  "IDSeccionC" IDSeccionC, "FecInic" FecInic INTO V_APEC_IDSECCIONC, V_APEC_IDSECCIONC_INIC
          FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia"='UCCI'
          AND "IDPerAcad" = V_APEC_TERM
          AND "IDSeccionC" = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
          AND "IDSeccionC" <> '15NEX1A';

          -- GET DEUDA(s)
          SELECT "Deuda" INTO V_APEC_MOUNT FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    = P_ID_ALUMNO
          AND "IDSede"      = V_APEC_CAMP
          AND "IDPerAcad"   = V_APEC_TERM
          AND "IDSeccionC"  = V_APEC_IDSECCIONC
          AND "FecInic"     = V_APEC_IDSECCIONC_INIC
          AND "IDConcepto"  = P_DETL_CODE;
          
          V_DEUDA_INDICADOR := V_APEC_MOUNT;
          
          COMMIT;
    ELSE --**************************** 1 ---> BANNER ***************************
          
          -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
          TZKCDAA.p_calc_deuda_alumno(P_PIDM,'PEN');
          COMMIT;
              
          -- GET deuda
          SELECT COUNT(TZRCDAB_AMOUNT)
          INTO   V_DEUDA_INDICADOR
          FROM   TZRCDAB
          WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
          AND TZRCDAB_PIDM = P_PIDM
          --AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE
          AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
          
    END IF;        
   
    IF V_DEUDA_INDICADOR > 0 THEN
      P_DEUDA   := 'TRUE';
    ELSE
      P_DEUDA   :='FALSE';
    END IF;

END P_GET_SIDEUDA;
