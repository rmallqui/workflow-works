/*
drop procedure p_get_usuario_regaca;
GRANT EXECUTE ON p_get_usuario_regaca TO wfobjects;
GRANT EXECUTE ON p_get_usuario_regaca TO wfauto;

set serveroutput on
DECLARE
p_nombre                  VARCHAR2(255);
p_correo           VARCHAR2(255);
p_phone          VARCHAR2(255);
P_id                VARCHAR2(255);
begin
  P_GET_CORDINADOR_PROGRAM('110',P_id,p_nombre,p_correo,p_phone);
  DBMS_OUTPUT.PUT_LINE(P_id || '****' || p_nombre || '****' ||  p_correo || '---' || p_phone);
end;
*/


-- ## Se descarto su uso para esta ocación 201720 26/07/2017
create or replace PROCEDURE P_GET_CORDINADOR_PROGRAM (
      P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_ID                OUT SPRIDEN.SPRIDEN_ID%TYPE,
      P_NOMBREC           OUT VARCHAR2,
      P_CORREO            OUT VARCHAR2,
      P_PHONE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_CORDINADOR_PROGRAM
  FECHA     : 19/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : A traves de un codigo de carrera Ejm "110" se obtiene los datos del coordinador
  =================================================================================================================== */
AS
      V_PIDM              SPRIDEN.SPRIDEN_PIDM%TYPE;
      V_NOM_APP           VARCHAR2(200);
      
      ------- GET MAIL
      CURSOR C_CORREO IS
        SELECT GOREMAL_EMAIL_ADDRESS FROM (
              SELECT ORDEN, GOREMAL_EMAIL_ADDRESS FROM (
                  SELECT 1 ORDEN, GOREMAL_EMAIL_ADDRESS FROM GOREMAL 
                  WHERE GOREMAL_PIDM = V_PIDM and GOREMAL_STATUS_IND = 'A' AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                  UNION
                  SELECT 2 ORDEN, GOREMAL_EMAIL_ADDRESS FROM GOREMAL
                  WHERE GOREMAL_PIDM = V_PIDM and GOREMAL_STATUS_IND = 'A' AND GOREMAL_PREFERRED_IND = 'Y' AND GOREMAL_EMAIL_ADDRESS NOT LIKE ('%@continental.edu.pe')
                  UNION
                  SELECT 3 ORDEN, GOREMAL_EMAIL_ADDRESS FROM GOREMAL
                  WHERE GOREMAL_PIDM = V_PIDM and GOREMAL_STATUS_IND = 'A' AND GOREMAL_PREFERRED_IND <> 'Y' AND GOREMAL_EMAIL_ADDRESS NOT LIKE ('%@continental.edu.pe')
              ) ORDER BY ORDEN
        ) WHERE ROWNUM = 1;
      
        ------- GET PHONE
        CURSOR C_PHONE IS
          SELECT PHONE FROM (
              -- ORDEN para obtener el telefono prefrencial, caso contrario un telefono no preferencial.
              SELECT PHONE FROM (
                  SELECT ORDEN, PHONE FROM (
                      -- Telefono preferencial
                      SELECT 1 ORDEN, TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PHONE
                      FROM SPRTELE LEFT JOIN STVTELE ON SPRTELE_TELE_CODE = STVTELE_CODE
                      WHERE SPRTELE_PIDM = V_PIDM AND SPRTELE_PRIMARY_IND = 'Y' AND SPRTELE_STATUS_IND IS NULL
                      AND (TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%<%' 
                            OR TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%>%')
                      ORDER BY SPRTELE_SEQNO
                  )
                  UNION
                  SELECT ORDEN, PHONE FROM (
                      -- Telefono no preferencial
                      SELECT 2 ORDEN, TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PHONE
                      FROM SPRTELE LEFT JOIN STVTELE ON SPRTELE_TELE_CODE = STVTELE_CODE
                      WHERE SPRTELE_PIDM = V_PIDM AND SPRTELE_PRIMARY_IND IS NULL AND SPRTELE_STATUS_IND IS NULL
                      AND (TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%<%' 
                            OR TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) NOT LIKE '%>%')
                      ORDER BY SPRTELE_SEQNO
                  )
              ) ORDER BY ORDEN
          ) WHERE ROWNUM = 1;
BEGIN 
--     
      P_NOMBREC := '-';
      P_CORREO  := '-';
      P_PHONE   := '-';
      
      -- GET ID, PIDM , NOMBRES
      SELECT DNI, SPRIDEN_PIDM, (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME) NAMES 
      INTO P_ID, V_PIDM, P_NOMBREC
      FROM ADVISOR 
      INNER JOIN SPRIDEN
      ON SPRIDEN_ID = DNI
      WHERE TIPO_ASESOR = 'COR' 
      AND CARRERA = P_PROGRAM_NEW 
      AND NIVEL = 'PG';
      
      ------- GET MAIL
      OPEN C_CORREO;
      LOOP
          FETCH C_CORREO INTO P_CORREO;
          EXIT WHEN C_CORREO%NOTFOUND;
      END LOOP;
      CLOSE C_CORREO;
      
      ------- GET PHONE
      OPEN C_PHONE;
      LOOP
          FETCH C_PHONE INTO P_PHONE;
          EXIT WHEN C_PHONE%NOTFOUND;
      END LOOP;
      CLOSE C_PHONE;
      
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_CORDINADOR_PROGRAM;