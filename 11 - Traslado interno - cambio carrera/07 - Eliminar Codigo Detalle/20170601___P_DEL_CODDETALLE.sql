/*
-- buscar objecto
select dbms_metadata.get_ddl('PROCEDURE','P_SET_CODDETALLE') from dual
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

SET SERVEROUTPUT ON
DECLARE   P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
          P_ERROR                 VARCHAR2(100);
          p_date                  DATE;
BEGIN
    P_DEL_CODDETALLE(241106,'74288044','S01','201710','C01',P_TRAN_NUMBER,P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_TRAN_NUMBER  || '--' || P_ERROR);
END;
*/


CREATE OR REPLACE PROCEDURE P_DEL_CODDETALLE ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, 
        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_DEL_CODDETALLE
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE.
              APEC-BDUCCI: Amortiza un cargo al estudiante con mismo costo del CONCEPTO. 

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_NOM_SERVICIO            VARCHAR2(30);
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    V_MESSAGE                 EXCEPTION;
    V_TRAN_NUMBER_OUT         TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
    V_ROWID                   GB_COMMON.INTERNAL_RECORD_ID_TYPE;
    
    -- APEC PARAMS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    V_APEC_IDSECCIONC_INIC  DATE;
    V_APEC_MOUNT            NUMBER;
    
    V_FOUND_AMOUNT          TBRACCD.TBRACCD_AMOUNT%TYPE;
    V_COD_DETALLE           TBRACCD.TBRACCD_DETAIL_CODE%TYPE;
    V_TERM_CODE             STVTERM.STVTERM_CODE%TYPE;
BEGIN
      

      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)

            SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
            SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
            
            
            --**********************************************************************************
            -- GET datos CUENTA APEC
            SELECT t1."IDSeccionC", t1."FecInic", t1."Monto" INTO  V_APEC_IDSECCIONC, V_APEC_IDSECCIONC_INIC, V_APEC_MOUNT
              FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
              INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
                ON t1."IDSede"          = t2."IDsede"
                AND t1."IDPerAcad"      = t2."IDPerAcad" 
                AND t1."IDDependencia"  = t2."IDDependencia" 
                AND t1."IDSeccionC"     = t2."IDSeccionC" 
                AND t1."FecInic"        = t2."FecInic"
              WHERE t2."IDDependencia"  = 'UCCI'
                -- AND t2."IDsede"        = V_APEC_CAMP -- Para la SeccionC especial es indistinto la SEDE
                AND t2."IDSeccionC"     = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
                AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
                AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
                AND t2."IDSeccionC"     <> '15NEX1A'
                AND t2."IDPerAcad"      = V_APEC_TERM
                AND T1."IDPerAcad"      = V_APEC_TERM
                AND t1."IDConcepto"     = P_DETL_CODE;
            
            --******************************************************************************
            -- Validar si existe la cuenta del estudiante  para traslado interno
            SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDSede" = V_APEC_CAMP
            AND "IDAlumno" = P_ID_ALUMNO
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDDependencia" = 'UCCI'
            AND "IDSeccionC" = V_APEC_IDSECCIONC
            AND "FecInic" = V_APEC_IDSECCIONC_INIC
            AND "IDConcepto" = P_DETL_CODE;
            
            IF (V_INDICADOR = 1) THEN
                  -- UPDATE MONTO(s)
                  UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                  SET "Cargo" = "Cargo" - V_APEC_MOUNT
                  WHERE "IDDependencia" = 'UCCI'
                  AND "IDAlumno"    = P_ID_ALUMNO
                  AND "IDSede"      = V_APEC_CAMP
                  AND "IDPerAcad"   = V_APEC_TERM
                  AND "IDSeccionC"  = V_APEC_IDSECCIONC
                  AND "FecInic" = V_APEC_IDSECCIONC_INIC
                  AND "IDConcepto"  = P_DETL_CODE;
            ELSE
                  RAISE V_EXCEP_NOTFOUND_CARGO;
            END IF;
            
            
            P_TRAN_NUMBER := 0;

      ELSE -- 1 ---> BANNER

             -- Validar si la transaccion (DEUDA) NO tubo algun movimiento.
            SELECT COUNT(*) INTO V_INDICADOR
            FROM (
                    -- : listar LOS CODIGOS QUE YA TIENEN ALGUN MOVIMIENTO. : TRAN , CHG 
                    SELECT TBRAPPL_PIDM TEMP_PIDM ,TBRAPPL_PAY_TRAN_NUMBER TEMP_PAY_TRAN_NUMBER
                    FROM TBRAPPL
                    WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM
                    UNION 
                    SELECT TBRAPPL_PIDM, TBRAPPL_CHG_TRAN_NUMBER
                    FROM TBRAPPL
                    WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM
            ) WHERE P_TRAN_NUMBER = TEMP_PAY_TRAN_NUMBER;
            
            IF V_INDICADOR = 1 THEN
                  RAISE V_MESSAGE;
            END IF;              
                
            -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
            TZKCDAA.p_calc_deuda_alumno(P_PIDM,'PEN');
            COMMIT;
                
            -- GET , COD_DETALLE , AMOUNT(NEGATIVO) de la transaccion a cancelar  
            SELECT  (TZRCDAB_AMOUNT * (-1)), 
                    TZRCDAB_DETAIL_CODE, 
                    TZRCDAB_TERM_CODE
            INTO    V_FOUND_AMOUNT,
                    V_COD_DETALLE, 
                    V_TERM_CODE
            FROM   TZRCDAB
            WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
            AND TZRCDAB_PIDM = P_PIDM 
            AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
            
            -- GET DESCRIPCION del cod_detalle
            SELECT TBBDETC_DESC INTO V_NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = V_COD_DETALLE;
        
            -- #######################################################################
            -- GENERAR DEUDA
            TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM,        -- PIDEM ALUMNO
                                     p_term_code            =>  V_TERM_CODE,          -- DETALLE
                                     p_detail_code          =>  V_COD_DETALLE,      -- CODIGO DETALLE  -- mismo codigo detalle
                                     p_user                 =>  USER,               -- USUARIO
                                     p_entry_date           =>  SYSDATE,     
                                     p_amount               =>  V_FOUND_AMOUNT,
                                     p_effective_date       =>  SYSDATE,
                                     p_bill_date            =>  NULL,    
                                     p_due_date             =>  NULL,    
                                     p_desc                 =>  V_NOM_SERVICIO,       -- referencia por consultar 
                                     p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                     p_tran_number_paid     =>  P_TRAN_NUMBER,      -- numero de transaccion que ser� pagara
                                     p_crossref_pidm        =>  NULL,    
                                     p_crossref_number      =>  NULL,    
                                     p_crossref_detail_code =>  NULL,     
                                     p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                     p_acct_feed_ind        =>  'Y',
                                     p_session_number       =>  0,    
                                     p_cshr_end_date        =>  NULL,    
                                     p_crn                  =>  NULL,
                                     p_crossref_srce_code   =>  NULL,
                                     p_loc_mdt              =>  NULL,
                                     p_loc_mdt_seq          =>  NULL,    
                                     p_rate                 =>  NULL,    
                                     p_units                =>  NULL,     
                                     p_document_number      =>  NULL,    
                                     p_trans_date           =>  NULL,    
                                     p_payment_id           =>  NULL,    
                                     p_invoice_number       =>  NULL,    
                                     p_statement_date       =>  NULL,    
                                     p_inv_number_paid      =>  NULL,    
                                     p_curr_code            =>  NULL,    
                                     p_exchange_diff        =>  NULL,    
                                     p_foreign_amount       =>  NULL,    
                                     p_late_dcat_code       =>  NULL,    
                                     p_atyp_code            =>  NULL,    
                                     p_atyp_seqno           =>  NULL,    
                                     p_card_type_vr         =>  NULL,    
                                     p_card_exp_date_vr     =>  NULL,     
                                     p_card_auth_number_vr  =>  NULL,    
                                     p_crossref_dcat_code   =>  NULL,    
                                     p_orig_chg_ind         =>  NULL,    
                                     p_ccrd_code            =>  NULL,    
                                     p_merchant_id          =>  NULL,    
                                     p_data_origin          =>  'WORKFLOW',    
                                     p_override_hold        =>  'N',     
                                     p_tran_number_out      =>  V_TRAN_NUMBER_OUT, 
                                     p_rowid_out            =>  V_ROWID);
        
              ------------------------------------------------------------------------------  
                
                  -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
                  TZJAPOL.p_run_proc_tvrappl(P_ID_ALUMNO);
              
              ------------------------------------------------------------------------------  
      END IF;
      
      COMMIT;
EXCEPTION
  WHEN V_MESSAGE THEN
        P_ERROR  := '- La deuda tubo movimientos por lo que no se puede cancelar directamente.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN V_EXCEP_NOTFOUND_CARGO THEN
        P_ERROR := '- No se encontro el concepto en la cuenta corriente.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_DEL_CODDETALLE;