/*
SET SERVEROUTPUT ON
DECLARE   
     P_CREDITOS     VARCHAR2(4000);
     P_CICLO        VARCHAR2(4000);
BEGIN    
    P_CREDITOS_CICLO('241106',P_CREDITOS,P_CICLO); --241106 --145916
    DBMS_OUTPUT.PUT_LINE(P_CREDITOS);
    DBMS_OUTPUT.PUT_LINE(P_CICLO);
    
    P_CREDITOS_CICLO('145916',P_CREDITOS,P_CICLO); --241106 --145916
    DBMS_OUTPUT.PUT_LINE(P_CREDITOS);
    DBMS_OUTPUT.PUT_LINE(P_CICLO);
END;
*/


create or replace PROCEDURE P_CREDITOS_CICLO (
      P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_CREDITOS        OUT INTEGER,
      P_CICLO           OUT INTEGER
)
/* ===================================================================================================================
  NOMBRE    : P_CREDITOS_CICLO
  FECHA     : 08/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Calcula  los creditos y ciclo del estudiante.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    
    V_CRED_OBLIG          INTEGER := 0;  
    V_CRED_TOTAL          INTEGER := 0;
    V_CICLO_ACTL          INTEGER := 0;

    -- GET CREDITOS ACUMULADOS Y CICLO DEL ESTUDIANTE
    CURSOR C_CRED_CICLO IS
    SELECT UNIQUE
             NVL(CREDITOS_OBLIGATORIO,'0') AS CREDITOS_OBLIGATORIO,
             NVL(CREDITOS_ACTUAL,'0') AS CREDITOS_TOTAL,
    
             CASE WHEN CREDITOS_ACTUAL IS NULL THEN '1'
             ELSE
             NVL(CASE   WHEN CREDITOS_ACTUAL <= 22 AND EFF = '201510' THEN '1'
                        WHEN CREDITOS_ACTUAL > 22 AND CREDITOS_ACTUAL <= 44 AND EFF = '201510' THEN '2'
                        WHEN CREDITOS_ACTUAL > 44 AND CREDITOS_ACTUAL <= 66 AND EFF = '201510' THEN '3'
                        WHEN CREDITOS_ACTUAL > 66 AND CREDITOS_ACTUAL <= 88 AND EFF = '201510' THEN '4'
                        WHEN CREDITOS_ACTUAL > 88 AND CREDITOS_ACTUAL <= 110 AND EFF = '201510' THEN '5'
              
                        WHEN CREDITOS_ACTUAL > 110 AND CREDITOS_ACTUAL <= 135 AND EFF = '201510' THEN '6'
                        WHEN CREDITOS_ACTUAL > 135 AND CREDITOS_ACTUAL <= 160 AND EFF = '201510' THEN '7'
              
                        WHEN CREDITOS_ACTUAL > 160 AND CREDITOS_ACTUAL <= 182 AND EFF = '201510' THEN '8'
                        WHEN CREDITOS_ACTUAL > 182 AND CREDITOS_ACTUAL <= 204 AND EFF = '201510' THEN '9'
                        WHEN CREDITOS_ACTUAL > 204 AND CREDITOS_ACTUAL <= 226 AND EFF = '201510' THEN '10'
                        WHEN CREDITOS_ACTUAL > 226 AND CREDITOS_ACTUAL <= 248 AND EFF = '201510' AND PROGRAMA IN ('312','502') THEN '11'
                        WHEN CREDITOS_ACTUAL > 248 AND CREDITOS_ACTUAL <= 270 AND EFF = '201510' AND PROGRAMA IN ('312','502') THEN '12'
                        WHEN CREDITOS_ACTUAL > 270 AND CREDITOS_ACTUAL <= 314 AND EFF = '201510' AND PROGRAMA IN ('502') THEN '13'
                        WHEN CREDITOS_ACTUAL > 314 AND PROGRAMA IN ('502') THEN '14'
                        ELSE REPLACE(NVL(CASE   WHEN CREDITOS_ACTUAL <= 22 AND EFF = '200710' THEN '1'
                                                WHEN CREDITOS_ACTUAL > 22 AND CREDITOS_ACTUAL <= 44 AND EFF = '200710' THEN '2'
                                                WHEN CREDITOS_ACTUAL > 44 AND CREDITOS_ACTUAL <= 66 AND EFF = '200710' THEN '3'
                                                WHEN CREDITOS_ACTUAL > 66 AND CREDITOS_ACTUAL <= 88 AND EFF = '200710' THEN '4'
                                                WHEN CREDITOS_ACTUAL > 88 AND CREDITOS_ACTUAL <= 110 AND EFF = '200710' THEN '5'
                                                
                                                WHEN CREDITOS_ACTUAL > 110 AND CREDITOS_ACTUAL <= 132 AND EFF = '200710' THEN '6'
                                                WHEN CREDITOS_ACTUAL > 132 AND CREDITOS_ACTUAL <= 154 AND EFF = '200710' THEN '7'
                                                
                                                WHEN CREDITOS_ACTUAL > 154 AND CREDITOS_ACTUAL <= 176 AND EFF = '200710' THEN '8'
                                                WHEN CREDITOS_ACTUAL > 176 AND CREDITOS_ACTUAL <= 198 AND EFF = '200710' THEN '9'
                                                WHEN CREDITOS_ACTUAL > 198 AND CREDITOS_ACTUAL <= 220 AND EFF = '200710' THEN '10'
                                                WHEN CREDITOS_ACTUAL > 220 AND CREDITOS_ACTUAL <= 242 AND EFF = '200710' AND PROGRAMA IN ('312','502') THEN '11'
                                                WHEN CREDITOS_ACTUAL > 242 AND CREDITOS_ACTUAL <= 264 AND EFF = '200710' AND PROGRAMA IN ('312','502') THEN '12'
                                                WHEN CREDITOS_ACTUAL > 264 AND CREDITOS_ACTUAL <= 286 AND EFF = '200710' AND PROGRAMA IN ('502') THEN '13'
                                                WHEN CREDITOS_ACTUAL > 286 AND PROGRAMA IN ('502') THEN '14'
                                                ELSE REPLACE(NVL(CASE   WHEN CREDITOS_ACTUAL <= 22 AND EFF = '201710' THEN '1'
                                                                        WHEN CREDITOS_ACTUAL > 22 AND CREDITOS_ACTUAL <= 44 AND EFF = '201710' THEN '2'
                                                                        WHEN CREDITOS_ACTUAL > 44 AND CREDITOS_ACTUAL <= 66 AND EFF = '201710' THEN '3'
                                                                        WHEN CREDITOS_ACTUAL > 66 AND CREDITOS_ACTUAL <= 88 AND EFF = '201710' THEN '4'
                                                                        WHEN CREDITOS_ACTUAL > 88 AND CREDITOS_ACTUAL <= 110 AND EFF = '201710' THEN '5'
                                                                        
                                                                        WHEN CREDITOS_ACTUAL > 110 AND CREDITOS_ACTUAL <= 135 AND EFF = '201710' THEN '6'
                                                                        WHEN CREDITOS_ACTUAL > 135 AND CREDITOS_ACTUAL <= 160 AND EFF = '201710' THEN '7'
                                                                        
                                                                        WHEN CREDITOS_ACTUAL > 160 AND CREDITOS_ACTUAL <= 182 AND EFF = '201710' THEN '8'
                                                                        WHEN CREDITOS_ACTUAL > 182 AND CREDITOS_ACTUAL <= 204 AND EFF = '201710' THEN '9'
                                                                        WHEN CREDITOS_ACTUAL > 204 AND CREDITOS_ACTUAL <= 226 AND EFF = '201710' THEN '10'
                                                                        WHEN CREDITOS_ACTUAL > 226 AND CREDITOS_ACTUAL <= 248 AND EFF = '201710' AND PROGRAMA IN ('312','502') THEN '11'
                                                                        WHEN CREDITOS_ACTUAL > 248 AND CREDITOS_ACTUAL <= 270 AND EFF = '201710' AND PROGRAMA IN ('312','502') THEN '12'
                                                                        WHEN CREDITOS_ACTUAL > 270 AND CREDITOS_ACTUAL <= 314 AND EFF = '201710' AND PROGRAMA IN ('502') THEN '13'
                                                                        WHEN CREDITOS_ACTUAL > 314 AND PROGRAMA IN ('502') THEN '14' END
                                                ,''),'','')
                        END,''),'','')
             END,'10') END AS CICLO_ACTUAL
    FROM (
    /*------------------------------- INIT WITH --------------------------------*/
        WITH T1 AS (
                    SELECT    SORLCUR_PIDM        CODIGO,        
                              SORLCUR_TERM_CODE   PERIODO,
                              SORLCUR_PROGRAM     PROGRAMA,
                              SORLCUR_LEVL_CODE   NIVEL,
                              SORLCUR_CAMP_CODE   CAMPUS,  
                              SORLFOS_DEPT_CODE   DEPARTAMENTO
                    FROM (
                            SELECT    SORLCUR_PIDM,         SORLCUR_TERM_CODE,    SORLCUR_PROGRAM,  
                                      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,    SORLFOS_DEPT_CODE
                            FROM SORLCUR        INNER JOIN SORLFOS
                                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                                  AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                            WHERE   SORLCUR_PIDM        =   P_PIDM
                                AND SORLCUR_LMOD_CODE   =   'LEARNER'
                                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                                AND SORLCUR_CURRENT_CDE =   'Y'
                            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
                    ) WHERE ROWNUM <= 1
        ), T2 AS (
                SELECT UNIQUE SMBPOGN_PIDM,
                     SMBPOGN_REQUEST_NO,
                     SMRDOUS_CRSE_SOURCE,
                     SMBPOGN_TERM_CODE_EFF,
                     CASE
                       WHEN SMBPOGN_TERM_CODE_EFF >= '200710' AND SMBPOGN_TERM_CODE_EFF < '201510' THEN '200710'
                       WHEN SMBPOGN_TERM_CODE_EFF >= '201510' AND SMBPOGN_TERM_CODE_EFF < '201710' THEN '201510'
                       --WHEN SMBPOGN_TERM_CODE_EFF >= '201710' AND SMBPOGN_TERM_CODE_EFF < '' THEN ''
                       ELSE REPLACE(NVL(SMBPOGN_TERM_CODE_EFF,''),'','')
                     END EFF,
                     SMBPOGN_REQ_CREDITS_OVERALL,
                     SMBPOGN_ACT_CREDITS_OVERALL,
                     SMBPOGN_CAMP_CODE
                FROM SMBPOGN LEFT JOIN SMRDOUS
                ON SMBPOGN_PIDM = SMRDOUS_PIDM
                AND SMBPOGN_REQUEST_NO = SMRDOUS_REQUEST_NO
                INNER JOIN (  SELECT SMBPOGN_PIDM AS PIDPOGN,
                              MAX(SMBPOGN_REQUEST_NO) AS RQPOGN
                              FROM SMBPOGN
                              GROUP BY SMBPOGN_PIDM) 
                ON SMBPOGN_PIDM=PIDPOGN
                AND SMBPOGN_REQUEST_NO=RQPOGN
                WHERE SMBPOGN_PIDM = P_PIDM
        ),T3 AS (
                SELECT  SMBPOGN_PIDM,
                        SMRDRRQ_REQUEST_NO,
                        
                        --ORIGINAL
                        SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) AS CREDITOS_ELECTIVO,
                        --SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) AS CREDITOS_ELECTIVO,
                        
                        --ORIGINAL
                        SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) -
                        --SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) -
                        SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) -
                        SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END) AS CREDITOS_OBLIGATORIO,
                        
                        SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) AS DIPLOMADO,
                        
                        CASE
                             WHEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) > SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0))
                                  THEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) + (SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) - SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) - SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END))
                             WHEN SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) > SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS))
                                  THEN SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) + (SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) - SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) - SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END))
                             WHEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) = SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0))
                                  THEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) + (SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) - SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) - SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END))
                             ELSE SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) +
                                  (
                                   SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) -
                                   SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) -
                                   SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END)
                                  ) END AS CREDITOS_ACTUAL
                FROM SMRDOUS
                LEFT JOIN SMRDRRQ
                    ON SMRDRRQ_PIDM = SMRDOUS_PIDM
                    AND SMRDRRQ_REQUEST_NO = SMRDOUS_REQUEST_NO
                    AND SMRDOUS_AREA = SMRDRRQ_AREA
                    AND SMRDOUS_CAA_SEQNO = SMRDRRQ_CAA_SEQNO
                    AND SMRDOUS_KEY_RULE = SMRDRRQ_KEY_RULE
                    AND SMRDOUS_TERM_CODE_EFF = SMRDRRQ_TERM_CODE_EFF
                    AND SMRDOUS_RUL_SEQNO = SMRDRRQ_RUL_SEQNO
                LEFT JOIN SMBPOGN 
                    ON SMBPOGN_PIDM = SMRDOUS_PIDM
                    AND SMBPOGN_REQUEST_NO = SMRDOUS_REQUEST_NO
                WHERE SMRDOUS_CRSE_SOURCE IN ('H','T')
                AND SMRDOUS_PIDM = P_PIDM
                GROUP BY SMBPOGN_PIDM,SMRDRRQ_REQUEST_NO
        )
        SELECT DISTINCT * FROM T1
        LEFT JOIN T2 ON T2.SMBPOGN_PIDM = T1.CODIGO
        LEFT JOIN T3 ON T2.SMBPOGN_PIDM = T3.SMBPOGN_PIDM
                     AND T2.SMBPOGN_REQUEST_NO = T3.SMRDRRQ_REQUEST_NO
    /*------------------------------- FIN WITH ---------------------------------*/
    );

BEGIN
    
    OPEN C_CRED_CICLO;
    LOOP
      FETCH C_CRED_CICLO INTO V_CRED_OBLIG, V_CRED_TOTAL, V_CICLO_ACTL;
      EXIT WHEN C_CRED_CICLO%NOTFOUND;
    END LOOP;
    CLOSE C_CRED_CICLO;

    P_CREDITOS     := V_CRED_TOTAL;
    P_CICLO        := V_CICLO_ACTL;

END P_CREDITOS_CICLO;
