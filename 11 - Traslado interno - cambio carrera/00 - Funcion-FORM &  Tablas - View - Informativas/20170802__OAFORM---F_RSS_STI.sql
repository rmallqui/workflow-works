/*
SET SERVEROUTPUT ON
DECLARE BOOL VARCHAR2(1);
BEGIN
        BOOL :=  F_RSS_STI('80978','0','0');
        DBMS_OUTPUT.PUT_LINE(BOOL);
END;
*/


create or replace FUNCTION F_RSS_STI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_STI
              "Regla Solicitud de Servicio de SOLICITUD DE TRASLADO INTERNO "
  FECHA     : 08/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : - para alumnos con mayor a 22 creditos
              - No estar estudiando ni tener asignaturas en el presente periodo
              - El periodo se determina desde inicio de matricula y una semana antes de inicio de clases.
              

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   27/07/2017    RMALLQUI      Se agrego la VALIDACION de retencion ('01'-deuda pendiente)
  002   02/08/2017    RMALLQUI      Se agrego la validacion de que la solicitud solo se puede realizar una vez por periodo.
  =================================================================================================================== */
IS
      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL013';
      V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE; 
      V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;

      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM                SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
      V_CREDITOS            INTEGER;
      V_CICLO               INTEGER;
      V_NRC_N               INTEGER;
      V_PRIORIDAD           NUMBER;
      V_INDICADOR           NUMBER;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;


      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y UNA SEMANA ANTES DE INICIO DE CLASES(SOBPTRM_START_DATE-7)
      */
      CURSOR C_SOBPTRM IS
      SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE FROM (
          -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
          FROM SOBPTRM
          INNER JOIN SFRRSTS
            ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
          WHERE SFRRSTS_RSTS_CODE = 'RW'
          AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
          AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (SOBPTRM_START_DATE - 7)
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;


      -- Validar si tiene NRC activo en un determinado periodo (*** No se esta filtrando parte de periodo para los casos de modulos de UVIR Y UPGT)
      CURSOR C_SFRSTCR_N IS
      SELECT COUNT(*) NRC_ON FROM SFRSTCR
      WHERE SFRSTCR_TERM_CODE = V_CODE_TERM
      AND SFRSTCR_PIDM = P_PIDM
      AND SFRSTCR_RSTS_CODE IN ('RW','RE');
BEGIN  
--   
    -- VALIDACIÓN QUE NO TENGA RETENCION DE DEUDA "01" : (Se esta reduciendo la fecha SPRHOLD_TO_DATE a las 00:00:00 horas)
    SELECT COUNT(*) INTO V_INDICADOR 
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM 
    AND SPRHOLD_HLDD_CODE = '01' -- RETENCION Deuda pendiente
    AND TO_DATE(TO_CHAR(SPRHOLD_TO_DATE, 'dd/mm/yyyy'),'dd/mm/yyyy') > SYSDATE; 
    IF V_INDICADOR > 0 THEN
        RETURN 'N';
    END IF;

    -- DATOS ALUMNO
    P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);


    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN C_SFRRSTS_PTRM;
    LOOP
      FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFRRSTS_PTRM;


    -- PARTE DE PERIODOS
    V_SUB_PTRM_1 := V_SUB_PTRM || '1';
    V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (V_CODE_DEPT ='UVIR' OR V_CODE_DEPT ='UPGT') THEN '2' ELSE '-' END;


    ------------------------------------------------------------
    -- >> Obteniendo PERIODO ACTIVO Y LA PARTE PERIODO  --
    OPEN C_SOBPTRM;
      LOOP
        FETCH C_SOBPTRM INTO P_CODE_TERM, V_PTRM;
        IF C_SOBPTRM%FOUND THEN
            V_CODE_TERM := P_CODE_TERM;
        ELSE EXIT;
      END IF;
      END LOOP;
    CLOSE C_SOBPTRM;
    
    
    ------------------------------------------------------------
    -- >> VALIDACION:  SOLO se puede un solo TRASLADO INTERNO por PERIODO.
    SELECT COUNT(SVRSVPR_PROTOCOL_SEQ_NO) INTO V_INDICADOR FROM SVRSVPR 
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_SRVC_CODE = P_CODIGO_SOL
        AND SVRSVPR_SRVS_CODE = 'CO'; -- Estado 'CO'-Completado
    IF V_INDICADOR > 0 THEN
        RETURN 'N';
    END IF;


    ---------------------------------------------------------------------------------------------
    -- >> Validar si el estudiante esta estudiando en el periodo actual activo (TAMBIEN VALIDA por modulo PARTE PERIODO)
    OPEN C_SFRSTCR_N;
    LOOP
      FETCH C_SFRSTCR_N INTO V_NRC_N;
      EXIT WHEN C_SFRSTCR_N%NOTFOUND;
    END LOOP;
    CLOSE C_SFRSTCR_N;    


    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD(P_PIDM, P_CODIGO_SOL, V_CODE_DEPT,NULL,V_CODE_CAMP, V_PRIORIDAD);
    IF (V_PRIORIDAD <>  3 AND V_PRIORIDAD <> 5 AND V_PRIORIDAD <> 8) THEN
        RETURN 'N';
    END IF;


    -----------------------------------------------
    -- GET CREDITOS CICLO STUDN
    P_CREDITOS_CICLO(P_PIDM,V_CREDITOS,V_CICLO);
    

    IF V_CREDITOS >= 22 AND V_NRC_N = 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_STI;