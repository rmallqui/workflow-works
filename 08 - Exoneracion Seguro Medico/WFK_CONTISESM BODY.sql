create or replace PACKAGE BODY WFK_CONTISESM AS
/*******************************************************************************
 WFK_CONTISESM:
       Conti Package SOLICITUD DE EXONERACIÓN DE SEGURO DE SALUD
*******************************************************************************/
-- FILE NAME..: WFK_CONTISESM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISESM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/


PROCEDURE P_GET_USUARIO_BIENESTAR (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO              OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO_BIENESTAR
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              del Area especifica de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
      P_Email_Address           VARCHAR2(100);
    
      P_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO P_Email_Address FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_CORREO := P_CORREO || P_Email_Address || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      P_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_CORREO,1,LENGTH(P_CORREO) -1) INTO P_CORREO
      FROM DUAL
      WHERE SUBSTR(P_CORREO,-1,1) = ',';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              -- --DBMS_OUTPUT.PUT_LINE ();
              P_ERROR := 'A ocurrido un problema, se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, se encontraron mas de una ORGANIZACIÒN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre del ROL: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, NO  se encontrò ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_GET_USUARIO_BIENESTAR;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_AN                  OUT NUMBER,
  P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 12/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : cambia el estado de la solicitud (XXXXXX)

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   10/03/2016    RMALLQUI      Se agrego el parametro P_AN paravalidar si la solicitud este ANULADO.
  =================================================================================================================== */
AS
            P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
            P_ESTADO_PREVIOUS         SVVSRVS.SVVSRVS_CODE%TYPE;
            P_FECHA_SOL               SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
            P_INDICADOR               NUMBER;
            E_INVALID_ESTADO          EXCEPTION;
            V_PRIORIDAD               SVRSVPR.SVRSVPR_RSRV_SEQ_NO%TYPE;
BEGIN
--
  ----------------------------------------------------------------------------
        
        -- GET tipo de solicitud por ejemplo "SOL003" 
        SELECT  SVRSVPR_SRVC_CODE, 
                SVRSVPR_SRVS_CODE,
                SVRSVPR_RECEPTION_DATE,
                SVRSVPR_RSRV_SEQ_NO
        INTO    P_CODIGO_SOLICITUD,
                P_ESTADO_PREVIOUS,
                P_FECHA_SOL,
                V_PRIORIDAD
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
        
        -- SI EL ESTADO ES ANULADO (AN)
        P_AN := 0;
        IF P_ESTADO_PREVIOUS = 'AN' THEN
              P_AN := 1;
        END IF;
        
        -- VALIDAR que :
            -- El ESTADO actual sea modificable segun la configuracion.
            -- El ETSADO enviado pertenesca entre los asignados al tipo de SOLICTUD.
        SELECT COUNT(*) INTO P_INDICADOR
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
        AND SVRSVPR_SRVS_CODE IN (
              -- Estados de solic. MODIFICABLES segun las reglas y configuracion
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        )
        AND P_ESTADO IN (
              -- Estados de solic. configurados
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        );
        
        
        IF P_INDICADOR = 0 THEN
              RAISE E_INVALID_ESTADO;
        ELSIF (P_ESTADO_PREVIOUS <> P_ESTADO) THEN
              -- UPDATE SOLICITUD
              UPDATE SVRSVPR
                SET SVRSVPR_SRVS_CODE = P_ESTADO,  --AN , AP , RE entre otros
                --SVRSVPR_ACCD_TRAN_NUMBER = P_TRAN_NUMBER_OLD,
                --SVRSVPR_BILLING_IND = 'N', -- CASE WHEN P_ESTADO = 'AP' THEN 'Y' ELSE 'N' END,
                SVRSVPR_ACTIVITY_DATE = SYSDATE,
                SVRSVPR_DATA_ORIGIN = 'WorkFlow'
              WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
              AND SVRSVPR_SRVS_CODE IN (
                        -- Estados de solic. MODIFICABLES segun las reglas y configuracion
                        SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                        FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                        INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                        ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                        AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                        INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                        ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                        WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                        AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                        AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
                        AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                        AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
                )
              AND P_ESTADO IN (
                      -- Estados de solic. configurados
                      SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                      FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                      INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                      ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                      AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                      INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                      ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                      WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                      AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                      AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                      AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
              );
              
              COMMIT;
        END IF;
        
EXCEPTION
  WHEN E_INVALID_ESTADO THEN
          P_ERROR  := 'El "ESTADO" actual no es modificable ò no se encuentra entre los ESTADOS disponibles para la solicitud.';
  WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_CAMBIO_ESTADO_SOLICITUD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO_ALUMNO       OUT VARCHAR2
)
AS
      P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO P_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      
      -- GET COMENTARIO de la solicitud. (comentario PERSONALZIADO.)
      SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO 
      FROM (
            SELECT SVRSVAD_ADDL_DATA_DESC
            FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
            WHERE SVRSRAD_SRVC_CODE = P_CODIGO_SOLICITUD
            AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
            ORDER BY SVRSVAD_ADDL_DATA_SEQ DESC
      ) WHERE ROWNUM = 1;

END P_OBTENER_COMENTARIO_ALUMNO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_REGISTRAR_SEGURO (
      P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_COD_ATRIBUTO          IN STVATTS.STVATTS_CODE%TYPE,       -- COD atributos
      P_CODIGO_DETALLE        IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, --------------- APEC
      P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_REGISTRAR_SEGURO
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : ATRIBUTO de SEGURO MEDICO resta el COSTO DE SEGURO al alumno. 
              (Agrega un atributo y posteriormente genera el concepto(Cod Detalle) relacionado.)

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS

        P_PERIODO_NEXT              SFBETRM.SFBETRM_TERM_CODE%TYPE;
        NOM_SERVICIO                VARCHAR2(30);
        
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
              
        P_COD_DETALLE           SFRRGFE.SFRRGFE_DETL_CODE%TYPE;
        P_INDICADOR             NUMBER;
        C_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        P_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
        
        SAVE_ACT_DATE_OUT       VARCHAR2(100);
        RETURN_STATUS_IN_OUT    NUMBER;
        
        -- APEC PARAMS
        P_PART_PERIODO          VARCHAR2(9);
        P_APEC_CAMP             VARCHAR2(9);
        P_APEC_DEPT             VARCHAR2(9);
        P_APEC_TERM             VARCHAR2(10);
        P_APEC_IDSECCIONC       VARCHAR2(15);
        P_APEC_FECINIC          DATE;
        P_APEC_MOUNT          NUMBER;
        P_APEC_IDALUMNO       VARCHAR2(10);
BEGIN  
  
      -- GET - NEXT Periodo
      SELECT NEXT_TERM INTO P_PERIODO_NEXT
      FROM (
        SELECT STVTERM_CODE, LEAD(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) NEXT_TERM
        FROM STVTERM ORDER BY STVTERM_CODE DESC
      ) WHERE STVTERM_CODE = P_PERIODO;
    

      -- #######################################################################
      -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
      SELECT    SORLCUR_SEQNO,      
                SORLCUR_LEVL_CODE,    
                SORLCUR_CAMP_CODE,        
                SORLCUR_COLL_CODE,
                SORLCUR_DEGC_CODE,  
                SORLCUR_PROGRAM,      
                SORLCUR_TERM_CODE_ADMIT,  
                --SORLCUR_STYP_CODE,
                SORLCUR_STYP_CODE,  
                SORLCUR_RATE_CODE,    
                SORLFOS_DEPT_CODE   
      INTO      C_SEQNO,
                C_ALUM_NIVEL, 
                C_ALUM_CAMPUS, 
                C_ALUM_ESCUELA, 
                C_ALUM_GRADO, 
                C_ALUM_PROGRAMA, 
                C_ALUM_PERD_ADM,
                C_ALUM_TIPO_ALUM,
                C_ALUM_TARIFA,
                C_ALUM_DEPARTAMENTO
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                        SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                        SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;

      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)
      
            -- GET CRONOGRAMA SECCIONC
            SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
            
            SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
            SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
            SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
            
            -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_DEUDA
            WITH 
                CTE_tblSeccionC AS (
                        -- GET SECCIONC
                        SELECT  "IDSeccionC" IDSeccionC,
                                "FecInic" FecInic
                        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                        WHERE "IDDependencia"='UCCI'
                        AND "IDsede"    = P_APEC_CAMP
                        AND "IDPerAcad" = P_APEC_TERM
                        AND "IDEscuela" = C_ALUM_PROGRAMA
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                        AND "IDSeccionC" <> '15NEX1A'
                        AND SUBSTRB("IDSeccionC",-2,2) IN (
                            -- PARTE PERIODO           
                            SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                        )
                ),
                CTE_tblPersonaAlumno AS (
                        SELECT "IDAlumno" IDAlumno 
                        FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                        WHERE "IDPersona" IN ( 
                            SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                        )
                )
            SELECT "IDAlumno", "IDSeccionC" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
            FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
            AND "IDSede"      = P_APEC_CAMP
            AND "IDPerAcad"   = P_APEC_TERM
            AND "IDEscuela"   = C_ALUM_PROGRAMA
            AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
            AND "IDConcepto"  = P_CODIGO_DETALLE;
            
            -- GET MONTO 
            SELECT "Monto" INTO P_APEC_MOUNT
            FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDSede"      = P_APEC_CAMP
            AND "IDPerAcad"   = P_APEC_TERM
            AND "IDSeccionC"  = P_APEC_IDSECCIONC
            AND "IDConcepto"  = P_CODIGO_DETALLE;
            
            -- UPDATE MONTO(s)
            UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            SET "Cargo" = "Cargo" - P_APEC_MOUNT
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    = P_APEC_IDALUMNO
            AND "IDSede"      = P_APEC_CAMP
            AND "IDPerAcad"   = P_APEC_TERM
            AND "IDEscuela"   = C_ALUM_PROGRAMA
            AND "IDSeccionC"  = P_APEC_IDSECCIONC
            AND "IDConcepto"  = P_CODIGO_DETALLE;
      
      ELSE -- ********************************** 1 ********************************---> BANNER
      
            -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
            SELECT SFRRGFE_DETL_CODE, SFRRGFE_MIN_CHARGE INTO P_COD_DETALLE, C_CARGO_MINIMO
            FROM SFRRGFE 
            WHERE SFRRGFE_TERM_CODE = P_PERIODO AND SFRRGFE_ATTS_CODE = P_COD_ATRIBUTO 
            AND SFRRGFE_TYPE = 'STUDENT'
            AND NVL(NVL(SFRRGFE_LEVL_CODE, c_alum_nivel),'-')           = NVL(NVL(c_alum_nivel, SFRRGFE_LEVL_CODE),'-')---------------- Nivel
            AND NVL(NVL(SFRRGFE_CAMP_CODE, c_alum_campus),'-')          = NVL(NVL(c_alum_campus, SFRRGFE_CAMP_CODE),'-')  ------------- Campus (sede)
            AND NVL(NVL(SFRRGFE_COLL_CODE, c_alum_escuela),'-')         = NVL(NVL(c_alum_escuela, SFRRGFE_COLL_CODE),'-') ------------- Escuela 
            AND NVL(NVL(SFRRGFE_DEGC_CODE, c_alum_grado),'-')           = NVL(NVL(c_alum_grado, SFRRGFE_DEGC_CODE),'-') --------------- Grado
            AND NVL(NVL(SFRRGFE_PROGRAM, c_alum_programa),'-')          = NVL(NVL(c_alum_programa, SFRRGFE_PROGRAM),'-') -------------- Programa
            AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm),'-')  = NVL(NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT),'-') ------ Periodo Admicion
            -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
            -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
            -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
            AND NVL(NVL(SFRRGFE_DEPT_CODE, c_alum_departamento),'-')    = NVL(NVL(c_alum_departamento, SFRRGFE_DEPT_CODE),'-') -------- Departamento
            -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
            AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum),'-') = NVL(NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') --- Tipo Alumno Curriculum
            AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa),'-')   = NVL(NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------ Trf Curriculum (Escala)
            
            -- GET - Descripcion de CODIGO DETALLE  
            SELECT TBBDETC_DESC INTO NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;
      
      
            -- #######################################################################
            -- SET ATRIBUTO alumno
            INSERT INTO SGRSATT
              (
                SGRSATT_PIDM, 
                SGRSATT_TERM_CODE_EFF,
                SGRSATT_ATTS_CODE,
                SGRSATT_ACTIVITY_DATE,
                SGRSATT_STSP_KEY_SEQUENCE
              )
            SELECT P_PIDM_ALUMNO, P_PERIODO, P_COD_ATRIBUTO, SYSDATE, NULL
            FROM DUAL
            WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                          WHERE SGRSATT_PIDM = P_PIDM_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
                          AND (SGRSATT_ATTS_CODE = P_COD_ATRIBUTO OR SGRSATT_ATTS_CODE IS NULL));
            P_INDICADOR := SQL%ROWCOUNT;  
            -- ACTUALIZAR en caso exista NULL con el ATRIBUTO para el periodo.
            IF P_INDICADOR = 0  THEN
                UPDATE SGRSATT SET SGRSATT_ATTS_CODE = P_COD_ATRIBUTO
                WHERE SGRSATT_PIDM = P_PIDM_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
                AND SGRSATT_ATTS_CODE IS NULL 
                AND NOT EXISTS (SELECT SGRSATT_PIDM FROM SGRSATT 
                          WHERE SGRSATT_PIDM = P_PIDM_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO
                          AND SGRSATT_ATTS_CODE = P_COD_ATRIBUTO);
            END IF;
         
            -- SET - ATRIBUTO FINALIZAR (LIMITAR VALIDES del atributo A UN SOLO PERIODO)
            INSERT INTO SGRSATT
              ( 
                SGRSATT_PIDM, 
                SGRSATT_TERM_CODE_EFF,
                SGRSATT_ATTS_CODE,
                SGRSATT_ACTIVITY_DATE,
                SGRSATT_STSP_KEY_SEQUENCE
              )
            SELECT P_PIDM_ALUMNO, P_PERIODO_NEXT, NULL, SYSDATE, NULL
            FROM DUAL
            WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                              WHERE SGRSATT_PIDM = P_PIDM_ALUMNO 
                              AND SGRSATT_TERM_CODE_EFF = P_PERIODO_NEXT
                          );
            P_INDICADOR := SQL%ROWCOUNT;
      
            -- #######################################################################
            -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
            SFKFEES.p_processfeeassessment (  P_PERIODO,
                                              P_PIDM_ALUMNO,
                                              SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                              SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                              'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                              'Y',          -- create TBRACCD records
                                              'SFAREGS',    -- where assessment originated from
                                              'Y',          -- commit changes
                                              SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                              'N',          -- do not ignore SFRFMAX rules
                                              RETURN_STATUS_IN_OUT );   -- OUT -- return_status

      END IF;
        
      COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_REGISTRAR_SEGURO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--                   
END WFK_CONTISESM;