APUNTES
=================================================================================
ENALCE COMPARTIDO: https://drive.google.com/open?id=0B6g9PRwmQiqoVUswS3hSOXNCX2M

Se creo un paquete en la tarea 2: contendra en orden todas las funciones, procedimeintos creados para este fin.
		

	+++++++++++++++++ SOLICITUD DE EXONERACIÓN DE SEGURO DE SALUD ++++++++++++++++++++++++++++

		- Nombre del paquete : 			WFK_CONTISESM
		- Cuerpo del pakquete :			WFK_CONTISESM BODY



---------------------------------------------------------------------------------------------------------------------------------
FUNCION : 
---------------------------------------------------------------------------------------------------------------------------------

		-- 

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 1 --> carpeta 1 : Ttrigger -- """"" SOL012 """""
---------------------------------------------------------------------------------------------------------------------------------
	## Detecte la creación de una nueva solicitud del tipo SOL012.

	- Se elaboro triguer, Trigguer-TG_SVASVPR_DESPUES_INSERT

	- se modificò el orden de los parametros
					P_PIDM_ALUMNO
					P_ID_ALUMNO
					P_PERIODO
					P_CORREO_ALUMNO
					P_NOMBRE_ALUMNO
					P_FOLIO_SOLICITUD
					P_FECHA_SOLICITUD
					P_COMENTARIO_ALUMNO
					P_MODALIDAD_ALUMNO
					P_COD_SEDE
					P_NOMBRE_SEDE

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 2 --> carpeta 2 : Obtener usuario 
---------------------------------------------------------------------------------------------------------------------------------
				PROCEDURE P_GET_USUARIO_BIENESTAR (
				      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
				      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
				      P_CORREO              OUT VARCHAR2,
				      P_ROL_SEDE            OUT VARCHAR2,
				      P_ERROR               OUT VARCHAR2
				)

	- Se creo procedimeinto: lleva nombre de archivo con la fecha por delante 20161223-P_GET_USUARIO_BIENESTAR.sql
	  selecionar el ultimo creado.

	- WF : 

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 3 --> carpeta 3 : cambio de estado solicitud
---------------------------------------------------------------------------------------------------------------------------------
					PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
							  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
							  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
							  P_ERROR               OUT VARCHAR2
					  )

	- Se creo procedimeinto: lleva nombre de archivo con la fecha por delante 20161107_p_cambio_estado_solicitud.sql
	  selecionar el ultimo creado.

	- WF : 

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 4 --> carpeta 4 - p_obtener_comentario_alumno
---------------------------------------------------------------------------------------------------------------------------------
				P_OBTENER_COMENTARIO_ALUMNO (
				      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
				      P_COMENTARIO_ALUMNO       OUT VARCHAR2
				)

	- Se creo procedimeinto: lleva nombre de archivo con la fecha por delante 20161209_P_OBTENER_COMENTARIO_ALUMNO.sql
	  selecionar el ultimo creado.

	- WF : 


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 5 --> carpeta 5 - p_registrar_seguro
---------------------------------------------------------------------------------------------------------------------------------
			PROCEDURE P_REGISTRAR_SEGURO (
	              P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
	              P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
	              P_COD_ATRIBUTO          IN STVATTS.STVATTS_CODE%TYPE,       -- COD atributos
	              P_CODIGO_DETALLE        IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, --------------- APEC
	              P_ERROR                 OUT VARCHAR2
	          );

	- Se creo procedimeinto: lleva nombre de archivo con la fecha por delante 20161223----P_REGISTRAR_SEGURO.sql
	  selecionar el ultimo creado.

	- WF : 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

EVENTOS: